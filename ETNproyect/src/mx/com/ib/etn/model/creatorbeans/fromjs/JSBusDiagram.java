package mx.com.ib.etn.model.creatorbeans.fromjs;

import mx.com.ib.etn.view.seats.GraphicButtonSeat;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

public class JSBusDiagram {

	private int numFilasDiagram;
	private int numFilasArriba;
	private int numErrorDiagram;

	private String descErrorDiagram;
	private int numAsientosDiagram;

	private String codigoError;
	private String descripcionError;

	/*

	int SEAT_OCCUPIED = 0;
	int SEAT_NOT_OCCUPIED = 1;
	int SEAT_OCCUPIED_TV = 2;
	int SEAT_NOT_OCCUPIED_TV = 3;
	int SEAT_AISLE  = 4;

	c -> cafeter�a
	H -> ba�o hombres
	M -> ba�o mujeres
	P -> escalera
	T -> television
	U -> ba�o unisex

	"01  ","00  ","02  ","03T ",

	*/
	
	public GraphicButtonSeat[][] createDiagram(JSONObject jsObject) throws JSONException {
	//public GraphicButtonSeat[][] createDiagram(JSONObject jsObject, String xxx) throws JSONException {

		//jsObject = createJSONDiagram();

		JSONArray jsonArray = jsObject.getJSONArray("diagramaAutobus");

		String snumFilas = (String) jsObject.get("numFilas");

		numFilasDiagram = Integer.parseInt(snumFilas);
		numErrorDiagram = jsObject.getInt("numError");
		descErrorDiagram = (String) jsObject.get("descError");
		numAsientosDiagram = jsObject.getInt("numAsientos");
		
		// Calculos para crear el array
		
		int i = numFilasDiagram - 1;
		int j = (numAsientosDiagram/i) + 1;
		
		String arrayArrive[] = null;
		
		GraphicButtonSeat seats[][] = new  GraphicButtonSeat[i][j];
		
		int length = jsonArray.length();

		int c = 0;
		
		for (int a = 0; a < i; a++) {

			for (int b = 0; b < j; b++) {
				
				String info = (String)jsonArray.get(c);
				info = info.toUpperCase();
				System.out.println(info);
				
				c++;
				
				int statusSeat = GraphicButtonSeat.SEAT_NOT_OCCUPIED;
				
				int other = 0; 
				
				if (info.indexOf("T")>=0){
					statusSeat = GraphicButtonSeat.SEAT_NOT_OCCUPIED_TV;
				} else if (info.indexOf("000")>=0){
					statusSeat = GraphicButtonSeat.SEAT_AISLE;
					if (info.indexOf("H")>=0){
						other = GraphicButtonSeat.SEAT_WC_MAN;
					} else if (info.indexOf("M")>=0){
						other = GraphicButtonSeat.SEAT_WC_WOMAN;
					} else if (info.indexOf("C")>=0){
						other = GraphicButtonSeat.SEAT_COFFEE_SHOP;
					} else if (info.indexOf("U")>=0){
						other = GraphicButtonSeat.SEAT_WC_UNISEX;
					} else if (info.indexOf("P")>=0){
						other = GraphicButtonSeat.SEAT_STAIR;
					}
				}
				
				GraphicButtonSeat buttonSeat = new GraphicButtonSeat(statusSeat, other);
				
				buttonSeat.setSeat(info);
				
				seats[a][b] = buttonSeat;
			}
		}
		
		return seats;
	}


	public  GraphicButtonSeat[][] setOccupancy(JSONObject jsObject, GraphicButtonSeat seats[][]) throws JSONException {

		
		//jsObject = createJSONOccupancy();
		
		codigoError = (String) jsObject.get("codigoError");
		descripcionError = (String) jsObject.get("descripcionError");

		JSONArray jsonArray = jsObject.getJSONArray("asientos");
		int length = jsonArray.length();

		int i = seats.length;
		int j = seats[0].length;

		int c = 0;
		
		for(int a = 0; a < i; a++){
			for(int b = 0; b < j; b++){
				
				if (!seats[a][b].isSeatAisle()){
					String status = (String) jsonArray.get(c);
					c++;
					System.out.println(status + " " + c);
					
					if (status.equals("1")){
						
						seats[a][b].setOccupied(true);
					}
				}
				seats[a][b].setBitmap();
			}
		}
	
		return seats;
	}
}



/*
private JSONObject createJSONDiagram() throws JSONException{
	
	String sJson = "{\"diagramaAutobus\":[\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"023T\",\"024T\",\"000 \",\"025T\",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000H\",\"000M\",\"000C\",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"001T\",\"000 \",\"002T\",\"003T\",\"004T\",\"000 \",\"005T\",\"006T\",\"007T\",\"000 \",\"008T\",\"009T\",\"010T\",\"000 \",\"011T\",\"012T\",\"013T\",\"000 \",\"000P\",\"000 \",\"014T\",\"000 \",\"015T\",\"016T\",\"017T\",\"000 \",\"018T\",\"019T\",\"020T\",\"000 \",\"021T\",\"022T\",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \"],\"numFilas\":\"7\",\"numFilasArriba\":\"8\",\"numError\":1111,\"descError\":\"Cadena correcta.\",\"numAsientos\":25}";
	
	JSONObject jsonObject = new JSONObject(sJson);
	
	return jsonObject;
}

private JSONObject createJSONOccupancy() throws JSONException{
	
	String sJson = "{\"asientos\":[\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\"codigoError\":\"00\",\"descripcionError\":\"Sin Error.\"}";
	
	JSONObject jsonObject = new JSONObject(sJson);
	
	return jsonObject;
}
*/

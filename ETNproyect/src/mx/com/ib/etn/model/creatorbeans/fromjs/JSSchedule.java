package mx.com.ib.etn.model.creatorbeans.fromjs;

import java.util.Vector;

import mx.com.ib.etn.beans.dto.Scheduled;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

public class JSSchedule {

	public Vector getVectorSchedule(JSONObject jsObject) throws JSONException{

		JSONArray jsonArray = jsObject.getJSONArray("listaCorridas");
		Scheduled scheduled = null;
		Vector scheduleds = new Vector();

		int length = jsonArray.length();

		for (int i = 0; i < length; i++) {

			JSONObject childJSONObject = jsonArray.getJSONObject(i);
			scheduled = new Scheduled();

			int numError = childJSONObject.getInt("errorNumero");

			if (numError == 0){
				scheduled.setFlight(childJSONObject.getString("corrida"));
				scheduled.setHourDepart(childJSONObject.getString("horaCorrida"));
				scheduled.setDateDepart(childJSONObject.getString("fechaInicial"));
				scheduled.setDateArrive(childJSONObject.getString("fechaLlegada"));
				scheduled.setHourArrive(childJSONObject.getString("secuencia"));
				scheduled.setCompanyFlight(childJSONObject.getString("empresaCorrida"));
				scheduled.setCharge(childJSONObject.getString("tarifa"));
				scheduled.setChargeAdults(childJSONObject.getString("tarifaAdulto"));
				scheduled.setChargeChildren(childJSONObject.getString("tarifaNino"));
				scheduled.setChargeElderlies(childJSONObject.getString("tarifaInsen"));
				scheduled.setChargeStudents(childJSONObject.getString("tarifaEstudiante"));
				scheduled.setChargeProfessors(childJSONObject.getString("tarifaMaestro"));
				scheduled.setFlightKind(childJSONObject.getString("tipoCorrida"));
				scheduled.setServicio(childJSONObject.getString("detServicio"));

				scheduleds.addElement(scheduled);
			} else {

				JSError jsError = new JSError(jsObject); 
				scheduleds.addElement(jsError);
			}
		}

        return scheduleds;
	}
}

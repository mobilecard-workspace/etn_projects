package mx.com.ib.etn.beans.beans;

import java.util.Vector;

public class ListaNits {

	private Vector nits;
	
	public ListaNits(){
		
		nits = new Vector();
	}
	
	public Vector getNits() {
		return nits;
	}

	public void setNits(Vector nits) {
		this.nits = nits;
	}
	
	public void addElement(Nit nit){
		nits.addElement(nit);
	}
}

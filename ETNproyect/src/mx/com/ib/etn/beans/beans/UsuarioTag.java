package mx.com.ib.etn.beans.beans;

public class UsuarioTag  {
    
    private String usuario;
    private String tipotag;
    private String etiqueta;
    private String numero;
    private String dv;
    
    
    public UsuarioTag(String usuario,String tipotag,String etiqueta,String numero,String dv){
    	this.usuario=usuario;
    	this.tipotag=tipotag;
    	this.etiqueta=etiqueta;
    	this.numero=numero;
    	this.dv=dv;
    	
    }
    
        /**
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    
        
        /**
     * @return the usuario
     */
    public String getTipoTag() {
        return tipotag;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setTipoTag(String tipotag) {
        this.tipotag = tipotag;
    }
    
   /**
     * @return the usuario
     */
    public String getEtiqueta() {
        return etiqueta;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }
    
   /**
     * @return the usuario
     */
    public String getNumero() {
        return numero;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }
    
   /**
     * @return the usuario
     */
    public String getDv() {
        return dv;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setDv(String dv) {
        this.dv = dv;
    }
      
    
}


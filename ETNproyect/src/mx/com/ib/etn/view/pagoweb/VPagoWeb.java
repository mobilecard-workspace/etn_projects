package mx.com.ib.etn.view.pagoweb;

import mx.com.ib.etn.model.connection.Url;
import mx.com.ib.etn.view.controls.CustomMainScreen;
import net.rim.device.api.browser.field2.BrowserField;
import net.rim.device.api.browser.field2.BrowserFieldConfig;
import net.rim.device.api.i18n.SimpleDateFormat;
import net.rim.device.api.ui.component.DateField;
import net.rim.device.api.ui.component.LabelField;


public class VPagoWeb extends CustomMainScreen {

	private DateField dateField;

	public VPagoWeb(String title) {
	
		super(true, "Pago 3D-Secure");
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("mm:ss");
		dateField = new DateField("Tiempo restante para transacción:", 480000, dateFormat, LabelField.NON_FOCUSABLE);
		add(dateField);
		
		(new Thread(new Updater(this))).start();
	}

	public void updateDateField(long seconds){
		dateField.setDate(seconds);
	}
	
	
	public void execute(String get){

		System.out.println(Url.URL_PAGO_PROSA + "?" + get);
		System.out.println(Url.URL_PAGO_PROSA + "?" + get);
		
		VConnectionFactory connFactory = new VConnectionFactory();

		BrowserFieldConfig config = new BrowserFieldConfig();
		config.setProperty(BrowserFieldConfig.CONNECTION_FACTORY, connFactory);
        config.setProperty(BrowserFieldConfig.ENABLE_COOKIES,Boolean.TRUE);
        config.setProperty(BrowserFieldConfig.JAVASCRIPT_ENABLED, Boolean.TRUE);
        config.setProperty(BrowserFieldConfig.NAVIGATION_MODE,BrowserFieldConfig.NAVIGATION_MODE_POINTER);
        BrowserField browserField = new BrowserField(config); 
        browserField.requestContent(Url.URL_PAGO_PROSA + "?" + get);
        
	    //browserField.requestContent("http://mobilecard.mx:8080/ProComGDF/prosa_gdf.jsp?" + "");
	    
	    add(browserField);
	}
}

package mx.com.ib.etn.view.pagoweb;

import java.util.Date;

import net.rim.device.api.ui.UiApplication;

public class Updater implements Runnable{

	private VPagoWeb vPagoWeb;
	
	public Updater(VPagoWeb vPagoWeb){
		
		this.vPagoWeb = vPagoWeb;
	}
	
	private void updateField(long seconds) {
		synchronized (UiApplication.getEventLock()) {
			vPagoWeb.updateDateField(seconds);
		}
	}
	
	public void run() {

		try {
			boolean exit = true;

			Date first = new Date();

			long lFirst = first.getTime() + 480000;

			while (exit) {
				Date second = new Date();
				long lSecond = second.getTime();

				long resta = lFirst - lSecond;
				
				if (resta < 0){
					resta = 0;
					exit = false;
				}

				updateField(resta);
				Thread.sleep(1000);
			}
			
			synchronized (UiApplication.getEventLock()) {
				UiApplication.getUiApplication().popScreen(vPagoWeb);
			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}

package mx.com.ib.etn.view.roundflight;

import java.util.Vector;

import mx.com.ib.etn.beans.beans.Resumen;
import mx.com.ib.etn.beans.dto.Router;
import mx.com.ib.etn.beans.dto.Scheduled;
import mx.com.ib.etn.model.connection.base.ThreadHTTP;
import mx.com.ib.etn.model.connection.base.UrlEncode;
import mx.com.ib.etn.model.data.DataKeeper;
import mx.com.ib.etn.model.data.Payer;
import mx.com.ib.etn.view.controls.CustomMainScreen;
import mx.com.ib.etn.view.controls.Viewable;
import mx.com.ib.etn.view.resumen.ViewResumenCompra;
import net.rim.blackberry.api.browser.URLEncodedPostData;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Screen;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;

import org.json.me.JSONException;
import org.json.me.JSONObject;


public class ViewRoundFligth extends CustomMainScreen implements
FieldChangeListener, Viewable {

	
	private DataKeeper dataKeeper;
	private ButtonField accept;
	private boolean isBasicFlight;

	private JSONObject jsDepart;
	private JSONObject jsArrive;

	private String importeTotal1;
	private String numError1;
	private String idReservacion1;
	
	private String importeTotal2;
	private String numError2;
	private String idReservacion2;
	
	private double importe1 = 0;
	private double importe2 = 0;
	private double monto = 0;
	
	public ViewRoundFligth(boolean isBasicFlight) {

		super(true, "Asignación de asientos");

		this.isBasicFlight = isBasicFlight;

		
		dataKeeper = DataKeeper.getInstance();
		
		Scheduled scheduledDepart = dataKeeper.getScheduledDepart(); 
		Scheduled scheduledArrive = dataKeeper.getScheduledArrive();
		Router routerDepart = dataKeeper.getRouterDepart();
		Router routerArrive = dataKeeper.getRouterArrive();

		CustomGridFieldRoundFligth gridFieldRoundFligthDeparte = new CustomGridFieldRoundFligth(true, isBasicFlight, routerDepart, routerArrive, scheduledDepart);
		add(gridFieldRoundFligthDeparte);
		
		CustomGridFieldRoundFligth gridFieldRoundFligthArrive = new CustomGridFieldRoundFligth(false, isBasicFlight, routerArrive, routerDepart, scheduledArrive);
		add(gridFieldRoundFligthArrive);
		
		accept = new ButtonField("Hacer reservación");
		accept.setChangeListener(this);
		add(accept);
	}

	
	public void sendMessage(String message) {

		Dialog.alert(message);
	}
	
	public void setData(int request, JSONObject jsObject) {
/*
		if (jsObject == null){
			final int OK = 0;
			final int DEFAULT = OK;
			int answer = 0;

			answer = Dialog.ask("No se pudo interpretar la respuesta, por favor consulte su saldo.", new String[] {"Salir"}, new int[] {OK}, DEFAULT);

			switch (answer) {
			case OK:
				UiApplication.getUiApplication().popScreen(this);
				break;
			}
		} else {

		}
*/
		
		String referencia = null;
		
		try {
			
			Resumen resumenOrigen = new Resumen();
			Resumen resumenDestino = new Resumen();
			
			JSONObject jsReferencia = jsObject.getJSONObject("0");
			referencia = jsReferencia.getString("secuencia");

			JSONObject jsViaje1 = jsObject.getJSONObject("1");
			String descError = jsViaje1.getString("descError");
			int numError = jsViaje1.getInt("numError");

			//if ((descError == null)||(descError.length() == 0)){
			if (numError == 0){
				importeTotal1 = jsViaje1.getString("importeTotal");
				numError1 = jsViaje1.getString("numError");
				idReservacion1 = jsViaje1.getString("idReservacion");
				
				if (jsObject.has("2")){
					
					JSONObject jsViaje2 = jsObject.getJSONObject("2");
					
					importeTotal2 = jsViaje2.getString("importeTotal");
					numError2 = jsViaje2.getString("numError");
					idReservacion2 = jsViaje2.getString("idReservacion");
				} else {
					numError2 = "0";
					resumenDestino.setValid(false);
				}

				//if ((!numError1.equals("0"))&&(!numError2.equals("0"))){
				if(true){
					if (importeTotal1 != null){
						
						importe1 = Double.valueOf(importeTotal1).doubleValue();
					}
					
					if (importeTotal2 != null){
						
						importe2 = Double.valueOf(importeTotal2).doubleValue();
					}
					
					monto = importe1 + importe2;

					resumenOrigen.setIdReservacion(idReservacion1);
					resumenOrigen.setImporte(importeTotal1);
					resumenOrigen.setImporteTotal(String.valueOf(monto));

					resumenDestino.setIdReservacion(idReservacion1);
					resumenDestino.setImporte(importeTotal2);
					resumenDestino.setImporteTotal(String.valueOf(monto));
/*
					ResumenPopUp resumenPopUp = new ResumenPopUp(this, resumenOrigen, resumenDestino, referencia);
					UiApplication.getUiApplication().pushScreen(resumenPopUp);
*/
					//Screen screen = UiApplication.getUiApplication().getActiveScreen();
					UiApplication.getUiApplication().popScreen(this);
					UiApplication.getUiApplication().pushScreen(new ViewResumenCompra(resumenOrigen, resumenDestino, referencia));

				} else {

					Dialog.alert("No se pudo reservar el/los boletos");
				}
			} else {

				Dialog.alert(descError);
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}


	}

	private int payment = 0;
	
	public void fieldChanged(Field field, int context) {

		if (field == (ButtonField)accept){

			payment = 0;
			
			if ((dataKeeper.getSeatsDepart()!= null)&&(dataKeeper.getSeatsArrive()!= null)){

				URLEncodedPostData idSecuencia = new URLEncodedPostData("", "");
				UrlEncode idUrlEncode = new UrlEncode();
				idUrlEncode.setRequest(Viewable.ID_SECUENCIA);
				idUrlEncode.setEncodedPostData(idSecuencia);

				Payer payer = new Payer();
				URLEncodedPostData irEncoded = payer.toPay(true, isBasicFlight, this);
				URLEncodedPostData regresarEncoded = payer.toPay(false, isBasicFlight, this);

				UrlEncode irUrlEncode = new UrlEncode();
				UrlEncode regresarUrlEncode = new UrlEncode();

				irUrlEncode.setRequest(Viewable.CREATE);
				irUrlEncode.setEncodedPostData(irEncoded);

				regresarUrlEncode.setRequest(Viewable.CREATE);
				regresarUrlEncode.setEncodedPostData(regresarEncoded);

				Vector vector = new Vector();
				vector.addElement(idUrlEncode);
				vector.addElement(irUrlEncode);
				vector.addElement(regresarUrlEncode);

				ThreadHTTP threadHTTP = new ThreadHTTP();
				threadHTTP.setHttpConfigurator(this, vector);
				threadHTTP.start();

			} else {
				
				Dialog.alert("Falta Asignar Asientos");
			}
		}
	}


	public boolean onClose(){

		dataKeeper.setPassengersArrive(null);
		dataKeeper.setPassengersDepart(null);

		dataKeeper.setSeatsDepart(null);
		dataKeeper.setSeatsArrive(null);

		return super.onClose();
	}
}



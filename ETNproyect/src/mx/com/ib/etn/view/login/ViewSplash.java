package mx.com.ib.etn.view.login;

import java.util.Timer;
import java.util.TimerTask;



import mx.com.ib.etn.model.connection.Url;
import mx.com.ib.etn.model.connection.addcel.implementation.Versionador;
import mx.com.ib.etn.utils.UtilIcon;
import mx.com.ib.etn.view.controls.CustomMainScreen;
import mx.com.ib.etn.view.etnbuy.ViewETNBuy;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;

public class ViewSplash extends CustomMainScreen {

	public ViewSplash() {

		super(false, null);

		BitmapField splash = UtilIcon.getSplash();

		add(splash);
		
		Timer timer = new Timer();

		Wait wait = new Wait(this);
		
		timer.schedule(wait, 2800);
	}

	class Wait extends TimerTask {

		private ViewSplash viewSplash;

		public Wait(ViewSplash viewSplash) {

			this.viewSplash = viewSplash;
			
			Versionador versionador = new Versionador("", Url.URL_VERSIONADOR, null);
			versionador.run();
		}

		public void run() {

			UiApplication.getUiApplication().invokeAndWait(new Runnable() {

				public void run() {
					//UiApplication.getUiApplication().popScreen(viewSplash);
					//UiApplication.getUiApplication().pushScreen(new ViewETNBuy(null));
				}
			});

		}
	}
}

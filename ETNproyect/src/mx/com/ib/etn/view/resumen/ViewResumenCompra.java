package mx.com.ib.etn.view.resumen;

import java.util.Vector;

import mx.com.ib.etn.beans.beans.Resumen;
import mx.com.ib.etn.beans.beans.UserBean;
import mx.com.ib.etn.beans.dto.Passenger;
import mx.com.ib.etn.beans.dto.Router;
import mx.com.ib.etn.beans.dto.Scheduled;
import mx.com.ib.etn.model.data.DataKeeper;
import mx.com.ib.etn.model.data.DataTravel;
import mx.com.ib.etn.utils.UtilIcon;
import mx.com.ib.etn.view.controls.CustomMainScreen;
import mx.com.ib.etn.view.controls.CustomSelectedButtonField;
import mx.com.ib.etn.view.controls.TitleOrangeRichTextField;
import mx.com.ib.etn.view.pagoweb.VPagoWeb;
import net.rim.blackberry.api.browser.URLEncodedPostData;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Screen;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.VerticalFieldManager;


public class ViewResumenCompra extends CustomMainScreen implements FieldChangeListener{

	private DataKeeper dataKeeper;
	private DataTravel dataTravel;

	private Scheduled scheduledDepart = null; 
	private Scheduled scheduledArrive = null;
	private Router routerDepart = null; 
	private Router routerArrive = null;
	
	private Vector passengersDepart;
	private Vector passengersArrive;

	private CustomSelectedButtonField aceptar;
	private CustomSelectedButtonField cancelar;
	
	private Resumen resumenOrigen;
	private Resumen resumenDestino;
	private String referencia;
	
	
	public ViewResumenCompra(Resumen resumenOrigen, Resumen resumenDestino, String referencia){
	//public ViewResumenCompra(){
		super(true, "Resumen de compra");

		this.resumenOrigen = resumenOrigen;
		this.resumenDestino = resumenDestino;
		this.referencia = referencia;

		dataKeeper = DataKeeper.getInstance();
		
		passengersDepart = dataKeeper.getPassengersDepart();
		passengersArrive = dataKeeper.getPassengersArrive(); 
		
		scheduledDepart = dataKeeper.getScheduledDepart(); 
		scheduledArrive = dataKeeper.getScheduledArrive();
		routerDepart = dataKeeper.getRouterDepart(); 
		routerArrive = dataKeeper.getRouterArrive();
		
		
		TitleOrangeRichTextField datosViaje = new TitleOrangeRichTextField("Datos del viaje");
		add(datosViaje);
		
		CustomGridFieldScheduled fieldScheduled = new CustomGridFieldScheduled(routerDepart, routerArrive, scheduledDepart);
		add(fieldScheduled);


		TitleOrangeRichTextField pasajeros = new TitleOrangeRichTextField("Pasajeros");
		add(pasajeros);

		int size = passengersDepart.size();
		
		for(int index = 0; index < size; index++){
			
			Passenger passenger = (Passenger)passengersDepart.elementAt(index);
			
			CustomGridFieldPasajero customGridFieldPasajero = new CustomGridFieldPasajero(passenger);
			add(customGridFieldPasajero);
		}


		dataTravel = dataKeeper.getDataTravel();
		
		if (!dataTravel.isBasicFlight()){
			
			add(new LabelField(""));
			TitleOrangeRichTextField datosViajeRegreso = new TitleOrangeRichTextField("Datos del viaje de regreso");
			add(datosViajeRegreso);

			CustomGridFieldScheduled fieldScheduled2 = new CustomGridFieldScheduled(routerArrive, routerDepart, scheduledArrive);
			add(fieldScheduled2);

			size = passengersArrive.size();

			TitleOrangeRichTextField pasajerosIda = new TitleOrangeRichTextField("Pasajeros");
			add(pasajerosIda);
			
			for(int index = 0; index < size; index++){

				Passenger passenger = (Passenger)passengersArrive.elementAt(index);

				CustomGridFieldPasajero customGridFieldPasajero = new CustomGridFieldPasajero(passenger);
				add(customGridFieldPasajero);
			}
		}

		add(new LabelField(""));
		CustomGridFieldResumen fieldResumen = new CustomGridFieldResumen(resumenOrigen);
		add(fieldResumen);
		
		add(new LabelField(""));
		
		VerticalFieldManager fieldManager = new VerticalFieldManager();
		
		aceptar = new CustomSelectedButtonField("Pagar", Field.FIELD_VCENTER);
		aceptar.setChangeListener(this);
		fieldManager.add(aceptar);
		
		
		cancelar = new CustomSelectedButtonField("Cancelar", Field.FIELD_VCENTER);
		cancelar.setChangeListener(this);
		fieldManager.add(cancelar);
		
		add(fieldManager);
	}

	
	public void fieldChanged(Field field, int context) {

		if (field == aceptar){
			URLEncodedPostData encodedPostData = new URLEncodedPostData(URLEncodedPostData.DEFAULT_CHARSET, false);

			encodedPostData.append("user", UserBean.idLogin);  
			encodedPostData.append("referencia", referencia);
			encodedPostData.append("monto", resumenOrigen.getImporteTotal());
			encodedPostData.append("lang", "es");
			//encodedPostData.append("monto", "3");

			String post = encodedPostData.toString();
			
			//Screen screen = UiApplication.getUiApplication().getActiveScreen();
			UiApplication.getUiApplication().popScreen(this);
			
			VPagoWeb pagoWeb = new VPagoWeb("Pago 3D-Secure ETN");
			pagoWeb.execute(post);
			
			Screen screen = UiApplication.getUiApplication().getActiveScreen();
			UiApplication.getUiApplication().popScreen(screen);
			
		    UiApplication.getUiApplication().pushScreen(pagoWeb);
		    
		    //UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
		    
		} else if (field == cancelar){
			
			UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
		}

	}
}

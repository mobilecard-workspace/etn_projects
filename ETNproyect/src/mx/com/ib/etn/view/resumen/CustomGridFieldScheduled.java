package mx.com.ib.etn.view.resumen;

import net.rim.device.api.ui.component.NullField;
import mx.com.ib.etn.beans.dto.Router;
import mx.com.ib.etn.beans.dto.Scheduled;

public class CustomGridFieldScheduled extends CustomGridField{

	public CustomGridFieldScheduled(Router routerDepart, Router routerArrive, Scheduled scheduled){
		
		super();
		
		CustomRichTextField origen = new CustomRichTextField("Origen: ", routerDepart.getDescription());
		CustomRichTextField destino = new CustomRichTextField("Destino: ", routerArrive.getDescription());
		CustomRichTextField fechaSalida = new CustomRichTextField("Fecha de Salida: ", scheduled.getDateDepart());
		CustomRichTextField horaSalida = new CustomRichTextField("Hora de Salida: ", scheduled.getHourDepart());
		CustomRichTextField servicio = new CustomRichTextField("Servicio: ", scheduled.getServicio());
		add(origen);
		add(destino);
		add(fechaSalida);
		add(horaSalida);
		add(servicio);
		add(new NullField());
	}
}

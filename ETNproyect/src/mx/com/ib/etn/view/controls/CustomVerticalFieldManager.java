package mx.com.ib.etn.view.controls;


import net.rim.device.api.system.Display;
import net.rim.device.api.ui.container.HorizontalFieldManager;

public class CustomVerticalFieldManager extends HorizontalFieldManager{

	int width;
	
	public CustomVerticalFieldManager(){
		
		super();
		width = Display.getWidth();
	}
	
	public int getPreferredWidth() {
		return width;
	}
}

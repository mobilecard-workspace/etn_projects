package mx.com.ib.etn.view.seats;

import mx.com.ib.etn.beans.dto.Passenger;
import mx.com.ib.etn.model.creatorbeans.fromjs.JSBusDiagram;
import mx.com.ib.etn.model.data.DataKeeper;
import mx.com.ib.etn.model.data.DataTravel;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.container.GridFieldManager;

public class CustomGridFieldSeats extends GridFieldManager implements FieldChangeListener{

	
	private String flight;
	private String dateDepart;
	private JSBusDiagram jsBusDiagram;
	private EditField passengerFiled;
	private GridFieldManager fieldManager = null;

	private Passenger passenger;
	private GraphicButtonSeat seats[][];

	private DataKeeper dataKeeper = null;
	private DataTravel dataTravel = null;

	private boolean isDepartFlight;

	private boolean action = false;
	private boolean name = false;

	int i = 0;
	int j = 0;
	
	int widthSeat = 0;
	int heightSeat = 0;
	
	

	public CustomGridFieldSeats(int rows,int columns,long style, Passenger passenger, GraphicButtonSeat seats[][]){
		super(rows, columns, style);
		
		this.i = rows;
		this.j = columns;
		this.seats = seats;
		this.passenger = passenger;

		updateManager(seats);
	}
	
	
	public int getSizeWidth(){
		return widthSeat * j;
	}
	
	private void updateManager(GraphicButtonSeat seats[][]) {

		widthSeat = 0;
		heightSeat = 0;
		int columns = j;

		int widthDisplay = Display.getWidth();
		int heightDisplay = Display.getHeight();

		if ((i > 0) && (j > 0)) {

			widthSeat = seats[0][0].getPreferredWidth();
			heightSeat = seats[0][0].getPreferredHeight();
		}

		try{
			for (int a = 0; a < i; a++) {
				for (int b = 0; b < j; b++) {
					
					if (seats[a][b] != null){
						seats[a][b].setChangeListener(null);
						seats[a][b].setChangeListener(this);
						
						add(seats[a][b]);
					} else {
						//System.out.print("Es null");
					}
				}
			}
		} catch (Exception e){
			e.printStackTrace();
		}

		setColumnPadding(0);
		setRowPadding(0);
	}


	public void fieldChanged(Field field, int context) {
		
		if (field instanceof GraphicButtonSeat) {

			action = true;
			GraphicButtonSeat seat = (GraphicButtonSeat) field;
			seat.setChangeListener(null);
			allocateSeat(seat, passenger);
			seat.setChangeListener(this);
		}
		
	}
	
	private void allocateSeat(GraphicButtonSeat seat, Passenger passenger){

		if (!seat.isOccupied()) {
			Passenger passenger2 = seat.getPassenger();

			if (passenger2 == null) {

				int i = seats.length;
				int j = seats[0].length;

				for (int a = 0; a < i; a++) {
					for (int b = 0; b < j; b++) {

						Passenger passenger3 = seats[a][b].getPassenger();

						if (passenger3 != null) {

							if (passenger3.equals(passenger)) {
								seats[a][b].setPassenger(null);
								seats[a][b].isPermitted(false);
							}
						}
					}
				}
				
				passenger.setSeat(seat.getSeat());
				seat.setPassenger(passenger);
				seat.isPermitted(true);
			}
		}
	}
	
	
	public void reallocateSeat(Passenger passenger) {
		for (int a = 0; a < i; a++) {
			for (int b = 0; b < j; b++) {

				Passenger passenger2 = seats[a][b].getPassenger();

				if (passenger2 != null) {

					if (passenger2.equals(passenger)) {
						seats[a][b].setPassenger(null);
						seats[a][b].isPermitted(false);
					}
				}
			}
		}
	}
	
	public void clearSeats(){

		for (int a = 0; a < i; a++) {
			for (int b = 0; b < j; b++) {
				seats[a][b].setChangeListener(null);
			}
		}
	}
	
	public GraphicButtonSeat[][] clone(){

		GraphicButtonSeat[][] clone = new GraphicButtonSeat[i][j];
		
		for (int a = 0; a < i; a++) {
			for (int b = 0; b < j; b++) {
				
				clone[a][b].other = seats[a][b].other;
				clone[a][b].seatStatusBitMap = seats[a][b].seatStatusBitMap;
				clone[a][b].customerStatusBitMap = seats[a][b].customerStatusBitMap;
				clone[a][b].preferredHeight = seats[a][b].preferredHeight;
				clone[a][b].preferredWidth = seats[a][b].preferredWidth;
				clone[a][b].unFocusHeight = seats[a][b].unFocusHeight;
				clone[a][b].unFocusWidth = seats[a][b].unFocusWidth;
				clone[a][b].focusHeight = seats[a][b].focusHeight;
				clone[a][b].focusWidth = seats[a][b].focusWidth;
				clone[a][b].statusSeat = seats[a][b].statusSeat;
				clone[a][b].isUsedByCustomer = seats[a][b].isUsedByCustomer;
				clone[a][b].isOccupied = seats[a][b].isOccupied;
				clone[a][b].seat = seats[a][b].seat;
				clone[a][b].passenger = seats[a][b].passenger;
			}
		}
		
		return clone;
	}
	
	

}

package mx.com.ib.etn.view.etnbuy;

import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import mx.com.ib.etn.beans.beans.UserBean;
import mx.com.ib.etn.beans.dto.Router;
import mx.com.ib.etn.model.connection.Url;
import mx.com.ib.etn.model.connection.addcel.implementation.CreditInfoThread;
import mx.com.ib.etn.model.connection.addcel.implementation.InfoBoletos;
import mx.com.ib.etn.model.connection.base.ThreadHTTP;
import mx.com.ib.etn.model.creatorbeans.fromjs.JSRouterArrive;
import mx.com.ib.etn.model.creatorbeans.fromjs.JSRouterDepart;
import mx.com.ib.etn.model.data.DataKeeper;
import mx.com.ib.etn.utils.UtilColor;
import mx.com.ib.etn.utils.UtilDate;
import mx.com.ib.etn.utils.UtilIcon;
import mx.com.ib.etn.view.animated.SplashScreen;
import mx.com.ib.etn.view.boleto.ViewBoletos;
import mx.com.ib.etn.view.controls.AnuncioLabelField;
import mx.com.ib.etn.view.controls.CustomMainScreen;
import mx.com.ib.etn.view.controls.CustomSelectedButtonField;
import mx.com.ib.etn.view.controls.TitleOrangeRichTextField;
import mx.com.ib.etn.view.controls.Viewable;
import mx.com.ib.etn.view.controls.WhiteLabelField;
import mx.com.ib.etn.view.etnbuy.route.CustomButtonFieldManager;
import mx.com.ib.etn.view.popup.LoginPopUp;
import mx.com.ib.etn.view.scheduled.ViewScheduledDepart;
import net.rim.blackberry.api.browser.URLEncodedPostData;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.DateField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NumericChoiceField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import org.json.me.JSONException;
import org.json.me.JSONObject;

public class ViewETNBuy extends CustomMainScreen implements
		FieldChangeListener, Viewable {

	
	private CustomSelectedButtonField createRegistrer = null;
	private CustomSelectedButtonField getInfoBoletos = null;
	
	private CustomSelectedButtonField basicFlight = null;
	private CustomSelectedButtonField roundFlight = null;

	private ObjectChoiceField choiceFieldDepart = null;
	private ObjectChoiceField choiceFieldArrive = null;

	private DateField dateFieldDepart = null;
	private DateField dateFieldArrive = null;

	private NumericChoiceField choiceFieldAdult = null;
	private NumericChoiceField choiceFieldChild = null;
	private NumericChoiceField choiceFieldElderly = null;
	private NumericChoiceField choiceFieldStudent = null;
	private NumericChoiceField choiceFieldProfessor = null;

	private Bitmap encodedImage = null;
	private ButtonField ok = null;

	private boolean isBasicFlight = false;
	private boolean isFirst = true;

	private String depart = null;
	private String arrive = null;

	private String idDepart = null;
	private String idArrive = null;
	
	private long dateDepart = 0;
	private long dateArrive = 0;

	private int iDepart = 0;
	private int iArrive = 0;

	private int iAdults = 0;
	private int iChildren = 0;
	private int iElderlies = 0;
	private int iStudents = 0;
	private int iProfessors = 0;

	private Vector vectorDepart = null;
	private String arrayDepart[] = null;

	private Vector vectorArrive = null;
	private String arrayArrive[] = null;

	
	SplashScreen splashScreen = null;
	
	
	private CustomButtonFieldManager customFieldManager = null;
	private HorizontalFieldManager customFieldManager01 = null;

	private boolean showSplash = false;
	
	public ViewETNBuy(boolean showSplash) {

		super(false, null);
		splashScreen = SplashScreen.getInstance();

		this.showSplash = showSplash;
		
		configurationFlightView(isBasicFlight);
		loadDepartData();
	}


	private void loadDepartData() {

		URLEncodedPostData encodedPostData = new URLEncodedPostData(
				URLEncodedPostData.DEFAULT_CHARSET, false);

		ThreadHTTP threadHTTP = new ThreadHTTP();
		threadHTTP.setHttpConfigurator(Viewable.DEPART, this, encodedPostData);
		threadHTTP.start();
	}


	private void loadArriveData() {
		
		if (showSplash){
			splashScreen.start();
		}

		
		choiceFieldArrive.setChoices(null);

		int index = choiceFieldDepart.getSelectedIndex();

		if (index >= 0) {
			Router router = (Router) vectorDepart.elementAt(index);

			URLEncodedPostData encodedPostData = new URLEncodedPostData(
					URLEncodedPostData.DEFAULT_CHARSET, false);
			encodedPostData.append("origen", router.getKey());

			ThreadHTTP threadHTTP = new ThreadHTTP();
			threadHTTP.setHttpConfigurator(Viewable.ARRIVE, this,
					encodedPostData);
			threadHTTP.start();
		} else {
			Dialog.alert("No existe informaci�n en Origen");
		}
	}

	
	public void sendMessage(String message) {

		Dialog.alert(message);
	}
	
	
	public void setData(int request, JSONObject jsObject) {

		splashScreen.remove();
		
		if (jsObject != null){
			
			switch (request) {
			case Viewable.DEPART: {
				JSRouterDepart jsRouter = new JSRouterDepart();
				try {
					vectorDepart = jsRouter.getVectorRouters(jsObject,
							Viewable.DEPART);
					arrayDepart = jsRouter.getArrayRouters(jsObject,
							Viewable.DEPART);
					choiceFieldDepart.setChoices(arrayDepart);

					choiceFieldArrive.setChoices(null);
				} catch (JSONException e) {
					e.printStackTrace();
					Dialog.alert("Error al interpretar los datos");
				}
			}
				break;

			case Viewable.ARRIVE: {
				JSRouterArrive jsRouter = new JSRouterArrive();
				try {
					vectorArrive = jsRouter.getVectorRouters(jsObject,
							Viewable.ARRIVE);
					arrayArrive = jsRouter.getArrayRouters(jsObject,
							Viewable.ARRIVE);
					choiceFieldArrive.setChoices(arrayArrive);
				} catch (JSONException e) {
					e.printStackTrace();
					Dialog.alert("Error al interpretar los datos");
				}
			}
				break;

			default:
				break;
			}
		} else {
			Dialog.alert("Informaci�n no recibida.");
		}
	}

	
	public void configurationFlightView(boolean isBasicFlight) {

		deleteAll();
		add(new LabelField(""));
		BitmapField bitmapField = UtilIcon.getTitle();
		add(bitmapField);
		add(new LabelField(""));
		TitleOrangeRichTextField orangeRichTextField01 = new TitleOrangeRichTextField(
				"Datos de viaje");

		add(orangeRichTextField01);
		add(new LabelField(""));

		createRegistrer = new CustomSelectedButtonField(
				"Registrarse", Field.FIELD_VCENTER);
		createRegistrer.setChangeListener(this);
		getInfoBoletos = new CustomSelectedButtonField(
				"Boletos Adquiridos", Field.FIELD_VCENTER);
		getInfoBoletos.setChangeListener(this);
		customFieldManager01 = new HorizontalFieldManager();
		customFieldManager01.add(createRegistrer);
		customFieldManager01.add(getInfoBoletos);
		add(customFieldManager01);

		CustomSelectedButtonField basicFlight = new CustomSelectedButtonField(
				"Viaje Sencillo", Field.FIELD_VCENTER);
		CustomSelectedButtonField roundFlight = new CustomSelectedButtonField(
				"Viaje Redondo", Field.FIELD_VCENTER);

		basicFlight.setSelected(this.isBasicFlight);
		roundFlight.setSelected(!this.isBasicFlight);

		customFieldManager = new CustomButtonFieldManager(basicFlight,
				roundFlight);
		basicFlight.setChangeListener(this);
		roundFlight.setChangeListener(this);

		customFieldManager.add(roundFlight);
		customFieldManager.add(basicFlight);

		add(customFieldManager);

		String choices[] = null;

		choiceFieldDepart = new ObjectChoiceField("", arrayDepart, iDepart);
		choiceFieldDepart.setChangeListener(this);
		WhiteLabelField textFieldDepart = new WhiteLabelField("Origen");

		HorizontalFieldManager fieldManager1 = new HorizontalFieldManager();
		fieldManager1.add(textFieldDepart);
		fieldManager1.add(choiceFieldDepart);
		add(fieldManager1);

		choiceFieldArrive = new ObjectChoiceField("", choices, iArrive);
		choiceFieldArrive.setChangeListener(this);
		WhiteLabelField textFieldArrive = new WhiteLabelField("Destino");
		HorizontalFieldManager fieldManager2 = new HorizontalFieldManager();
		fieldManager2.add(textFieldArrive);
		fieldManager2.add(choiceFieldArrive);
		add(fieldManager2);

		dateFieldDepart = new DateField("", System.currentTimeMillis(),
				DateField.DATE) {
			protected void paint(Graphics graphics) {
				graphics.setColor(UtilColor.TITLE_STRING_BLACK);
				super.paint(graphics);
			}
		};

		WhiteLabelField labelDateDepart = new WhiteLabelField("Fecha de salida");
		HorizontalFieldManager fieldManager3 = new HorizontalFieldManager();
		fieldManager3.add(labelDateDepart);
		fieldManager3.add(dateFieldDepart);
		add(fieldManager3);

		dateFieldArrive = new DateField("", System.currentTimeMillis(),
				DateField.DATE) {
			protected void paint(Graphics graphics) {
				graphics.setColor(UtilColor.TITLE_STRING_BLACK);
				super.paint(graphics);
			}
		};
		WhiteLabelField labelDateArrive = new WhiteLabelField(
				"Fecha de Regreso");
		HorizontalFieldManager fieldManager4 = new HorizontalFieldManager();
		fieldManager4.add(labelDateArrive);
		fieldManager4.add(dateFieldArrive);
		if (!isBasicFlight) {
			add(fieldManager4);
		}

		if (dateDepart > 0) {
			dateFieldDepart.setDate(dateDepart);
		}

		if (dateArrive > 0) {
			dateFieldArrive.setDate(dateArrive);
		}

		TitleOrangeRichTextField orangeRichTextField02 = new TitleOrangeRichTextField(
				"A�adir pasajeros");
		add(orangeRichTextField02);

		choiceFieldAdult = addChoiceField("Adulto", iAdults);
		choiceFieldChild = addChoiceField("Menor", iChildren);
		choiceFieldElderly = addChoiceField("Senectud", iElderlies);
		choiceFieldStudent = addChoiceField("Estudiante", iStudents);
		choiceFieldProfessor = addChoiceField("Profesor", iProfessors);

		ok = new ButtonField("Elegir Horario");
		ok.setChangeListener(this);

		add(ok);
		add(new AnuncioLabelField("�Dudas? �Problemas? Ll�manos al: 5540-3124 o por correo electr�nico a soporte_etn@addcel.com"));
		
	}

	private NumericChoiceField addChoiceField(String title, int number) {
		int iStartAt = 0;
		int iEndAt = 19;
		int iIncrement = 1;

		WhiteLabelField labelField = new WhiteLabelField(title);
		NumericChoiceField choiceField = new NumericChoiceField("", iStartAt,
				iEndAt, iIncrement, number);

		HorizontalFieldManager fieldManager = new HorizontalFieldManager();
		fieldManager.add(labelField);
		fieldManager.add(choiceField);
		add(fieldManager);

		return choiceField;
	}

	private void keepData() {

		dateDepart = dateFieldDepart.getDate();
		dateArrive = dateFieldArrive.getDate();

		iDepart = choiceFieldDepart.getSelectedIndex();
		iArrive = choiceFieldArrive.getSelectedIndex();

		DataKeeper dataKeeper = DataKeeper.getInstance();

		if (iDepart >= 0) {
			Router router = (Router) vectorDepart.elementAt(iDepart);
			dataKeeper.setRouterDepart(router);
			depart = router.getDescription();
			idDepart = router.getKey();
		} else {
			depart = null;
			dataKeeper.setRouterDepart(null);
		}

		if (iArrive >= 0) {
			Router router = (Router) vectorArrive.elementAt(iArrive);
			dataKeeper.setRouterArrive(router);
			arrive = router.getDescription();
			idArrive = router.getKey();
		} else {
			arrive = null;
			dataKeeper.setRouterArrive(null);
		}

		iAdults = choiceFieldAdult.getSelectedIndex();
		iChildren = choiceFieldChild.getSelectedIndex();
		iElderlies = choiceFieldElderly.getSelectedIndex();
		iStudents = choiceFieldStudent.getSelectedIndex();
		iProfessors = choiceFieldProfessor.getSelectedIndex();
	}

	private String getStringForChoice(int index, ObjectChoiceField choiceField) {
		String choice = (String) choiceField.getChoice(index);
		return choice;
	}

	public void fieldChanged(Field arg0, int arg1) {

		if (arg0 == (ObjectChoiceField) choiceFieldDepart) {

			if (isFirst) {
				isFirst = false;
				loadArriveData();
			} else {

				if (ObjectChoiceField.CONTEXT_CHANGE_OPTION == arg1) {

					loadArriveData();
				}
			}

		} else if (arg0 == (ButtonField) ok) {

			keepData();

			Calendar calendar = Calendar.getInstance();

			long dateDepart01l = dateFieldDepart.getDate();
			long dateArrive01l = dateFieldArrive.getDate();

			Date dateDepart01 = new Date(dateDepart01l);
			Date dateArrive02 = new Date(dateArrive01l);

			calendar.setTime(dateDepart01);
			Date a = calendar.getTime();

			calendar.setTime(dateArrive02);
			Date b = calendar.getTime();

			boolean value = false;

			if (isBasicFlight) {
				value = true;
			} else {
				value = UtilDate.after(a, b);
			}


			if (value) {

				if (verifyNumPassengers()) {
					DataKeeper dataKeeper = DataKeeper.getInstance();
					dataKeeper.setDataTravel(new Date(dateDepart), new Date(
							dateArrive), depart, arrive, idDepart, idArrive, iAdults, iChildren,
							iElderlies, iStudents, iProfessors, isBasicFlight);

					if (verifyChild()){
						
						if(verify19()){
							UiApplication.getUiApplication().pushScreen(
									new ViewScheduledDepart(true, isBasicFlight));
						} else {
							Dialog.alert("Se puede comprar hasta 19 boletos");
						}

					} else {
						Dialog.alert("Un ni�o debe ir acompa�ado de un adulto");
					}
					

				} else {

					Dialog.alert("Selecciona al menos un pasajero.");
				}

			} else {

				Dialog.alert("La Fecha de regreso es anterior a la fecha de salida");
			}

		} else if (arg0 == (CustomSelectedButtonField) customFieldManager
				.getBasicFlight()) {
			roundFlight = customFieldManager.getRoundFlight();
			roundFlight.setSelected(false);
			isBasicFlight = true;
			keepData();
			configurationFlightView(isBasicFlight);
			loadArriveData();
		} else if (arg0 == (CustomSelectedButtonField) customFieldManager
				.getRoundFlight()) {
			basicFlight = customFieldManager.getBasicFlight();
			basicFlight.setSelected(false);
			isBasicFlight = false;
			keepData();
			configurationFlightView(isBasicFlight);
			loadArriveData();
		} else if (arg0 == getInfoBoletos){
			
			//UserBean.idLogin = "1362692560880";
			if (UserBean.idLogin == null){
				UiApplication.getUiApplication().pushScreen(new LoginPopUp(this));
			} else {
				
				URLEncodedPostData encodedPostData = new URLEncodedPostData(
						URLEncodedPostData.DEFAULT_CHARSET, false);
				
				encodedPostData.append("modulo", "C");
				//encodedPostData.append("idBusqueda", UserBean.idLogin);
				encodedPostData.append("idBusqueda", UserBean.idLogin);
				
				
				String encoded = encodedPostData.toString();
				
				ViewBoletos boletos = new ViewBoletos();
				
				InfoBoletos infoBoletos = new InfoBoletos(boletos, Url.URL_INFO_BOLETOS, encoded);
				infoBoletos.run();
				
				UiApplication.getUiApplication().pushScreen(boletos);
			}
		} else if (arg0 == createRegistrer){

			CreditInfoThread infoThread = new CreditInfoThread("proveedores", null, Url.URL_GET_PROVIDERS, "");
			infoThread.run();
		}
	}
	
	
	private boolean verify19(){

		return ((choiceFieldAdult.getSelectedIndex() + 
		choiceFieldChild.getSelectedIndex() + 
		choiceFieldElderly.getSelectedIndex() + 
		choiceFieldStudent.getSelectedIndex() + 
		choiceFieldProfessor.getSelectedIndex()) <= 19);
	}
	
	
	private boolean verifyChild(){
		
		return !((choiceFieldChild.getSelectedIndex() > 0 ) && (
		(choiceFieldAdult.getSelectedIndex() == 0)&&
		(choiceFieldElderly.getSelectedIndex() == 0)&& 
		(choiceFieldProfessor.getSelectedIndex() == 0))
		);
	}

	private boolean verifyNumPassengers() {

		boolean value = false;

		if ((iAdults > 0) || (iChildren > 0) || (iElderlies > 0)
				|| (iStudents > 0) || (iProfessors > 0)

		) {
			value = true;
		} else {
			value = false;
		}

		return value;
	}
}

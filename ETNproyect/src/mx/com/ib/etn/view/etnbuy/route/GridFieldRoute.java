package mx.com.ib.etn.view.etnbuy.route;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.container.HorizontalFieldManager;

public class GridFieldRoute extends HorizontalFieldManager {

	private int width = 0;
	
	
	public GridFieldRoute(){
		width = Display.getWidth();
	}
	
	public int getPreferredWidth() {
		return width;
	}
}

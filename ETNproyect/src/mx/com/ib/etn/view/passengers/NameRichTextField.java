package mx.com.ib.etn.view.passengers;

import mx.com.ib.etn.beans.dto.Passenger;
import mx.com.ib.etn.utils.UtilColor;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;

public class NameRichTextField extends RichTextField{

	private Passenger passenger;
	private int width = 0;
	
	public NameRichTextField(Passenger passenger) {

		super("", RichTextField.NON_FOCUSABLE | RichTextField.USE_ALL_HEIGHT);
		this.passenger = passenger;
		width = Display.getWidth();
	}

	public void paint(Graphics graphics) {
		
		graphics.setColor(UtilColor.ELEMENT_STRING_TITLE);
		graphics.drawText(passenger.getName(), 0, 0, DrawStyle.LEFT, width);

		super.paint(graphics);
	}
}

package mx.com.ib.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import net.rim.device.api.crypto.AESEncryptorEngine;
import net.rim.device.api.crypto.AESKey;
import net.rim.device.api.crypto.BlockEncryptor;
import net.rim.device.api.crypto.CBCEncryptorEngine;
import net.rim.device.api.crypto.CryptoTokenException;
import net.rim.device.api.crypto.CryptoUnsupportedOperationException;
import net.rim.device.api.crypto.InitializationVector;
import net.rim.device.api.crypto.PKCS5FormatterEngine;

public class EncryptClass {

	public static String EncryptaAESUNO(String plainText)
			throws CryptoTokenException, CryptoUnsupportedOperationException,
			IOException {
		byte plainbyte[] = plainText.getBytes();
		byte KeyByte[] = { (byte) 141, (byte) 75, (byte) 143, (byte) 19,
				(byte) 123, (byte) 247, (byte) 127, (byte) 184, (byte) 69,
				(byte) 192, (byte) 159, (byte) 144, (byte) 163, (byte) 175,
				(byte) 31, (byte) 43 };
		// Se crea el AES Key con la llave de UNO
		AESKey key = new AESKey(KeyByte);

		// Inicializa el vector con la misma llave del AES
		InitializationVector iv = new InitializationVector(KeyByte);

		// Se genera CBC Engine, necesario para el vector IV y despues el
		// formater para aguantar los 256 bits
		CBCEncryptorEngine CBCengine = new CBCEncryptorEngine(
				new AESEncryptorEngine(key), iv);
		PKCS5FormatterEngine pk5formatter = new PKCS5FormatterEngine(CBCengine);

		// Se crea el outputstream para alojar la cadena encriptada, como es
		// stream aguanta los 256 del formatter
		ByteArrayOutputStream encryptoutput = new ByteArrayOutputStream();

		// Se genera el encriptor con el formatter y se indica el stream de
		// salida
		BlockEncryptor encryptor = new BlockEncryptor(pk5formatter,
				encryptoutput);

		// Se pasa al encriptor el password en claro (los bytes)
		encryptor.write(plainbyte, 0, plainbyte.length);
		encryptor.close();
		byte[] output = encryptoutput.toByteArray();

		// El resultado es un byte, se debe pasar por el base64
		return new String(base64Encode(output));
	}

	private static char[] map1 = new char[64];
	static {
		int i = 0;
		for (char c = 'A'; c <= 'Z'; c++) {
			map1[i++] = c;
		}
		for (char c = 'a'; c <= 'z'; c++) {
			map1[i++] = c;
		}
		for (char c = '0'; c <= '9'; c++) {
			map1[i++] = c;
		}
		map1[i++] = '+';
		map1[i++] = '/';
	}

	public static String base64Encode(byte[] in) {
		int iLen = in.length;
		int oDataLen = (iLen * 4 + 2) / 3;// output length without padding
		int oLen = ((iLen + 2) / 3) * 4;// output length including padding
		char[] out = new char[oLen];
		int ip = 0;
		int op = 0;
		int i0;
		int i1;
		int i2;
		int o0;
		int o1;
		int o2;
		int o3;
		while (ip < iLen) {
			i0 = in[ip++] & 0xff;
			i1 = ip < iLen ? in[ip++] & 0xff : 0;
			i2 = ip < iLen ? in[ip++] & 0xff : 0;
			o0 = i0 >>> 2;
			o1 = ((i0 & 3) << 4) | (i1 >>> 4);
			o2 = ((i1 & 0xf) << 2) | (i2 >>> 6);
			o3 = i2 & 0x3F;
			out[op++] = map1[o0];
			out[op++] = map1[o1];
			out[op] = op < oDataLen ? map1[o2] : '=';
			op++;
			out[op] = op < oDataLen ? map1[o3] : '=';
			op++;
		}
		return new String(out);
	}
}

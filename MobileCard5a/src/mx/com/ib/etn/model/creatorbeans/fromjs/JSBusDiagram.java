package mx.com.ib.etn.model.creatorbeans.fromjs;

import mx.com.ib.etn.view.seats.GraphicButtonSeat;
import net.rim.device.api.ui.container.GridFieldManager;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

public class JSBusDiagram {

	
	private int numFilasDiagram;
	private int numErrorDiagram;
	private String descErrorDiagram;
	private int numAsientosDiagram;

	private String codigoError;
	private String descripcionError;

	
	public GraphicButtonSeat[][] createDiagram(JSONObject jsObject) throws JSONException {

		JSONArray jsonArray = jsObject.getJSONArray("diagramaAutobus");

		String snumFilas = (String) jsObject.get("numFilas");

		numFilasDiagram = Integer.parseInt(snumFilas);
		numErrorDiagram = jsObject.getInt("numError");
		descErrorDiagram = (String) jsObject.get("descError");
		numAsientosDiagram = jsObject.getInt("numAsientos");
		
		// Calculos para crear el array
		
		int i = numFilasDiagram - 1;
		int j = (numAsientosDiagram/i) + 1;
		
		String arrayArrive[] = null;
		
		GraphicButtonSeat seats[][] = new  GraphicButtonSeat[i][j];
		
		int length = jsonArray.length();

		int c = 0;
		
		for (int a = 0; a < i; a++) {

			for (int b = 0; b < j; b++) {
				
				String info = (String)jsonArray.get(c);
				
				System.out.println(info);
				
				c++;
				
				int statusSeat = GraphicButtonSeat.SEAT_NOT_OCCUPIED;
				
				if (info.indexOf("T")>=0){
					statusSeat = GraphicButtonSeat.SEAT_NOT_OCCUPIED_TV;
				} else if (info.indexOf("00")>=0){
					statusSeat = GraphicButtonSeat.SEAT_AISLE;
				}

				GraphicButtonSeat buttonSeat = new GraphicButtonSeat(statusSeat);
				
				buttonSeat.setSeat(info);
				
				seats[a][b] = buttonSeat;
			}
		}
		
		return seats;
	}


	public  GraphicButtonSeat[][] setOccupancy(JSONObject jsObject, GraphicButtonSeat seats[][]) throws JSONException {

		codigoError = (String) jsObject.get("codigoError");
		descripcionError = (String) jsObject.get("descripcionError");

		JSONArray jsonArray = jsObject.getJSONArray("asientos");
		int length = jsonArray.length();
/*
		for (int i = 0; i < length; i++){
			String status = (String) jsonArray.get(i);
			System.out.println(status);
		}
*/
		int i = seats.length;
		int j = seats[0].length;

		int c = 0;
		
		for(int a = 0; a < i; a++){
			for(int b = 0; b < j; b++){
				
				if (!seats[a][b].isSeatAisle()){
					String status = (String) jsonArray.get(c);
					c++;
					System.out.println(status + " " + c);
					
					if (status.equals("1")){
						
						seats[a][b].setOccupied(true);
					}
				}
				seats[a][b].setBitmap();
			}
		}
	
		return seats;
	}
}



/*



		for (int i = 0; i < length; i++) {

			System.out.println((String) jsonArray.get(i));
		}




int SEAT_OCCUPIED = 0;
int SEAT_NOT_OCCUPIED = 1;
int SEAT_OCCUPIED_TV = 2;
int SEAT_NOT_OCCUPIED_TV = 3;
int SEAT_AISLE  = 4;

c -> cafeter�a
H -> ba�o hombres
M -> ba�o mujeres
P -> escalera
T -> television
U -> ba�o unisex

"01  ","00  ","02  ","03T ",
*/
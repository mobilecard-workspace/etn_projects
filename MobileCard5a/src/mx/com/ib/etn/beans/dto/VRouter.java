package mx.com.ib.etn.beans.dto;

public class VRouter {

	private String office;
	private String dateDepart;
	private String hourDepart;
	
	private String stringError;
	private String numberError;
	
	
	public String getOffice() {
		return office;
	}
	public void setOffice(String office) {
		this.office = office;
	}
	public String getDateDepart() {
		return dateDepart;
	}
	public void setDateDepart(String dateDepart) {
		this.dateDepart = dateDepart;
	}
	public String getHourDepart() {
		return hourDepart;
	}
	public void setHourDepart(String hourDepart) {
		this.hourDepart = hourDepart;
	}
	public String getStringError() {
		return stringError;
	}
	public void setStringError(String stringError) {
		this.stringError = stringError;
	}
	public String getNumberError() {
		return numberError;
	}
	public void setNumberError(String numberError) {
		this.numberError = numberError;
	}
}

package mx.com.ib.etn.beans.dto;


public class Scheduled {
	
	private String flight;
	private String companyFlight;
	private String hourDepart;
	private String hourArrive;
	
	private String dateDepart;
	private String dateArrive;
	private String charge;
	
	private String chargeAdults;
	private String chargeChildren;
	private String chargeElderlies;
	private String chargeStudents;
	private String chargeProfessors;


	public String getDateDepartToString() {

		String day = dateDepart.substring(0, 2);
		String month = dateDepart.substring(3, 5);
		String year = dateDepart.substring(6, 10);

		return day + month + year;
	}

	
	public String getHourDepartToString() {

		String hour = hourDepart.substring(0, 2);
		String minute = hourDepart.substring(3, 5);

		return hour + minute + "00";
	}
	
	
	
	
	public String getChargeAdults() {
		return chargeAdults;
	}
	public void setChargeAdults(String chargeAdults) {
		this.chargeAdults = chargeAdults;
	}
	public String getChargeChildren() {
		return chargeChildren;
	}
	public void setChargeChildren(String chargeChildren) {
		this.chargeChildren = chargeChildren;
	}
	public String getChargeElderlies() {
		return chargeElderlies;
	}
	public void setChargeElderlies(String chargeElderlies) {
		this.chargeElderlies = chargeElderlies;
	}
	public String getChargeStudents() {
		return chargeStudents;
	}
	public void setChargeStudents(String chargeStudents) {
		this.chargeStudents = chargeStudents;
	}
	public String getChargeProfessors() {
		return chargeProfessors;
	}
	public void setChargeProfessors(String chargeProfessors) {
		this.chargeProfessors = chargeProfessors;
	}
	public String getFlight() {
		return flight;
	}
	public void setFlight(String flight) {
		this.flight = flight;
	}
	public String getCompanyFlight() {
		return companyFlight;
	}
	public void setCompanyFlight(String companyFlight) {
		this.companyFlight = companyFlight;
	}
	public String getHourDepart() {
		return hourDepart;
	}
	public void setHourDepart(String hourDepart) {
		this.hourDepart = hourDepart;
	}
	public String getHourArrive() {
		return hourArrive;
	}
	public void setHourArrive(String hourArrive) {
		this.hourArrive = hourArrive;
	}
	public String getDateDepart() {
		return dateDepart;
	}
	public void setDateDepart(String dateDepart) {
		this.dateDepart = dateDepart;
	}
	public String getDateArrive() {
		return dateArrive;
	}
	public void setDateArrive(String dateArrive) {
		this.dateArrive = dateArrive;
	}
	public String getCharge() {
		return charge;
	}
	public void setCharge(String charge) {
		this.charge = charge;
	}
}

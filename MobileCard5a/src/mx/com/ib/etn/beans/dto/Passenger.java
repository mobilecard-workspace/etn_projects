package mx.com.ib.etn.beans.dto;

public class Passenger {

	final public static String KIND_ADULT = "Adulto"; 
	final public static String KIND_CHILDREN = "Ni�o";
	final public static String KIND_ELDERL = "Insen";
	final public static String KIND_STUDENT = "Estudiante";
	final public static String KIND_PROFESSOR = "Maestro";

	private String kind;
	private String charge;
	private String name = "Sin nombre";
	private String seat;
	private boolean named = false;
	
	public String getKind() {
		return kind;
	}
	public void setKind(String kind) {
		this.kind = kind;
	}


	public String getCharge() {
		return charge;
	}
	public void setCharge(String charge) {
		this.charge = charge;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSeat() {
		return seat;
	}
	public void setSeat(String seat) {
		this.seat = seat;
	}
	public boolean isNamed() {
		return named;
	}
	public void setNamed(boolean named) {
		this.named = named;
	}
}

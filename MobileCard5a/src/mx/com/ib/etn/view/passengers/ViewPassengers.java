package mx.com.ib.etn.view.passengers;

import java.util.Vector;

import mx.com.ib.etn.beans.dto.Passenger;
import mx.com.ib.etn.beans.dto.Scheduled;
import mx.com.ib.etn.model.data.DataKeeper;
import mx.com.ib.etn.model.data.DataTravel;
import mx.com.ib.etn.model.data.Payer;
import mx.com.ib.etn.utils.UtilColor;
import mx.com.ib.etn.utils.UtilIcon;
import mx.com.ib.etn.view.animated.SplashScreen;
import mx.com.ib.etn.view.controls.CustomMainScreen;
import mx.com.ib.etn.view.controls.TitleOrangeRichTextField;
import mx.com.ib.etn.view.controls.Viewable;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;

import org.json.me.JSONObject;


/*
	Reune la informacion de los pasajeros, menos el nombre
	y crea un gridfielmanager que los mostrara.
*/

public class ViewPassengers extends CustomMainScreen implements FieldChangeListener,
Viewable  {

	
	private DataKeeper dataKeeper;
	private DataTravel dataTravel;
	private Scheduled scheduled; 
			
	private Vector passengers;
	private ButtonField accept;
	private ButtonField acceptBasic;
	private boolean isDepartFlight;
	private boolean isBasicFlight;
	private SplashScreen splashScreen;
	
	public ViewPassengers(boolean isDepartFlight, boolean isBasicFlight){

		super();

		this.isDepartFlight = isDepartFlight;
		this.isBasicFlight = isBasicFlight;
		
		BitmapField bitmapField = UtilIcon.getTitle();
		add(bitmapField);

		TitleOrangeRichTextField orangeRichTextField = new TitleOrangeRichTextField("Paso 4 - Selección de pasajeros y asientos");
		add(orangeRichTextField);
		add(new LabelField(""));

		dataKeeper = DataKeeper.getInstance();
		dataTravel = dataKeeper.getDataTravel();
		
		if (isDepartFlight){
			scheduled = dataKeeper.getScheduledDepart();
			passengers = dataKeeper.getPassengersDepart();
			if (passengers == null){
				passengers = new Vector();
				setPassengers();
				dataKeeper.setPassengersDepart(passengers);
			}
		} else {
			scheduled = dataKeeper.getScheduledArrive();
			passengers = dataKeeper.getPassengersArrive();
			if (passengers == null){
				passengers = new Vector();
				setPassengers();
				dataKeeper.setPassengersArrive(passengers);
			}
		}

		addPassengers(passengers);

		
		if (isBasicFlight){
			acceptBasic = new ButtonField("Hacer pago", ButtonField.CONSUME_CLICK);
			acceptBasic.setChangeListener(this);
			add(acceptBasic);
		} else {
			accept = new ButtonField("Continuar", ButtonField.CONSUME_CLICK);
			accept.setChangeListener(this);
			add(accept);
		}
	}

	
	public void setPassengers(){
		int iAdults = dataTravel.getiAdults();
		int iChildren = dataTravel.getiChildren();
		int iElderlies = dataTravel.getiElderlies();
		int iStudents = dataTravel.getiStudents();
		int iProfessors = dataTravel.getiProfessors();

		addPassenger(iAdults, Passenger.KIND_ADULT, scheduled.getChargeAdults());
		addPassenger(iChildren, Passenger.KIND_CHILDREN, scheduled.getChargeChildren());
		addPassenger(iElderlies, Passenger.KIND_ELDERL, scheduled.getChargeElderlies());
		addPassenger(iStudents, Passenger.KIND_STUDENT, scheduled.getChargeStudents());
		addPassenger(iProfessors, Passenger.KIND_PROFESSOR, scheduled.getChargeProfessors());
	}
	
	
	
	private void addPassenger(int numPassenger, String kindPassenger, String charger){

		if (numPassenger > 0){
			for(int i = 0; i < numPassenger; i++){
				Passenger passenger = new Passenger();
				passenger.setKind(kindPassenger);
				passenger.setCharge(charger);
				passengers.addElement(passenger);
			}
		}
	}
	
	
	private void addPassengers(Vector passengers){
		
		int size = passengers.size();
		
		for(int index = 0; index < size; index++){
			
			Passenger passenger = (Passenger)passengers.elementAt(index);

			CustomGridFieldListPassengers fieldListPassengers = null; 
			fieldListPassengers = new CustomGridFieldListPassengers(isDepartFlight, isBasicFlight, passenger);
			add(fieldListPassengers);
		}
	}
	
	
	public void fieldChanged(Field field, int context) {
		
		if (checkUsed()){
			if (field == (ButtonField)accept){
				UiApplication.getUiApplication().popScreen(this);
			} else if (field == (ButtonField)acceptBasic){

				splashScreen = SplashScreen.getInstance();
				splashScreen.start();
				Payer payer = new Payer();
				payer.toPay(isBasicFlight, isBasicFlight, this);
			}
			
		} else {
			
			Dialog.alert("Aun faltan pasajeros por asignar");
		}
	}

	
	private boolean checkUsed(){
		
		boolean value = true;
		
		int fields = getFieldCount();
		
		for (int index = 0; index < fields; index++){
			
			Field field = getField(index);
			
			if (field instanceof CustomGridFieldListPassengers){
				
				CustomGridFieldListPassengers passengerView = (CustomGridFieldListPassengers)field;
				
				Passenger passenger = passengerView.getPassenger();

				if (
						(!passenger.isNamed()) &&
						(passenger.getSeat() == null) &&
						(passenger.getSeat().length() == 0)
					){
					value = false;
				}
			}
		}
		
		return value;
	}

	
	public void sendMessage(String message) {

		Dialog.alert(message);
	}
	
	
	public void setData(int request, JSONObject jsObject) {

		splashScreen.remove();
		
		if (jsObject == null){
			final int OK = 0;
			int answer = 0;
			int DEFAULT = OK;
			answer = Dialog.ask("No se pudo interpretar la respuesta, por favor consulte su saldo.", new String[] {"Salir"}, new int[] {OK}, DEFAULT);
			
			switch (answer) {
			case OK:
				//UiApplication.getUiApplication().popScreen(this);
				break;
			}
		}
		
		if (isBasicFlight){
			
			final int OK = 0;
			int answer = 0;
			int DEFAULT = OK;
			answer = Dialog.ask("Transacción exitosa", new String[] {"Aceptar"}, new int[] {OK}, DEFAULT);
			
			switch (answer) {
			case OK:
				//UiApplication.getUiApplication().popScreen(this);
				break;
			}
		}
	}


	public boolean onClose(){

		
		deleteAll();
		
		if (isBasicFlight){

			dataKeeper.setPassengersDepart(null);
			dataKeeper.setPassengersArrive(null);
			dataKeeper.setSeatsDepart(null);
			dataKeeper.setSeatsArrive(null);
		}

		return super.onClose();
	}
}

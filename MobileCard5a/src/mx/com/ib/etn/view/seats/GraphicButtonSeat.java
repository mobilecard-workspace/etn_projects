package mx.com.ib.etn.view.seats;

import mx.com.ib.etn.beans.dto.Passenger;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.component.ButtonField;

public class GraphicButtonSeat extends Field{

	final public static int SEAT_NOT_OCCUPIED = 1;
	final public static int SEAT_NOT_OCCUPIED_TV = 2;
	
	final public static int SEAT_AISLE  = 3;
	
	final public static int SEAT_OCCUPIED = 11;
	final public static int SEAT_OCCUPIED_TV = 12;

	final public static int KIND_ADULT = 6;
	final public static int KIND_CHILD = 7;
	final public static int KIND_ELDERLY = 8;
	final public static int KIND_STUDENT = 9;
	final public static int KIND_PROFESOR = 10;

	

	private Bitmap seatStatusBitMap;
	private Bitmap customerStatusBitMap;
	
	private int preferredHeight;
	private int preferredWidth;
	
	private int unFocusHeight;
	private int unFocusWidth;
	
	private int focusHeight;
	private int focusWidth;


	
	private int statusSeat = 0;
	private boolean isUsedByCustomer = false;


	private boolean isOccupied = false;
	private String seat;
	private Passenger passenger;
	

	public boolean isUsedByCustomer() {
		return isUsedByCustomer;
	}


	public void setUsedByCustomer(boolean isUsedByCustomer) {
		this.isUsedByCustomer = isUsedByCustomer;
	}
	
	public void setUsedByCustomerInvalidate(boolean isUsedByCustomer) {
		this.isUsedByCustomer = isUsedByCustomer;
		invalidate();
	}
	
	
	public boolean isSamePassenger(Passenger passenger){
		
		return passenger.equals(passenger);
	}
	
	
	public GraphicButtonSeat(int statusSeat){
		super(ButtonField.CONSUME_CLICK);
		this.statusSeat = statusSeat;
	}
	
	public boolean isOccupied() {
		return isOccupied;
	}

	public void setOccupied(boolean isOccupied) {
		this.isOccupied = isOccupied;
	}

	public boolean isSeatAisle(){
		return GraphicButtonSeat.SEAT_AISLE == statusSeat;
	}

	public void setBitmap(){

		if ((isOccupied)&&(statusSeat != GraphicButtonSeat.SEAT_AISLE)){

			statusSeat += 10;
		}
		
		switch (statusSeat) {
		
		case GraphicButtonSeat.SEAT_OCCUPIED:
			seatStatusBitMap = Bitmap.getBitmapResource("asiento_ocupado.png");
			break;

		case GraphicButtonSeat.SEAT_NOT_OCCUPIED:
			seatStatusBitMap = Bitmap.getBitmapResource("asiento_desocupado.png");
			break;
			
		case GraphicButtonSeat.SEAT_OCCUPIED_TV:
			seatStatusBitMap = Bitmap.getBitmapResource("asiento_ocupado_tele.png");
			break;

		case GraphicButtonSeat.SEAT_NOT_OCCUPIED_TV:
			seatStatusBitMap = Bitmap.getBitmapResource("asiento_desocupado_tele.png");
			break;

		case GraphicButtonSeat.SEAT_AISLE:
			seatStatusBitMap = Bitmap.getBitmapResource("asiento_pasillo.png");
			break;
		}
		
		if (seatStatusBitMap == null){
			
			seatStatusBitMap = Bitmap.getBitmapResource("asiento_pasillo.png");
		}
		
		if (statusSeat == GraphicButtonSeat.SEAT_NOT_OCCUPIED_TV){
			
			customerStatusBitMap = Bitmap.getBitmapResource("asiento_ocupado_tele_cliente.png");
		} else {
			customerStatusBitMap = Bitmap.getBitmapResource("asiento_ocupado_cliente.png");
		}
		


		this.customerStatusBitMap = customerStatusBitMap;
		this.seatStatusBitMap = seatStatusBitMap;

		unFocusHeight = seatStatusBitMap.getHeight();
		unFocusWidth = seatStatusBitMap.getWidth();

		focusHeight = customerStatusBitMap.getHeight();
		focusWidth = customerStatusBitMap.getWidth();

		preferredHeight = Math.max(focusHeight, unFocusHeight);
		preferredWidth = Math.max(focusWidth, unFocusWidth);
	}
	
	public int getPreferredHeight(){
		return preferredHeight;
	}

	public int getPreferredWidth(){
		return preferredWidth;
	}

	protected void layout(int width, int height){
		setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	protected void paint(Graphics graphics){

		graphics.clear();
		if (isUsedByCustomer){
			graphics.drawBitmap(0, 0, focusWidth, focusHeight, customerStatusBitMap, 0, 0);
		} else {
			graphics.drawBitmap(0, 0, unFocusWidth, unFocusHeight, seatStatusBitMap, 0, 0);
		}
		
		if (isFocus()){
			graphics.setColor(Color.WHITE);
			//graphics.setGlobalAlpha(60);
			graphics.setGlobalAlpha(110);
			graphics.fillRoundRect(0, 0, preferredWidth, preferredHeight, 1, 1);
			graphics.setGlobalAlpha(255);
		}
	}
	
	public boolean isFocusable(){

		boolean value = !((isOccupied) || (statusSeat == GraphicButtonSeat.SEAT_AISLE));
		
		return value;
	}
	
	protected void onFocus(int direction){
		super.onFocus(direction);
		invalidate();
	}
	
	protected void onUnfocus(){
		super.onUnfocus();
		invalidate();
	}
	
	protected boolean navigationClick(int status, int time) {
		fieldChangeNotify(0);
		//isPermitted();
		return true;
	}
	
	protected boolean keyChar(char character, int status, int time) {
		if (character == Keypad.KEY_ENTER) {
			fieldChangeNotify(0);
			//isPermitted();
			return true;
		}
		return super.keyChar(character, status, time);
	}
	
/*
	private void isPermitted(){
		
		if ((!isOccupied) && (!isSeatAisle())){
			isUsedByCustomer = !isUsedByCustomer;
		}
		
		invalidate();
	}
*/
	
	
	public void isPermitted(boolean isUsed){
		
		if ((!isOccupied) && (!isSeatAisle())){
			isUsedByCustomer = isUsed;
		}
		
		invalidate();
	}
	
	
	
	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}

	public Passenger getPassenger() {
		return passenger;
	}

	public void setPassenger(Passenger passenger) {
		this.passenger = passenger;
	}
}




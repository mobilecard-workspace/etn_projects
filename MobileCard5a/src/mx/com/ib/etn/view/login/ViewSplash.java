package mx.com.ib.etn.view.login;

import java.util.Timer;
import java.util.TimerTask;

import mx.com.ib.etn.utils.UtilIcon;
import mx.com.ib.etn.view.controls.CustomMainScreen;
import net.rim.device.api.system.Application;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;

public class ViewSplash extends CustomMainScreen {

	public ViewSplash() {

		super();

		BitmapField splash = UtilIcon.getSplash();

		add(splash);
		
		Timer timer = new Timer();

		Wait wait = new Wait(this);
		
		timer.schedule(wait, 2800);
	}

	class Wait extends TimerTask {

		private ViewSplash viewSplash;

		public Wait(ViewSplash viewSplash) {

			this.viewSplash = viewSplash;
		}

		public void run() {

			synchronized (Application.getEventLock()) {
				UiApplication.getUiApplication().popScreen(viewSplash);
				UiApplication.getUiApplication().pushScreen(new ViewLogin());
			}
		}
	}
}

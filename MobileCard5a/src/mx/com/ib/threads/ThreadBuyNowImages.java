package mx.com.ib.threads;

import java.io.InputStream;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.util.DataBuffer;

import mx.com.ib.app.MainClass;
import mx.com.ib.controls.ProductLabelField;
import mx.com.ib.utils.Utils;

public class ThreadBuyNowImages extends Thread {
	// public class ThreadBuyNowImages implements Runnable {

	public ProductLabelField[] productLabelFields = null;

	public int index = 0;
	public int limit = 0;

	private MainScreen mainScreen;
	
	
	public ThreadBuyNowImages(MainScreen mainScreen, ProductLabelField[] productLabelFields,
			int index, int limit) {
		this.productLabelFields = productLabelFields;
		this.index = index;
		this.limit = limit;
		this.mainScreen = mainScreen;
	}

	public void run() {

		if (MainClass.idealConnection != null) {

			try {

				this.setPriority(Thread.MIN_PRIORITY);

				for (int i = index; i < limit; i++) {

					String url = null;

					if (this.productLabelFields[i].getProductBeans() != null) {
						url = this.productLabelFields[i].getProductBeans()
								.getPath();
					} else {
						url = this.productLabelFields[i].getGeneralBean()
								.getPath();
					}

					if (url != null && url != "") {
						final int pos = i;

						while (Thread.activeCount() > 7) {

							try {
								ThreadBuyNowImages.sleep(500);
							} catch (InterruptedException e) {

							}
						}

						Thread thread = new Thread(new MapGetter(
								this.productLabelFields[i], url));
						thread.start();
					}
				}
			} catch (Exception e) {

				e.printStackTrace();
			}
		} else {
			Utils.checkConnectionType();
			new ThreadBuyNowImages(mainScreen, this.productLabelFields, this.index,
					this.limit).start();
		}
	}

	private class MapGetter implements Runnable {

		ProductLabelField productLabelField;
		String url;

		public MapGetter(ProductLabelField productLabelField, String url) {

			this.productLabelField = productLabelField;
			this.url = url;
		}

		public void run() {

			try {

				url = url + ";" + MainClass.idealConnection;

				HttpConnection conn = (HttpConnection) Connector.open(url);

				// int progress = 0;
				InputStream in = null;
				byte[] data = new byte[256];

				in = conn.openInputStream();
				DataBuffer db = new DataBuffer();
				int chunk = 0;
				while (-1 != (chunk = in.read(data))) {
					// progress += chunk;
					db.write(data, 0, chunk);
				}

				in.close();
				data = db.getArray();

				if (data != null) {

					Bitmap b = Bitmap.createBitmapFromBytes(data, 0,
							data.length, 1);
					productLabelField.setBitmap(b);
					mainScreen.invalidate();
					//MainClass.mainClass.repaint();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}

package mx.com.ib.threads;

import java.io.UnsupportedEncodingException;
import java.util.Vector;

import crypto.Crypto;

import net.rim.device.api.system.Application;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.SeparatorField;
import net.rim.device.api.ui.container.VerticalFieldManager;

import mx.com.ib.app.MainClass;
import mx.com.ib.beans.GeneralBean;
import mx.com.ib.beans.UsuarioTag;
import mx.com.ib.connection.Communications;
import mx.com.ib.connection.HttpListener;
import mx.com.ib.controls.ConsultProductLabelField;
import mx.com.ib.controls.TagsLabelField;
import mx.com.ib.utils.Utils;
import mx.com.ib.views.IaveScreen;
import mx.com.ib.views.IaveScreenVip;
import mx.com.ib.views.MessagePopupScreen;



public class GetTagsIaveVipThread extends Thread implements HttpListener{
	
	public String url = MainClass.URL_GET_TAGS_USER;
	public String Json = "";
	public boolean isLogin=false;
	public GeneralBean generalBean = null;
	
	public int font = 5;
	
	
	public GetTagsIaveVipThread(String tipo,GeneralBean generalBean){
		this.generalBean=generalBean;
		System.out.println("{\"cadena\":\""+MainClass.IdUser+"|"+1+"\"}");		
		String cadena=Crypto.aesEncrypt(MainClass.key, "{\"cadena\":\""+MainClass.IdUser+"|"+tipo+"\"}");
		this.Json="json="+cadena;
		
	}
	
	public void run() {
		// TODO Auto-generated method stub
		connect();
	}
	
	public void connect(){
		
		Application.getApplication();
		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.start();
		}
		
		if(MainClass.idealConnection != null)
		{
			
			if(this.url != null)
			{				
				try{				    	   
					Communications.sendHttpPost(this, this.url,this.Json);

		       }catch(Throwable t){
		    	   getMessageError();
		       }
			}
		}
		else
		{		
			//System.out.println("ELSE: ");
			Utils.checkConnectionType();
			this.connect();
		}
		
	}

	public void handleHttpError(int errorCode, String error) {
		// TODO Auto-generated method stub
		getMessageError();
	}

	public boolean isDestroyed() {
		// TODO Auto-generated method stub
		return false;
	}

	public void receiveEstatus(String msg) {
		// TODO Auto-generated method stub
		
	}

	public void receiveHeaders(Vector _headers) {
		// TODO Auto-generated method stub
		
	}

	public void receiveHttpResponse(int appCode, byte[] response) {
		// TODO Auto-generated method stub
		
		String sTemp = null;
		StringBuffer sb = new StringBuffer();
		
		try {
			
			sb.append(new String(response,0,response.length,"UTF-8"));
			sTemp = new String (sb.toString());
			//System.out.println("STEMP: "+sTemp);
			sTemp = Crypto.aesDecrypt(MainClass.key, sTemp);
			System.out.println("STEMP GET TAGS: "+sTemp);
			
			UsuarioTag[] tags = MainClass.jsParser.getTagsbyUSer(sTemp);
			MainClass.TagsBeansbyUser=tags;
			
			UiApplication.getUiApplication();
			synchronized (Application.getEventLock()) {
				
				MainClass.splashScreen.remove();
				UiApplication.getUiApplication().pushScreen(new IaveScreenVip(this.generalBean));

			}
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			getMessageError();
		}
		
		
	}
	
	public void getMessageError(){

		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.remove();
			UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Informaci�n no disponible. Favor de intentar m�s tarde", Field.NON_FOCUSABLE));
		}
		
	}

}

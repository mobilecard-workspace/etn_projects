package mx.com.ib.threads;

import java.util.Vector;

import crypto.Crypto;

import net.rim.device.api.system.Application;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;
import mx.com.ib.app.MainClass;
import mx.com.ib.connection.Communications;
import mx.com.ib.connection.HttpListener;
import mx.com.ib.utils.Utils;
import mx.com.ib.views.LoginScreen;
import mx.com.ib.views.MessagePopupScreen;

public class RegisterThread extends Thread implements HttpListener{
	
	public String url = MainClass.URL_USER_INSERT;
	public String post = "";
//	public JSONParser jsParser = new JSONParser();
	
	public RegisterThread(String json){

		this.post = "json="+json;
	}
	
	public void run() {
		// TODO Auto-generated method stub
		connect();
	}
	
public void connect(){
		
		Application.getApplication();
		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.start();
		}
		
		if(MainClass.idealConnection != null)
		{
			
			if(this.url != null)
			{				
				try{				    	   
					Communications.sendHttpPost(this,this.url,this.post);
					//System.out.println("========================");
					   //System.out.println("URL: "+this.url);
					   //System.out.println("JSON: "+this.post
//							   );
					   //System.out.println("========================");
		       }catch(Throwable t){
		    	   //System.out.println("========================");
				   //System.out.println("Excepcion en GETJSON: "+t);
				   //System.out.println("========================");
		       }
			}
		}
		else
		{		
			//System.out.println("ELSE: ");
			Utils.checkConnectionType();
			this.connect();
		}
		
	}

	public void handleHttpError(int errorCode, String error) {
		// TODO Auto-generated method stub
		
	}

	public boolean isDestroyed() {
		// TODO Auto-generated method stub
		return false;
	}

	public void receiveEstatus(String msg) {
		// TODO Auto-generated method stub
		
	}

	public void receiveHeaders(Vector _headers) {
		// TODO Auto-generated method stub
		
	}

	public void receiveHttpResponse(int appCode, byte[] response) {
		// TODO Auto-generated method stub
		
		String sTemp = null;
		StringBuffer sb = new StringBuffer();
		try {
			
			sb.append(new String(response,0,response.length,"UTF-8"));
			sTemp = new String (sb.toString());
			
			//System.out.println("STEMP: "+sTemp);
			
			sTemp = Crypto.aesDecrypt(Utils.parsePass(MainClass.p), sTemp);
		
			UiApplication.getUiApplication();
			synchronized (Application.getEventLock()) {
				
					MainClass.splashScreen.remove();

				if(MainClass.jsParser.isRegister(sTemp)){					
		
					UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
					UiApplication.getUiApplication().pushScreen(new LoginScreen());					
					UiApplication.getUiApplication().pushScreen(new MessagePopupScreen(MainClass.jsParser.getMessage(sTemp),Field.NON_FOCUSABLE));
					
				}else{

					UiApplication.getUiApplication().pushScreen(new MessagePopupScreen(MainClass.jsParser.getMessage(sTemp),Field.NON_FOCUSABLE));

				}
				
			}

		}catch(Exception e){
			
			getMessageError();
		}
	}
	
	public void getMessageError(){

		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.remove();
			UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Informaci�n no disponible. Favor de intentar m�s tarde", Field.NON_FOCUSABLE));
		}
		
	}

}

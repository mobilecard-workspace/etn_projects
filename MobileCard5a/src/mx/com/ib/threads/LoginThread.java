package mx.com.ib.threads;

import java.util.Vector;

import mx.com.ib.app.MainClass;
import mx.com.ib.connection.Communications;
import mx.com.ib.connection.HttpListener;
import mx.com.ib.utils.Utils;
import mx.com.ib.views.LoginScreen;
import mx.com.ib.views.MenuScreen;
import mx.com.ib.views.MessagePopupScreen;
import mx.com.ib.views.PswdPopupScreen98;
import mx.com.ib.views.UpdateUser;
import net.rim.device.api.system.Application;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;
import crypto.Crypto;

public class LoginThread extends Thread implements HttpListener{
	
	public String url = MainClass.URL_LOGIN;
	public String post = "";
//	public JSONParser jsParser = new JSONParser();
	//public LoginScreen screen = new LoginScreen();
	
	public LoginScreen screen = null;

	public LoginThread(LoginScreen screen, String json){

		this.post = "json="+json;
		System.out.println(post);
		this.screen = screen;
	}

	public void connect(){
		
		Application.getApplication();
		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.start();
		}
		
		if(MainClass.idealConnection != null)
		{
			
			if(this.url != null)
			{				
				try{				    	   
					Communications.sendHttpPost(this,this.url,this.post);

		       }catch(Throwable t){
		       }
			}
		}
		else
		{		
			Utils.checkConnectionType();
			this.connect();
		}
		
	}
	
	public void run() {

		this.setPriority(Thread.MAX_PRIORITY);
		connect();

	}

	public void handleHttpError(int errorCode, String error) {
		getMessageError(error);
		
	}

	public boolean isDestroyed() {
		return false;
	}

	public void receiveEstatus(String msg) {
	}

	public void receiveHeaders(Vector _headers) {
	}

	public void receiveHttpResponse(int appCode, byte[] response) {
		
		String sTemp = null;
		StringBuffer sb = new StringBuffer();		
		
		try {
			
			sb.append(new String(response,0,response.length,"UTF-8"));
			sTemp = new String (sb.toString());
			
			
			sTemp = Crypto.aesDecrypt(Utils.parsePass(MainClass.p), sTemp);
			System.out.println("lo que regresa el login "+sTemp);
			
			UiApplication.getUiApplication();
			synchronized (Application.getEventLock()) {
				
				MainClass.splashScreen.remove();
				
				if(MainClass.jsParser.isLogin(sTemp)){	
					this.screen.cleanFields();
					UiApplication.getUiApplication().pushScreen(new MenuScreen());					
				}else if(MainClass.jsParser.isLogin2(sTemp)){

					this.screen.cleanFields();
					UiApplication.getUiApplication().pushScreen(new UpdateUser("", Field.NON_FOCUSABLE));
					UiApplication.getUiApplication().pushScreen(new MessagePopupScreen(MainClass.msg, Field.NON_FOCUSABLE));
				} else if (MainClass.jsParser.isLogin3(sTemp)){
					//	MainClass.userBean = null;	
						this.screen.cleanFields();
						UiApplication.getUiApplication().pushScreen(new PswdPopupScreen98("", Field.NON_FOCUSABLE));
						UiApplication.getUiApplication().pushScreen(new MessagePopupScreen(MainClass.msg, Field.NON_FOCUSABLE));
				} else {
					//Correci�n
					MainClass.userBean = null;					
					UiApplication.getUiApplication().pushScreen(new MessagePopupScreen(MainClass.jsParser.getMessageError(sTemp), Field.NON_FOCUSABLE));
					
				}				
			}
		}catch(Exception e){
			//System.out.println(e.toString());
			getMessageError(e.getMessage());
		}

	}
	
	public void getMessageError(String e){

		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.remove();
			UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Intente m�s tarde. ", Field.NON_FOCUSABLE));
		}
		
	}

}

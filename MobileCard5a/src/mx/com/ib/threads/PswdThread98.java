package mx.com.ib.threads;

import java.io.UnsupportedEncodingException;
import java.util.Vector;

import crypto.Crypto;

import net.rim.device.api.system.Application;
import net.rim.device.api.system.JPEGEncodedImage;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;

import mx.com.ib.app.MainClass;
import mx.com.ib.connection.Communications;
import mx.com.ib.connection.HttpListener;
import mx.com.ib.parser.JSONParser;
import mx.com.ib.utils.Utils;
import mx.com.ib.views.MenuScreen;
import mx.com.ib.views.MessagePopupScreen;

public class PswdThread98 extends Thread implements HttpListener{
	
	public String url = MainClass.URL_UPDATE_PASS;
	public String post = "";
//	public JSONParser jsParser = new JSONParser();
	
	public PswdThread98(String json){
		
		this.post = json;
		
	}
	
	public void run() {
		// TODO Auto-generated method stub
		
		connect();
		
	}
	
	public void connect(){		
		
		if(MainClass.idealConnection != null)
		{
			
			if(this.url != null)
			{				
				try{				    	   

					Communications.sendHttpPost(this, this.url, this.post);

		       }catch(Throwable t){
		    	   getMessageError();
		       }
			}
		}
		else
		{		
			//System.out.println("ELSE: ");
			Utils.checkConnectionType();
			this.connect();
		}
		
	}

	public void handleHttpError(int errorCode, String error) {
		// TODO Auto-generated method stub
		
		getMessageError();
		
		
	}

	public boolean isDestroyed() {
		// TODO Auto-generated method stub
		return false;
	}

	public void receiveEstatus(String msg) {
		// TODO Auto-generated method stub
		
	}

	public void receiveHeaders(Vector _headers) {
		// TODO Auto-generated method stub
		
	}

	public void receiveHttpResponse(int appCode, byte[] response) {
		// TODO Auto-generated method stub
		
		String sTemp = null;
		StringBuffer sb = new StringBuffer();
		
		try {
			
			sb.append(new String(response,0,response.length,"UTF-8"));
			sTemp = new String (sb.toString());
			
			//System.out.println("=======");
			//System.out.println("STEMP: "+sTemp);
			//System.out.println("=======");
			sTemp = Crypto.aesDecrypt(Utils.parsePass(MainClass.p2), sTemp);
			
			//System.out.println("=======");
		
			
			UiApplication.getUiApplication();
			synchronized (Application.getEventLock()) {
				
				MainClass.splashScreen.remove();
				
				if(MainClass.jsParser.isUpdate(sTemp)){
					
					UiApplication.getUiApplication().pushScreen(new MenuScreen());			
					UiApplication.getUiApplication().pushScreen(new MessagePopupScreen(MainClass.jsParser.getMessage(sTemp), Field.NON_FOCUSABLE));
					MainClass.userBean.setPswd(MainClass.userPswd);
					MainClass.p=MainClass.userPswd;
					MainClass.p2="";
					
					
				}else{
					
					UiApplication.getUiApplication().pushScreen(new MessagePopupScreen(MainClass.jsParser.getMessage(sTemp), Field.NON_FOCUSABLE));
					
				}
				
			}
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void getMessageError(){

		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.remove();
			UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Informaci�n no disponible. Favor de intentar m�s tarde", Field.NON_FOCUSABLE));
		}
		
	}

//	public String getJSON(){
		
		
		
//	}

}
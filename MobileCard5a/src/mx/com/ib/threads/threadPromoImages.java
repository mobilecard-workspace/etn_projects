package mx.com.ib.threads;

import java.io.InputStream;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.util.DataBuffer;

import mx.com.ib.app.MainClass;
import mx.com.ib.controls.ProductLabelField;
import mx.com.ib.controls.PromotionLabelField;
import mx.com.ib.utils.Utils;

public class threadPromoImages extends Thread{
	
	public PromotionLabelField[] promotionLabelField = null;
	
	public int index = 0;
	public int limit = 0;
	
	public threadPromoImages(PromotionLabelField[] promotionLabelField, int index, int limit){
		this.promotionLabelField = promotionLabelField;
		this.index = index;
		this.limit = limit;
		
		//System.out.println("CONSTRUCIT HILO");
	}
	
	public void run() {
		// TODO Auto-generated method stub
		//System.out.println("RUN");
		if(MainClass.idealConnection!=null){
			
			this.setPriority(Thread.MIN_PRIORITY);
			for(int i=index; i<limit; i++)
			{
				//System.out.println("DENTRO FOR");
				try{
					//System.out.println("======================");
					//System.out.println("PATH "+this.promotionLabelField[i].getProductBean().getPath());
					//System.out.println("======================");
				if(this.promotionLabelField[i].getProductBean().getPath()!= null && this.promotionLabelField[i].getProductBean().getPath() !=""){
					final int pos = i;
					
					while(Thread.activeCount()>7){
						//System.out.println("# DE HILOS ACTIVOS: "+Thread.activeCount()+"");
						try {
							ThreadBuyNowImages.sleep(500);
						} catch(InterruptedException e) {
							//System.out.println("======================");
							//System.out.println("ERROR HILO IMAGEN");
							//System.out.println("======================");
						}
					}
					
					new Thread(){
						public void run() {
							// TODO Auto-generated method stub
							try{
								HttpConnection conn = (HttpConnection) Connector.open(promotionLabelField[pos].getProductBean().getPath() + ";"
										+ MainClass.idealConnection);
								int progress = 0;
								InputStream in = null;
								byte[] data = new byte[256];
								try {
									in = conn.openInputStream();
									DataBuffer db = new DataBuffer();
									int chunk = 0;
									while (-1 != (chunk = in.read(data))) {
										progress += chunk;
										db.write(data, 0, chunk);
									}
									in.close();
									data = db.getArray();
								} catch (Exception e) {	}
								 if(data != null)
								 {
									 System.gc();
									 Bitmap b = Bitmap.createBitmapFromBytes(data, 0, data.length, 1);
									 promotionLabelField[pos].setImage(b);
//									 productLabelFields[pos].setBitmap(b);
//									 articleBuyNow[pos].setThumb(b);
//								 	 MainClass.mainClass.repaint();
								 }
							}catch (Exception e) {
								// TODO: handle exception
							}
						}
					}.start();
				}
			}catch(Exception e){
//				//System.out.println("EXCEPTION "+e.toString());
				if(this.promotionLabelField[i].getProductBean().getPath()!= null && this.promotionLabelField[i].getProductBean().getPath() !=""){
					final int pos = i;
					
					while(Thread.activeCount()>7){
						//System.out.println("# DE HILOS ACTIVOS: "+Thread.activeCount()+"");
						try {
							ThreadBuyNowImages.sleep(500);
						} catch(InterruptedException es) {
							//System.out.println("======================");
							//System.out.println("ERROR HILO IMAGEN");
							//System.out.println("======================");
						}
					}
					
					new Thread(){
						public void run() {
							// TODO Auto-generated method stub
							try{
								HttpConnection conn = (HttpConnection) Connector.open(promotionLabelField[pos].getProductBean().getPath() + ";"
										+ MainClass.idealConnection);
								int progress = 0;
								InputStream in = null;
								byte[] data = new byte[256];
								try {
									in = conn.openInputStream();
									DataBuffer db = new DataBuffer();
									int chunk = 0;
									while (-1 != (chunk = in.read(data))) {
										progress += chunk;
										db.write(data, 0, chunk);
									}
									in.close();
									data = db.getArray();
								} catch (Exception e) {	}
								 if(data != null)
								 {
									 System.gc();
									 Bitmap b = Bitmap.createBitmapFromBytes(data, 0, data.length, 1);
									 promotionLabelField[pos].setImage(b);
//									 productLabelFields[pos].setBitmap(b);
//									 articleBuyNow[pos].setThumb(b);
//								 	 MainClass.mainClass.repaint();
								 }
							}catch (Exception e) {
								// TODO: handle exception
							}
						}
					}.start();
				}
				}
			}
		}else
		{
			Utils.checkConnectionType();
			new threadPromoImages(this.promotionLabelField, this.index, this.limit).start();
		}
		
	}

}

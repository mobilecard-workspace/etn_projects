package mx.com.ib.threads;

import java.io.UnsupportedEncodingException;
import java.util.Vector;

import mx.com.ib.app.MainClass;
import mx.com.ib.beans.CardBean;
import mx.com.ib.connection.Communications;
import mx.com.ib.connection.HttpListener;
import mx.com.ib.utils.Utils;
import mx.com.ib.views.MessagePopupScreen;
import net.rim.device.api.system.Application;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;
import crypto.Crypto;

public class InfoUserThread extends Thread implements HttpListener{
	
	public String url = MainClass.URL_GET_USERDATA;
	public String post = "";
	public String tipo = "";
	
	public InfoUserThread(String json){
		
		this.post = "json="+json;
	
	}
	
	public void run() {
		// TODO Auto-generated method stub
		connect();
	}
	
	public void connect(){
		
		
		if(MainClass.idealConnection != null)
		{
			
			if(this.url != null)
			{				
				try{				    	   
					
					Communications.sendHttpPost(this,this.url,this.post);
					
		       }catch(Throwable t){
		    	   
		    	   getMessageError();
		    	   
		       }
			}
		}
		else
		{		
			//System.out.println("ELSE: ");
			Utils.checkConnectionType();
			this.connect();
		}
		
	}

	public void handleHttpError(int errorCode, String error) {
		// TODO Auto-generated method stub
	
		getMessageError();
		
	}

	public boolean isDestroyed() {
		// TODO Auto-generated method stub
		return false;
	}

	public void receiveEstatus(String msg) {
		// TODO Auto-generated method stub
		
	}

	public void receiveHeaders(Vector _headers) {
		// TODO Auto-generated method stub
		
	}

	public void receiveHttpResponse(int appCode, byte[] response) {
		// TODO Auto-generated method stub
	
		String sTemp = null;
		StringBuffer sb = new StringBuffer();		
			
			try {
				
				sb.append(new String(response,0,response.length,"UTF-8"));
				sTemp = new String (sb.toString());				
				sTemp = Crypto.aesDecrypt(Utils.parsePass(MainClass.p), sTemp);
				System.out.println(sTemp);
				MainClass.userBean = MainClass.jsParser.getUser(sTemp);	
				
				//AGREGANDO CREACI�N DE OBJETO CARDBEAN 10-12-2012
				
				MainClass.visaBean = new CardBean();
				MainClass.visaBean.setNumTarjeta(MainClass.userBean.getCredit());
				MainClass.visaBean.setVencimientoTarjeta(MainClass.userBean.getLife());
				
				UiApplication.getUiApplication();
				synchronized (Application.getEventLock()) {
					
					if(MainClass.userBean != null){
						
							new CreditInfoThread("proveedores").start();
						
					}else{
						
						getMessageError();
						
					}
					
				}				
								
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		
	}	
	
	public void getMessageError(){

		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.remove();
			UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Informaci�n no disponible. Favor de intentar m�s tarde", Field.NON_FOCUSABLE));
		}
		
	}

}

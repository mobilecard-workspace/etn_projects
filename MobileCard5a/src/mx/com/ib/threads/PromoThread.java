package mx.com.ib.threads;

import java.io.UnsupportedEncodingException;
import java.util.Vector;

import crypto.Crypto;

import net.rim.device.api.system.Application;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;

import mx.com.ib.app.MainClass;
import mx.com.ib.beans.ProductBean;
import mx.com.ib.connection.Communications;
import mx.com.ib.connection.HttpListener;
import mx.com.ib.controls.PromotionLabelField;
import mx.com.ib.utils.Utils;
import mx.com.ib.views.MessagePopupScreen;
import mx.com.ib.views.PromotionsScreen;

public class PromoThread extends Thread implements HttpListener{
	
	public String url = MainClass.URL_GET_PROMOTIONS;
	
	public PromoThread(){
		
	}
	
	public void run() {
		// TODO Auto-generated method stub
		connect();
	}
	
	public void connect(){
		
		Application.getApplication();
		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.start();
		}
		
		if(MainClass.idealConnection != null)
		{
			
			if(this.url != null)
			{				
				try{				    	   
					Communications.sendHttpRequest(this, this.url);

		       }catch(Throwable t){
		    	   getMessageError();
		       }
			}
		}
		else
		{		
			//System.out.println("ELSE: ");
			Utils.checkConnectionType();
			this.connect();
		}
		
	}

	public void handleHttpError(int errorCode, String error) {
		// TODO Auto-generated method stub
		getMessageError();
	}

	public boolean isDestroyed() {
		// TODO Auto-generated method stub
		return false;
	}

	public void receiveEstatus(String msg) {
		// TODO Auto-generated method stub
		
	}

	public void receiveHeaders(Vector _headers) {
		// TODO Auto-generated method stub
		
	}

	public void receiveHttpResponse(int appCode, byte[] response) {
		// TODO Auto-generated method stub
		
		String sTemp = null;
		StringBuffer sb = new StringBuffer();
		
		try {
			
			sb.append(new String(response,0,response.length,"UTF-8"));
			sTemp = new String (sb.toString());
			//System.out.println("STEMP: "+sTemp);
			sTemp = Crypto.aesDecrypt(MainClass.key, sTemp);
			//System.out.println("STEMP: "+sTemp);
			
			ProductBean[] promos = MainClass.jsParser.getPromos(sTemp);
			
			UiApplication.getUiApplication();
			synchronized (Application.getEventLock()) {
				
				MainClass.splashScreen.remove();
				
				if(promos.length > 0){
					
					MainClass.promotionLabelField = new PromotionLabelField[promos.length];
					
					for(int i = 0; i < promos.length; i++){
					
						MainClass.promotionLabelField[i] = new PromotionLabelField(promos[i]);
						
					}
					
					UiApplication.getUiApplication().pushScreen(new PromotionsScreen());
					
				}else{
					
					UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Actualmente no hay Promociones Disponibles", Field.NON_FOCUSABLE));
					
				}
				
			}
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			getMessageError();
		}
		
		
	}
	
	public void getMessageError(){

		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.remove();
			UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Informaci�n no disponible. Favor de intentar m�s tarde", Field.NON_FOCUSABLE));
		}
		
	}

}

package mx.com.ib.threads;

import java.util.Vector;

import mx.com.ib.app.MainClass;
import mx.com.ib.connection.Communications;
import mx.com.ib.connection.HttpListener;
import mx.com.ib.utils.Utils;
import mx.com.ib.views.MessagePopupScreen;
import mx.com.ib.views.WebPurchaseScreen;
import net.rim.device.api.system.Application;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;

/**
 * 
 * @author ADDCEL14
 * SDK 5.0.0
 */

public class PurchaseInterjetThread extends Thread implements HttpListener {
	
	private String login;
	private String pnr;
	private String monto;
	private String url;
	
	public PurchaseInterjetThread(String pnr, String monto){
		this.login = MainClass.userBean.getLogin();
		this.pnr = pnr;
		this.monto = monto;
		this.url = MainClass.URL_INTERJET_PURCHASE + "?user="+this.login
					+"&pnr="+this.pnr+"&monto="+this.monto;
	}
	
	public void run(){
		connect();
	}
	
	public void connect(){
		Application.getApplication();
		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.start();
		}
		
		if(MainClass.idealConnection != null){
			if(this.url != null){
				try{
					Communications.sendHttpRequest(this, this.url);
				}catch(Throwable t){
					
				}
			}
		}
		else{
			Utils.checkConnectionType();
			this.connect();
		}
	}

	public void receiveHttpResponse(int appCode, byte[] response) {
		// TODO Auto-generated method stub
		
		String sTemp = null;
		StringBuffer sb = new StringBuffer();
		String result;
		
		try{
			sb.append(new String(response, 0, response.length, "UTF-8"));
			sTemp = new String(sb.toString());
			System.out.println("****RESULT_WS_INTERJET_TRANS****" + sTemp);
			if(sTemp != ""){
				result = MainClass.jsParser.getPurchaseInterjet(sTemp);
				System.out.println("****ID EXTRAIDA****" + result);
				UiApplication.getUiApplication();
				
				synchronized (Application.getEventLock()) {
					MainClass.splashScreen.remove();
					if (result.length() != 0){
						UiApplication.getUiApplication().pushScreen(new WebPurchaseScreen(setWebPurchaseURL()));
					} else {
						Dialog.alert("Su compra no puede realizarse en este momento.");
					}

				}
			}
		}
		catch (Exception e) {
			// TODO: handle exception
			getMessageError();
		}

	}

	public void handleHttpError(int errorCode, String error) {
		// TODO Auto-generated method stub

	}

	public void receiveEstatus(String msg) {
		// TODO Auto-generated method stub

	}

	public void receiveHeaders(Vector _headers) {
		// TODO Auto-generated method stub

	}

	public boolean isDestroyed() {
		// TODO Auto-generated method stub
		return false;
	}
	

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}
	
	public void getMessageError(){
		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.remove();
			UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Informaci�n no disponible. Favor de intentar m�s tarde", Field.NON_FOCUSABLE));
		}
	}
	
	public String setWebPurchaseURL(){
		String url = "http://mobilecard.mx:8080/ProCom/comercio_fin.jsp?user=" +
					getLogin() +
					"&referencia=" + getPnr()+ 
					"&monto=" + Utils.reemplaza(getMonto(), ".", "%2E");
		

		System.out.println("***URL WEB PROCOM***" + url);
		
		return url;
	}

}

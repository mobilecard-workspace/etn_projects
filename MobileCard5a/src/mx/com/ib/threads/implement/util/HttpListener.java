package mx.com.ib.threads.implement.util;

import mx.com.ib.views.components.Viewable;
import net.rim.device.api.system.Application;
import net.rim.device.api.ui.UiApplication;

import org.json.me.JSONObject;


public abstract class HttpListener {

	private String url;
	private HttpPoster httpPoster;
	private String post;
	private Viewable viewable;

	public HttpListener(String post, String url, Viewable viewable) {

		httpPoster = new HttpPoster();
		this.post = post;
		this.viewable = viewable;
		this.url = url;
	}

	public void run() {
		connect();
	}

	public void connect() {

		try {

			url = url + "?json=" + post;
			
			httpPoster.sendHttpRequest(this, this.url);

		} catch (Exception e) {

			e.printStackTrace();
			sendMessageError();
		}
	}

	public void sendMessageError() {

		synchronized (Application.getEventLock()) {
			viewable.sendMessage("Error");
		}
	}


	public void handleHttpError(int errorCode, String error) {
		sendMessageError(error);
	}


	public void sendMessageError(String error) {

		synchronized (Application.getEventLock()) {
			viewable.sendMessage(error);
		}
	}


	public void sendData(JSONObject jsObject) {

		
		final JSONObject jsObject01 = jsObject;
		
		UiApplication.getUiApplication().invokeLater(new Runnable() {
			public void run() {

				viewable.setData(0, jsObject01);
			}
		});
		
		
		/*
		synchronized (Application.getEventLock()) {
			viewable.setData(0, jsObject);
		}
		*/
	}


	public abstract void receiveHttpResponse(int appCode, byte[] response);

}

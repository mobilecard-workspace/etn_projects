package mx.com.ib.threads.implement;

import mx.com.ib.app.MainClass;
import mx.com.ib.threads.implement.util.HttpListener;
import mx.com.ib.views.components.Viewable;

import org.json.me.JSONException;
import org.json.me.JSONObject;

public class InsertaLinea extends HttpListener {

	public InsertaLinea(String post, Viewable viewable) {
		super(post, MainClass.URL_GOB_ESTADO_INSERTA, viewable);
	}

	public void receiveHttpResponse(int appCode, byte[] response) {

		JSONObject jsObject = null;
		String json = new String(response);

		try {
			StringBuffer buffer = new StringBuffer();
			buffer.append("{insercion:").append(json).append("}");
			jsObject = new JSONObject(buffer.toString());
			sendData(jsObject);
		} catch (JSONException e) {
			e.printStackTrace();
			sendMessageError("Error en lectura de JSON");
		}
	}
}

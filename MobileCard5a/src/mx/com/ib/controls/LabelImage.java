package mx.com.ib.controls;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.LabelField;


public class LabelImage extends Field implements DrawStyle
{

	Bitmap image = null;
	long text = -1;
	LabelField label;
	int backgroundColor = -1;
	
	public LabelImage(String i, long style)
	{
		super(style);
		image = Bitmap.getBitmapResource(i);
	}
	public LabelImage(Bitmap i, long style)
	{
		super(style);
		image = i;
	}
	LabelImage(Bitmap i, int b, long style)
	{
		super(style);
		image = i;
		backgroundColor = b;
	}
	
	public LabelImage(String i, long text, long style)
	{
		super(style);
		image = Bitmap.getBitmapResource(i);
		label = new LabelField(Long.toString(text));
//		label.setFont(MainClass.categoriesFont);
		this.text = text;
	}
	
	protected void layout(int width, int height) {
		if(text >= 0)
		{
			this.setExtent(label.getPreferredWidth()+20, label.getPreferredHeight());
			
		}
		else
		{
			this.setExtent(image.getWidth(), image.getHeight());
		}
	}
	
	public int getImageWidth()
	{
		return 120;//image.getWidth();
	}
	public int getImageHeight()
	{
		return 90;//image.getHeight();
	}
	
	public void setNewText(long i)
	{
		text = i;
		invalidate();
	}

	public void setNew(LabelImage i)
	{
		image = i.image;
		layout(image.getHeight(), image.getWidth());
	}
	
	public void setNew(Bitmap i)
	{
		image = i;
		layout(image.getHeight(), image.getWidth());
	}
	
	protected void drawFocus(Graphics g,boolean on)
	{
		g.drawBitmap(0, 0, image.getWidth(), image.getHeight(), image, 0, 0);
		if(text >= 0)
		g.drawText(Long.toString(text), 5, 5);
	}
	
	protected void paint(Graphics graphics) {
		if(backgroundColor>=0)
		{
			graphics.setColor(backgroundColor);
			graphics.fillRect(0, 0, getExtent().width, getExtent().height);
		}
		graphics.drawBitmap(0, 0, image.getWidth(), image.getHeight(), image, 0, 0);
		
		if(text >= 0)
		{
//			graphics.setFont(MainClass.categoriesFont);
			graphics.setColor(Color.WHITE);
			if(Long.toString(text).length() < 2)
				graphics.drawText("0"+Long.toString(text), (getExtent().width/2) - (label.getPreferredWidth()/2),
						(getExtent().height/2) - (label.getPreferredHeight()/2));
			else
				graphics.drawText(Long.toString(text), (getExtent().width/2) - (label.getPreferredWidth()/2),
					(getExtent().height/2) - (label.getPreferredHeight()/2));
		}
	}

}

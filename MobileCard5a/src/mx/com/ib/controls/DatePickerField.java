package mx.com.ib.controls;

import java.util.Calendar;
import java.util.Date;

import mx.com.ib.controls.DatePickerDialog.DatePickerField.NumTimeField;
import mx.com.ib.controls.DatePickerDialog.DatePickerField.StrTimeField;
import mx.com.ib.controls.DatePickerDialog.DatePickerField.TimeField;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

public class DatePickerField extends HorizontalFieldManager implements
FieldChangeListener{
	
  	private String[] daysOfWeek = new String[] { "Domingo", "Lunes",
			"Martes", "Miercoles", "Jueves", "Viernes", 
			"Sabado" };
	private String[] months = new String[] { "Ene", "Feb", "Mar", "Abr",
			"May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", 
			"Dic" };
	private int mDayOfMonth = 10;
	private int mMonth = 1;
	private int mYear = 2009;
	
	public ButtonField mOKButton;
    public ButtonField mCancelButton;

	private StrTimeField mDayOfWeekField;
	private StrTimeField mMonthField;
	private NumTimeField mDayOfMonthField;
	private NumTimeField mYearField;
	Calendar calendar = Calendar.getInstance();

	public DatePickerField(Date date) {
		
		mOKButton = new ButtonField("OK", ButtonField.CONSUME_CLICK);
		mCancelButton = new ButtonField("Cancel", ButtonField.CONSUME_CLICK);
		calendar.setTime(date);
		mYear = calendar.get(Calendar.YEAR);
		// Calendar.JANUARY == 0, so +1 value
		mMonth = calendar.get(Calendar.MONTH);
		mDayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		// think it's better to disable Day Of Week edit
		mDayOfWeekField = new StrTimeField(daysOfWeek, dayOfWeek - 1,
				NON_FOCUSABLE);
		mDayOfWeekField.setChangeListener(this);
//		add(mDayOfWeekField);
		mMonthField = new StrTimeField(months, mMonth);
		mMonthField.setChangeListener(this);
		add(mMonthField);
		mDayOfMonthField = new NumTimeField(mDayOfMonth, 1, 31);
		mDayOfMonthField.setChangeListener(this);
		add(mDayOfMonthField);
		mYearField = new NumTimeField(mYear, 1900, 2005);
		mYearField.setChangeListener(this);
		add(mYearField);
		add(mOKButton);
		add(mCancelButton);
	}

	public void fieldChanged(Field field, int context) {
		mDayOfMonth = mDayOfMonthField.getValue();
		calendar.set(calendar.DAY_OF_MONTH, mDayOfMonth);
		mMonth = mMonthField.getValue();
		calendar.set(calendar.MONTH, mMonth);
		mYear = mYearField.getValue();
		calendar.set(calendar.YEAR, mYear);
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
		mDayOfWeekField.setIndex(dayOfWeek);
	}

	public Date getDate() {
		return calendar.getTime();
	}

	public void setDate(Date date) {
		calendar.setTime(date);
		mYear = calendar.get(Calendar.YEAR);
		mMonth = calendar.get(Calendar.MONTH);
		mDayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		mDayOfWeekField.setIndex(dayOfWeek - 1);
		mMonthField.setIndex(mMonth);
		mDayOfMonthField.setValue(mDayOfMonth);
		mYearField.setValue(mYear);
	}

	abstract class TimeField extends LabelField {
		int mWidth = 0;

		public TimeField() {
			super("", FOCUSABLE);
		}

		public TimeField(long style) {
			super("", style);
		}

		protected abstract void switchValue(int step);

		protected boolean navigationMovement(int dx, int dy, 
				int status, int time) {
		    if (Math.abs(dy) > Math.abs(dx)) {
		        switchValue(-dy);
			return true;
		    } else
		        return super.navigationMovement(dx, dy, status, time);
		}

		boolean prepared = false;

		protected void onFocus(int direction) {
			prepared = false;
			super.onFocus(direction);
		}

		protected void onUnfocus() {
			invalidate();
			super.onUnfocus();
		}

		// comment on RIM OS < 4.7
//		protected boolean touchEvent(TouchEvent msg) {
//			if (isFocus() && msg.getEvent() == TouchEvent.CLICK) {
//				if (!prepared) {
//					prepared = true;
//				} else {
//					int y = msg.getY(1);
//					int cy = getTop() + (getHeight() >> 1);
//					switchValue((y > cy) ? -1 : 1);
//				}
//			}
//			return false;
//		}

		public int getPreferredWidth() {
			return mWidth;
		}

		public int getPreferredHeight() {
			return super.getPreferredHeight() + 24;
		}

		protected void layout(int width, int height) {
			super.layout(width, height);
			setExtent(getPreferredWidth(), getPreferredHeight());
		}

		protected void paint(Graphics graphics) {
			String text = getText();
			Font font = getFont();
			int x = (getPreferredWidth() 
				- font.getAdvance(text)) >> 1;
			int y = (getPreferredHeight() - font.getHeight()) >> 1;
			graphics.drawText(text, x, y);
			if (isFocus()) {
				graphics.setColor(Color.WHITE);
				int xc = (getPreferredWidth() >> 1);
				int y1 = 10, y2 = 0, x2 = xc - 9, x1 = xc + 9;

				int[] xPts = new int[] { x1, x2, xc };
				int[] yPts = new int[] { y1, y1, y2 };
				graphics.drawFilledPath(xPts, yPts, 
				    null, null);

				y2 = getPreferredHeight();
				y1 = y2 - 10;

				yPts = new int[] { y1, y1, y2 };
				graphics.drawFilledPath(xPts, yPts, 
				    null, null);
			}
		}

		public abstract int getValue();
	}

	class StrTimeField extends TimeField {
		String[] mValues;
		int mIndex;

		public StrTimeField(String[] values) {
			this(values, 0);
		}

		public StrTimeField(String[] values, int value) {
			this(values, value, FOCUSABLE);
		}

		public StrTimeField(String[] values, int value, long style) {
			super(style);
			mValues = values;
			setIndex(value);
			Font font = getFont();
			for (int i = 0; i < mValues.length; i++) {
				int width = font.getAdvance(mValues[i]);
				mWidth = Math.max(mWidth, width);
			}
			mWidth += 10;
		}

		protected void switchValue(int step) {
			int index = mIndex + step;
			if (index < 0 || index >= mValues.length)
				index += ((index > 0) ? -1 : 1) 
				    * mValues.length;
			setIndex(index);
		}

		private void setIndex(int index) {
			if (index >= 0 && index < mValues.length) {
				mIndex = index;
				setText(mValues[mIndex]);
			}
		}

		public int getValue() {
			return mIndex;
		}

	}

	class NumTimeField extends TimeField {
		int mValue;
		int mMinValue;
		int mMaxValue;

		public NumTimeField(int val, int minVal, int maxVal) {
			this(val, minVal, maxVal, FOCUSABLE);
		}

		public NumTimeField(int val, int minVal, int maxVal,
				long style) {
			super(style);
			mValue = val;
			mMinValue = minVal;
			mMaxValue = maxVal;

			setText(String.valueOf(mValue));
			int maxDig = String.valueOf(mMaxValue).length();
			String test = "";
			for (int i = 0; i < maxDig; i++)
				test += "0";
			mWidth = getFont().getAdvance(test);
			mWidth += 10;
		}

		protected void switchValue(int step) {
			int value = mValue + step;
			if (value > mMaxValue)
				value = value - (mMaxValue - mMinValue + 1);
			if (value < mMinValue)
				value = value + (mMaxValue - mMinValue + 1);
			setValue(value);
		}

		private void setValue(int value) {
			mValue = value;
			setText(String.valueOf(mValue));
		}

		public int getValue() {
			return mValue;
		}
	}
}



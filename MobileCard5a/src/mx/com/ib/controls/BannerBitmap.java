package mx.com.ib.controls;

import mx.com.ib.app.MainClass;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.BitmapField;

public class BannerBitmap extends BitmapField {
	private int bg_color;
	public BannerBitmap(){
		super();
		setBitmap(Bitmap.getBitmapResource(MainClass.FOOTER));
		bg_color = MainClass.BG_GRAY;
	}
	
	public BannerBitmap(String bitmap){
		super();
		setBitmap(Bitmap.getBitmapResource(bitmap));
		bg_color = MainClass.BG_GRAY;
	}
	
	public BannerBitmap(int bg_color){
		super();
		setBitmap(Bitmap.getBitmapResource(MainClass.FOOTER));
		this.bg_color = bg_color;
	}
	
	public BannerBitmap(int bg_color, String bitmap){
		super();
		setBitmap(Bitmap.getBitmapResource(bitmap));
		this.bg_color = bg_color;
	}
	
	protected void paint(Graphics g){
		int defColor = g.getColor();
		g.setColor(bg_color);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		g.setColor(defColor);
		super.paint(g);
	}
	
	public void setBgColor(int color){
		bg_color = color;
	}
}

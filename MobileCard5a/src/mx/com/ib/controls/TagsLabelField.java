package mx.com.ib.controls;

import crypto.Crypto;
import mx.com.ib.app.MainClass;
import mx.com.ib.beans.UsuarioTag;
import mx.com.ib.threads.RemoveTagsThread;
import mx.com.ib.utils.Utils;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;

public class TagsLabelField extends LabelField{
	

	public UsuarioTag consultBean = null; 
	
	public int x = (Display.getWidth()-50)/3;
	public int color = Color.BLACK;
	public int colorField = Color.ORANGE;
	public long style = 0;
	
	public Font font = null;
	public int xFont = 160;
	
	public TagsLabelField(UsuarioTag consultBean, int color, int colorField, Font font,long style){
		
		super(consultBean.getUsuario(), style);
		
		this.consultBean = consultBean;
		this.color = color;
		this.colorField = colorField;
		this.font = font;
		this.style = style;
		
	}
	
	protected void paint(Graphics graphics) {
		// TODO Auto-generated method stub
		graphics.setColor(colorField);
		graphics.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
		
		graphics.setColor(this.color);
		graphics.setFont(font);
		
		if(Display.getWidth() < 320){
			xFont = 140; 
		}
	//	if(!MainClass.TagsBeansbyUser[i].getUsuario().equals("-1")||MainClass.TagsBeansbyUser[i].getUsuario().equals("-1")){}
		
		int py = 5;
		Utils.paintText(graphics, "TAG: "+this.consultBean.getNumero(), 5, py, 55);
		py += 15;
		
		if(Integer.parseInt(this.consultBean.getTipoTag()) != 2){
			Utils.paintText(graphics, "D�gito Verificador o cliente: "+this.consultBean.getDv(), 5, py, 50);
			py+=15;
		}
		
		Utils.paintText(graphics, "Etiqueta:  "+this.consultBean.getEtiqueta(), 5, py, 50);
		//Utils.paintText(graphics, "Tipo Tag: "+this.consultBean.getTipoTag(), 5, 50, 50);
		py += 15;
		
		invalidate();
		
	}
	
	protected void drawFocus(Graphics graphics, boolean on) {
		// TODO Auto-generated method stub
		graphics.setColor(colorField);
		
		graphics.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
		graphics.setGlobalAlpha(150);
		graphics.setColor(this.color);
		graphics.setFont(font);
		
		if(Display.getWidth() < 320){
			xFont = 140; 
		}
		
		int py = 5;
		Utils.paintText(graphics, "TAG: "+this.consultBean.getNumero(), 5, py, 55);
		py += 15;
		
		if(Integer.parseInt(this.consultBean.getTipoTag()) != 2){
			Utils.paintText(graphics, "D�gito Verificador: "+this.consultBean.getDv(), 5, py, 50);
			py+=15;
		}
		
		Utils.paintText(graphics, "Etiqueta:  "+this.consultBean.getEtiqueta(), 5, py, 50);
		//Utils.paintText(graphics, "Tipo Tag: "+this.consultBean.getTipoTag(), 5, 50, 50);
		py += 15;
		invalidate();
		
	}
	
	protected boolean navigationClick(int status, int time) {
		// TODO Auto-generated method stub
		int tipo=Dialog.ask(Dialog.D_OK_CANCEL, "�eliminar el TAG "+this.consultBean.getEtiqueta()+"?");
		if(tipo == Dialog.OK){
			MainClass.splashScreen.start();
			String TAGJson="";
			TAGJson="{\"etiqueta\":\""+this.consultBean.getEtiqueta()+"\",\"numero\":\""+this.consultBean.getNumero()+"\",\"dv\":\""+this.consultBean.getDv()+"\",\"tipotag\":\""+this.consultBean.getTipoTag()+"\",\"usuario\":\""+MainClass.userBean.getIdUser()+"\"}";
			System.out.println(TAGJson);
			String json=Crypto.aesEncrypt(MainClass.key,TAGJson);
			new RemoveTagsThread(json).start();
			
			
		}

		return true;
	}
	protected void onUnfocus() {
		// TODO Auto-generated method stub
		super.onUnfocus();
	}
	
	protected void layout(int width, int height) 
	{

		this.setExtent(getPreferredWidth(), getPreferredHeight());		
		
	}
	
	public int getPreferredWidth() {
		// TODO Auto-generated method stub
		return Display.getWidth();
	}
	
	public int getPreferredHeight() {
		// TODO Auto-generated method stub
		return 55;
	}

}

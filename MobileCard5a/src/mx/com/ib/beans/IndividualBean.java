package mx.com.ib.beans;

public class IndividualBean {
	public String nombre,apellidopaterno,apellidomaterno,sexo,FecNac,edocivil,TelCasa,TelOfna,TelCel,mail,Calle,NumExt,NumInt,Col,CP,numPer;

	public String getNumPer() {
		return numPer;
	}

	public void setNumPer(String numPer) {
		this.numPer = numPer;
	}

	public String getNombre() {
		return nombre;
	}

	public IndividualBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public IndividualBean(String nombre, String apellidopaterno,
			String apellidomaterno, String sexo, String fecNac,
			String edocivil, String telCasa, String telOfna, String telCel,
			String mail, String calle, String numExt, String numInt,
			String col, String cP,String num) {
		super();
		this.nombre = nombre;
		this.apellidopaterno = apellidopaterno;
		this.apellidomaterno = apellidomaterno;
		this.sexo = sexo;
		FecNac = fecNac;
		this.edocivil = edocivil;
		TelCasa = telCasa;
		TelOfna = telOfna;
		TelCel = telCel;
		this.mail = mail;
		Calle = calle;
		NumExt = numExt;
		NumInt = numInt;
		Col = col;
		CP = cP;
		numPer=num;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidopaterno() {
		return apellidopaterno;
	}

	public void setApellidopaterno(String apellidopaterno) {
		this.apellidopaterno = apellidopaterno;
	}

	public String getApellidomaterno() {
		return apellidomaterno;
	}

	public void setApellidomaterno(String apellidomaterno) {
		this.apellidomaterno = apellidomaterno;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getFecNac() {
		return FecNac;
	}

	public void setFecNac(String fecNac) {
		FecNac = fecNac;
	}

	public String getEdocivil() {
		return edocivil;
	}

	public void setEdocivil(String edocivil) {
		this.edocivil = edocivil;
	}

	public String getTelCasa() {
		return TelCasa;
	}

	public void setTelCasa(String telCasa) {
		TelCasa = telCasa;
	}

	public String getTelOfna() {
		return TelOfna;
	}

	public void setTelOfna(String telOfna) {
		TelOfna = telOfna;
	}

	public String getTelCel() {
		return TelCel;
	}

	public void setTelCel(String telCel) {
		TelCel = telCel;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getCalle() {
		return Calle;
	}

	public void setCalle(String calle) {
		Calle = calle;
	}

	public String getNumExt() {
		return NumExt;
	}

	public void setNumExt(String numExt) {
		NumExt = numExt;
	}

	public String getNumInt() {
		return NumInt;
	}

	public void setNumInt(String numInt) {
		NumInt = numInt;
	}

	public String getCol() {
		return Col;
	}

	public void setCol(String col) {
		Col = col;
	}

	public String getCP() {
		return CP;
	}

	public void setCP(String cP) {
		CP = cP;
	}

}

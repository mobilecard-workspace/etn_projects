package mx.com.ib.beans;

public class BeneficiarioBean {

	public String Nombre,ApPat,ApMat,FecNac,id_gen,id_edoc,id_par;

	public BeneficiarioBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BeneficiarioBean(String nombre, String apPat, String apMat,
			String fecNac, String id_gen, String id_edoc, String id_par) {
		super();
		Nombre = nombre;
		ApPat = apPat;
		ApMat = apMat;
		FecNac = fecNac;
		this.id_gen = id_gen;
		this.id_edoc = id_edoc;
		this.id_par = id_par;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public String getApPat() {
		return ApPat;
	}

	public void setApPat(String apPat) {
		ApPat = apPat;
	}

	public String getApMat() {
		return ApMat;
	}

	public void setApMat(String apMat) {
		ApMat = apMat;
	}

	public String getFecNac() {
		return FecNac;
	}

	public void setFecNac(String fecNac) {
		FecNac = fecNac;
	}

	public String getId_gen() {
		return id_gen;
	}

	public void setId_gen(String id_gen) {
		this.id_gen = id_gen;
	}

	public String getId_edoc() {
		return id_edoc;
	}

	public void setId_edoc(String id_edoc) {
		this.id_edoc = id_edoc;
	}

	public String getId_par() {
		return id_par;
	}

	public void setId_par(String id_par) {
		this.id_par = id_par;
	}
}

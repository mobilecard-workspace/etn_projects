package mx.com.ib.views;

import mx.com.ib.app.MainClass;
import mx.com.ib.controls.BgManager;
import mx.com.ib.controls.CustomVerticalFieldManager;
import mx.com.ib.controls.GenericImageButtonField;
import net.rim.blackberry.api.browser.Browser;
import net.rim.blackberry.api.browser.BrowserSession;
import net.rim.device.api.system.ApplicationDescriptor;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class AboutScreen extends MainScreen {

	public HorizontalFieldManager hfmMain = null;
	public HorizontalFieldManager hfmTitle = null;

	public CustomVerticalFieldManager customVerticalFieldManager = null;
	public CustomVerticalFieldManager customVerticalFieldManager2 = null;

	public GenericImageButtonField btnBuy = null;
	public GenericImageButtonField btnConsult = null;
	public GenericImageButtonField btnAccount = null;
	public GenericImageButtonField btnPromotions = null;
	public GenericImageButtonField btnExit = null;

	public LabelField version = null;
	public LabelField contacto = null;
	public LabelField proveedor = null;
	public LabelField contacto2 = null;
	public LabelField proveedor2 = null;
	public LabelField privacidad = null;
	public LabelField privacidad2 = null;
	public LabelField web = null;

	public BgManager bgManager = null;
	public Bitmap bField = null;

	public VerticalFieldManager vfmFooter = null;

	public AboutScreen() {
		initVariables();
		// initButtons();
		addFields();

	}

	public void initVariables() {
		TransitionContext transition = new TransitionContext(
				TransitionContext.TRANSITION_SLIDE);
		transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
		transition.setIntAttribute(TransitionContext.ATTR_DIRECTION,
				TransitionContext.DIRECTION_LEFT);
		transition.setIntAttribute(TransitionContext.ATTR_STYLE,
				TransitionContext.STYLE_PUSH);
		UiEngineInstance engine = Ui.getUiEngineInstance();
		engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH,
				transition);
		TransitionContext transition2 = new TransitionContext(
				TransitionContext.TRANSITION_SLIDE);
		transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
		transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION,
				TransitionContext.DIRECTION_RIGHT);
		transition2.setIntAttribute(TransitionContext.ATTR_STYLE,
				TransitionContext.STYLE_PUSH);
		engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP,
				transition2);
		engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
		bgManager = new BgManager(MainClass.BG_GRAY);
		bgManager = new BgManager(MainClass.BG_MENU);
		// title = new LabelField("		MENU", Field.FIELD_HCENTER){
		// protected void layout(int width, int height) {
		// // TODO Auto-generated method stub
		// super.layout(80, height);
		// }
		// }
		// ;
		// hfmMain.add(new NullField(Field.FOCUSABLE));
		hfmMain = new HorizontalFieldManager(Field.FIELD_HCENTER
				| VERTICAL_SCROLL | Field.USE_ALL_WIDTH);
		hfmTitle = new HorizontalFieldManager(Field.FIELD_RIGHT);
		vfmFooter = new VerticalFieldManager(VERTICAL_SCROLL);
		ApplicationDescriptor appDesc = ApplicationDescriptor
				.currentApplicationDescriptor();
		version = new LabelField("Versi�n: " + appDesc.getVersion());
		contacto = new LabelField("Contacto:");
		proveedor = new LabelField("Proveedor: Addcel");
		contacto2 = new LabelField("soporte@addcel.com", Field.FOCUSABLE);
		privacidad = new LabelField("Aviso de Privacidad: ", Field.FOCUSABLE);
		privacidad2 = new LabelField("http://www.addcel.com/privacidad.html",
				Field.FOCUSABLE) {

			public void paint(Graphics g) {

				g.setColor(Color.LIGHTBLUE);
				super.paint(g);
			}

			protected boolean navigationClick(int status, int time) {

				// TODO Auto-generated method stub
				// MainClass.splashScreen.start();
				BrowserSession browserSession;
				browserSession = Browser.getDefaultSession();

				browserSession
						.displayPage("http://www.addcel.com/privacidad.html");

				return true;
			}

			protected void onFocus(int direction) {
				super.onFocus(direction);
				invalidate();
				//
			}

			protected void onUnfocus() {
				super.onUnfocus();
				invalidate();
			}

		};
		privacidad2.setFont(Font.getDefault().derive(Font.UNDERLINED));

		// privacidad2= new
		// LabelField("http://www.addcel.com/privacidad.html",Field.FOCUSABLE);
		contacto2.select(true);
		proveedor2 = new LabelField("Vis�tanos en:");
		web = new LabelField("www.addcel.com", Field.FOCUSABLE) {

			public void paint(Graphics g) {

				g.setColor(Color.LIGHTBLUE);
				super.paint(g);
			}

			protected boolean navigationClick(int status, int time) {

				// TODO Auto-generated method stub
				// MainClass.splashScreen.start();
				BrowserSession browserSession;
				browserSession = Browser.getDefaultSession();

				browserSession.displayPage("http://www.addcel.com");

				return true;
			}

			protected void onFocus(int direction) {
				super.onFocus(direction);
				invalidate();
				//
			}

			protected void onUnfocus() {
				super.onUnfocus();
				invalidate();
			}

		};
		web.select(true);
		version.setFont(MainClass.appFont2);
		contacto.setFont(MainClass.appFont2);
		proveedor.setFont(MainClass.appFont2);
		contacto2.setFont(MainClass.appFont2);
		proveedor2.setFont(MainClass.appFont2);
		privacidad.setFont(MainClass.appFont2);
		privacidad2.setFont(MainClass.appFont2);
		contacto2.setFont(MainClass.appFont2);
		web.setFont(MainClass.appFont2);
		// contacto.select(true);
		customVerticalFieldManager = new CustomVerticalFieldManager(
				Display.getWidth() / 2, Display.getHeight(),
				HorizontalFieldManager.USE_ALL_WIDTH | Field.FIELD_VCENTER);
		customVerticalFieldManager2 = new CustomVerticalFieldManager(
				(Display.getWidth() / 2), Display.getHeight(),
				HorizontalFieldManager.USE_ALL_WIDTH | Field.FIELD_VCENTER);

		bField = Bitmap.getBitmapResource(MainClass.BG_MENU_MC);

	}

	public void addFields() {

		add(new NullField());

		BitmapField bit = new BitmapField(bField);

		bit.setMargin(Display.getHeight() / 2 - bField.getHeight() / 2, 0, 0, 0);

		// bField.setMargin(top, right, bottom, left)

		int suma = Bitmap.getBitmapResource(MainClass.BTN_BUY).getHeight()
				+ Bitmap.getBitmapResource(MainClass.BTN_ACCOUNT).getHeight()
				+ Bitmap.getBitmapResource(MainClass.BTN_PROMOTIONS)
						.getHeight()
				+ Bitmap.getBitmapResource(MainClass.BTN_CONSULT).getHeight();

		version.setMargin(Display.getHeight() / 2 - suma / 2, 0, 0, 0);
		customVerticalFieldManager2.add(bit);
		customVerticalFieldManager.add(version);
		customVerticalFieldManager.add(new LabelField(""));
		customVerticalFieldManager.add(contacto);
		customVerticalFieldManager.add(contacto2);
		customVerticalFieldManager.add(new LabelField(""));
		customVerticalFieldManager.add(proveedor);
		customVerticalFieldManager.add(proveedor2);

		customVerticalFieldManager.add(web);
		customVerticalFieldManager.add(new LabelField(""));
		customVerticalFieldManager.add(privacidad);
		customVerticalFieldManager.add(privacidad2);
		hfmMain.add(customVerticalFieldManager2);
		hfmMain.add(customVerticalFieldManager);

		bgManager.add(hfmMain);

		add(bgManager);
	}

}

package mx.com.ib.views;

import mx.com.ib.app.MainClass;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.RichTextField;
import net.rim.device.api.ui.component.SeparatorField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class ImagePopUpune extends PopupScreen implements FieldChangeListener{
	
	public ButtonField btnOk = null;
	public VerticalFieldManager hfmButton = null;
	public BitmapField bField=null;
	public BitmapField bField1=null;
	public HorizontalFieldManager hfmBtns2 = null;
	public HorizontalFieldManager hfmBtns3 = null;
	public VerticalFieldManager hfmBtns4 = null;

	public ImagePopUpune(String text, long style){
		
		super(new VerticalFieldManager(NO_VERTICAL_SCROLL|Field.FIELD_HCENTER));
		hfmBtns2 = new HorizontalFieldManager(Field.FIELD_HCENTER);
		hfmBtns3 = new HorizontalFieldManager(Field.FIELD_HCENTER);
		hfmBtns4 = new VerticalFieldManager(FOCUSABLE |VERTICAL_SCROLL|Field.FIELD_HCENTER);
		hfmButton = new VerticalFieldManager(FOCUSABLE |VERTICAL_SCROLL|Field.FIELD_HCENTER);
		VerticalFieldManager principal = new VerticalFieldManager();
		
		
		
		btnOk = new ButtonField("Ok", ButtonField.CONSUME_CLICK);
		if(text.equals("CVV2")){
			 bField = new BitmapField(Bitmap.getBitmapResource(MainClass.BG_CVV2),Field.FOCUSABLE);
		}
		if(text.equals("TAG")){
			 bField = new BitmapField(Bitmap.getBitmapResource(MainClass.BG_IAVETAG),Field.FOCUSABLE);
			 bField1 = new BitmapField(Bitmap.getBitmapResource(MainClass.BG_IAVETAG2),Field.FOCUSABLE);
		}
		if(text.equals("Digito_Verificador")){
			 bField = new BitmapField(Bitmap.getBitmapResource(MainClass.BG_IAVEDV),Field.FOCUSABLE);
			 bField1 = new BitmapField(Bitmap.getBitmapResource(MainClass.BG_IAVETAG2),Field.FOCUSABLE);
		}
		if(text.equals("Digito_Verificador_pase")){
			 bField = new BitmapField(Bitmap.getBitmapResource(MainClass.BG_PASE2),Field.FOCUSABLE);
		//	 bField1 = new BitmapField(Bitmap.getBitmapResource(MainClass.BG_IAVETAG2),Field.FOCUSABLE);
		}
		
		if(text.equals("OHL")){
			 bField = new BitmapField(Bitmap.getBitmapResource(MainClass.BG_OHLTAG),Field.FOCUSABLE);
			// bField1 = new BitmapField(Bitmap.getBitmapResource(MainClass.BG_OHLTAG),Field.FOCUSABLE);
		}
		if(text.equals("VIAPASS")){
			 bField = new BitmapField(Bitmap.getBitmapResource(MainClass.BG_VIAPASS),Field.FOCUSABLE);
			 bField1 = new BitmapField(Bitmap.getBitmapResource(MainClass.BG_VIAPASS1),Field.FOCUSABLE);
		}
		if(text.equals("PASE")){
			 bField = new BitmapField(Bitmap.getBitmapResource(MainClass.BG_PASE),Field.FOCUSABLE);
		//	 bField1 = new BitmapField(Bitmap.getBitmapResource(MainClass.BG_PASE2),Field.FOCUSABLE);
		}
		btnOk.setChangeListener(this);
		principal.add(new RichTextField("�qu� es "+text+"?", style){
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				graphics.setFont(MainClass.appFont);
				super.paint(graphics);
			}
		});
		principal.add(new SeparatorField());
		//setBitmap(Bitmap.getBitmapResource(MainClass.FOOTER));
	/*	secundaria.add(new RichTextField(text, style){
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				FontFamily alphaSansFamily;
				Font fuente = null;
				try {
					alphaSansFamily = FontFamily.forName("BBAlpha Serif");
					fuente = alphaSansFamily.getFont(Font.PLAIN, 7, Ui.UNITS_pt);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
					this.setFont(fuente);
				super.paint(graphics);
			}
		});*/
		
		;
		hfmBtns3.add(principal);
		hfmBtns4.add(bField);
		
		if(!text.equals("CVV2")&&!text.equals("OHL")&&!text.equals("PASE")&&!text.equals("Digito_Verificador_pase")){hfmBtns4.add(bField1);}		
		hfmBtns2.add(btnOk);
		hfmButton.add(hfmBtns3);
		
		hfmButton.add(hfmBtns4);
		
		hfmButton.add(hfmBtns2);
		
		
		//secundaria.add(hfmButton);
		add(hfmButton);
		
	}
	
	public boolean keyChar(char key, int status, int time) {
	    //intercept the ESC key - exit the app on its receipt
	    boolean retval = false;
	    switch (key) {

	        case Characters.ESCAPE:
	            close();
	            break;
	        default:
	            retval = super.keyChar(key,status,time);
	    }
	    return retval;
	}

	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		
		if(field == btnOk){
			close();
		}
		
	}
	
	public void close() {
		// TODO Auto-generated method stub
		UiApplication.getUiApplication().popScreen(this);
	}
	
}

package mx.com.ib.views;

import mx.com.ib.app.MainClass;
import mx.com.ib.controls.BannerBitmap;
import mx.com.ib.controls.BgManager;
import mx.com.ib.controls.BorderBasicEditField;
import mx.com.ib.controls.CustomVerticalFieldManager;
import mx.com.ib.controls.GenericImageButtonField;
import mx.com.ib.threads.MontoInterjetThread;
import mx.com.ib.threads.PurchaseInterjetThread;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class InterjetScreen extends MainScreen {
	public BgManager bgManager = null;
	public LabelField description = null;
	public LabelField description2 = null;
	public LabelField reservation = null;
	public LabelField validateReservation = null;
	public LabelField monto = null;
	
	public BorderBasicEditField basicReservation = null;
	public BorderBasicEditField basicMonto = null;
	
	public HorizontalFieldManager hfmMain = null;
	public HorizontalFieldManager hfmTitle = null;
	public HorizontalFieldManager hfmReserva = null;
	
	public VerticalFieldManager vfmFooter = null;
	
	public CustomVerticalFieldManager customVerticalFieldManager = null;
	
	public GenericImageButtonField btnAccept = null;
	public MontoInterjetThread montoInterjetThread = null;
	
	public InterjetScreen(){
		initVariables();
		addFields();
		
	}
	
	public void initVariables(){
		TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
		transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
		transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	    
	    TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	    transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
	    
	    bgManager = new BgManager(MainClass.BG_GRAY);
	    hfmReserva = new HorizontalFieldManager(VerticalFieldManager.FIELD_HCENTER|VERTICAL_SCROLL){
	    	public int getPreferredWidth(){
	    		return Display.getWidth();
	    	}
	    	
	    	protected void sublayout(int maxWidth, int maxHeight){
	    		super.sublayout(getPreferredWidth(), maxHeight);
	    	}
	    };
	    
	    hfmTitle = new HorizontalFieldManager(Field.FIELD_RIGHT);
	    //hfmBtns = new HorizontalFieldManager(Field.FIELD_HCENTER);
	    
	    vfmFooter = new VerticalFieldManager(VERTICAL_SCROLL){
	    	public int getPreferredWitdh(){
	    		return Display.getWidth();
	    	}
	    	
	    	protected void subLayout(int maxWidth, int maxHeight) {
				super.sublayout(getPreferredWitdh(), maxHeight);
			}
	    };
	    
	    customVerticalFieldManager = new CustomVerticalFieldManager(Display.getWidth()-20,Display.getHeight()-30, HorizontalFieldManager.FIELD_LEFT | VERTICAL_SCROLL);
	    description2 = new LabelField("Descripci�n de la compra:",Field.FIELD_HCENTER);
		description = new LabelField("Interjet");
		reservation = new LabelField("Ingrese su n�mero de reservaci�n:");
		monto = new LabelField("Monto a pagar:");
		description2.setFont(MainClass.appFont);
		
		basicReservation = new BorderBasicEditField("","", 8, Display.getWidth(), Field.FOCUSABLE|EditField.FILTER_DEFAULT);
		//basicMonto = new BorderBasicEditField("", "", 8, Display.getWidth(), Field.FOCUSABLE | EditField.FILTER_REAL_NUMERIC);
		basicMonto = new BorderBasicEditField("", "", 33, Display.getWidth(), BasicEditField.NON_FOCUSABLE);
		
		btnAccept = new GenericImageButtonField(MainClass.BTN_COMPRAR,MainClass.BTN_COMPRAR_OVER,""){
			
			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub
				checkFields();
				return true;
			}
			
		};
		
		validateReservation = new LabelField("Validar", Field.FOCUSABLE){
			public void paint(Graphics g){
				
				
				g.setColor(MainClass.BG_ORANGE);
				super.paint(g);
			}
			protected boolean navigationClick(int status, int time) {
				if(!basicReservation.equals("")){
					MainClass.splashScreen.start();
					new MontoInterjetThread(basicReservation.getText().trim(), basicMonto).run();
				}
				return true;
			}
			protected void onFocus(int direction) {  
		        super.onFocus(direction);
		        invalidate(); 
		    }   
		 
			 protected void onUnfocus() {  
		           super.onUnfocus();
		           invalidate();  
		    } 
		};
	}
	
	public void addFields(){
		setBanner(new BannerBitmap(MainClass.HD_COMPRAS));
		customVerticalFieldManager.add(hfmTitle);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(description2);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(description);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		
		customVerticalFieldManager.add(reservation);
		customVerticalFieldManager.add(basicReservation);
		customVerticalFieldManager.add(validateReservation);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(monto);
		customVerticalFieldManager.add(basicMonto);

		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(btnAccept);
		
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		
		bgManager.add(customVerticalFieldManager);
		add(bgManager);		
		
	}
	
	public void checkFields(){
		if(basicReservation.getText().trim().equals("")){
			Dialog.alert("Debe ingresar su n�mero de reservaci�n.");
		} else if(basicMonto.getText().trim().equals("")){
			Dialog.alert("Debe obtener monto a pagar.");
		} else {
			
			try{
				
				String sMonto = basicMonto.getText();
				
				double dMonto = Double.parseDouble(sMonto);
				MainClass.splashScreen.start();
				new PurchaseInterjetThread(basicReservation.getText(), sMonto).run();

			} catch(NumberFormatException e){
				Dialog.alert("El monto es inv�lido.");
			}
			

			
			/*
			if (isNumber(basicMonto.getText())){
				MainClass.splashScreen.start();
				new PurchaseInterjetThread(basicReservation.getText(), basicMonto.getText()).run();
			} else {
				Dialog.alert("La cantidad no es valida.");
			}
			*/
		}
	}	
	
	public void close(){
		UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
    }
	    
	protected boolean onSavePrompt() 
	{
		return true;
	}
	
	private static final int NUMBER_MAX_LENGTH = String.valueOf(Long.MAX_VALUE).length();

	public static boolean isNumber(String string) {
	    if (string == null || string.equals("")) {
	        return false;
	    }
	    if (string.length() >= NUMBER_MAX_LENGTH) {
	        try {
	            Long.parseLong(string);
	        } catch (Exception e) {
	            return false;
	        }
	    } else {
	        int i = 0;
	        if (string.charAt(0) == '-') {
	            if (string.length() > 1) {
	                i++;
	            } else {
	                return false;
	            }
	        }
	        for (; i < string.length(); i++) {
	            if (!Character.isDigit(string.charAt(i))) {
	                return false;
	            }
	        }
	    }
	    return true;
	}
	
	
}

package mx.com.ib.views;


import java.util.Calendar;
import crypto.Crypto;
import mx.com.ib.app.MainClass;
import mx.com.ib.beans.GeneralBean;
import mx.com.ib.controls.BannerBitmap;
import mx.com.ib.controls.BgManager;
import mx.com.ib.controls.BorderBasicEditField;
import mx.com.ib.controls.BorderPasswordField;
import mx.com.ib.controls.CustomVerticalFieldManager;
import mx.com.ib.controls.GenericImageButtonField;
import mx.com.ib.threads.GPSThread;
import mx.com.ib.threads.PurchaseIAVEThread;
import mx.com.ib.threads.PurchaseViaPass;
import mx.com.ib.utils.Utils;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class ViaPassScreen extends MainScreen {
	
public BgManager bgManager = null;	
	
	public LabelField description = null;
	public LabelField description2 = null;
	public LabelField clave = null;
	public LabelField pin = null;
	public LabelField vigencia = null;	
	public LabelField password = null, helpPass=null;
	public LabelField cvv2 = null;
	public LabelField img1 = null;
	public LabelField img2 = null;
	public LabelField img3 = null;	
	public LabelField Message = null;
	
	public BorderBasicEditField basicClave = null;
	public BorderBasicEditField basicPin = null;	
	public BorderBasicEditField basicCvv2 = null;
	
	public BorderPasswordField basicPassword = null;
	
	public HorizontalFieldManager hfmMain = null;	
	public HorizontalFieldManager hfmBtns = null;	
	public HorizontalFieldManager Cvv2HFM = null;
	public HorizontalFieldManager tagHFM = null;
	public HorizontalFieldManager dvHFM = null;
	public HorizontalFieldManager hfmPass = null;
	
	public VerticalFieldManager vfmFooter = null;	
	public CustomVerticalFieldManager customVerticalFieldManager = null;
	
//	public GenericImageButtonField btnRegister = null;
	public GenericImageButtonField btnAccept = null;
	public GenericImageButtonField btnTags = null;
	
	public GeneralBean generalBean = null;
	
	public String yearsCredit[] = null;
	public String monthsCredit[] = null;
	public String TagNames[] = null;
	
	public ObjectChoiceField choicesYearsCredit  = null;
	public ObjectChoiceField choicesMonthsCredit  = null;
	public ObjectChoiceField choicesTagNames  = null;
	
	public int monthCredit = 0;
	public int yearCredit = 0;
		
	public ViaPassScreen(GeneralBean generalBean){
		
		this.generalBean = generalBean;		
		initVariables();		
		addFields();		
	}
	
	public void initVariables(){
		
	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	    
	    TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	    transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
	    
		bgManager = new BgManager(MainClass.BG_GRAY);
		Cvv2HFM   = new HorizontalFieldManager(Field.FIELD_LEFT);
		tagHFM    = new HorizontalFieldManager(Field.FIELD_LEFT);
		dvHFM     = new HorizontalFieldManager(Field.FIELD_LEFT);
		hfmPass = new HorizontalFieldManager(Field.FIELD_LEFT);
		new GPSThread(1,5,30,15,7200).start();
		
		hfmMain = new HorizontalFieldManager(VerticalFieldManager.FIELD_HCENTER|VERTICAL_SCROLL){
			public int getPreferredWidth() {
				// TODO Auto-generated method stub
				return Display.getWidth();
			}
			
			protected void sublayout(int maxWidth, int maxHeight) {
				// TODO Auto-generated method stub
				super.sublayout(getPreferredWidth(), maxHeight);
			}
		};
		
		hfmBtns = new HorizontalFieldManager(Field.FIELD_HCENTER);
		customVerticalFieldManager = new CustomVerticalFieldManager(Display.getWidth()-20,Display.getHeight(), HorizontalFieldManager.FIELD_LEFT| VERTICAL_SCROLL);
		
		vfmFooter = new VerticalFieldManager(VERTICAL_SCROLL){
			public int getPreferredWidth() {
				// TODO Auto-generated method stub
				return Display.getWidth();
			}
			
			protected void sublayout(int maxWidth, int maxHeight) {
				// TODO Auto-generated method stub
				super.sublayout(getPreferredWidth(), maxHeight);
			}
		};
		
	helpPass =new LabelField("? ",Field.FOCUSABLE){
			
			public void paint(Graphics g){
				
				
				g.setColor(MainClass.BG_ORANGE);
				super.paint(g);
			}
			protected boolean navigationClick(int status, int time) {
				Dialog.inform("La contrase�a debe tener m�nimo 8 caracteres y m�ximo 12");
				//UiApplication.getUiApplication().pushScreen(new RecUserScreen());
				return true;
			}
			protected void onFocus(int direction) {  
		        super.onFocus(direction);
		        invalidate(); 
		     //   
		    }   
		 
			 protected void onUnfocus() {  
		           super.onUnfocus();
		           invalidate();  
		    } 
			
			
		};
		
		
		
		
		description = new LabelField(MainClass.textTitleConfirm);
		description2 = new LabelField("Descripci�n de la compra:",Field.FIELD_HCENTER);
		clave = new LabelField("N�mero de TAG:");
		pin = new LabelField("N�mero de Cliente: ");
		vigencia= new LabelField("Vigencia de la Tarjeta:");
		password = new LabelField("Digite su contrase�a de MobileCard: ");
		Message = new LabelField("Todas tus recargas VIAPASS tendr�n una comisi�n de $"+MainClass.COMISION_VIAPASS+"0 MX con IVA INCLUIDO.",Field.FIELD_RIGHT){			
			public void paint(Graphics graphics){				
				int g = graphics.getColor();
				graphics.setColor(MainClass.BG_ORANGE);
				super.paint(graphics);
			}
			protected void onFocus(int direction) {  
		        super.onFocus(direction);
		        invalidate(); 
		     //   
		    }   
		 
			 protected void onUnfocus() {  
		           super.onUnfocus();
		           invalidate();  
		    } 
		};
	
		cvv2 = new LabelField("CVV2 Tarjeta: ");
		Message.setFont(MainClass.appFont1);
		//clave.setFont(MainClass.appFont);
		//pin.setFont(MainClass.appFont);
		
		
		basicClave = new BorderBasicEditField("","", 12, Display.getWidth()-20, Field.FOCUSABLE|EditField.FILTER_NUMERIC){
			public void paint(Graphics g){
				int c = g.getColor();
				
				if(!this.isEditable()){
					g.setColor(0x888888);
					g.fillRect(0, 0, getWidth(), getHeight());	
				}
				
				g.setColor(c);
				super.paint(g);
				
				
			}
		};
		basicPin = new BorderBasicEditField("","", 8, Display.getWidth()-20, Field.FOCUSABLE|EditField.FILTER_NUMERIC){
			public void paint(Graphics g){
				int c = g.getColor();
				
				if(!this.isEditable()){
					g.setColor(0x888888);
					g.fillRect(0, 0, getWidth(), getHeight());	
				}
				
				g.setColor(c);
				super.paint(g);
				
				
			}
		};
		
		basicClave.setEditable(false);
		basicPin.setEditable(false);
		
		basicPassword = new BorderPasswordField("","", 12, Display.getWidth()-20, Field.FOCUSABLE);
		basicCvv2 = new BorderBasicEditField("","", 4, Display.getWidth()-20, Field.FOCUSABLE|EditField.FILTER_NUMERIC);
		
		
		btnAccept = new GenericImageButtonField(MainClass.BTN_COMPRAR,MainClass.BTN_COMPRAR_OVER,""){
			
			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub				
				// UiApplication.getUiApplication().pushScreen(new FolioScreen());
				checkFields();
				return true;
			}			
		};
		
		btnTags = new GenericImageButtonField(MainClass.BTN_COMPRAR,MainClass.BTN_COMPRAR_OVER,""){
			
			protected boolean navigationClick(int status, int time) {
			
				return true;
			}			
		};
		
		yearsCredit = new String[13];
		Calendar c = Calendar.getInstance();
		
		yearsCredit = new String[13];
		
		for(int i = 0; i < 13; i++){
			yearsCredit[i] = new String(Integer.toString(c.get(Calendar.YEAR)+i));
		}
		
		monthsCredit = new String[]{"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		choicesMonthsCredit  = new ObjectChoiceField("Mes Vencimiento: ", monthsCredit){
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				super.paint(graphics);
				invalidate();
			}
			
			protected void drawFocus(Graphics graphics, boolean on) {
				// TODO Auto-generated method stub
				super.drawFocus(graphics, on);
				invalidate();
			}
		};
		
		choicesYearsCredit  = new ObjectChoiceField("A�o Vencimiento: ", yearsCredit){
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				super.paint(graphics);
				invalidate();
			}
			
			protected void drawFocus(Graphics graphics, boolean on) {
				// TODO Auto-generated method stub
				super.drawFocus(graphics, on);
				invalidate();
			}
		};
	    img1 =new LabelField("�qu� es esto?",Field.FOCUSABLE){
			
			public void paint(Graphics g){
				
				
				g.setColor(Color.ORANGE);
				super.paint(g);
			}
			protected boolean navigationClick(int status, int time) {
				
				// TODO Auto-generated method stub
				MainClass.splashScreen.start();
				MainClass.splashScreen.remove();
				UiApplication.getUiApplication().pushScreen(new ImagePopUpune("CVV2", Field.FOCUSABLE));
				return true;
			}
			protected void onFocus(int direction) {  
		        super.onFocus(direction);
		        invalidate(); 
		     //   
		    }   
		 
			 protected void onUnfocus() {  
		           super.onUnfocus();
		           invalidate();  
		    } 
			
			
		};
		img1.setFont(Font.getDefault().derive(Font.UNDERLINED));
		
	    img2 =new LabelField("�qu� es esto?",Field.FOCUSABLE){
			
			public void paint(Graphics g){
				
				
				g.setColor(Color.ORANGE);
				super.paint(g);
			}
			protected boolean navigationClick(int status, int time) {
				
				// TODO Auto-generated method stub
				MainClass.splashScreen.start();
				MainClass.splashScreen.remove();
				UiApplication.getUiApplication().pushScreen(new ImagePopUpune("VIAPASS", Field.FOCUSABLE));
				return true;
			}
			protected void onFocus(int direction) {  
		        super.onFocus(direction);
		        invalidate(); 
		     //   
		    }   
		 
			 protected void onUnfocus() {  
		           super.onUnfocus();
		           invalidate();  
		    } 
			
			
		};
		img2.setFont(Font.getDefault().derive(Font.UNDERLINED));

		img3 =new LabelField("�qu� es esto?",Field.FOCUSABLE){
			
			public void paint(Graphics g){
				
				
				g.setColor(Color.ORANGE);
				super.paint(g);
			}
			protected boolean navigationClick(int status, int time) {
				
				// TODO Auto-generated method stub
				
				UiApplication.getUiApplication().pushScreen(new ImagePopUpune("VIAPASS", Field.FOCUSABLE));
				return true;
			}
			protected void onFocus(int direction) {  
		        super.onFocus(direction);
		        invalidate(); 
		     //   
		    }   
		 
			 protected void onUnfocus() {  
		           super.onUnfocus();
		           invalidate();  
		    } 
			
			
		};
		img3.setFont(Font.getDefault().derive(Font.UNDERLINED));
		
		if(MainClass.TagsBeansbyUser.length!=0){
			 TagNames = new String[MainClass.TagsBeansbyUser.length];
				
				for(int i = 0; i < MainClass.TagsBeansbyUser.length; i++){
					
					TagNames[i] = MainClass.TagsBeansbyUser[i].getEtiqueta();
				}
				
				choicesTagNames = new ObjectChoiceField("Seleccione Tag: ", TagNames){
					protected void paint(Graphics graphics) {
						// TODO Auto-generated method stub
						super.paint(graphics);
						invalidate();
					}
					
					protected void drawFocus(Graphics graphics, boolean on) {
						// TODO Auto-generated method stub
						super.drawFocus(graphics, on);
						invalidate();
					}
				};
				
				choicesTagNames.setChangeListener(new FieldChangeListener(){

					public void fieldChanged(Field field, int context) {
						// TODO Auto-generated method stub
						System.out.println(context);
							if(context==2){
								if(!MainClass.TagsBeansbyUser[choicesTagNames.getSelectedIndex()].getUsuario().equals("-1")&&!MainClass.TagsBeansbyUser[choicesTagNames.getSelectedIndex()].getUsuario().equals("9999999999999"))
								{
									basicClave.setText(MainClass.TagsBeansbyUser[choicesTagNames.getSelectedIndex()].getNumero());
									basicPin.setText(MainClass.TagsBeansbyUser[choicesTagNames.getSelectedIndex()].getDv());
									
									basicClave.setEditable(false);
									basicPin.setEditable(false);
									basicClave.setVisualState(Field.VISUAL_STATE_DISABLED);
									basicPin.setVisualState(Field.VISUAL_STATE_DISABLED);
									
								}else{
									basicClave.setEditable(true);
									basicPin.setEditable(true);
									basicClave.setVisualState(Field.VISUAL_STATE_NORMAL);
									basicPin.setVisualState(Field.VISUAL_STATE_NORMAL);
									basicClave.setText("");
									basicPin.setText("");
								}
								
							}
					}
					
				});
		 }
		
		
		
		
	}
	
	public void addFields(){
		
		setBanner(new BannerBitmap(MainClass.HD_COMPRAS));
		customVerticalFieldManager.add(description2);
		
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(description);
		
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(Message);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		 if(MainClass.TagsBeansbyUser.length!=0){
			customVerticalFieldManager.add(choicesTagNames);
			customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE)); 
		 }
		
		tagHFM.add(clave);
		tagHFM.add(img2);
		customVerticalFieldManager.add(tagHFM);
		customVerticalFieldManager.add(basicClave);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		
		dvHFM.add(pin);
		dvHFM.add(img3);
		Cvv2HFM.add(cvv2);
		Cvv2HFM.add(img1);
		hfmPass.add(password);
		hfmPass.add(helpPass);
		hfmBtns.add(btnAccept);
		customVerticalFieldManager.add(dvHFM);
		customVerticalFieldManager.add(basicPin);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));		
		customVerticalFieldManager.add(Cvv2HFM);
		customVerticalFieldManager.add(basicCvv2);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(vigencia);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(choicesMonthsCredit);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(choicesYearsCredit);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));		
		customVerticalFieldManager.add(hfmPass);
		customVerticalFieldManager.add(basicPassword);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));		
		customVerticalFieldManager.add(hfmBtns);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));				
		bgManager.add(customVerticalFieldManager);
		add(bgManager);	
		
	}

	public void checkFields(){
		
		int _yearCredit = choicesYearsCredit.getSelectedIndex();
		int _monthCredit = choicesMonthsCredit.getSelectedIndex()+1;
		String stringMonth = "";
		
		if(_monthCredit < 10){			
			stringMonth = "0" + _monthCredit;			
		}else{			
			stringMonth = String.valueOf(_monthCredit);			
		}
		
		if(basicPin.getText().trim().equals("")){			
			Dialog.alert("Debe de ingresar su n�mero de cliente.");			
		}/*else if(basicClave.getText().trim().equals("")){			
			Dialog.alert("Debe de ingresar su clave de tarjeta.");			
		}*/else if(basicPassword.getText().trim().equals("")){			
			Dialog.alert("Debe de ingresar su contrase�a.");			
		}else if(basicCvv2.getText().trim().equals("")){			
			Dialog.alert("Debe de ingresar su n�mero CVV.");			
		}else if(!Utils.validDate(""+_monthCredit, ""+_yearCredit)){
        	Dialog.alert( "La vigencia es incorrecta." );
        }else if(basicPassword.getText().trim().length() < 8 ){        	
        	Dialog.alert( "Tu contrase�a debe ser de al menos 8 caract�res.");        	
        }
		else{			
			MainClass.splashScreen.start();
			new PurchaseViaPass(getJSON(),basicPassword.getText().trim()).run();
		}		
	}
	
	public String getJSON(){
		
		String json = "";
		
					
			yearCredit = choicesYearsCredit.getSelectedIndex();
			monthCredit = choicesMonthsCredit.getSelectedIndex()+1;
			
//			String numMonth = choicesMonthsCredit.getSelectedIndex()+1;			
			String stringMonth = "";
			
			if(monthCredit < 10){				
				stringMonth = "0" + monthCredit;				
			}else{				
				stringMonth = String.valueOf(monthCredit);				
			}
			
			String vigency = yearsCredit[choicesYearsCredit.getSelectedIndex()];			
			vigency = vigency.substring((vigency.length()-2), vigency.length());			
			String js = "{\"login\":\""+MainClass.userBean.getLogin()+"\",\"password\":"+"\""+basicPassword.getText().trim()+"\",\"tipo\":\""+MainClass.TIPO+"\",\"modelo\":\""+MainClass.MODELO+"\",\"software\":\""+MainClass.SOFTWARE+"\",\"key\":\""+Utils.getImei()+"\",\"imei\":\""+Utils.getImei()+"\",\"cvv2\":\""+basicCvv2.getText().trim()+"\",\"vigencia\":\""+stringMonth+vigency+"\",\"cx\":\""+MainClass.X+"\",\"cy\":\""+MainClass.Y+"\",\"producto\":\""+this.generalBean.getClave()+"\",\"tarjeta\":\""+basicClave.getText().trim()+"\",\"pin\":\""+basicPin.getText().trim()+"\"}";
	
			String pass=basicPassword.getText().trim();
			json = Crypto.aesEncrypt(Utils.parsePass(pass),js.toString());
			String json2= Utils.mergeStr(json,pass);
		
		return json2;
		
	}
	
	public void close(){		
		UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
    }
	    
	protected boolean onSavePrompt(){
		return true;
	}
}

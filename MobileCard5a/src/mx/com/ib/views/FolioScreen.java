package mx.com.ib.views;

import mx.com.ib.app.MainClass;
import mx.com.ib.beans.PurchaseBean;
import mx.com.ib.controls.BannerBitmap;
import mx.com.ib.controls.BgManager;
import mx.com.ib.controls.CustomVerticalFieldManager;
import mx.com.ib.controls.GenericImageButtonField;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.ActiveRichTextField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class FolioScreen extends MainScreen{
	
	public HorizontalFieldManager hfmMain = null;
	public HorizontalFieldManager hfmImage = null;
	public HorizontalFieldManager hfmBtns = null;
//	public HorizontalFieldManager hfmTitle = null;
	
	public CustomVerticalFieldManager customVerticalFieldManager = null;
	
	public VerticalFieldManager vfmFooter = null;
	
	public GenericImageButtonField btnAccept = null;
	
	public LabelField title = null;
	public LabelField title2 = null;
	public LabelField folio = null;
	
	public LabelField numFolio = null;

	
	public LabelField text  = null;
	public LabelField textIAVE  = null;
	public ActiveRichTextField note  = null;
//	public LabelField terms = null;
	
//	public Bitmap bitmap = null;
	
	//public BitmapField bitmapField = null;
	
	public BgManager bgManager = null;	
	public PurchaseBean purchaseBean = null;
	
	public FolioScreen(PurchaseBean purchaseBean){
		
		this.purchaseBean = purchaseBean;
		
		initVariables();
		
		addFields();
		
	}
	
	public void initVariables(){
		
	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	    
	    TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	    transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	     
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
	    
		bgManager = new BgManager(MainClass.BG_GRAY);
		
		//bitmap = Bitmap.getBitmapResource("tarjetota.PNG");
		
		//bitmapField = new BitmapField(bitmap);
		
		hfmMain = new HorizontalFieldManager(Field.FIELD_HCENTER|VERTICAL_SCROLL);
		hfmImage = new HorizontalFieldManager(Field.FIELD_HCENTER);
		hfmBtns = new HorizontalFieldManager(Field.FIELD_HCENTER);
		//hfmTitle = new HorizontalFieldManager(Field.FIELD_RIGHT);
		
		customVerticalFieldManager = new CustomVerticalFieldManager(Display.getWidth(),Display.getHeight(),Field.USE_ALL_WIDTH | HorizontalFieldManager.FIELD_LEFT|VERTICAL_SCROLL);
		
		title = new LabelField("�Tu Compra ha sido realizada!", Field.FOCUSABLE|Field.FIELD_HCENTER);
		title2 = new LabelField("No ha sido posible completar la Compra:", Field.FOCUSABLE|Field.FIELD_HCENTER);
		folio = new LabelField("Autorizaci�n bancaria: ", Field.NON_FOCUSABLE|Field.FIELD_HCENTER);
		
		numFolio = new LabelField(this.purchaseBean.getFolio(), Field.NON_FOCUSABLE|Field.FIELD_HCENTER);

		text = new LabelField(this.purchaseBean.getMessage(), Field.NON_FOCUSABLE|Field.FIELD_HCENTER);
		
		textIAVE = new LabelField("Tu recarga ser� activada en 30 minutos para todas las casetas IAVE de la rep�blica mexicana.", Field.FOCUSABLE|Field.FIELD_HCENTER){
			public void paint(Graphics graphics){				
				int g = graphics.getColor();
				graphics.setColor(MainClass.BG_ORANGE);
				super.paint(graphics);
			}
		};
		note = new ActiveRichTextField("Dudas o aclaraciones cont�ctanos a soporte@addcel.com o al tel�fono en D.F. y �rea metropolitana 52575140 � del interiorde la rep�blica al 018001223324",Field.FOCUSABLE);
		note.select(true);
		title.setFont(MainClass.alphaSansFamily.getFont(Font.PLAIN, 10, Ui.UNITS_pt));
		title2.setFont(MainClass.alphaSansFamily.getFont(Font.PLAIN, 10, Ui.UNITS_pt));
		folio.setFont(MainClass.appFont);
	
		numFolio.setFont(MainClass.appFont);
		text.setFont(MainClass.appFont);
		note.setFont(MainClass.appFont1);
		textIAVE.setFont(MainClass.appFont1);
		//textIAVE.setFont(MainClass.appFont1);
		vfmFooter = new VerticalFieldManager(VERTICAL_SCROLL);
		
		btnAccept = new GenericImageButtonField(MainClass.BTN_CONTINUE_OVER ,MainClass.BTN_CONTINUE,""){			
			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub				
				close();
				return true;
			}			
		};		
		
	}
	
	public void close()
    {
		if(this.purchaseBean.getResult().equals("1"))
		{
			System.out.println("compra exitosa");
			UiApplication.getUiApplication().pushScreen(new MenuScreen());
		}
		else
		{
			System.out.println("compra fallida");
			UiApplication.getUiApplication().popScreen(this);
		}
		
    }
	
	public void addFields(){
		
		setBanner(new BannerBitmap(MainClass.HD_CONFIRMACION));
		 
		
		
		if(!this.purchaseBean.getResult().equals("1")){ // Compra no exitosa
			customVerticalFieldManager.add(title2);
			customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
			customVerticalFieldManager.add(text);
			customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));			
		}else{  // Compra exitosa
			customVerticalFieldManager.add(title);
			customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
			customVerticalFieldManager.add(folio);
			customVerticalFieldManager.add(numFolio);
			customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
			customVerticalFieldManager.add(text);		
			customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		}		
		
		
		
		//hfmImage.add(bitmapField);		
		customVerticalFieldManager.add(hfmImage);		
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		
		if(this.purchaseBean.getResult().equals("1")&&MainClass.textTitleConfirm.indexOf("IAVE") != -1){			
		// IAVE
				customVerticalFieldManager.add(textIAVE);
				customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));		
		}				
		hfmBtns.add(btnAccept);		
		customVerticalFieldManager.add(hfmBtns);		
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));		
		customVerticalFieldManager.add(note);
		hfmMain.add(customVerticalFieldManager);		
		vfmFooter.add(hfmMain);			
		bgManager.add(vfmFooter);
		setStatus(new BannerBitmap());
		add(bgManager);		
	}
	
	public boolean onClose() {
		// TODO Auto-generated method stub
		if(!this.purchaseBean.getFolio().equals(""))
		{
		
			UiApplication.getUiApplication().pushScreen(new MenuScreen());
		}
		else
		{
		
			UiApplication.getUiApplication().popScreen(this);
		}
		return true;
	}
	
	protected boolean onSavePrompt() {
		return true;
	}

}

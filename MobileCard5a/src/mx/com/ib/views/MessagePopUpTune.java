package mx.com.ib.views;

import mx.com.ib.app.MainClass;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.FontFamily;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.RichTextField;
import net.rim.device.api.ui.component.SeparatorField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class MessagePopUpTune extends PopupScreen implements FieldChangeListener{
	
	public ButtonField btnOk = null;
	public HorizontalFieldManager hfmButton = null;

	public MessagePopUpTune(String text, long style){
		
		super(new VerticalFieldManager(NO_VERTICAL_SCROLL));
		
		hfmButton = new HorizontalFieldManager(Field.FIELD_HCENTER);
		VerticalFieldManager principal = new VerticalFieldManager(FOCUSABLE);
		
		VerticalFieldManager secundaria = new VerticalFieldManager(FOCUSABLE |VERTICAL_SCROLL);
		btnOk = new ButtonField("Ok", ButtonField.CONSUME_CLICK);
		
		btnOk.setChangeListener(this);
		principal.add(new RichTextField("TERMINOS Y CONDICIONES", style){
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				graphics.setFont(MainClass.appFont);
				super.paint(graphics);
			}
		});
		principal.add(new SeparatorField());
		secundaria.add(new RichTextField(text, style){
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				FontFamily alphaSansFamily;
				Font fuente = null;
				try {
					alphaSansFamily = FontFamily.forName("BBAlpha Serif");
					fuente = alphaSansFamily.getFont(Font.PLAIN, 7, Ui.UNITS_pt);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
					this.setFont(fuente);
				super.paint(graphics);
			}
		});
		
		hfmButton.add(btnOk);
		
		secundaria.add(hfmButton);
		add(principal);
		add(secundaria);
		
	}
	
	public boolean keyChar(char key, int status, int time) {
	    //intercept the ESC key - exit the app on its receipt
	    boolean retval = false;
	    switch (key) {

	        case Characters.ESCAPE:
	            close();
	            break;
	        default:
	            retval = super.keyChar(key,status,time);
	    }
	    return retval;
	}

	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		
		if(field == btnOk){
			close();
		}
		
	}
	
	public void close() {
		// TODO Auto-generated method stub
		UiApplication.getUiApplication().popScreen(this);
	}
	
}

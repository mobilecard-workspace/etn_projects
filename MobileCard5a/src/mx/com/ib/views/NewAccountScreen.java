package mx.com.ib.views;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import crypto.Crypto;
import mx.com.ib.app.MainClass;
import mx.com.ib.beans.UserBean;
import mx.com.ib.controls.BannerBitmap;
import mx.com.ib.controls.BgManager;
import mx.com.ib.controls.BorderBasicEditField;
import mx.com.ib.controls.BorderPasswordField;
import mx.com.ib.controls.CustomVerticalFieldManager;
import mx.com.ib.controls.GenericImageButtonField;
import mx.com.ib.controls.HorizontalFieldManagerS;
import mx.com.ib.threads.RegisterThread;
import mx.com.ib.threads.Tags2Thread;
import mx.com.ib.threads.TagsThread;
import mx.com.ib.threads.TermsThread;
import mx.com.ib.threads.UpdateUserThread;
import mx.com.ib.utils.Utils;
import net.rim.device.api.i18n.SimpleDateFormat;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.CheckboxField;
import net.rim.device.api.ui.component.DateField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.EmailAddressEditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.component.PasswordEditField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class NewAccountScreen extends MainScreen {
	
	public String myUser = "";
	public String myPswd = "";
	
	public CheckboxField acceptTerms = null;
	
	public Calendar c;
	
	public LabelField user = null,helpUser=null,helpPass=null,helpPhone=null,helpPhone1=null;
	public LabelField password = null;
	public LabelField passwordConfirm = null;
	public LabelField mail = null;
	public LabelField mailConfirm = null;
	public LabelField title = null;
	public LabelField refresh = null;
	public LabelField name = null;
	public LabelField lastNames = null;
	public LabelField lastNamesM = null;
	public LabelField birthDate = null;	
	public LabelField address = null;
	public LabelField numCel,numCel1 = null;
	public LabelField credit = null;
	public LabelField creditDate = null;	
	public LabelField birthDate1 = null;
	public LabelField telefonocasa = null;	
	public LabelField telefonooficina = null;
	
	public BorderBasicEditField basicUser = null;	
	public BorderBasicEditField basicName = null;
	public BorderBasicEditField basicLastNames = null;
	public BorderBasicEditField basicLastNamesM = null;
	public BorderBasicEditField basicBirthDate = null;
	public BorderBasicEditField basicAddress = null;
	public BorderBasicEditField basicNumCel,basicNumCel1 = null;
	public BorderBasicEditField basicCredit = null;
	public BorderBasicEditField basicBank = null;
	public BorderBasicEditField basicCreditDate = null;
	public BorderBasicEditField basictelcasa = null;
	public BorderBasicEditField basicteloficina = null;
	
	
	//direccion
	public LabelField ciudad = null;
	public LabelField calle = null;
	public LabelField numext = null;	
	public LabelField numint = null;
	public LabelField colonia = null;	
	public LabelField cp = null;
	
	public LabelField cpVisa = null;
	public LabelField domVisa = null;
	
	public LabelField cpAmex = null;
	
	public BorderBasicEditField basicCiudad = null;
	public BorderBasicEditField basicCalle = null;
	public BorderBasicEditField basicNumEx = null;
	public BorderBasicEditField basicNumInt = null;
	public BorderBasicEditField basicColonia = null;
	public BorderBasicEditField basicCP = null;
	
	public BorderBasicEditField basicCpVisa = null;
	public BorderBasicEditField basicDomVisa = null;
	
	public BorderBasicEditField basicCpAmex = null;
	
	public BorderPasswordField basicPassword = null;
	public BorderPasswordField basicPasswordConfirm = null;
	
	public BorderBasicEditField basicMail = null;
	public BorderBasicEditField basicMailConfirm = null;
	
	
	public EmailAddressEditField email;
	
	public PasswordEditField pswd;
	public PasswordEditField confirmPswd;
	
	public VerticalFieldManager vfMain = null;
	
	public HorizontalFieldManager registerManager;	
	public HorizontalFieldManager hfmChoices = null;
	public HorizontalFieldManager hfmUser = null;
	public HorizontalFieldManager hfmPass = null;
	public HorizontalFieldManager hfmPhone,hfmPhone1 = null;
	public HorizontalFieldManager hfmTags = null;
	public HorizontalFieldManager hfmTags2 = null;
	public HorizontalFieldManager hfmTagsf = null;
	
	public HorizontalFieldManagerS hfmBtns = null;
	
	public CustomVerticalFieldManager customVerticalFieldManager = null;
	public CustomVerticalFieldManager creditVerticalFieldManager = null;
	public CustomVerticalFieldManager creditDateVerticalFieldManager = null;
	
	
	public BgManager bgManager = null;
	
	public GenericImageButtonField btnContinue = null;
	public GenericImageButtonField btnSave = null;
	public GenericImageButtonField btnExit = null;
	public GenericImageButtonField btnPswd = null;
	public GenericImageButtonField btnTags = null;
	public GenericImageButtonField btnTags2 = null;
	public LabelField btnTerms=null;
	public LabelField lblTags=null;
	
	public int widthManager = 0;
	public int day = 0;
	public int month = 0;
	public int year = 0;
	
	public String creditNames[] = null;
	public String edoNames[] = null;
	public String bankNames[] = null;
	public String providerNames[] = null;
	public String days[] = null;
	public String edos[] = null;
	public String months[] = null;
	public String monthsCredit[] = null;	
	public String years[] = null;
	public String yearsCredit[] = null;
	public String sex[] = null;
	public String sex2[] = null;
	public String CreditoGuardado = "";
	public String CreditoModificado = "";
	public String numTarjeta="";
	
	public ObjectChoiceField choicesCreditNames  = null;
	public ObjectChoiceField choicesBankNames  = null;
	public ObjectChoiceField choicesProviderNames  = null;
	public ObjectChoiceField choicesMonths  = null;
	public ObjectChoiceField choicesMonthsCredit  = null;
	public ObjectChoiceField choicesYearsCredit  = null;
	public ObjectChoiceField choicesSex  = null;
	public ObjectChoiceField choicesEDO  = null;
	
	public boolean isLogin = false;
	
	public SimpleDateFormat dateFormat = null;
	public DateField dateField = null;
	
	public Calendar calendar = null;
	public Calendar calendarbirthday = null;
	
	public NewAccountScreen(){
		
		setFont(MainClass.appFont);		
		
		initVariables();
		

		addLabels();		

	}
	
	public NewAccountScreen(boolean isLogin){
		
		setFont(MainClass.appFont);		
		
		this.isLogin = isLogin;
		
		initVariables();
		
		addLabels();
		

	}
	
	public void initVariables(){
	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	    
	    TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	    transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
	  
	    bgManager = new BgManager(MainClass.BG_ORANGE);
	    
		widthManager = Display.getWidth() - 20;		
		
		hfmBtns = new HorizontalFieldManagerS(HorizontalFieldManager.FIELD_HCENTER| HorizontalFieldManager.USE_ALL_WIDTH,Bitmap.getBitmapResource(MainClass.BTN_PSWD_OVER).getWidth());
		hfmChoices = new HorizontalFieldManager(Field.FIELD_HCENTER);
		hfmUser=new HorizontalFieldManager(Field.FIELD_LEFT);
		hfmPass=new HorizontalFieldManager(Field.FIELD_LEFT);
		hfmPhone=new HorizontalFieldManager(Field.FIELD_LEFT);
		hfmPhone1=new HorizontalFieldManager(Field.FIELD_LEFT);
		hfmTags=new HorizontalFieldManager(Field.FIELD_LEFT);
		hfmTags2=new HorizontalFieldManager(Field.FIELD_LEFT);
		hfmTagsf=new HorizontalFieldManager(Field.FIELD_HCENTER);
		user = new LabelField("Nombre de Usuario: ", Field.NON_FOCUSABLE);
		lblTags = new LabelField("TAG�s:    ", Field.NON_FOCUSABLE);
		telefonooficina = new LabelField("Tel�fono Oficina: ", Field.NON_FOCUSABLE);
		telefonocasa = new LabelField("Tel�fono Casa:    ", Field.NON_FOCUSABLE);
		password = new LabelField("Contrase�a: ", Field.NON_FOCUSABLE);
		passwordConfirm = new LabelField("Confirmar Contrase�a: ", Field.NON_FOCUSABLE);
		mail = new LabelField("Correo electr�nico: ", Field.NON_FOCUSABLE);
		mailConfirm = new LabelField("Confirmar correo electr�nico: ", Field.NON_FOCUSABLE);
		//direccion
		ciudad = new LabelField("Ciudad:    ", Field.NON_FOCUSABLE);
		calle = new LabelField("Calle:    ", Field.NON_FOCUSABLE);
		numext = new LabelField("N�mero Exterior: ", Field.NON_FOCUSABLE);
		numint = new LabelField("N�mero Interior: ", Field.NON_FOCUSABLE);
		colonia = new LabelField("Colonia: ", Field.NON_FOCUSABLE);
		cp = new LabelField("C�digo postal: ", Field.NON_FOCUSABLE);
		
		cpVisa = new LabelField("C�digo postal Visa/Mastercard", Field.NON_FOCUSABLE);
		domVisa = new LabelField("Domicilio Estado de cuenta Visa/Mastercard", Field.NON_FOCUSABLE);
		
		cpAmex = new LabelField("C�digo postal American Express: ", Field.NON_FOCUSABLE);

		
		
		
		title = new LabelField("	REGISTRATE", Field.FIELD_HCENTER){
			protected void layout(int width, int height) {
				// TODO Auto-generated method stub
				super.layout(120, height);
			}
		};
		
		refresh = new LabelField("	ACTUALIZA TUS DATOS", Field.FIELD_HCENTER){
			protected void layout(int width, int height) {
				// TODO Auto-generated method stub
				super.layout(120, height);
			}
		};
		
		name = new LabelField("Nombre(s): ", Field.NON_FOCUSABLE);	
		lastNames = new LabelField("Apellido Paterno: ", Field.NON_FOCUSABLE);
		lastNamesM = new LabelField("Apellido Materno: ", Field.NON_FOCUSABLE);
		birthDate = new LabelField("Fecha de Nacimiento (dd/mm/yyyy): ", Field.NON_FOCUSABLE);	
		address = new LabelField("Domicilio de Estado de Cuenta Tarjeta: ", Field.NON_FOCUSABLE);
		numCel = new LabelField("N�mero Celular: ", Field.NON_FOCUSABLE);
		numCel1 = new LabelField("Confirmar n�mero de Celular: ", Field.NON_FOCUSABLE);
		credit = new LabelField("N�mero de Tarjeta: ", Field.NON_FOCUSABLE);
		creditDate = new LabelField("Fecha de Vencimiento (mm/yy): ", Field.NON_FOCUSABLE);
//		typeCredit = new LabelField("Tipo de Tarjeta: ", Field.NON_FOCUSABLE);
		birthDate1 = new LabelField("Fecha de Nacimiento: ", Field.NON_FOCUSABLE);
		
		basicUser = new BorderBasicEditField("","", 16, widthManager, Field.FOCUSABLE);		
		basicName = new BorderBasicEditField("","", 50, widthManager, Field.FOCUSABLE);
	//	basicName = new BorderBasicEditField("","", 30, widthManager, Field.FOCUSABLE);
		basicLastNames = new BorderBasicEditField("","", 50, widthManager, Field.FOCUSABLE);
		basicLastNamesM = new BorderBasicEditField("","", 50, widthManager, Field.FOCUSABLE);
		basicBirthDate = new BorderBasicEditField("","", 10, widthManager, Field.FOCUSABLE);
		basicAddress = new BorderBasicEditField("","", 100, widthManager, Field.FOCUSABLE);
		
		//direccion
		basicCiudad = new BorderBasicEditField("","", 50, widthManager, Field.FOCUSABLE);
		basicCalle = new BorderBasicEditField("","", 50, widthManager, Field.FOCUSABLE);
		basicColonia = new BorderBasicEditField("","", 50, widthManager, Field.FOCUSABLE);
		basicNumEx = new BorderBasicEditField("","", 5, widthManager, Field.FOCUSABLE|EditField.FILTER_NUMERIC);
		basicNumInt = new BorderBasicEditField("","", 20, widthManager, Field.FOCUSABLE);
		basicCP = new BorderBasicEditField("","", 6, widthManager, Field.FOCUSABLE|EditField.FILTER_NUMERIC);
		
		basicCpVisa = new BorderBasicEditField("", "", 6, widthManager, Field.FOCUSABLE|EditField.FILTER_NUMERIC);
		basicDomVisa = new BorderBasicEditField("", "", 6, widthManager, Field.FOCUSABLE);
		
		basicCpAmex = new BorderBasicEditField("", "", 6, widthManager, Field.FOCUSABLE|EditField.FILTER_NUMERIC);
		
		
		basictelcasa = new BorderBasicEditField("","", 10, widthManager, Field.FOCUSABLE|EditField.FILTER_NUMERIC);
		basicteloficina = new BorderBasicEditField("","", 10, widthManager, Field.FOCUSABLE|EditField.FILTER_NUMERIC);
		if(isLogin){
		basicNumCel = new BorderBasicEditField("","", 10, widthManager, Field.FOCUSABLE|EditField.FILTER_NUMERIC|Field.READONLY);
		basicNumCel1 = new BorderBasicEditField("","", 10, widthManager, Field.FOCUSABLE|EditField.FILTER_NUMERIC);
		}else{
			basicNumCel1 = new BorderBasicEditField("","", 10, widthManager, Field.FOCUSABLE|EditField.FILTER_NUMERIC);
			basicNumCel = new BorderBasicEditField("","", 10, widthManager, Field.FOCUSABLE|EditField.FILTER_NUMERIC);	
		}
		
		
		if(isLogin){
			basicCredit = new BorderBasicEditField("","", 20, widthManager, Field.FOCUSABLE);
		}else{
			basicCredit = new BorderBasicEditField("","", 20, widthManager, Field.FOCUSABLE|EditField.FILTER_NUMERIC);
		}
		
		basicBank = new BorderBasicEditField("","", 30, widthManager, Field.FOCUSABLE);
		basicCreditDate = new BorderBasicEditField("","", 5, widthManager, Field.FOCUSABLE);
		
		basicPassword = new BorderPasswordField("","", 12, widthManager, Field.FOCUSABLE);
		basicPasswordConfirm = new BorderPasswordField("","", 12, widthManager, Field.FOCUSABLE);
		
		
		
		
		if(isLogin){
			basicMail = new BorderBasicEditField("","", 50, widthManager, Field.FOCUSABLE|Field.READONLY);
		}else{
			basicMail = new BorderBasicEditField("","", 50, widthManager, Field.FOCUSABLE);
		}
		
		
		basicMailConfirm = new BorderBasicEditField("","", 50, widthManager, Field.FOCUSABLE);
		
		acceptTerms = new CheckboxField("Acepto terminos y condiciones.", false);
		
		//lo nuevo
		edoNames = new String[MainClass.EdoBeans.length];
		
		for(int i = 0; i < MainClass.EdoBeans.length; i++){
			edoNames[i] = MainClass.EdoBeans[i].getDescription();
		}
		//hasta aqui
		
		
		creditNames = new String[MainClass.creditBeans.length];
		
		for(int i = 0; i < MainClass.creditBeans.length; i++){
			creditNames[i] = MainClass.creditBeans[i].getDescription();
		}
		
		bankNames = new String[MainClass.bankBeans.length];
		
		for(int i = 0; i < MainClass.bankBeans.length; i++){
			bankNames[i] = MainClass.bankBeans[i].getDescription();
		}
		
		providerNames = new String[MainClass.providersBeans.length];
		
		for(int i = 0; i < MainClass.providersBeans.length; i++){
			providerNames[i] = MainClass.providersBeans[i].getDescription();
		}
		
		days = new String[]{"01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"};
		months = new String[]{"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		years = new String[]{"1940","1941","1942","1943","1944","1945","1946","1947","1948","1949","1950","1951","1952","1953","1954","1955","1956","1957","1958","1959","1960","1961","1962","1963","1964","1965","1966","1967","1968","1969","1970","1971","1972","1973","1974","1975","1976","1977","1978","1979","1980","1981","1982","1983","1984","1985","1986","1987","1988","1989","1990","1991","1992","1993","1994","1995","1996","1997","1998","1999","2000","2001","2002","2003","2004","2005","2006","2007","2008","2009","2010", "2011"};
		monthsCredit = new String[]{"01","02","03","04","05","06","07","08","09","10","11","12"};
		sex = new String[]{"Seleccione","FEMENINO","MASCULINO"};
		sex2 = new String[]{"","F","M"};
		edos = new String[]{"Seleccione","Aguascalientes","Baja California","Baja California Sur","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"};
		yearsCredit = new String[13];
		c = Calendar.getInstance();
		
		yearsCredit = new String[13];
		for(int i = 0; i < 13; i++){
			yearsCredit[i] = new String(Integer.toString(c.get(Calendar.YEAR)+i));
		}		
		choicesCreditNames = new ObjectChoiceField("Tipo de Tarjeta: ", creditNames){
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				super.paint(graphics);
				invalidate();
			}
			
			protected void drawFocus(Graphics graphics, boolean on) {
				// TODO Auto-generated method stub
				super.drawFocus(graphics, on);
				invalidate();
			}
		};
		choicesBankNames = new ObjectChoiceField("Banco: ", bankNames){
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				super.paint(graphics);
				invalidate();
			}
			
			protected void drawFocus(Graphics graphics, boolean on) {
				// TODO Auto-generated method stub
				super.drawFocus(graphics, on);
				invalidate();
			}
		};
		choicesProviderNames = new ObjectChoiceField("Proveedor: ", providerNames){
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				super.paint(graphics);
				invalidate();
			}
			
			protected void drawFocus(Graphics graphics, boolean on) {
				// TODO Auto-generated method stub
				super.drawFocus(graphics, on);
				invalidate();
			}
		};
		choicesMonths  = new ObjectChoiceField("Mes: ", months);
		choicesMonthsCredit  = new ObjectChoiceField("Mes Vencimiento: ", monthsCredit){
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				super.paint(graphics);
				invalidate();
			}
			
			protected void drawFocus(Graphics graphics, boolean on) {
				// TODO Auto-generated method stub
				super.drawFocus(graphics, on);
				invalidate();
			}
		};
		choicesYearsCredit  = new ObjectChoiceField("A�o Vencimiento: ", yearsCredit){
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				super.paint(graphics);
				invalidate();
			}
			
			protected void drawFocus(Graphics graphics, boolean on) {
				// TODO Auto-generated method stub
				super.drawFocus(graphics, on);
				invalidate();
			}
		};
		choicesSex = new ObjectChoiceField("Sexo: ", sex){
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				super.paint(graphics);
				invalidate();
			}
			
			protected void drawFocus(Graphics graphics, boolean on) {
				// TODO Auto-generated method stub
				super.drawFocus(graphics, on);
				invalidate();
			}
		};
		choicesEDO = new ObjectChoiceField("Estado: ", edoNames){
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				super.paint(graphics);
				invalidate();
			}
			
			protected void drawFocus(Graphics graphics, boolean on) {
				// TODO Auto-generated method stub
				super.drawFocus(graphics, on);
				invalidate();
			}
		};
		
		customVerticalFieldManager = new CustomVerticalFieldManager(Display.getWidth()-20,Display.getHeight()-30, HorizontalFieldManager.FIELD_LEFT | VERTICAL_SCROLL);
		creditVerticalFieldManager = new CustomVerticalFieldManager((Display.getWidth()-20)/2, HorizontalFieldManager.FIELD_LEFT);
		creditDateVerticalFieldManager = new CustomVerticalFieldManager((Display.getWidth()-20)/2, HorizontalFieldManager.FIELD_LEFT);
		
		btnContinue =  new GenericImageButtonField(MainClass.BTN_CONTINUE_OVER, MainClass.BTN_CONTINUE,"continuar"){
			
			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub
				
				register();
				return true;
			}
			
		};
		
		btnSave =  new GenericImageButtonField(MainClass.BTN_CONTINUE_OVER, MainClass.BTN_CONTINUE,"ok"){
			
			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub
				register();
//				close();
				return true;
			}
			
		};
		
		btnPswd =  new GenericImageButtonField(MainClass.BTN_PSWD_OVER,MainClass.BTN_PSWD,"ok"){
			
			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub
				
				UiApplication.getUiApplication().pushScreen(new PswdPopupScreen("", Field.NON_FOCUSABLE));
				return true;
			}
			
		};
		
	    btnTags =  new GenericImageButtonField(MainClass.BTN_TAGS_OVER,MainClass.BTN_TAGS,"Tags"){
			
			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub
				new TagsThread(isLogin).start();
				
				return true;
			}
			
		};
		  btnTags2 =  new GenericImageButtonField(MainClass.BTN_TAGS2_OVER,MainClass.BTN_TAGS2,"Tags"){
				
				protected boolean navigationClick(int status, int time) {
					// TODO Auto-generated method stub
					new Tags2Thread(isLogin).start();
					
					return true;
				}
				
			};
		
		btnTerms =new LabelField("T�rminos y Condiciones",Field.FOCUSABLE){
			
			public void paint(Graphics g){
				
				
				g.setColor(Color.BLUE);
				super.paint(g);
			}
			protected boolean navigationClick(int status, int time) {
				
				// TODO Auto-generated method stub
				MainClass.splashScreen.start();
				new TermsThread().run();
				return true;
			}
			
			
		};
		btnTerms.setFont(Font.getDefault().derive(Font.UNDERLINED));
		
		
		
			calendar = Calendar.getInstance();
			
			calendarbirthday = Calendar.getInstance();
			
			dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			dateField = new DateField("", Long.MIN_VALUE, dateFormat, Field.FIELD_LEFT){
				protected void paint(Graphics graphics) {
					// TODO Auto-generated method stub
					super.paint(graphics);
					invalidate();
				}
				
				protected void drawFocus(Graphics graphics, boolean on) {
					// TODO Auto-generated method stub
					super.drawFocus(graphics, on);
					invalidate();
				}
			};
			dateField.setDate(calendar.getTime());
			
			
			helpUser =new LabelField("?",Field.FOCUSABLE){
				
				public void paint(Graphics g){
					
					
					g.setColor(MainClass.BG_GRAY);
					super.paint(g);
				}
				protected boolean navigationClick(int status, int time) {
					Dialog.inform("El nombre de usuario es de m�ximo 16 caracteres");
				
					return true;
				}
				protected void onFocus(int direction) {  
			        super.onFocus(direction);
			        invalidate(); 
			     //   
			    }   
			 
				 protected void onUnfocus() {  
			           super.onUnfocus();
			           invalidate();  
			    } 
				
				
			};
				helpPass =new LabelField("?",Field.FOCUSABLE){
				
				public void paint(Graphics g){
					
					
					g.setColor(MainClass.BG_GRAY);
					super.paint(g);
				}
				protected boolean navigationClick(int status, int time) {
					Dialog.inform("La contrase�a es de m�nimo 8 caracteres y m�ximo 12");
					return true;
				}
				protected void onFocus(int direction) {  
			        super.onFocus(direction);
			        invalidate(); 
			     //   
			    }   
			 
				 protected void onUnfocus() {  
			           super.onUnfocus();
			           invalidate();  
			    } 
				
				
			};
	helpPhone =new LabelField("?",Field.FOCUSABLE){
				
				public void paint(Graphics g){
					
					
					g.setColor(MainClass.BG_GRAY);
					super.paint(g);
				}
				protected boolean navigationClick(int status, int time) {
					Dialog.inform("El tel�fono celular debe ser de 10 d�gitos");
					return true;
				}
				protected void onFocus(int direction) {  
			        super.onFocus(direction);
			        invalidate(); 
			     //   
			    }   
			 
				 protected void onUnfocus() {  
			           super.onUnfocus();
			           invalidate();  
			    } 
				
				
			};
	helpPhone1 =new LabelField("?",Field.FOCUSABLE){
				
				public void paint(Graphics g){
					
					
					g.setColor(MainClass.BG_GRAY);
					super.paint(g);
				}
				protected boolean navigationClick(int status, int time) {
					Dialog.inform("El tel�fono celular debe ser de 10 d�gitos");
					return true;
				}
				protected void onFocus(int direction) {  
			        super.onFocus(direction);
			        invalidate(); 
			     //   
			    }   
			 
				 protected void onUnfocus() {  
			           super.onUnfocus();
			           invalidate();  
			    } 
				
				
			};
	}
	
	public void addLabels(){
		
		bgManager.add(new NullField());
	
		if(!isLogin){
	
			setBanner(new BannerBitmap(MainClass.BG_ORANGE, MainClass.HD_REGISTRATE));		
			hfmUser.add(user);	
			hfmUser.add(helpUser);	
			customVerticalFieldManager.add(hfmUser);	
			customVerticalFieldManager.add(basicUser);
			
			hfmPhone.add(numCel);
			hfmPhone.add(helpPhone);
			customVerticalFieldManager.add(hfmPhone);
			customVerticalFieldManager.add(basicNumCel);
			if(isLogin){
			customVerticalFieldManager.add(new LabelField(""));	
			customVerticalFieldManager.add(choicesProviderNames);				
			customVerticalFieldManager.add(new LabelField(""));
			}
			
			if(!isLogin){
				hfmPhone1.add(numCel1);
				hfmPhone1.add(helpPhone1);
		    customVerticalFieldManager.add(hfmPhone1);
			customVerticalFieldManager.add(basicNumCel1);
			customVerticalFieldManager.add(new LabelField(""));	
			customVerticalFieldManager.add(choicesProviderNames);				
			customVerticalFieldManager.add(new LabelField(""));
			customVerticalFieldManager.add(mail);		
			customVerticalFieldManager.add(basicMail);
			customVerticalFieldManager.add(mailConfirm);		
			customVerticalFieldManager.add(basicMailConfirm);
			}
			
			
			
			
			/*hfmPass.add(password);
			hfmPass.add(helpPass);
			customVerticalFieldManager.add(hfmPass);
			customVerticalFieldManager.add(basicPassword);	
			customVerticalFieldManager.add(passwordConfirm);		
			customVerticalFieldManager.add(basicPasswordConfirm);
			*/		
			customVerticalFieldManager.add(name);		
			customVerticalFieldManager.add(basicName);	
			customVerticalFieldManager.add(lastNames);		
			customVerticalFieldManager.add(basicLastNames);
			customVerticalFieldManager.add(lastNamesM);		
			customVerticalFieldManager.add(basicLastNamesM);
		
			
		}else{

			setBanner(new BannerBitmap(MainClass.BG_ORANGE, MainClass.HD_ACTUALIZATE));		
			customVerticalFieldManager.add(name);		
			customVerticalFieldManager.add(basicName);		
			customVerticalFieldManager.add(lastNames);		
			customVerticalFieldManager.add(basicLastNames);	
			customVerticalFieldManager.add(lastNamesM);		
			customVerticalFieldManager.add(basicLastNamesM);
			hfmPhone.add(numCel);
			hfmPhone.add(helpPhone);
			customVerticalFieldManager.add(hfmPhone);
			customVerticalFieldManager.add(basicNumCel);
			customVerticalFieldManager.add(new LabelField(""));	
			customVerticalFieldManager.add(choicesProviderNames);				
			customVerticalFieldManager.add(new LabelField(""));
			customVerticalFieldManager.add(mail);		
			customVerticalFieldManager.add(basicMail);		
		/*	customVerticalFieldManager.add(mailConfirm);		
			customVerticalFieldManager.add(basicMailConfirm);*/

		}
		customVerticalFieldManager.add(new LabelField("",Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(choicesSex);
		customVerticalFieldManager.add(new LabelField("",Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(birthDate1);
		customVerticalFieldManager.add(dateField);
		customVerticalFieldManager.add(new LabelField("",Field.NON_FOCUSABLE));	
		customVerticalFieldManager.add(telefonocasa);
		customVerticalFieldManager.add(basictelcasa);
	
		customVerticalFieldManager.add(telefonooficina);
		customVerticalFieldManager.add(basicteloficina);
		customVerticalFieldManager.add(new LabelField("",Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(new LabelField("Direcci�n",Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(new LabelField("",Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(choicesEDO);
		customVerticalFieldManager.add(ciudad);
		customVerticalFieldManager.add(basicCiudad);
		customVerticalFieldManager.add(calle);
		customVerticalFieldManager.add(basicCalle);
		customVerticalFieldManager.add(numext);
		customVerticalFieldManager.add(basicNumEx);
		customVerticalFieldManager.add(numint);
		customVerticalFieldManager.add(basicNumInt);
		customVerticalFieldManager.add(colonia);
		customVerticalFieldManager.add(basicColonia);
		customVerticalFieldManager.add(cp);
		customVerticalFieldManager.add(basicCP);
		customVerticalFieldManager.add(new LabelField("",Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(credit);		
		customVerticalFieldManager.add(basicCredit);
		
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(choicesCreditNames);		
		customVerticalFieldManager.add(new LabelField(""));
		customVerticalFieldManager.add(address);		
		customVerticalFieldManager.add(basicAddress);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(choicesMonthsCredit);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(choicesYearsCredit);	
		hfmChoices.add(creditVerticalFieldManager);
		hfmChoices.add(creditDateVerticalFieldManager);	
		customVerticalFieldManager.add(hfmChoices);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		

		
	
		
		if(isLogin){customVerticalFieldManager.add(new LabelField("Agregar y consultar TAG:"));
		customVerticalFieldManager.add(new LabelField(""));
		}else{
			customVerticalFieldManager.add(new LabelField("Agregar un TAG:"));
			customVerticalFieldManager.add(new LabelField(""));
		}
		hfmTags.add(btnTags);
		hfmTagsf.add(hfmTags);
		if(isLogin){
			hfmTagsf.add(new LabelField("  "));
			hfmTagsf.add(btnTags2);
		}
		customVerticalFieldManager.add(hfmTagsf);
		customVerticalFieldManager.add(new LabelField(""));
		
		

		if(!isLogin){
		
			hfmBtns.add(btnContinue);
			
		}else{
		
			hfmBtns.add(btnSave);
			hfmBtns.add(btnPswd);
			
		}	
		
		customVerticalFieldManager.add(btnTerms);						
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(acceptTerms);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		
	    customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(hfmBtns);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		
		bgManager.add(customVerticalFieldManager);
		
		add(bgManager);
		
		if(isLogin){
		
			setTextFields();
			
		
		}else{
		
			calendar.set(Calendar.YEAR, (c.get(Calendar.YEAR)-18));
			dateField.setDate(calendar.getTime());
			
		}
		

	}
		
	private void register(){
		
		if(!basicCredit.getText().equals("")){
			
			if(CreditoModificado.equals(basicCredit.getText())){
				numTarjeta=CreditoGuardado;
				
			}else{
				
				numTarjeta=basicCredit.getText();
				
			}	
			
		}
		
		
		int _yearCredit = choicesYearsCredit.getSelectedIndex();
		int _monthCredit = choicesMonthsCredit.getSelectedIndex()+1;
		Date da = new Date(dateField.getDate());
		
		Calendar ca = Calendar.getInstance();
		Calendar cal = Calendar.getInstance();
		ca.setTime(da);		
		
		int valAge = cal.get(Calendar.YEAR) - ca.get(Calendar.YEAR);

		if(!isLogin){
			
			if(basicUser.getText().trim().equals("")){
				
	            Dialog.alert("Ingrese su Nombre de Usuario.");
	            
	        }
			
		}		
		 if(basicName.getText().trim().equals("")){
        	
            Dialog.alert("Ingrese su Nombre.");
            
        }else if(basicMail.getText().length() < 5){
        	
            Dialog.alert("Su mail no es v�lido.");
            
        }else if(basicMailConfirm.getText().length() < 5&&!isLogin){
        	
            Dialog.alert("Su mail de confirmaci�n no es v�lido.");
            
        }
        else if(!isEmail(basicMail.getText())){
        	
            Dialog.alert("Su mail no es v�lido.");
            
        }else if(!isEmail(basicMailConfirm.getText())&&!isLogin){
        	
            Dialog.alert("Su mail de confirmaci�n no es v�lido.");
            
        }
        else if(!basicMail.getText().trim().equals(basicMailConfirm.getText().trim())&&!isLogin){
        	
        	Dialog.alert("El Correo no coincide.");
        	
        }
		 
		 else if(basicLastNames.getText().trim().equals("")){
        	
            Dialog.alert("Ingrese sus apellidos.");
            
        }else if( valAge < 18 ){
        	
        	Dialog.alert( "Usted debe de ser mayor de edad para acceder a esta aplicaci�n." );
        	
        }else if(valAge > 100){
        	
        	Dialog.alert( "Fecha de Nacimiento invalida. El a�o no puede ser anterior a " + (cal.get(Calendar.YEAR)-100) + ".");
        	
        }else if(choicesSex.getSelectedIndex()==0){
        	
            Dialog.alert("Elija su sexo.");
            choicesSex.setFocus();
            
        }else if(basicAddress.getText().trim().equals("")&&choicesCreditNames.getSelectedIndex()==2){
        	
            Dialog.alert("Ingrese su Direcci�n de American Express.");
            
        }else if(basicNumCel.getText().trim().equals("")){
        	
            Dialog.alert("Ingrese su N�mero Celular.");
            
        }else if(!Utils.isNumeric(basicNumCel.getText().trim())){
        	
        	Dialog.alert("No se aceptan letras ni caracteres especiales en el n�mero telef�nico.");
        }else if( basicNumCel.getText().trim().length() < 10 ){
        	
        	Dialog.alert( "El n�mero telef�nico debe contener al menos 10 d�gitos." );
        	
        }else if(basicNumCel1.getText().trim().equals("")&&!isLogin){
        	
            Dialog.alert("Ingrese su N�mero Celular de confirmaci�n.");
            
        }else if(!Utils.isNumeric(basicNumCel1.getText().trim())&&!isLogin){
        	
        	Dialog.alert("No se aceptan letras ni caracteres especiales en el n�mero telef�nico.");
        }else if( basicNumCel1.getText().trim().length() < 10 &&!isLogin){
        	
        	Dialog.alert( "El n�mero telef�nico debe contener al menos 10 d�gitos." );
        	
        }else if(!basicNumCel.getText().trim().equals(basicNumCel1.getText().trim())&&!isLogin){
        	        	
        	Dialog.alert( "El n�mero telef�nico no es el mismo." );
        	
        }else if(basicCiudad.getText().trim().equals("")){
        	
            Dialog.alert("Ingrese su ciudad.");
            basicCiudad.setFocus();
        }else if(basicCalle.getText().trim().equals("")){
        	
            Dialog.alert("Ingrese su calle.");
            basicCalle.setFocus();
        }
        else if(basicColonia.getText().trim().equals("")){
        	
            Dialog.alert("Ingrese su colonia.");
            basicColonia.setFocus();
        }
        else if(basicNumEx.getText().trim().equals("")){
        	
            Dialog.alert("Ingrese su N�mero exterior.");
            basicNumEx.setFocus();
        }
        else if(basicCP.getText().trim().equals("")){
        	
            Dialog.alert("Ingrese su C�digo Postal.");
            basicCP.setFocus();
            
        }else if(!Utils.isNumeric(basicCP.getText().trim())){
        	
        	Dialog.alert("No se aceptan letras ni caracteres especiales en el c�digo postal.");
        	basicCP.setFocus();
        }		 
        else if(numTarjeta.trim().equals("")){
        	
            Dialog.alert("Ingrese el n�mero de su Tarjeta de Cr�dito.");
            
        }else if(numTarjeta.length()<13 ){
        	
            Dialog.alert("El n�mero de su tarjeta debe de ser al menos de 13 d�gitos.");
            
        }else if(!Utils.isNumeric(numTarjeta.trim())){
        	
        	Dialog.alert("No se aceptan letras ni caracteres especiales en el n�mero de la tarjeta.");
        	
        }else if(!acceptTerms.getChecked()){
        	
        	Dialog.alert("Debe de aceptar los terminos y condiciones.");
        	
        }else if(!Utils.validDate(""+_monthCredit, ""+_yearCredit))
        {
        	Dialog.alert( "La vigencia es incorrecta." );
        }else{
        	
        	try{

            	MainClass.type = choicesCreditNames.getSelectedIndex();
            	MainClass.bank = choicesBankNames.getSelectedIndex();
            	MainClass.monthCredit = choicesMonthsCredit.getSelectedIndex();
            	MainClass.yearCredit = choicesYearsCredit.getSelectedIndex();
            	MainClass.sex = choicesSex.getSelectedIndex();
            	MainClass.edo = choicesEDO.getSelectedIndex();
            	
            	if(!isLogin){
            		System.out.println("1");
        			MainClass.p=MainClass.passTEMP;
        			System.out.println("2");
            		MainClass.userJSON = Crypto.aesEncrypt(Utils.parsePass(MainClass.passTEMP),"{\"login\":\""+basicUser.getText().trim()+"\", \"password\":"+"\""+MainClass.passTEMP+"\"}");
            		System.out.println("3");
            		String n=Crypto.aesEncrypt(Utils.parsePass(MainClass.passTEMP),generateUserJSON());
            		System.out.println("4");            		
            		String n2=Utils.mergeStr(n,MainClass.passTEMP);
            		System.out.println("5");
            		new RegisterThread(n2).start();
            		
            		/*
            		 * MainClass.p=basicPassword.getText().trim();
            		MainClass.userJSON = Crypto.aesEncrypt(Utils.parsePass(basicPassword.getText().trim()),"{\"login\":\""+basicUser.getText().trim()+"\", \"password\":"+"\""+basicPassword.getText().trim()+"\"}");
   					String n=Crypto.aesEncrypt(Utils.parsePass(basicPassword.getText().trim()),generateUserJSON());
   					String n2=Utils.mergeStr(n, basicPassword.getText().trim());
            		new RegisterThread(n2).start();
            		 * */

            	}else{

            		MainClass.splashScreen.start();
            	
            		MainClass.userJSON = Crypto.aesEncrypt(Utils.parsePass(MainClass.p),"{\"login\":\""+basicUser.getText().trim()+"\", \"password\":"+"\""+MainClass.p+"\"}");
   				
            		String n=Crypto.aesEncrypt(Utils.parsePass(MainClass.p),generateUserJSON());
         
   					String n2=Utils.mergeStr(n, MainClass.p);   					
            		new UpdateUserThread(n2).start();            		
                	
                	
            	}
            	
        	}catch(Exception e){
        		System.out.println("error "+e.toString());
        		Dialog.alert("Ocurri� un error. Int�ntelo de nuevo m�s tarde");
        		
        	}     	
        	
        }
		
	}
	
	private void setTextFields() {
		System.out.println("1");
		basicPassword.setText(MainClass.userBean.getPswd());
		System.out.println("1.1");
		basicPasswordConfirm.setText(MainClass.userBean.getPswd());
		basicName.setText(MainClass.userBean.getName());
		basicLastNames.setText(MainClass.userBean.getLastName());
		System.out.println("1.2");
		//nuevo
		basicLastNamesM.setText(MainClass.userBean.getMaterno());
		System.out.println("1.3");
		if(MainClass.userBean.getSexo().toLowerCase().equals("masculino")||MainClass.userBean.getSexo().toLowerCase().equals("hombre")||MainClass.userBean.getSexo().toLowerCase().equals("m"))
		{choicesSex.setSelectedIndex(2);}
		if(MainClass.userBean.getSexo().toLowerCase().equals("femenimo")||MainClass.userBean.getSexo().toLowerCase().equals("mujer")||MainClass.userBean.getSexo().toLowerCase().equals("f"))
		{choicesSex.setSelectedIndex(1);}
		System.out.println("1.4");
		basictelcasa.setText(MainClass.userBean.getTel_casa());
		basicteloficina.setText(MainClass.userBean.getTel_oficina());
		System.out.println("1.5");
		System.out.println(Integer.parseInt(MainClass.userBean.getId_estado())-1);
		if(Integer.parseInt(MainClass.userBean.getId_estado())!=0){
			choicesEDO.setSelectedIndex(Integer.parseInt(MainClass.userBean.getId_estado())-1);
		}
	
		System.out.println("1.6");
		basicCiudad.setText(MainClass.userBean.getCiudad());
		basicCalle.setText(MainClass.userBean.getCalle());
		basicNumEx.setText(MainClass.userBean.getNum_ext());
		basicNumInt.setText(MainClass.userBean.getNum_interior());
		basicColonia.setText(MainClass.userBean.getColonia());
		basicCP.setText(MainClass.userBean.getCp());
		System.out.println("1.7");
		basicAddress.setText(MainClass.userBean.getDom_amex());
	
		System.out.println("2");
	
		
		basicMail.setText(MainClass.userBean.getMail());
		//basicMailConfirm.setText(MainClass.userBean.getMail());
	
		basicNumCel.setText(MainClass.userBean.getPhone());
		
		String credit = MainClass.userBean.getCredit();
		
		
		CreditoGuardado=MainClass.userBean.getCredit();
		credit = credit.substring(MainClass.userBean.getCredit().length()-4, MainClass.userBean.getCredit().length());
		//----------------------------------------------------------
		System.out.println("3");
		
		String credit2="";
	
		for(int x =0; x<MainClass.userBean.getCredit().length()-4;x++){
		 credit2+="X";
		
		}
		basicCredit.setText(credit2+credit);
		CreditoModificado=basicCredit.getText();
		int selected = 0;
		String numYear = MainClass.userBean.getBirthday().substring(0, MainClass.userBean.getBirthday().indexOf("-"));
		calendar.set(Calendar.YEAR, Integer.parseInt(numYear));
		
		for(int i = 0; i < years.length; i++ ){
			
			if(years[i].equals(numYear)){
			
				selected = i;
			}
			
		}
		System.out.println("4");
		String newYear = MainClass.userBean.getBirthday().substring(MainClass.userBean.getBirthday().indexOf("-")+1, MainClass.userBean.getBirthday().length()); 
		
		String month = "";
		
		month = newYear.substring(0, newYear.indexOf("-"));
		calendar.set(Calendar.MONTH, Integer.parseInt(month)-1);
		
		newYear = newYear.substring(newYear.indexOf("-")+1, newYear.length());
		calendar.set(Calendar.DATE, Integer.parseInt(newYear));
		dateField.setDate(calendar.getTime());
		choicesMonths.setSelectedIndex((Integer.parseInt(month)-1));
		
		for(int i = 0; i < MainClass.bankBeans.length; i++){
			
			if(MainClass.userBean.getBank().equals(MainClass.bankBeans[i].getClave())){
				
				selected = i;
				
			}
			
		}
		System.out.println("5");
		choicesBankNames.setSelectedIndex(selected);
	
		for(int i = 0; i < MainClass.creditBeans.length; i++){
			
			if(MainClass.userBean.getCreditType().equals(MainClass.creditBeans[i].getClave())){
				
				selected = i;
				
			}
			
		}
		
		choicesCreditNames.setSelectedIndex(selected);
		
		newYear = "";
		month = "";

		for(int i = 0; i < MainClass.providersBeans.length; i++){
			
			if(MainClass.userBean.getProvider().equals(MainClass.providersBeans[i].getClave())){
				
				selected = i;

			}
			
		}
		
		choicesProviderNames.setSelectedIndex(selected);

		selected = Integer.parseInt(MainClass.userBean.getLife().substring(0,MainClass.userBean.getLife().indexOf("/")))-1;

		choicesMonthsCredit.setSelectedIndex(selected);
		
		newYear = MainClass.userBean.getLife().substring(MainClass.userBean.getLife().indexOf("/")+1, MainClass.userBean.getLife().length());
		
		for(int i = 0; i < yearsCredit.length; i++){
			
			if(yearsCredit[i].equals("20"+newYear)){
				
				selected = i;
			}

		}
		System.out.println("6");
		choicesYearsCredit.setSelectedIndex(selected);
		
	}
	
	public void close()
    { 
		
		UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());

    }
	    
	protected boolean onSavePrompt() 
	{
		return true;
	}
	
	public String generateUserJSON() throws UnsupportedEncodingException{
		
		StringBuffer userJSON = new StringBuffer("{\"login\":\"");
				if(!isLogin){
		userJSON.append(basicUser.getText().trim());
		}else{
			userJSON.append(MainClass.userBean.getLogin());
		}
		
		if(!isLogin){
			userJSON.append("\",\"password\":\""+MainClass.passTEMP);
		}else{
			userJSON.append("\",\"password\":\""+MainClass.p);
		}
		
		if(!isLogin){
			MainClass.userJSON = Crypto.aesEncrypt(Utils.parsePass(MainClass.passTEMP), "{\"login\":\""+basicUser.getText().trim()+"\",\"password\":"+"\""+MainClass.passTEMP+"\"}");;
			MainClass.userBean = new UserBean(basicUser.getText().trim(),MainClass.passTEMP,"","","","","","","","","","","");
		}
		
		int numMonth = choicesMonths.getSelectedIndex()+1;
		
		String stringMonth = "";
		
		if(numMonth < 10){
			
			stringMonth = "0" + numMonth;
			
		}else{
			
			stringMonth = String.valueOf(numMonth);
			
		}
		Date d = new Date(dateField.getDate());
		Date da = new Date(dateField.getDate());
		
		calendar.setTime(da);
		
		int month = calendar.get(calendar.MONTH)+1;
		int day = calendar.get(calendar.DATE);
		String strMonth = "";
		String strDate = "";
		
		if(month < 10){
			strMonth = "0" + month;
		}else{
			strMonth = "" + month;
		}
		
		if(day < 10){
			strDate = "0" + day;
		}else{
			strDate = "" + day;
		}
		
		userJSON.append("\",\"nacimiento\":\""+calendar.get(calendar.YEAR)+"-"+strMonth+"-"+strDate);	
		userJSON.append("\",\"telefono\":\""+basicNumCel);
		userJSON.append("\",\"nombre\":\""+basicName.getText().trim());	
		userJSON.append("\",\"apellido\":\""+basicLastNames.getText().trim());		
		userJSON.append("\",\"direccion\":\""+basicCalle.getText());		
		userJSON.append("\",\"tarjeta\":\""+numTarjeta.trim());	
		if(isLogin){
		userJSON.append("\",\"mail\":\""+basicMail.getText().trim());
		}else{
			userJSON.append("\",\"mail\":\""+basicMail.getText().trim());
		}
		numMonth = choicesMonthsCredit.getSelectedIndex()+1;
		
		stringMonth = "";
		
		if(numMonth < 10){
			
			stringMonth = "0" + numMonth;
			
		}else{
			
			stringMonth = String.valueOf(numMonth);
			
		}
		
		String vigency = yearsCredit[choicesYearsCredit.getSelectedIndex()];		
		vigency = vigency.substring((vigency.length()-2), vigency.length());	
		
		//nuevoscampos
		userJSON.append("\",\"materno\":\""+basicLastNamesM.getText().trim());
		
		userJSON.append("\",\"sexo\":\""+sex2[MainClass.sex]);
		userJSON.append("\",\"tel_casa\":\""+basictelcasa.getText().trim());	
		userJSON.append("\",\"tel_oficina\":\""+basicteloficina.getText().trim());
		userJSON.append("\",\"id_estado\":"+MainClass.EdoBeans[choicesEDO.getSelectedIndex()].getClave());
		userJSON.append(",\"ciudad\":\""+basicCiudad.getText().trim());
		userJSON.append("\",\"calle\":\""+basicCalle.getText().trim());
		userJSON.append("\",\"num_ext\":"+basicNumEx.getText().trim());
		userJSON.append(",\"num_interior\":\""+basicNumInt.getText().trim());
		userJSON.append("\",\"colonia\":\""+basicColonia.getText().trim());
		userJSON.append("\",\"cp\":"+basicCP.getText().trim());
		if(choicesCreditNames.getSelectedIndex()==2){
		userJSON.append(",\"dom_amex\":\""+basicAddress.getText());	}else{
		userJSON.append(",\"dom_amex\":\"");
			
		}
		/*
		 * 	basicColonia = new BorderBasicEditField("","", 50, widthManager, Field.FOCUSABLE);
		basicNumEx = new BorderBasicEditField("","", 5, widthManager, Field.FOCUSABLE|EditField.FILTER_NUMERIC);
		basicNumInt = new BorderBasicEditField("","", 20, widthManager, Field.FOCUSABLE);
		basicCP = new BorderBasicEditField("","", 6, widthManager, Field.FOCUSABLE|EditField.FILTER_NUMERIC);
		
		 * 
		 * */
		
		
		
		userJSON.append("\",\"vigencia\":\""+stringMonth+"/"+vigency);		
		userJSON.append("\",\"banco\":"+MainClass.bankBeans[choicesBankNames.getSelectedIndex()].getClave());		
		userJSON.append(",\"tipotarjeta\":"+MainClass.creditBeans[choicesCreditNames.getSelectedIndex()].getClave());		
		userJSON.append(",\"proveedor\":"+MainClass.providersBeans[choicesProviderNames.getSelectedIndex()].getClave());
		userJSON.append(",\"imei\":\""+Utils.getImei()+"\"");
	
		if(!isLogin){
			userJSON.append(","+MainClass.TAG_REGISTER);
			userJSON.append("\",\"tipo\":\""+MainClass.TIPO);	
			userJSON.append("\",\"software\":\""+MainClass.SOFTWARE);	
			userJSON.append("\",\"modelo\":\""+MainClass.MODELO);	
			userJSON.append("\",\"key\":\""+Utils.getImei()+"\"");	
		}
		
		
		userJSON.append(",\"status\":1}");
		System.out.println(userJSON.toString());
		return userJSON.toString();
		
	}
	public boolean isEmail(String correo){
		boolean escorreo=true;
		if(correo.indexOf("@")==-1){
			escorreo=false;
		}
		if(correo.indexOf(".")==-1){
			escorreo=false;
		}
		
		return escorreo;
		
	}
	
}

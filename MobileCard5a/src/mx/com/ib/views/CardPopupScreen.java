package mx.com.ib.views;

import java.util.Calendar;

import mx.com.ib.app.MainClass;
import mx.com.ib.beans.CardBean;
import mx.com.ib.controls.BorderBasicEditField;
import mx.com.ib.controls.GenericImageButtonField;
import mx.com.ib.utils.Utils;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class CardPopupScreen extends PopupScreen implements FieldChangeListener {
	
	public CardBean cardBean= null;
	
	public final int tipoTarjeta;
	public LabelField numTarjeta = null;
	public LabelField cpTarjeta = null;
	public LabelField domTarjeta = null;
	public LabelField vencimientoTarjeta = null;
	
	public BorderBasicEditField basicNumTarjeta = null;
	public BorderBasicEditField basicCpTarjeta = null;
	public BorderBasicEditField basicDomTarjeta = null;
	
	
	public HorizontalFieldManager hfmTitle = null;
	public HorizontalFieldManager hfmBtn = null;
	public HorizontalFieldManager hfmnick = null;
	
	public int widthManager = 0;
	public boolean isLogin = false;
	
	public Calendar c;
	
	public GenericImageButtonField btnChange = null;
	private NewAccountScreen actualizate=null;
	
	public String monthsCredit[] = null;	
	public String yearsCredit[] = null;
	
	public ObjectChoiceField choicesMonthsCredit  = null;
	public ObjectChoiceField choicesYearsCredit  = null;
	
	private String numCardOriginal = "";
	
	public CardPopupScreen(int tipo, long style, boolean isLogin){
		super(new VerticalFieldManager(VERTICAL_SCROLL));
		this.isLogin = isLogin;
		this.tipoTarjeta = tipo;
		initVariables();
		addFields();
		if(isLogin)
			setFields();
	}

	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		
	}
	
	public void initVariables(){
	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	    TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 2000);
	    transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	    transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
		hfmTitle = new HorizontalFieldManager(Field.FIELD_HCENTER);
		hfmnick = new HorizontalFieldManager(Field.FIELD_LEFT);
		hfmBtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		
		widthManager = Display.getWidth() - 100;
		
		numTarjeta = new LabelField("N�mero de tarjeta: ", Field.NON_FOCUSABLE);
		cpTarjeta = new LabelField("C�digo postal estado de cuenta: ", Field.NON_FOCUSABLE);
		domTarjeta = new LabelField("Domicilio estado de cuenta: ", Field.NON_FOCUSABLE);
		
		if(isLogin){
			if(tipoTarjeta == 0){
				numCardOriginal = MainClass.visaBean.getNumTarjeta();
			}
			else if(tipoTarjeta == 1){
				numCardOriginal = MainClass.amexBean.getNumTarjeta();
			}
			basicNumTarjeta = new BorderBasicEditField("","", 16, Display.getWidth()-20, Field.FOCUSABLE);
		
		}else
			basicNumTarjeta = new BorderBasicEditField("","", 16, Display.getWidth()-20, Field.FOCUSABLE|EditField.FILTER_NUMERIC);
		
		basicCpTarjeta = new BorderBasicEditField("","", 6, Display.getWidth()-20, Field.FOCUSABLE|EditField.FILTER_NUMERIC);
		basicDomTarjeta = new BorderBasicEditField("","",100, Display.getWidth()- 20, Field.FOCUSABLE|EditField.FILTER_NUMERIC);
		
		monthsCredit = new String[]{"01","02","03","04","05","06","07","08","09","10","11","12"};
		
		c = Calendar.getInstance();
		
		yearsCredit = new String[13];
		for(int i = 0; i < 13; i++){
			yearsCredit[i] = new String(Integer.toString(c.get(Calendar.YEAR)+i));
		}	
		
		choicesMonthsCredit = new ObjectChoiceField("Mes Vencimiento: ", monthsCredit){
			protected void paint(Graphics graphics){
				super.paint(graphics);
				invalidate();
			}
			
			protected void drawFocus(Graphics graphics, boolean on) {
				super.drawFocus(graphics, on);
				invalidate();
			}
		};
		
		choicesYearsCredit  = new ObjectChoiceField("A�o Vencimiento: ", yearsCredit){
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				super.paint(graphics);
				invalidate();
			}
			
			protected void drawFocus(Graphics graphics, boolean on) {
				// TODO Auto-generated method stub
				super.drawFocus(graphics, on);
				invalidate();
			}
		};
		
		
		
		btnChange = new GenericImageButtonField(MainClass.BTN_SAVE_OVER, MainClass.BTN_SAVE, "continuar"){
			protected boolean navigationClick(int status, int time){
				
				if(tipoTarjeta == 1){
					if(basicCpTarjeta.getText().trim().equals("")){
						Dialog.alert("Debe ingresar el c�digo postal de su estado de cuenta");
					}
					else if(basicDomTarjeta.getText().trim().equals("")){
						Dialog.alert("Debe ingresar el domicilio de su estado de cuenta");
					
					}
				}else if(tipoTarjeta == 0){
					if(basicNumTarjeta.getText().trim().equals("")){
						Dialog.alert("Debe ingresar el n�mero de su tarjeta de cr�dito");
					}
				}				
				if(validateFields())
					agregarTarjeta();
				
				return true;
			}
		};
	}
	
	public void addFields(){
		add(new NullField());
		if(tipoTarjeta == 0){
			add(new LabelField("Ingrese datos VISA/MASTERCARD", Field.NON_FOCUSABLE));
		}
		else if(tipoTarjeta == 1){
			add(new LabelField("Ingrese datos AMEX", Field.NON_FOCUSABLE));
		}
		add(hfmTitle);
		add(numTarjeta);
		add(basicNumTarjeta);
		add(new LabelField("",Field.NON_FOCUSABLE));
		add(cpTarjeta);
		add(basicCpTarjeta);
		add(new LabelField("",Field.NON_FOCUSABLE));
		add(domTarjeta);
		add(basicDomTarjeta);
		add(new LabelField("",Field.NON_FOCUSABLE));
		add(choicesMonthsCredit);
		add(choicesYearsCredit);
		hfmBtn.add(btnChange);
		add(new LabelField(""));
		add(hfmBtn);
		add(new LabelField(""));
		
	}
	
	public boolean keyChar(char key, int status, int time){
		boolean retval = false;
		switch(key){
		case Characters.ESCAPE:
			close();
			break;
		default:
			retval = super.keyChar(key, status, time);
		}
		return retval;
	}
	
	public void close(){
		UiApplication.getUiApplication().popScreen(this);
	}
	
	public void agregarTarjeta(){
		
		String num = basicNumTarjeta.getText().trim();
		String cp = basicCpTarjeta.getText().trim();
		String dom = basicDomTarjeta.getText().trim();
		String vence = getVigency();

		if(tipoTarjeta == 0){
			if(!isLogin){
				MainClass.visaBean = new CardBean();
			}
			MainClass.visaBean.setNumTarjeta(num);
			MainClass.visaBean.setCpTarjeta(cp);
			MainClass.visaBean.setDomTarjeta(dom);
			MainClass.visaBean.setVencimientoTarjeta(vence);				
		}
		else if(tipoTarjeta == 1){
			if(!isLogin){
				MainClass.amexBean = new CardBean();		
			}
			MainClass.amexBean.setNumTarjeta(num);
			MainClass.amexBean.setCpTarjeta(cp);
			MainClass.amexBean.setDomTarjeta(dom);
			MainClass.amexBean.setVencimientoTarjeta(vence);
		}
		UiApplication.getUiApplication().popScreen(this);
		
	}
	
	private String getVigency(){

		int numMonth = choicesMonthsCredit.getSelectedIndex() + 1;
		String monthString = "";
		
		if(numMonth < 10){
			monthString = "0" + numMonth;
		}
		else{
			monthString = String.valueOf(numMonth);
		}
		
		String vigency = yearsCredit[choicesYearsCredit.getSelectedIndex()];
		vigency = vigency.substring((vigency.length() - 2), vigency.length());
		
		return monthString + "/" + vigency;
	}
	
	private void setFields(){
		if(tipoTarjeta == 0){
			String credit = MainClass.visaBean.getNumTarjeta();
			credit = credit.substring(MainClass.visaBean.getNumTarjeta().length()-4, MainClass.visaBean.getNumTarjeta().length());
			String credit2="";
		
			for(int x =0; x<MainClass.visaBean.getNumTarjeta().length()-4;x++){
			 credit2+="*";			
			}
			
			basicNumTarjeta.setText(credit2+credit);
			setVigency(MainClass.visaBean);
			//CreditoModificado=basicCredit.getText();						
		}	
	}
	
	private void setVigency(CardBean cardBean){
		String vigenciaString = cardBean.getVencimientoTarjeta();
		String month = "";
		String year = "";
		int yearNumber = 0;
		
		System.out.println("***FECHA DE VIGENCIA: " + cardBean.getVencimientoTarjeta()+"***");
		System.out.println("***ENTRANDO A CICLO PARA PARSEAR FECHA***");
		
		for (int i = 0; i < vigenciaString.length(); i ++){
			if (i < 2)
				month += vigenciaString.charAt(i);
			else if(i > 2)
				year += vigenciaString.charAt(i);
		}
		
		System.out.println("***SALIENDO A CICLO PARA PARSEAR FECHA***");
		
		if(month.startsWith("0")){
			month = month.substring(1);
		}
		choicesMonthsCredit.setSelectedIndex(Integer.parseInt(month) - 1);
		
		for(int i = 0; i < yearsCredit.length; i++){
			if(yearsCredit[i].equals("20" + year)){
				yearNumber = i;
			}
		}
		
		choicesYearsCredit.setSelectedIndex(yearNumber);
	
	}
	
	private boolean validateFields(){
		if(basicNumTarjeta.getText().trim().equals("")){   	
	        Dialog.alert("Ingrese el n�mero de su Tarjeta de Cr�dito.");
	        return false;
	    }
	    else if(basicNumTarjeta.getText().length()<13 ){
	    	Dialog.alert("El n�mero de su tarjeta debe de ser al menos de 13 d�gitos.");
	        return false;
	    }
	    else if(!Utils.isNumeric(basicNumTarjeta.getText())){
	    	Dialog.alert("No se aceptan letras ni caracteres especiales en el n�mero de la tarjeta.");
	    	return false;
	    }

	    /*else if(!Utils.validDate(""+_monthCredit, ""+_yearCredit)){
	    	Dialog.alert( "La vigencia es incorrecta." );
	    }*/
		return true;
	}
}

package mx.com.ib.views;

import java.io.UnsupportedEncodingException;

import crypto.Crypto;

import mx.com.ib.app.MainClass;
import mx.com.ib.controls.BorderBasicEditField;
import mx.com.ib.controls.BorderPasswordField;
import mx.com.ib.controls.GenericImageButtonField;
import mx.com.ib.threads.ComisionThread;
import mx.com.ib.threads.UpdateUserMailThread;
import mx.com.ib.threads.setSmsThread;
import mx.com.ib.utils.Utils;
import net.rim.device.api.io.FilterBaseInterface;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class SmsScreen extends PopupScreen implements FieldChangeListener{
	
	public LabelField actualPswd = null;
	public LabelField pswd = null;
	public LabelField confirmPswd = null;
	public LabelField mail = null;
	public LabelField title = null;
	
	public BorderBasicEditField basicActualPassword = null;
	public BorderBasicEditField basicPassword = null;
	public BorderPasswordField basicPasswordConfirm = null;
	
	public BorderBasicEditField basicmail = null;	
	
	public HorizontalFieldManager hfmTitle = null;
	public HorizontalFieldManager hfmBtn = null;
	
	public int widthManager = 0;
	
	public GenericImageButtonField btnChange = null;
	
	public SmsScreen(String text, long style){
		
		super(new VerticalFieldManager(VERTICAL_SCROLL));
		
		initVariables();
		
		addFields();
		
	}
	
	public void initVariables(){
	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	     TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	     transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 2000);
	     transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	     transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    	 engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
		hfmTitle = new HorizontalFieldManager(Field.FIELD_HCENTER);
		hfmBtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		
		widthManager = Display.getWidth() - 100;
		
		actualPswd = new LabelField("Tel�fono Celular: ", Field.NON_FOCUSABLE);
		pswd = new LabelField("Confirmaci�n de Celular: ", Field.NON_FOCUSABLE);
		
		title = new LabelField("Escriba su n�mero celular para poder enviar nuevamente su c�digo de activaci�n", Field.NON_FOCUSABLE);
		
		basicActualPassword = new BorderBasicEditField("","", 10, this.getWidth(), Field.FOCUSABLE|EditField.FILTER_NUMERIC);
		basicPassword = new BorderBasicEditField("","", 10, this.getWidth(), Field.FOCUSABLE|EditField.FILTER_NUMERIC);
		
		btnChange =  new GenericImageButtonField(MainClass.BTN_SMS_OVER, MainClass.BTN_SMS,"continuar"){
			
			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub
				if(basicActualPassword.getText().trim().length()<10){
					Dialog.alert("El n�mero debe de ser de al menos 10 d�gitos");
				}
				else if(basicPassword.getText().trim().length()<10){
					Dialog.alert("El n�mero de confirmaci�n debe de ser de al menos 10 d�gitos");
				}
				else if(!basicPassword.getText().trim().equals(basicActualPassword.getText().trim())){
					Dialog.alert("Los tel�fonos no concuerdan");
				}
				else{
					checkPaswd(basicActualPassword.getText().trim());
				}
			
				return true;
			}
			
		};
		
	}
	
	public void addFields(){
		
		hfmTitle.add(title);
		hfmBtn.add(btnChange);
		add(hfmTitle);
		add(new LabelField(""));
		add(actualPswd);
		add(basicActualPassword);
		add(new LabelField(""));
		add(pswd);
		add(basicPassword);
		add(new LabelField(""));
		
		add(hfmBtn);
		add(new LabelField(""));
	
	}

	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		
	}
	
	public boolean keyChar(char key, int status, int time) {
	    //intercept the ESC key
	    boolean retval = false;
	    switch (key) {

	        case Characters.ESCAPE:
	            close();
	            break;
	        default:
	            retval = super.keyChar(key,status,time);
	    }
	    return retval;
	}
	
	public void close() {
		// TODO Auto-generated method stub
		UiApplication.getUiApplication().popScreen(this);
	}
	
	public void checkPaswd(String num){
					
		MainClass.splashScreen.start();		
		String json = Crypto.aesEncrypt(MainClass.key,num);
		new setSmsThread(json).start();		
		
	}
	
	
	


}
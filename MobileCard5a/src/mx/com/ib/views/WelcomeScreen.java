package mx.com.ib.views;

import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import org.json.me.JSONObject;

import mx.com.ib.app.MainClass;
import mx.com.ib.controls.BgManager;
import mx.com.ib.utils.Utils;
import net.rim.device.api.system.Application;
import net.rim.device.api.system.ApplicationDescriptor;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import addcel.core.mx.communication.Versionador;
import addcel.core.mx.communication.WSinvoker;

public class WelcomeScreen extends MainScreen {

	public HorizontalFieldManager hfmMain = null;
	public HorizontalFieldManager hfmImage = null;
	public HorizontalFieldManager hfmBtns = null;
	public LabelField title = null;
	public LabelField terms = null;
	public BgManager bgManager = null;

	Timer thBanner = null;
	MyTimerTask timerTask = null;
	private int WAIT_TIME = 10000000;

	public WelcomeScreen() {

		initVariables();

		Versionador versionador = new Versionador(this);
		versionador.start();
	}

	public void initVariables() {
		TransitionContext transition = new TransitionContext(
				TransitionContext.TRANSITION_SLIDE);
		transition.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
		transition.setIntAttribute(TransitionContext.ATTR_DIRECTION,
				TransitionContext.DIRECTION_LEFT);
		transition.setIntAttribute(TransitionContext.ATTR_STYLE,
				TransitionContext.STYLE_PUSH);
		UiEngineInstance engine = Ui.getUiEngineInstance();
		engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH,
				transition);
		TransitionContext transition2 = new TransitionContext(
				TransitionContext.TRANSITION_SLIDE);
		transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 2000);
		transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION,
				TransitionContext.DIRECTION_RIGHT);
		transition2.setIntAttribute(TransitionContext.ATTR_STYLE,
				TransitionContext.STYLE_PUSH);
		engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP,
				transition2);
		engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
		thBanner = new Timer();

		timerTask = new MyTimerTask();

		bgManager = new BgManager(MainClass.BG_SPLASH);

		hfmMain = new HorizontalFieldManager(Field.FIELD_HCENTER
				| VERTICAL_SCROLL);
		hfmImage = new HorizontalFieldManager(Field.FIELD_HCENTER);
		hfmBtns = new HorizontalFieldManager(Field.FIELD_HCENTER);

		title = new LabelField("BIENVENIDO A TU SERVICIO", Field.NON_FOCUSABLE);
		terms = new LabelField("TERMINOS Y CONDICIONES", Field.NON_FOCUSABLE);

		title.setFont(MainClass.alphaSansFamily.getFont(Font.PLAIN, 10,
				Ui.UNITS_pt));

	}
/*
	public void addFields() {

		BitmapField bField = new BitmapField(Bitmap.getBitmapResource(MainClass.BG_SPLASH));

		bgManager.add(bField);
		Utils.getDeviceINFO();
		add(bgManager);

		synchronized (Application.getEventLock()) {
			final String version = getVersion();
			System.out.println(version);

			if (version != null && !version.equals("")) {
				final String[] ver = split(version, "|");
				ApplicationDescriptor appDesc = ApplicationDescriptor
						.currentApplicationDescriptor();

				if (ver[0].equals(appDesc.getVersion())) {
					thBanner.schedule(timerTask, 1000);
				} else {
					UiApplication.getUiApplication().invokeLater(
							new Runnable() {

								public void run() {
									// TODO Auto-generated method stub
									if (ver[1].equals("1")) {
										UiApplication
												.getUiApplication()
												.popScreen(
														UiApplication
																.getUiApplication()
																.getActiveScreen());
										UiApplication.getUiApplication()
												.pushScreen(
														new ScreenVersion(
																ver[2], "1"));

									} else if (ver[1].equals("2")) {

										UiApplication
												.getUiApplication()
												.popScreen(
														UiApplication
																.getUiApplication()
																.getActiveScreen());
										UiApplication.getUiApplication()
												.pushScreen(
														new ScreenVersion(
																ver[2], "2"));

									}

								}

							});
				}
			} else {
				thBanner.schedule(timerTask, 1000);
			}

		}
	}
*/
	
	
	public void addFields(JSONObject jsonObject) {

		synchronized (Application.getEventLock()) {
			BitmapField bField = new BitmapField(Bitmap.getBitmapResource(MainClass.BG_SPLASH));
	
			bgManager.add(bField);
			Utils.getDeviceINFO();
			add(bgManager);

			String version01 = jsonObject.optString("version", ""); 
			final String tipo = jsonObject.optString("tipo", "");
			final String url = jsonObject.optString("url", "");
			
			if (version01 != null && !version01.equals("")) {

				ApplicationDescriptor appDesc = ApplicationDescriptor.currentApplicationDescriptor();

				if (version01.equals(appDesc.getVersion())) {
					thBanner.schedule(timerTask, 1000);
				} else {

					if (tipo.equals("1")) {
						UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
						UiApplication.getUiApplication().pushScreen(new ScreenVersion(url, "1"));
					} else if (tipo.equals("2")) {

						UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
						UiApplication.getUiApplication().pushScreen(new ScreenVersion(url, "2"));
					}
				}
			} else {
				thBanner.schedule(timerTask, 1000);
			}
		}
	}
	
	
	protected boolean onSavePrompt() {
		return true;
	}

	class MyTimerTask extends TimerTask {

		public MyTimerTask() {
		}

		public void run() {
			synchronized (Application.getEventLock()) {
				System.out.println("::::TIME'S UP:::");
				UiApplication.getUiApplication().popScreen(
						UiApplication.getUiApplication().getActiveScreen());
				UiApplication.getUiApplication().pushScreen(new LoginScreen());
			}
		}
	}

	protected boolean keyDown(int keycode, int time) {

		if (Keypad.key(keycode) == Characters.ESCAPE) {
			System.exit(0);
			return true;
		}
		return false;
	}

	private String getVersion() {
		// TODO Auto-generated method stub

		// lanzarError("Verson: "+appDesc.getVersion());
		String paramsGetVersion[] = { "App", "Plataforma" };
		// String valsGetVersion[] = {"MBB", appDesc.getVersion()};
		String valsGetVersion[] = { "1", "3" };
		WSinvoker wsGetVersion = new WSinvoker("GetVersion", paramsGetVersion,
				valsGetVersion);

		try { // Obtener Secciones

			Thread thGetVersion = new Thread(wsGetVersion);
			thGetVersion.start();

			int wait = 0;
			while (!wsGetVersion.isFinished) {
				if (wait == WAIT_TIME) {
					wsGetVersion.isFinished = true;
					wsGetVersion.estado = false;
				}
				wait += 1;
				// thGetVersion.sleep(1000);
			}
		} catch (Exception ex) {
			System.out.println("Error: " + ex.getMessage());
		} finally {

		}
		return wsGetVersion.resultado;
	}

	private String[] split(String original, String separador) {
		Vector nodes = new Vector();
		String separator = separador; // "|";
		// System.out.println("split start...................");
		// Parse nodes into vector
		int index = original.indexOf(separator);
		while (index >= 0) {
			nodes.addElement(original.substring(0, index));
			original = original.substring(index + separator.length());
			index = original.indexOf(separator);
		}
		// Get the last node
		nodes.addElement(original);

		// Create splitted string array
		String[] result = new String[nodes.size()];
		if (nodes.size() > 0) {
			for (int loop = 0; loop < nodes.size(); loop++) {
				result[loop] = (String) nodes.elementAt(loop);
				// System.out.println(result[loop]);
			}

		}

		return result;
	}

}

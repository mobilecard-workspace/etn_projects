package mx.com.ib.views;

import crypto.Crypto;
import mx.com.ib.app.MainClass;
import mx.com.ib.controls.BgManager;
import mx.com.ib.controls.BorderBasicEditField;
import mx.com.ib.controls.CustomVerticalFieldManager;
import mx.com.ib.controls.GenericImageButtonField;
import mx.com.ib.controls.HorizontalFieldManagerS;
import mx.com.ib.threads.PasswordThread;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;


public class RecUserScreen extends MainScreen{
	
	public BgManager bgManager = null;
	
//	public LabelField title = null;
	public LabelField correo = null;
	
	public LabelField contact=null;
	
	public BorderBasicEditField basicUser = null;
	
	public HorizontalFieldManager hfmMain = null;
	public HorizontalFieldManager hfmTitle = null;
	
	public HorizontalFieldManagerS hfmBtns = null;
	
	public VerticalFieldManager vfmFooter = null;
	
	public CustomVerticalFieldManager customVerticalFieldManager = null;
	public CustomVerticalFieldManager customVerticalFieldManager2 = null;
	
	public GenericImageButtonField btnOk = null;	
	
//	public String json = "";
	
	public RecUserScreen(){
	
		initVariables();
		
		addFields();
		
	}
	
	public void initVariables(){
		 TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
		    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
		    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
		    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
		    UiEngineInstance engine = Ui.getUiEngineInstance();
		    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
		     TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
		     transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 2000);
		     transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
		     transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
		    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
		    	 engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
	    		bgManager = new BgManager(MainClass.BG_GRAY);

	    		correo = new LabelField("Digite su correo electrónico: ");
	    		correo.setFont(MainClass.appFont);
		
		hfmMain = new HorizontalFieldManager(VerticalFieldManager.FIELD_HCENTER|HorizontalFieldManager.FIELD_HCENTER|Field.FIELD_HCENTER|VERTICAL_SCROLL){
			public int getPreferredWidth() {
				// TODO Auto-generated method stub
				return Display.getWidth();
			}
			
			protected void sublayout(int maxWidth, int maxHeight) {
				// TODO Auto-generated method stub
				super.sublayout(getPreferredWidth(), maxHeight);
			}
		};
		hfmBtns = new HorizontalFieldManagerS(HorizontalFieldManager.FIELD_HCENTER| HorizontalFieldManager.USE_ALL_WIDTH,Bitmap.getBitmapResource(MainClass.BTN_REGISTER).getWidth());
		hfmTitle = new HorizontalFieldManager(Field.FIELD_HCENTER);
		
		vfmFooter = new VerticalFieldManager(VERTICAL_SCROLL){
			public int getPreferredWidth() {
				// TODO Auto-generated method stub
				return Display.getWidth();
			}
			
			protected void sublayout(int maxWidth, int maxHeight) {
				// TODO Auto-generated method stub
				super.sublayout(getPreferredWidth(), maxHeight);
			}
		};
		
		customVerticalFieldManager = new CustomVerticalFieldManager(Display.getWidth(),Display.getHeight(), HorizontalFieldManager.FIELD_LEFT| VERTICAL_SCROLL);
		//customVerticalFieldManager2 = new CustomVerticalFieldManager(Display.getWidth(),145, HorizontalFieldManager.FIELD_LEFT| VERTICAL_SCROLL);
		
		basicUser = new BorderBasicEditField("","", 60, Display.getWidth(), Field.FOCUSABLE);
		
		
		btnOk =  new GenericImageButtonField(MainClass.BTN_OK_OVER,MainClass.BTN_OK,"ok"){
			
			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub				
				if(!basicUser.getText().equals("")){
					
					if(isEmail(basicUser.getText())){
						MainClass.splashScreen.start();	
						String cadena=Crypto.aesEncrypt(MainClass.key, "{\"cadena\":\""+basicUser.getText().trim()+"\"}");
						//String json = Crypto.aesEncrypt(MainClass.key,remplazacadena(basicUser.getText().trim()));
						new PasswordThread(cadena).start();
						
					}
					else{
						
						Dialog.alert("Escribe un correo electrónico válido");
					}
					}else{
						
						Dialog.alert("Escribe un correo electrónico válido");
					}
				
				return true;
			}
			
		};		
		
		contact =new LabelField("Ó comuníquese con Addcel a soporte@addcel.com o a los teléfonos en la Cd. de México y área metropolitana 52575140 ó del interior de la república al 01 800 1223324 durante días hábiles.");
		
	}
	
	public void addFields(){		
		
		add(new NullField());
		BitmapField bField = new BitmapField(Bitmap.getBitmapResource(MainClass.BG_LOGIN));
		
		customVerticalFieldManager.add(bField);
		customVerticalFieldManager.add(new LabelField(""));
	
		customVerticalFieldManager.add(correo);
		customVerticalFieldManager.add(basicUser);
		
		customVerticalFieldManager.add(new LabelField(""));		
		
		hfmBtns.add(btnOk);
		
		customVerticalFieldManager.add(hfmBtns);
		contact.setPadding(20, 0, 0, 0);
		customVerticalFieldManager.add(contact);
		customVerticalFieldManager.add(new LabelField("",Field.NON_FOCUSABLE));
		
		hfmMain.add(customVerticalFieldManager);
		
		vfmFooter.add(hfmMain);
		
		bgManager.add(vfmFooter);
				
		add(bgManager);		
		
	}
	
	protected boolean onSavePrompt() {
		return true;
	}
	
	public void cleanFields(){
		
		basicUser.setText("");
		
	}
	
	
	protected boolean keyDown(int keycode, int time)
	   {
	        if (Keypad.key(keycode) == Characters.ESCAPE) 
	        {
	        	close();
	        	return true;
	        }
	        return false;
	   }
	
	public boolean isEmail(String correo){
		boolean escorreo=true;
		if(correo.indexOf("@")==-1){
			escorreo=false;
		}
		if(correo.indexOf(".")==-1){
			escorreo=false;
		}
		
		return escorreo;
		
	}
	
	public String reemplaza(String src, String orig, String nuevo) {
        if (src == null) {
            return null;
        }
        int tmp = 0;
        String srcnew = "";
        while (tmp >= 0) {
            tmp = src.indexOf(orig);
            if (tmp >= 0) {
                if (tmp > 0) {
                     srcnew += src.substring(0, tmp);
                }
                srcnew += nuevo;
                src = src.substring(tmp + orig.length());
            }
        }
        srcnew += src;
        return srcnew;
    }

    public String remplazacadena(String conAcentos) {
        String caracteres[] = {"@", "."};
        String letras[] = {"|arroba|", "|punto|"};
        for (int counter = 0; counter < caracteres.length; counter++) {
            conAcentos = reemplaza(conAcentos, caracteres[counter], letras[counter]);
        }
        return conAcentos;
    }	
}

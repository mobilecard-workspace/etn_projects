package mx.com.ib.etn.view.roundflight;

import mx.com.ib.etn.beans.dto.Router;
import mx.com.ib.etn.beans.dto.Scheduled;
import mx.com.ib.etn.model.data.DataKeeper;
import mx.com.ib.etn.model.data.Payer;
import mx.com.ib.etn.utils.UtilColor;
import mx.com.ib.etn.utils.UtilIcon;
import mx.com.ib.etn.view.controls.CustomMainScreen;
import mx.com.ib.etn.view.controls.TitleOrangeRichTextField;
import mx.com.ib.etn.view.controls.Viewable;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;

import org.json.me.JSONObject;

public class ViewRoundFligth extends CustomMainScreen implements
FieldChangeListener, Viewable {

	
	private DataKeeper dataKeeper;
	private ButtonField accept;
	private boolean isBasicFlight;

	private JSONObject jsDepart;
	private JSONObject jsArrive;

	public ViewRoundFligth(boolean isBasicFlight) {

		super();

		this.isBasicFlight = isBasicFlight;
		
		BitmapField bitmapField = UtilIcon.getTitle();
		add(bitmapField);

		TitleOrangeRichTextField orangeRichTextField = new TitleOrangeRichTextField(
				"Paso 5 - Asignación de asientos");
		add(orangeRichTextField);
		add(new LabelField(""));
		
		
		dataKeeper = DataKeeper.getInstance();
		
		Scheduled scheduledDepart = dataKeeper.getScheduledDepart(); 
		Scheduled scheduledArrive = dataKeeper.getScheduledArrive();
		Router routerDepart = dataKeeper.getRouterDepart();
		Router routerArrive = dataKeeper.getRouterArrive();

		CustomGridFieldRoundFligth gridFieldRoundFligthDeparte = new CustomGridFieldRoundFligth(true, isBasicFlight, routerDepart, routerArrive, scheduledDepart);
		add(gridFieldRoundFligthDeparte);
		
		CustomGridFieldRoundFligth gridFieldRoundFligthArrive = new CustomGridFieldRoundFligth(false, isBasicFlight, routerArrive, routerDepart, scheduledArrive);
		add(gridFieldRoundFligthArrive);
		
		accept = new ButtonField("Hacer pago");
		accept.setChangeListener(this);
		add(accept);
	}

	
	public void sendMessage(String message) {

		Dialog.alert(message);
	}
	
	public void setData(int request, JSONObject jsObject) {

		if (jsObject == null){
			final int OK = 0;
			final int DEFAULT = OK;
			int answer = 0;

			answer = Dialog.ask("No se pudo interpretar la respuesta, por favor consulte su saldo.", new String[] {"Salir"}, new int[] {OK}, DEFAULT);

			switch (answer) {
			case OK:
				UiApplication.getUiApplication().popScreen(this);
				break;
			}
		} else {

			if (payment == 0){

				jsDepart = jsObject;
				payment = 1;
				Payer payer = new Payer();
				payer.toPay(false, isBasicFlight, this);
			}

			if (payment == 1){

				jsArrive = jsObject;
			}
		}
	}

	private int payment = 0;
	
	public void fieldChanged(Field field, int context) {

		if (field == (ButtonField)accept){

			payment = 0;
			
			if ((dataKeeper.getSeatsDepart()!= null)&&(dataKeeper.getSeatsArrive()!= null)){
				
				Payer payer = new Payer();
				payer.toPay(true, isBasicFlight, this);
				//payer.toPay(false, isBasicFlight, this);
			} else {
				
				Dialog.alert("Falta Asignar Asientos");
			}
		}
	}


	public boolean onClose(){

		dataKeeper.setPassengersArrive(null);
		dataKeeper.setPassengersDepart(null);

		dataKeeper.setSeatsDepart(null);
		dataKeeper.setSeatsArrive(null);

		return super.onClose();
	}
}
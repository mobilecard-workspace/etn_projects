package mx.com.ib.etn.view.vroute;

import java.util.Vector;

import mx.com.ib.etn.beans.dto.Scheduled;
import mx.com.ib.etn.beans.dto.VRouter;
import mx.com.ib.etn.model.connection.base.ThreadHTTP;
import mx.com.ib.etn.model.creatorbeans.fromjs.JSVRouter;
import mx.com.ib.etn.utils.UtilDate;
import mx.com.ib.etn.utils.UtilIcon;
import mx.com.ib.etn.view.animated.SplashScreen;
import mx.com.ib.etn.view.controls.CustomMainScreen;
import mx.com.ib.etn.view.controls.TitleOrangeRichTextField;
import mx.com.ib.etn.view.controls.Viewable;
import net.rim.blackberry.api.browser.URLEncodedPostData;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;

import org.json.me.JSONException;
import org.json.me.JSONObject;

public class ViewRoute extends CustomMainScreen implements Viewable {


	private Scheduled scheduled;
	private String depart;
	private SplashScreen splashScreen;
	
	public ViewRoute(Scheduled scheduled, String depart){

		super();
		
		BitmapField bitmapField = UtilIcon.getTitle();
		add(bitmapField);
		
		TitleOrangeRichTextField orangeRichTextField = new TitleOrangeRichTextField("Paso 4 - Revisar Itinerario");
		add(orangeRichTextField);
		add(new LabelField(""));
		
		URLEncodedPostData encodedPostData = new URLEncodedPostData(
				URLEncodedPostData.DEFAULT_CHARSET, false);

		String dateDepart = scheduled.getDateDepart();

		String date = UtilDate.getDD_MM_AAAAtoDDMMAAAA(dateDepart);
		
		splashScreen = SplashScreen.getInstance();
		splashScreen.start();
		
		encodedPostData.append("corrida", scheduled.getFlight());
		encodedPostData.append("fecha", date);
		encodedPostData.append("origen", depart);

		ThreadHTTP threadHTTP = new ThreadHTTP();
		threadHTTP.setHttpConfigurator(Viewable.VROUTE, this, encodedPostData);
		threadHTTP.start();
	}
	
	
	public void sendMessage(String message) {

		Dialog.alert(message);
	}
	
	public void setData(int request, JSONObject jsObject) {

		splashScreen.remove();
		
		JSVRouter jsRouter = new JSVRouter();
		
		try {
			Vector vectorVRouters = jsRouter.getVector(jsObject);

			int size = vectorVRouters.size();

			VRouter depart = null;
			VRouter arrive = null;
			boolean bDepart = true;
			
			for(int i = 0; i < size; i++){
				
				if (bDepart){
					
					depart = (VRouter)vectorVRouters.elementAt(i);
					bDepart = false;
				}

				if (!bDepart){

					if(i > 0){
						arrive = (VRouter)vectorVRouters.elementAt(i);

						CustomGridFieldVRouter fieldVRouter = new CustomGridFieldVRouter(depart, arrive);
						
						if (fieldVRouter.setData()){
							add(fieldVRouter);
						}

						bDepart = false;
						depart = arrive;
						arrive = null;
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
			Dialog.alert("Error al interpretar los datos");
		}
	}
}

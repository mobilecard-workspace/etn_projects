package mx.com.ib.etn.view.vroute;

import mx.com.ib.etn.beans.dto.VRouter;
import mx.com.ib.etn.utils.UtilColor;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class CustomGridFieldVRouter extends VerticalFieldManager {

	private VRouter depart;
	private VRouter arrive;
	
	public CustomGridFieldVRouter(VRouter depart, VRouter arrive){
		super();
		this.depart = depart;
		this.arrive = arrive;
	}
	
	public boolean setData(){
		
		boolean result = false;
		
		if ((depart != null)&&(arrive != null)){
			result = true;
			DepartRichTextField departRichTextField = new DepartRichTextField(depart);
			TimeDepartRichTextField timeDepartRichTextField1 = new TimeDepartRichTextField(depart);
			TimeArriveRichTextField timeArriveRichTextField1 = new TimeArriveRichTextField(depart);
			ArriveRichTextField arriveRichTextField = new ArriveRichTextField(arrive);
			TimeDepartRichTextField timeDepartRichTextField2 = new TimeDepartRichTextField(arrive);
			TimeArriveRichTextField timeArriveRichTextField2 = new TimeArriveRichTextField(arrive);
			
			add(departRichTextField);
			add(timeDepartRichTextField1);
			add(arriveRichTextField);
			add(timeDepartRichTextField2);
		}
		
		return result;
	}
	
	
	protected void paint(Graphics graphics){

		graphics.clear();
		
		if (isFocus()){
			graphics.setColor(UtilColor.ELEMENT_FOCUS);
			graphics.setGlobalAlpha(200);
			graphics.fillRoundRect(0, 0, getPreferredWidth(), getPreferredHeight(), 1, 1);
			graphics.setGlobalAlpha(255);
		} else {
			graphics.setColor(UtilColor.ELEMENT_BACKGROUND);
			graphics.fillRoundRect(0, 0, getPreferredWidth(), getPreferredHeight(), 1, 1);
		}
		
		super.paint(graphics);
	}
	
	public boolean isFocusable(){
		return true;
	}
	
	public int getPreferredWidth(){
		return Display.getWidth();
	}
	
	
	protected boolean navigationClick(int status, int time) {
		fieldChangeNotify(0);
		return super.navigationClick(status, time);
	}
	
	protected boolean keyChar(char character, int status, int time) {
		if (character == Keypad.KEY_ENTER) {
			fieldChangeNotify(0);
			return true;
		}
		return super.keyChar(character, status, time);
	}

	protected void onFocus(int direction){
		super.onFocus(direction);
		invalidate();
	}
	
	protected void onUnfocus(){
		super.onUnfocus();
		invalidate();
	}
}

package mx.com.ib.etn.model.creatorbeans.fromjs;

import java.util.Vector;

import mx.com.ib.etn.beans.dto.VRouter;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

public class JSVRouter {

	public Vector getVector(JSONObject jsObject) throws JSONException{
		
		JSONArray jsonArray = jsObject.getJSONArray("listaItinerarios");

		VRouter router = null;
		Vector routers = new Vector();
		
		int length = jsonArray.length();
		
        for (int i = 0; i < length; i++) {
             JSONObject childJSONObject = jsonArray.getJSONObject(i);
             
             router = new VRouter();
             router.setOffice(childJSONObject.getString("oficina"));
             router.setDateDepart(childJSONObject.getString("fechaSalida"));
             router.setHourDepart(childJSONObject.getString("horaSalida"));
             router.setStringError(childJSONObject.getString("errorCadena"));
             router.setNumberError(childJSONObject.getString("errorNumero"));

             routers.addElement(router);
        }

        return routers;
	}
}

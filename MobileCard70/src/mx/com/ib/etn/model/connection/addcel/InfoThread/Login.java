package mx.com.ib.etn.model.connection.addcel.InfoThread;

import java.io.UnsupportedEncodingException;
import java.util.Vector;

import mx.com.ib.etn.model.connection.Url;
import mx.com.ib.etn.model.connection.addcel.base.Communicator;
import mx.com.ib.etn.model.connection.addcel.base.HttpListener;
import mx.com.ib.etn.model.connection.addcel.base.HttpPoster;
import mx.com.ib.etn.model.connection.addcel.json.JSONParser;
import mx.com.ib.etn.utils.UtilBB;
import mx.com.ib.etn.utils.UtilSecurity;
import mx.com.ib.etn.view.animated.SplashScreen;
import mx.com.ib.etn.view.controls.Viewable;
import mx.com.ib.etn.view.etnbuy.ViewETNBuy;
import net.rim.device.api.system.Application;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.MainScreen;

//public class Login extends Thread implements HttpListener {
public class Login implements HttpListener {

	private String url = Url.URL_LOGIN;

	private String password = null;
	private String json = null;
	private String jsonEncrypt = null;
	private String jsonEncrypt2 = null;

	private String post; // verificar this.post = "json="+json;
	private HttpPoster poster;
	private Viewable viewable;
	private SplashScreen splashScreen;

	public Login(Viewable viewable, String user, String password) {

		this.viewable = viewable;
		splashScreen = SplashScreen.getInstance();

		try {

			if ((user != null) && (password != null)) {

				user = user.trim();
				this.password = password.trim();
				json = createJSon(user, password);
				String parse = UtilSecurity.parsePass(password);
				jsonEncrypt = UtilSecurity.aesEncrypt(parse, json);
				jsonEncrypt2 = UtilSecurity.mergeStr(jsonEncrypt, password);
				this.post = "json=" + jsonEncrypt2;
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			viewable.sendMessage("Error de codificaci�n.");
		}
	}

	private String createJSon(String user, String password)
			throws UnsupportedEncodingException {

		StringBuffer json = new StringBuffer();

		// json.append("json=");
		json.append("{\"login\":\"").append(user);
		json.append("\",\"tipo\":\"").append(
				UtilBB.getDeviceINFO(UtilBB.MANUFACTURER_NAME));
		json.append("\",\"imei\":\"").append(UtilBB.getImei());
		json.append("\",\"modelo\":\"").append(
				UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME));
		json.append("\",\"software\":\"").append(
				UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION));
		json.append("\",\"key\":\"").append(UtilBB.getImei());
		json.append("\",\"password\":\"").append(password);
		json.append("\",\"passwordS\":\"").append(UtilSecurity.sha1(password));
		json.append("\"}");

		return json.toString();
	}

	public void connect() {

		String idealConnection = UtilBB.checkConnectionType();

		if (idealConnection != null) {

			if (this.url != null) {
				try {

					Communicator communicator = Communicator.getInstance();
					communicator.sendHttpPost(this, this.url, this.post);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else {
			viewable.sendMessage("No se pudo crear una conexi�n, intenta de nuevo por favor.");
		}
	}

	public void run() {
		//this.setPriority(Thread.MAX_PRIORITY);
		connect();
	}

	public void handleHttpError(int errorCode, String error) {
		getMessageError(error);
	}

	public boolean isDestroyed() {
		return false;
	}

	public void receiveEstatus(String msg) {
	}

	public void receiveHeaders(Vector _headers) {
	}

	public void receiveHttpResponse(int appCode, byte[] response) {

		String sTemp = null;
		StringBuffer sb = new StringBuffer();

		try {
			sb.append(new String(response, 0, response.length, "UTF-8"));

			sTemp = new String(sb.toString());

			sTemp = UtilSecurity.aesDecrypt(UtilSecurity.parsePass(password),
					sTemp);

			// UiApplication.getUiApplication();
			synchronized (Application.getEventLock()) {

				splashScreen.remove();

				JSONParser jsParser = new JSONParser();

				if (jsParser.isLogin(sTemp)) {
					/*
					 * this.screen.cleanFields();
					 * UiApplication.getUiApplication().pushScreen( new
					 * MenuScreen());
					 */
					// UiApplication.getUiApplication().;
					UiApplication.getUiApplication().popScreen((MainScreen)viewable);
					UiApplication.getUiApplication().pushScreen(new ViewETNBuy(null));

				} else if (jsParser.isLogin2(sTemp)) {
					// MainClass.userBean = null;
					/*
					 * this.screen.cleanFields();
					 * 
					 * UiApplication.getUiApplication().pushScreen( new
					 * UpdateUser("", Field.NON_FOCUSABLE));
					 * UiApplication.getUiApplication().pushScreen( new
					 * MessagePopupScreen(MainClass.msg, Field.NON_FOCUSABLE));
					 */
				} else if (jsParser.isLogin3(sTemp)) {

					// this.screen.cleanFields();
					// UiApplication.getUiApplication().pushScreen(new
					// PswdPopupScreen98("", Field.NON_FOCUSABLE));
					// UiApplication.getUiApplication().pushScreen(new
					// MessagePopupScreen(MainClass.msg, Field.NON_FOCUSABLE));

				} else {
					viewable.sendMessage("El usuario y/o contrase�a son inv�lidos.");
					// Correci�n
					/*
					 * MainClass.userBean = null;
					 * UiApplication.getUiApplication().pushScreen( new
					 * MessagePopupScreen(MainClass.jsParser
					 * .getMessageError(sTemp), Field.NON_FOCUSABLE));
					 */
				}
			}

		} catch (UnsupportedEncodingException e) {
			viewable.sendMessage("Error al codificar la respuesta. Intenta de nuevo por favor.");
			e.printStackTrace();
		}
	}

	public void getMessageError(String error) {

		synchronized (Application.getEventLock()) {

			splashScreen.remove();
			viewable.sendMessage(error);
		}
	}
}



/*
public void runs() {
	for (int i = 0; i < 10; i++) {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException ex) {
		}
		// Ensure we have the event lock
		synchronized (UiApplication.getEventLock()) {
			mainScreen.appendLabelText("Update");
		}
	}
}
*/
package mx.com.ib.etn.model.data;


import mx.com.ib.etn.beans.dto.Scheduled;
import mx.com.ib.etn.model.connection.base.ThreadHTTP;
import mx.com.ib.etn.utils.UtilBB;
import mx.com.ib.etn.view.controls.Viewable;

import net.rim.blackberry.api.browser.URLEncodedPostData;

public class Payer {

	public void toPay(boolean isDepartFlight, boolean isBasicFlight, Viewable viewable){
		
		URLEncodedPostData encodedPostData = new URLEncodedPostData(
				URLEncodedPostData.DEFAULT_CHARSET, false);

		DataKeeper dataKeeper = DataKeeper.getInstance();
		DataTravel dataTravel = dataKeeper.getDataTravel();

		if (isDepartFlight){
			Scheduled scheduled = dataKeeper.getScheduledDepart();
			
			encodedPostData.append("origen", dataTravel.getDepart());
			encodedPostData.append("destino", dataTravel.getArrive());
			
			encodedPostData.append("fecha", scheduled.getDateDepartToString());
			encodedPostData.append("hora", scheduled.getHourDepartToString());
			encodedPostData.append("corrida", scheduled.getFlight());

		} else {
			Scheduled scheduled = dataKeeper.getScheduledArrive();
			
			encodedPostData.append("origen", dataTravel.getDepart());
			encodedPostData.append("destino", dataTravel.getArrive());
			
			encodedPostData.append("fecha", scheduled.getDateDepartToString());
			encodedPostData.append("hora", scheduled.getHourDepartToString());
			encodedPostData.append("corrida", scheduled.getFlight());
		}

		encodedPostData.append("numAdulto", String.valueOf(dataTravel.getiAdults()));
		encodedPostData.append("numNino", String.valueOf(dataTravel.getiChildren()));
		encodedPostData.append("numInsen", String.valueOf(dataTravel.getiElderlies()));
		encodedPostData.append("numEstudiante", String.valueOf(dataTravel.getiStudents()));
		encodedPostData.append("numMaestro", String.valueOf(dataTravel.getiProfessors()));

		//encodedPostData.append("idUsuario", MainClass.userBean.getIdUsuario());
		encodedPostData.append("idUsuario", "1344351105776");
		encodedPostData.append("imei", UtilBB.getImei());
		//encodedPostData.append("modelo", MainClass.MODELO);
		encodedPostData.append("modelo", UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME));
		//encodedPostData.append("software", MainClass.SOFTWARE);
		encodedPostData.append("software", UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION));
		//encodedPostData.append("tel�fono", MainClass.userBean.getPhone());
		encodedPostData.append("telefono", "9999225689");
		encodedPostData.append("tipoProducto", "ETNMC");
		//encodedPostData.append("tipoTelefono", MainClass.TIPO);
		encodedPostData.append("tipoTelefono", UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME));
		encodedPostData.append("wkey", UtilBB.getImei());
		
		if (isBasicFlight){
			encodedPostData.append("tipoViaje", "S");
		} else {
			encodedPostData.append("tipoViaje", "R");
		}



		String seats = null;
		String names = null;

		if (isDepartFlight){
			dataKeeper.createSeatsNamesDepart();
			seats = dataKeeper.getStringSeatsDepart();
			names = dataKeeper.getStringNamesDepart();
		} else {
			dataKeeper.createSeatsNamesArrive();
			seats = dataKeeper.getStringSeatsArrive();
			names = dataKeeper.getStringNamesArrive();
		}

		encodedPostData.append("asientos", seats);
		encodedPostData.append("nombres", names);

		ThreadHTTP threadHTTP = new ThreadHTTP();
		threadHTTP.setHttpConfigurator(Viewable.CREATE, viewable,
				encodedPostData);
		threadHTTP.start();
	}
}

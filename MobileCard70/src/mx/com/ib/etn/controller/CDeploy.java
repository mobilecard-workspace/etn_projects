package mx.com.ib.etn.controller;

import mx.com.ib.etn.view.scheduled.ViewScheduledDepart;
import mx.com.ib.etn.view.etnbuy.ViewETNBuy;
import net.rim.device.api.ui.Screen;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.MainScreen;


public class CDeploy {
	
	public static final int BUY = 0;
	public static final int SCHEDULED = 1;
	
	public void displayView(MainScreen mainScreen, int action) throws ClassNotFoundException{
		
		Screen screen = null;
		
		switch (action) {
		case BUY:
			screen = new ViewETNBuy(null);
			UiApplication.getUiApplication().popScreen(mainScreen);
			UiApplication.getUiApplication().pushScreen((Screen)screen);
			break;
		case SCHEDULED:
			screen = new ViewScheduledDepart(true, true);
			UiApplication.getUiApplication().popScreen(mainScreen);
			UiApplication.getUiApplication().pushScreen((Screen)screen);
			break;
		}
	}
}

package mx.com.ib.etn.utils;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.component.BitmapField;

public class UtilIcon {

	
	public static BitmapField getTitle(){
		
		String resolution = String.valueOf(Display.getWidth())  + "_titulo_principal.png";
		
		Bitmap encodedImage = Bitmap.getBitmapResource(resolution);
		BitmapField bitmapField = new BitmapField(encodedImage);

		
		return bitmapField;
	}

	
	
	public static BitmapField getTitleEDO(){
		
		String resolution = String.valueOf(Display.getWidth())  + "_header_compras_edo.png";
		
		Bitmap encodedImage = Bitmap.getBitmapResource(resolution);
		BitmapField bitmapField = new BitmapField(encodedImage);

		
		return bitmapField;
	}
	
	
	public static BitmapField getSplash(){
		
		//240_260_splash
		
		int width = Display.getWidth();
		int height = Display.getHeight();
		
		StringBuffer stringBuffer = new StringBuffer();
		
		stringBuffer.append(width).append("_");
		stringBuffer.append(height).append("_");
		stringBuffer.append("splash.png");
		
		String splash = stringBuffer.toString();
		
		Bitmap encodedImage = Bitmap.getBitmapResource(splash);
		BitmapField bitmapField = new BitmapField(encodedImage);

		
		return bitmapField;
	}
	
}

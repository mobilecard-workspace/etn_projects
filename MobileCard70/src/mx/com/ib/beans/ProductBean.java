package mx.com.ib.beans;

public class ProductBean {
	
	private String clave = "";
	private String path = "";
	private String description = "";	
	
	public ProductBean() {
		super();
	}
	
	public ProductBean(String clave, String path) {
		super();
		this.clave = clave;
		this.path = path;		
	}

	public ProductBean(String clave, String path, String description) {
		super();
		this.clave = clave;
		this.path = path;
		this.description = description;
	}

	public String getClave() {
		return clave;
	}
	
	public void setClave(String clave) {
		this.clave = clave;
	}
	
	public String getPath() {
		return path;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

}

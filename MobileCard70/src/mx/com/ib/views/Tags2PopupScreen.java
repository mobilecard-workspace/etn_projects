package mx.com.ib.views;


import mx.com.ib.app.MainClass;
import mx.com.ib.controls.BorderBasicEditField;
import mx.com.ib.controls.GenericImageButtonField;
import mx.com.ib.threads.GetTagsThread;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.ChoiceField;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class Tags2PopupScreen extends PopupScreen implements FieldChangeListener{
	
	public LabelField TipoTag = null;
	public LabelField nick = null,pin=null,clave=null;
	public LabelField img2 = null;
	public LabelField img3 = null;	
	
	
	public BorderBasicEditField basicLabel = null;	
	public BorderBasicEditField basicClave = null;
	public BorderBasicEditField basicPin = null;	
	
	public HorizontalFieldManager hfmTitle = null;
	public HorizontalFieldManager hfmBtn = null;
	
	public HorizontalFieldManager tagHFM = null;
	public HorizontalFieldManager dvHFM = null;
	
	public VerticalFieldManager vfmInternal = null;
	
	public String tagNames[] = null;
	public ObjectChoiceField choicesTagNames  = null;
	
	public int widthManager = 0;
	public boolean isLogin = false;
	
	public GenericImageButtonField btnChange = null;
	private NewAccountScreen actualizate=null;
	
	public Tags2PopupScreen(String text, long style,boolean isLogin,NewAccountScreen actualizate){
		
		super(new VerticalFieldManager(VERTICAL_SCROLL));
		this.isLogin=isLogin;
		this.actualizate=actualizate;
		initVariables();
		
		addFields();
		
	}
public Tags2PopupScreen(String text, long style,boolean isLogin){
		
		super(new VerticalFieldManager(VERTICAL_SCROLL));
		this.isLogin=isLogin;
		
		initVariables();
		
		addFields();
		
	}
	
	public void initVariables(){
	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	     TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	     transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 2000);
	     transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	     transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    	 engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
		hfmTitle = new HorizontalFieldManager(Field.FIELD_HCENTER);
		hfmBtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		
		widthManager = Display.getWidth() - 100;
		
		TipoTag = new LabelField("Tus Tag�s registrados: ", Field.NON_FOCUSABLE);
		nick = new LabelField("Presiona sobre ellos para eliminarlo: ", Field.NON_FOCUSABLE);
		pin = new LabelField("D�gito verificador: ");
		
		vfmInternal = new VerticalFieldManager(VERTICAL_SCROLL);
		tagHFM    = new HorizontalFieldManager(Field.FIELD_LEFT);
		dvHFM     = new HorizontalFieldManager(Field.FIELD_LEFT);
		basicClave = new BorderBasicEditField("","", 14, Display.getWidth()-20, Field.FOCUSABLE|EditField.FILTER_NUMERIC);
		basicPin = new BorderBasicEditField("","", 1, Display.getWidth()-20, Field.FOCUSABLE|EditField.FILTER_NUMERIC);
		
		basicLabel = new BorderBasicEditField("","", 12, Display.getWidth(), Field.FOCUSABLE);		
		
		btnChange =  new GenericImageButtonField(MainClass.BTN_BACK_OVER, MainClass.BTN_BACK,"continuar"){
			
			protected boolean navigationClick(int status, int time) {
				UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
				//UiApplication.getUiApplication().popScreen(this);	
				return true;
			}
			
		};
		
		tagNames = new String[MainClass.TagsBeans.length];
		
		for(int i = 0; i < MainClass.TagsBeans.length; i++){
			tagNames[i] = MainClass.TagsBeans[i].getNombre();
		}
		
		choicesTagNames = new ObjectChoiceField("TIPO:", tagNames){
			protected void paint(Graphics graphics) {
				
				// TODO Auto-generated method stub
				super.paint(graphics);
				invalidate();
			}
			
			protected void drawFocus(Graphics graphics, boolean on) {
				// TODO Auto-generated method stub
				super.drawFocus(graphics, on);
				invalidate();
			}
			
			//protected boolean navigationClick(int status, int time) {
				
				// TODO Auto-generated method stub
				//MainClass.splashScreen.start();
				//vfmInternal.deleteAll();
				//new GetTagsThread(MainClass.TagsBeans[choicesTagNames.getSelectedIndex()].getClave(),vfmInternal).start();
				//return true;
			//}
			
			
			
		};
	choicesTagNames.setChangeListener(new FieldChangeListener(){

		public void fieldChanged(Field field, int context) {
			// TODO Auto-generated method stub
			System.out.println(choicesTagNames.getSelectedIndex());
				if(context == ChoiceField.CONTEXT_CHANGE_OPTION){
					MainClass.splashScreen.start();
					vfmInternal.deleteAll();
					new GetTagsThread(MainClass.TagsBeans[choicesTagNames.getSelectedIndex()].getClave(),vfmInternal).start();
				}
		}
		
	});
	///choicesTagNames.
		

	
		
	}
	
	public void addFields(){
		add(new NullField());
		hfmTitle.add(TipoTag);
		hfmBtn.add(btnChange);
		add(hfmTitle);
		add(new LabelField(""));
		add(choicesTagNames);
		add(new LabelField(""));
		add(nick);
		add(new LabelField(""));
		add(vfmInternal);
		add(new LabelField(""));
		add(hfmBtn);
		MainClass.splashScreen.start();
		vfmInternal.deleteAll();
		new GetTagsThread(MainClass.TagsBeans[choicesTagNames.getSelectedIndex()].getClave(),vfmInternal).start();
		
		
		
	}

	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		
	}
	
	public boolean keyChar(char key, int status, int time) {
	    //intercept the ESC key
	    boolean retval = false;
	    switch (key) {

	        case Characters.ESCAPE:
	            close();
	            break;
	        default:
	            retval = super.keyChar(key,status,time);
	    }
	    return retval;
	}
	
	public void close() {
		// TODO Auto-generated method stub
		UiApplication.getUiApplication().popScreen(this);
	}
	
	

}
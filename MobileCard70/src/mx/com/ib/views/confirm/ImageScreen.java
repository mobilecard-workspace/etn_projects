package mx.com.ib.views.confirm;


import java.util.Timer;

import mx.com.ib.app.MainClass;
import mx.com.ib.controls.BgManager;
import mx.com.ib.utils.Utils;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;

public class ImageScreen extends MainScreen{
	
	public HorizontalFieldManager hfmMain = null;
	public HorizontalFieldManager hfmImage = null;
	public HorizontalFieldManager hfmBtns = null;	
	public LabelField title = null;
	public LabelField terms = null;	
	public BgManager bgManager = null;
	
	Timer thBanner = null;
	
	
	
	public ImageScreen(){
		
		initVariables();
		
		addFields();
		
	}
	
	public void initVariables(){
	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	     TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	     transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	     transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	     transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    	 engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
		
		
	
		
		bgManager = new BgManager(MainClass.BG_VITA);
				
		
		hfmMain = new HorizontalFieldManager(Field.FIELD_HCENTER|VERTICAL_SCROLL);
		hfmImage = new HorizontalFieldManager(Field.FIELD_HCENTER);
		hfmBtns = new HorizontalFieldManager(Field.FIELD_HCENTER);
		
		
		
	}
	
	public void addFields(){
		
		BitmapField bField = new BitmapField(Utils.resizeBitmap2(Bitmap.getBitmapResource(MainClass.BG_VITA), Display.getWidth(), Display.getHeight()));
		
		bgManager.add(bField);
		MainClass.splashScreen.remove();
		add(bgManager);
		  
		  
	}
	
	protected boolean onSavePrompt() {
		return true;
	}
	
	
	
	
	protected boolean keyDown(int keycode, int time)
	   {

	        if (Keypad.key(keycode) == Characters.ESCAPE) 
	        {
	        	UiApplication.getUiApplication().popScreen(this);
	        	return true;
	        }
	        return false;
	   }

    

}

package mx.com.ib.views;

import mx.com.ib.app.MainClass;
import mx.com.ib.controls.BgManager;
import mx.com.ib.controls.CustomVerticalFieldManager;
import mx.com.ib.controls.GenericImageButtonField;
import net.rim.blackberry.api.browser.Browser;
import net.rim.blackberry.api.browser.BrowserSession;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.ActiveRichTextField;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class ScreenVersion  extends MainScreen {
	
	
	public HorizontalFieldManager hfmMain = null;
	public HorizontalFieldManager hfmTitle = null;
	public HorizontalFieldManager hfmTitle2 = null;
	
	public CustomVerticalFieldManager customVerticalFieldManager = null;
	public CustomVerticalFieldManager customVerticalFieldManager2 = null;
	
	public GenericImageButtonField btnBuy = null;
	public GenericImageButtonField btnConsult = null;
	public GenericImageButtonField btnAccount = null;
	public GenericImageButtonField btnPromotions = null;
	public GenericImageButtonField btnExit = null;
	
	public LabelField version = null;
	public LabelField text = null;
	public LabelField informacion = null;
	public LabelField telefono = null;
	public LabelField contacto = null;
	public ActiveRichTextField note  = null;



	
	public LabelField privacidad2 = null;

	
	public BgManager bgManager = null;
	public Bitmap bField=null;
	public String tipo="",prioridad="";
	public VerticalFieldManager vfmFooter = null;
	
	public GenericImageButtonField btnDescargar = null;
	public GenericImageButtonField btnIgnorar = null;	

	public ScreenVersion(String tipo,String prioridad){
		this.tipo=tipo;
		this.prioridad=prioridad;
		initVariables();
		addFields();
	}
	
	public void initVariables(){
		TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	     TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	     transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 2000);
	     transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	     transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    	 engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
		bgManager = new BgManager(MainClass.BG_GRAY);
		bgManager = new BgManager(MainClass.BG_MENU);


		hfmMain = new HorizontalFieldManager(Field.FIELD_HCENTER|VERTICAL_SCROLL|Field.USE_ALL_WIDTH);
		hfmTitle = new HorizontalFieldManager(Field.FIELD_RIGHT);
		hfmTitle2 = new HorizontalFieldManager(Field.FIELD_HCENTER);
		vfmFooter = new VerticalFieldManager(VERTICAL_SCROLL);
		version= new LabelField("�IMPORTANTE!");
		
		version.setFont(MainClass.alphaSansFamily.getFont(Font.PLAIN, 10, Ui.UNITS_pt));
		
		informacion= new LabelField("Para mayor informaci�n cominucate al:",Field.FIELD_HCENTER);
		text= new LabelField("Hay una nueva versi�n de MobileCard disponible.",Field.FIELD_HCENTER);
		note = new ActiveRichTextField("Dudas o aclaraciones cont�ctanos a soporte@addcel.com o al tel�fono en D.F. y �rea metropolitana 52575140 � del interiorde la rep�blica al 018001223324",Field.FOCUSABLE);
		note.select(true);
			btnDescargar =  new GenericImageButtonField(MainClass.BTN_DESCARGAR_OVER,MainClass.BTN_DESCARGAR,"descargar"){
			
			protected boolean navigationClick(int status, int time) {
				BrowserSession browserSession; 
    			browserSession = Browser.getDefaultSession();
    			
    			browserSession.displayPage(tipo);
				return true;
			}
			
		};
		
			btnIgnorar =  new GenericImageButtonField(MainClass.BTN_IGNORAR_OVER,MainClass.BTN_IGNORAR,"ignorar"){
			
			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub
				UiApplication.getUiApplication().pushScreen(new LoginScreen());
				return true;
			}
			
		};
	

		note.setFont(MainClass.appFont1);
		
		customVerticalFieldManager = new CustomVerticalFieldManager(Display.getWidth()/2,Display.getHeight(), HorizontalFieldManager.USE_ALL_WIDTH|Field.FIELD_VCENTER|VerticalFieldManager.VERTICAL_SCROLL);
		customVerticalFieldManager2 = new CustomVerticalFieldManager((Display.getWidth()/2),Display.getHeight(), HorizontalFieldManager.USE_ALL_WIDTH|Field.FIELD_VCENTER);
		
		 bField =  Bitmap.getBitmapResource(MainClass.BG_MENU_MC);
		 
		
		
		
		
	}
	
	

	public void addFields(){
		
		
		add(new NullField());
		
		BitmapField bit= new BitmapField(bField);

		
		
		bit.setMargin(Display.getHeight()/2-bField.getHeight()/2, 0, 0, 0);
	
		int suma = Bitmap.getBitmapResource(MainClass.BTN_BUY).getHeight() +
		Bitmap.getBitmapResource(MainClass.BTN_ACCOUNT).getHeight()	+
		Bitmap.getBitmapResource(MainClass.BTN_PROMOTIONS).getHeight() +
		Bitmap.getBitmapResource(MainClass.BTN_CONSULT).getHeight()
		;
		
		version.setMargin(Display.getHeight()/2-suma/2, 0, 0, 0);
		customVerticalFieldManager2.add(bit);
		
		customVerticalFieldManager.add(version);
		customVerticalFieldManager.add(new LabelField(""));
		hfmTitle2.add(text);
		customVerticalFieldManager.add(hfmTitle2);
		customVerticalFieldManager.add(new LabelField(""));
		customVerticalFieldManager.add(btnDescargar);
		if(prioridad.equals("2")){
			customVerticalFieldManager.add(new LabelField(""));
			customVerticalFieldManager.add(btnIgnorar);	
		}
		
		customVerticalFieldManager.add(new LabelField(""));
		customVerticalFieldManager.add(note);
		
		
		hfmMain.add(customVerticalFieldManager2);
		hfmMain.add(customVerticalFieldManager);
				
		bgManager.add(hfmMain);
		
		add(bgManager);
	}
		
	}


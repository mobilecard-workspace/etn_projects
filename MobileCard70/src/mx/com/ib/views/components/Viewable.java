package mx.com.ib.views.components;

import org.json.me.JSONObject;

public interface Viewable {

	public void setData(int request, JSONObject jsObject);
	public void sendMessage(String message);
}

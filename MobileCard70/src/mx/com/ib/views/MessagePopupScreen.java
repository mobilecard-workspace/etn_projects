package mx.com.ib.views;

import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.RichTextField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class MessagePopupScreen extends PopupScreen implements FieldChangeListener{
	
	public ButtonField btnOk = null;
	public HorizontalFieldManager hfmButton = null;

	public MessagePopupScreen(String text, long style){
		
		super(new VerticalFieldManager(VERTICAL_SCROLL));
		
		hfmButton = new HorizontalFieldManager(Field.FIELD_HCENTER);
		
		btnOk = new ButtonField("Ok", ButtonField.CONSUME_CLICK);
		
		btnOk.setChangeListener(this);
		
		add(new RichTextField(text, style));
		
		hfmButton.add(btnOk);
		
		add(hfmButton);
		
	}
	
	public boolean keyChar(char key, int status, int time) {
	    //intercept the ESC key - exit the app on its receipt
	    boolean retval = false;
	    switch (key) {

	        case Characters.ESCAPE:
	            close();
	            break;
	        default:
	            retval = super.keyChar(key,status,time);
	    }
	    return retval;
	}

	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		
		if(field == btnOk){
			close();
		}
		
	}
	
	public void close() {
		// TODO Auto-generated method stub
		UiApplication.getUiApplication().popScreen(this);
	}
	
}

package mx.com.ib.views;

import mx.com.ib.app.MainClass;
import mx.com.ib.beans.GeneralBean;
import mx.com.ib.beans.ProductBean;
import mx.com.ib.controls.BannerBitmap;
import mx.com.ib.controls.BgManager;
import mx.com.ib.controls.CustomVerticalFieldManager;
import mx.com.ib.controls.ProductLabelField;
import mx.com.ib.threads.ThreadBuyNowImages;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.FlowFieldManager;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;


public class BuyScreen extends MainScreen{
	
	public BgManager bgManager = null;
	public LabelField products = null;
	public CustomVerticalFieldManager vfmFooter = null;
	public ProductBean[] productBean = null;
	public GeneralBean[] bankBeans = null;
	public ProductLabelField[] productLabelFields = null;
	public FlowFieldManager grid;

/*
	public BuyScreen(){
		initVariables();
		addFields();
	}
*/
	public BuyScreen(ProductBean[] productBeans){
		
		this.productBean = productBeans;
		initVariables();
		addFields();
	}
	
	public BuyScreen(GeneralBean[] productBeans){
		
		this.bankBeans = productBeans;
		initVariables();
		addFields();
		products.setText(MainClass.textTitle);		
	}
	
	public void initVariables(){
	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	    
	    TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	    transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
	    
		bgManager = new BgManager(MainClass.BG_GRAY);
		products = new LabelField("Cat�logo de Productos", Field.USE_ALL_WIDTH | LabelField.HCENTER);
		
		grid = new FlowFieldManager( Field.USE_ALL_WIDTH);		
		vfmFooter = new CustomVerticalFieldManager(Display.getWidth(),
				Display.getHeight(), 
				HorizontalFieldManager.FIELD_LEFT | VERTICAL_SCROLL);
	}
	
	
public void addFields(){

		setBanner(new BannerBitmap(MainClass.HD_TIENDA));

		vfmFooter.add(new LabelField(""));
		vfmFooter.add(products);
		vfmFooter.add(grid);
		
		if(this.productBean!=null){
			
			int length = this.productBean.length;
			
			productLabelFields = new ProductLabelField[length];		
			
			for(int i = 0; i < length; i++){
				
				productLabelFields[i] = new ProductLabelField(this.productBean[i]);
				grid.add(productLabelFields[i]);
			}
			
			//new ThreadBuyNowImages(productLabelFields, 0, productLabelFields.length).start();

		} else {
			
			int length = this.bankBeans.length;
			
			if (length > 0){
			
				productLabelFields = new ProductLabelField[length];
				
				for(int i = 0; i < length; i++){
					
					productLabelFields[i] = new ProductLabelField(this.bankBeans[i]);
					grid.add(productLabelFields[i]);
				}			

				//new ThreadBuyNowImages(productLabelFields, 0, productLabelFields.length).start();
			}
			

			//else if(productLabelFields.length == 1 && bankBeans.length == 1){
				/*
				productLabelFields[0] = new ProductLabelField(this.bankBeans[0]);
				ProductLabelField prod = productLabelFields[0];
				MainClass.splashScreen.start();
				MainClass.bitmap = prod.bitmap;
				MainClass.textTitleInternal = MainClass.textTitle + " - " + prod.generalBean.getDescription(); 
				System.out.println("solo hay un proveedor");
				MainClass.IDPROVEEDOR=prod.generalBean.getClave();
				new CatProductsThread(prod.generalBean.getClaveWS()).start();
			//	MainClass.splashScreen.remove();
			*/
				//}
			
		}
		
	
		
		
		bgManager.add(vfmFooter);
		setStatus(new BannerBitmap());
		add(bgManager);
		
		
		new ThreadBuyNowImages(this, productLabelFields, 0, productLabelFields.length).start();
	}

}

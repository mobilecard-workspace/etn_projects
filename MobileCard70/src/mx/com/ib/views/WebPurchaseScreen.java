/**
 * 
 */
package mx.com.ib.views;

import mx.com.ib.app.MainClass;
import mx.com.ib.controls.BannerBitmap;
import mx.com.ib.controls.BgManager;
import mx.com.ib.controls.CustomVerticalFieldManager;
import net.rim.device.api.browser.field2.BrowserField;
import net.rim.device.api.browser.field2.BrowserFieldConfig;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;

/**
 * @author ADDCEL14
 * SDK 5.0.0
 */
public class WebPurchaseScreen extends MainScreen {
	public BgManager bgManager = null;
	public CustomVerticalFieldManager customVerticalFieldManager = null;
	public BrowserField browserField = null;
	public String url;
	
	public WebPurchaseScreen(String url){
		this.url = url;
		initVariables();
		addFields();
		
	}
	
	public void initVariables(){
		TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	    
	    TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	    transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
	    
	    bgManager = new BgManager(MainClass.BG_GRAY);
	    customVerticalFieldManager = new CustomVerticalFieldManager(Display.getWidth(), USE_ALL_WIDTH| VERTICAL_SCROLL);
	    
	    setBrowserField();
	   
	}
	
	public void close(){
		UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
	}
	
	public void addFields(){
		setBanner(new BannerBitmap(MainClass.HD_COMPRAS));
		customVerticalFieldManager.add(browserField);
		bgManager.add(customVerticalFieldManager);
		add(bgManager);
	}
	
	public void setBrowserField(){
		BrowserFieldConfig config = new BrowserFieldConfig();
        config.setProperty(BrowserFieldConfig.ENABLE_COOKIES,Boolean.TRUE);
        config.setProperty(BrowserFieldConfig.JAVASCRIPT_ENABLED, Boolean.TRUE);
        config.setProperty(BrowserFieldConfig.NAVIGATION_MODE,BrowserFieldConfig.NAVIGATION_MODE_POINTER);
        browserField = new BrowserField(config); 
	    browserField.requestContent(url);
	}
}

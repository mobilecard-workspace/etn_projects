package mx.com.ib.views;

import java.util.Calendar;

import mx.com.ib.app.MainClass;
import mx.com.ib.beans.GeneralBean;
import mx.com.ib.controls.BannerBitmap;
import mx.com.ib.controls.BgManager;
import mx.com.ib.controls.BorderBasicEditField;
import mx.com.ib.controls.BorderPasswordField;
import mx.com.ib.controls.CustomVerticalFieldManager;
import mx.com.ib.controls.GenericImageButtonField;
import mx.com.ib.threads.GPSThread;
import mx.com.ib.threads.PurchaseThread;
import mx.com.ib.utils.Utils;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import crypto.Crypto;

public class ConfirmProductScreen extends MainScreen {

	public BgManager bgManager = null;
	public LabelField description = null;
	public LabelField description2, Message = null;
	public LabelField password = null, helpPass = null;
	public LabelField credit = null;
	public LabelField phone = null;
	public LabelField confirmPhone = null;
	public LabelField img1 = null;
	public LabelField vige = null;

	public BorderBasicEditField basicCredit = null;
	public BorderBasicEditField basicPhone = null;
	public BorderBasicEditField basicConfirmPhone = null;
	public BorderPasswordField basicPassword = null;

	public HorizontalFieldManager hfmMain = null;
	public HorizontalFieldManager hfmTitle = null;

	public HorizontalFieldManager hfmBtns, hfmChoices = null;
	public HorizontalFieldManager Cvv2HFM = null;

	public VerticalFieldManager vfmFooter = null;

	public CustomVerticalFieldManager customVerticalFieldManager,
			creditVerticalFieldManager, creditDateVerticalFieldManager = null;

	public GenericImageButtonField btnAccept = null;

	public GeneralBean generalBean = null;

	public String yearsCredit[] = null;
	public String monthsCredit[] = null;

	public ObjectChoiceField choicesYearsCredit = null;
	public ObjectChoiceField choicesMonthsCredit = null;

	public int monthCredit = 0;
	public int yearCredit = 0;

	public ConfirmProductScreen(GeneralBean generalBean) {

		this.generalBean = generalBean;

		initVariables();

		addFields();

	}

	public void initVariables() {

		TransitionContext transition = new TransitionContext(
				TransitionContext.TRANSITION_SLIDE);
		transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
		transition.setIntAttribute(TransitionContext.ATTR_DIRECTION,
				TransitionContext.DIRECTION_LEFT);
		transition.setIntAttribute(TransitionContext.ATTR_STYLE,
				TransitionContext.STYLE_PUSH);

		UiEngineInstance engine = Ui.getUiEngineInstance();
		engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH,
				transition);

		TransitionContext transition2 = new TransitionContext(
				TransitionContext.TRANSITION_SLIDE);
		transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
		transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION,
				TransitionContext.DIRECTION_RIGHT);
		transition2.setIntAttribute(TransitionContext.ATTR_STYLE,
				TransitionContext.STYLE_PUSH);

		engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP,
				transition2);
		engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);

		new GPSThread(1, 5, 30, 15, 7200).start();

		bgManager = new BgManager(MainClass.BG_GRAY);
		Cvv2HFM = new HorizontalFieldManager(Field.FIELD_LEFT);
		Message = new LabelField(
				"Todas tus recargas Telcel tendr�n una comisi�n de $"
						+ MainClass.COMISION_TAE_TELCEL
						+ "0 MX con IVA INCLUIDO.", Field.FIELD_RIGHT) {
			public void paint(Graphics graphics) {
				int g = graphics.getColor();
				graphics.setColor(MainClass.BG_ORANGE);
				super.paint(graphics);
			}

			protected void onFocus(int direction) {
				super.onFocus(direction);
				invalidate();
				//
			}

			protected void onUnfocus() {
				super.onUnfocus();
				invalidate();
			}
		};
		Message.setFont(MainClass.appFont1);
		hfmMain = new HorizontalFieldManager(VerticalFieldManager.FIELD_HCENTER
				| VERTICAL_SCROLL) {
			public int getPreferredWidth() {
				// TODO Auto-generated method stub
				return Display.getWidth();
			}

			protected void sublayout(int maxWidth, int maxHeight) {
				// TODO Auto-generated method stub
				super.sublayout(getPreferredWidth(), maxHeight);
			}
		};

		hfmTitle = new HorizontalFieldManager(Field.FIELD_RIGHT);
		hfmBtns = new HorizontalFieldManager(Field.FIELD_HCENTER);

		vfmFooter = new VerticalFieldManager(VERTICAL_SCROLL) {
			public int getPreferredWidth() {
				// TODO Auto-generated method stub
				return Display.getWidth();
			}

			protected void sublayout(int maxWidth, int maxHeight) {
				// TODO Auto-generated method stub
				super.sublayout(getPreferredWidth(), maxHeight);
			}
		};

		customVerticalFieldManager = new CustomVerticalFieldManager(
				Display.getWidth() - 20, Display.getHeight() - 30,
				HorizontalFieldManager.FIELD_LEFT | VERTICAL_SCROLL);
		creditVerticalFieldManager = new CustomVerticalFieldManager(
				(Display.getWidth() - 20) / 2,
				HorizontalFieldManager.FIELD_LEFT);
		creditDateVerticalFieldManager = new CustomVerticalFieldManager(
				(Display.getWidth() - 20) / 2,
				HorizontalFieldManager.FIELD_LEFT);
		hfmChoices = new HorizontalFieldManager(Field.FIELD_HCENTER);
		description2 = new LabelField("Descripci�n de la compra:",
				Field.FIELD_HCENTER);
		description = new LabelField(MainClass.textTitleConfirm);
		password = new LabelField("Digite su contrase�a de MobileCard:");
		credit = new LabelField("CVV2 Tarjeta:");
		phone = new LabelField("N�mero celular:");
		confirmPhone = new LabelField("Confirma el n�mero de celular:");
		vige = new LabelField("Vigencia de la Tarjeta:");
		description2.setFont(MainClass.appFont);

		basicPassword = new BorderPasswordField("", "", 12, Display.getWidth(),
				Field.FOCUSABLE);
		basicCredit = new BorderBasicEditField("", "", 4, Display.getWidth(),
				Field.FOCUSABLE | EditField.FILTER_NUMERIC);
		basicPhone = new BorderBasicEditField("", "", 10, Display.getWidth(),
				Field.FOCUSABLE | EditField.FILTER_NUMERIC);
		basicConfirmPhone = new BorderBasicEditField("", "", 10,
				Display.getWidth(), Field.FOCUSABLE | EditField.FILTER_NUMERIC);

		btnAccept = new GenericImageButtonField(MainClass.BTN_COMPRAR,
				MainClass.BTN_COMPRAR_OVER, "") {

			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub

				// UiApplication.getUiApplication().pushScreen(new
				// FolioScreen());
				checkFields();
				return true;
			}

		};

		yearsCredit = new String[13];
		Calendar c = Calendar.getInstance();

		yearsCredit = new String[13];
		for (int i = 0; i < 13; i++) {
			yearsCredit[i] = new String(Integer.toString(c.get(Calendar.YEAR)
					+ i));
		}

		monthsCredit = new String[] { "Enero", "Febrero", "Marzo", "Abril",
				"Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre",
				"Noviembre", "Diciembre" };
		choicesMonthsCredit = new ObjectChoiceField("Mes Vencimiento: ",
				monthsCredit) {
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				super.paint(graphics);
				invalidate();
			}

			protected void drawFocus(Graphics graphics, boolean on) {
				// TODO Auto-generated method stub
				super.drawFocus(graphics, on);
				invalidate();
			}

		};

		choicesYearsCredit = new ObjectChoiceField("A�o Vencimiento: ",
				yearsCredit) {
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				super.paint(graphics);
				invalidate();
			}

			protected void drawFocus(Graphics graphics, boolean on) {
				// TODO Auto-generated method stub
				super.drawFocus(graphics, on);
				invalidate();
			}

			protected void onFocus(int direction) {
				super.onFocus(direction);
				invalidate();
				//
			}

			protected void onUnfocus() {
				super.onUnfocus();
				invalidate();
			}

		};

		img1 = new LabelField("�qu� es esto?", Field.FOCUSABLE) {

			public void paint(Graphics g) {

				g.setColor(Color.ORANGE);
				super.paint(g);
			}

			protected boolean navigationClick(int status, int time) {

				// TODO Auto-generated method stub
				MainClass.splashScreen.start();
				MainClass.splashScreen.remove();
				UiApplication.getUiApplication().pushScreen(
						new ImagePopUpune("CVV2", Field.FOCUSABLE));
				return true;
			}

			protected void onFocus(int direction) {
				super.onFocus(direction);
				invalidate();
				//
			}

			protected void onUnfocus() {
				super.onUnfocus();
				invalidate();
			}

		};
		img1.setFont(Font.getDefault().derive(Font.UNDERLINED));

		helpPass = new LabelField("? ", Field.FOCUSABLE) {

			public void paint(Graphics g) {

				g.setColor(MainClass.BG_ORANGE);
				super.paint(g);
			}

			protected boolean navigationClick(int status, int time) {
				Dialog.inform("La contrase�a debe tener m�nimo 8 caracteres y m�ximo 12");
				// UiApplication.getUiApplication().pushScreen(new
				// RecUserScreen());
				return true;
			}

			protected void onFocus(int direction) {
				super.onFocus(direction);
				invalidate();
				//
			}

			protected void onUnfocus() {
				super.onUnfocus();
				invalidate();
			}

		};

	}

	public void addFields() {

		setBanner(new BannerBitmap(MainClass.HD_COMPRAS));

		customVerticalFieldManager.add(hfmTitle);

		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));

		customVerticalFieldManager.add(description2);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(description);
		// customVerticalFieldManager.add(description2);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		if (MainClass.IDPROVEEDOR.equals("3")) {

			customVerticalFieldManager.add(Message);
			customVerticalFieldManager.add(new LabelField("",
					Field.NON_FOCUSABLE));
		}
		customVerticalFieldManager.add(password);
		customVerticalFieldManager.add(basicPassword);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		Cvv2HFM.add(credit);
		Cvv2HFM.add(img1);
		customVerticalFieldManager.add(Cvv2HFM);
		customVerticalFieldManager.add(basicCredit);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(vige);

		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(new LabelField(""));
		customVerticalFieldManager.add(choicesMonthsCredit);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(choicesYearsCredit);
		hfmChoices.add(creditVerticalFieldManager);
		hfmChoices.add(creditDateVerticalFieldManager);
		customVerticalFieldManager.add(hfmChoices);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(phone);
		customVerticalFieldManager.add(basicPhone);
		customVerticalFieldManager.add(new LabelField(""));

		customVerticalFieldManager.add(confirmPhone);
		customVerticalFieldManager.add(basicConfirmPhone);
		customVerticalFieldManager.add(new LabelField(""));

		hfmBtns.add(btnAccept);

		customVerticalFieldManager.add(hfmBtns);
		customVerticalFieldManager.add(new LabelField(""));

		// hfmMain.add(customVerticalFieldManager);

		// vfmFooter.add(new NullField());

		// vfmFooter.add(hfmMain);

		// vfmFooter.add(new FooterLabelField());

		bgManager.add(customVerticalFieldManager);

		add(bgManager);

	}

	public void checkFields() {

		String x =MainClass.X;
		String y =MainClass.Y;
		
		int _yearCredit = choicesYearsCredit.getSelectedIndex();
		int _monthCredit = choicesMonthsCredit.getSelectedIndex() + 1;

		String stringMonth = "";
		if (_monthCredit < 10) {
			stringMonth = "0" + _monthCredit;
		} else {
			stringMonth = String.valueOf(_monthCredit);
		}

		if (basicPassword.getText().trim().equals("")) {
			Dialog.alert("Debe de ingresar su contrase�a.");
		} else if (basicCredit.getText().trim().equals("")) {
			Dialog.alert("Debe de ingresar su n�mero CVV.");
		} else if (basicCredit.getText().trim().length() < 3) {
			Dialog.alert("El n�mero CVV debe contener al menos 3 d�gitos.");
		} else if (basicPhone.getText().trim().equals("")) {
			Dialog.alert("Debe de ingresar el n�mero telef�nico al que se le realizar� el abono.");
		} else if (!Utils.isNumeric(basicPhone.getText().trim())) {
			Dialog.alert("No se aceptan letras ni caracteres especiales en el n�mero telef�nico.");
		} else if (basicPhone.getText().trim().length() < 10) {
			Dialog.alert("El n�mero telef�nico debe contener al menos 10 d�gitos.");
		} else if (basicConfirmPhone.getText().trim().equals("")) {
			Dialog.alert("Debe de ingresar el n�mero telef�nico al que se le realizar� el abono.");
		} else if (!Utils.isNumeric(basicConfirmPhone.getText().trim())) {
			Dialog.alert("No se aceptan letras ni caracteres especiales en el n�mero telef�nico.");
		} else if (basicConfirmPhone.getText().trim().length() < 10) {
			Dialog.alert("El n�mero telef�nico debe contener al menos 10 d�gitos.");
		} else if (!basicConfirmPhone.getText().trim()
				.equals(basicPhone.getText().trim())) {
			Dialog.alert("Los n�meros no concuerdan, favor de verificar");
		} else if (!Utils.validDate("" + _monthCredit, "" + _yearCredit)) {
			Dialog.alert("La vigencia es incorrecta.");
		} else if (basicPassword.getText().trim().length() < 8) {
			Dialog.alert("Tu contrase�a debe ser de al menos 8 caract�res.");
		}
		// else if(basicConfirmPhone.getText().trim().equals("")){
		//
		// Dialog.alert(
		// "Debe de confirmar el n�mero telef�nico al que se le realizar� el abono."
		// );
		//
		// }else if(!Utils.isNumeric(basicConfirmPhone.getText().trim())){
		//
		// Dialog.alert(
		// "No se aceptan letras ni caracteres especiales en el n�mero telef�nico de confirmacion."
		// );
		//
		// }else if( basicConfirmPhone.getText().trim().length() < 10 ){
		//
		// Dialog.alert(
		// "El n�mero telef�nico de confirmacion debe contener al menos 10 d�gitos."
		// );
		//
		// }else if(
		// !basicPhone.getText().trim().equals(basicConfirmPhone.getText().trim())
		// ){
		//
		// Dialog.alert(
		// "El n�mero telef�nico no coincide con el numero de confirmacion." );
		//
		// }
		else {
			MainClass.splashScreen.start();
			new PurchaseThread(getJSON(), basicPassword.getText().trim()).run();
		}

	}

	public String getJSON() {

		String json = "";

		try {

			yearCredit = choicesYearsCredit.getSelectedIndex();
			monthCredit = choicesMonthsCredit.getSelectedIndex() + 1;

			// String numMonth = choicesMonthsCredit.getSelectedIndex()+1;

			String stringMonth = "";

			if (monthCredit < 10) {

				stringMonth = "0" + monthCredit;

			} else {

				stringMonth = String.valueOf(monthCredit);

			}

			String vigency = yearsCredit[choicesYearsCredit.getSelectedIndex()];

			vigency = vigency.substring((vigency.length() - 2),
					vigency.length());

			String js = "{\"login\":\"" + MainClass.userBean.getLogin()
					+ "\",\"password\":" + "\"" + basicPassword.getText().trim() 
					+ "\",\"cvv2\":\"" + basicCredit.getText().trim()
					+ "\",\"telefono\":\"" + basicPhone.getText().trim() 
					+ "\",\"imei\":\"" + Utils.getImei() 
					+ "\",\"vigencia\":\"" + stringMonth + vigency 
					+ "\",\"cx\":\"" + MainClass.X 
					+ "\",\"tipo\":\"" + MainClass.TIPO 
					+ "\",\"modelo\":\"" + MainClass.MODELO
					+ "\",\"software\":\"" + MainClass.SOFTWARE
					+ "\",\"key\":\"" + Utils.getImei() 
					+ "\",\"cy\":\"" + MainClass.Y 
					+ "\",\"producto\":\"" + this.generalBean.getClave() + "\"}";

			String jsonpass = Crypto.aesEncrypt(
					Utils.parsePass(basicPassword.getText().trim()), js);
			json = Utils.mergeStr(jsonpass, basicPassword.getText().trim());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return json;
		}
		System.out.println(json.toString());
		return json;

	}

	public void close() {

		UiApplication.getUiApplication().popScreen(
				UiApplication.getUiApplication().getActiveScreen());

	}

	protected boolean onSavePrompt() {
		return true;
	}

}

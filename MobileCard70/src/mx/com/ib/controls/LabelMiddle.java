package mx.com.ib.controls;

import mx.com.ib.app.MainClass;
import mx.com.ib.utils.Utils;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.component.LabelField;

public class LabelMiddle extends LabelField{
	
	public String text = "";
	
	public LabelMiddle(String text, long style){
		
		super(text, style);
		
		this.text = text;
		
	}
	
	protected void paint(Graphics graphics) {
		// TODO Auto-generated method stub
		setFont(MainClass.alphaSansFamily.getFont(Font.PLAIN, 10, Ui.UNITS_pt));
		Utils.paintTextMiddle(graphics, text, (getWidth()-(getFont().getAdvance(this.text)))/2, 10, 25);
		super.paint(graphics);
	}

	protected void layout(int width, int height) 
	{

		this.setExtent(getPreferredWidth(), getPreferredHeight());		
		
	}
	
	public int getPreferredWidth() {
		// TODO Auto-generated method stub
		return Display.getWidth();
	}
	
	public int getPreferredHeight() {
		// TODO Auto-generated method stub
		return 200;
	}

}

package mx.com.ib.controls;

import mx.com.ib.beans.ConsultBean;
import mx.com.ib.utils.Utils;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.LabelField;

public class ConsultProductLabelField extends LabelField{
	

	public ConsultBean consultBean = null; 
	
	public int x = (Display.getWidth()-50)/3;
	public int color = Color.BLACK;
	public int colorField = Color.ORANGE;
	public long style = 0;
	
	public Font font = null;
	public int xFont = 160;
	
	public ConsultProductLabelField(ConsultBean consultBean, int color, int colorField, Font font,long style){
		
		super(consultBean.getProduct(), style);
		
		this.consultBean = consultBean;
		this.color = color;
		this.colorField = colorField;
		this.font = font;
		this.style = style;
		
	}
	
	protected void paint(Graphics graphics) {
		// TODO Auto-generated method stub
		graphics.setColor(colorField);
		graphics.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
		
		graphics.setColor(this.color);
		graphics.setFont(font);
		
		if(Display.getWidth() < 320){
			xFont = 140; 
		}
		
		Utils.paintText(graphics, ""+this.consultBean.getConcept(), 5, 5, 55);
		Utils.paintText(graphics, "Fecha y Hora: "+this.consultBean.getDate(), 5, 20, 50);
		Utils.paintText(graphics, "Cargo: $  "+this.consultBean.getAmount(), 5, 35, 50);
		
		invalidate();
		
	}
	
	protected void drawFocus(Graphics graphics, boolean on) {
		// TODO Auto-generated method stub
		graphics.setColor(colorField);
		
		graphics.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
		graphics.setGlobalAlpha(150);
		graphics.setColor(this.color);
		graphics.setFont(font);
		
		if(Display.getWidth() < 320){
			xFont = 140; 
		}
		
		Utils.paintText(graphics, ""+this.consultBean.getConcept(), 5, 5, 55);
		Utils.paintText(graphics, "Fecha y Hora: "+this.consultBean.getDate(), 5, 20, 50);
		Utils.paintText(graphics, "Cargo: $ "+this.consultBean.getAmount(), 5, 35, 50);
		
		invalidate();
		
	}
	
	protected void onUnfocus() {
		// TODO Auto-generated method stub
		super.onUnfocus();
	}
	
	protected void layout(int width, int height) 
	{

		this.setExtent(getPreferredWidth(), getPreferredHeight());		
		
	}
	
	public int getPreferredWidth() {
		// TODO Auto-generated method stub
		return Display.getWidth();
	}
	
	public int getPreferredHeight() {
		// TODO Auto-generated method stub
		return 55;
	}

}

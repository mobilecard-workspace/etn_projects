package mx.com.ib.controls;

import mx.com.ib.utils.Utils;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class BgManager extends VerticalFieldManager {
	private Bitmap backgroundBitmap;
	private int bgColor;
	
	public BgManager(String fondo) {
		
		super(VerticalFieldManager.USE_ALL_WIDTH | 
    			VerticalFieldManager.USE_ALL_HEIGHT |
    			VerticalFieldManager.NO_VERTICAL_SCROLL |
    			VerticalFieldManager.NO_VERTICAL_SCROLLBAR|
    			VerticalFieldManager.FIELD_HCENTER);		
		if(Display.getWidth()<=240){
			if(Display.getHeight()==260){
				backgroundBitmap = Utils.resizeBitmap(Bitmap.getBitmapResource(fondo), Display.getWidth(), Display.getHeight());
			}else if(Display.getHeight()<260){
				backgroundBitmap = Utils.resizeBitmap(Bitmap.getBitmapResource(fondo), Display.getWidth(), Display.getHeight());
			}			
		}else if(Display.getWidth()==320){
				backgroundBitmap = Bitmap.getBitmapResource(fondo);
		}else if(Display.getWidth()==360){
			if(Display.getHeight()==480){
				backgroundBitmap = Bitmap.getBitmapResource(fondo);
			}else{
				backgroundBitmap = Utils.resizeBitmap(Bitmap.getBitmapResource(fondo), Display.getWidth(), Display.getHeight());
			}
		}else if(Display.getWidth()==480){
			if(Display.getHeight()==360){
				backgroundBitmap = Utils.resizeBitmap(Bitmap.getBitmapResource(fondo), Display.getWidth(), Display.getHeight());
			}else{
				backgroundBitmap = Utils.resizeBitmap(Bitmap.getBitmapResource(fondo), Display.getWidth(), Display.getHeight());
			}
		}	
		
		bgColor = 0x414042;
	}
	
	public BgManager(int color) {
		
		super(VerticalFieldManager.USE_ALL_WIDTH | 
    			VerticalFieldManager.USE_ALL_HEIGHT |
    			VerticalFieldManager.NO_VERTICAL_SCROLL |
    			VerticalFieldManager.NO_VERTICAL_SCROLLBAR|
    			VerticalFieldManager.FIELD_HCENTER);		
		bgColor = color;		
	}

	public void paint(Graphics graphics) {
		// Draw the background image and then call paint.

		graphics.setColor(bgColor);
		graphics.fillRect(0, 0, Display.getWidth(), Display.getHeight());
		graphics.setColor(Color.WHITE);
		
		if(backgroundBitmap != null){
			 graphics.drawBitmap(0, 0, Display.getWidth(), Display.getHeight(), backgroundBitmap, 0, 0);
		}
		
		super.paint(graphics);
	}

	protected void sublayout(int width, int height) {
		super.sublayout(width, Display.getHeight());
		setExtent(width, Display.getHeight());
	}

	public int getPreferredHeight() {
		return Display.getHeight();
	}

	public int getPreferredWidth() {
		return Display.getWidth();
	}
}
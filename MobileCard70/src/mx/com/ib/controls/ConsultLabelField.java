package mx.com.ib.controls;


import mx.com.ib.beans.ConsultBean;
import mx.com.ib.utils.Utils;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.LabelField;

public class ConsultLabelField extends LabelField{
	
	public ConsultBean consultBean = null; 
	
	public int x = (Display.getWidth()-50)/3;
	public int color = Color.BLACK;
	public int colorField = Color.BLACK;
	public long style = 0;
	
	public Font font = null;
	
	public ConsultLabelField(ConsultBean consultBean, int color, int colorField, Font font,long style){
		
		super(consultBean.getProduct(), style);
		
		this.consultBean = consultBean;
		this.color = color;
		this.colorField = colorField;
		this.font = font;
		this.style = style;
		
	}
	
	protected void paint(Graphics graphics) {
		// TODO Auto-generated method stub
		graphics.setColor(colorField);
		graphics.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
		
		graphics.setColor(this.color);
		graphics.setFont(font);
		Utils.paintText(graphics, this.consultBean.getDate(), (5), 5, 6);
		Utils.paintText(graphics, this.consultBean.getConcept(), (15+x), 5, 50);
		if(this.style == Field.FOCUSABLE){
			Utils.paintText(graphics, "$ " +this.consultBean.getAmount(), (35+(2*x)), 5, 20);
		}else{
			Utils.paintText(graphics, this.consultBean.getAmount(), (25+(2*x)), 5, 20);
		}
		
		
		invalidate();
		
	}
	
	protected void drawFocus(Graphics graphics, boolean on) {
		// TODO Auto-generated method stub
//		Utils.paintText(graphics, this.consultBean.getDate(), 0, 5, 50);
		
		graphics.setColor(colorField);
		
		graphics.fillRect(0, 0, getPreferredWidth(), getPreferredHeight());
		graphics.setGlobalAlpha(150);
		graphics.setColor(this.color);
		graphics.setFont(font);
		Utils.paintText(graphics, this.consultBean.getDate(), (5), 5, 6);
		Utils.paintText(graphics, this.consultBean.getConcept(), (15+x), 5, 50);
//		Utils.paintText(graphics, "$ " +this.consultBean.getAmount(), (25+(2*x)), 5, 20);
		if(this.style == Field.FOCUSABLE){
			Utils.paintText(graphics, "$ " +this.consultBean.getAmount(), (35+(2*x)), 5, 20);
		}else{
			Utils.paintText(graphics, this.consultBean.getAmount(), (25+(2*x)), 5, 20);
		}
		
		invalidate();
		
	}
	
	protected void onUnfocus() {
		// TODO Auto-generated method stub
		super.onUnfocus();
	}
	
	protected void layout(int width, int height) 
	{

		this.setExtent(getPreferredWidth(), getPreferredHeight());		
		
	}
	
	public int getPreferredWidth() {
		// TODO Auto-generated method stub
		return Display.getWidth()-20;
	}
	
	public int getPreferredHeight() {
		// TODO Auto-generated method stub
		return 50;
	}

}

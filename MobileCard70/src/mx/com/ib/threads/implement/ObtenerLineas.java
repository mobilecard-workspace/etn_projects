package mx.com.ib.threads.implement;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import mx.com.ib.app.MainClass;
import mx.com.ib.threads.implement.util.HttpListener;
import mx.com.ib.views.components.Viewable;

public class ObtenerLineas extends HttpListener {

	public ObtenerLineas(String post, Viewable viewable) {
		super(post, MainClass.URL_GOB_ESTADO_LINEA, viewable);
	}

	public void receiveHttpResponse(int appCode, byte[] response) {

		JSONObject jsObject = null;
		String json = new String(response);

		try {
			
			jsObject = new JSONObject(json);
			sendData(jsObject);
		} catch (JSONException e) {
			e.printStackTrace();
			sendMessageError("Error en lectura de JSON");
		}
	}
}



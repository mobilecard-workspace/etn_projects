package mx.com.ib.threads;

import java.io.UnsupportedEncodingException;
import java.util.Vector;

import crypto.Crypto;

import net.rim.device.api.system.Application;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;

import mx.com.ib.app.MainClass;
import mx.com.ib.connection.Communications;
import mx.com.ib.connection.HttpListener;
import mx.com.ib.utils.Utils;
import mx.com.ib.views.CardPopupScreen;
import mx.com.ib.views.MessagePopupScreen;

public class CardsThread extends Thread implements HttpListener {


	public String url = MainClass.URL_GET_TAGS;
	public boolean isLogin = false;
	public int tipoTarjeta = 0;

	public CardsThread(boolean isLogin, int tipoTarjeta){
		this.isLogin = isLogin;
		this.tipoTarjeta = tipoTarjeta;
	}

	public void run(){
		connect();
	}
	
	public void connect(){
		Application.getApplication();
		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.start();
		}
		
		if(MainClass.idealConnection != null){
			if(this.url != null){
				try{
					Communications.sendHttpRequest(this,this.url);
				}catch(Throwable t){
					getMessageError();
				}
			}
		}
		else{
			Utils.checkConnectionType();
			this.connect();
		}
	}

	public void receiveHttpResponse(int appCode, byte[] response) {
		// TODO Auto-generated method stub
		String sTemp = null;
		StringBuffer sb = new StringBuffer();
		
		try{
			sb.append(new String(response,0, response.length, "UTF-8"));
			sTemp = new String(sb.toString());
			sTemp = Crypto.aesDecrypt(MainClass.key, sTemp);
			System.out.println("STEMP TAGS: "+sTemp);
			
			UiApplication.getUiApplication();
			synchronized (Application.getEventLock()) {
				MainClass.splashScreen.remove();
				UiApplication.getUiApplication().pushScreen(new CardPopupScreen(tipoTarjeta, Field.NON_FOCUSABLE, isLogin));
			}
		}catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			getMessageError();
		}
		
	}

	public void handleHttpError(int errorCode, String error) {
		// TODO Auto-generated method stub
		getMessageError();
		
	}

	public void receiveEstatus(String msg) {
		// TODO Auto-generated method stub
		
	}

	public void receiveHeaders(Vector _headers) {
		// TODO Auto-generated method stub
		
	}

	public boolean isDestroyed() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public void getMessageError(){

		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.remove();
			UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Informaci�n no disponible. Favor de intentar m�s tarde", Field.NON_FOCUSABLE));
		}
		
	}
	
}

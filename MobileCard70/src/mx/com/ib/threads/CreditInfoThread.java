package mx.com.ib.threads;

import java.util.Vector;

import crypto.Crypto;

import net.rim.device.api.system.Application;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;

import mx.com.ib.app.MainClass;
import mx.com.ib.beans.GeneralBean;
import mx.com.ib.connection.Communications;
import mx.com.ib.connection.HttpListener;
import mx.com.ib.parser.JSONParser;
import mx.com.ib.utils.Utils;
import mx.com.ib.views.MessagePopupScreen;
import mx.com.ib.views.NewAccountScreen;

public class CreditInfoThread extends Thread implements HttpListener {

	public String url = "";
	public String id = "";

	public CreditInfoThread(String id) {

		this.id = id;

		if (this.id.equals("bancos")) {

			this.url = MainClass.URL_GET_BANKS;

		} else if (this.id.equals("tarjetas")) {

			this.url = MainClass.URL_GET_CARDTYPES;

		} else if (this.id.equals("proveedores")) {

			this.url = MainClass.URL_GET_PROVIDERS;

		} else if (this.id.equals("estados")) {

			this.url = MainClass.URL_GET_ESTADOS;

		}

	}

	public void run() {
		// TODO Auto-generated method stub
		connect();
	}

	public void connect() {

		if (MainClass.idealConnection != null) {

			if (this.url != null) {
				try {
					// Communications.sendHttpPost(this,this.url,this.post);
					Communications.sendHttpRequest(this, this.url);
					// System.out.println("========================");
					// System.out.println("URL: "+this.url);
					// //System.out.println("JSON: "+this.post
					// );
					// System.out.println("========================");
				} catch (Throwable t) {
					// System.out.println("========================");
					// System.out.println("Excepcion en GETJSON: "+t);
					// System.out.println("========================");
				}
			}
		} else {
			// System.out.println("ELSE: ");
			Utils.checkConnectionType();
			this.connect();
		}

	}

	public void handleHttpError(int errorCode, String error) {
		// TODO Auto-generated method stub

		// System.out.println("=======");
		// System.out.println("CONEXION");
		// System.out.println("=======");
		getMessageError();
	}

	public boolean isDestroyed() {
		// TODO Auto-generated method stub
		return false;
	}

	public void receiveEstatus(String msg) {
		// TODO Auto-generated method stub

	}

	public void receiveHeaders(Vector _headers) {
		// TODO Auto-generated method stub

	}

	public void receiveHttpResponse(int appCode, byte[] response) {
		// TODO Auto-generated method stub

		String sTemp = null;
		StringBuffer sb = new StringBuffer();

		try {

			sb.append(new String(response, 0, response.length, "UTF-8"));
			sTemp = new String(sb.toString());

			System.out.println("STEMP: " + sTemp);
			GeneralBean[] gralBeans = null;
			sTemp = Crypto.aesDecrypt(MainClass.key, sTemp);
			System.out.println("STEMP: " + sTemp);
			// System.out.println("======================");
			// System.out.println(sTemp);
			// System.out.println("======================");

			gralBeans = MainClass.jsParser.getInfoCredits(this.id, sTemp);

			// if(this.id.equals("proveedores")){

			if (gralBeans != null) {

				if (gralBeans.length > 0) {

					if (this.id.equals("proveedores")) {

						MainClass.providersBeans = gralBeans;
						new CreditInfoThread("bancos").start();

					} else if (this.id.equals("bancos")) {

						MainClass.bankBeans = gralBeans;

						new CreditInfoThread("tarjetas").start();

					} else if (this.id.equals("tarjetas")) {

						MainClass.creditBeans = gralBeans;

						new CreditInfoThread("estados").start();

					} else if (this.id.equals("estados")) {

						MainClass.EdoBeans = gralBeans;

						// System.out.println("SYNC");

						UiApplication.getUiApplication();

						synchronized (Application.getEventLock()) {
							// System.out.println("SPLASH");
							MainClass.splashScreen.remove();

							if (MainClass.userBean == null) {
								// System.out.println("IF");

								UiApplication.getUiApplication().pushScreen(
										new NewAccountScreen());

							} else {

								// ERRORRRRRR....!!!!

								UiApplication.getUiApplication().pushScreen(
										new NewAccountScreen(true));

							}

						}

					}

				} else {

					// System.out.println("< 0");

					getMessageError();

				}

			} else {

				getMessageError();

			}

		} catch (Exception e) {

			getMessageError();

		}

	}

	public void getMessageError() {

		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.remove();
			UiApplication
					.getUiApplication()
					.pushScreen(
							new MessagePopupScreen(
									"Informaci�n no disponible. Favor de intentar m�s tarde",
									Field.NON_FOCUSABLE));
		}

	}

}

package mx.com.ib.threads;

import java.io.UnsupportedEncodingException;
import java.util.Vector;

import mx.com.ib.app.MainClass;
import mx.com.ib.beans.GeneralBean;
import mx.com.ib.connection.Communications;
import mx.com.ib.connection.HttpListener;
import mx.com.ib.etn.view.roundflight.ViewRoundFligth;
import mx.com.ib.utils.Utils;
import mx.com.ib.views.ConfirmProductScreen;
import mx.com.ib.views.MessagePopupScreen;
import mx.com.ib.views.VitamedicaBuyScreen;
import net.rim.device.api.system.Application;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;
import crypto.Crypto;

public class ComisionThread extends Thread implements HttpListener {

	public String url = MainClass.URL_GET_COMISION;
	public String post = "";
	public int t = 0;
	public GeneralBean generalBean = null;

	public ComisionThread(String json, int tipo, GeneralBean generalBean) {
		this.post = "json=" + json;
		this.generalBean = generalBean;
		this.t = tipo;
	}

	public void run() {
		// TODO Auto-generated method stub
		connect();
	}

	public void connect() {

		if (MainClass.idealConnection != null) {

			if (this.url != null) {
				
				Communications.sendHttpPost(this, this.url, this.post);				
			}
		} else {
			Utils.checkConnectionType();
			this.connect();
		}

	}

	public void handleHttpError(int errorCode, String error) {
		// TODO Auto-generated method stub
		getMessageError();
	}

	public boolean isDestroyed() {
		// TODO Auto-generated method stub
		return false;
	}

	public void receiveEstatus(String msg) {
		// TODO Auto-generated method stub

	}

	public void receiveHeaders(Vector _headers) {
		// TODO Auto-generated method stub

	}
	
	private boolean isBasicFlight;
	
	public boolean isBasicFlight() {
		return isBasicFlight;
	}

	public void setBasicFlight(boolean isBasicFlight) {
		this.isBasicFlight = isBasicFlight;
	}

	public void receiveHttpResponse(int appCode, byte[] response) {
		// TODO Auto-generated method stub

		String sTemp = null;
		StringBuffer sb = new StringBuffer();

		try {

			sb.append(new String(response, 0, response.length, "UTF-8"));
			sTemp = new String(sb.toString());
			System.out.println(sTemp);
			sTemp = Crypto.aesDecrypt(MainClass.key, sTemp);

			//UiApplication.getUiApplication();
			synchronized (Application.getEventLock()) {
				MainClass.splashScreen.remove();
				if (t == 1) {
					MainClass.COMISION_IAVE = sTemp;
					System.out.println("COMISION_IAVE " + "0"
							+ MainClass.COMISION_IAVE);
					new GetTagsIaveThread("1", this.generalBean).start();
				}
				if (t == 2) {
					MainClass.COMISION_OHL = sTemp;
					System.out.println("COMISION_OHL " + "0"
							+ MainClass.COMISION_OHL);
					new GetTagsOhlThread("2", this.generalBean).start();
				}
				if (t == 3) {
					MainClass.COMISION_VIAPASS = sTemp;
					System.out.println("COMISION_VIAPASS " + "0"
							+ MainClass.COMISION_VIAPASS);
					new GetTagsViaPass("3", this.generalBean).start();
				}
				if (t == 4) {
					MainClass.COMISION_IAVE_VIP = sTemp;
					System.out.println("COMISION_IAVE_VIP " + "0"
							+ MainClass.COMISION_IAVE_VIP);
					new GetTagsIaveVipThread("4", this.generalBean).start();
				}
				if (t == 5) {
					MainClass.COMISION_TAE_TELCEL = sTemp;
					System.out.println("COMISION_TAE_TELCEL " + "0"
							+ MainClass.COMISION_TAE_TELCEL);
					UiApplication.getUiApplication().pushScreen(
							new ConfirmProductScreen(this.generalBean));
					// new GetTagsIaveVipThread("4",this.generalBean).start();
				}
				if (t == 6) {
					MainClass.COMISION_VITAMEDICA = sTemp;
					System.out.println("COMISION_VITAMEDICA " + "0"
							+ MainClass.COMISION_VITAMEDICA);
					UiApplication.getUiApplication().pushScreen(
							new VitamedicaBuyScreen(this.generalBean));
				}
				if (t == 12) {
					MainClass.COMISION_ETN = sTemp;
					System.out.println("COMISION_ETN " + "0"
							+ MainClass.COMISION_ETN);
					UiApplication.getUiApplication().pushScreen(new ViewRoundFligth(isBasicFlight));
				}
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			getMessageError();
		} catch (Exception e) {
			e.printStackTrace();
			//getMessageError();
		}
	}



	public void getMessageError() {

		synchronized (Application.getEventLock()) {

			UiApplication
					.getUiApplication()
					.pushScreen(
							new MessagePopupScreen(
									"Informaci�n no disponible. Favor de intentar m�s tarde",
									Field.NON_FOCUSABLE));
		}
	}
}
package mx.com.ib.threads;

import java.io.UnsupportedEncodingException;
import java.util.Vector;

import net.rim.device.api.system.Application;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.SeparatorField;
import net.rim.device.api.ui.container.VerticalFieldManager;

import crypto.Crypto;

import mx.com.ib.app.MainClass;
import mx.com.ib.beans.ConsultBean;
import mx.com.ib.connection.Communications;
import mx.com.ib.connection.HttpListener;
import mx.com.ib.controls.ConsultLabelField;
import mx.com.ib.controls.ConsultProductLabelField;
import mx.com.ib.utils.Utils;
import mx.com.ib.views.MessagePopupScreen;

public class ConsultThread extends Thread implements HttpListener{
	
	public String url = MainClass.URL_GET_CONSULTS;
	public String post = "";
	public VerticalFieldManager vfm = null;
	public int font = 5;
	
	public ConsultThread(String json, VerticalFieldManager vfm){
		
		this.post = json;
		this.vfm = vfm;
		
		
	}
	
	public void run() {
		// TODO Auto-generated method stub
		connect();
	}
	
	public void connect(){		
		
		if(MainClass.idealConnection != null)
		{
			
			if(this.url != null)
			{				
				try{				    	   
//					Communications.sendHttpPost(this,this.url,this.post);
					Communications.sendHttpPost(this, this.url, this.post);
					//System.out.println("========================");
					   //System.out.println("URL: "+this.url);
//					   //System.out.println("JSON: "+this.post
//							   );
					   //System.out.println("========================");
		       }catch(Throwable t){
		    	   //System.out.println("========================");
				   //System.out.println("Excepcion en GETJSON: "+t);
				   //System.out.println("========================");
		       }
			}
		}
		else
		{		
			//System.out.println("ELSE: ");
			Utils.checkConnectionType();
			this.connect();
		}
		
	}	

	public void handleHttpError(int errorCode, String error) {
		// TODO Auto-generated method stub
		
	}

	public boolean isDestroyed() {
		// TODO Auto-generated method stub
		return false;
	}

	public void receiveEstatus(String msg) {
		// TODO Auto-generated method stub
		
	}

	public void receiveHeaders(Vector _headers) {
		// TODO Auto-generated method stub
		
	}

	public void receiveHttpResponse(int appCode, byte[] response) {
		// TODO Auto-generated method stub
		
		String sTemp = null;
		StringBuffer sb = new StringBuffer();
		
		try {
			
			sb.append(new String(response,0,response.length,"UTF-8"));
			sTemp = new String (sb.toString());
			sTemp = Crypto.aesDecrypt(Utils.parsePass(MainClass.p), sTemp);
			//System.out.println("STEMP: "+sTemp);
			
			ConsultBean[] consultBeans = MainClass.jsParser.getConsults(sTemp);
			
			if(consultBeans == null || consultBeans.length == 0){
				synchronized (Application.getEventLock()) {
					MainClass.splashScreen.remove();
					this.vfm.add(new SeparatorField());
					this.vfm.add(
							new LabelField("No existen compras en el historial", 
									Field.NON_FOCUSABLE|Field.FIELD_HCENTER)
					);
					this.vfm.add(new SeparatorField());
				}
			}else{
				synchronized (Application.getEventLock()) {
					MainClass.splashScreen.remove();
					this.vfm.add(new SeparatorField());
					
					for(int i = 0; i < consultBeans.length; i++){
						
						this.vfm.add(new ConsultProductLabelField(consultBeans[i], Color.WHITE, Color.BLACK, MainClass.alphaSansFamily.getFont(Font.PLAIN, font, Ui.UNITS_pt),  Field.FOCUSABLE));
						this.vfm.add(new SeparatorField());
						
					}
				}				
			}
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			getMessageError();
		}
		
		
	}
	
public void getMessageError(){
		
		//System.out.println("=======");
		//System.out.println("CREDIT INFO THREAD");
		//System.out.println("=======");
		
		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.remove();
			UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Informaci�n no disponible. Favor de intentar m�s tarde", Field.NON_FOCUSABLE));
		}
		
	}

}

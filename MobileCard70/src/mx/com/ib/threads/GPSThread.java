package mx.com.ib.threads;

import mx.com.ib.app.MainClass;
import net.rim.device.api.gps.BlackBerryLocation;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import rimx.location.simplelocation.SimpleLocationListener;
import rimx.location.simplelocation.SimpleLocationProvider;

public class GPSThread extends Thread implements SimpleLocationListener {
	private SimpleLocationProvider simpleProvider=null;
	private BlackBerryLocation location;
	private int RetryFactor,trackingInterval,  GPSTimeout, GEOTimeout, delay;
	private String[] modeStrings;
	private int fixCount=0, gpsFixCount=0, geoFixCount=0;
	
	public GPSThread(int RetryFactor,int trackingInterval, int GPSTimeout, int GEOTimeout, int delay){
	this.RetryFactor=RetryFactor;
	this.trackingInterval=trackingInterval;
	this.GPSTimeout=GPSTimeout;
	this.GEOTimeout=GEOTimeout;
	this.delay=delay;
	/*#ifdef BlackBerrySDK6.0.0
	modeStrings = new String[] {"OPTIMAL", "GPS", "GEOLOCATION", "GEOLOCATION CELL", "GEOLOCATION WLAN"};
	#endif*/
	//#ifdef BlackBerrySDK5.0.0
	modeStrings = new String[] {"OPTIMAL", "GPS", "GEOLOCATION"};
	//#endif
	}
	public void run() {
		// TODO Auto-generated method stub
		setGPSValues();
	}
	public void setGPSValues(){
		
		if(simpleProvider==null){
			try{
				for(int x=0; x<modeStrings.length;x++){
					System.out.println("MODE: "+getMode(x));
					simpleProvider = new SimpleLocationProvider(getMode(x));
					if(simpleProvider!=null){
						break;
					}
				}
				
			} catch(final Exception le){
				UiApplication.getUiApplication().invokeLater(new Runnable(){
					public void run(){
					//	Dialog.alert(le.getMessage());
					}
				});			
			} 
			
			if(simpleProvider!=null){
				simpleProvider.setGeolocationTimeout(GEOTimeout);
				simpleProvider.setGPSTimeout(GPSTimeout);
				simpleProvider.setRetryFactor(RetryFactor);
				simpleProvider.setTrackignInterval(trackingInterval);
				simpleProvider.setMaxRetryDelay(delay);
			}
		}
		try{
		simpleProvider.addSimpleLocationListener(this,trackingInterval);
		new Thread(){
			public void run(){					
				location = simpleProvider.getLocation(30);
				
				if(location!=null && location.isValid()){
					System.out.println(location.getQualifiedCoordinates().getLatitude() + ", " + location.getQualifiedCoordinates().getLongitude());
					MainClass.X=""+location.getQualifiedCoordinates().getLatitude();
					MainClass.Y=""+location.getQualifiedCoordinates().getLongitude();
					//statusField.setText("Obtained single location.");							
				} else{
					System.out.println("Failed to obtain location!");
				}
			}
		}.start();
		}catch(final Exception le){
			
		}
	}
	
	
	public int getMode(int selectedIndex){
		switch(selectedIndex){
			case 0:
				return SimpleLocationProvider.MODE_OPTIMAL;				
			case 1:
				return SimpleLocationProvider.MODE_GPS;
			case 2:
				return SimpleLocationProvider.MODE_GEOLOCATION;
			/*#ifdef BlackBerrySDK6.0.0
			case 3:
				return SimpleLocationProvider.MODE_GEOLOCATION_CELL;
			case 4:
				return SimpleLocationProvider.MODE_GEOLOCATION_WLAN;
			#endif*/
			default:
				return SimpleLocationProvider.MODE_OPTIMAL;
		}		
	}
	
	public void locationEvent(int event, Object eventData) {		
		synchronized(UiApplication.getEventLock()){
			if(event == SimpleLocationListener.EVENT_GPS_LOCATION){
				location = (BlackBerryLocation)eventData;
				System.out.println(location.getQualifiedCoordinates().getLatitude() + ", " + location.getQualifiedCoordinates().getLongitude());
				System.out.println("GPS");
				fixCount++;
				gpsFixCount++;
				System.out.println(Integer.toString(fixCount));
				System.out.println(Integer.toString(gpsFixCount));			
			} else if(event == SimpleLocationListener.EVENT_CELL_GEOLOCATION){
				location = (BlackBerryLocation)eventData;
				System.out.println(location.getQualifiedCoordinates().getLatitude() + ", " + location.getQualifiedCoordinates().getLongitude());
				System.out.println("Cell Tower Geolocation");
				fixCount++;
				geoFixCount++;
				System.out.println(Integer.toString(fixCount));
				System.out.println(Integer.toString(geoFixCount));			
			} else if(event == SimpleLocationListener.EVENT_WLAN_GEOLOCATION){
				location = (BlackBerryLocation)eventData;
				System.out.println(location.getQualifiedCoordinates().getLatitude() + ", " + location.getQualifiedCoordinates().getLongitude());
				System.out.println("WLAN Geolocation");
				fixCount++;
				geoFixCount++;
				System.out.println(Integer.toString(fixCount));
				System.out.println(Integer.toString(geoFixCount));			
			} else if(event == SimpleLocationListener.EVENT_UNKNOWN_MODE){
				location = (BlackBerryLocation)eventData;
				System.out.println(location.getQualifiedCoordinates().getLatitude() + ", " + location.getQualifiedCoordinates().getLongitude());
				System.out.println("Unknown");
				fixCount++;			
				System.out.println(Integer.toString(fixCount));						
			} else if(event == SimpleLocationListener.EVENT_ACQUIRING_LOCATION){
				System.out.println("EVENT_ACQUIRING_LOCATION - attempt = " + eventData);	
			} else if(event == SimpleLocationListener.EVENT_LOCATION_FAILED){
				System.out.println("EVENT_LOCATION_FAILED - attempt = " + eventData);	
			}
		}
	}
	public void debugLog(String msg) {
		// TODO Auto-generated method stub
		
	}
	
	
}

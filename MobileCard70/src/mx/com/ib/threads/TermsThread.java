package mx.com.ib.threads;

import java.io.UnsupportedEncodingException;
import java.util.Vector;

import crypto.Crypto;

import net.rim.device.api.system.Application;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;

import mx.com.ib.app.MainClass;
import mx.com.ib.connection.Communications;
import mx.com.ib.connection.HttpListener;
import mx.com.ib.parser.JSONParser;
import mx.com.ib.utils.Utils;
import mx.com.ib.views.MessagePopUpTune;
import mx.com.ib.views.MessagePopupScreen;
import mx.com.ib.views.ProductScreen;

public class TermsThread extends Thread implements HttpListener{
	
	public String url = MainClass.URL_TERMS;
	
	public TermsThread(){
		
	}
	
	public void run() {
		// TODO Auto-generated method stub		
		
		connect();
		
	}
	
	public void connect(){		
		
		if(MainClass.idealConnection != null)
		{
			
			if(this.url != null)
			{				
				try{				    	   
//					Communications.sendHttpPost(this,this.url,this.post);
					Communications.sendHttpRequest(this, this.url);

		       }catch(Throwable t){
		    	   getMessageError();
		       }
			}
		}
		else
		{		

			Utils.checkConnectionType();
			this.connect();
		}
		
	}

	public void handleHttpError(int errorCode, String error) {
		// TODO Auto-generated method stub
	
		getMessageError();
		
	}

	public boolean isDestroyed() {
		// TODO Auto-generated method stub
		return false;
	}

	public void receiveEstatus(String msg) {
		// TODO Auto-generated method stub
		
	}

	public void receiveHeaders(Vector _headers) {
		// TODO Auto-generated method stub
		
	}

	public void receiveHttpResponse(int appCode, byte[] response) {
		// TODO Auto-generated method stub
		
		String sTemp = null;
		StringBuffer sb = new StringBuffer();
		
		try {
			
			sb.append(new String(response,0,response.length,"UTF-8"));
			sTemp = new String (sb.toString());
			
			sTemp = Crypto.aesDecrypt(MainClass.key, sTemp);
			
			UiApplication.getUiApplication();
			synchronized (Application.getEventLock()) {
				
				MainClass.splashScreen.remove();
				
				String acentos= remplazacadena(MainClass.jsParser.getTerms(sTemp));
				
				UiApplication.getUiApplication().pushScreen(new MessagePopUpTune(acentos, Field.FOCUSABLE));
				
			}			
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			getMessageError();
		}
		
	}
	
	public void getMessageError(){

		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.remove();
			UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Informaci�n no disponible. Favor de intentar m�s tarde", Field.NON_FOCUSABLE));
		}
		
	}
	
	
	public String reemplaza(String src, String orig, String nuevo) {
        if (src == null) {
            return null;
        }
        int tmp = 0;
        String srcnew = "";
        while (tmp >= 0) {
            tmp = src.indexOf(orig);
            if (tmp >= 0) {
                if (tmp > 0) {
                     srcnew += src.substring(0, tmp);
                }
                srcnew += nuevo;
                src = src.substring(tmp + orig.length());
            }
        }
        srcnew += src;
        return srcnew;
    }

    public String remplazacadena(String conAcentos) {
        String caracteres[] = {"�?", "á", "É", "é", "�?", "í", "Ó", "ó", "Ú", "ú", "Ñ", "ñ"};
        String letras[] = {"�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�"};
        for (int counter = 0; counter < caracteres.length; counter++) {
            conAcentos = reemplaza(conAcentos, caracteres[counter], letras[counter]);
        }
        return conAcentos;
    }

}

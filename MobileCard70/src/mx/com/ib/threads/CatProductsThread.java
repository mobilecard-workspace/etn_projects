package mx.com.ib.threads;

import java.util.Vector;

import mx.com.ib.app.MainClass;
import mx.com.ib.connection.Communications;
import mx.com.ib.connection.HttpListener;
import mx.com.ib.utils.Utils;
import mx.com.ib.views.MessagePopupScreen;
import mx.com.ib.views.ProductScreen;
import net.rim.device.api.system.Application;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;
import crypto.Crypto;

public class CatProductsThread extends Thread implements HttpListener{
	
	public String url = MainClass.URL_GET_PRODUCTS;
	public String post = "";
	public String cla = "";

	public CatProductsThread(String clave){
		this.cla=clave;
		System.out.println("la clave que me llega: "+cla);
		post = "json="+Crypto.aesEncrypt(MainClass.key,"{\"proveedor\":\""+clave+"\"}");
	}
	
	public void run() {

		connect();
	}	
	
	public void connect(){		
	
		if(MainClass.idealConnection != null){			
			if(this.url != null){				
				try{				    	   
					Communications.sendHttpPost(this,this.url,this.post);
		       }catch(Throwable t){
		       }
			}
		} else {
			Utils.checkConnectionType();
			this.connect();
		}
	}

	public void handleHttpError(int errorCode, String error) {}

	public boolean isDestroyed() {
		return false;
	}

	public void receiveEstatus(String msg) {}

	public void receiveHeaders(Vector _headers) {}

	public void receiveHttpResponse(int appCode, byte[] response) {

		String sTemp = null;
		sTemp = new String(response);
		sTemp = Crypto.aesDecrypt(MainClass.key, sTemp);

		synchronized (Application.getEventLock()) {

			MainClass.splashScreen.remove();

			ProductScreen productScreen = null; 
			productScreen = new ProductScreen(MainClass.jsParser.getProducts("productos", sTemp));
			UiApplication.getUiApplication().pushScreen(productScreen);
			// UiApplication.getUiApplication().pushScreen(new CobroPlacaEdoMExico());
		}
	}
	
	public void getMessageError(){

		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.remove();
			UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Informaci�n no disponible. Favor de intentar m�s tarde", Field.NON_FOCUSABLE));
		}
	}
}



/*	
public void receiveHttpResponse(int appCode, byte[] response) {

	String sTemp = null;
	StringBuffer sb = new StringBuffer();
	
	try {
		sb.append(new String(response,0,response.length,"UTF-8"));
		sTemp = new String (sb.toString());
		sTemp = new String(response); 
		
		sTemp = Crypto.aesDecrypt(MainClass.key, sTemp);

		UiApplication.getUiApplication();
		synchronized (Application.getEventLock()) {

			MainClass.splashScreen.remove();
			if(cla.equals("VITA")){}

			UiApplication.getUiApplication().pushScreen(new ProductScreen(MainClass.jsParser.getProducts("productos", sTemp)));
		}
	} catch (UnsupportedEncodingException e) {
		e.printStackTrace();
		getMessageError();
	}
}
*/

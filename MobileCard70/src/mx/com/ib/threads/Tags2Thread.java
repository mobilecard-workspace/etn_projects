package mx.com.ib.threads;

import java.io.UnsupportedEncodingException;
import java.util.Vector;

import crypto.Crypto;

import net.rim.device.api.system.Application;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;

import mx.com.ib.app.MainClass;
import mx.com.ib.beans.ProductBean;
import mx.com.ib.beans.TipoRecargaTag;
import mx.com.ib.connection.Communications;
import mx.com.ib.connection.HttpListener;
import mx.com.ib.controls.PromotionLabelField;
import mx.com.ib.utils.Utils;
import mx.com.ib.views.MessagePopupScreen;
import mx.com.ib.views.PromotionsScreen;
import mx.com.ib.views.PswdPopupScreen;
import mx.com.ib.views.Tags2PopupScreen;
import mx.com.ib.views.TagsPopupScreen;

public class Tags2Thread extends Thread implements HttpListener{
	
	public String url = MainClass.URL_GET_TAGS;
	public boolean isLogin=false;
	
	public Tags2Thread(boolean isLogin){
		this.isLogin=isLogin;
	}
	
	public void run() {
		// TODO Auto-generated method stub
		connect();
	}
	
	public void connect(){
		
		Application.getApplication();
		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.start();
		}
		
		if(MainClass.idealConnection != null)
		{
			
			if(this.url != null)
			{				
				try{				    	   
					Communications.sendHttpRequest(this, this.url);

		       }catch(Throwable t){
		    	   getMessageError();
		       }
			}
		}
		else
		{		
			//System.out.println("ELSE: ");
			Utils.checkConnectionType();
			this.connect();
		}
		
	}

	public void handleHttpError(int errorCode, String error) {
		// TODO Auto-generated method stub
		getMessageError();
	}

	public boolean isDestroyed() {
		// TODO Auto-generated method stub
		return false;
	}

	public void receiveEstatus(String msg) {
		// TODO Auto-generated method stub
		
	}

	public void receiveHeaders(Vector _headers) {
		// TODO Auto-generated method stub
		
	}

	public void receiveHttpResponse(int appCode, byte[] response) {
		// TODO Auto-generated method stub
		
		String sTemp = null;
		StringBuffer sb = new StringBuffer();
		
		try {
			
			sb.append(new String(response,0,response.length,"UTF-8"));
			sTemp = new String (sb.toString());
			//System.out.println("STEMP: "+sTemp);
			sTemp = Crypto.aesDecrypt(MainClass.key, sTemp);
			System.out.println("STEMP TAGS: "+sTemp);
			
			TipoRecargaTag[] tags = MainClass.jsParser.getTags(sTemp);
			
			UiApplication.getUiApplication();
			synchronized (Application.getEventLock()) {
				
				MainClass.splashScreen.remove();
				
				if(tags.length > 0){
					
				
					MainClass.TagsBeans=tags;
					UiApplication.getUiApplication().pushScreen(new Tags2PopupScreen("", Field.NON_FOCUSABLE,isLogin));
					
				}else{
					
					UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Ocurri�n un error en el servidor, intente m�s tarde", Field.NON_FOCUSABLE));
					
				}
				
			}
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			getMessageError();
		}
		
		
	}
	
	public void getMessageError(){

		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.remove();
			UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Informaci�n no disponible. Favor de intentar m�s tarde", Field.NON_FOCUSABLE));
		}
		
	}

}

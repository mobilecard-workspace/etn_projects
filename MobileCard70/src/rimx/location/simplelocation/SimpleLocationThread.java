package rimx.location.simplelocation;

import javax.microedition.location.LocationListener;

import net.rim.device.api.gps.BlackBerryCriteria;
import net.rim.device.api.gps.BlackBerryLocation;
import net.rim.device.api.gps.BlackBerryLocationProvider;


/**
 * Handles all tasks to setup a tracking session.
 * @author shaque
 *
 */
class SimpleLocationThread extends Thread{	
//class SimpleLocationThread{
	private BlackBerryCriteria criteria;
	private BlackBerryLocationProvider locationProvider = null;	
	private LocationListener listener = null;
	private SimpleLocationProvider simpleProvider;
		
	private int trackingInterval = 5;
	

	public SimpleLocationThread(BlackBerryCriteria criteria, int interval, LocationListener listener, SimpleLocationProvider simpleProvider){
		this.criteria = criteria;
		this.trackingInterval = interval;		
		this.listener = listener;		
		this.simpleProvider = simpleProvider;
	}

	public void run(){		
		startTracking();			
	}
			
	private void startTracking(){
		try{
			locationProvider = (BlackBerryLocationProvider) BlackBerryLocationProvider.getInstance(criteria);
			simpleProvider.updateLocationProviderReference(locationProvider);
			locationProvider.setLocationListener(listener, trackingInterval, -1, -1);			
		} catch (Throwable t){
			
		}
	}
	
	public BlackBerryLocation getSingleFix(int timeout){
		try{
			locationProvider = (BlackBerryLocationProvider) BlackBerryLocationProvider.getInstance(criteria);
			simpleProvider.updateLocationProviderReference(locationProvider);
			return (BlackBerryLocation)locationProvider.getLocation(timeout);	
		} catch (Throwable t){
			return null;
		}		
	}
	
	public void resetProvider(){		
		if(locationProvider!=null){			
			locationProvider.setLocationListener(null, 0, 0, 0);
			locationProvider.reset();
			locationProvider = null;			
		}
	}	
}
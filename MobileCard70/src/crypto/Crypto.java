//package ironbit.system.crypto;

package crypto;

import mx.com.ib.utils.Utils;


public class Crypto {
	
	public static String aesEncrypt(String seed, String cleartext) {
		
		String decryptedText = "";
		
		try{
			
			decryptedText = AESBinFormat.encode(Utils.replaceConAcento(cleartext), seed);

		}catch(Exception e){
			
			e.printStackTrace();
			decryptedText = "";
		}
		 	   
		return decryptedText;
	}
	
	
	public static String aesDecrypt(String seed, String encrypted) {
		
		String decryptedText = "";
		
		try{
			decryptedText =  Utils.reemplazaHTML(AESBinFormat.decode(encrypted, seed));
		}catch(Exception e){
			e.printStackTrace();
			decryptedText = "";
		}
		
		return decryptedText;
	}
}
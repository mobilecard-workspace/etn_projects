//#preprocess
package addcel.core.mx.communication;
import net.rim.device.api.servicebook.*;
import net.rim.device.api.system.DeviceInfo;
import net.rim.device.api.system.CoverageInfo;
//#ifdef JDE_4_7_0
import net.rim.device.api.system.WLANInfo;
//#endif

public class Connection {
        public Connection() {
        }
        /*public String getUID() {
            String uid = ";deviceside=false";
            try {
                ServiceBook sb = ServiceBook.getSB();
                ServiceRecord[] records = sb.findRecordsByCid("WPTCP");
        
                for(int i=0; i < records.length; i++) {
                    if (records[i].isValid() && !records[i].isDisabled()) {
                        if (records[i].getUid() != null && records[i].getUid().length() != 0) {
                            if ((records[i].getUid().toLowerCase().indexOf("wap2") != -1) &&
                                (records[i].getUid().toLowerCase().indexOf("wifi") == -1) &&
                                (records[i].getUid().toLowerCase().indexOf("mms") == -1)) {
                                    return uid + ";ConnectionUID=" + records[i].getUid();
                            }
                        }
                    } 
                }
            } catch(Exception e) {
                System.out.println("Connection.getUID():" + e.toString());
            }
            return uid;
        }*/
        
    public synchronized String getUID() {     
        String conString = "";
                        
        if(DeviceInfo.isSimulator()) {
           // if(ResourceProperties.USE_MDS_IN_SIMULATOR) {
                conString = ";deviceside=false";                 
          //  }
            //else {
                conString = ";deviceside=true";
           // }
        }
        
        //#ifdef JDE_4_7_0
        else if(WLANInfo.getWLANState() == WLANInfo.WLAN_STATE_CONNECTED) {
            conString = ";interface=wifi";
        }
        //#endif     

        else if((CoverageInfo.getCoverageStatus() & CoverageInfo.COVERAGE_DIRECT) == CoverageInfo.COVERAGE_DIRECT) {             
            String carrierUid = getCarrierBIBSUid();
            if(carrierUid == null) {
                //conString = ";deviceside=true";
                conString = null;
            }
            else {
                conString = ";deviceside=false;connectionUID="+carrierUid + ";ConnectionType=mds-public";
            }
        }               
        
        else if((CoverageInfo.getCoverageStatus() & CoverageInfo.COVERAGE_MDS) == CoverageInfo.COVERAGE_MDS) {
            conString = ";deviceside=false";
        }
        
        else if(CoverageInfo.getCoverageStatus() == CoverageInfo.COVERAGE_NONE) {
            conString = null;
        }
        
        else {
            //conString = ";deviceside=false";
            conString = null;
        }        
        //if(conString == null) conString = "";
        return conString;
    }
    
    private synchronized String getCarrierBIBSUid() {
        ServiceRecord[] sr = ServiceBook.getSB().getRecords();
        int current;
        int length = sr.length; 
        
        for(current = 0; current < length; current++) {
            if(sr[current].getCid().toLowerCase().equals("ippp")) {
                if(sr[current].getName().toLowerCase().indexOf("bibs") >= 0) {
                    return sr[current].getUid();
                }
            }
        }
        return null;
    }
    
}



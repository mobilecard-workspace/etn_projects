package mx.com.ib.parser;



import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import app.MainClass;

import beans.Busqueda;
import beans.CategoriesBean;
import beans.Especialidad;
import beans.Estado;
import beans.LoginBean;
import beans.Municipio;


import beans.Respuesta;
import beans.Version;
import beans.member;


public class JSONParser {
	
	public JSONParser(){
		
	}
	
	
	public member GET_MEMBER(String json){
		

		member catBean= new member() ; 
			String Date = null,Member = null,Name = "";
			String Code="";
			String LastName="";
			JSONArray array=new JSONArray();
			try {						
		;
		
					JSONObject jsObject = new JSONObject(json);
					if(jsObject.has("code")){				
						Code = jsObject.getString("code");	
						//System.out.println(id);
					}
					if(jsObject.has("date")){
						Date = jsObject.getString("date");
						//System.out.println(msj);
				}	
					if(jsObject.has("lastname")){
						LastName = jsObject.getString("lastname");
					//	System.out.println(url);
				}	
					if(jsObject.has("members")){
						Member = jsObject.getString("members");
					//	System.out.println(url);Name
				}	
					if(jsObject.has("name")){
						Name = jsObject.getString("name");
					//	System.out.println(url);
				}	
					if(jsObject.has("lista")){
						array = jsObject.getJSONArray("lista");
						try {	
							
							String Date2 = null,Member2 = null,Name2 = "";
							String Code2="";
							String LastName2="";
							member[] benes= new member[0] ; 
							benes= new member[array.length()];
							for(int x=0; x<array.length();x++){
								JSONObject jsObject2 = array.getJSONObject(x);
								if(jsObject2.has("code")){				
									Code2 = jsObject2.getString("code");	
									//System.out.println(id);
								}
								if(jsObject2.has("date")){
									Date2 = jsObject2.getString("date");
									//System.out.println(msj);
							}	
								if(jsObject2.has("lastname")){
									LastName2 = jsObject2.getString("lastname");
								//	System.out.println(url);
							}	
								if(jsObject2.has("members")){
									Member2 = jsObject2.getString("members");
								//	System.out.println(url);Name
							}	
								if(jsObject2.has("name")){
									Name2 = jsObject2.getString("name");
								//	System.out.print2ln(url);
							}	
								benes[x] = new member(Code2, Name2, LastName2,Date2,Member2);
							}	
							MainClass.Benes=benes;
								
							
					
							
							
							
						} catch (JSONException e) {
							
							e.printStackTrace();
							return catBean;
						}
						
						
					//	System.out.println(url);
				}	
					catBean = new member(Code, Name, LastName,Date,Member);
					
				
				
				
				
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return catBean;
			}
			
			return catBean;
			}
	
	
public CategoriesBean[] GETCATEGORIES_MOBILE(String json){
	

	CategoriesBean[] catBean= new CategoriesBean[0] ; 
		String msj = "";
		String id="";
		String url="";
		
		try {						
			JSONArray lista = new JSONArray(json);
			catBean= new CategoriesBean[lista.length()];
			for(int x=0; x<=lista.length();x++){
				JSONObject jsObject = lista.getJSONObject(x);
				if(jsObject.has("ID")){				
					id = jsObject.getString("ID");	
					//System.out.println(id);
				}
				if(jsObject.has("Text")){
					msj = jsObject.getString("Text");
					//System.out.println(msj);
			}	
				if(jsObject.has("URL")){
					url = jsObject.getString("URL");
				//	System.out.println(url);
			}	
				catBean[x] = new CategoriesBean(id, msj, url);
			}
			
			
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return catBean;
		}
		
		return catBean;
		}




public Respuesta SET_COMMENT(String json){
	

	Respuesta catBean= new Respuesta(); 
		String msj = "";
		String id="";
		
		
		try {						
			JSONObject lista = new JSONObject(json);
		
		
				
				if(lista.has("ID")){				
					id = lista.getString("ID");	
					System.out.println(id);
				}
				if(lista.has("Mensaje")){
					msj = lista.getString("Mensaje");
					System.out.println(msj);
			}	
				
				catBean = new Respuesta(id, msj);
			
			
			
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return catBean;
		}
		
		return catBean;
		}


public LoginBean GET_LOGIN(String json){
	

	LoginBean catBean= new LoginBean(); 
		
		int id=0;
		
		String title="";
		
		try {						
		
				JSONObject jsObject = new JSONObject(json);
				if(jsObject.has("ID")){				
					id = jsObject.getInt("ID");	
					//System.out.println(id);
				}
				
				if(jsObject.has("Mensaje")){
					title = jsObject.getString("Mensaje");
					//System.out.println(msj);
			}	
				
				catBean = new LoginBean(title,id);
			
			
			
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return catBean;
		}
		
		return catBean;
		}

public Busqueda[] GET_BUSQUEDA(String json){
	
	System.out.println(json);
	Busqueda[] catBean= null ; 
		
		String ID="";
		
		String Especialidades = "",Nombre = "",Precio = "",Ubicacion="";
		
		try {						
			JSONArray lista = new JSONArray(json);
			catBean= new Busqueda[lista.length()];
			for(int x=0; x<lista.length();x++){
				JSONObject jsObject = lista.getJSONObject(x);
				if(jsObject.has("id")){				
					ID = jsObject.getString("id");	
					//System.out.println(id);
				}
				if(jsObject.has("ubicacion")){
					Ubicacion = jsObject.getString("ubicacion");
					//System.out.println(msj);
			}	
				if(jsObject.has("precio")){
					Precio = jsObject.getString("precio");
					//System.out.println(msj);
			}	
				if(jsObject.has("nombre")){
					Nombre = jsObject.getString("nombre");
					//System.out.println(msj);
			}	
				if(jsObject.has("especialidades")){
					Especialidades = jsObject.getString("especialidades");
					//System.out.println(msj);
			}	
				
				catBean[x] = new Busqueda(Especialidades,ID,Nombre,Precio,Ubicacion);
			}
			
			
			
			
		} catch (JSONException e) {
			
			System.out.println("e*********************** "+e);
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			
			return catBean;
		}
		
		return catBean;
		}

public Especialidad[] GET_ESP(String json){
	

	Especialidad[] catBean= new Especialidad[0] ; 
		
		String id="";
		
		String title="";
		
		try {						
			JSONArray lista = new JSONArray(json);
			catBean= new Especialidad[lista.length()];
			for(int x=0; x<=lista.length();x++){
				JSONObject jsObject = lista.getJSONObject(x);
				if(jsObject.has("id")){				
					id = jsObject.getString("id");	
					//System.out.println(id);
				}
				
				if(jsObject.has("especialidades")){
					title = jsObject.getString("especialidades");
					//System.out.println(msj);
			}	
				
				catBean[x] = new Especialidad(id,title);
			}
			
			
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return catBean;
		}
		
		return catBean;
		}
public Municipio[] GET_MUN(String json){
	

	Municipio[] catBean= null ; 
		
		String id="";
		
		String title="";
		
		try {						
			JSONArray lista = new JSONArray(json);
			catBean= new Municipio[lista.length()];
			for(int x=0; x<lista.length();x++){
				JSONObject jsObject = lista.getJSONObject(x);
				if(jsObject.has("id")){				
					id = jsObject.getString("id");	
					//System.out.println(id);
				}
				
				if(jsObject.has("municipios")){
					title = jsObject.getString("municipios");
					//System.out.println(msj);
			}	
				
				catBean[x] = new Municipio(id,title);
			}
			
			
			
			
		} catch (JSONException e) {
			
			e.printStackTrace();
			return catBean;
		}
		
		return catBean;
		}
public Estado[] GET_ESTADOS(String json){
	

	Estado[] catBean= new Estado[0] ; 
		
		String id="";
		
		String title="";
		
		try {						
			JSONArray lista = new JSONArray(json);
			catBean= new Estado[lista.length()];
			for(int x=0; x<lista.length();x++){
				JSONObject jsObject = lista.getJSONObject(x);
				if(jsObject.has("id")){				
					id = jsObject.getString("id");	
					//System.out.println(id);
				}
				
				if(jsObject.has("estado")){
					title = jsObject.getString("estado");
					//System.out.println(msj);
			}	
				
				catBean[x] = new Estado(id,title);
			}
			
			
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return catBean;
		}
		
		return catBean;
		}















public Version GET_VERSION(String json){
	

	Version VER= new Version() ; 
		
		String id="";
		String url="";
		String tipo="";
		String version="";
		
		try {			
			JSONObject jsObject= new JSONObject(json);
			
			
				if(jsObject.has("id")){				
					id = jsObject.getString("id");	
					//System.out.println(id);
				}
				if(jsObject.has("tipo")){				
					tipo = jsObject.getString("tipo");	
					//System.out.println(id);
				}
				if(jsObject.has("url")){
					url = jsObject.getString("url");
					//System.out.println(msj);
			}	
				if(jsObject.has("version")){
					version = jsObject.getString("version");
					//System.out.println(msj);
			}	
				
				
				//int id, String tipo, String version, String url
				VER = new Version(Integer.parseInt(id),tipo,version,url);
			
			
			
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return VER;
		}
		
		return VER;
		}

	
}

package beans;

public class member {

	String code,name,lastname,date,member;

	public member() {
		super();
		// TODO Auto-generated constructor stub
	}

	public member(String code, String name, String lastname, String date,
			String member) {
		super();
		this.code = code;
		this.name = name;
		this.lastname = lastname;
		this.date = date;
		this.member = member;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getMember() {
		return member;
	}

	public void setMember(String member) {
		this.member = member;
	}
}

package beans;

public class Busqueda {
String Especialidades,Nombre,Precio,Ubicacion=null;
String ID="";
public Busqueda() {
	super();
	// TODO Auto-generated constructor stub
}

public Busqueda(String especialidades, String iD, String nombre, String precio,
		String ubicacion) {
	super();
	Especialidades = especialidades;
	ID = iD;
	Nombre = nombre;
	Precio = precio;
	Ubicacion = ubicacion;
}

public String getEspecialidades() {
	return Especialidades;
}

public void setEspecialidades(String especialidades) {
	Especialidades = especialidades;
}

public String getID() {
	return ID;
}

public void setID(String iD) {
	ID = iD;
}

public String getNombre() {
	return Nombre;
}

public void setNombre(String nombre) {
	Nombre = nombre;
}

public String getPrecio() {
	return Precio;
}

public void setPrecio(String precio) {
	Precio = precio;
}

public String getUbicacion() {
	return Ubicacion;
}

public void setUbicacion(String ubicacion) {
	Ubicacion = ubicacion;
}
}

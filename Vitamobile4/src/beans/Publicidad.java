package beans;

import net.rim.device.api.system.Bitmap;

public class Publicidad {
	
	
	 String title;
     String text;
     String urlImg;
     String url;
     String date;
     int id;
     int Sectionid;
     private Bitmap thumbnail;


     public Publicidad(int id, int Sectionid, String title, String text, String urlImg,String url)
     {
         this.text = text;
         this.id = id;
         this.Sectionid = Sectionid;
         this.title = title;
         this.urlImg = urlImg;
         this.url = url;
        
     }
     public Publicidad(int id, int Sectionid, String title, String text, String urlImg)
     {
         this.text = text;
         this.id = id;
         this.Sectionid = Sectionid;
         this.title = title;
         this.urlImg = urlImg;      
     }

     public Publicidad() {
		// TODO Auto-generated constructor stub
	}
	public String gettitle()
     {
         return title;
     }

	
	 public Bitmap getThumbnail() {
         return thumbnail;
 }
 public void setThumbnail(Bitmap thumbnail) {
         this.thumbnail = thumbnail;
 }
     public void settitle(String title)
     {
         this.title = title;
     }
     public String geturlNews()
     {
         return url;
     }

     public void setDate(String date)
     {
         this.date = date;
     }
     public String getDate()
     {
         return date;
     }

     public void setUrlnews(String urlNews)
     {
         this.url = urlNews;
     }

     public String getText()
     {
         return text;
     }

     public void setText(String text)
     {
         this.text = text;
     }

     public String getURL()
     {
         return urlImg;
     }

     public void setURL(String url)
     {
         this.urlImg = url;
     }

     public int getId()
     {
         return id;
     }

     public void setId(int ID)
     {
         this.id = ID;
     }

     public int getIdSection()
     {
         return Sectionid;
     }

     public void setIdSection(int IDSection)
     {
         this.Sectionid = IDSection;
     }

}

package mypackage;

import net.rim.blackberry.api.browser.Browser;
import net.rim.blackberry.api.browser.BrowserSession;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import Controls.BannerBitmap;
import Controls.BgManager;
import Controls.CustomVerticalFieldManager;
import Controls.GenericImageButtonField;
import app.MainClass;

public class queEsVitaScreen extends MainScreen{
	
	public LabelField Message = null;
	
	public VerticalFieldManager hfmMain = null;
	public HorizontalFieldManager hfmTitle = null;
	
	public HorizontalFieldManager hfmBtns1 = null;
	public HorizontalFieldManager hfmBtns2 = null;
	public HorizontalFieldManager hfmBtns3 = null;
	public HorizontalFieldManager hfmBtns4 = null;
	public VerticalFieldManager  hfmBtns5 = null;
	public HorizontalFieldManager hfmBtns6 = null;
	
	public CustomVerticalFieldManager customVerticalFieldManager = null;
	public CustomVerticalFieldManager customVerticalFieldManager2 = null;
	public GenericImageButtonField btnBeneficios = null;
	public GenericImageButtonField btnFunciona = null;
	public GenericImageButtonField btnPreguntas = null;
	public GenericImageButtonField btnAtencion = null;

	
	public BgManager bgManager = null;
	public Bitmap bField=null;
	public Bitmap bField2=null;
	
	
	public HorizontalFieldManager hfmbtn = null;
	public VerticalFieldManager vfmFooter = null;
	
	public queEsVitaScreen(){
		
		initVariables();
		initButtons();
		addFields();
		
		
	}
	
	public void initVariables(){
	 
		bgManager = new BgManager(MainClass.BG_MENU);

		hfmMain = new VerticalFieldManager(Field.FIELD_HCENTER|VERTICAL_SCROLL|Field.USE_ALL_WIDTH);
		hfmTitle = new HorizontalFieldManager(Field.FIELD_RIGHT);
		vfmFooter = new VerticalFieldManager(VERTICAL_SCROLL);
		
		customVerticalFieldManager = new CustomVerticalFieldManager(Display.getWidth(),Display.getHeight()/2, HorizontalFieldManager.USE_ALL_WIDTH|Field.FIELD_VCENTER);
		customVerticalFieldManager2 = new CustomVerticalFieldManager(Display.getWidth(),Display.getHeight()/3, HorizontalFieldManager.USE_ALL_WIDTH|Field.FIELD_VCENTER);
		hfmBtns1 = new HorizontalFieldManager(Field.FIELD_HCENTER){
			//define width
           
			
		};
		hfmBtns2 = new HorizontalFieldManager(Field.FIELD_HCENTER);
		hfmBtns3 = new HorizontalFieldManager(HorizontalFieldManager.FIELD_RIGHT){
			
			 public int getPreferredWidth()
	            {
	                return Display.getWidth();
	            }
	      
	          /*  protected void sublayout( int maxWidth, int maxHeight )
	            {
	                super.sublayout(getPreferredWidth(), 
	                                getPreferredHeight());
	                setExtent(getPreferredWidth(), getPreferredHeight());
	            }*/
		};
		hfmBtns4 = new  HorizontalFieldManager(Field.FIELD_RIGHT);
		hfmBtns6 = new  HorizontalFieldManager(Field.FIELD_HCENTER);
		   hfmbtn = new HorizontalFieldManager(Field.FIELD_RIGHT);
		bField =  Bitmap.getBitmapResource(MainClass.HEADER_MENU);
		bField2=Bitmap.getBitmapResource(MainClass.TEXT);
		//bField2 =  Bitmap.getBitmapResource(MainClass.BTN_INVITA);
		hfmBtns5 = new  VerticalFieldManager(Field.FIELD_HCENTER);
		
		
		Message = new LabelField("Membres�a Vitam�dica ofrece asistencia m�dica telef�nica gratuita, precios preferenciales con m�dicos privados de diferentes especialidades y descuentos exclusivos en farmacias y otros servicios m�dicos.",Field.FIELD_HCENTER){			
			public void paint(Graphics graphics){				
				int g = graphics.getColor();
				graphics.setColor(MainClass.BG_GRAY);
				super.paint(graphics);
			}
			protected void onFocus(int direction) {  
		        super.onFocus(direction);
		        invalidate(); 
		     //   
		    }   
		 
			 protected void onUnfocus() {  
		           super.onUnfocus();
		           invalidate();  
		    } 
		};
		Message.setFont(MainClass.appFont1);
		
	}
	
	public void addFields(){	
	//	setBanner(new BannerBitmap(MainClass.HEADER));
		
		setBanner(new BannerBitmap(MainClass.SUB_HEADER));
		add(new NullField());
		hfmMain.add(new LabelField());
		hfmBtns1.add(btnBeneficios);
		hfmBtns1.add(btnFunciona);
		
	
		
		//margen 
	
	/*	btndirectorio.setMargin(Display.getHeight()/6, 0, 0, 0);
		btnvita.setMargin(Display.getHeight()/6, 0, 0, 0);
		btnporcentaje.setMargin(Display.getHeight()/6, 0, 0, 0);
		
		if(Display.getHeight()==480){
			btndirectorio.setMargin(Display.getHeight()/10, 0, 0, 0);
			btnvita.setMargin(Display.getHeight()/10, 0, 0, 0);
			btnporcentaje.setMargin(Display.getHeight()/10, 0, 0, 0);
		}*/
		
	
		hfmMain.add(hfmBtns3);
		
		
		hfmMain.add(hfmBtns6);
		
	//	hfmMain.add(Message);
		hfmBtns1.setPadding(Display.getHeight()/30, 0, 0, 0);
		hfmMain.add(hfmBtns1);	
		
		
		//parte de abajo
		hfmBtns2.add(btnPreguntas);
		hfmBtns2.add(btnAtencion);
	
		hfmBtns2.setPadding(Display.getHeight()/30, 0, 0, 0);
		hfmMain.add(hfmBtns2);	

		
		
		
		
		
		
		bgManager.add(hfmMain);
		
		add(bgManager);
		
	}
	
	public void initButtons(){
		
		btnBeneficios = new GenericImageButtonField(MainClass.BENEFICIOS_OVER,MainClass.BENEFICIOS,"comprar"){
			
			protected boolean navigationClick(int status, int time) {
				BrowserSession browserSession; 
    			browserSession = Browser.getDefaultSession();   			
    			browserSession.displayPage("http://mobilecard.mx:8080/Vitamedica/beneficiosi.htm");	
				return true;
			}
			
		};
		btnPreguntas = new GenericImageButtonField(MainClass.PREGUNTAS_OVER,MainClass.PREGUNTAS,"comprar"){
			
			protected boolean navigationClick(int status, int time) {
				BrowserSession browserSession; 
    			browserSession = Browser.getDefaultSession();   			
    			browserSession.displayPage("http://mobilecard.mx:8080/Vitamedica/preguntasi.htm");	
				
				
				return true;
			}
			
		}; 
		
		btnAtencion = new GenericImageButtonField(MainClass.ATENCION_OVER,MainClass.ATENCION,"comprar"){
			
			protected boolean navigationClick(int status, int time) {
				
				BrowserSession browserSession; 
    			browserSession = Browser.getDefaultSession();   			
    			browserSession.displayPage("http://mobilecard.mx:8080/Vitamedica/atencioni.htm");	
				
				return true;
			}
			
		}; 
		btnFunciona = new GenericImageButtonField(MainClass.FUNCIONA_OVER,MainClass.FUNCIONA,"comprar"){
			
			protected boolean navigationClick(int status, int time) {
				BrowserSession browserSession; 
    			browserSession = Browser.getDefaultSession();   			
    			browserSession.displayPage("http://mobilecard.mx:8080/Vitamedica/comofuncionai.htm");	
				
				return true;
			}
			
		}; 
		}
	
	
	

	
	protected boolean onSavePrompt() {
		return true;
	}
	
	public void close()
    {
		UiApplication.getUiApplication().popScreen(this);
	
    }
	
	protected boolean keyDown(int keycode, int time)
	   {

	        if (Keypad.key(keycode) == Characters.ESCAPE) 
	        {
	        	close();
	     
	        	return true;
	        }
	        return false;
	   }

}
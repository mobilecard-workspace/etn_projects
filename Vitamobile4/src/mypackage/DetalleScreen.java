package mypackage;

import net.rim.blackberry.api.invoke.Invoke;
import net.rim.blackberry.api.invoke.PhoneArguments;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ActiveRichTextField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import Controls.BannerBitmap;
import Controls.BgManager;
import Controls.BorderBasicEditField;
import Controls.GenericImageButtonField;
import app.MainClass;

public class DetalleScreen extends MainScreen{
	public BgManager bgManager = null;
	public VerticalFieldManager hfmMain,hfmbtn2 = null;
	public HorizontalFieldManager hfmbtn = null;
	public LabelField MyName,MyName2,proveedor,nameFriend,email,nombre,nombre2= null;
	public BorderBasicEditField basicMyName,basicnameFriend, basicemail=null;
	public int widthManager = 0;
	public GenericImageButtonField btnOk = null;
	public ActiveRichTextField note=null;
	public ActiveRichTextField note2=null;
	   VerticalFieldManager m = new VerticalFieldManager();
	String name="",prove="",Direccion="",Cp="",Colonia="",Tel="",Tel2="",Municipio="",Estado="",Cx="",Cy="",Lunes="",Martes="",Miercoles="",Jueves="",Viernes="",Sabado="",Domingo="";
	private int x=0;

	public DetalleScreen(){
		initVariables();
		addFields();
	}
	public void initVariables(){
	  
	    widthManager = Display.getWidth();		
	    
	    try {
			JSONObject jsObject=new JSONObject(MainClass.DetalleProveedor);
			if(jsObject.has("nombre")){				
				name = jsObject.getString("nombre");	
				//System.out.println(id);
			}
			if(jsObject.has("especialidad")){				
				prove = jsObject.getString("especialidad");	
				//System.out.println(id);
			}
			if(jsObject.has("direccion")){				
				Direccion = jsObject.getString("direccion");	
				//System.out.println(id);
			}
			if(jsObject.has("cp")){				
				Cp = jsObject.getString("cp");	
				//System.out.println(id);
			}
			if(jsObject.has("colonia")){				
				Colonia = jsObject.getString("colonia");	
				//System.out.println(id);
			}
			if(jsObject.has("municipio")){				
				Municipio = jsObject.getString("municipio");	
				//System.out.println(id);Estado
			}
			if(jsObject.has("estado")){				
				Estado = jsObject.getString("estado");	
				//System.out.println(id);
			}
			if(jsObject.has("cx")){				
				Cx = jsObject.getString("cx");	
				//System.out.println(id);
			}
			if(jsObject.has("cy")){				
				Cy = jsObject.getString("cy");	
				//System.out.println(id);
			}
			if(jsObject.has("lunes")){				
				Lunes = jsObject.getString("lunes");	
				//System.out.println(id);
			}
			
			if(jsObject.has("martes")){				
				Martes = jsObject.getString("martes");	
				//System.out.println(id);
			}
			
			if(jsObject.has("miercoles")){				
				Miercoles = jsObject.getString("miercoles");	
				//System.out.println(id);
			}
			
			if(jsObject.has("jueves")){				
				Jueves = jsObject.getString("jueves");	
				//System.out.println(id);
			}
			if(jsObject.has("viernes")){				
				Viernes = jsObject.getString("viernes");	
				//System.out.println(id);
			}
			if(jsObject.has("sabado")){				
				Sabado = jsObject.getString("sabado");	
				//System.out.println(id);
			}
			if(jsObject.has("domingo")){				
				Domingo = jsObject.getString("domingo");	
				//System.out.println(id);
			}
			if(jsObject.has("tel")){				
				Tel = jsObject.getString("tel");	
				//System.out.println(id);
			}
			if(jsObject.has("tel2")){				
				Tel2 = jsObject.getString("tel2");	
				//System.out.println(id);
			}
			
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		//bgManager = new BgManager(MainClass.BG_CUENTA);
		bgManager = new BgManager(MainClass.BG_MENU);
	    MyName = new LabelField("Nombre: ",Field.NON_FOCUSABLE){
			public void paint(Graphics graphics){				
				int g = graphics.getColor();
				graphics.setColor(0x000000);
				super.paint(graphics);
			}
			protected void onFocus(int direction) {  
		        super.onFocus(direction);
		        invalidate(); 
		     //   
		    }   
		 
			 protected void onUnfocus() {  
		           super.onUnfocus();
		           invalidate();  
		    }
            };	
	    MyName2 = new LabelField(name, Field.FOCUSABLE){
			public void paint(Graphics graphics){				
				int g = graphics.getColor();
				graphics.setColor(0x000000);
				super.paint(graphics);
			}
			protected void onFocus(int direction) {  
		        super.onFocus(direction);
		        invalidate(); 
		     //   
		    }   
		 
			 protected void onUnfocus() {  
		           super.onUnfocus();
		           invalidate();  
		    }
            };	
	    proveedor = new LabelField(prove, Field.FOCUSABLE){
			public void paint(Graphics graphics){				
				int g = graphics.getColor();
				graphics.setColor(0x000000);
				super.paint(graphics);
			}
			protected void onFocus(int direction) {  
		        super.onFocus(direction);
		        invalidate(); 
		     //   
		    }   
		 
			 protected void onUnfocus() {  
		           super.onUnfocus();
		           invalidate();  
		    }
            };	
	    //MyName2.setBackground(BackgroundFactory.createLinearGradientBackground(0x0099CCFF,0x0099CCFF,0x00336699,0x00336699));
	  //  proveedor.setBackground(BackgroundFactory.createLinearGradientBackground(0x0099CCFF,0x0099CCFF,0x00336699,0x00336699));
	   
	    
	    
	 


	    
	    
	    hfmMain = new VerticalFieldManager(Field.FIELD_HCENTER|VERTICAL_SCROLL|Field.USE_ALL_WIDTH);
	    hfmbtn = new HorizontalFieldManager(Field.FIELD_LEFT);
	  
	    
	    
	    btnOk =  new GenericImageButtonField(MainClass.BTN_INVITAR_OVER,MainClass.BTN_INVITAR,"ok"){
			
			protected boolean navigationClick(int status, int time) {
			
				
				return true;
			}
			
		};		
		
		
		//parser
		
	}
	public void addFields(){
		setBanner(new BannerBitmap(MainClass.SUB_HEADER));
		add(new NullField());
		
		hfmbtn.add(MyName);
		
		MyName.setFont(Font.getDefault().derive(Font.BOLD, Font.getDefault().getHeight(), Ui.UNITS_px));
		hfmMain.add(hfmbtn);
		hfmMain.add(new LabelField(""));
		hfmbtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		//hfmbtn.setBackground(BackgroundFactory.createLinearGradientBackground(0x0099CCFF,0x0099CCFF,0x00336699,0x00336699));
		hfmbtn.add(MyName2);
		hfmMain.add(hfmbtn);
		hfmbtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		hfmbtn.add(proveedor);
	//	hfmbtn.setBackground(BackgroundFactory.createLinearGradientBackground(0x0099CCFF,0x0099CCFF,0x00336699,0x00336699));
		hfmMain.add(hfmbtn);
		
	
		MyName = new LabelField("Direcci�n: ",Field.NON_FOCUSABLE){
			public void paint(Graphics graphics){				
				int g = graphics.getColor();
				graphics.setColor(0x000000);
				super.paint(graphics);
			}
			protected void onFocus(int direction) {  
		        super.onFocus(direction);
		        invalidate(); 
		     //   
		    }   
		 
			 protected void onUnfocus() {  
		           super.onUnfocus();
		           invalidate();  
		    }
            };	
	    MyName.setFont(Font.getDefault().derive(Font.BOLD, Font.getDefault().getHeight(), Ui.UNITS_px));
	    hfmbtn = new HorizontalFieldManager(Field.FIELD_LEFT);
	    hfmMain.add(MyName);
		hfmMain.add(new LabelField(""));
		
		
	    MyName2 = new LabelField(Direccion, Field.FOCUSABLE){
			public void paint(Graphics graphics){				
				int g = graphics.getColor();
				graphics.setColor(0x000000);
				super.paint(graphics);
			}
			protected void onFocus(int direction) {  
		        super.onFocus(direction);
		        invalidate(); 
		     //   
		    }   
		 
			 protected void onUnfocus() {  
		           super.onUnfocus();
		           invalidate();  
		    }
            };	
		hfmbtn = new HorizontalFieldManager(Field.FIELD_LEFT);
	//	hfmbtn.setBackground(BackgroundFactory.createLinearGradientBackground(0x0099CCFF,0x0099CCFF,0x00336699,0x00336699));
		hfmbtn.add(MyName2);
		hfmMain.add(hfmbtn);
		
		
		
		  MyName2 = new LabelField("C.P."+": "+Cp, Field.FOCUSABLE){
				public void paint(Graphics graphics){				
					int g = graphics.getColor();
					graphics.setColor(0x000000);
					super.paint(graphics);
				}
				protected void onFocus(int direction) {  
			        super.onFocus(direction);
			        invalidate(); 
			     //   
			    }   
			 
				 protected void onUnfocus() {  
			           super.onUnfocus();
			           invalidate();  
			    }
	            };	
			hfmbtn = new HorizontalFieldManager(Field.FIELD_LEFT);
		//	hfmbtn.setBackground(BackgroundFactory.createLinearGradientBackground(0x0099CCFF,0x0099CCFF,0x00336699,0x00336699));
			hfmbtn.add(MyName2);
			hfmMain.add(hfmbtn);
		
			
		    MyName2 = new LabelField(Colonia, Field.FOCUSABLE){
				public void paint(Graphics graphics){				
					int g = graphics.getColor();
					graphics.setColor(0x000000);
					super.paint(graphics);
				}
				protected void onFocus(int direction) {  
			        super.onFocus(direction);
			        invalidate(); 
			     //   
			    }   
			 
				 protected void onUnfocus() {  
			           super.onUnfocus();
			           invalidate();  
			    }
	            };	
			hfmbtn = new HorizontalFieldManager(Field.FIELD_LEFT);
		//	hfmbtn.setBackground(BackgroundFactory.createLinearGradientBackground(0x0099CCFF,0x0099CCFF,0x00336699,0x00336699));
			hfmbtn.add(MyName2);
			hfmMain.add(hfmbtn);
			
			

		    MyName2 = new LabelField(Municipio, Field.FOCUSABLE){
				public void paint(Graphics graphics){				
					int g = graphics.getColor();
					graphics.setColor(0x000000);
					super.paint(graphics);
				}
				protected void onFocus(int direction) {  
			        super.onFocus(direction);
			        invalidate(); 
			     //   
			    }   
			 
				 protected void onUnfocus() {  
			           super.onUnfocus();
			           invalidate();  
			    }
	            };	
			hfmbtn = new HorizontalFieldManager(Field.FIELD_LEFT);
	//		hfmbtn.setBackground(BackgroundFactory.createLinearGradientBackground(0x0099CCFF,0x0099CCFF,0x00336699,0x00336699));
			hfmbtn.add(MyName2);
			hfmMain.add(hfmbtn);
			
			
			 MyName2 = new LabelField(Estado, Field.FOCUSABLE){
					public void paint(Graphics graphics){				
						int g = graphics.getColor();
						graphics.setColor(0x000000);
						super.paint(graphics);
					}
					protected void onFocus(int direction) {  
				        super.onFocus(direction);
				        invalidate(); 
				     //   
				    }   
				 
					 protected void onUnfocus() {  
				           super.onUnfocus();
				           invalidate();  
				    }
		            };	
				hfmbtn = new HorizontalFieldManager(Field.FIELD_LEFT);
		//		hfmbtn.setBackground(BackgroundFactory.createLinearGradientBackground(0x0099CCFF,0x0099CCFF,0x00336699,0x00336699));
				hfmbtn.add(MyName2);
				hfmMain.add(hfmbtn);
				
				
				//mapa	
				hfmMain.add(new LabelField(""));
				hfmbtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
				hfmbtn.add(new ButtonField("Ver Mapa"){
					protected boolean navigationClick(int status, int time) {
						
						String uno="http://maps.google.com/maps/api/staticmap?center="+Cx+","+Cy+"&zoom=15&size="+Display.getWidth()+"x"+Display.getHeight()+"&sensor=false&markers="+Cx+","+Cy;
					//	
						System.out.println(uno);
						//19.382328,-99.253922&zoom=14&size=512x512&sensor=false&markers=19.382328,-99.253922
						UiApplication.getUiApplication().pushScreen(new UbicacionScreen(uno,Cx,Cy));
						
						
			            
						return true;
						}
					
						
					});
				hfmMain.add(hfmbtn);
				
				
				
				//horarios
				 MyName2 = new LabelField("Horario", Field.NON_FOCUSABLE){
						public void paint(Graphics graphics){				
							int g = graphics.getColor();
							graphics.setColor(0x000000);
							super.paint(graphics);
						}
						protected void onFocus(int direction) {  
					        super.onFocus(direction);
					        invalidate(); 
					     //   
					    }   
					 
						 protected void onUnfocus() {  
					           super.onUnfocus();
					           invalidate();  
					    }
			            };	
					hfmbtn = new HorizontalFieldManager(Field.FIELD_LEFT);
					  MyName2.setFont(Font.getDefault().derive(Font.BOLD, Font.getDefault().getHeight(), Ui.UNITS_px));
				//	hfmbtn.setBackground(BackgroundFactory.createLinearGradientBackground(0x0099CCFF,0x0099CCFF,0x00336699,0x00336699));
					hfmbtn.add(MyName2);
					hfmMain.add(hfmbtn);
			
					
					   MyName2 = new LabelField("Lun: "+Lunes, Field.FOCUSABLE){
							public void paint(Graphics graphics){				
								int g = graphics.getColor();
								graphics.setColor(0x000000);
								super.paint(graphics);
							}
							protected void onFocus(int direction) {  
						        super.onFocus(direction);
						        invalidate(); 
						     //   
						    }   
						 
							 protected void onUnfocus() {  
						           super.onUnfocus();
						           invalidate();  
						    }
				            };	
						hfmbtn = new HorizontalFieldManager(Field.FIELD_LEFT);
				//		hfmbtn.setBackground(BackgroundFactory.createLinearGradientBackground(0x0099CCFF,0x0099CCFF,0x00336699,0x00336699));
						hfmbtn.add(MyName2);
						hfmMain.add(hfmbtn);
						
						   MyName2 = new LabelField("Mar: "+Martes, Field.FOCUSABLE){
								public void paint(Graphics graphics){				
									int g = graphics.getColor();
									graphics.setColor(0x000000);
									super.paint(graphics);
								}
								protected void onFocus(int direction) {  
							        super.onFocus(direction);
							        invalidate(); 
							     //   
							    }   
							 
								 protected void onUnfocus() {  
							           super.onUnfocus();
							           invalidate();  
							    }
					            };	
							hfmbtn = new HorizontalFieldManager(Field.FIELD_LEFT);
				//			hfmbtn.setBackground(BackgroundFactory.createLinearGradientBackground(0x0099CCFF,0x0099CCFF,0x00336699,0x00336699));
							hfmbtn.add(MyName2);
							hfmMain.add(hfmbtn);
							 MyName2 = new LabelField("Mie: "+Miercoles, Field.FOCUSABLE){
									public void paint(Graphics graphics){				
										int g = graphics.getColor();
										graphics.setColor(0x000000);
										super.paint(graphics);
									}
									protected void onFocus(int direction) {  
								        super.onFocus(direction);
								        invalidate(); 
								     //   
								    }   
								 
									 protected void onUnfocus() {  
								           super.onUnfocus();
								           invalidate();  
								    }
						            };	
								hfmbtn = new HorizontalFieldManager(Field.FIELD_LEFT);
				//				hfmbtn.setBackground(BackgroundFactory.createLinearGradientBackground(0x0099CCFF,0x0099CCFF,0x00336699,0x00336699));
								hfmbtn.add(MyName2);
								hfmMain.add(hfmbtn);
								 MyName2 = new LabelField("Jue: "+Jueves, Field.FOCUSABLE){
										public void paint(Graphics graphics){				
											int g = graphics.getColor();
											graphics.setColor(0x000000);
											super.paint(graphics);
										}
										protected void onFocus(int direction) {  
									        super.onFocus(direction);
									        invalidate(); 
									     //   
									    }   
									 
										 protected void onUnfocus() {  
									           super.onUnfocus();
									           invalidate();  
									    }
							            };	
									hfmbtn = new HorizontalFieldManager(Field.FIELD_LEFT);
					//				hfmbtn.setBackground(BackgroundFactory.createLinearGradientBackground(0x0099CCFF,0x0099CCFF,0x00336699,0x00336699));
									hfmbtn.add(MyName2);
									hfmMain.add(hfmbtn);
									 MyName2 = new LabelField("Vie: "+Viernes, Field.FOCUSABLE){
											public void paint(Graphics graphics){				
												int g = graphics.getColor();
												graphics.setColor(0x000000);
												super.paint(graphics);
											}
											protected void onFocus(int direction) {  
										        super.onFocus(direction);
										        invalidate(); 
										     //   
										    }   
										 
											 protected void onUnfocus() {  
										           super.onUnfocus();
										           invalidate();  
										    }
								            };	
										hfmbtn = new HorizontalFieldManager(Field.FIELD_LEFT);
						//				hfmbtn.setBackground(BackgroundFactory.createLinearGradientBackground(0x0099CCFF,0x0099CCFF,0x00336699,0x00336699));
										hfmbtn.add(MyName2);
										hfmMain.add(hfmbtn);
										 MyName2 = new LabelField("Sab: "+Sabado, Field.FOCUSABLE){
												public void paint(Graphics graphics){				
													int g = graphics.getColor();
													graphics.setColor(0x000000);
													super.paint(graphics);
												}
												protected void onFocus(int direction) {  
											        super.onFocus(direction);
											        invalidate(); 
											     //   
											    }   
											 
												 protected void onUnfocus() {  
											           super.onUnfocus();
											           invalidate();  
											    }
									            };	
											hfmbtn = new HorizontalFieldManager(Field.FIELD_LEFT);
							//				hfmbtn.setBackground(BackgroundFactory.createLinearGradientBackground(0x0099CCFF,0x0099CCFF,0x00336699,0x00336699));
											hfmbtn.add(MyName2);
											hfmMain.add(hfmbtn);
											MyName2 = new LabelField("Dom: "+Domingo, Field.FOCUSABLE){
												public void paint(Graphics graphics){				
													int g = graphics.getColor();
													graphics.setColor(0x000000);
													super.paint(graphics);
												}
												protected void onFocus(int direction) {  
											        super.onFocus(direction);
											        invalidate(); 
											     //   
											    }   
											 
												 protected void onUnfocus() {  
											           super.onUnfocus();
											           invalidate();  
											    }
									            };	
											hfmbtn = new HorizontalFieldManager(Field.FIELD_LEFT);
								//			hfmbtn.setBackground(BackgroundFactory.createLinearGradientBackground(0x0099CCFF,0x0099CCFF,0x00336699,0x00336699));
											hfmbtn.add(MyName2);
											hfmMain.add(hfmbtn);
											
											
											
											//contacto
											
											
											MyName = new LabelField("Tel�fono(s): ",Field.NON_FOCUSABLE){
												public void paint(Graphics graphics){				
													int g = graphics.getColor();
													graphics.setColor(0x000000);
													super.paint(graphics);
												}
												protected void onFocus(int direction) {  
											        super.onFocus(direction);
											        invalidate(); 
											     //   
											    }   
											 
												 protected void onUnfocus() {  
											           super.onUnfocus();
											           invalidate();  
											    }
									            };	
										    MyName.setFont(Font.getDefault().derive(Font.BOLD, Font.getDefault().getHeight(), Ui.UNITS_px));
										    hfmbtn = new HorizontalFieldManager(Field.FIELD_LEFT);
										    hfmMain.add(MyName);
											hfmMain.add(new LabelField(""));
											
											
										    MyName2 = new LabelField(Tel, Field.FOCUSABLE){
												public void paint(Graphics graphics){				
													int g = graphics.getColor();
													graphics.setColor(0x000000);
													super.paint(graphics);
												}
												protected void onFocus(int direction) {  
											        super.onFocus(direction);
											        invalidate(); 
											     //   
											    }   
											 
												 protected void onUnfocus() {  
											           super.onUnfocus();
											           invalidate();  
											    }
												 
												 protected boolean navigationClick(int status, int time) {
														// TODO Auto-generated method stub
													 makeCall(Tel);
														return true;
													}
													
													
													
									            };	
											hfmbtn = new HorizontalFieldManager(Field.FIELD_LEFT);
										//	hfmbtn.setBackground(BackgroundFactory.createLinearGradientBackground(0x0099CCFF,0x0099CCFF,0x00336699,0x00336699));
											hfmbtn.add(MyName2);
											hfmMain.add(hfmbtn);
				
											
											MyName2 = new LabelField(Tel2, Field.FOCUSABLE){
												public void paint(Graphics graphics){				
													int g = graphics.getColor();
													graphics.setColor(0x000000);
													super.paint(graphics);
												}
												protected void onFocus(int direction) {  
											        super.onFocus(direction);
											        invalidate(); 
											     //   
											    }   
											 
												 protected void onUnfocus() {  
											           super.onUnfocus();
											           invalidate();  
											    }
												 
												 protected boolean navigationClick(int status, int time) {
														// TODO Auto-generated method stub
													 makeCall(Tel2);
														return true;
													}
													
													
												
									            };	
											hfmbtn = new HorizontalFieldManager(Field.FIELD_LEFT);
										//	hfmbtn.setBackground(BackgroundFactory.createLinearGradientBackground(0x0099CCFF,0x0099CCFF,0x00336699,0x00336699));
											hfmbtn.add(MyName2);
											hfmMain.add(hfmbtn);
			
		
			bgManager.add(hfmMain);
		add(bgManager);
	}

	public void close() {
		// TODO Auto-generated method stub
		UiApplication.getUiApplication().popScreen(this);
	}
	protected boolean onSavePrompt() 
	{
		return true;
	}
	public void makeCall(String data){
        String phoneNumber = data;
        if ( phoneNumber.length() == 0 ){
            System.out.println("Error,  not a phone number");
        }
        else {
            PhoneArguments call = new PhoneArguments (PhoneArguments.ARG_CALL,phoneNumber);
            Invoke.invokeApplication(Invoke.APP_TYPE_PHONE, call);
        }   
}
	
	

	
	
}

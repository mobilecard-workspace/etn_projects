package mypackage;

import java.io.InputStream;
import java.util.TimerTask;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import mx.com.ib.utils.Utils;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;
import net.rim.device.api.system.KeyListener;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.util.DataBuffer;
import Controls.BgManager;
import app.MainClass;
import beans.Banner;

public class UbicacionScreen extends MainScreen 
{   	
	VerticalFieldManager _fieldManagerMiddle;
	public Bitmap b=null;
	BitmapField _bitmap;
	Bitmap _DAFLogo;
	Banner version;
	public BgManager bgManager = null;	
	private BitmapField thumbnailField;
	String URL="";
	
	String cx, cy=null;
	
	public UbicacionScreen(String url,String cx,String cy)
	{   super();      
		this.URL=url;
		this.cx=cx;
		this.cy=cy;
		System.gc();
		bgManager = new BgManager(0xffffff);
		_fieldManagerMiddle = new VerticalFieldManager(Manager.VERTICAL_SCROLL | Field.USE_ALL_WIDTH | Field.FIELD_HCENTER);

		
        thumbnailField = new BitmapField() {};
       
        _fieldManagerMiddle.add(thumbnailField);
   
        
        
		UiApplication.getUiApplication().invokeLater(new Runnable() {
            public void run() {
          	 
            	 MainClass.splashScreen.start();
               	 setThumbnail2(cargaIMG(URL));
            
            }
        });	
       
   
    	
	
        bgManager.add(_fieldManagerMiddle);
		add(bgManager);
		
		 addMenuItem(_viewItem);
		 addMenuItem(_viewItem2);
		 addMenuItem(_viewItem3);
		 addMenuItem(_viewItem4);
		
	}   
	
	public void dismiss() 
	
	{
		
		
		 System.gc();
	}   
	
	private class CountDown extends TimerTask 
	{      
		public void run() 
		{         
			DismissThread dThread = new DismissThread();         
			UiApplication.getUiApplication().invokeLater(dThread);
		}   
	}   
	
	private class DismissThread implements Runnable 
	{      
		public void run() 
		{         
			dismiss();      
		}   
	}   
	
	protected boolean navigationClick(int status, int time) 
	{     
		dismiss();      
		return true;   
	}   
	
	protected boolean navigationUnclick(int status, int time) 
	{      
		return false;   
	}   
	
	protected boolean navigationMovement(int dx, int dy, int status, int time) 
	{      
		return false;   
	}   
	
	
	 
	
	
	public  synchronized Bitmap cargaIMG(final String path){
		Utils.checkConnectionType();
		

				
				if(MainClass.idealConnection!=null){
					try{
											HttpConnection conn = (HttpConnection) Connector.open(path + ";"
													+ MainClass.idealConnection);
											
											int progress = 0;
											InputStream in = null;
											byte[] data = new byte[256];
											try {
												in = conn.openInputStream();
												DataBuffer db = new DataBuffer();
												int chunk = 0;
												while (-1 != (chunk = in.read(data))) {
													progress += chunk;
													db.write(data, 0, chunk);
												}
												in.close();
												data = db.getArray();
											} catch (Exception e) {	}
											
											
											if(data != null){
												System.gc();
												 b = Bitmap.createBitmapFromBytes(data, 0, data.length, 1);
													System.gc();
												UiApplication.getUiApplication().invokeLater(new Runnable() {
							                          public void run() {
							                        	 
							                        		MainClass.mainClass.repaint();
							                          
							                          }
							                      });	
							                     
												
												
												
												
//											 	
											
											}
										}catch (Exception e) {
											//TODO: handle exception
										}}
	
	return b;	
	}
	 public void setThumbnail2(final Bitmap image) {
	    	
	        if(thumbnailField != null) {
	            UiApplication.getUiApplication().invokeLater(new Runnable() {
	                public void run() {
	                 
	                	if(image != null){
	                		Bitmap img=null;
	                		
	                		thumbnailField.setBitmap(image);
	                		MainClass.splashScreen.remove();
	                		
	                		   
	                	}
	                	
	                }
	            });
	        }else{
	        	
	        }
	    }
	 private MenuItem _viewItem = new MenuItem("Zoom +", 110, 10)
	 {
	     public void run()
	     {
	    		
				//	
				System.gc();
					if(MainClass.zoom<25){
						MainClass.zoom=MainClass.zoom+1;
						String uno="http://maps.google.com/maps/api/staticmap?center="+cx+","+cy+"&zoom="+MainClass.zoom+"&size="+Display.getWidth()+"x"+Display.getHeight()+"&sensor=false&markers="+cx+","+cy+"&maptype="+MainClass.MODO_MAPA;
						UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
						UiApplication.getUiApplication().pushScreen(new UbicacionScreen(uno,cx,cy));
					}else{
						Dialog.alert("Demasiado zoom");
					}
					
					
	     }
	 };
	 private MenuItem _viewItem2 = new MenuItem("Zoom -", 110, 10)
	 {
	     public void run()
	     {
	    		System.gc();
	    	 if(MainClass.zoom>1){
	 			MainClass.zoom=MainClass.zoom+-1;
					String uno="http://maps.google.com/maps/api/staticmap?center="+cx+","+cy+"&zoom="+MainClass.zoom+"&size="+Display.getWidth()+"x"+Display.getHeight()+"&sensor=false&markers="+cx+","+cy+"&maptype="+MainClass.MODO_MAPA;
					UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
					UiApplication.getUiApplication().pushScreen(new UbicacionScreen(uno,cx,cy));
				}else{
					Dialog.alert("No se puedo menos zoom");
				}
	     }
	 };//satellite
	 
	 private MenuItem _viewItem3 = new MenuItem("Modo Sat�lite", 110, 10)
	 {
	     public void run()
	     {
	    		System.gc();
	    		
	    		
	    		MainClass.MODO_MAPA="satellite";
					String uno="http://maps.google.com/maps/api/staticmap?center="+cx+","+cy+"&zoom="+MainClass.zoom+"&size="+Display.getWidth()+"x"+Display.getHeight()+"&sensor=false&markers="+cx+","+cy+"&maptype="+MainClass.MODO_MAPA;
					UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
					UiApplication.getUiApplication().pushScreen(new UbicacionScreen(uno,cx,cy));
				
	     }
	 };//satellite
	 private MenuItem _viewItem4 = new MenuItem("Modo Cl�sico", 110, 10)
	 {
	     public void run()
	     {
	    		System.gc();
	    		MainClass.MODO_MAPA="roadmap";
					String uno="http://maps.google.com/maps/api/staticmap?center="+cx+","+cy+"&zoom="+MainClass.zoom+"&size="+Display.getWidth()+"x"+Display.getHeight()+"&sensor=false&markers="+cx+","+cy+"&maptype="+MainClass.MODO_MAPA;
					UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
					UiApplication.getUiApplication().pushScreen(new UbicacionScreen(uno,cx,cy));
				
	     }
	 };
}
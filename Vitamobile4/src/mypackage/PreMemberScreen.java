package mypackage;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import Controls.BannerBitmap;
import Controls.BgManager;
import app.MainClass;

public class PreMemberScreen extends MainScreen {
	public VerticalFieldManager hfmMain = null;
	public LabelField MyName= null;
	public HorizontalFieldManager hfmbtn = null;
	public BgManager bgManager = null;
	public int x=0;
	public PreMemberScreen(){
		
			bgManager = new BgManager(MainClass.BG_MENU);
			
		hfmMain = new VerticalFieldManager(Field.FIELD_HCENTER|VERTICAL_SCROLL|Field.USE_ALL_WIDTH);
		  hfmbtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		   MyName = new LabelField("Membresías: ", Field.NON_FOCUSABLE){
			   public void paint(Graphics graphics){				
					int g = graphics.getColor();
					graphics.setColor(0x000000);
					super.paint(graphics);
				} 
		   };	
		
		add(new NullField());
		setBanner(new BannerBitmap(MainClass.SUB_HEADER));
		
		hfmbtn.add(MyName);
		MyName.setFont(Font.getDefault().derive(Font.BOLD, Font.getDefault().getHeight(), Ui.UNITS_px));
		hfmMain.add(hfmbtn);
		hfmMain.add(new LabelField());
		
		  MyName = new LabelField("Titular: ", Field.NON_FOCUSABLE){
			  public void paint(Graphics graphics){				
					int g = graphics.getColor();
					graphics.setColor(MainClass.BG_GRAY);
					super.paint(graphics);
				} 
			 
			  
		  };
		  hfmMain.add(MyName);
		
			hfmMain.add(new LabelField(MainClass.Membresia.getName().toUpperCase().trim()+" "+MainClass.Membresia.getLastname().toUpperCase().trim(),Field.FOCUSABLE){
				
				  public void paint(Graphics graphics){				
						int g = graphics.getColor();
						graphics.setColor(0x3333FF);
						super.paint(graphics);
					} 
				  
				  protected void onFocus(int direction) {  
				        super.onFocus(direction);
				        invalidate(); 
				     //   
				    }   
				 
					 protected void onUnfocus() {  
				           super.onUnfocus();
				           invalidate();  
				    }
				  protected boolean navigationClick(int status, int time) {
						// TODO Auto-generated method stub
					 
					  UiApplication.getUiApplication().pushScreen(new MemberScreen());
						return true;
					}
					
					
					
				
			});
		  
			
			for(x=0;x<MainClass.Benes.length;x++){
				
				  MyName = new LabelField("Beneficiario: ", Field.NON_FOCUSABLE){
					  public void paint(Graphics graphics){				
							int g = graphics.getColor();
							graphics.setColor(MainClass.BG_GRAY);
							super.paint(graphics);
						} 
					  
				  };
				  hfmMain.add(MyName);
				
					hfmMain.add(new LabelField(MainClass.Benes[x].getName().toUpperCase().trim()+" "+MainClass.Benes[x].getLastname().toUpperCase().trim(),Field.FOCUSABLE){
						 final int yy=x;
						  public void paint(Graphics graphics){				
								int g = graphics.getColor();
								graphics.setColor(0x3333FF);
								super.paint(graphics);
							} 
						  
						  protected void onFocus(int direction) {  
						        super.onFocus(direction);
						        invalidate(); 
						     //   
						    }   
						 
							 protected void onUnfocus() {  
						           super.onUnfocus();
						           invalidate();  
						    }
						  protected boolean navigationClick(int status, int time) {
								// TODO Auto-generated method stub
							
							  UiApplication.getUiApplication().pushScreen(new MemberScreen(yy));
								return true;
							}
							
							
						
						
					});
				
			}
		  
		  
		bgManager.add(hfmMain);
		add(bgManager);
		
	}
	
}
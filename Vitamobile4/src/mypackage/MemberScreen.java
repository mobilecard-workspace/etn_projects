package mypackage;

import mx.com.ib.utils.Utils;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.FlowFieldManager;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import Controls.BgManager;
import Controls.CustomVerticalFieldManager;
import Controls.GenericImageButtonField;
import Controls.TitleSectionField;
import app.MainClass;
import beans.Publicidad;

public class MemberScreen extends MainScreen{

	
	public BgManager bgManager = null;	
	public LabelField code,name,lastname,date,espacio,espacio2,member,membresia,vence,fecha,Nombre = null;	
	public CustomVerticalFieldManager vfmFooter,customVerticalFieldManager = null;	
	public Publicidad categoria = null;

	public FlowFieldManager grid;
	public Bitmap b=null;
	private BitmapField imageField=null;
	public VerticalFieldManager vfmInternalConsults = null;
	public HorizontalFieldManager hfm,hfm2,hfm3 =null;
	public Bitmap bitmap;
	public GenericImageButtonField btnface,btntw = null;
	 private TitleSectionField titleSectionField;
	 private int FOCUS_BG_COLOR = Color.RED;
	 private boolean IS_FOCUS;
	    private int NON_FOCUS_BG_COLOR = Color.SNOW;
	    private BitmapField thumbnailField;
	    private  VerticalFieldManager h=null;
	    Bitmap backgroundBitmap=null;
	    private int miembro=0;
	public MemberScreen(){
	
		
		initVariables();
		
		addFields();
		
		
	
	}
	
public MemberScreen(int men){
	
		this.miembro=men;
		initVariables2();
		
		addFields();
		
		
	
	}
public void initVariables2(){
   
 //   engine.setAcceptableDirections(Display.DIRECTION_EAST);
    

	
	
	//este si lo rota
	
	

	


    h = new VerticalFieldManager(VerticalFieldManager.NO_HORIZONTAL_SCROLL|Field.FOCUSABLE|VerticalFieldManager.VERTICAL_SCROLL|VerticalFieldManager.FIELD_HCENTER);
    bgManager = new BgManager(0xfffffff);
 
    String nombre=MainClass.Benes[miembro].getName().toUpperCase().trim();
    String apes=MainClass.Benes[miembro].getLastname().toUpperCase().trim();
    String mem=MainClass.Benes[miembro].getMember().toUpperCase().trim();
    
   
	code = new LabelField(MainClass.Benes[miembro].getCode(), Field.USE_ALL_WIDTH | LabelField.HCENTER|LabelField.ELLIPSIS){
		
		public void paint(Graphics graphics){				
			int g = graphics.getColor();
			graphics.setColor(0x000000);
			super.paint(graphics);
			super.setPosition(LabelField.LEFT);
			
		}
		


	
	};
	code.setFont(MainClass.appFont1);

	
	espacio = new LabelField("", Field.USE_ALL_WIDTH | LabelField.HCENTER){
		
		public void paint(Graphics graphics){				
			int g = graphics.getColor();
			graphics.setColor(0x000000);
			super.paint(graphics);
		}
	};
	espacio.setFont(MainClass.appFont1);
	espacio2 = new LabelField("", Field.USE_ALL_WIDTH | LabelField.HCENTER){
		
		public void paint(Graphics graphics){				
			int g = graphics.getColor();
			graphics.setColor(0x000000);
			super.paint(graphics);
		}
	};
	espacio2.setFont(MainClass.appFont1);
	Nombre = new LabelField(""+nombre,  LabelField.LEFT){
		
		public void paint(Graphics graphics){				
			int g = graphics.getColor();
			graphics.setColor(0x000000);
			super.paint(graphics);
		}
	};
	Nombre.setFont(MainClass.appFont1);
	if(Display.getWidth()==320){
		Nombre.setMargin(6,4,4,4);	
	}
	if(Display.getWidth()==480){
		Nombre.setMargin(8,4,4,4);	
	}
	if(Display.getWidth()==640){
		Nombre.setMargin(9,4,4,4);	
	}
	 if(Display.getWidth()==360&&Display.getHeight()==480){
		 Nombre.setMargin(8,4,4,4);	
	 }
	
	lastname = new LabelField(""+apes,  LabelField.LEFT){
		
		public void paint(Graphics graphics){				
			int g = graphics.getColor();
			graphics.setColor(0x000000);
			super.paint(graphics);
		}
	};
	lastname.setFont(MainClass.appFont1);
	lastname.setMargin(4,4,4,4);
	
	name = new LabelField("Socio: ", LabelField.LEFT){
		
		public void paint(Graphics graphics){				
			int g = graphics.getColor();
			graphics.setColor(0x000000);
			super.paint(graphics);
		}
	};
	name.setFont(MainClass.appFont2);
	name.setFont(Font.getDefault().derive(Font.BOLD, Font.getDefault().getHeight()-4, Ui.UNITS_px));
	name.setMargin(4,4,4,4);
	
	
	member = new LabelField("No. Membresia:", LabelField.LEFT){
		
		public void paint(Graphics graphics){				
			int g = graphics.getColor();
			graphics.setColor(0x000000);
			super.paint(graphics);
		}
	};
	member.setFont(MainClass.appFont1);
	member.setFont(Font.getDefault().derive(Font.BOLD, Font.getDefault().getHeight()-5, Ui.UNITS_px));
	member.setMargin(4,4,4,4);
	
	
	membresia = new LabelField(mem,  LabelField.LEFT){
		
		public void paint(Graphics graphics){				
			int g = graphics.getColor();
			graphics.setColor(0x000000);
			super.paint(graphics);
		}
	};
	membresia.setFont(MainClass.appFont1);
	
	
	if(Display.getWidth()==320){
		membresia.setMargin(5,4,4,4);	
	}
	if(Display.getWidth()==480){
		membresia.setMargin(8,4,4,4);	
	}
	if(Display.getWidth()==640){
		membresia.setMargin(9,4,4,4);	
	}
	if(Display.getWidth()==360&&Display.getHeight()==480){
		membresia.setMargin(7,4,4,4);	
	 }
	
	
	
	
	vence = new LabelField("Vence: ", LabelField.LEFT){
		
		public void paint(Graphics graphics){				
			int g = graphics.getColor();
			graphics.setColor(0x000000);
			super.paint(graphics);
		}
	};
	vence.setFont(MainClass.appFont1);
	vence.setFont(Font.getDefault().derive(Font.BOLD, Font.getDefault().getHeight()-4, Ui.UNITS_px));
	vence.setMargin(4,4,4,4);
	
	date = new LabelField(MainClass.Membresia.getDate().toUpperCase(),  LabelField.LEFT){
		
		public void paint(Graphics graphics){				
			int g = graphics.getColor();
			graphics.setColor(0x000000);
			super.paint(graphics);
		}
	};
	date.setFont(MainClass.appFont1);
	
	
	if(Display.getWidth()==320){
		date.setMargin(6,4,4,4);
	}
	if(Display.getWidth()==480){
		date.setMargin(8,4,4,4);
	}
	if(Display.getWidth()==640){
		date.setMargin(9,4,4,4);	
	}
	if(Display.getWidth()==360&&Display.getHeight()==480){
		date.setMargin(8,4,4,4);	
	 }
	

	customVerticalFieldManager = new CustomVerticalFieldManager((Display.getWidth()/2)+5,Display.getHeight(), HorizontalFieldManager.USE_ALL_WIDTH|Field.FIELD_VCENTER);
	grid = new FlowFieldManager( Field.USE_ALL_WIDTH);		
	vfmFooter = new CustomVerticalFieldManager(Display.getWidth(),
			Display.getHeight(), 
			HorizontalFieldManager.FIELD_LEFT | VERTICAL_SCROLL);
	 hfm = new HorizontalFieldManager(); 
	 hfm2 = new HorizontalFieldManager(); 
	 hfm3 = new HorizontalFieldManager(); 
	
	
}
	
	public void initVariables(){
	  
	    

		
		
		//este si lo rota
		
	

	
	    h = new VerticalFieldManager(VerticalFieldManager.NO_HORIZONTAL_SCROLL|Field.FOCUSABLE|VerticalFieldManager.VERTICAL_SCROLL|VerticalFieldManager.FIELD_HCENTER);
	    bgManager = new BgManager(0xfffffff);
	 
	    String nombre=MainClass.Membresia.getName().toUpperCase();
	    String apes=MainClass.Membresia.getLastname().toUpperCase();
	    String mem=MainClass.Membresia.getMember().toUpperCase();
	    
	   
		code = new LabelField(MainClass.Membresia.getCode(), Field.USE_ALL_WIDTH | LabelField.HCENTER|LabelField.ELLIPSIS){
			
			public void paint(Graphics graphics){				
				int g = graphics.getColor();
				graphics.setColor(0x000000);
				super.paint(graphics);
				super.setPosition(LabelField.LEFT);
				
			}
			
	

		
		};
		code.setFont(MainClass.appFont1);
	
		
		espacio = new LabelField("", Field.USE_ALL_WIDTH | LabelField.HCENTER){
			
			public void paint(Graphics graphics){				
				int g = graphics.getColor();
				graphics.setColor(0x000000);
				super.paint(graphics);
			}
		};
		espacio.setFont(MainClass.appFont1);
		espacio2 = new LabelField("", Field.USE_ALL_WIDTH | LabelField.HCENTER){
			
			public void paint(Graphics graphics){				
				int g = graphics.getColor();
				graphics.setColor(0x000000);
				super.paint(graphics);
			}
		};
		espacio2.setFont(MainClass.appFont1);
		Nombre = new LabelField(""+nombre,  LabelField.LEFT){
			
			public void paint(Graphics graphics){				
				int g = graphics.getColor();
				graphics.setColor(0x000000);
				super.paint(graphics);
			}
		};
		Nombre.setFont(MainClass.appFont1);
		if(Display.getWidth()==320){
			Nombre.setMargin(6,4,4,4);	
		}
		if(Display.getWidth()==480){
			Nombre.setMargin(8,4,4,4);	
		}
		 if(Display.getWidth()==360&&Display.getHeight()==480){
			 Nombre.setMargin(8,4,4,4);	
		 }
		
		lastname = new LabelField(""+apes,  LabelField.LEFT){
			
			public void paint(Graphics graphics){				
				int g = graphics.getColor();
				graphics.setColor(0x000000);
				super.paint(graphics);
			}
		};
		lastname.setFont(MainClass.appFont1);
		lastname.setMargin(4,4,4,4);
		
		name = new LabelField("Socio: ", LabelField.LEFT){
			
			public void paint(Graphics graphics){				
				int g = graphics.getColor();
				graphics.setColor(0x000000);
				super.paint(graphics);
			}
		};
		name.setFont(MainClass.appFont2);
		name.setFont(Font.getDefault().derive(Font.BOLD, Font.getDefault().getHeight()-4, Ui.UNITS_px));
		name.setMargin(4,4,4,4);
		
		
		member = new LabelField("No. Membresia:", LabelField.LEFT){
			
			public void paint(Graphics graphics){				
				int g = graphics.getColor();
				graphics.setColor(0x000000);
				super.paint(graphics);
			}
		};
		member.setFont(MainClass.appFont1);
		member.setFont(Font.getDefault().derive(Font.BOLD, Font.getDefault().getHeight()-5, Ui.UNITS_px));
		member.setMargin(4,4,4,4);
		
		
		membresia = new LabelField(mem,  LabelField.LEFT){
			
			public void paint(Graphics graphics){				
				int g = graphics.getColor();
				graphics.setColor(0x000000);
				super.paint(graphics);
			}
		};
		membresia.setFont(MainClass.appFont1);
		
		
		if(Display.getWidth()==320){
			membresia.setMargin(5,4,4,4);	
		}
		if(Display.getWidth()==480){
			membresia.setMargin(8,4,4,4);	
		}
		if(Display.getWidth()==360&&Display.getHeight()==480){
			membresia.setMargin(7,4,4,4);	
		 }
		
		
		
		
		vence = new LabelField("Vence: ", LabelField.LEFT){
			
			public void paint(Graphics graphics){				
				int g = graphics.getColor();
				graphics.setColor(0x000000);
				super.paint(graphics);
			}
		};
		vence.setFont(MainClass.appFont1);
		vence.setFont(Font.getDefault().derive(Font.BOLD, Font.getDefault().getHeight()-4, Ui.UNITS_px));
		vence.setMargin(4,4,4,4);
		
		date = new LabelField(MainClass.Membresia.getDate().toUpperCase(),  LabelField.LEFT){
			
			public void paint(Graphics graphics){				
				int g = graphics.getColor();
				graphics.setColor(0x000000);
				super.paint(graphics);
			}
		};
		date.setFont(MainClass.appFont1);
		
		
		if(Display.getWidth()==320){
			date.setMargin(6,4,4,4);
		}
		if(Display.getWidth()==480){
			date.setMargin(8,4,4,4);
		}
		if(Display.getWidth()==360&&Display.getHeight()==480){
			date.setMargin(8,4,4,4);	
		 }
		
	
		customVerticalFieldManager = new CustomVerticalFieldManager((Display.getWidth()/2)+5,Display.getHeight(), HorizontalFieldManager.USE_ALL_WIDTH|Field.FIELD_VCENTER);
		grid = new FlowFieldManager( Field.USE_ALL_WIDTH);		
		vfmFooter = new CustomVerticalFieldManager(Display.getWidth(),
				Display.getHeight(), 
				HorizontalFieldManager.FIELD_LEFT | VERTICAL_SCROLL);
		 hfm = new HorizontalFieldManager(); 
		 hfm2 = new HorizontalFieldManager(); 
		 hfm3 = new HorizontalFieldManager(); 
		
		
	}
	
	
public void addFields(){
	
		
	
		add(new NullField());			
		Bitmap temp=null;
		/*if(Display.getWidth()<Display.getHeight()){
			temp = Bitmap.getBitmapResource("memberh.png");
		}else{
			temp = Bitmap.getBitmapResource("member.png");
			
		}*/
		 
		temp = Bitmap.getBitmapResource("member.png");
		if(Display.getWidth()<Display.getHeight()){
			backgroundBitmap=Utils.resizeBitmap2(temp, Display.getWidth(),Display.getHeight()-(Display.getHeight()/6));
	}else{
	 backgroundBitmap=Utils.resizeBitmap2(temp, Display.getWidth(),Display.getHeight());
		
	}
      //  final Bitmap backgroundBitmap=Utils.resizeBitmap2(temp, Display.getWidth(),Display.getHeight());
        //para el caso de que el alto sea mayor al ancho
        HorizontalFieldManager horizontalFieldManager = 
            new HorizontalFieldManager(
                HorizontalFieldManager.USE_ALL_WIDTH | 
                HorizontalFieldManager.USE_ALL_HEIGHT){

                //Override the paint method to draw the background image.
                public void paint(Graphics graphics) {
                    //Draw the background image and then call paint.
                    graphics.drawBitmap(0, 0,  Display.getWidth(), Display.getHeight(), backgroundBitmap, 0, 0);
                    super.paint(graphics);
                }            

            };
            if(Display.getWidth()==360&&Display.getHeight()==480){
            	  code.setMargin((Display.getHeight()/2)-Display.getHeight()/8, 0, 0, 0);
    	    }else{
    	    	
    	    	  code.setMargin((Display.getHeight()/2)-Display.getHeight()/13, 0, 0, 0);	
    	    }
    	  
          
            customVerticalFieldManager.add(code);
            customVerticalFieldManager.add(espacio);
          
           
            
            hfm.add(name);
            hfm.add(Nombre);
            customVerticalFieldManager.add(hfm);
            customVerticalFieldManager.add(lastname);
    
            customVerticalFieldManager.add(espacio2);
            
            
            hfm3.add(member);
            hfm3.add(membresia);
            customVerticalFieldManager.add(hfm3);
       
            hfm2.add(vence);
            hfm2.add(date);
            customVerticalFieldManager.add(hfm2);
          
            horizontalFieldManager.add(customVerticalFieldManager);
        	MainClass.splashScreen.remove();
           
     	
		bgManager.add(horizontalFieldManager);
		
		add(bgManager);
		 System.gc();}
	

protected void sizeChanged(int w, int h) {
	   Dialog.alert("The size of the Canvas has changed");
	}  


/*protected void onUiEngineAttached(boolean attached) {
    super.onUiEngineAttached(attached);
    Ui.getUiEngineInstance().setAcceptableDirections(Display.DIRECTION_LANDSCAPE);
}*/
	




}
package mypackage;

import net.rim.device.api.system.ApplicationDescriptor;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.FlowFieldManager;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import Controls.BannerBitmap;
import Controls.BgManager;
import Controls.BorderBasicEditField;
import Controls.CustomVerticalFieldManager;
import Controls.GenericImageButtonField;
import Controls.ProductLabelField;
import app.MainClass;
import beans.Publicidad;

public class SearchScreen extends MainScreen{

	
	public BgManager bgManager = null;	
	public LabelField products,label = null;	
	public CustomVerticalFieldManager vfmFooter,vfmFooter2 = null;	
	public Publicidad categoria = null;
	
	public FlowFieldManager grid;
	public Bitmap b=null;
	public VerticalFieldManager vfmInternalConsults = null;
	public Bitmap bitmap;
	public BorderBasicEditField text;
	public GenericImageButtonField btnface,btntw = null;	
	ApplicationDescriptor appDesc = ApplicationDescriptor.currentApplicationDescriptor();
	public SearchScreen(){
	
		
		initVariables();
		
		addFields();
		
		
	
	}
	


	
	public void initVariables(){
		int widthManager = Display.getWidth() - 20;
	   
	    text = new BorderBasicEditField("","", 40, widthManager, Field.FOCUSABLE);	
	    bgManager = new BgManager(0x850c10);
		products = new LabelField("Busca tu noticia", Field.USE_ALL_WIDTH | LabelField.HCENTER);
		products.setFont(MainClass.appFont);
		label = new LabelField("Escribe la palabra clave de la noticia que est�s buscando: ", Field.USE_ALL_WIDTH | LabelField.HCENTER);
		label.setFont(MainClass.appFont2);
		//setBitmap(Bitmap.getBitmapResource("tiempoaire.png"));
		grid = new FlowFieldManager( Field.USE_ALL_WIDTH);		
		vfmFooter = new CustomVerticalFieldManager(Display.getWidth()-20,
				Display.getHeight(), 
				HorizontalFieldManager.FIELD_HCENTER | VERTICAL_SCROLL);
		vfmFooter2 = new CustomVerticalFieldManager(Display.getWidth()-80,
				Display.getHeight(), 
				HorizontalFieldManager.FIELD_HCENTER | VERTICAL_SCROLL);
			
		/*HorizontalFieldManager hfm = new HorizontalFieldManager(); 
		setBanner(hfm);*/
		
	}
	
	
public void addFields(){
		add(new NullField());	
		setBanner(new BannerBitmap(MainClass.HEADER));
		vfmFooter.add(products);
		vfmFooter.add(new LabelField());
		vfmFooter.add(label);
		vfmFooter.add(new LabelField());
		vfmFooter2.add(text);
		vfmFooter2.add(new LabelField());		
		vfmFooter2.add(new ButtonField("Buscar"){
		protected boolean navigationClick(int status, int time) {
				
	            
				if(text.getText().equals("")){
					Dialog.inform("Escriba una o varias palabras claves");
				}else if(text.getText().length()<3){
					Dialog.inform("Clave demasiado corta");
				}else{
					ProductLabelField.t=true;
					String banner="clave="+replaceSpace(text.getText());					
			
						
				}
			
            
			return true;
			}
			
		});
		vfmFooter.add(vfmFooter2);
		bgManager.add(vfmFooter);		
		add(bgManager);
		 System.gc();
	}
public boolean onSavePrompt(){
	return true;
}
public String replaceSpace(String original){
	StringBuffer ret = new StringBuffer();
	for(int i=0; i<original.length(); i++){
		String sp = original.charAt(i)+"";
		if(sp.equals(" ")){
			ret.append("%20");
		}
		if(sp.equals("%")){
			ret.append("");
		}
		else
			ret.append(original.charAt(i));
	}
	return ret.toString();
}
}
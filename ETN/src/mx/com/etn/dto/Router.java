package mx.com.etn.dto;

public class Router implements DTO{

	private String key;
	private String description;

	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String toString(){
		return description;
	}
}

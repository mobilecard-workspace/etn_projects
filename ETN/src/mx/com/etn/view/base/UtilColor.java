package mx.com.etn.view.base;

import net.rim.device.api.ui.Color;


public class UtilColor {
	
	private final static int GREEN_01 = 0x90c1c7;
	private final static int GREEN_02 = 0x3db7b0;

	private final static int BLUE = 0x23486d;

	private final static int GREY_00 = Color.WHITE;
	private final static int GREY_01 = 0xebebeb;
	private final static int GREY_02 = 0xd1d7d2;
	private final static int GREY_03 = 0xb4bdb6;
	private final static int GREY_04 = 0x939598;
	private final static int GREY_05 = 0x231f20;

	
	public final static int MAIN_BACKGROUND = GREY_01;

	public final static int TITLE_STRING = GREY_00;
	public final static int TITLE_BACKGROUND = GREEN_01;

	public final static int BUTTON_FOCUS = BLUE;
	public final static int BUTTON_SELECTED = GREEN_01;
	public final static int BUTTON_UNSELECTED = GREY_02;

	public final static int BUTTON_STRING_FOCUS = GREY_01;
	public final static int BUTTON_STRING_SELECTED = GREY_00;
	public final static int BUTTON_STRING_UNSELECTED = GREY_04;

	public final static int SUBTITLE_STRING = GREY_01;
	public final static int SUBTITLE_BACKGROUND = GREEN_01;

	public final static int ELEMENT_STRING = GREY_04;
	public final static int ELEMENT_BACKGROUND = GREY_01;

	public final static int EDIT_TEXT_STRING_DATA = GREY_05;
	public final static int EDIT_TEXT_BACKGROUND_FOCUS = GREY_01;
	public final static int EDIT_TEXT_BACKGROUND_UNFOCUS = GREY_02;

	public final static int LIST_DESCRIPTION_TEXT = BLUE;
	public final static int LIST_DESCRIPTION_DATA = GREY_05;

	public final static int LIST_BACKGROUND_SELECTED = GREEN_01;
	public final static int LIST_BACKGROUND_UNSELECTED = GREY_01;

/*
	public static int BG_GRAY = 0x414042;
	public static int BG_ORANGE = 0xF16430;
	public static int COLOR_LINK = 0x38D4FF;
*/

	public static int toRGB(int r, int g, int b){
		return (r<<16)+(g<<8)+b;		 
	}

	public static int getBlack(){
		return toRGB(30, 30, 30);
	}

	public static int getDarkGray(){
		return toRGB(120, 120, 120);
	}

	public static int getLightGray(){
		return toRGB(200, 200, 200);
	}

	public static int getLightOrange(){
		return toRGB(255, 184, 101);
	}

	public static int getDarkOrange(){
		return toRGB(255, 159, 45);
	}
}

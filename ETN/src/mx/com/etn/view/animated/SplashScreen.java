package mx.com.etn.view.animated;

import net.rim.device.api.system.EncodedImage;
import net.rim.device.api.system.GIFEncodedImage;
import net.rim.device.api.ui.ContextMenu;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Menu;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class SplashScreen extends PopupScreen {

	boolean dosPantallas = false;
	public static String GIF_LOADER = "preloader_b.agif";
	
	private static SplashScreen instance = null;

	private SplashScreen() {
		super(new VerticalFieldManager(), Field.FOCUSABLE);
		init();
	}	
	
	
	public synchronized static SplashScreen getInstance() {
		if (instance == null) {
			synchronized (SplashScreen.class) {
				SplashScreen inst = instance;
				if (inst == null) {
					synchronized (SplashScreen.class) {
						instance = new SplashScreen();
					}
				}
			}
		}
		return instance;
	}


	private void init() {

		EncodedImage image =(EncodedImage) (GIFEncodedImage.getEncodedImageResource(GIF_LOADER));
		
		AnimatedGIFField gif = new AnimatedGIFField((GIFEncodedImage)image);

		add(gif);
	}

	

	public void remove() {
		if (this.isDisplayed()) {
			UiApplication.getUiApplication().popScreen(this);
		}
	}
	

	public void start() {
		dosPantallas = false;
		synchronized (UiApplication.getEventLock()) {
			if (!this.isDisplayed()) {
				UiApplication.getUiApplication().pushScreen(this);
				UiApplication.getUiApplication().repaint();
			}
		}

	}

	
	public void start(boolean _dosPantallas) {
		dosPantallas = _dosPantallas;
		synchronized (UiApplication.getEventLock()) {
			if (!this.isDisplayed()) {
				UiApplication.getUiApplication().pushScreen(this);
				UiApplication.getUiApplication().repaint();
			}
		}

	}

	
	protected boolean keyDown(int in_nKeyCode, int in_nTime) {
		int nKeyPressed = Keypad.key(in_nKeyCode);

		if (nKeyPressed == Keypad.KEY_MENU) {
			this.getMenu(Menu.INSTANCE_DEFAULT).show();
			return true;
		}

		return false;
	}

	
	public boolean onClose() {
		
		if (dosPantallas) {
			UiApplication.getUiApplication().popScreen(this);
			UiApplication.getUiApplication().popScreen(
					UiApplication.getUiApplication().getActiveScreen());
		} else {
			UiApplication.getUiApplication().popScreen(this);
		}
		return true;
	}

	
	private final MenuItem cancelar = new MenuItem("Cancelar conexi�n", 0, 0) {

		public void run() {

			UiApplication.getUiApplication().popScreen(
					UiApplication.getUiApplication().getActiveScreen());
		}
	};

	
	protected void makeMenu(Menu menu, int instance) {

		Field focus = UiApplication.getUiApplication().getActiveScreen()
				.getLeafFieldWithFocus();
		if (focus != null) {
			ContextMenu contextMenu = focus.getContextMenu();
			if (!contextMenu.isEmpty()) {
//				menu.add(contextMenu);
//				menu.addSeparator();
			}
		}
	}
}
package mx.com.etn.view.uicomponents;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.container.HorizontalFieldManager;

public class CustomButtonFieldManager extends HorizontalFieldManager{

	private CustomSelectedButtonField basicFlight;
	private CustomSelectedButtonField roundFlight;
	private int width;
	private int preferredWidth;

	public CustomSelectedButtonField getBasicFlight(){
		return basicFlight;
	}
	
	public CustomSelectedButtonField getRoundFlight(){
		return roundFlight;
	}
	
	public CustomButtonFieldManager(CustomSelectedButtonField basicFlight, CustomSelectedButtonField roundFlight){
		
		super();
		width = Display.getWidth();
		preferredWidth = width/2;
		this.basicFlight = basicFlight;
		this.roundFlight = roundFlight;
	}
	
	public int getPreferredWidth() {
		return preferredWidth;
	}
}

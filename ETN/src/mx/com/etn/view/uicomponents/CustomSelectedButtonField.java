package mx.com.etn.view.uicomponents;


import mx.com.etn.view.base.UtilColor;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;

public class CustomSelectedButtonField extends Field {
	
	private String label;
	/*
	private int foregroundColor;
	private int backgroundColor;	
	private int focusedForegroundColor;
	private int focusedBackgroundColor;
	*/
	private boolean isSelected = false;

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
		invalidate();
	}
	/*
	public CustomSelectedButtonField(String label, int foregroundColor,
			int backgroundColor, int focusedForegroundColor,
			int focusedBackgroundColor, long style) {
		super(style);
		this.label = label;
		this.foregroundColor = foregroundColor;
		this.backgroundColor = backgroundColor;
		this.focusedForegroundColor = focusedForegroundColor;
		this.focusedBackgroundColor = focusedBackgroundColor;
	}

	public CustomSelectedButtonField(String label, int foregroundColor,
			int backgroundColor, long style) {
		super(style);
		this.label = label;
		this.foregroundColor = foregroundColor;
		this.backgroundColor = backgroundColor;
	}
	*/
	public CustomSelectedButtonField(String label, long style) {
		super(style);
		this.label = label;
		//this.foregroundColor = Color.BEIGE;
		//this.backgroundColor = Color.BLUE;
	}
	
	public int getPreferredHeight() {
		return getFont().getHeight() + 8;
	}

	public int getPreferredWidth() {
		return Display.getWidth()/2;
	}

	protected void layout(int width, int height) {
		setExtent(Math.min(width, getPreferredWidth()), Math.min(height, getPreferredHeight()));
	}

	protected void paint(Graphics graphics) {

		
		if (isFocus()){
			graphics.setColor(UtilColor.BUTTON_FOCUS);
			graphics.fillRoundRect(1, 1, getWidth()-2, getHeight()-2, 12, 12);
			graphics.setColor(UtilColor.BUTTON_STRING_FOCUS);
			graphics.drawText(label, 4, 4);
		} else if (isSelected) {
			graphics.setColor(UtilColor.BUTTON_SELECTED);
			graphics.fillRoundRect(1, 1, getWidth()-2, getHeight()-2, 12, 12);
			graphics.setColor(UtilColor.BUTTON_STRING_SELECTED);
			graphics.drawText(label, 4, 4);
		} else {
			graphics.setColor(UtilColor.BUTTON_UNSELECTED);
			graphics.fillRoundRect(1, 1, getWidth()-2, getHeight()-2, 12, 12);
			graphics.setColor(UtilColor.BUTTON_STRING_UNSELECTED);
			graphics.drawText(label, 4, 4);
		}
	}

	public boolean isFocusable() {
		return true;
	}

	
	public boolean isSelectable() {
		return true;
	}


	protected void drawFocus(Graphics graphics, boolean on) {
	}
	
	protected void onFocus(int direction) {
		super.onFocus(direction);
		invalidate();
	}
	
	protected void onUnfocus() {
		super.onUnfocus();
		invalidate();
	}
	
	protected boolean navigationClick(int status, int time) {
		fieldChangeNotify(0);
		isSelected = !isSelected;
		return true;
	}
	
	protected boolean keyChar(char character, int status, int time) {
		if (character == Keypad.KEY_ENTER) {
			fieldChangeNotify(0);
			isSelected = !isSelected;
			return true;
		}
		return super.keyChar(character, status, time);
	}
}

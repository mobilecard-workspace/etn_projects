package mx.com.etn.view;

import java.util.Vector;

import mx.com.etn.controller.Controller;
import mx.com.etn.dto.Router;
import mx.com.etn.model.Error;
import mx.com.etn.model.dataaccess.DataAccessible;
import mx.com.etn.view.animated.SplashScreen;
import mx.com.etn.view.base.UtilColor;
import mx.com.etn.view.base.UtilIcon;
import mx.com.etn.view.base.Viewable;
import mx.com.etn.view.uicomponents.AnuncioLabelField;
import mx.com.etn.view.uicomponents.CustomButtonFieldManager;
import mx.com.etn.view.uicomponents.CustomSelectedButtonField;
import mx.com.etn.view.uicomponents.ElementLabelField;
import mx.com.etn.view.uicomponents.SubtitleRichTextField;
import net.rim.blackberry.api.browser.URLEncodedPostData;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.DateField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NumericChoiceField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

public class ViewETNBuy extends Viewable implements
FieldChangeListener{
	
	private CustomSelectedButtonField createRegistrer = null;
	private CustomSelectedButtonField getInfoBoletos = null;
	
	private CustomSelectedButtonField basicFlight = null;
	private CustomSelectedButtonField roundFlight = null;

	private ObjectChoiceField choiceFieldDepart = null;
	private ObjectChoiceField choiceFieldArrive = null;

	private DateField dateFieldDepart = null;
	private DateField dateFieldArrive = null;

	private NumericChoiceField choiceFieldAdult = null;
	private NumericChoiceField choiceFieldChild = null;
	private NumericChoiceField choiceFieldElderly = null;
	private NumericChoiceField choiceFieldStudent = null;
	private NumericChoiceField choiceFieldProfessor = null;

	private Bitmap encodedImage = null;
	private ButtonField ok = null;

	private boolean isBasicFlight = false;
	private boolean isFirst = true;

	private String depart = null;
	private String arrive = null;

	private String idDepart = null;
	private String idArrive = null;
	
	private long dateDepart = 0;
	private long dateArrive = 0;

	private int iDepart = 0;
	private int iArrive = 0;

	private int iAdults = 0;
	private int iChildren = 0;
	private int iElderlies = 0;
	private int iStudents = 0;
	private int iProfessors = 0;

	private Vector vectorDepart = null;
	private String arrayDepart[] = null;

	private Vector vectorArrive = null;
	private String arrayArrive[] = null;

	
	SplashScreen splashScreen = null;
	
	
	private CustomButtonFieldManager customFieldManager = null;
	private HorizontalFieldManager customFieldManager01 = null;

	private boolean showSplash = false;

	public ViewETNBuy(boolean isSetTitle, String title) {
		super(isSetTitle, title);

		configurationFlightView(true);
	}

	
	private void loadArriveData() {
		
		if (showSplash){
			splashScreen.start();
		}

		
		choiceFieldArrive.setChoices(null);

		int index = choiceFieldDepart.getSelectedIndex();

		Router router = (Router) choiceFieldDepart.getChoice(index);
		
		Controller.execute(Controller.OBTENER_REGRESO, this, router);
	}
	
	public void fieldChanged(Field field, int context) {
		
		if (field == (ObjectChoiceField) choiceFieldDepart) {

			if (isFirst) {
				isFirst = false;
				loadArriveData();
			} else {

				if (ObjectChoiceField.CONTEXT_CHANGE_OPTION == context) {

					loadArriveData();
				}
			}

		} /*else if (arg0 == (ButtonField) ok) {

			keepData();

			Calendar calendar = Calendar.getInstance();

			long dateDepart01l = dateFieldDepart.getDate();
			long dateArrive01l = dateFieldArrive.getDate();

			Date dateDepart01 = new Date(dateDepart01l);
			Date dateArrive02 = new Date(dateArrive01l);

			calendar.setTime(dateDepart01);
			Date a = calendar.getTime();

			calendar.setTime(dateArrive02);
			Date b = calendar.getTime();

			boolean value = false;

			if (isBasicFlight) {
				value = true;
			} else {
				value = UtilDate.after(a, b);
			}


			if (value) {

				if (verifyNumPassengers()) {
					DataKeeper dataKeeper = DataKeeper.getInstance();
					dataKeeper.setDataTravel(new Date(dateDepart), new Date(
							dateArrive), depart, arrive, idDepart, idArrive, iAdults, iChildren,
							iElderlies, iStudents, iProfessors, isBasicFlight);

					if (verifyChild()){
						
						if(verify19()){
							UiApplication.getUiApplication().pushScreen(
									new ViewScheduledDepart(true, isBasicFlight));
						} else {
							Dialog.alert("Se puede comprar hasta 19 boletos");
						}

					} else {
						Dialog.alert("Un ni�o debe ir acompa�ado de un adulto");
					}
					

				} else {

					Dialog.alert("Selecciona al menos un pasajero.");
				}

			} else {

				Dialog.alert("La Fecha de regreso es anterior a la fecha de salida");
			}

		} else if (arg0 == (CustomSelectedButtonField) customFieldManager
				.getBasicFlight()) {
			roundFlight = customFieldManager.getRoundFlight();
			roundFlight.setSelected(false);
			isBasicFlight = true;
			keepData();
			configurationFlightView(isBasicFlight);
			loadArriveData();
		} else if (arg0 == (CustomSelectedButtonField) customFieldManager
				.getRoundFlight()) {
			basicFlight = customFieldManager.getBasicFlight();
			basicFlight.setSelected(false);
			isBasicFlight = false;
			keepData();
			configurationFlightView(isBasicFlight);
			loadArriveData();
		} else if (arg0 == getInfoBoletos){
			
			//UserBean.idLogin = "1362692560880";
			if (UserBean.idLogin == null){
				UiApplication.getUiApplication().pushScreen(new LoginPopUp(this));
			} else {
				
				URLEncodedPostData encodedPostData = new URLEncodedPostData(
						URLEncodedPostData.DEFAULT_CHARSET, false);
				
				encodedPostData.append("modulo", "C");
				//encodedPostData.append("idBusqueda", UserBean.idLogin);
				encodedPostData.append("idBusqueda", UserBean.idLogin);
				
				
				String encoded = encodedPostData.toString();
				
				ViewBoletos boletos = new ViewBoletos();
				
				InfoBoletos infoBoletos = new InfoBoletos(boletos, Url.URL_INFO_BOLETOS, encoded);
				infoBoletos.run();
				
				UiApplication.getUiApplication().pushScreen(boletos);
			}
		} else if (arg0 == createRegistrer){

			CreditInfoThread infoThread = new CreditInfoThread("proveedores", null, Url.URL_GET_PROVIDERS, "");
			infoThread.run();
		} */
		
	}

	public void analyzeData(int request, Object object) {

		System.out.println(request);
		
		switch (request) {
		case DataAccessible.ERROR:
			Dialog.alert((String)object);
			break;
		case DataAccessible.DATA:
			
			if (object != null){
				
				Vector vector = (Vector)object;
				
				int size = vector.size();
				
				Router routers[] = new Router[size];
				
				vector.copyInto(routers);
				
				choiceFieldDepart.setChoices(routers);
				
			} else {
				
				Dialog.alert(Error.NULLPOINTER_EXCEPTION);
			}
			
			
			break;
		}
	}
	
	
	public void configurationFlightView(boolean isBasicFlight) {

		deleteAll();
		add(new LabelField(""));
		BitmapField bitmapField = UtilIcon.getTitle();
		add(bitmapField);
		add(new LabelField(""));
		SubtitleRichTextField subtitleRichTextField = new SubtitleRichTextField(
				"Datos de viaje");

		add(subtitleRichTextField);
		add(new LabelField(""));

		createRegistrer = new CustomSelectedButtonField(
				"Registrarse", Field.FIELD_VCENTER);
		createRegistrer.setChangeListener(this);
		getInfoBoletos = new CustomSelectedButtonField(
				"Boletos Adquiridos", Field.FIELD_VCENTER);
		getInfoBoletos.setChangeListener(this);
		customFieldManager01 = new HorizontalFieldManager();
		customFieldManager01.add(createRegistrer);
		customFieldManager01.add(getInfoBoletos);
		add(customFieldManager01);

		CustomSelectedButtonField basicFlight = new CustomSelectedButtonField(
				"Viaje Sencillo", Field.FIELD_VCENTER);
		CustomSelectedButtonField roundFlight = new CustomSelectedButtonField(
				"Viaje Redondo", Field.FIELD_VCENTER);

		basicFlight.setSelected(this.isBasicFlight);
		roundFlight.setSelected(!this.isBasicFlight);

		customFieldManager = new CustomButtonFieldManager(basicFlight,
				roundFlight);
		basicFlight.setChangeListener(this);
		roundFlight.setChangeListener(this);

		customFieldManager.add(roundFlight);
		customFieldManager.add(basicFlight);

		add(customFieldManager);

		String choices[] = null;

		choiceFieldDepart = new ObjectChoiceField("", arrayDepart, iDepart);
		choiceFieldDepart.setChangeListener(this);
		ElementLabelField textFieldDepart = new ElementLabelField("Origen");

		HorizontalFieldManager fieldManager1 = new HorizontalFieldManager();
		fieldManager1.add(textFieldDepart);
		fieldManager1.add(choiceFieldDepart);
		add(fieldManager1);

		choiceFieldArrive = new ObjectChoiceField("", choices, iArrive);
		choiceFieldArrive.setChangeListener(this);
		ElementLabelField textFieldArrive = new ElementLabelField("Destino");
		HorizontalFieldManager fieldManager2 = new HorizontalFieldManager();
		fieldManager2.add(textFieldArrive);
		fieldManager2.add(choiceFieldArrive);
		add(fieldManager2);

		dateFieldDepart = new DateField("", System.currentTimeMillis(),
				DateField.DATE) {
			protected void paint(Graphics graphics) {
				graphics.setColor(UtilColor.ELEMENT_STRING);
				super.paint(graphics);
			}
		};

		ElementLabelField labelDateDepart = new ElementLabelField("Fecha de salida");
		HorizontalFieldManager fieldManager3 = new HorizontalFieldManager();
		fieldManager3.add(labelDateDepart);
		fieldManager3.add(dateFieldDepart);
		add(fieldManager3);

		dateFieldArrive = new DateField("", System.currentTimeMillis(),
				DateField.DATE) {
			protected void paint(Graphics graphics) {
				graphics.setColor(UtilColor.ELEMENT_STRING);
				super.paint(graphics);
			}
		};
		ElementLabelField labelDateArrive = new ElementLabelField(
				"Fecha de Regreso");
		HorizontalFieldManager fieldManager4 = new HorizontalFieldManager();
		fieldManager4.add(labelDateArrive);
		fieldManager4.add(dateFieldArrive);
		if (!isBasicFlight) {
			add(fieldManager4);
		}

		if (dateDepart > 0) {
			dateFieldDepart.setDate(dateDepart);
		}

		if (dateArrive > 0) {
			dateFieldArrive.setDate(dateArrive);
		}

		SubtitleRichTextField orangeRichTextField02 = new SubtitleRichTextField(
				"A�adir pasajeros");
		add(orangeRichTextField02);

		choiceFieldAdult = addChoiceField("Adulto", iAdults);
		choiceFieldChild = addChoiceField("Menor", iChildren);
		choiceFieldElderly = addChoiceField("Senectud", iElderlies);
		choiceFieldStudent = addChoiceField("Estudiante", iStudents);
		choiceFieldProfessor = addChoiceField("Profesor", iProfessors);

		ok = new ButtonField("Elegir Horario");
		ok.setChangeListener(this);

		add(ok);
		add(new AnuncioLabelField("�Dudas? �Problemas? Ll�manos al: 5540-3124 o por correo electr�nico a soporte_etn@addcel.com"));
	}
	
	private NumericChoiceField addChoiceField(String title, int number) {
		int iStartAt = 0;
		int iEndAt = 19;
		int iIncrement = 1;

		ElementLabelField labelField = new ElementLabelField(title);
		NumericChoiceField choiceField = new NumericChoiceField("", iStartAt,
				iEndAt, iIncrement, number);

		HorizontalFieldManager fieldManager = new HorizontalFieldManager();
		fieldManager.add(labelField);
		fieldManager.add(choiceField);
		add(fieldManager);

		return choiceField;
	}
	
}

package mx.com.etn.model.dataaccess;

import java.util.Vector;

import mx.com.etn.addcelexception.OwnException;
import mx.com.etn.model.URL;
import mx.com.etn.model.dataaccess.components.connection.http.MethodPOST;
import mx.com.etn.model.dataaccess.components.toObject.ORutaSalida;
import mx.com.etn.view.base.Viewable;

public class DARutaSalida extends DataAccessible implements Runnable {

	public DARutaSalida(Viewable viewable, String json) {
		super(viewable, json);
	}

	public void execute(Viewable viewable, String json) {

		String data = null;

		connectable = new MethodPOST(URL.URL_GET_ORIGENES);
		
		Vector vector = null;
		
		try {
			connectable.execute(json);
			data = (String)connectable.getData(); 
			objectable = new ORutaSalida();
			objectable.execute(data);
			vector = (Vector)objectable.getData();
			viewable.setData(DataAccessible.DATA, vector);
		} catch (OwnException e) {
			viewable.setData(DataAccessible.ERROR, e.toString());
		}
	}

	public void run() {
		execute(viewable, data);
	}

}

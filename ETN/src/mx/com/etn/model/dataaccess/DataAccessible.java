package mx.com.etn.model.dataaccess;

import mx.com.etn.model.dataaccess.components.connection.Connectable;
import mx.com.etn.model.dataaccess.components.descryption.Descryptionable;
import mx.com.etn.model.dataaccess.components.encryption.Encryptionable;
import mx.com.etn.model.dataaccess.components.toJson.Jsonable;
import mx.com.etn.model.dataaccess.components.toObject.Objectable;
import mx.com.etn.view.base.Viewable;

public abstract class DataAccessible {
	
	public final static int ERROR = 0;
	public final static int DATA = 1;
	
	
	protected Viewable viewable;
	protected String data;
	
	protected Connectable connectable;
	protected Descryptionable descryptionable;
	protected Encryptionable encryptionable;
	protected Jsonable jsonable;
	protected Objectable objectable;
	
	public DataAccessible(Viewable viewable, String data){
		this.viewable = viewable;
		this.data = data;
	}
	
	abstract void execute(Viewable viewable, String json);
}

package mx.com.etn.model.dataaccess;

import java.util.Vector;

import mx.com.etn.addcelexception.OwnException;
import mx.com.etn.model.URL;
import mx.com.etn.model.dataaccess.components.connection.http.MethodGET;
import mx.com.etn.model.dataaccess.components.toObject.ORutaRegreso;
import mx.com.etn.view.base.Viewable;

public class DARutaRegreso extends DataAccessible implements Runnable {

	public DARutaRegreso(Viewable viewable, String json) {
		super(viewable, json);
	}

	public void run() {
		execute(viewable, data);
	}

	void execute(Viewable viewable, String json) {
		
		this.viewable = viewable;
		
		Vector vector = null;
		

		try {
			
			String data = json;
			connectable = new MethodGET(URL.URL_GET_ORIGENES);
			connectable.execute(data);
			data = (String)connectable.getData();
			objectable = new ORutaRegreso();
			objectable.execute(data);
			vector = (Vector)objectable.getData();
			viewable.setData(DataAccessible.DATA, vector);
		} catch (OwnException e) {
			viewable.setData(DataAccessible.ERROR, e.toString());
		}
	}
}

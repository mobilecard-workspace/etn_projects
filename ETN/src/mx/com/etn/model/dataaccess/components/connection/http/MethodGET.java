package mx.com.etn.model.dataaccess.components.connection.http;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import mx.com.etn.Start;
import mx.com.etn.addcelexception.OwnException;
import mx.com.etn.model.Error;
import mx.com.etn.model.dataaccess.components.connection.Connectable;
import mx.com.etn.view.base.Viewable;
import net.rim.device.api.io.http.HttpProtocolConstants;

/*
	Some other notes on GET requests:
	
	GET requests can be cached
	GET requests remain in the browser history
	GET requests can be bookmarked
	GET requests should never be used when dealing with sensitive data
	GET requests have length restrictions
	GET requests should be used only to retrieve data
*/


public class MethodGET implements Connectable {

	private HttpConnection httpConnection;
	private InputStream inputStream;
	private String url = null;
	private String data = null;
	//private Viewable viewable = null;
	
	StringBuffer stringBuffer = null;
	
	public MethodGET(String url){
		this.url = url;
		//this.viewable = viewable;
	}
	
	public String createURL() {
		return url + "?json=" + data + ";" + Start.IDEAL_CONNECTION + ";" + "ConnectionTimeout=30000";
	}
	
	public void execute(String data) throws OwnException {

		this.data = data;
		
		try {

			String id = "0";
			stringBuffer = new StringBuffer();

			httpConnection = (HttpConnection) Connector.open(createURL());

			httpConnection.setRequestMethod(HttpConnection.POST);

			httpConnection.setRequestProperty(
					HttpProtocolConstants.HEADER_CONTENT_TYPE, data);

			System.out.println( createURL());
			
			byte[] postBytes = data.getBytes();
			httpConnection.setRequestProperty(
					HttpProtocolConstants.HEADER_CONTENT_LENGTH,
					Integer.toString(postBytes.length));
			OutputStream strmOut = httpConnection.openOutputStream();
			strmOut.write(postBytes);
			strmOut.flush();
			strmOut.close();

			inputStream = httpConnection.openInputStream();
			int c;
			while ((c = inputStream.read()) != -1) {

				stringBuffer.append((char) c);
			}

			System.out.println(stringBuffer.toString());

		} catch (IOException e) {
			throw new OwnException(Error.HTTP_SEND_DATA);
		} finally {

			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (httpConnection != null) {
					httpConnection.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		//return stringBuffer.toString();
	}


	public Object getData() {
		return stringBuffer.toString();
	}
}

/*
 * 
 * final JSONObject jsObject = new JSONObject( stringBuffer.toString());
 * 
 * UiApplication.getUiApplication().invokeLater(new Runnable() { public void
 * run() {
 * 
 * viewable.setData(request, jsObject); } });
 * 
 * 
 * 
 * 
 * 
 * 
 * e.printStackTrace(); final JSONObject jsObject = null;
 * 
 * UiApplication.getUiApplication().invokeLater(new Runnable() { public void
 * run() {
 * 
 * viewable.setData(request, jsObject); } });
 */


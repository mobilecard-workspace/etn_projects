package mx.com.etn.model.dataaccess.components.toObject;

import java.util.Vector;

import mx.com.etn.addcelexception.OwnException;

public interface Objectable {

	public void execute(String data) throws OwnException;
	public Object getData();
}

package mx.com.etn.model.dataaccess.components.descryption;

import mx.com.etn.view.base.Viewable;

public interface Descryptionable {

	public void execute(Viewable viewable, String url, String data);
}

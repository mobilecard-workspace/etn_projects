package mx.com.etn.model.dataaccess.components.toJson;

import mx.com.etn.view.base.Viewable;

public interface Jsonable {

	public void execute(Viewable viewable, String url, String data);
}

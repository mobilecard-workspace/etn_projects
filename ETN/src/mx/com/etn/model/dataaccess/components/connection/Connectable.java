package mx.com.etn.model.dataaccess.components.connection;

import mx.com.etn.addcelexception.OwnException;

public interface Connectable {

	public void execute(String data) throws OwnException;
	public String createURL();
	public Object getData();
}

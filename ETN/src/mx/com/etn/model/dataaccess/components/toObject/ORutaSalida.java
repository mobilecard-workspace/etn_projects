package mx.com.etn.model.dataaccess.components.toObject;

import java.util.Vector;

import mx.com.etn.addcelexception.OwnException;
import mx.com.etn.dto.Router;
import mx.com.etn.model.Error;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

public class ORutaSalida implements Objectable {

	Vector routers = null;
	
	public void execute(String data) throws OwnException {
		
		JSONObject jsonObject = null;

		
		try {
			jsonObject = new JSONObject(data);
			
			JSONArray jsonArray = jsonObject.getJSONArray("lista");

			Router router = null;
			routers = new Vector();
			
			int length = jsonArray.length();
			
			String sRouters[] = new String[length];
			
	        for (int i = 0; i < length; i++) {
	             JSONObject childJSONObject = jsonArray.getJSONObject(i);

	             router = new Router();
	             router.setKey(childJSONObject.getString("clave"));
	             String description = childJSONObject.getString("descripcion"); 
	             router.setDescription(description);
	             sRouters[i] = description;
	             routers.addElement(router);
	        }
		} catch (JSONException e) {
			e.printStackTrace();
			throw new OwnException(Error.JSON_EXCEPTION);
		}
	}
	
	public Object getData() {
		return routers;
	}
}

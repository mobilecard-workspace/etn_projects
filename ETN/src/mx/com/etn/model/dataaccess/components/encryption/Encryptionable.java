package mx.com.etn.model.dataaccess.components.encryption;

public interface Encryptionable {

	public void execute(String data);
}

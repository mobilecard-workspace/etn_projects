package mx.com.etn.model;

public class URL {

	public static String URL_GET_USERDATA = "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_getUserData.jsp";
	public static String URL_LOGIN = "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_userLogin.jsp";
	public static String URL_ETN = "http://www.mobilecard.mx:8080/ETNWSConsumer/";
	
	public static String URL_VERSIONADOR = "http://www.mobilecard.mx:8080/Vitamedica/getVersion?idApp=3&idPlataforma=3";
	
	public static String URL_PAGO_PROSA = "http://mobilecard.mx:8080/ProcomETN/envio.jsp";
	public static String URL_INFO_BOLETOS = "http://mobilecard.mx:8080/ETNWSConsumer/ConsultaBoletoEtn";
	public static String URL_TERMS = "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_getConditions.jsp";
	public static String URL_USER_INSERT = "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_userInsert.jsp";
	
	public static String URL_GET_BANKS = "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_getBanks.jsp";
	public static String URL_GET_CARDTYPES = "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_getCardType.jsp";
	public static String URL_GET_PROVIDERS = "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_getProviders.jsp";
	public static String URL_GET_ESTADOS = "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_getEstados.jsp";
	public static String URL_UPDATE_PASS_MAIL = "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_userPasswordUpdateMail.jsp";
	public static String URL_GET_ORIGENES = "http://www.mobilecard.mx:8080/ETNWSConsumer/ObtenerOrigenes";
	public static String URL_GET_DESTINOS = "http://www.mobilecard.mx:8080/ETNWSConsumer/ObtenerDestinos";
	
	
	
}

package mx.com.etn.controller;

import net.rim.blackberry.api.browser.URLEncodedPostData;
import net.rim.device.api.ui.UiApplication;
import mx.com.etn.dto.Router;
import mx.com.etn.model.dataaccess.DARutaRegreso;
import mx.com.etn.model.dataaccess.DARutaSalida;
import mx.com.etn.view.ViewETNBuy;
import mx.com.etn.view.base.Viewable;

public class Controller {

	/*
	
	Splash
	Versionamiento
	Configurar corrida
	Seleccionar horario salida
	Seleccionar horario regreso
	Asignar asientos
		Mostrar lista de viajes y Seleccionar (si es redondo)
		Mostrar lista de pasajeros y seleccionar uno
		Mostrar asientos y seleccionar uno
	Mostrar resumen de compra
	Hacer pago 3DSecure
	

	Registro
	Consulta de boletos
	Autenticarse
	
	
	*/
	
	public final static int CONFIGURAR_CORRIDA = 0;
	public final static int OBTENER_REGRESO = 1;
	
	public static void execute(int opcion, Viewable viewable, Object object){
		
		switch (opcion) {
		case CONFIGURAR_CORRIDA:
			
			ViewETNBuy viewETNBuy = new ViewETNBuy(true, "Venta");  
			DARutaSalida rutaSalida = new DARutaSalida(viewETNBuy, "");
			Thread thread00 = new Thread(rutaSalida);
			thread00.start();
			UiApplication.getUiApplication().pushScreen(viewETNBuy);
			break;

		case OBTENER_REGRESO:
			
			Router router = (Router)object;
			
			URLEncodedPostData encodedPostData = new URLEncodedPostData(
					URLEncodedPostData.DEFAULT_CHARSET, false);
			encodedPostData.append("origen", router.getKey());
			
			DARutaRegreso daRutaRegreso = new DARutaRegreso(viewable, encodedPostData.toString());
			Thread thread01 = new Thread(daRutaRegreso);
			thread01.start();
			break;
			
		default:
			break;
		}
		
		
		
	}
	
	
	
}
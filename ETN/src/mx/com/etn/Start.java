package mx.com.etn;

import mx.com.etn.controller.Controller;
import mx.com.etn.model.util.UtilBB;
import net.rim.device.api.ui.UiApplication;

public class Start extends UiApplication {
	
	static public String IDEAL_CONNECTION = null;
	
	public static void main(String[] args) {
		
		Start theApp = new Start();
		theApp.enterEventDispatcher();
	}


	public Start() {

		IDEAL_CONNECTION = UtilBB.checkConnectionType();
		Controller.execute(Controller.CONFIGURAR_CORRIDA, null, null);
	}
}

/*
Thread thread = new Thread(new DARutaSalida(null, ""));
thread.start();
*/


//CreditInfoThread creditInfoThread = new CreditInfoThread("proveedores", null,  Url.URL_GET_BANKS, null);
//creditInfoThread.run();
//pushScreen(new ViewRegistrer());
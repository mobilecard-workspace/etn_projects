package mx.com.ib.etn.utils;

import net.rim.device.api.ui.Color;

public class UtilColor {

	//R144 G193 Y199	= 0x90c1c7

	private  final static int R35_G72_Y109 = 0x20486d;
	private final static int R235_G235_G235 = 0xebebeb;
	private final static int R61_G183_Y179 = 0x3db7b3;
	private final static int R144_G193_Y199 = 0x90c1c7;
	private final static int R147_G149_Y152 = 0x939598;
	private final static int R35_G31_Y32 = 0x231f20;
	private final static int R180_G189_Y182 = 0xb4bdb6;
	private final static int R209_G215_Y210 = 0xd1d7d2;
	
	private final static int R00_G84_Y128 = 0x005480;
	
	public final static int BACKGROUND = R235_G235_G235;
	public final static int BACKGROUND_FOCUS = R209_G215_Y210;
	
	public final static int TITLE_STRING_BLACK = R147_G149_Y152;
	public final static int TITLE_STRING = Color.WHITE;
	public final static int TITLE_BACKGROUND = R144_G193_Y199; 

	//public final static int BUTTON_FOCUS = R180_G189_Y182;
	public final static int BUTTON_FOCUS = R144_G193_Y199;
	//public final static int BUTTON_FOCUS_STRING = R147_G149_Y152;
	public final static int BUTTON_FOCUS_STRING = Color.WHITE;
	//public final static int BUTTON_SELECTED = R144_G193_Y199;
	public final static int BUTTON_SELECTED = R00_G84_Y128;
	public final static int BUTTON_UNSELECTED = R209_G215_Y210;

	public final static int ELEMENT_BACKGROUND = R235_G235_G235;
	public final static int ELEMENT_FOCUS = R209_G215_Y210;
	//public final static int ELEMENT_FOCUS = R00_G84_Y128;
	//public final static int ELEMENT_STRING_TITLE = R147_G149_Y152;
	public final static int ELEMENT_STRING_TITLE = R00_G84_Y128;
	//public final static int ELEMENT_STRING_DATA = R61_G183_Y179;
	public final static int ELEMENT_STRING_DATA = 0x333333;

/*
	public static int BG_GRAY = 0x414042;
	public static int BG_ORANGE = 0xF16430;
	public static int COLOR_LINK = 0x38D4FF;
*/

	public static int toRGB(int r, int g, int b){
		return (r<<16)+(g<<8)+b;		 
	}

	public static int getBlack(){
		return toRGB(30, 30, 30);
	}

	public static int getDarkGray(){
		return toRGB(120, 120, 120);
	}

	public static int getLightGray(){
		return toRGB(200, 200, 200);
	}

	public static int getLightOrange(){
		return toRGB(255, 184, 101);
	}

	public static int getDarkOrange(){
		return toRGB(255, 159, 45);
	}
}

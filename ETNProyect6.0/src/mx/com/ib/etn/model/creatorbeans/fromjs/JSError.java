package mx.com.ib.etn.model.creatorbeans.fromjs;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

public class JSError {

	private JSONObject jsObject;
	private int numError;
	private String error;


	public JSError(JSONObject jsObject) throws JSONException{
		
		JSONArray jsonArray = jsObject.getJSONArray("listaCorridas");
		JSONObject childJSONObject = jsonArray.getJSONObject(0);

		setNumError(childJSONObject.getInt("errorNumero"));
		setError(childJSONObject.getString("errorCadena"));
	}


	public int getNumError() {
		return numError;
	}

	public void setNumError(int numError) {
		this.numError = numError;
	}
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}

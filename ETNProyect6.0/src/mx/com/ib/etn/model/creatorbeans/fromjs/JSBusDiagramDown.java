package mx.com.ib.etn.model.creatorbeans.fromjs;

import java.util.Vector;

import mx.com.ib.etn.view.seats.GraphicButtonSeat;
import net.rim.device.api.ui.Field;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

public class JSBusDiagramDown {

	private int numFilasDiagram;
	private int numFilasArriba;
	private int numErrorDiagram;

	private String descErrorDiagram;
	private int numAsientosDiagram;

	private String codigoError;
	private String descripcionError;

	private Vector seats = null;
/*
	private JSONObject createJSONDiagram() throws JSONException{
		
		String sJson = "{\"diagramaAutobus\":[\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"023T\",\"024T\",\"000 \",\"025T\",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000H\",\"000M\",\"000C\",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"001T\",\"000 \",\"002T\",\"003T\",\"004T\",\"000 \",\"005T\",\"006T\",\"007T\",\"000 \",\"008T\",\"009T\",\"010T\",\"000 \",\"011T\",\"012T\",\"013T\",\"000 \",\"000P\",\"000 \",\"014T\",\"000 \",\"015T\",\"016T\",\"017T\",\"000 \",\"018T\",\"019T\",\"020T\",\"000 \",\"021T\",\"022T\",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \"],\"numFilas\":\"7\",\"numFilasArriba\":\"8\",\"numError\":1111,\"descError\":\"Cadena correcta.\",\"numAsientos\":25}";
		
		JSONObject jsonObject = new JSONObject(sJson);
		
		return jsonObject;
	}
	
	private JSONObject createJSONOccupancy() throws JSONException{
		
		String sJson = "{\"asientos\":[\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\"codigoError\":\"00\",\"descripcionError\":\"Sin Error.\"}";
		
		JSONObject jsonObject = new JSONObject(sJson);
		
		return jsonObject;
	}
*/

	public Vector createDiagram(JSONObject jsObject) throws JSONException {

		JSONArray jsonArray = jsObject.getJSONArray("diagramaAutobus");

		String snumFilas = jsObject.optString("numFilas", "0");
		String snumFilasArriba = jsObject.optString("numFilasArriba", "0");

		numFilasDiagram = Integer.parseInt(snumFilas);
		numFilasArriba =  Integer.parseInt(snumFilasArriba);
		numErrorDiagram = jsObject.getInt("numError");
		descErrorDiagram = (String) jsObject.get("descError");
		numAsientosDiagram = jsObject.getInt("numAsientos");
		
		int length = jsonArray.length();

		seats = new Vector();
		
		for(int index = 0; index < length; index++){
			
			String info = (String)jsonArray.get(index);
			info = info.toUpperCase();

			int statusSeat = GraphicButtonSeat.SEAT_NOT_OCCUPIED;

			int other = 0;
			
			if (info.indexOf("T")>=0){
				statusSeat = GraphicButtonSeat.SEAT_NOT_OCCUPIED_TV;
			} else if (info.indexOf("000")>=0){
				statusSeat = GraphicButtonSeat.SEAT_AISLE;
				if (info.indexOf("H")>=0){
					other = GraphicButtonSeat.SEAT_WC_MAN;
				} else if (info.indexOf("M")>=0){
					other = GraphicButtonSeat.SEAT_WC_WOMAN;
				} else if (info.indexOf("C")>=0){
					other = GraphicButtonSeat.SEAT_COFFEE_SHOP;
				} else if (info.indexOf("U")>=0){
					statusSeat = GraphicButtonSeat.SEAT_WC_UNISEX;
				} else if (info.indexOf("P")>=0){
					other = GraphicButtonSeat.SEAT_STAIR;
				}
			}
			
			GraphicButtonSeat buttonSeat = new GraphicButtonSeat(statusSeat, other);
			
			buttonSeat.setSeat(info);
			
			seats.addElement(buttonSeat);
		}
		
		return seats;
	}


	public Vector setOccupancy(JSONObject jsObject, Vector seats) throws JSONException {

		JSONArray jsonArray = jsObject.getJSONArray("asientos");
		
		int asientos = 0; 
		int size = this.seats.size();
		int index2 = 0;

		for(int index = 0; index < size; index++ ){
			
			Field fSeat = (Field)this.seats.elementAt(index);
			
			if (fSeat instanceof GraphicButtonSeat){
				
				GraphicButtonSeat seat = (GraphicButtonSeat)fSeat;
				
				String used = jsonArray.optString(index2, "0");
				index2++;
				if (used.equals("1")){
					seat.setOccupied(true);
				}
			}
		}
		
		return seats;
	}


	public GraphicButtonSeat[][] getPrimerPiso(Vector vectorSeats){
		
		GraphicButtonSeat arraySeats[][] = new  GraphicButtonSeat[7][4];
		
		//int size = vectorSeats.size();

		int index = 0;
		for(int index1 = 0; index1 < 7; index1++){
			
			for(int index2 = 0; index2 < 4; index2++){
				arraySeats[index1][index2] = (GraphicButtonSeat) vectorSeats.elementAt(index);
				arraySeats[index1][index2].setBitmap();
				index++;
			}
		}
		return arraySeats;
	}


	public GraphicButtonSeat[][] getSegundoPiso(Vector vectorSeats, int filasArriba){
		
		GraphicButtonSeat arraySeats[][] = new  GraphicButtonSeat[filasArriba][4];
		
		//int size = vectorSeats.size();

		int index = 54;
		for(int index1 = 0; index1 < filasArriba; index1++){
			
			for(int index2 = 0; index2 < 4; index2++){
				arraySeats[index1][index2] = (GraphicButtonSeat) vectorSeats.elementAt(index);
				arraySeats[index1][index2].setBitmap();
				index++;
			}
		}
		return arraySeats;
	}


	public GraphicButtonSeat[][] getArray(GraphicButtonSeat[][] primerPiso, GraphicButtonSeat[][] segundoPiso){
		
		int pi = primerPiso.length;
		int pj = primerPiso[0].length;
		
		int si = segundoPiso.length;
		int sj = segundoPiso[0].length;

		int i = si + pi;
		
		GraphicButtonSeat arraySeats[][] = new  GraphicButtonSeat[i + 1][pj];
		
		int index = 0;
		
		for(int index1 = 0; index1 < pi; index1++){
			
			try{
				for(int index2 = 0; index2 < pj; index2++){
					arraySeats[index][index2] =  primerPiso[index1][index2];
					arraySeats[index][index2].setBitmap();
				}
				index++;
			} catch (Exception e){
				e.printStackTrace();
			}

		}
		
		

			for(int index2 = 0; index2 < pj; index2++){
				GraphicButtonSeat buttonSeat = new GraphicButtonSeat(GraphicButtonSeat.SEAT_AISLE, GraphicButtonSeat.SEAT_SECOND_FLOOR);
				arraySeats[index][index2] =  buttonSeat;
				arraySeats[index][index2].setBitmap();
			}
			index++;
		
		//GraphicButtonSeat buttonSeat = new GraphicButtonSeat(GraphicButtonSeat.SEAT_AISLE, GraphicButtonSeat.SEAT_SECOND_FLOOR);
		
		
		
		for(int index1 = 0; index1 < si; index1++){
			
			for(int index2 = 0; index2 < sj; index2++){
				arraySeats[index][index2] =  segundoPiso[index1][index2];
				arraySeats[index][index2].setBitmap();
			}
			index++;
		}
		
		return arraySeats;
	}
}

	
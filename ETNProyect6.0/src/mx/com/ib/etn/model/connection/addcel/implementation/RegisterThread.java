package mx.com.ib.etn.model.connection.addcel.implementation;

import mx.com.ib.etn.beans.beans.UserBean;
import mx.com.ib.etn.model.connection.Url;
import mx.com.ib.etn.model.connection.addcel.json.JSONParser;
import mx.com.ib.etn.utils.UtilSecurity;
import mx.com.ib.etn.view.controls.Viewable;
import net.rim.device.api.ui.UiApplication;

import org.json.me.JSONObject;

public class RegisterThread extends HttpListener {

	public RegisterThread(String json, Viewable viewable) {

		super(viewable, Url.URL_USER_INSERT, "json=" + json);
	}

	public void receiveHttpResponse(int appCode, byte[] response) {

		String sTemp = null;

		try {

			sTemp = new String(response);

			sTemp = UtilSecurity.aesDecrypt(
					UtilSecurity.parsePass(UserBean.passTEMP), sTemp);

			System.out.println(sTemp);
			System.out.println(sTemp);
			
			JSONParser jsParser = new JSONParser();

			if (jsParser.isRegister(sTemp)) {

				UiApplication.getUiApplication().invokeLater(new Runnable() {
					public void run() {
						UiApplication.getUiApplication().popScreen(
								UiApplication.getUiApplication()
										.getActiveScreen());
					}
				});

			} else {

				final JSONObject jsObject = new JSONObject(sTemp);

				UiApplication.getUiApplication().invokeLater(new Runnable() {
					public void run() {
						sendData(jsObject);
					}
				});

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

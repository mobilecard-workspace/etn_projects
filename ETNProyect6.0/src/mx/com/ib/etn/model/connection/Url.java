package mx.com.ib.etn.model.connection;

public class Url {

	//private static String URL = "mobilecard.mx:8080";
	private static String URL = "50.57.192.213:8080";
	
	public static String URL_GET_USERDATA = "http://" + URL + "/AddCelBridge/Servicios/adc_getUserData.jsp";
	public static String URL_LOGIN = "http://" + URL + "/AddCelBridge/Servicios/adc_userLogin.jsp";
	public static String URL_ETN = "http://" + URL + "/ETNWSConsumer/";
	
	public static String URL_VERSIONADOR = "http://mobilecard.mx:8080/Vitamedica/getVersion?idApp=3&idPlataforma=3";

	public static String URL_PAGO_PROSA = "http://" + URL + "/ProcomETN/envio.jsp";
	public static String URL_INFO_BOLETOS = "http://" + URL + "/ETNWSConsumer/ConsultaBoletoEtn";
	public static String URL_TERMS = "http://" + URL + "/AddCelBridge/Servicios/adc_getConditions.jsp";
	public static String URL_USER_INSERT = "http://" + URL + "/AddCelBridge/Servicios/adc_userInsert.jsp";

	public static String URL_GET_BANKS = "http://" + URL + "/AddCelBridge/Servicios/adc_getBanks.jsp";
	public static String URL_GET_CARDTYPES = "http://" + URL + "/AddCelBridge/Servicios/adc_getCardType.jsp";
	public static String URL_GET_PROVIDERS = "http://" + URL + "/AddCelBridge/Servicios/adc_getProviders.jsp";
	public static String URL_GET_ESTADOS = "http://" + URL + "/AddCelBridge/Servicios/adc_getEstados.jsp";

	public static String URL_GET_ORIGENES = "http://" + URL + "/ETNWSConsumer/ObtenerOrigenes";
	
	public static String URL_GET_RESERVACIONES = "http://" + URL + "/ETNWSConsumer/ConsultarReservacion";
	public static String URL_GET_SECUENCIA = "http://" + URL + "/AddCelBridge/ClienteGeneraSecuencia";
	
	public static String URL_UPDATE_PASS_MAIL = "http://" + URL + "/AddCelBridge/Servicios/adc_userPasswordUpdateMail.jsp";
}

package mx.com.ib.etn.model.connection.base;

import net.rim.blackberry.api.browser.URLEncodedPostData;

public class UrlEncode {

	private URLEncodedPostData encodedPostData = null;
	private int request = 0;
	
	public URLEncodedPostData getEncodedPostData() {
		return encodedPostData;
	}
	public void setEncodedPostData(URLEncodedPostData encodedPostData) {
		this.encodedPostData = encodedPostData;
	}
	public int getRequest() {
		return request;
	}
	public void setRequest(int request) {
		this.request = request;
	}
}

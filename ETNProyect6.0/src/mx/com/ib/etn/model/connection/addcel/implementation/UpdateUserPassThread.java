package mx.com.ib.etn.model.connection.addcel.implementation;

import mx.com.ib.etn.utils.UtilSecurity;
import mx.com.ib.etn.utils.Utils;
import mx.com.ib.etn.view.controls.Viewable;

public class UpdateUserPassThread extends HttpListener{

	private String newPassword;
	
	public UpdateUserPassThread(Viewable viewable, String url, String post, String newPassword){

		super(viewable, url, post);
		
		this.newPassword = newPassword;
	}


	public void receiveHttpResponse(int appCode, byte[] response) {

		String sTemp = null;
		sTemp = new String(response, 0, response.length);
		sTemp = UtilSecurity.aesDecrypt(Utils.parsePass(newPassword), sTemp);
	}
}


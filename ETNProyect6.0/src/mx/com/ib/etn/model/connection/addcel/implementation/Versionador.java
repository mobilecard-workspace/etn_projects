package mx.com.ib.etn.model.connection.addcel.implementation;

import mx.com.ib.etn.model.connection.Url;
import mx.com.ib.etn.view.WVersionador;
import mx.com.ib.etn.view.controls.Viewable;
import mx.com.ib.etn.view.etnbuy.ViewETNBuy;
import net.rim.device.api.system.Application;
import net.rim.device.api.system.ApplicationDescriptor;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;

import org.json.me.JSONException;
import org.json.me.JSONObject;




public class Versionador extends HttpListener {

	private Viewable viewable;
	private String url = Url.URL_VERSIONADOR;

	public Versionador(String post, String url, Viewable viewable){
		super(post, url, viewable);
		this.viewable = viewable;
	}


	public void receiveHttpResponse(int appCode, byte[] response) {

		try {

			String json = new String(response);
			JSONObject jsonObject;
			jsonObject = new JSONObject(json);

			String version01 = jsonObject.optString("version", ""); 
			final String tipo = jsonObject.optString("tipo", "");
			//final String tipo = "2";
			final String url = jsonObject.optString("url", "");

			synchronized (Application.getEventLock()) {
				if (version01 != null && !version01.equals("")) {

					ApplicationDescriptor appDesc = ApplicationDescriptor.currentApplicationDescriptor();

					if (version01.equals(appDesc.getVersion())) {
						UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
						UiApplication.getUiApplication().pushScreen(new ViewETNBuy(true));
					} else {

						if (tipo.equals("1")) {
							//obligatoria
							UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
							UiApplication.getUiApplication().pushScreen(new WVersionador("", url));
						} else if (tipo.equals("2")) {
							//no obligatoria
							UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
							UiApplication.getUiApplication().pushScreen(new ViewETNBuy(false));
							UiApplication.getUiApplication().pushScreen(new WVersionador("", url));
						}
					}
				}
			}
		} catch (JSONException e) {
			Dialog.alert("Error al intepretar la respuesta.");
			e.printStackTrace();
		}
	}
}

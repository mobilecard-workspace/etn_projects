package mx.com.ib.etn.model.connection.addcel.implementation;

import java.util.Vector;

import mx.com.ib.etn.beans.beans.GeneralBean;
import mx.com.ib.etn.model.connection.Url;
import mx.com.ib.etn.model.connection.addcel.json.JSONParser;
import mx.com.ib.etn.utils.UtilBB;
import mx.com.ib.etn.utils.UtilSecurity;
import mx.com.ib.etn.view.animated.SplashScreen;
import mx.com.ib.etn.view.controls.Viewable;
import mx.com.ib.etn.view.registrer.ViewRegistrer;
import net.rim.device.api.system.Application;
import net.rim.device.api.ui.UiApplication;

public class CreditInfoThread extends HttpListener {

	public String url = "";
	public String id = "";

	private static GeneralBean[] EdoBeans = null;
	private static GeneralBean[] bankBeans = null;
	private static GeneralBean[] creditBeans = null;
	private static GeneralBean[] providersBeans = null;
	
	private SplashScreen splashScreen = null;
	
	public CreditInfoThread(String id, Viewable viewable, String url, String post) {

		super(viewable, url, post);
		
		this.id = id;

		if (this.id.equals("bancos")) {

			this.url = Url.URL_GET_BANKS;

		} else if (this.id.equals("tarjetas")) {

			this.url = Url.URL_GET_CARDTYPES;

		} else if (this.id.equals("proveedores")) {

			this.url = Url.URL_GET_PROVIDERS;

		} else if (this.id.equals("estados")) {

			this.url = Url.URL_GET_ESTADOS;

		}
		
		splashScreen = SplashScreen.getInstance();

	}

	public void run() {
		connect();
	}

	public void connect() {

		String idealConnection = UtilBB.checkConnectionType();
		
		if (idealConnection != null) {

			if (this.url != null) {
				try {
					Communicator communicator = Communicator.getInstance();
					communicator.sendHttpRequest(this, this.url);

				} catch (Exception t) {
					t.printStackTrace();
				}
			}
		} else {
		}

	}

	public void handleHttpError(int errorCode, String error) {
		getMessageError();
	}

	public boolean isDestroyed() {
		return false;
	}

	public void receiveEstatus(String msg) {
	}

	public void receiveHeaders(Vector _headers) {
	}

	
	public void receiveHttpResponse(int appCode, byte[] response) {

		String sTemp = null;
		StringBuffer sb = new StringBuffer();
		String key = "1234567890ABCDEF0123456789ABCDEF";
		JSONParser jsParser = new JSONParser();
		
		try {

			sb.append(new String(response, 0, response.length, "UTF-8"));
			sTemp = new String(sb.toString());

			System.out.println("STEMP: " + sTemp);
			GeneralBean[] gralBeans = null;
			sTemp = UtilSecurity.aesDecrypt(key, sTemp);
			System.out.println("STEMP: " + sTemp);

			gralBeans = jsParser.getInfoCredits(this.id, sTemp);

			if (gralBeans != null) {

				if (gralBeans.length > 0) {

					if (this.id.equals("proveedores")) {

						providersBeans = gralBeans;

						new CreditInfoThread("bancos", null, url, null).run();
						
					} else if (this.id.equals("bancos")) {

						bankBeans = gralBeans;

						new CreditInfoThread("tarjetas", null, url, null).run();

					} else if (this.id.equals("tarjetas")) {

						creditBeans = gralBeans;

						new CreditInfoThread("estados", null, url, null).run();
					} else if (this.id.equals("estados")) {

						EdoBeans = gralBeans;

						//UiApplication.getUiApplication();

						synchronized (Application.getEventLock()) {
							
							splashScreen.remove();
							UiApplication.getUiApplication().pushScreen(new ViewRegistrer(EdoBeans, bankBeans, creditBeans, providersBeans));
						}
					}

				} else {

					getMessageError();
				}

			} else {

				getMessageError();
			}

		} catch (Exception e) {

			getMessageError();
		}
	}

	public void getMessageError() {

		synchronized (Application.getEventLock()) {
			// MainClass.splashScreen.remove();
			// UiApplication.getUiApplication().pushScreen(new
			// MessagePopupScreen("Informaci�n no disponible. Favor de intentar m�s tarde",
			// Field.NON_FOCUSABLE));
		}

	}

}

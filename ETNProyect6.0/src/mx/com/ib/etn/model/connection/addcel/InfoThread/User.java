package mx.com.ib.etn.model.connection.addcel.InfoThread;

import java.io.UnsupportedEncodingException;
import java.util.Vector;


import mx.com.ib.etn.beans.beans.UserBean;
import mx.com.ib.etn.model.connection.Url;
import mx.com.ib.etn.model.connection.addcel.base.Communicator;
import mx.com.ib.etn.model.connection.addcel.base.HttpListener;
import mx.com.ib.etn.model.connection.addcel.json.JSONParser;
import mx.com.ib.etn.utils.UtilBB;
import mx.com.ib.etn.utils.UtilSecurity;
import net.rim.device.api.system.Application;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;


public class User implements HttpListener {
	
	public String url = Url.URL_GET_USERDATA;
	public String post = "";
	public String tipo = "";
	public JSONParser jsParser = null;

	public User(String json) {
		this.post = "json=" + json;
		jsParser = new JSONParser();
	}

	public void run() {
		connect();
	}

	public void connect() {

		String idealConnection = UtilBB.checkConnectionType();

		if (idealConnection != null) {

			if (this.url != null) {
				try {

					Communicator communicator = Communicator.getInstance();
					communicator.sendHttpPost(this, this.url, this.post);
					
				} catch (Exception e) {
					e.printStackTrace();
					getMessageError("No se conecto al servidor, intenta de nuevo por favor.");
				}
			}
		} else {
			
		}
	}

	public void handleHttpError(int errorCode, String error) {
		getMessageError(error);
	}

	public boolean isDestroyed() {
		return false;
	}

	public void receiveEstatus(String msg) {
	}

	public void receiveHeaders(Vector _headers) {
	}

	public void receiveHttpResponse(int appCode, byte[] response) {

		String sTemp = null;
		//StringBuffer sb = new StringBuffer();

		try {

			synchronized (Application.getEventLock()) {
				//sb.append(new String(response, 0, response.length, "UTF-8"));
				//sTemp = new String(sb.toString());
				sTemp = new String(response, 0, response.length, "UTF-8");
				sTemp = UtilSecurity.aesDecrypt(UtilSecurity.parsePass(""), sTemp);
				System.out.println(sTemp);
				UserBean userBean = jsParser.getUser(sTemp);
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			getMessageError("Hubo un error al leer la información requerida.");
		}  catch (Exception e) {
			e.printStackTrace();
			getMessageError("Hubo un error al procesar la información requerida.");
		}

	}

	public void getMessageError(String error) {

		final String errors = error;
		
		UiApplication.getUiApplication().invokeLater(new Runnable() {
			public void run() {
				Dialog.alert(errors);
			}
		});
	}

}

package mx.com.ib.etn.view.resumen;

import net.rim.device.api.ui.component.NullField;
import mx.com.ib.etn.beans.beans.Resumen;

public class CustomGridFieldResumen extends CustomGridField{

	public CustomGridFieldResumen(Resumen resumen, boolean iva){
		
		super();
		
		String data = resumen.getImporteTotal();
		
		if (iva){
			
			data += "*";
		}
		
		CustomRichTextField rResumen = new CustomRichTextField("Total: ", data);
		
		add(rResumen);
		add(new NullField());
	}
	
}

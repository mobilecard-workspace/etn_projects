package mx.com.ib.etn.view.resumen;

import mx.com.ib.etn.beans.dto.Passenger;
import net.rim.device.api.ui.component.NullField;

public class CustomGridFieldPasajero extends CustomGridField {

	public CustomGridFieldPasajero(Passenger passenger) {

		super();

		CustomRichTextField sNombre = new CustomRichTextField("Nombre: ", passenger.getName());
		CustomRichTextField sAsiento = new CustomRichTextField("Asiento: ", passenger.getSeat());
		CustomRichTextField sTipo = new CustomRichTextField("Tipo: ", passenger.getKind());
		
		add(sNombre);
		add(sAsiento);
		add(sTipo);
		add(new NullField());
	}
}




/*

public class CustomGridFieldPasajero extends VerticalFieldManager {

	public CustomGridFieldPasajero(Passenger passenger) {

		super();

		CustomRichTextField sNombre = new CustomRichTextField("Nombre: ", passenger.getName());
		CustomRichTextField sAsiento = new CustomRichTextField("Asiento: ", passenger.getSeat());
		CustomRichTextField sTipo = new CustomRichTextField("Tipo: ", passenger.getKind());
		
		add(sNombre);
		add(sAsiento);
		add(sTipo);
		add(new NullField());
	}

	protected void paint(Graphics graphics) {

		graphics.clear();

		if (isFocus()) {
			graphics.setColor(UtilColor.ELEMENT_FOCUS);
			graphics.setGlobalAlpha(200);
			graphics.fillRoundRect(0, 0, getPreferredWidth(),
					getPreferredHeight(), 1, 1);
			graphics.setGlobalAlpha(255);
		} else {
			graphics.setColor(UtilColor.ELEMENT_BACKGROUND);
			graphics.fillRoundRect(0, 0, getPreferredWidth(),
					getPreferredHeight(), 1, 1);
		}

		super.paint(graphics);
	}

	public boolean isFocusable() {
		return true;
	}

	public int getPreferredWidth() {
		return Display.getWidth();
	}

	protected boolean navigationClick(int status, int time) {
		fieldChangeNotify(0);
		//showView();
		return super.navigationClick(status, time);
	}

	protected boolean keyChar(char character, int status, int time) {
		if (character == Keypad.KEY_ENTER) {
			fieldChangeNotify(0);
			//showView();
			return true;
		}

		return super.keyChar(character, status, time);
	}

	protected boolean touchEvent(TouchEvent message) {

		if (message.getEvent() == TouchEvent.CLICK) {
			fieldChangeNotify(0);
			//showView();
			return true;
		}

		return super.touchEvent(message);
	}

	protected void onFocus(int direction) {
		super.onFocus(direction);
		invalidate();
	}

	protected void onUnfocus() {
		super.onUnfocus();
		invalidate();
	}
}




*/
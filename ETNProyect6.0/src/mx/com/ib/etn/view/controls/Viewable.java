package mx.com.ib.etn.view.controls;

import org.json.me.JSONObject;

public interface Viewable {

	public final static int ROUTE = 1;
	public final static int DEPART = 2;
	public final static int ARRIVE = 3;
	public final static int SCHEDULED = 4;
	public final static int VROUTE = 5;
	public final static int CREATE = 6;
	
	public final static int ID_SECUENCIA = 7;
	
	
	public final static int SEE_DIAGRAM = 10;
	public final static int SEE_OCCUPANCY = 11;

	public void setData(int request, JSONObject jsObject);
	public void sendMessage(String message);
}

package mx.com.ib.etn.view.controls;

import mx.com.ib.etn.utils.UtilColor;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.LabelField;

public class AnuncioLabelField extends LabelField{

	private String text;
	int width = 0;
	
	public AnuncioLabelField(String text){
		super(text, LabelField.NON_FOCUSABLE|DrawStyle.HCENTER);
		this.text = text;
		width = Display.getWidth();
	}
	
	
	public AnuncioLabelField(String text, long style ){
		super(text, style);
		this.text = text;
		width = Display.getWidth();
	}
	
	
	public void paint(Graphics graphics) {

		graphics.setColor(UtilColor.TITLE_STRING_BLACK);
		//graphics.drawText(text, 0, 0, DrawStyle.HCENTER, width);

		super.paint(graphics);
	}
}

package mx.com.ib.etn.view.controls.temp;

import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.LabelField;

public class WhiteLabelField extends LabelField{

	
	public WhiteLabelField (Object text){
		super(text);
	}
	
	protected void paint(Graphics graphics) {
		
		graphics.setBackgroundColor(Color.BLUE);
		graphics.setColor(Color.AQUA);
		super.paint(graphics);
	}
}

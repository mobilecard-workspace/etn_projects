package mx.com.ib.etn.view.controls;

import mx.com.ib.etn.utils.UtilColor;
import mx.com.ib.etn.utils.UtilIcon;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.FontFamily;
import net.rim.device.api.ui.Screen;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;


public class CustomMainScreen extends MainScreen{

	final public static String PRECIOS_IVA = "* Precio(s) con IVA incluido";
	
	protected Font font = null;
	private int width;
	private int height;

	public CustomMainScreen(boolean isSetTitle, String title){
		super(Screen.DEFAULT_CLOSE);
		setBackground(UtilColor.BACKGROUND);
		width = Display.getWidth();
		height = Display.getHeight();

		if (isSetTitle){

			add(new LabelField(""));
			BitmapField bitmapField = UtilIcon.getTitle();
			add(bitmapField);
			add(new LabelField(""));
			TitleOrangeRichTextField orangeRichTextField01 = new TitleOrangeRichTextField(title);

			add(orangeRichTextField01);

		}
	}
	
	public int getPreferredWidth(){
		return width;
	}

	public boolean onSavePrompt(){
	    return true;
	} 
	
	
	public void setBackground(int color){
		
    	VerticalFieldManager manager = null;
    	Background background = null;
    	
    	manager = (VerticalFieldManager)getMainManager();
		background = BackgroundFactory.createSolidBackground(color);
		manager.setBackground(background);
	}

	
	public Font setFont(){
		
		Font font = null;  
		FontFamily alphaSansFamily;
		try {
			alphaSansFamily = FontFamily.forName("BBAlpha Sans Condensed");
		   	font = alphaSansFamily.getFont(Font.PLAIN, 7, Ui.UNITS_pt);
			setFont(font);
		} catch (ClassNotFoundException e) {
			Dialog.alert("No se pudo cargar la Fuente");
		}
	   	
	   	return font;
	}
}

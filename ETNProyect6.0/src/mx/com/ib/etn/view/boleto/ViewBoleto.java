package mx.com.ib.etn.view.boleto;

import java.util.Vector;

import mx.com.ib.etn.beans.beans.Boleto;
import mx.com.ib.etn.model.creatorbeans.fromjs.JSNits;
import mx.com.ib.etn.utils.UtilIcon;
import mx.com.ib.etn.view.controls.Viewable;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.component.SeparatorField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

import org.json.me.JSONException;
import org.json.me.JSONObject;

public class ViewBoleto extends MainScreen implements Viewable {

	
	Vector boletos;
	String nit;

	Vector nits;
	
	public ViewBoleto(Boleto boleto){
	
		BitmapField titleBoleto = UtilIcon.getTitleBoleto();
		add(titleBoleto);

		
		Bitmap bodyBoleto = UtilIcon.getBodyBoletoBitmap();
		
		
		Background bkBodyBoleto = BackgroundFactory.createBitmapBackground(bodyBoleto);
		
		//add(bodyBoleto);
		
		VerticalFieldManager fieldManager = new VerticalFieldManager(VerticalFieldManager.USE_ALL_HEIGHT);
		fieldManager.setBackground(bkBodyBoleto);

		fieldManager.add(new LabelField(""));
		
		//fieldManager.add(new SeparatorField());

		BoletoDataRichTextField nit = new BoletoDataRichTextField("Nit", boleto.getNit());
		fieldManager.add(nit);

		BoletoDataRichTextField nomOperación = new BoletoDataRichTextField("No. Operación", boleto.getOperacion());
		fieldManager.add(nomOperación);

		BoletoDataRichTextField origen = new BoletoDataRichTextField("Origen", boleto.getOrigen());
		fieldManager.add(origen);

		BoletoDataRichTextField destino = new BoletoDataRichTextField("Destino", boleto.getDestino());
		fieldManager.add(destino);
		
		BoletoDataRichTextField salida = new BoletoDataRichTextField("Salida", boleto.getFecha() + " " + boleto.getHora());
		fieldManager.add(salida);
		
		BoletoDataRichTextField tipoServicio = new BoletoDataRichTextField("Tipo Servicio", boleto.getTipoViaje());
		fieldManager.add(tipoServicio);
		
		BoletoDataRichTextField pasajero = new BoletoDataRichTextField("Pasajero", boleto.getNombre());
		fieldManager.add(pasajero);
		
		BoletoDataRichTextField asiento = new BoletoDataRichTextField("Asiento", boleto.getAsiento());
		fieldManager.add(asiento);
		
		BoletoDataRichTextField tipoPasajero = new BoletoDataRichTextField("Tipo Pasajero", boleto.getTipoAsiento());
		fieldManager.add(tipoPasajero);
		
		BoletoDataRichTextField monto = new BoletoDataRichTextField("Monto", boleto.getImporte());
		fieldManager.add(monto);
		
		BoletoDataRichTextField formaPago = new BoletoDataRichTextField("Forma de Pago", boleto.getOperacion());
		fieldManager.add(formaPago);
		
		BoletoDataRichTextField afiliacion = new BoletoDataRichTextField("Afiliacion", boleto.getAfiliacion());
		fieldManager.add(afiliacion);
		//fieldManager.add(new SeparatorField());
		add(fieldManager);
	}
	
	
	public void setData(int request, JSONObject jsObject) {
		
		String js = jsObject.toString();
		
		JSNits jsNits = new JSNits();
		try {
			Vector nits = jsNits.getUnitVector(jsObject);
			System.out.println(js);
		} catch (JSONException e) {
			e.printStackTrace();
			Dialog.alert("Fallo al leer el json");
		}
		
		System.out.println(js);
	}

	public void sendMessage(String message) {
		// TODO Auto-generated method stub
		
	}


}


/*
Destino: TEQUISQUIAPAN
Salida: VIERNES 24/05/2013 09:30
Tipo Servicio: DE LUJO
Pasajero: PRUEBA
Asiento: 4
Tipo Pasajero: INSEN
Monto: $7.50
Forma de Pago: TB
Afiliacion: 3742228
*/
//this.setBackground(background);

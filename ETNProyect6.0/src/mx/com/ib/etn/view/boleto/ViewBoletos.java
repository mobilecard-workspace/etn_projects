package mx.com.ib.etn.view.boleto;

import java.util.Vector;

import mx.com.ib.etn.beans.beans.Boleto;
import mx.com.ib.etn.model.creatorbeans.fromjs.JSNits;
import mx.com.ib.etn.view.controls.CustomMainScreen;
import mx.com.ib.etn.view.controls.Viewable;
import net.rim.device.api.ui.component.Dialog;

import org.json.me.JSONException;
import org.json.me.JSONObject;

public class ViewBoletos extends CustomMainScreen implements Viewable {

	public ViewBoletos(){
		
		super(true, "Boletos Adquiridos.");
/*
		BitmapField bitmapField = UtilIcon.getTitle();
		add(bitmapField);
		
		TitleOrangeRichTextField orangeRichTextField = new TitleOrangeRichTextField("Boletos.");
		add(orangeRichTextField);
		add(new LabelField(""));
*/
	}
	
	
	public void setData(int request, JSONObject jsObject) {

		try {
			
			JSNits jsNits = new JSNits();
			
			Vector nits = jsNits.getUnitVector(jsObject);

			int size = nits.size();
			
			for(int index = 0; index < size; index++){

				Boleto boleto = (Boleto)nits.elementAt(index);
				
				CustomGridFieldBoleto gridFieldBoleto = new CustomGridFieldBoleto(boleto);
				add(gridFieldBoleto);
			}

		} catch (JSONException e) {
			e.printStackTrace();
			Dialog.alert("Fallo al leer el json");
		}
	}

	public void sendMessage(String message) {
	}
}

package mx.com.ib.etn.view.scheduled;



import java.util.Date;
import java.util.Vector;

import mx.com.ib.etn.beans.dto.Scheduled;
import mx.com.ib.etn.model.connection.base.ThreadHTTP;
import mx.com.ib.etn.model.creatorbeans.fromjs.JSError;
import mx.com.ib.etn.model.creatorbeans.fromjs.JSSchedule;
import mx.com.ib.etn.model.data.DataKeeper;
import mx.com.ib.etn.model.data.DataTravel;
import mx.com.ib.etn.utils.UtilDate;
import mx.com.ib.etn.view.animated.SplashScreen;
import mx.com.ib.etn.view.controls.AnuncioLabelField;
import mx.com.ib.etn.view.controls.CustomMainScreen;
import mx.com.ib.etn.view.controls.Viewable;
import net.rim.blackberry.api.browser.URLEncodedPostData;
import net.rim.device.api.ui.component.Dialog;

import org.json.me.JSONException;
import org.json.me.JSONObject;

public class ViewScheduledDepart extends CustomMainScreen implements Viewable {

	private Vector fieldManagers;
	private SplashScreen splashScreen;
	boolean isDepartFlight;
	boolean isBasicFlight;

	public ViewScheduledDepart(boolean isDepartFlight, boolean isBasicFlight) {

		super(true, "Selecci�n de horario de salida");

		this.isBasicFlight = isBasicFlight;
		this.isDepartFlight = isDepartFlight;

		add(new AnuncioLabelField(CustomMainScreen.PRECIOS_IVA));
		
		getInfo(isDepartFlight);
	}

	public void getInfo(boolean value) {

		String origen;
		String destino;
		String fechaIni;

		URLEncodedPostData encodedPostData = new URLEncodedPostData(
				URLEncodedPostData.DEFAULT_CHARSET, false);

		DataKeeper dataKeeper = DataKeeper.getInstance();

		DataTravel dataTravel = dataKeeper.getDataTravel();

		if (value) {

			origen = dataTravel.getIdDepart();
			destino = dataTravel.getIdArrive();
			Date date = dataTravel.getDateDepart();
			fechaIni = UtilDate.getDateToDDMMAAAA(date);

		} else {
			origen = dataTravel.getIdArrive();
			destino = dataTravel.getIdDepart();
			Date date = dataTravel.getDateArrive();
			fechaIni = UtilDate.getDateToDDMMAAAA(date);
		}

		splashScreen = SplashScreen.getInstance();

		encodedPostData.append("origen", origen);
		encodedPostData.append("destino", destino);
		encodedPostData.append("fechaIni", fechaIni);
		encodedPostData.append("numAdulto",
				Integer.toString(dataTravel.getiAdults()));
		encodedPostData.append("numNino",
				Integer.toString(dataTravel.getiChildren()));
		encodedPostData.append("numInsen",
				Integer.toString(dataTravel.getiElderlies()));
		encodedPostData.append("numEst",
				Integer.toString(dataTravel.getiStudents()));
		encodedPostData.append("numMto",
				Integer.toString(dataTravel.getiProfessors()));
		encodedPostData.append("numPaisano", "0");

		ThreadHTTP threadHTTP = new ThreadHTTP();
		threadHTTP.setHttpConfigurator(Viewable.SCHEDULED, this,
				encodedPostData);
		threadHTTP.start();

	}


	public void sendMessage(String message) {
		splashScreen.remove();
		Dialog.alert(message);
	}
	
	
	public void setData(int request, JSONObject jsObject) {

		JSSchedule jsSchedule = new JSSchedule();

		try {

			Vector schedules = jsSchedule.getVectorSchedule(jsObject);
			int size = schedules.size();

			fieldManagers = new Vector();
			
			for (int i = 0; i < size; i++) {

				Object object = schedules.elementAt(i);
				
				if (object instanceof JSError){
					
					JSError jsError = (JSError)schedules.elementAt(i);
					
					Dialog.alert(jsError.getError());
					
				} else {
					
					Scheduled scheduled = (Scheduled) schedules.elementAt(i);
					CustomGridFieldScheduledDepart fieldManager = new CustomGridFieldScheduledDepart(isDepartFlight, isBasicFlight, scheduled);
					
					fieldManagers.addElement(fieldManager);
					add(fieldManager);
				}
			}

			splashScreen.remove();
			
		} catch (JSONException e) {
			e.printStackTrace();
			Dialog.alert("Error al leer la informacion");
		}
	}
}

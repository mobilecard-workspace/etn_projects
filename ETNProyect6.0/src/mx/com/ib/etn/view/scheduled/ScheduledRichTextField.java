package mx.com.ib.etn.view.scheduled;

import mx.com.ib.etn.beans.dto.Scheduled;
import mx.com.ib.etn.utils.UtilColor;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;

public class ScheduledRichTextField extends RichTextField {

	final public static int DATE_DEPART = 0;
	final public static int DATE_ARRIVE = 1;
	final public static int DATE_LINE = 2;
	final public static int DATE_CHARGE = 3;
	final public static int DATE_SERVICE = 4;
	
	final private String dateDepart = "Partida: ";
	final private String dateArrive = "Llegada: ";
	final private String line = "Linea: ";
	final private String charge = "Costo: ";
	final private String servicio = "Servicio: ";
	
	private String showData = "";
	private String data = "";
	private Scheduled scheduled;
	//private int type;

	
	public Scheduled getScheduled(){
		return scheduled;
	}
	
	public ScheduledRichTextField(Scheduled scheduled, int type) {

		super("", RichTextField.NON_FOCUSABLE | RichTextField.USE_ALL_HEIGHT);
		
		this.scheduled = scheduled;
		
		this.data = data;
		showData = dateDepart + data;
		
		switch (type) {
		case ScheduledRichTextField.DATE_DEPART:
			showData = dateDepart;
			data = scheduled.getDateDepart() + " " + scheduled.getHourDepart();
			break;
		case ScheduledRichTextField.DATE_ARRIVE:
			showData = dateArrive;
			data = scheduled.getDateArrive() + " " + scheduled.getHourArrive();
			break;
		case ScheduledRichTextField.DATE_LINE:
			showData = line;
			data = scheduled.getCompanyFlight();
			break;
		case ScheduledRichTextField.DATE_CHARGE:
			showData = charge;
			data = scheduled.getCharge();
			if (scheduled.isIva()){
				data = data + "*";
			}
			break;
		case ScheduledRichTextField.DATE_SERVICE:
			showData = servicio;
			data = scheduled.getServicio();
			break;
		}
	}


	public void paint(Graphics graphics) {

		graphics.setColor(UtilColor.ELEMENT_STRING_TITLE);
		graphics.drawText(showData, 0, 0, DrawStyle.RIGHT,
				Display.getWidth() / 4);
		graphics.setColor(UtilColor.ELEMENT_STRING_DATA);
		graphics.drawText(data, Display.getWidth() / 4, 0, DrawStyle.LEFT,
				Display.getWidth());

		super.paint(graphics);
	}
}


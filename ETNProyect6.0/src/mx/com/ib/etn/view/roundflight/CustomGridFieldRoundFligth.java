package mx.com.ib.etn.view.roundflight;

import mx.com.ib.etn.beans.dto.Router;
import mx.com.ib.etn.beans.dto.Scheduled;
import mx.com.ib.etn.utils.UtilColor;
import mx.com.ib.etn.view.passengers.ViewPassengers;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class CustomGridFieldRoundFligth extends VerticalFieldManager {

	
	private Router routerDepart;
	private Router routerArrive;
	private Scheduled scheduled;
	private boolean isDepartFlight;
	private boolean isBasicFlight;
	
	public CustomGridFieldRoundFligth(boolean isDepartFlight, boolean isBasicFlight, Router routerDepart, Router routerArrive, Scheduled scheduled) {

		super();

		this.isDepartFlight = isDepartFlight;
		this.isBasicFlight = isBasicFlight;

		this.routerDepart = routerDepart;
		this.routerArrive = routerArrive;
		this.scheduled = scheduled;

		RoundFlightRichTextField sDepart = new RoundFlightRichTextField(routerDepart, routerArrive, scheduled, RoundFlightRichTextField.DEPART);
		RoundFlightRichTextField sArrive = new RoundFlightRichTextField(routerDepart, routerArrive, scheduled, RoundFlightRichTextField.ARRIVE);
		RoundFlightRichTextField sDateDepart = new RoundFlightRichTextField(routerDepart, routerArrive, scheduled, RoundFlightRichTextField.DATE_DEPART);
		RoundFlightRichTextField sDateArrive = new RoundFlightRichTextField(routerDepart, routerArrive, scheduled, RoundFlightRichTextField.DATE_ARRIVE);
		RoundFlightRichTextField sService = new RoundFlightRichTextField(routerDepart, routerArrive, scheduled, RoundFlightRichTextField.DATE_SERVICE);

		add(sDepart);
		add(sArrive);
		add(sDateDepart);
		add(sDateArrive);
		add(sService);
		add(new NullField());
	}

	protected void paint(Graphics graphics) {

		graphics.clear();

		if (isFocus()) {
			graphics.setColor(UtilColor.ELEMENT_FOCUS);
			graphics.setGlobalAlpha(200);
			graphics.fillRoundRect(0, 0, getPreferredWidth(),
					getPreferredHeight(), 1, 1);
			graphics.setGlobalAlpha(255);
		} else {
			graphics.setColor(UtilColor.ELEMENT_BACKGROUND);
			graphics.fillRoundRect(0, 0, getPreferredWidth(),
					getPreferredHeight(), 1, 1);
		}

		super.paint(graphics);
	}

	public boolean isFocusable() {
		return true;
	}

	public int getPreferredWidth() {
		return Display.getWidth();
	}

	protected boolean navigationClick(int status, int time) {
		fieldChangeNotify(0);
		showView();
		return super.navigationClick(status, time);
	}

	protected boolean keyChar(char character, int status, int time) {
		if (character == Keypad.KEY_ENTER) {
			fieldChangeNotify(0);
			showView();
			return true;
		}

		/*
		 * else if (character == Keypad.KEY_ESCAPE){ Dialog.alert("Salir"); }
		 */
		return super.keyChar(character, status, time);
	}

	protected boolean touchEvent(TouchEvent message) {

		if (message.getEvent() == TouchEvent.CLICK) {
			fieldChangeNotify(0);
			showView();
			return true;
		}

		return super.touchEvent(message);
	}

	protected void onFocus(int direction) {
		super.onFocus(direction);
		invalidate();
	}

	protected void onUnfocus() {
		super.onUnfocus();
		invalidate();
	}
	
	private void showView(){

		UiApplication.getUiApplication().pushScreen(new ViewPassengers(isDepartFlight, isBasicFlight, scheduled.isIva()));
	}
	
	
}

package mx.com.ib.etn.view.registrer;

import java.io.UnsupportedEncodingException;
import java.util.Date;

import mx.com.ib.etn.beans.beans.GeneralBean;
import mx.com.ib.etn.beans.beans.UserBean;
import mx.com.ib.etn.model.connection.Url;
import mx.com.ib.etn.model.connection.addcel.implementation.RegisterThread;
import mx.com.ib.etn.model.connection.addcel.implementation.Terminos;
import mx.com.ib.etn.utils.UtilBB;
import mx.com.ib.etn.utils.UtilDate;
import mx.com.ib.etn.utils.UtilSecurity;
import mx.com.ib.etn.view.controls.CustomMainScreen;
import mx.com.ib.etn.view.controls.Viewable;
import mx.com.ib.etn.view.controls.WhiteLabelField;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.CheckboxField;
import net.rim.device.api.ui.component.DateField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

import org.json.me.JSONException;
import org.json.me.JSONObject;


public class ViewRegistrer extends CustomMainScreen implements FieldChangeListener, Viewable{

	LabelField lnombreUsuario;
	LabelField lnumeroCelular01;
	LabelField lconfNumeroCelular;
	LabelField lemail;
	LabelField lconfirmarEmail;
	LabelField lproveedor;

	LabelField lnombre;
	LabelField lapellidoPaterno;
	LabelField lapellidoMaterno;
	LabelField lsexo;
	LabelField lfechaNacimiento;

	LabelField ltelefonoCasa;
	LabelField ltelefonoOficina;

	LabelField lestado;
	LabelField lciudad;
	LabelField lcalle;

	LabelField lnumExterior;
	LabelField lnumInterior;
	LabelField lcolonia;
	LabelField lCP;

	LabelField lnumeroTarjeta;
	LabelField ltipoTarjeta;
	LabelField ldomicilioTarjeta;

	LabelField lAnioTarjeta;
	LabelField lMesTarjeta;



	EditField nombreUsuario;
	EditField numeroCelular01;
	EditField confNumeroCelular;
	EditField email;
	EditField confirmarEmail;
	ObjectChoiceField proveedor;

	EditField nombre;
	EditField apellidoPaterno;
	EditField apellidoMaterno;
	ObjectChoiceField sexo;
	DateField fechaNacimiento;

	EditField telefonoCasa;
	EditField telefonoOficina;

	ObjectChoiceField estado;
	EditField ciudad;
	EditField calle;

	EditField numExterior;
	EditField numInterior;
	EditField colonia;
	EditField CP;

	EditField numeroTarjeta;
	ObjectChoiceField tipoTarjeta;
	EditField domicilioTarjeta;

	ObjectChoiceField anioTarjeta;
	ObjectChoiceField mesTarjeta;
	
	
	public CheckboxField acceptTerms = null;
	
	ButtonField terminos;
	ButtonField aceptar;

	private GeneralBean[] EdoBeans = null;
	private GeneralBean[] bankBeans = null;
	private GeneralBean[] creditBeans = null;
	private GeneralBean[] providersBeans = null;
	
	
	public ViewRegistrer(GeneralBean[] EdoBeans, GeneralBean[] bankBeans, GeneralBean[] creditBeans, GeneralBean[] providersBeans) {

		super(true, "Registro.");
		this.EdoBeans = EdoBeans;
		this.bankBeans = bankBeans;
		this.creditBeans = creditBeans;
		this.providersBeans = providersBeans;

		setVariables();
		addVariables();

	}

	private void setVariables() {

		lnombreUsuario = new WhiteLabelField("Nombre de usuario:",
				LabelField.NON_FOCUSABLE);
		lnumeroCelular01 = new WhiteLabelField("N�mero de celular:",
				LabelField.NON_FOCUSABLE);
		lconfNumeroCelular = new WhiteLabelField("Confirmar n�mero de celular:",
				LabelField.NON_FOCUSABLE);
		lemail = new WhiteLabelField("Correo electr�nico:", LabelField.NON_FOCUSABLE);
		lconfirmarEmail = new WhiteLabelField("Confirmar correo electr�nico:",
				LabelField.NON_FOCUSABLE);
		lproveedor = new WhiteLabelField("Proveedor:", LabelField.NON_FOCUSABLE);

		lnombre = new WhiteLabelField("Nombre:", LabelField.NON_FOCUSABLE);
		lapellidoPaterno = new WhiteLabelField("Apellido paterno:",
				LabelField.NON_FOCUSABLE);
		lapellidoMaterno = new WhiteLabelField("Apellido materno:",
				LabelField.NON_FOCUSABLE);
		lsexo = new WhiteLabelField("Sexo:", LabelField.NON_FOCUSABLE);
		lfechaNacimiento = new WhiteLabelField("Fecha de nacimiento:",
				LabelField.NON_FOCUSABLE);


		ltelefonoCasa = new WhiteLabelField("Tel�fono de casa:",
				LabelField.NON_FOCUSABLE);
		ltelefonoOficina = new WhiteLabelField("Tel�fono de oficina:",
				LabelField.NON_FOCUSABLE);

		lestado = new WhiteLabelField("Estado:", LabelField.NON_FOCUSABLE);
		lciudad = new WhiteLabelField("Ciudad:", LabelField.NON_FOCUSABLE);
		lcalle = new WhiteLabelField("Calle:", LabelField.NON_FOCUSABLE);

		lnumExterior = new WhiteLabelField("N�mero exterior:",
				LabelField.NON_FOCUSABLE);
		lnumInterior = new WhiteLabelField("N�mero interior:",
				LabelField.NON_FOCUSABLE);
		lcolonia = new WhiteLabelField("Colonia:", LabelField.NON_FOCUSABLE);
		lCP = new WhiteLabelField("C�digo postal:", LabelField.NON_FOCUSABLE);

		lnumeroTarjeta = new WhiteLabelField("N�mero de tarjeta:",
				LabelField.NON_FOCUSABLE);
		ltipoTarjeta = new WhiteLabelField("Tipo de tarjeta:",
				LabelField.NON_FOCUSABLE);
		ldomicilioTarjeta = new WhiteLabelField(
				"Domicilio de estado de cuenta de tarjeta:",
				LabelField.NON_FOCUSABLE);
		
		//---------------------------------------------------------------------------------------------
		//---------------------------------------------------------------------------------------------
		//---------------------------------------------------------------------------------------------		
		
		
		Background background = BackgroundFactory.createSolidBackground(Color.WHITE);
		
		nombreUsuario = new EditField("", "");
		//nombreUsuario.setBackground(background);
		numeroCelular01 = new EditField("", "");
		//numeroCelular01.setBackground(background);
		confNumeroCelular = new EditField("", "");
		email = new EditField("", "");
		confirmarEmail = new EditField("", "");

		//String proveedores[] = { "Iusacell", "Movistar", "Telcel" };

		proveedor = new ObjectChoiceField("", providersBeans, 0);

		nombre = new EditField("", "");
		apellidoPaterno = new EditField("", "");
		apellidoMaterno = new EditField("", "");

		//String sexos[] = { "Masculino", "Femenino" };
		String sexos[] = { "Seleccione","FEMENINO","MASCULINO" };
		sexo = new ObjectChoiceField("", sexos, 0);

		fechaNacimiento = new DateField("", new Date().getTime(),
				DateField.DATE);

		telefonoCasa = new EditField("", "");
		telefonoOficina = new EditField("", "");

		estado = new ObjectChoiceField("", EdoBeans, 16);

		ciudad = new EditField("", "");
		calle = new EditField("", "");

		numExterior = new EditField("", "");
		numInterior = new EditField("", "");
		colonia = new EditField("", "");
		CP = new EditField("", "");

		numeroTarjeta = new EditField("", "");

		tipoTarjeta = new ObjectChoiceField("", creditBeans, 0);
		domicilioTarjeta = new EditField("", "");
		
		
		
		
		
		
		
		
		nombreUsuario.setBackground(background);
		numeroCelular01.setBackground(background);
		confNumeroCelular.setBackground(background);
		email.setBackground(background);
		confirmarEmail.setBackground(background);


		nombre.setBackground(background);
		apellidoPaterno.setBackground(background);
		apellidoMaterno.setBackground(background);

		telefonoCasa.setBackground(background);
		telefonoOficina.setBackground(background);

		ciudad.setBackground(background);
		calle.setBackground(background);

		numExterior.setBackground(background);
		numInterior.setBackground(background);
		colonia.setBackground(background);
		CP.setBackground(background);

		numeroTarjeta.setBackground(background);
		domicilioTarjeta.setBackground(background);

		
		
		
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------		
 
		/*
		nombreUsuario = new EditField("", "");
		numeroCelular01 = new EditField("", "");
		confNumeroCelular = new EditField("", "");
		email = new EditField("", "");
		confirmarEmail = new EditField("", "");

		//String proveedores[] = { "Iusacell", "Movistar", "Telcel" };

		proveedor = new ObjectChoiceField("", providersBeans, 0);

		nombre = new EditField("", "");
		apellidoPaterno = new EditField("", "");
		apellidoMaterno = new EditField("", "");

		//String sexos[] = { "Masculino", "Femenino" };
		String sexos[] = { "Seleccione","FEMENINO","MASCULINO" };
		sexo = new ObjectChoiceField("", sexos, 0);

		fechaNacimiento = new DateField("", new Date().getTime(),
				DateField.DATE);

		curp = new EditField("", "");
		rfc = new EditField("", "");
		placas = new EditField("", "");
		cuentaPredial = new EditField("", "");

		telefonoCasa = new EditField("", "");
		telefonoOficina = new EditField("", "");

		estado = new ObjectChoiceField("", EdoBeans, 16);

		ciudad = new EditField("", "");
		calle = new EditField("", "");

		numExterior = new EditField("", "");
		numInterior = new EditField("", "");
		colonia = new EditField("", "");
		CP = new EditField("", "");

		numeroTarjeta = new EditField("", "");

		tipoTarjeta = new ObjectChoiceField("", creditBeans, 0);
		domicilioTarjeta = new EditField("", "");
		*/
		
		lAnioTarjeta =  new WhiteLabelField("A�o Vencimiento:", LabelField.NON_FOCUSABLE); 
		lMesTarjeta =  new WhiteLabelField("Mes Vencimiento:", LabelField.NON_FOCUSABLE);
		
		
		String meses[] = {"enero", "febrero", "marzo", 
						  "abril", "mayo", "junio", "julio", 
						  "agosto", "septiembre", "octubre", 
						  "noviembre", "diciembre"};
		
		mesTarjeta = new ObjectChoiceField("", meses, 6);
		
		String anios[] = UtilDate.getYears(5);
		anioTarjeta = new ObjectChoiceField("", anios, 0);
		
		acceptTerms = new CheckboxField("Acepto terminos y condiciones.", false);
		
		terminos =  new ButtonField("T�rminos y Condiciones", ButtonField.CONSUME_CLICK);
		terminos.setChangeListener(this);
		
		aceptar = new ButtonField("Aceptar", ButtonField.CONSUME_CLICK);
		aceptar.setChangeListener(this);
	}

	private void addVariables() {

		add(lnombreUsuario);
		add(nombreUsuario);
		add(lnumeroCelular01);
		add(numeroCelular01);
		add(lconfNumeroCelular);
		add(confNumeroCelular);
		add(lemail);
		add(email);
		add(lconfirmarEmail);
		add(confirmarEmail);
		add(lproveedor);
		add(proveedor);

		add(lnombre);
		add(nombre);
		add(lapellidoPaterno);
		add(apellidoPaterno);
		add(lapellidoMaterno);
		add(apellidoMaterno);
		add(lsexo);
		add(sexo);
		add(lfechaNacimiento);
		add(fechaNacimiento);

		add(ltelefonoCasa);
		add(telefonoCasa);
		add(ltelefonoOficina);
		add(telefonoOficina);

		add(lestado);
		add(estado);
		add(lciudad);
		add(ciudad);
		add(lcalle);
		add(calle);

		add(lnumExterior);
		add(numExterior);
		add(lnumInterior);
		add(numInterior);
		add(lcolonia);
		add(colonia);
		add(lCP);
		add(CP);

		add(lnumeroTarjeta);
		add(numeroTarjeta);
		add(ltipoTarjeta);
		add(tipoTarjeta);
		
		add(lMesTarjeta);
		add(mesTarjeta);
		add(lAnioTarjeta);
		add(anioTarjeta);

		add(ldomicilioTarjeta);
		add(domicilioTarjeta);
		
		add(acceptTerms);
		
		add(terminos);
		add(aceptar);
	}

	private boolean verificarDatos() {

		boolean value = true;

		if ((nombreUsuario.getText().length() < 8)||(nombreUsuario.getText().length() > 16)) {
			Dialog.alert("El nombre de usuario debe tener entre 8 y 16 caracteres.");
			value = false;
		} else if (nombreUsuario.getText().length() == 0) {
			Dialog.alert("Falto introducir el usuario.");
			value = false;
		} else if (numeroCelular01.getText().length() == 0) {
			Dialog.alert("Falto introducir el n�mero de celular.");
			value = false;
		} else if (confNumeroCelular.getText().length() == 0) {
			Dialog.alert("Falto introducir la confirmaci�n del n�mero de celular.");
			value = false;
		} else if (email.getText().length() == 0) {
			Dialog.alert("Falto introducir el correo electr�nico.");
			value = false;
		} else if (confirmarEmail.getText().length() == 0) {
			Dialog.alert("Falto introducir la confirmaci�n del correo electr�nico.");
			value = false;
		} else if (nombre.getText().length() == 0) {
			Dialog.alert("Falto introducir el nombre.");
			value = false;
		} else if (apellidoPaterno.getText().length() == 0) {
			Dialog.alert("Falto introducir el apellido paterno.");
			value = false;
		} else if (apellidoMaterno.getText().length() == 0) {
			Dialog.alert("Falto introducir el apellido materno.");
			value = false;
		} else if (telefonoCasa.getText().length() == 0) {
			Dialog.alert("Falto introducir el tel�fono de la casa.");
			value = false;
		} else if (telefonoOficina.getText().length() == 0) {
			Dialog.alert("Falto introducir el tel�fono de la oficina.");
			value = false;
		} else if (ciudad.getText().length() == 0) {
			Dialog.alert("Falto introducir la ciudad.");
			value = false;
		} else if (calle.getText().length() == 0) {
			Dialog.alert("Falto introducir la calle.");
			value = false;
		} else if (numExterior.getText().length() == 0) {
			Dialog.alert("Falto introducir el n�mero exterior.");
			value = false;
		} else if (numInterior.getText().length() == 0) {
			Dialog.alert("Falto introducir el n�mero interior.");
			value = false;
		} else if (colonia.getText().length() == 0) {
			Dialog.alert("Falto introducir la colonia.");
			value = false;
		} else if (CP.getText().length() == 0) {
			Dialog.alert("Falto introducir el C�digo postal.");
			value = false;
		} else if (numeroTarjeta.getText().length() == 0) {
			Dialog.alert("Falto introducir el n�mero de tarjeta.");
			value = false;
		} else if (domicilioTarjeta.getText().length() == 0) {
			Dialog.alert("Falto introducir el domicilio del estado de cuenta.");
			value = false;
		} else if (getDataFromChoice(sexo).equals("Seleccione")){
			Dialog.alert("Falto seleccionar el sexo del usuario.");
			value = false;
		}

		return value;
	}

	public void fieldChanged(Field field, int context) {

		if (field == aceptar) {
			
			if (acceptTerms.getChecked()){
				
				if (verificarDatos()){
					crearRegistro();
				}
				
			} else {
				
				Dialog.alert("Debe de aceptar los terminos y condiciones.");
			}
		} else if (field == terminos){
			
			Terminos terminos = new Terminos(null, Url.URL_TERMS, this);
			terminos.run();
		}

	}
	
	private void crearRegistro(){
		
		try {
				String parsePass02 = UtilSecurity.parsePass(UserBean.passTEMP);
				String userJson02 = generateUserJSON();
				String n = UtilSecurity.aesEncrypt(parsePass02, userJson02);
				String n2 = UtilSecurity.mergeStr(n, UserBean.passTEMP);
				new RegisterThread(n2, this).run();
		} catch (Exception e) {
			System.out.println("error " + e.toString());
			Dialog.alert("Ocurri� un error. Int�ntelo de nuevo m�s tarde");
		}
	}
	
	private String createUserJson(String pass){
		
		String value = "{\"login\":\"" + nombreUsuario.getText().trim() + 
						"\", \"password\":\"" + pass + 
						"\"}";
		return value;
	}
	
	
	public String generateUserJSON() throws UnsupportedEncodingException {

		StringBuffer userJSON = new StringBuffer();
		
		userJSON.append("{\"login\":\"").append(nombreUsuario.getText().trim());
		userJSON.append("\",\"password\":\"").append(UserBean.passTEMP);
		
		userJSON.append("\",\"nacimiento\":\"").append(UtilDate.getYYYY_MM_DDFromLong(fechaNacimiento.getDate()));
		userJSON.append("\",\"telefono\":\"").append(numeroCelular01.getText().trim());
		userJSON.append("\",\"nombre\":\"").append(nombre.getText().trim());
		userJSON.append("\",\"apellido\":\"").append(apellidoPaterno.getText().trim());
		userJSON.append("\",\"ciudad\":\"").append(ciudad.getText());
		userJSON.append("\",\"tarjeta\":\"").append(numeroTarjeta.getText().trim());
		userJSON.append("\",\"terminos\":\"1");
		userJSON.append("\",\"registro\":\"").append(UtilDate.getDateToDDMMAAAA(new Date()));
		userJSON.append("\",\"mail\":\"").append(email.getText().trim());
		userJSON.append("\",\"materno\":\"").append(apellidoMaterno.getText().trim());

		userJSON.append("\",\"sexo\":\"").append(getDataFromChoice(sexo));
		userJSON.append("\",\"tel_casa\":\"").append(telefonoCasa.getText().trim());
		userJSON.append("\",\"tel_oficina\":\"").append(telefonoOficina.getText().trim());
		userJSON.append("\",\"id_estado\":\"").append(getDataFromChoice(estado));
		userJSON.append("\",\"ciudad\":\"").append(ciudad.getText().trim());
		userJSON.append("\",\"calle\":\"").append(calle.getText().trim());
		userJSON.append("\",\"num_ext\":\"").append(numExterior.getText().trim());
		userJSON.append("\",\"num_interior\":\"").append(numInterior.getText().trim());
		userJSON.append("\",\"colonia\":\"").append(colonia.getText().trim());
		userJSON.append("\",\"cp\":\"").append(CP.getText().trim());


		if (tipoTarjeta.getSelectedIndex() == 2) {
			userJSON.append("\",\"dom_amex\":\"").append(domicilioTarjeta.getText().trim());
		} else {
			userJSON.append("\",\"dom_amex\":\"");
		}

		
		int numMonth = mesTarjeta.getSelectedIndex() + 1;

		String stringMonth = String.valueOf(numMonth);

		if (numMonth < 10) {
			stringMonth = "0" + stringMonth;
		}		
		
		String SAnioTarjeta = getDataFromChoice(anioTarjeta);
		SAnioTarjeta = SAnioTarjeta.substring((SAnioTarjeta.length() - 2), SAnioTarjeta.length());
		userJSON.append("\",\"vigencia\":\"").append(stringMonth).append("/").append(SAnioTarjeta);
		
		//userJSON.append("\",\"banco\":\"").append(proveedor.getSelectedIndex());
		
		userJSON.append("\",\"banco\":\"").append(getDataFromChoice(proveedor));
		userJSON.append("\",\"tipotarjeta\":\"").append(getDataFromChoice(tipoTarjeta));
		userJSON.append("\",\"proveedor\":\"").append(getDataFromChoice(proveedor));
		
		userJSON.append("\",\"imei\":\"").append(UtilBB.getImei());
		userJSON.append("\",\"etiqueta\":\"\",\"numero\":\"0\",\"dv\":\"0\",\"idtiporecargatag\":\"1");
		userJSON.append("\",\"tipo\":\"").append(UtilBB.getDeviceINFO(UtilBB.MANUFACTURER_NAME));
		userJSON.append("\",\"software\":\"").append(UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION));
		userJSON.append("\",\"modelo\":\"").append(UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME));
		userJSON.append("\",\"key\":\"").append(UtilBB.getImei() + "\"");
		userJSON.append(",\"status\":1}");
		System.out.println(userJSON.toString());
		return userJSON.toString();
	}


	private String getDataFromChoice(ObjectChoiceField choiceField){

		int iValue = choiceField.getSelectedIndex();
		Object oValue = (Object)choiceField.getChoice(iValue);

		String sValue = null;

		if (oValue instanceof GeneralBean){

			GeneralBean bean = (GeneralBean)oValue;
			sValue = bean.getClave();
		} else {

			sValue = (String)oValue;
		}

		return sValue;
	}

	public void setData(int request, JSONObject jsObject) {

		/*
		{"resultado":5,"mensaje":"No es posible registrar el usuario."}
		*/
		
		String value;
		try {
			value = jsObject.getString("mensaje");
			Dialog.alert(value);
		} catch (JSONException e) {
			Dialog.alert("Error al leer la respuesta.");
		}
		
	}

	public void sendMessage(String message) {
		// TODO Auto-generated method stub
	}
	
}


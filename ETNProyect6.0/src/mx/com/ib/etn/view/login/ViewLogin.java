package mx.com.ib.etn.view.login;

import mx.com.ib.etn.model.connection.addcel.InfoThread.Login;
import mx.com.ib.etn.utils.UtilColor;
import mx.com.ib.etn.utils.UtilIcon;
import mx.com.ib.etn.view.animated.SplashScreen;
import mx.com.ib.etn.view.controls.CustomLabelField;
import mx.com.ib.etn.view.controls.CustomMainScreen;
import mx.com.ib.etn.view.controls.Viewable;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.PasswordEditField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import org.json.me.JSONObject;

public class ViewLogin extends CustomMainScreen implements FieldChangeListener,
		Viewable {

	private EditField userEditLogin = null;
	private PasswordEditField userEditPassword = null;
	private ButtonField buttonField = null;

	
	public ViewLogin() {

		super(true, "Autenticarse.");
/*
		BitmapField title = UtilIcon.getTitle();
		add(title);
*/
		HorizontalFieldManager loginFieldManager = new HorizontalFieldManager();
		CustomLabelField userLabelLogin = new CustomLabelField("Usuario: ",
				DrawStyle.LEFT, 1 / 3, UtilColor.TITLE_STRING_BLACK);
		userEditLogin = new EditField();
		loginFieldManager.add(userLabelLogin);
		loginFieldManager.add(userEditLogin);
		add(loginFieldManager);

		HorizontalFieldManager passwordFieldManager = new HorizontalFieldManager();
		CustomLabelField userLabelPassword = new CustomLabelField("Contrase�a: ",
				DrawStyle.LEFT, 1 / 3, UtilColor.TITLE_STRING_BLACK);
		userEditPassword = new PasswordEditField();

		passwordFieldManager.add(userLabelPassword);
		passwordFieldManager.add(userEditPassword);
		add(passwordFieldManager);

		buttonField = new ButtonField("Aceptar", ButtonField.CONSUME_CLICK);
		buttonField.setChangeListener(this);
		add(buttonField);
	}

	public void sendMessage(String message) {

		final String messages = message;
		
	      UiApplication.getUiApplication().invokeLater(new Runnable() 
	      {
	         public void run() 
	         {
	        	 Dialog.alert(messages);
	         }
	      });
		
	}
	
	public void setData(int request, JSONObject jsObject) {}

	
	public void fieldChanged(Field field, int context) {

		if (field == buttonField) {

			String login = userEditLogin.getText();
			String password = userEditPassword.getText();
			
			if (checkFields(login, password)){

				SplashScreen splashScreen = SplashScreen.getInstance();
				splashScreen.start();

				getData(login, password);
			} else {

				sendMessage("Falta informaci�n de el usuario � la contrase�a.");
			}
		}
	}

	
	private boolean checkFields(String login, String password) {

		boolean value = false;

		if ((login != null) && (login.length() > 0) && (password != null) && (password.length() > 0)) {
			value = true;
		}
		
		return value;
	}

	
	private void getData(String login, String password){
		
		Login loginThread = new Login(this, login, password);
		//loginThread.start();
		loginThread.run();
	}
}

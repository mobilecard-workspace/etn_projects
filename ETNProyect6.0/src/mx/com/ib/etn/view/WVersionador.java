package mx.com.ib.etn.view;

import org.json.me.JSONObject;

import mx.com.ib.etn.view.controls.CustomMainScreen;
import mx.com.ib.etn.view.controls.CustomSelectedButtonField;
import mx.com.ib.etn.view.controls.Viewable;
import net.rim.blackberry.api.browser.Browser;
import net.rim.blackberry.api.browser.BrowserSession;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.FontFamily;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.TextField;


public class WVersionador extends CustomMainScreen implements Viewable {

	public FontFamily alphaSansFamily = null;
	
	public WVersionador(String title, final String url) {
		
		super(true, "Nueva versi�n");
		
		try {
			
			alphaSansFamily = FontFamily.forName("BBAlpha Serif");
			LabelField importante = new LabelField("�IMPORTANTE!");
			
			importante.setFont(alphaSansFamily.getFont(Font.PLAIN, 9, Ui.UNITS_pt));
			add(importante);
			
			TextField nueva = new TextField(TextField.READONLY);
			nueva.setText("Hay una nueva versi�n de Tesoreria GDF disponible.");
			nueva.setFont(alphaSansFamily.getFont(Font.PLAIN, 7, Ui.UNITS_pt));
			add(nueva);
			
			CustomSelectedButtonField descarga = new CustomSelectedButtonField("Descargar", ButtonField.CONSUME_CLICK){
				
				protected boolean navigationClick(int status, int time) {
					return execute();
				}
				
				
				protected boolean touchEvent(int status, int time) {
					return execute();
				}
				
				protected boolean trackwheelClick(int status, int time) {
					return execute();
				}
				
				protected boolean execute(){
					BrowserSession browserSession; 
	    			browserSession = Browser.getDefaultSession();
	    			
	    			browserSession.displayPage(url);
					return true;
				}
				
			};
			
			add(descarga);
			
			TextField aclaracion = new TextField(TextField.READONLY|TextField.NON_FOCUSABLE);
			aclaracion.setText(					
					"Dudas o aclaraciones cont�ctanos a soporte@addcel.com " +
					"o al t�lefono en el D.F. y �rea metropolitana 5257 5140 " +
					"o del interior de la rep�blica al 01 800 122 33 24");
			
			aclaracion.setFont(alphaSansFamily.getFont(Font.PLAIN, 5, Ui.UNITS_pt));
			
			add(aclaracion);
			
		} catch (ClassNotFoundException e) {
			Dialog.alert("El sistema no pudo configurarse para ejecutarse.");
			e.printStackTrace();
		}
	}

	public void setData(int request, JSONObject jsObject) {
	}

	public void sendMessage(String message) {
	}
}
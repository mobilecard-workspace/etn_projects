package mx.com.ib.etn.view.seats;

import mx.com.ib.etn.beans.dto.Passenger;
import mx.com.ib.etn.model.creatorbeans.fromjs.JSBusDiagram;
import mx.com.ib.etn.model.data.DataKeeper;
import mx.com.ib.etn.model.data.DataTravel;
import mx.com.ib.etn.utils.UtilIcon;
import mx.com.ib.etn.view.controls.CustomMainScreen;
import mx.com.ib.etn.view.controls.TitleOrangeRichTextField;
import mx.com.ib.etn.view.controls.Viewable;
import mx.com.ib.etn.view.controls.WhiteLabelField;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.GridFieldManager;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

import org.json.me.JSONObject;

//public class ViewSeats extends CustomMainScreen implements FieldChangeListener, Viewable {
public class ViewSeats2 extends CustomMainScreen implements Viewable, FieldChangeListener {

	private String flight;
	private String dateDepart;
	private JSBusDiagram jsBusDiagram;
	private EditField passengerFiled;

	private Passenger passenger;
	private GraphicButtonSeat seats[][];

	private DataKeeper dataKeeper = null;
	private DataTravel dataTravel = null;

	private boolean isDepartFlight;

	private boolean action = false;
	private boolean name = false;
	private CustomGridFieldSeats gridFieldSeats;
	private Passenger lastPassenger;
	private SeatsController seatsController;

	
	HorizontalFieldManager horizontalFieldManager = new HorizontalFieldManager(Field.USE_ALL_WIDTH);
	
	public ViewSeats2(boolean isDepartFlight, String flight, String dateDepart, Passenger passenger) {
	//public ViewSeats2() {

		super(true, "Selecci�n de asiento y nombre de pasajero");
		this.flight = flight;
		this.dateDepart = dateDepart;
		this.passenger = passenger;
		this.isDepartFlight = isDepartFlight;
		this.lastPassenger = clonePassenger(passenger);

		WhiteLabelField labelPassenger = new WhiteLabelField("Pasajero: ");
		passengerFiled = new EditField("", passenger.getName());

		passengerFiled.setChangeListener(this);

		Background background = BackgroundFactory
				.createLinearGradientBackground(Color.WHITE, Color.WHITE,
						Color.WHITESMOKE, Color.WHITESMOKE);
		passengerFiled.setBackground(background);

		HorizontalFieldManager fieldManager = new HorizontalFieldManager();
		fieldManager.add(labelPassenger);
		fieldManager.add(passengerFiled);
		add(fieldManager);

		add(new LabelField(""));

		
		add(horizontalFieldManager);
		
		dataKeeper = DataKeeper.getInstance();

		if (isDepartFlight) {
			seats = dataKeeper.getSeatsDepart();
		} else {
			seats = dataKeeper.getSeatsArrive();
		}

		if (seats == null) {

			seatsController = new SeatsController(isDepartFlight, flight,
					dateDepart, passenger);
			seatsController.getData(this);

		} else {

			updateManager(seats);
		}
	}

	public void sendMessage(String message) {

		Dialog.alert(message);
	}

	public void setData(int request, JSONObject jsObject) {

		GraphicButtonSeat[][] seats = seatsController.getActionToList(jsObject);

		if (isDepartFlight) {
			dataKeeper.setSeatsDepart(seats);
		} else {
			dataKeeper.setSeatsArrive(seats);
		}
		updateManager(seats);
	}

	public boolean onSavePrompt() {

		System.out.println(passenger.getName());
		return super.onSavePrompt();
	}

	public void close() {

		if (gridFieldSeats != null){
			gridFieldSeats.clearSeats();
			gridFieldSeats.deleteAll();
		}

		super.close();
	}

	private Passenger clonePassenger(Passenger passenger) {

		Passenger passenger2 = new Passenger();

		passenger2.setKind(passenger.getKind());
		passenger2.setCharge(passenger.getCharge());
		passenger2.setName(passenger.getName());
		passenger2.setSeat(passenger.getSeat());
		passenger2.setNamed(passenger.isNamed());

		return passenger2;
	}

	
	BitmapField bitmapField;
	
	private void updateManager(GraphicButtonSeat seats[][]) {

		int i = seats.length;
		int j = seats[0].length;

		if (gridFieldSeats == null) {

			gridFieldSeats = new CustomGridFieldSeats(i, j,
					GridFieldManager.FIELD_HCENTER, passenger, seats);
			
			int gridWidth = gridFieldSeats.getSizeWidth();
			int width = Display.getWidth();
			
			int sizeWidth = (width - gridWidth)/2;
			
			Bitmap bm = new Bitmap(sizeWidth, 1);
			
			bitmapField = new BitmapField(bm);
			
			horizontalFieldManager.add(bitmapField);
			horizontalFieldManager.add(gridFieldSeats);
		} else {

			horizontalFieldManager.delete(bitmapField);
			horizontalFieldManager.delete(gridFieldSeats);
			gridFieldSeats = new CustomGridFieldSeats(i, j,
					GridFieldManager.FIELD_HCENTER, passenger, seats);
			
			
			int gridWidth = gridFieldSeats.getSizeWidth();
			int width = Display.getWidth();

			int sizeWidth = (width - gridWidth)/2;

			Bitmap bm = new Bitmap(sizeWidth, 1);

			bitmapField = new BitmapField(bm);

			horizontalFieldManager.add(bitmapField);
			horizontalFieldManager.add(gridFieldSeats);
		}
	}

	public void fieldChanged(Field field, int context) {

		if (field == (EditField) passengerFiled) {

			String name = passengerFiled.getText();

			if ((name != null) && (name.length() > 0)) {
				passenger.setName(name);
				passenger.setNamed(true);
			} else {
				passenger.setName(null);
				passenger.setNamed(false);
			}
		}
	}
}



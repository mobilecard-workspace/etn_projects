package mx.com.ib.etn.view.seats;

import java.util.Date;
import java.util.Vector;

import mx.com.ib.etn.beans.dto.Passenger;
import mx.com.ib.etn.model.connection.base.ThreadHTTPBase;
import mx.com.ib.etn.model.connection.base.UrlEncode;
import mx.com.ib.etn.model.data.DataKeeper;
import mx.com.ib.etn.model.data.DataTravel;
import mx.com.ib.etn.utils.UtilDate;
import mx.com.ib.etn.view.controls.Viewable;
import net.rim.blackberry.api.browser.URLEncodedPostData;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import mx.com.ib.etn.model.creatorbeans.fromjs.JSBusDiagram;
import mx.com.ib.etn.model.creatorbeans.fromjs.JSBusDiagramDown;

public class SeatsController implements Viewable{

	private boolean isDepartFlight;
	private String flight;
	private String dateDepart;
	private Passenger passenger;

	private int action = 0;

	public SeatsController(boolean isDepartFlight, String flight, String dateDepart, Passenger passenger){
		
		this.isDepartFlight = isDepartFlight;
		this.flight = flight;
		this.dateDepart = dateDepart;
		this.passenger = passenger;
	}
	

	public void getData(Viewable viewable){
		
		URLEncodedPostData encodedGetDiagram = new URLEncodedPostData(URLEncodedPostData.DEFAULT_CHARSET, false);

		encodedGetDiagram.append("fecha", dateDepart);
		encodedGetDiagram.append("corrida", flight);

		UrlEncode encodeDiagram = new UrlEncode();
		
		encodeDiagram.setEncodedPostData(encodedGetDiagram);
		encodeDiagram.setRequest(Viewable.SEE_DIAGRAM);
		
		UrlEncode encodeOccupancy = new UrlEncode();

		DataKeeper dataKeeper = DataKeeper.getInstance();
		DataTravel dataTravel = dataKeeper.getDataTravel();
		
		dataTravel = dataKeeper.getDataTravel();

		Date date = dataTravel.getDateDepart();

		String sDate = UtilDate.getDateToDDMMAAAA(date);

		URLEncodedPostData encodedGetOccupancy = new URLEncodedPostData(
				URLEncodedPostData.DEFAULT_CHARSET, false);

		encodedGetOccupancy.append("origen", dataTravel.getIdDepart());
		encodedGetOccupancy.append("destino", dataTravel.getIdArrive());

		encodedGetOccupancy.append("fecha", sDate);
		encodedGetOccupancy.append("corrida", flight);

		
		encodeOccupancy.setEncodedPostData(encodedGetOccupancy);
		encodeOccupancy.setRequest(Viewable.SEE_OCCUPANCY);
		
		
		Vector vector = new Vector();
		vector.addElement(encodeDiagram);
		vector.addElement(encodeOccupancy);
		
		
		ThreadHTTPBase threadHTTPBase = new ThreadHTTPBase(viewable, vector);
		threadHTTPBase.start();
	}


	public void setData(int action, JSONObject jsObject) {

		GraphicButtonSeat[][] list = getActionToList(jsObject);
	}


	public void sendMessage(String message) {

		
	}
	
	
	public GraphicButtonSeat[][] getActionToList(JSONObject jsObject){

		JSONObject firstObject = jsObject.optJSONObject("0");
		String numFilasArriba = (String)firstObject.optString("numFilasArriba", "-1");
		JSONArray diagrama = firstObject.optJSONArray("diagramaAutobus");
		
		
		JSONObject secondObject = jsObject.optJSONObject("1");
		JSONArray asientos = secondObject.optJSONArray("asientos");
		
		int action = Integer.parseInt(numFilasArriba);

		
		GraphicButtonSeat[][] piso = null; 
		
		try {
			
			switch(action){
			
			case 0:{
				// Obtener lista de piso
				JSBusDiagram jsBusDiagram = new JSBusDiagram();
				GraphicButtonSeat[][] diagram1 = jsBusDiagram.createDiagram(firstObject);
				piso = jsBusDiagram.setOccupancy(secondObject, diagram1);
			}

				break;
			case 8:{
				// Obtener listas de dos piso
				JSBusDiagramDown jsBusDiagram = new JSBusDiagramDown();
				Vector diagram1 = jsBusDiagram.createDiagram(firstObject);
				Vector diagram2 = jsBusDiagram.setOccupancy(secondObject, diagram1);
				GraphicButtonSeat[][] primerPiso = jsBusDiagram.getPrimerPiso(diagram2);
				GraphicButtonSeat[][] segundoPiso = jsBusDiagram.getSegundoPiso(diagram1, action);
				piso = jsBusDiagram.getArray(primerPiso, segundoPiso);
			}

				break;
			
			}

		} catch (JSONException e) {

			e.printStackTrace();
		}
		
		return piso;
	}
	
	
}



/*
c -> cafeter�a
H -> ba�o hombres
M -> ba�o mujeres
P -> escalera
T -> television
U -> ba�o unisex
*/



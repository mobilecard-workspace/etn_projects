package mx.com.ib.etn.view.seats;

import java.util.Date;
import java.util.Vector;

import mx.com.ib.etn.beans.dto.Passenger;
import mx.com.ib.etn.model.connection.base.ThreadHTTP;
import mx.com.ib.etn.model.creatorbeans.fromjs.JSBusDiagram;
import mx.com.ib.etn.model.creatorbeans.fromjs.JSBusDiagramDown;
import mx.com.ib.etn.model.data.DataKeeper;
import mx.com.ib.etn.model.data.DataTravel;
import mx.com.ib.etn.utils.UtilDate;
import mx.com.ib.etn.utils.UtilIcon;
import mx.com.ib.etn.view.controls.CustomMainScreen;
import mx.com.ib.etn.view.controls.TitleOrangeRichTextField;
import mx.com.ib.etn.view.controls.Viewable;
import mx.com.ib.etn.view.controls.WhiteLabelField;
import net.rim.blackberry.api.browser.URLEncodedPostData;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.GridFieldManager;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

import org.json.me.JSONException;
import org.json.me.JSONObject;

//public class ViewSeats extends CustomMainScreen implements FieldChangeListener, Viewable {
public class ViewSeats extends CustomMainScreen implements Viewable {

	private String flight;
	private String dateDepart;
	private JSBusDiagram jsBusDiagram;
	private EditField passengerFiled;

	private Passenger passenger;
	private GraphicButtonSeat seats[][];
	private GraphicButtonSeat seats1[][];
	private GraphicButtonSeat seats2[][];
	
	private DataKeeper dataKeeper = null;
	private DataTravel dataTravel = null;

	private boolean isDepartFlight;
	private boolean isDoubleFloor = false;
	
	private boolean action = false;
	private boolean name = false;
	private CustomGridFieldSeats gridFieldSeats;
	private Passenger lastPassenger;
	
	private ViewSeats viewSeats;
	
	public ViewSeats(boolean isDepartFlight, String flight, String dateDepart, Passenger passenger) {

		super(true, "Selecci�n de asiento y nombre de pasajero");
		
		this.flight = flight;
		this.dateDepart = dateDepart;
		this.passenger = passenger;
		this.isDepartFlight = isDepartFlight;
		this.lastPassenger = clonePassenger(passenger);
		
		this.viewSeats = viewSeats;
/*
		BitmapField bitmapField = UtilIcon.getTitle();
		add(bitmapField);

		TitleOrangeRichTextField orangeRichTextField = new TitleOrangeRichTextField(
				"Selecci�n de asiento y nombre de pasajero");
		add(orangeRichTextField);
		add(new LabelField(""));
*/
		WhiteLabelField labelPassenger = new WhiteLabelField("Pasajero: ");
		passengerFiled = new EditField("", passenger.getName());
		Listener listener = new Listener(passengerFiled, passenger);
		passengerFiled.setChangeListener(listener);

		Background background = BackgroundFactory
				.createLinearGradientBackground(Color.WHITE, Color.WHITE,
						Color.WHITESMOKE, Color.WHITESMOKE);
		passengerFiled.setBackground(background);

		HorizontalFieldManager fieldManager = new HorizontalFieldManager();
		fieldManager.add(labelPassenger);
		fieldManager.add(passengerFiled);
		add(fieldManager);

		add(new LabelField(""));

		dataKeeper = DataKeeper.getInstance();

		if(dataKeeper.isDoubleFloor()){
			
			if (isDepartFlight) {
				seats1 = dataKeeper.getSeatsDepart1();
				seats2 = dataKeeper.getSeatsDepart2();
			} else {
				seats1 = dataKeeper.getSeatsArrive1();
				seats2 = dataKeeper.getSeatsArrive2();
			}
		} else {

			if (isDepartFlight) {
				seats = dataKeeper.getSeatsDepart();
			} else {
				seats = dataKeeper.getSeatsArrive();
			}
		}



		if (
				(seats == null) &&
				(seats1 == null) &&
				(seats2 == null)
			) 
		 {

			URLEncodedPostData encodedPostData = new URLEncodedPostData(
					URLEncodedPostData.DEFAULT_CHARSET, false);

			encodedPostData.append("fecha", dateDepart);
			encodedPostData.append("corrida", flight);

			ThreadHTTP threadHTTP = new ThreadHTTP();
			threadHTTP.setHttpConfigurator(Viewable.SEE_DIAGRAM, this,
					encodedPostData);
			threadHTTP.start();
/*

			try {
				JSONObject jsonObject = createJSONDiagram();
				setData(Viewable.SEE_DIAGRAM, jsonObject);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
*/			
		} else {

			if(dataKeeper.isDoubleFloor()){

				if (isDepartFlight) {
					if (dataKeeper.isFirstFloor()){
						updateManager(seats1);
					} else {
						updateManager(seats2);
					}
				} else {
					if (dataKeeper.isFirstFloor()){
						updateManager(seats1);
					} else {
						updateManager(seats2);
					}
				}
			} else {
				updateManager(seats);
			}
		}
	}

	
	public void sendMessage(String message) {

		Dialog.alert(message);
	}
	
	
	private int filasArriba = 0;
	
	JSBusDiagramDown jsBusDiagramDown = null;
	Vector vSeats;
	
	
	public void setData(int request, JSONObject jsObject) {

		String tFilasArriba1 = null;
		
		if(jsObject.has("numFilasArriba")){
			String tFilasArriba = jsObject.optString("numFilasArriba", "0");
			try {
				tFilasArriba1 = jsObject.getString("numFilasArriba");
			} catch (JSONException e) {

				e.printStackTrace();
			}
			
			filasArriba = Integer.parseInt(tFilasArriba);
		}
		
		if (filasArriba > 0){

			dataKeeper.setDoubleFloor(true);

			if (request == Viewable.SEE_DIAGRAM) {

				try {
					jsBusDiagramDown = new JSBusDiagramDown();
					vSeats = jsBusDiagramDown.createDiagram(jsObject);

				} catch (JSONException jsonE) {

					jsonE.printStackTrace();
					Dialog.alert("Error al interpretar la informacion");
				}

				dataTravel = dataKeeper.getDataTravel();

				Date date = dataTravel.getDateDepart();

				String sDate = UtilDate.getDateToDDMMAAAA(date);

				URLEncodedPostData encodedPostData = new URLEncodedPostData(
						URLEncodedPostData.DEFAULT_CHARSET, false);

				encodedPostData.append("origen", dataTravel.getIdDepart());
				encodedPostData.append("destino", dataTravel.getIdArrive());

				encodedPostData.append("fecha", sDate);
				encodedPostData.append("corrida", flight);

				ThreadHTTP threadHTTP = new ThreadHTTP();
				threadHTTP.setHttpConfigurator(Viewable.SEE_OCCUPANCY, this,
						encodedPostData);
				threadHTTP.start();

			} else if (request == Viewable.SEE_OCCUPANCY) {

				try {

					vSeats = jsBusDiagramDown.setOccupancy(jsObject, vSeats);
					
					seats1 = jsBusDiagramDown.getPrimerPiso(vSeats);
					seats2 = jsBusDiagramDown.getSegundoPiso(vSeats, filasArriba);

					if (isDepartFlight) {
						dataKeeper.setSeatsDepart1(seats1);
						dataKeeper.setSeatsDepart2(seats2);
					} else {
						dataKeeper.setSeatsArrive1(seats1);
						dataKeeper.setSeatsArrive2(seats2);
					}
					
					if (dataKeeper.isFirstFloor()){
						updateManager(seats1);
					} else {
						updateManager(seats2);
					}

					
					if (dataKeeper.isDoubleFloor()){
						if (dataKeeper.isFirstFloor()){
							addMenuFirtsFloor();
						} else {
							addMenuSecondFloor();
						}
					}
					

				} catch (JSONException jsonE) {

					jsonE.printStackTrace();
				}
			}
		} else {
			
			dataKeeper.setDoubleFloor(false);
			
			if (request == Viewable.SEE_DIAGRAM) {

				try {
					jsBusDiagram = new JSBusDiagram();
					seats = jsBusDiagram.createDiagram(jsObject);

				} catch (JSONException jsonE) {

					jsonE.printStackTrace();
					Dialog.alert("Error al interpretar la informacion");
				}

				dataTravel = dataKeeper.getDataTravel();

				Date date = dataTravel.getDateDepart();

				String sDate = UtilDate.getDateToDDMMAAAA(date);

				URLEncodedPostData encodedPostData = new URLEncodedPostData(
						URLEncodedPostData.DEFAULT_CHARSET, false);

				encodedPostData.append("origen", dataTravel.getIdDepart());
				encodedPostData.append("destino", dataTravel.getIdArrive());

				encodedPostData.append("fecha", sDate);
				encodedPostData.append("corrida", flight);

				ThreadHTTP threadHTTP = new ThreadHTTP();
				threadHTTP.setHttpConfigurator(Viewable.SEE_OCCUPANCY, this,
						encodedPostData);
				threadHTTP.start();

			} else if (request == Viewable.SEE_OCCUPANCY) {

				try {

					seats = jsBusDiagram.setOccupancy(jsObject, seats);

					if (isDepartFlight) {
						dataKeeper.setSeatsDepart(seats);
					} else {
						dataKeeper.setSeatsArrive(seats);
					}
					
					if (dataKeeper.isDoubleFloor()){
						if (dataKeeper.isFirstFloor()){
							addMenuFirtsFloor();
						} else {
							addMenuSecondFloor();
						}
					}
					
					updateManager(seats);

				} catch (JSONException jsonE) {

					jsonE.printStackTrace();
				}
			}
		}
		

	}

	
	
	private void addMenuFirtsFloor(){
		MenuItem FirtsFloor = new MenuItem("Primer piso.", 50, 50){

			public void run(){

                UiApplication.getUiApplication().invokeLater(new Runnable() {
                    public void run() {
    					DataKeeper dataKeeper = DataKeeper.getInstance();
    					dataKeeper.setFirstFloor(true);
    					gridFieldSeats.clearSeats();
    					gridFieldSeats.deleteAll();
    					delete(gridFieldSeats);
    					UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
    					UiApplication.getUiApplication().pushScreen(new ViewSeats(isDepartFlight, flight, dateDepart, passenger));
                    }
                });
		    }
		};
		
		addMenuItem(FirtsFloor);
	}

	
	private void addMenuSecondFloor(){

		MenuItem SecondFloor = new MenuItem("Segundo piso.", 100, 100){
	        
			public void run(){

                UiApplication.getUiApplication().invokeLater(new Runnable() {
                    public void run() {
    					DataKeeper dataKeeper = DataKeeper.getInstance();
    					dataKeeper.setFirstFloor(false);
    					gridFieldSeats.clearSeats();
    					gridFieldSeats.deleteAll();
    					delete(gridFieldSeats);
    					UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
    					UiApplication.getUiApplication().pushScreen(new ViewSeats(isDepartFlight, flight, dateDepart, passenger));
                    }
                });
				
			}
		};
		
		addMenuItem(SecondFloor);
	}

	
	public boolean onSavePrompt(){
		
		System.out.println(passenger.getName());
		return super.onSavePrompt();
	}
	
	public void close() {
		
		if (gridFieldSeats != null){
			gridFieldSeats.clearSeats();
			gridFieldSeats.deleteAll();
		}

		super.close();
	}
	
	
	private Passenger clonePassenger(Passenger passenger){
		
		Passenger passenger2 = new Passenger();

		passenger2.setKind(passenger.getKind());
		passenger2.setCharge(passenger.getCharge());
		passenger2.setName(passenger.getName());
		passenger2.setSeat(passenger.getSeat());
		passenger2.setNamed(passenger.isNamed());
		
		return passenger2;
	}
	
	private void updateManager(GraphicButtonSeat seats[][]) {

		int i = seats.length;
		int j = seats[0].length;

		if (gridFieldSeats == null){

			gridFieldSeats = new CustomGridFieldSeats(i, j, GridFieldManager.FIELD_HCENTER, passenger, seats);
			add(gridFieldSeats);
		} else {
			
			delete(gridFieldSeats);
			gridFieldSeats = new CustomGridFieldSeats(i, j, GridFieldManager.FIELD_HCENTER, passenger, seats);
			add(gridFieldSeats); 
		}
	}
}



class Listener implements FieldChangeListener{

	private EditField passengerFiled;
	private Passenger passenger;
	
	Listener(EditField passengerFiled, Passenger passenger){
		
		this.passengerFiled = passengerFiled;
		this.passenger = passenger;
	}
	
	public void fieldChanged(Field field, int context) {

		if (field == (EditField)passengerFiled){
			
			String name = passengerFiled.getText();
			
			if ((name != null)&&(name.length() > 0)){
				passenger.setName(name);
				passenger.setNamed(true);
			} else {
				passenger.setName(null);
				passenger.setNamed(false);
			}
		}
	}
}



/*
				try {
					JSONObject jsonObject = createJSONOccupancy();
					setData(Viewable.SEE_OCCUPANCY, jsonObject);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
*/				

/*
private void addMenuFirtsFloor(){
	MenuItem FirtsFloor = new MenuItem("Primer piso.", 50, 50){
        
		public void run(){
			
			synchronized (Application.getEventLock()) {
				DataKeeper dataKeeper = DataKeeper.getInstance();
				dataKeeper.setFirstFloor(true);
				gridFieldSeats.clearSeats();
				gridFieldSeats.deleteAll();
				delete(gridFieldSeats);
				UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
				UiApplication.getUiApplication().pushScreen(new ViewSeats(isDepartFlight, flight, dateDepart, passenger));
			}
	    }
	};
	
	addMenuItem(FirtsFloor);
}


private void addMenuSecondFloor(){

	MenuItem SecondFloor = new MenuItem("Segundo piso.", 100, 100){
        
		public void run(){
			
			synchronized (Application.getEventLock()) {

				DataKeeper dataKeeper = DataKeeper.getInstance();
				dataKeeper.setFirstFloor(false);
				gridFieldSeats.clearSeats();
				gridFieldSeats.deleteAll();
				delete(gridFieldSeats);
				UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
				UiApplication.getUiApplication().pushScreen(new ViewSeats(isDepartFlight, flight, dateDepart, passenger));
			}
		}
	};
	
	addMenuItem(SecondFloor);
}

*/





/*


UiApplication.getUiApplication().invokeLater(new Runnable() {
    public void run() {
        Status.show("Clicked on "+o);
    }
});



MenuItem SecondFloor = new MenuItem("Segundo piso.", 100, 100){

public void run(){
DataKeeper dataKeeper = DataKeeper.getInstance();
dataKeeper.setFirstFloor(false);
UiApplication.getUiApplication().popScreen(viewSeats);
UiApplication.getUiApplication().pushScreen(new ViewSeats(isDepartFlight, flight, dateDepart, passenger));
}
};


if (dataKeeper.isFirstFloor()){
addMenuItem(SecondFloor);
} else {
addMenuItem(FirtsFloor);
}
*/



/*
private JSONObject createJSONDiagram() throws JSONException{
	
	String sJson = "{\"diagramaAutobus\":[\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"023T\",\"024T\",\"000 \",\"025T\",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000H\",\"000M\",\"000C\",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"001T\",\"000 \",\"002T\",\"003T\",\"004T\",\"000 \",\"005T\",\"006T\",\"007T\",\"000 \",\"008T\",\"009T\",\"010T\",\"000 \",\"011T\",\"012T\",\"013T\",\"000 \",\"000P\",\"000 \",\"014T\",\"000 \",\"015T\",\"016T\",\"017T\",\"000 \",\"018T\",\"019T\",\"020T\",\"000 \",\"021T\",\"022T\",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \",\"000 \"],\"numFilas\":\"7\",\"numFilasArriba\":\"8\",\"numError\":1111,\"descError\":\"Cadena correcta.\",\"numAsientos\":25}";
	
	JSONObject jsonObject = new JSONObject(sJson);
	
	return jsonObject;
}

private JSONObject createJSONOccupancy() throws JSONException{
	
	String sJson = "{\"asientos\":[\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\"codigoError\":\"00\",\"descripcionError\":\"Sin Error.\"}";
	
	JSONObject jsonObject = new JSONObject(sJson);
	
	return jsonObject;
}
*/








/*
 * 
 * 
 * Ya resolvimos las dudas con el tema de los asientos, la simbolog�a es la
 * siguiente:
 * 
 * c -> cafeter�a H -> ba�o hombres M -> ba�o mujeres P -> escalera T ->
 * television U -> ba�o unisex
 * 
 * para saber cual es la distribuci�n tendr�as que ver el siguiente arreglo
 * 
 * 
 * [ "01  ","00  ","02  ","03T ", "04  ","00  ","05  ","06  ",
 * "07  ","00  ","08  ","09  ", "10T ","00  ","11  ","12  ",
 * "13  ","00  ","14  ","15  ", "16  ","00  ","17  ","18T ",
 * "19  ","00  ","20  ","21  ", "22  ","00  ","23  ","24  ",
 * "00  ","00  ","00  ","00  ", "00  ","00  ","00  ","00  ",
 * "00  ","00  ","00  ","00  ", "00  ","00  ","00  ","00  ",
 * "00M ","00C ","00C ","00C ", "00H ","00" ], "numFilas":"9", "numError":1111,
 * "descError":"Cadena correcta.", "numAsientos":24
 * 
 * 
 * 
 * {"asientos":[ "0","0","0","0","0","0", "0","0","0","1","0","0",
 * "0","0","0","0","0","0", "0","0","0","0","0","0" ], "codigoError":"00",
 * "descripcionError":"Sin Error."}
 * 
 * 
 * 
 * 01 ,00 ,02 ,03T ,04 ,00 ,05 ,06 ,07 ,00 ,08 ,09 ,10T ,00 ,11 ,12 ,13 ,00 ,14
 * ,15 ,16 ,00 ,17 ,18T ,19 ,00 ,20 ,21 ,22 ,00 ,23 ,24 ,00 ,00 ,00 ,00 ,00 ,00
 * ,00 ,00 ,00 ,00 ,00 ,00 ,00 ,00 ,00 ,00 ,00M ,00C ,00C ,00C ,00H ,00|9|
 * 
 * el n�mero 9 que aparece al final entre pipes indica que son 9 filas
 * 
 * y el arreglo de enteros indica la distribuci�n de los asientos, por ejemplo:
 * 
 * 01 , 00 , 02 , 03T 04 , 00 , 05 , 06 07 , 00 , 08 , 09 10T, 00 , 11 , 12 13 ,
 * 00 , 14 , 15 16 , 00 , 17 , 18T 19 , 00 , 20 , 21 22 , 00 , 23 , 24 00 , 00 ,
 * 00 , 00 00 , 00 , 00 , 00 00 , 00 , 00 , 00 00 , 00 , 00 , 00 0M ,00C ,00C
 * ,00C ,00H 00
 * 
 * Si vemos el diagrama de la p�gina de ETN concuerda con lo que nos regresa la
 * consulta al servicio En la imagen apenas se percibe, pero si te das cuenta en
 * los asientos que tienen la letra "T" pinta una "Televisi�n", Charly ya
 * solicit� las im�genes para cada cosa que aparece en el diagrama.
 * 
 * Por otra parte la ocupaci�n de los asientos te la enviaremos en un arreglo de
 * enteros donde la longitud corresponder� al n�mero de asientos del autob�s,
 * actualmente estamos regresando 101 posiciones pero se est� trabajando en
 * adaptarlo a como te estoy especificando.
 */
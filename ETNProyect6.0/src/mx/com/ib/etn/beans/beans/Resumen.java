package mx.com.ib.etn.beans.beans;

public class Resumen {

	private String idReservacion;
	private String importe;
	private String importeTotal;
	
	private boolean valid = true;
	
	public String getIdReservacion() {
		return idReservacion;
	}
	public void setIdReservacion(String idReservacion) {
		this.idReservacion = idReservacion;
	}
	public String getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		this.importe = importe;
	}
	public String getImporteTotal() {
		return importeTotal;
	}
	public void setImporteTotal(String importeTotal) {
		this.importeTotal = importeTotal;
	}
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
}

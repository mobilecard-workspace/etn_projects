package mx.com.ib.etn.beans.beans;

import java.util.Vector;

public class Nit {

	private String nit;
	
	private Vector boletos;
	
	public Nit(){
		
		boletos = new Vector();
	}
	
	public Vector getBoletos() {
		return boletos;
	}

	public void setBoletos(Vector boletos) {
		this.boletos = boletos;
	}
	
	public void addElement(Boleto boleto){
		boletos.addElement(boleto);
	}

	public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}
}

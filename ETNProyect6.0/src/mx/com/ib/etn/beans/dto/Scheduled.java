package mx.com.ib.etn.beans.dto;


public class Scheduled {
	
	private String flight;
	private String companyFlight;
	private String hourDepart;
	private String hourArrive;
	
	private String dateDepart;
	private String dateArrive;
	private String charge;
	
	private String chargeAdults;
	private String chargeChildren;
	private String chargeElderlies;
	private String chargeStudents;
	private String chargeProfessors;

	private String servicio;
	
	private String flightKind;

	private int iva;
	
	/*
	public Scheduled(String borrame){
		
	}
	*/
	
	public String getDateDepartToString() {

		String day = dateDepart.substring(0, 2);
		String month = dateDepart.substring(3, 5);
		String year = dateDepart.substring(6, 10);

		return day + month + year;
	}

	
	public String getHourDepartToString() {

		String hour = hourDepart.substring(0, 2);
		String minute = hourDepart.substring(3, 5);

		return hour + minute + "00";
	}


	public String getFlight() {
		return flight;
	}


	public String getCompanyFlight() {
		return companyFlight;
	}


	public String getHourDepart() {
		return hourDepart;
	}


	public String getHourArrive() {
		return hourArrive;
	}


	public String getDateDepart() {
		return dateDepart;
	}


	public String getDateArrive() {
		return dateArrive;
	}


	public String getCharge() {
		return charge;
	}


	public String getChargeAdults() {
		return chargeAdults;
	}


	public String getChargeChildren() {
		return chargeChildren;
	}


	public String getChargeElderlies() {
		return chargeElderlies;
	}


	public String getChargeStudents() {
		return chargeStudents;
	}


	public String getChargeProfessors() {
		return chargeProfessors;
	}


	public String getFlightKind() {
		return flightKind;
	}

	public void setFlight(String flight) {
		this.flight = flight;
	}


	public void setCompanyFlight(String companyFlight) {
		this.companyFlight = companyFlight;
	}


	public void setHourDepart(String hourDepart) {
		this.hourDepart = hourDepart;
	}


	public void setHourArrive(String hourArrive) {
		this.hourArrive = hourArrive;
	}


	public void setDateDepart(String dateDepart) {
		this.dateDepart = dateDepart;
	}


	public void setDateArrive(String dateArrive) {
		this.dateArrive = dateArrive;
	}


	public void setCharge(String charge) {
		this.charge = charge;
	}


	public void setChargeAdults(String chargeAdults) {
		this.chargeAdults = chargeAdults;
	}


	public void setChargeChildren(String chargeChildren) {
		this.chargeChildren = chargeChildren;
	}


	public void setChargeElderlies(String chargeElderlies) {
		this.chargeElderlies = chargeElderlies;
	}


	public void setChargeStudents(String chargeStudents) {
		this.chargeStudents = chargeStudents;
	}


	public void setChargeProfessors(String chargeProfessors) {
		this.chargeProfessors = chargeProfessors;
	}


	public void setFlightKind(String flightKind) {
		this.flightKind = flightKind;
	}


	public String getServicio() {
		return servicio;
	}


	public void setServicio(String servicio) {
		this.servicio = servicio;
	}


	public int getIva() {
		return iva;
	}


	public void setIva(int iva) {
		this.iva = iva;
	}


	public boolean isIva(){
		
		if (iva == 0){
			return false;
		} else {
			return true;
		}
	}
}

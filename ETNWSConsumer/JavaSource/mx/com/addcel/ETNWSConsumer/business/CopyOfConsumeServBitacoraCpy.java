package mx.com.addcel.ETNWSConsumer.business;

import mx.com.addcel.ETNWSConsumer.dto.Bitacora;
import mx.com.addcel.ETNWSConsumer.dto.BitacoraPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.RespuestaCadena;
import mx.com.addcel.ETNWSConsumer.dto.RespuestaVenta;
import mx.com.addcel.ETNWSConsumer.dto.ViajeETNDto;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class CopyOfConsumeServBitacoraCpy {
	private Logger log=Logger.getLogger(CopyOfConsumeServBitacoraCpy.class);
	public Bitacora ingresarBitacora(BitacoraPeticionDto bitacoraEnvia, ViajeETNDto viaje) throws IOException {
		Bitacora bitacoraResp = new Bitacora();

		String data = "idUsuario="
				+ URLEncoder.encode(bitacoraEnvia.getIdUsuario(), "UTF-8")
				+ "&imei="
				+ URLEncoder.encode(bitacoraEnvia.getImei(), "UTF-8")
				+ "&modelo="
				+ URLEncoder.encode(bitacoraEnvia.getModelo(), "UTF-8")
				+ "&monto="
				+ URLEncoder.encode(bitacoraEnvia.getMonto(), "UTF-8")
				+ "&software="
				+ URLEncoder.encode(bitacoraEnvia.getSoftware(), "UTF-8")
				+ "&tipoProducto="
				+ URLEncoder.encode(bitacoraEnvia.getTipoProducto(), "UTF-8")
				+ "&tipoTelefono="
				+ URLEncoder.encode(bitacoraEnvia.getTipoTelefono(), "UTF-8")
				+ "&tipoViaje="
				+ URLEncoder.encode(bitacoraEnvia.getTipoViaje(), "UTF-8")
				+ "&wkey="
				+ URLEncoder.encode(bitacoraEnvia.getWkey(), "UTF-8")
				+ "&proveedor="
				+ URLEncoder.encode(bitacoraEnvia.getProveedor(), "UTF-8")
				+ "&producto="
				+ URLEncoder.encode(bitacoraEnvia.getProducto(), "UTF-8")
				+ "&folioETN="
				+ URLEncoder.encode(bitacoraEnvia.getFolioETN(), "UTF-8")
				+ "&codError="
				+ URLEncoder.encode(bitacoraEnvia.getCodError(), "UTF-8")
				+ "&detError=Reservacion+ETN+"
				+ URLEncoder.encode(bitacoraEnvia.getDetError(), "UTF-8")				
				+ "&estatus="
				+ URLEncoder.encode(bitacoraEnvia.getEstatus(), "UTF-8")
				+ "&idViaje="
				+ URLEncoder.encode(viaje.getIdViaje() +"", "UTF-8")
				+ "&idOrigen="
				+ URLEncoder.encode(viaje.getIdOrigen(), "UTF-8")
				+ "&idDestino="
				+ URLEncoder.encode(viaje.getIdDestino(), "UTF-8")
				+ "&origen="
				+ URLEncoder.encode(viaje.getOrigenETN(), "UTF-8")
				+ "&destino="
				+ URLEncoder.encode(viaje.getDestinoETN(), "UTF-8")
				+ "&fecha="
				+ URLEncoder.encode(viaje.getFechaETN(), "UTF-8")
				+ "&dia="
				+ URLEncoder.encode(viaje.getDiaViaje(), "UTF-8")
				+ "&hora="
				+ URLEncoder.encode(viaje.getHoraETN(), "UTF-8")
				+ "&tCorrida="
				+ URLEncoder.encode(viaje.getTipoCorrida(), "UTF-8")
				+ "&corrida="
				+ URLEncoder.encode(viaje.getCorridaETN(), "UTF-8");

		URL url = new URL(
				"http://mobilecard.mx:8080/AddCelBridge/ClienteBitacoraAgrega");
		log.info(url.toString() + "?" + data);
		URLConnection conn = url.openConnection();
		conn.setDoOutput(true);
		OutputStreamWriter wr;
		wr = new OutputStreamWriter(conn.getOutputStream());
		wr.write(data);
		wr.flush();

//		// Respuesta
		BufferedReader rd = new BufferedReader(new InputStreamReader(
				conn.getInputStream()));
		String line, respuesta = "";
		Gson gson = new Gson();

		while ((line = rd.readLine()) != null) {
			respuesta += line;
		}
		
		wr.close();
		rd.close();
		
		RespuestaCadena resp = gson.fromJson(respuesta, RespuestaCadena.class);
		bitacoraResp.setId(Long.parseLong(resp.getRegistrado()));
		bitacoraResp.setConcepto(bitacoraEnvia.getDetalle());
		return bitacoraResp;
	}

	public RespuestaVenta actualizaBitacora(Bitacora bitacora) {
		
		String respuesta="";
		RespuestaVenta resp= null;
		
		try {

			String data = "codError="
					+ URLEncoder.encode(bitacora.getCodigo_error() + "","UTF-8")
					+ "&ticket="
					+ URLEncoder.encode(bitacora.getTicket(), "UTF-8")
					+ "&autorizacion="
					+ URLEncoder.encode(bitacora.getNo_autorizacion() + "", "UTF-8")
					+ "&nit= "
					+ URLEncoder.encode(bitacora.getKey() + "", "UTF-8")
					+ "&idViaje= "
					+ URLEncoder.encode(bitacora.getImei() + "", "UTF-8")
					+ "&folioETN= "
					+ URLEncoder.encode(bitacora.getModelo() + "", "UTF-8")
					+ "&idBitacora="
					+ URLEncoder.encode(bitacora.getId() + "", "UTF-8");

			URL url = new URL(
					"http://localhost:8080/AddCelBridge/ClienteBitacoraActualiza");
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter wr;
			wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(data);
			wr.flush();

			// Respuesta
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			String line;
			Gson gson = new Gson();

			while ((line = rd.readLine()) != null) {
				respuesta += line;
			}
						
			resp = gson.fromJson(respuesta, RespuestaVenta.class);
			
			log.info("En actualizaBitacora "+respuesta);
			wr.close();
			rd.close();
			

		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException : ",e);
			return resp;
		} catch (MalformedURLException e) {
			log.error("MalformedURLException : ",e);
			return resp;
		} catch (IOException e) {
			log.error("IOException : ",e);
			return resp;
		}

		return resp;
	}
	
	/**
	 * Consulta los boletos disponibles para el usuario
	 * @param tipoCOnsulta. Si se consulta por idUsuario o por IdBitacora "U" o "B"
	 * @param parametro. Es el parámetro a consultar
	 * @return
	 */
	
	public List<Bitacora> consultaBitacoraBoleto(String modulo, String idBusqueda){
		
		Bitacora resp = new Bitacora();
		List<Bitacora> listaBitacora = new ArrayList<Bitacora>();
		
		try {

			String data = "modulo="
					+ URLEncoder.encode(modulo +"", "UTF-8")
					+ "&idBusqueda="
					+ URLEncoder.encode(idBusqueda, "UTF-8");

			URL url = new URL(
					"http://localhost:8080/AddCelBridge/ClienteBitacoraConsulta");
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter wr;
			wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(data);
			wr.flush();

			// Respuesta
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			String line, respuesta = "";
			Gson gson = new Gson();
			
			
			while ((line = rd.readLine()) != null) {
				respuesta += line;
			}
			
			
			listaBitacora = gson.fromJson(respuesta, List.class);
			
			wr.close();
			rd.close();			

		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException : ",e);
		} catch (MalformedURLException e) {
			log.error("MalformedURLException : ",e);
		} catch (IOException e) {
			log.error("IOException : ",e);
		}
				
		return listaBitacora;
	}

}

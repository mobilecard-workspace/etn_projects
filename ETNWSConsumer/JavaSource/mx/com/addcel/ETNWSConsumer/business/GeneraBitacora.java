package mx.com.addcel.ETNWSConsumer.business;

import mx.com.addcel.ETNWSConsumer.dto.Bitacora;
import mx.com.addcel.ETNWSConsumer.dto.BitacoraPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.BitacoraViajeDto;
import mx.com.addcel.ETNWSConsumer.dto.ConsultaViajesETN;
import mx.com.addcel.ETNWSConsumer.dto.ReservacionDto;
import mx.com.addcel.ETNWSConsumer.dto.VentaBoletoDto;
import mx.com.addcel.ETNWSConsumer.dto.ViajeETNDto;
import mx.com.addcel.ETNWSConsumer.dto.ViajeMaestroDto;

import org.apache.log4j.Logger;


public class GeneraBitacora {
	private Logger log=Logger.getLogger(GeneraBitacora.class);
	ConsumeServBitacora servBitacora = new ConsumeServBitacora();
	//TransaccionesDAO dao = new TransaccionesDAO(); Esto es por si se conecta directamente a la DB
	
		public Bitacora ingresaBitacora(ReservacionDto datosReservacion, 
				BitacoraPeticionDto bitacora, ViajeETNDto viaje ){ 
			
			Bitacora bitacoraResp = null;
			ViajeMaestroDto viajeDto = new ViajeMaestroDto();
			
			bitacora.setFolioETN(datosReservacion.getIdReservacion());
			bitacora.setMonto(datosReservacion.getImporteTotal()+"");
			bitacora.setCodError(datosReservacion.getNumError()+"");
			bitacora.setDetError(datosReservacion.getDescError());			
			
			viajeDto.setBitacoraEnvia(bitacora);
			viajeDto.setViaje(viaje);
			
			bitacoraResp = servBitacora.ingresarBitacora2(viajeDto);
						
			return bitacoraResp;
		}
		
		public boolean actualizaBitacora(ViajeETNDto viaje, VentaBoletoDto ventaBoleto,
				Bitacora bitacora, String autorizaBanco, long idViaje){
			
			boolean resp = false;
			String ticket = "";
			String foliosVenta="";
			
			log.info("actualiza bitácora");
			
			if(bitacora.getStatus() == 1){
				
				String folios[] = ventaBoleto.getOperaciones();
				for(int aux = 0; aux<folios.length; aux++){				
					foliosVenta += folios[aux] + "|";
				}
				
				ticket = " Compra de boleto exitosa. Autorizacion ETN: "+ventaBoleto.getNit()+
						 //" ,Folio Compra: "+foliosVenta+
						 " ,Folio Reservacion ETN: "+viaje.getIdReservacion()+",Folio reimpresion ETN: "+bitacora.getPase();
			}else{
				
				ticket = "Ocurrio un error: "+ventaBoleto.getDescError()+" con codigo: "+ventaBoleto.getCodigoError()+" ";
			}
			
			log.info("IdViaje: " + viaje.getIdViaje());
			log.info("ticket: " + ticket);
			
			viaje.setIdViaje(idViaje);
			viaje.setNit(ventaBoleto.getNit());
			bitacora.setDestino(ticket);			
			bitacora.setCodigo_error(ventaBoleto.getCodigoError());			
			bitacora.setNo_autorizacion(autorizaBanco);
			bitacora.setPase(idViaje);
			bitacora.setConcepto(foliosVenta);
			
			log.info("nit: " + viaje.getNit());
			
			BitacoraViajeDto viajeBit = new BitacoraViajeDto();
			
			viajeBit.setBitacora(bitacora);
			viajeBit.setViaje(viaje);
			
			resp = servBitacora.actualizaBitacora(viajeBit);
			//resp = dao.actualizaBitacora(viajeBit); Esto es por si se conecta directamente a la DB
			
			return resp;
		}
		
		public boolean actualizaBitacora(VentaBoletoDto ventaBoleto,
				Bitacora bitacora, String autorizaBanco, long idViaje){
			boolean resp = false;
			String ticket = "";
//			String foliosVenta="";
				
			ticket = "Error .. Ocurrió un error: "+ventaBoleto.getDescError()+" con código: "+ventaBoleto.getCodigoError();
			
			bitacora.setDestino(ticket);			
			bitacora.setCodigo_error(ventaBoleto.getCodigoError());			
			bitacora.setNo_autorizacion(autorizaBanco);
			bitacora.setPase(idViaje);		
			
			BitacoraViajeDto viajeBit = new BitacoraViajeDto();
			
			viajeBit.setBitacora(bitacora);
			
			resp = servBitacora.actualizaBitacora(viajeBit);
			//resp = dao.actualizaBitacora(viajeBit); Esto es por si se conecta directamente a la DB
			
			return resp;
		}
		
		public ConsultaViajesETN consultaViajesETN(long idViaje){
		
			ConsultaViajesETN viajes = servBitacora.consultaViajes(idViaje); 
			//ConsultaViajesETN viajes = dao.consultaViajes(idViaje); Esto es por si se conecta directamente a la DB
			return viajes;
		}
}

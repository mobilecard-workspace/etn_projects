/*
 * ---------------------------------------------------------------
 * Copyright (c) 2012 Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Prolongaci�n paseo de la reforma 61 oficina 6A2 sexto piso, colonia paseo de las lomas
 * C.P. 01330, Delegaci�n Alvaro Obreg�n, M�xico D.F.
 * Algunos Derechos Reservados
 *
 * Este software contiene informaci&oacute;n totalmente confidencial propiedad de
 * Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Queda totalmente prohibido su uso o divulgacion 
 * en forma parcial o total y solamente podr&aacute; ser utilizada de acuerdo a los 
 * t&eacute;rminos y estatutos que determine la propia empresa.
 * --------------------------------------------------------------
 */
package mx.com.addcel.ETNWSConsumer.business;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mx.com.addcel.ETNWSConsumer.dto.Bitacora;
import mx.com.addcel.ETNWSConsumer.dto.BitacoraPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.BoletoETNDto;
import mx.com.addcel.ETNWSConsumer.dto.BoletoMaestroDto;
import mx.com.addcel.ETNWSConsumer.dto.BoletosUsuario;
import mx.com.addcel.ETNWSConsumer.dto.CancelacionDto;
import mx.com.addcel.ETNWSConsumer.dto.CancelacionPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.ConsultaViajesETN;
import mx.com.addcel.ETNWSConsumer.dto.CorridaDto;
import mx.com.addcel.ETNWSConsumer.dto.CorridaPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.DestinoDto;
import mx.com.addcel.ETNWSConsumer.dto.DestinoPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.DiagramaDto;
import mx.com.addcel.ETNWSConsumer.dto.DiagramaPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.Functions;
import mx.com.addcel.ETNWSConsumer.dto.ItinerarioDto;
import mx.com.addcel.ETNWSConsumer.dto.ItinerarioPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.LibReservaPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.OcupacionDto;
import mx.com.addcel.ETNWSConsumer.dto.OcupacionPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.OficinaDto;
import mx.com.addcel.ETNWSConsumer.dto.ReservacionDto;
import mx.com.addcel.ETNWSConsumer.dto.ReservacionPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.TarifaPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.TarifasDto;
import mx.com.addcel.ETNWSConsumer.dto.TipoServicioDto;
import mx.com.addcel.ETNWSConsumer.dto.TipoServicioPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.VentaBoletoDto;
import mx.com.addcel.ETNWSConsumer.dto.VentaBoletoPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.ViajeETNDto;
import mx.com.addcel.ETNWSConsumer.util.ConstantesEtn;
import mx.com.addcel.ETNWSConsumer.util.ConvertirTipos;
import mx.com.addcel.ETNWSConsumer.util.ManejadorXml;
import mx.com.addcel.ETNWSConsumer.util.ObtenDiaFecha;

import org.apache.axis.types.UnsignedByte;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import citec._146._2._131._201.magic.CitecPortType;
import citec._146._2._131._201.magic.CitecPortTypeProxy;

/**
 * @author Ing. Efr&eacute;n Reyes Torres
 * @author Lic. Elena L&oacute;pez
 *  *
 */
public class ServiciosEtnImpl implements ServiciosEtn {
	private Logger log=Logger.getLogger(ServiciosEtnImpl.class);
	CitecPortType  etn   = new CitecPortTypeProxy();
	ConstantesEtn  constantesEtn = new ConstantesEtn();
	ConvertirTipos valida = new ConvertirTipos();
	Bitacora bitacoraResp = new Bitacora();
		
	/* (non-Javadoc)
	 * @see mx.com.addcel.business.ServiciosEtn#obtenerOficinas()
	 * Actualizaci�n: ELopez
	 * M�todo que interpreta la respuesta del WS getOficinas, este arroja una salida de un String separado por pipes "|" y a su vez por comas ","
	 * donde cada sub cadena se interpreta como clave y detalle de la oficina.
	 * y se guarda en el objeto OficinaDto.
	 */
	
		
	@Override
	public List<OficinaDto> obtenerOficinas() {
		
		String respuesta = "CAMI,CAMINOS|ACAP,ACAPULCO|ACAE,ACAPULCO EJIDO|AGDL,AEROPUERTO GUADALAJARA|AGUA,AGUASCALIENTES|APAT,APATZINGAN|NAVI,BARRA DE NAVIDAD|CAVA,CAVAZOS LERMA|"+
				"CDVI,CD. VICTORIA|CELA,CELAYA|CHIH,CHIHUAHUA|CHIL,CHILPANCINGO|CIHU,CIHUATLAN|CITY,CITY EXPRESS|CACU,CIUDAD ACU�A|CDOB,CIUDAD OBREGON|CVAL,CIUDAD VALLE|"+
				"COLI,COLIMA|COND,CONDOPLAZA|CUER,CUERNAVACA|CJUA,CUIDAD JUAREZ|CULI,CULIACAN|DURA,DURANGO|GPAL,GOMEZ PALACIO|GDLJ,GUADALAJARA|GUAN,GUANAJUATO|HAMB,HAMBURGO|"+
				"HERM,HERMOSILLO|BENI,HOTEL BENIDORM|HOWA,HOTEL HOWARD JOHNSON|HUAT,HUATULCO|IGUA,IGUALA|IRAP,IRAPUATO|JIQI,JIQUILPAN|PIED,LA PIEDAD|LEON,LEON|REYE,LOS REYES|"+
				"MANZ,MANZANILLO|MATA,MATAMOROS|MATE,MATEHUALA|MZTN,MAZATLAN|MEXI,MEXICALI|MEXN,MEXICO NORTE|MEXP,MEXICO PONIENTE|MXST,MEXICO SUR|MOCH,MOCHIS|TMOC,MOCHIS TAP|"+
				"MCVA,MONCLOVA|MONT,MONTERREY CENTRAL|MTYC,MONTERREY CHURUBUSCO|MORE,MORELIA|NOGA,NOGALES|TNOG,NOGALES TAP|NROS,NUEVA ROSITA|LARE,NUEVO LAREDO|PINE,PIEDRAS NEGRAS|"+
				"PINO,PINOTEPA NACIONAL|POCH,POCHUTLA|POZA,POZA RICA|PUEB,PUEBLA|PTOE,PUERTO ESCONDIDO|VALL,PUERTO VALLARTA|QUER,QUERETARO|REYN,REYNOSA|REYT,REYNOSA TERMINAL|"+
				"NVAL,RIVIERA NAYARIT|SAHU,SAHUAYO|SALA,SALAMANCA|SALT,SALTILLO|SNJL,SAN JUAN DE LOS LAGOS|SNJR,SAN JUAN DEL RIO|SNLP,SAN LUIS POTOSI|SCAT,SANTA CATARINA|SNMA,SN MIGUEL ALLENDE|"+
				"SRDL,STA. ROSA DE LIMA|TAMP,TAMPICO|TPIC,TEPIC|TEPO,TEPOTZOTLAN|TEQU,TEQUISQUIAPAN|TJUA,TIJUANA|GDLC,TLAQUEPAQUE|TOCU,TOCUMBO|TOLU,TOLUCA|TORR,TORREON|TUXP,TUXPAN|"+
				"URUA,URUAPAN|ZACA,ZACATECAS|ZAMO,ZAMORA|ZAPO,ZAPOPAN";
				
		String[] elementos;
		//StringHolder sH = new StringHolder();
		List<OficinaDto> oficinas = new ArrayList<OficinaDto>();
		
		//try {
			
			log.info("Entre a obtener oficinas ");
			// Se queda comentado porque la consulta genera NULO
			//etn.getOficinas("TAP", sH);			
			//respuesta = sH.value;
			//log.info("RESPUESTA " +respuesta);
			//log.info("ServiciosEtnImp:obtenerOficinas().getOficinas_WSResponse "+respuesta);
			if(respuesta != null){
				elementos = respuesta.split("\\|");
				
				OficinaDto oficina;
				for (int i = 0; i < elementos.length; i++) {
					if(elementos[i].indexOf(",") >0){
						oficina = new OficinaDto();			
						oficina.setClave(elementos[i].substring(0, elementos[i].indexOf(",")));
						oficina.setDescripcion(elementos[i].substring(elementos[i].indexOf(",") + 1, elementos[i].length()));
						oficinas.add(oficina);						
					}
					
				}
			}
			
		/*} catch (RemoteException e) {
			//log.info("ServiciosEtnImp:obtenerOficinas(): "+ e.getStackTrace());
			log.info("Error " + e.getStackTrace());
		}*/
			
		return oficinas;
	}

	/* (non-Javadoc)
	 * @see mx.com.addcel.business.ServiciosEtn#obtenerDestinos(mx.com.addcel.dto.DestinoPeticionDto)
	 * Actualizaci�n: ELopez
	 * M�todo que interpreta la salida del WS destinos, este arroja un String separado por pipes "|" y este a su vez, separado por diagonal "/"
	 * que indica la clave del destino y el detalle del mismo.
	 * La respuesta es procesada y se deja en el objeto DestinoDto.
	 */
	
	@Override
	public List<DestinoDto> obtenerDestinos(DestinoPeticionDto datosPeticionDestino) {
		// TODO Auto-generated method stub
		
		String respuesta = "";
		//String respuesta = "ACAP/ACAPULCO|APAT/APATZINGAN|BENI/HOTEL BENIDORM|CAVA/CAVAZOS";	
		
		DestinoDto destino;
		String[] elementos;
		List<DestinoDto> destinos = new ArrayList<DestinoDto>();
		
		try {
			
			log.info("Consultando destinos... ");			
			//respuesta = etn.destinos("ACAP","TAP","ETN");	
			respuesta = etn.destinos(datosPeticionDestino.getOficinaOrigen(),datosPeticionDestino.getEmpresaSolicita(),datosPeticionDestino.getEmpresaViaja());
			
			//log.info("ServiciosEtnImp:obtenerDestinos().destinos_WSResponse "+respuesta);
			if(respuesta != null){
				elementos = respuesta.split("\\|");	
				
				for(int i = 0; i < elementos.length; i++){
					//log.info("destino "+elementos[i].indexOf("/"));
					if(elementos[i].indexOf("/") >0){
						destino = new DestinoDto();			
						destino.setClaveDestino(elementos[i].substring(0,elementos[i].indexOf("/")));
						destino.setDescripcionDestino(elementos[i].substring(elementos[i].indexOf("/")+1,elementos[i].length()));						
						destinos.add(destino);
					}else {						
						if(elementos[i].indexOf("Error") >= 0){
							destino = new DestinoDto();
							destino.setClaveDestino("ERR");
							destino.setDescripcionDestino("Sin destinos");
							destinos.add(destino);
						}
						
					}				
				}
				log.info("Destinos: " + destinos.size());
			}else{ 
				log.info("Error en WS destinos respuesta = "+respuesta);
			}
			
		} catch (RemoteException e) {			
			log.error("Error en servicio ServiciosEtnImp:obtenerDestinos(): ", e);
			destino = new DestinoDto();
			destino.setClaveDestino("ERR");
			destino.setDescripcionDestino("Se agotó el tiempo de respuesta");
			destinos.add(destino);
		}
		
		return destinos;
	}

	
	/* (non-Javadoc)
	 * @see mx.com.addcel.business.ServiciosEtn#obtenerCorridas(mx.com.addcel.dto.CorridaPeticionDto)
	 * Actualizaci�n: ELopez
	 * M�todo que interpreta la salida del WS consultaCorridas, que arroja un String con formato de XML, el cu�l es le�do 
	 * y con cada uno de los elementos crea el objeto CorridaDto.
	 */
	@Override
	public List<CorridaDto> obtenerCorridas(CorridaPeticionDto datosPeticionCorrida) {
		
		ManejadorXml admonXml = new ManejadorXml();
		
		//String respuesta = "<?xml version=\"1.0\" encoding=\"UTF8\"?><corridas xsi:noNamespaceSchemaLocation=\"corridas.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchemainstance\"> <Record><Destino_final> </Destino_final><Tipo_itinerario> </Tipo_itinerario> <Hora_Corrida> </Hora_Corrida> <Tipo_Corrida></Tipo_Corrida> <Empresa_Corrida></Empresa_Corrida><Servicio></Servicio><Tarifa></Tarifa><Disp_Estudiantes></Disp_Estudiantes><Disp_Insen></Disp_Insen><Disp_Maestros></Disp_Maestros><Disp_General></Disp_General><Corrida>123222</Corrida><Sala>abababa</Sala><Anden>3333</Anden><Tarifa_Adulto>50.0</Tarifa_Adulto><Tarifa_Nino></Tarifa_Nino><Tarifa_Insen></Tarifa_Insen><Tarifa_Estudiante></Tarifa_Estudiante><Tarifa_Maestro></Tarifa_Maestro><Clave_Destino></Clave_Destino><Tipo_Lectura></Tipo_Lectura><Clave_Ctl></Clave_Ctl><Secuencia></Secuencia><Fecha_desfaza></Fecha_desfaza><Ubicacion></Ubicacion><Fecha_Inicial></Fecha_Inicial><Cupo_Autobus></Cupo_Autobus><Error_Numero></Error_Numero><Error_Cadena></Error_Cadena><Disp_Ninios></Disp_Ninios><Disp_Promo></Disp_Promo><Tarifa_Promo></Tarifa_Promo><Fecha_Llegada></Fecha_Llegada><Moneda></Moneda><Disp_Progpaisano></Disp_Progpaisano><Tarifa_Progpaisano></Tarifa_Progpaisano><Oficina_Frontera></Oficina_Frontera><Porcentaje_Tramo_Frontera></Porcentaje_Tramo_Frontera></Record></corridas>";
		String respuesta="";	
		List<CorridaDto> corridas = new ArrayList <CorridaDto>();
		/*double x = 2;  Estos datos comentados son para pruebas
		double y = 0;		
    	UnsignedByte u = new UnsignedByte();
    	Long tipoLong = 0L; 
    	u.setValue(tipoLong);
		cadena de prueba : CELA,ACAP,01012012,050000,PR,TAP,2,0,0,0,0,0,0,120000*/
		UnsignedByte uBtipoServ;
		TipoServicioPeticionDto tipoServDto = new TipoServicioPeticionDto();
		List<TipoServicioDto> tipoServ;
//		String resp1 = "", resp="";
		
		try{
			
			tipoServDto.setEmpresaSolicita(constantesEtn.CVE_EMPRESA);
			tipoServDto.setEmpresaViaja(constantesEtn.CVE_EMPRESA_VIAJA);
			
			tipoServ = obtenerTipoServicios(tipoServDto); 
			
			    //uBtipoServ  = new UnsignedByte(Long.parseLong(tipoDto.getCveServicio()));
			    
				uBtipoServ  = new UnsignedByte(0L);
			    
				//respuesta = etn.consultaCorridas("CAMI","GDLJ","22012013","090000",uBtipoServ,"ETN",x,y,y,y,y,u,y,"144500");			
				respuesta = etn.consultaCorridas(datosPeticionCorrida.getOrigen(),datosPeticionCorrida.getDestino(),
						datosPeticionCorrida.getFecha(),datosPeticionCorrida.getHora(),uBtipoServ,
						datosPeticionCorrida.getEmpresa(),datosPeticionCorrida.getNumAdulto(),datosPeticionCorrida.getNumNino(),
						datosPeticionCorrida.getNumInsen(),datosPeticionCorrida.getNumEstudiante(),datosPeticionCorrida.getNumMaestro(),
						datosPeticionCorrida.getMultiempresa(),datosPeticionCorrida.getNumPaisano(),datosPeticionCorrida.getHoraFin());
			    
				/*int aa = respuesta.indexOf("!");				
				if(aa>0){
					resp1 = respuesta.substring(0,aa -1);					
					resp = resp1 + respuesta.substring(aa +10,respuesta.length());
				}else{
					resp = respuesta;
				}*/
				
				
//				log.info("RESPUESTA CORRIDAS " +respuesta);
				
				//log.info("ServiciosEtnImp:obtenerCorridas().consultaCorridas_WSResponse "+respuesta);
				if (respuesta != null){
					Document documentoXml = admonXml.crearDocument(respuesta); 
					NodeList nodosTag = documentoXml.getElementsByTagName("Record");
					
					for(int aux=0; aux < nodosTag.getLength(); aux++){
						
						Node propNode = nodosTag.item(aux);
						
						if(propNode.getNodeType() == Node.ELEMENT_NODE){
							
							Element element = (Element) propNode;
							CorridaDto corrida = new CorridaDto();
							
							corrida.setDestino(admonXml.getValue("Destino_final",element));
							corrida.setTipoItinerario(admonXml.getValue("Tipo_itinerario",element));
							corrida.setHoraCorrida(admonXml.getValue("Hora_Corrida",element));
							corrida.setTipoCorrida(admonXml.getValue("Tipo_Corrida",element));
							corrida.setEmpresaCorrida(admonXml.getValue("Empresa_Corrida",element));
							corrida.setServicio(admonXml.getValue("Servicio",element));
							corrida.setTarifa(valida.getFloat(admonXml.getValue("Tarifa",element)));
							corrida.setDispEstudiante(valida.getInt(admonXml.getValue("Disp_Estudiantes",element)));
							corrida.setDispInsen(valida.getInt(admonXml.getValue("Disp_Insen",element)));
							corrida.setDispMaestro(valida.getInt(admonXml.getValue("Disp_Maestros",element)));
							corrida.setDispGeneral(valida.getInt(admonXml.getValue("Disp_General",element)));
							corrida.setCorrida(admonXml.getValue("Corrida",element));
							corrida.setSala(admonXml.getValue("Sala",element));
							corrida.setAnden(admonXml.getValue("Anden",element));
							corrida.setTarifaAdulto(valida.getFloat(admonXml.getValue("Tarifa_Adulto",element)));
							corrida.setTarifaNino(valida.getFloat(admonXml.getValue("Tarifa_Nino",element)));
							corrida.setTarifaInsen(valida.getFloat(admonXml.getValue("Tarifa_Insen",element)));
							corrida.setTarifaEstudiante(valida.getFloat(admonXml.getValue("Tarifa_Estudiante",element)));
							corrida.setTarifaMaestro(valida.getFloat(admonXml.getValue("Tarifa_Maestro",element)));
							corrida.setCveDestino(admonXml.getValue("Clave_Destino",element));
							corrida.setTipoLectura(admonXml.getValue("Tipo_Lectura",element));
							corrida.setCveCtl(admonXml.getValue("Clave_Ctl",element));
							corrida.setSecuencia(admonXml.getValue("Secuencia",element));
							corrida.setFechaDesfaza(admonXml.getValue("Fecha_desfaza",element));
							corrida.setUbicacion(admonXml.getValue("Ubicacion",element));
							corrida.setFechaInicial(admonXml.getValue("Fecha_Inicial",element));							
							corrida.setCupoAutobus(valida.getInt(admonXml.getValue("Cupo_Autobus",element)));
							corrida.setErrorNumero(valida.getInt(admonXml.getValue("Error_Numero",element)));
							corrida.setErrorCadena(admonXml.getValue("Error_Cadena",element));
							corrida.setDispNinos(valida.getInt(admonXml.getValue("Disp_Ninios",element)));
							corrida.setDispPromo(valida.getInt(admonXml.getValue("Disp_Promo",element)));
							corrida.setTarifaPromo(valida.getFloat(admonXml.getValue("Tarifa_Promo",element)));
							corrida.setFechaLlegada(admonXml.getValue("Fecha_Llegada",element));
							corrida.setMoneda(admonXml.getValue("Moneda",element));
							corrida.setDetServicio(buscaDetServicio(tipoServ, corrida.getServicio()));
							//corrida.setIva(valida.getInt(admonXml.getValue("Aplica_IVA",element)));
							corrida.setIva((((int)Math.random()*10)%2)==0?1:0);
							
							/*corrida.setDispPaisano(valida.getInt(admonXml.getValue("Disp_Progpaisano",element)));
							corrida.setOficinaFrontera(admonXml.getValue("Oficina_Frontera",element));
							corrida.setPorcentajeTramoFrontera(valida.getFloat(admonXml.getValue("Porcentaje_Tramo_Frontera",element)));
							corrida.setTarifaProgPaisano(valida.getFloat(admonXml.getValue("Tarifa_Progpaisano",element)));*/
							
							log.info("Corrida "+corrida.getCorrida());
							
							if(corrida.getErrorNumero()==0){ 
								corridas.add(corrida);
							}
						}					
					}
				}
			
			
		}catch (RemoteException e) {			
			//log.info("ServiciosEtnImp:obtenerCorridas(): "+ e.getStackTrace());
			log.error("RemoteException : ",e);
		}
		
		if(corridas.size() == 0){
			CorridaDto corrida = new CorridaDto();
			corrida.setErrorCadena("No existen corridas para la oficina destino, con la fecha y hora propuesta.");
			corrida.setErrorNumero(1);
			corridas.add(corrida);
		}
		
		return corridas;
	}
	
	public String buscaDetServicio(List<TipoServicioDto> listaServicios, String servicio){
		
		//Se buscan las corridas v�lidas para los tipos de servicios
//		log.info("tipo serv a buscar "+servicio);
		Iterator<?> iter = listaServicios.iterator();
		while (iter.hasNext())			
		{
			TipoServicioDto tipoDto = (TipoServicioDto) iter.next();
//			log.info("tipo Serv en curso "+tipoDto.getIdServicio());
			if(tipoDto.getIdServicio().equals(servicio)){
				return tipoDto.getDetServicio();
			}
		}
		
		log.info("No encontro el servicio, pone el default DE LUJO ");
		return "DE LUJO";
	}
	
	/* (non-Javadoc)
	 * @see mx.com.addcel.business.ServiciosEtn#obtenerTarifas(mx.com.addcel.dto.TarifaPeticionDto)
	 * Actualizaci�n: ELopez
	 * M�todo que interpreta la salida del WS consultaTarifas, que arroja un String separado por "," donde
	 * cada uno de los elementos son las distintas tarifas de la corrida consultada. 
	 * El resultado es dejado en el objeto TarifasDto.
	 * 
	 */
	@Override
	public TarifasDto obtenerTarifas(TarifaPeticionDto datosPeticionTarifa) {
		
		//String respuesta = "235.00,218.00,218.00,218.00,326.00,200.00,0,Sin Error";
		//String respuesta = "0,0,0,0,0,0,5,Ciudad Origen no Existe.";
		String respuesta="";		
		List<Float> tarifas = new ArrayList<Float>();
		TarifasDto detTarifas = new TarifasDto();			
		String[] elementos;
		//UnsignedByte u = new UnsignedByte();
		
		try{
			//cadena de prueba : ETN,CELA,ACAP,PR,ETN
			//respuesta = etn.consultaTarifas("ETN","CELA","ACAP",u,"ETN");
			respuesta = etn.consultaTarifas(datosPeticionTarifa.getEmpresaVende(),datosPeticionTarifa.getOrigen(),
					datosPeticionTarifa.getDestino(),datosPeticionTarifa.getTipoServicio(),datosPeticionTarifa.getEmpresaViaje());
								
//			log.info("respuesta "+respuesta);
			//log.info("ServiciosEtnImp:obtenerTarifas().consultaTarifas_WSResponse "+respuesta);
			if(respuesta != null){
				elementos = respuesta.split(",");
						
				try{
					for(int i=0; i<elementos.length; i++){
						if(i<6){					
							tarifas.add(Float.parseFloat(elementos[i]));
						}else if(i==6){					
							detTarifas.setCodError(elementos[i]);
						}else{
							detTarifas.setDetError(elementos[i]);
						}
					}		
					detTarifas.setTarifas(tarifas);
					
				}catch (NumberFormatException ex){
					detTarifas.setCodError(Integer.toString(constantesEtn.ETN_ERROR_CODE));
					detTarifas.setDetError(constantesEtn.ETN_ERROR_MESS);
					//log.info("ServiciosEtnImp:obtenerTarifas(). "+constantesEtn.ETN_MESSAGE_ERROR);
				}
			}else{
				log.info("Error en WS destinos respuesta = "+respuesta);
			}
		}catch (RemoteException e) {			
			log.error("Error en servicio ServiciosEtnImp:ObtenerTarifas(): ", e);
		}
								
		return detTarifas;
	}

	/* (non-Javadoc)
	 * @see mx.com.addcel.business.ServiciosEtn#consultarOcupacion(mx.com.addcel.dto.OcupacionPeticionDto)
	 * Actualizaci�n: ELopez
	 * M�todo que interpreta la salida del WS consultaOcupacion, que arroja un String separado por pipes "|" . 
	 * Donde la primera parte es el detalle de los asientos, la segunda es el c�digo y detalle de error.
	 * El resultado es guardado en el objeto OcupacionDto.
	 */
	@Override
	public OcupacionDto consultarOcupacion(
			OcupacionPeticionDto datosPeticionOcupacion) {
		
		//String respuesta = "01010000000000000000000000000000000000000000000000|00|Sin Error";
		String respuesta;	
		String cadena,subcadena,asientosTotales;
		ArrayList<String> asientos = new ArrayList<String>();		
		int aux1 =2;
		int maxAsientos = 0;
		String[] elementos;		
		//double y = 7392;
		OcupacionDto ocupacionAsientos    = new OcupacionDto();		
		DiagramaPeticionDto datosConsulta = new DiagramaPeticionDto();
		
		try{
			log.info("Consulta Ocupaci�n ");
			//respuesta = etn.consultaOcupacion("CAMI","GDLJ","18012013",y,"ETN","ETN");
			
			//Se consulta el diagrama para obtener el n�mero m�ximo de asientos
			datosConsulta.setCorrida(datosPeticionOcupacion.getStrCorrida());
			datosConsulta.setFecha(datosPeticionOcupacion.getFecha());
			datosConsulta.setEmpresaVende(datosPeticionOcupacion.getEmpresaVende());
			datosConsulta.setEmpresaViaje(datosPeticionOcupacion.getEmpresaViaje());
			
			DiagramaDto diagrama = consultarDiagramaAutobus(datosConsulta);
			maxAsientos = diagrama.getNumAsientos();
						
			respuesta = etn.consultaOcupacion(datosPeticionOcupacion.getOrigen(), datosPeticionOcupacion.getDestino(),
					datosPeticionOcupacion.getFecha(), datosPeticionOcupacion.getCorrida(),
					datosPeticionOcupacion.getEmpresaVende(),datosPeticionOcupacion.getEmpresaViaje());
						
//			log.info("Respuesta consultaOcupacion"+respuesta);
			//log.info("ServiciosEtnImp:consultarOcupacion().consultaOcupacion_WSResponse "+respuesta);
			if(respuesta != null){
				
				elementos = respuesta.split("\\|");
				
				if(elementos.length == 3){     // Se valida el formato de la cadena
					
					if(elementos[1].equals("00")){  //Cadena is OK			
						cadena = elementos[0].trim();
						asientosTotales = cadena.substring(0, maxAsientos);						
						try{							
							for(int aux=0; aux<asientosTotales.length(); aux++){
								if(aux1 <= asientosTotales.length()){
									aux1 = aux+1;
									subcadena = asientosTotales.substring(aux,aux1);									
									asientos.add(subcadena);
								}
							}
						}catch (NumberFormatException ex){					
							ocupacionAsientos.setCodigoError(Integer.toString(constantesEtn.ETN_ERROR_CODE));
							ocupacionAsientos.setDescripcionError(constantesEtn.ETN_ERROR_MESS);
							//log.info("ServiciosEtnImp:consultarOcupacion(). "+constantesEtn.ETN_MESSAGE_ERROR);											
						}
						ocupacionAsientos.setAsientos(asientos);
						ocupacionAsientos.setCodigoError(elementos[1]);
						ocupacionAsientos.setDescripcionError(elementos[2]);
						
					}else if(elementos[1].equals("19")){ // Error la corrida NO existe
						ocupacionAsientos.setCodigoError(elementos[1]);
						ocupacionAsientos.setDescripcionError(elementos[2]);						
					}else{ // Un error no identificado
						log.info("Error");
						ocupacionAsientos.setCodigoError(Integer.toString(constantesEtn.ETN_ERROR_CODE));
						ocupacionAsientos.setDescripcionError(constantesEtn.ETN_ERROR_MESS);
					}			
					
				}else{
					ocupacionAsientos.setCodigoError(Integer.toString(constantesEtn.ETN_ERROR_CODE));
					ocupacionAsientos.setDescripcionError(constantesEtn.ETN_ERROR_MESS);
					//log.info("ServiciosEtnImp:consultarOcupacion(). "+constantesEtn.ETN_MESSAGE_ERROR);
				}
			}else{
				log.info("Error en WS destinos respuesta = "+respuesta);
			}
			
		} catch (RemoteException e) {		
			log.error("Error en servicio ServiciosEtnImp:consultarOcupacion(): " , e);			
		}
			
		return ocupacionAsientos;
	}

	/* (non-Javadoc)
	 * @see mx.com.addcel.business.ServiciosEtn#consultarDiagramaAutobus(mx.com.addcel.dto.DiagramaPeticionDto)
	 * Actualizaci�n: ELopez
	 * M�todo que interpreta la salida del WS consultaDiagrama, que arroja un String separado por "," que indica el diagrama 
	 * de los asientos y la segunda parte es el c�digo y el detalle de error.
	 * El resultado se guarda en el objeto DiagramaDto.
	 */
	@Override
	public DiagramaDto consultarDiagramaAutobus(
			DiagramaPeticionDto datosPeticionDiagrama) {
		
		/*String respuesta = "01  ,00  ,02  ,04T ,05  ,00  ,06  ,08  ,09  ,00  ,10  ,12  ,13T ,00  ,14  ,16  ,"+
		"17  ,00  ,18  ,20  ,21  ,00  ,22  ,24T ,25  ,00  ,26  ,28  ,29  ,00  ,30  ,32  ,00  ,00  ,00  ,00  ,00  ," +
		"00  ,00  ,00  ,00  ,00  ,00  ,00  ,00  ,00  ,00  ,00  ,00M ,00C ,00C ,00C ,00H ,00  ,    ,    ,|9|8| ";    Se modificó con el cambio*/ 
		//String respuesta = ".|.|.|Corrida No Existe";  Se agrega un campo mas
		String respuesta;
		String patron1 = "(\\d+).*";	
		/*UnsignedInt uIcorrida = new UnsignedInt();
    	Long corridaLong = 7392L;
    	uIcorrida.setValue(corridaLong);*/
		
    	int aux2 = 0;
		String[] elementos;
		String[] diagrama  = null;
		Pattern patron = Pattern.compile(patron1);
		Matcher matcher;		
		List<Integer> lista = new ArrayList<Integer>();
		
		DiagramaDto diagramaDto = new DiagramaDto();
		
		try{
			log.info("Entre a consulta diagrama ");
			//respuesta = etn.consultaDiagrama("ETN",uIcorrida,"22012013","ETN");
			
			respuesta = etn.consultaDiagrama(datosPeticionDiagrama.getEmpresaVende(),datosPeticionDiagrama.getCorrida(),
				datosPeticionDiagrama.getFecha(),datosPeticionDiagrama.getEmpresaViaje());
			
//			log.info("respuesta "+respuesta);
			
			if(respuesta != null){
				
				//log.info("ServiciosEtnImp:consultarDiagramaAutobus().consultaDiagrama_WSResponse "+respuesta);
				elementos = respuesta.split("\\|");
				
				if(elementos.length >= 2){
					
					diagrama = elementos[0].split(",");
					diagramaDto.setDiagramaAutobus(diagrama);
					
					if(elementos[1].equals(".")){	
						diagramaDto.setNumAsientos(0);
						diagramaDto.setNumFilas(Integer.toString(constantesEtn.ETN_ROOT_CODE));
						diagramaDto.setNumFilasArriba(Integer.toString(constantesEtn.ETN_ROOT_CODE));
						diagramaDto.setNumError(constantesEtn.ETN_ROOT_CODE);
						diagramaDto.setDescError(constantesEtn.ETN_ROOT_MESS);
					}else{
						
						for(int aux= 0; aux<diagrama.length;aux++){						
							matcher = patron.matcher(diagrama[aux]);
							if(matcher.matches()){
								aux2 = Integer.parseInt(matcher.group(1));							
								if(aux2 > 0){
									lista.add(aux2);
								}
							}
						}
						
						diagramaDto.setNumAsientos(lista.size());
						diagramaDto.setNumFilas(elementos[1]);
						diagramaDto.setNumFilasArriba(elementos[2]);
						diagramaDto.setNumError(constantesEtn.ETN_OK_CODE);
						diagramaDto.setDescError(constantesEtn.ETN_OK_MESS);
					}
					
					try{				
						diagramaDto.setDescripcion(elementos[3]);
					}catch (ArrayIndexOutOfBoundsException ex){				
						diagramaDto.setDescError(constantesEtn.ETN_OK_MESS);
					}
					
				}else{ // Si la cadena de salida tiene un formato incorrecto			
					diagramaDto.setNumError(constantesEtn.ETN_ERROR_CODE);
					diagramaDto.setDescError(constantesEtn.ETN_ERROR_MESS);
					//log.info("ServiciosEtnImp:consultarDiagramaAutobus(). "+constantesEtn.ETN_MESSAGE_ERROR);				
				}
				
			}else{
				log.info("Error en WS consultaDiagrama respuesta = "+respuesta);
			}
		
		}catch (RemoteException e) {
			log.error("Error en servicio ServiciosEntImp:consultarDiagramaAutobus(): ", e);			
		}
		
		return diagramaDto;
	}

	/* (non-Javadoc)
	 * @see mx.com.addcel.business.ServiciosEtn#solicitarReservacion(mx.com.addcel.dto.ReservacionPeticionDto)
	 * Actualizaci&oacute;n: ELopez 
	 * M&eacute;todo que intepreta la salida del WS peticionReservacion, que arroja un String separado por pipes "|" que indica 
	 * los datos de la reservaci�n. El String es procesado y los datos se guardan en ReservacionDto.
	 */
	@Override
	public ReservacionDto solicitarReservacion(ReservacionPeticionDto datosPeticionReservacion, 
												BitacoraPeticionDto bitacora, String asientosStr, String nombPipes) {
		bitacora.setEstatus("0");
		
		GeneraBitacora bitacoraETN = new GeneraBitacora();
		String respuesta = null;
		//, tipoAsientos="", importes="";
		int contAd = 0, contNi = 0, contIn = 0, contEst = 0, contMa = 0; 
		String[] elementos;
		String[] listaAsientos;
		String[] listaNombres;
		//double x= 2;
		//double y = 0;
		//double c = 7392;
		ReservacionDto reservacion = new ReservacionDto();
		List<BoletoETNDto> listaBoletos = new ArrayList<BoletoETNDto> ();
		//int numeroAsientosSolicitados=0;
		
		try{
			log.info("Solicitando reservacion...");
			
			//respuesta = etn.peticionReservacion("CAMI","GDLJ","06062013","090000",x,y,y,y,y,c,"3233","PEDRO PEREZ",0,"TAP","ETN","P","M","CS");

			log.info(Functions.toString(datosPeticionReservacion));
			
			if (ConstantesEtn.AMBIENTE_ACTUAL.equals(ConstantesEtn.AMBIENTE_PROD)) {
				respuesta = etn.peticionReservacion(datosPeticionReservacion.getIdOrigen(), datosPeticionReservacion.getIdDestino(),
						datosPeticionReservacion.getFecha(),datosPeticionReservacion.getHora(),datosPeticionReservacion.getNumAdulto(),
						datosPeticionReservacion.getNumNino(),datosPeticionReservacion.getNumInsen(),datosPeticionReservacion.getNumEstudiante(),
						datosPeticionReservacion.getNumMaestro(),datosPeticionReservacion.getCorrida(),datosPeticionReservacion.getAsientos(),
						datosPeticionReservacion.getNombres(),datosPeticionReservacion.getFolioReservacion(),datosPeticionReservacion.getEmpresaVende(),
						datosPeticionReservacion.getEmpresaViaje(),datosPeticionReservacion.getTipoTerminal(),datosPeticionReservacion.getTipoCliente(),
						datosPeticionReservacion.getTipoOperacion());
            } else if (ConstantesEtn.AMBIENTE_ACTUAL.equals(ConstantesEtn.AMBIENTE_QA)) {
            	//respuesta = "000000001524|1534.25|0|Sin Error";
        		//respuesta = "0|0|5|Asientos Solicitados Ya Ocupados";
            	respuesta = ConstantesEtn.NUMERO_AUTORIZACION++ + "|" +
            			((datosPeticionReservacion.getTarifaAdulto() * datosPeticionReservacion.getNumAdulto()) +
    					(datosPeticionReservacion.getTarifaEstudiante() * datosPeticionReservacion.getNumEstudiante()) +
    					(datosPeticionReservacion.getTarifaNino() * datosPeticionReservacion.getNumNino()) +
    					(datosPeticionReservacion.getTarifaInsen() * datosPeticionReservacion.getNumInsen()) +
    					(datosPeticionReservacion.getTarifaMaestro() * datosPeticionReservacion.getNumMaestro())) +
    					"|0|Sin Error";
            }
						
			log.info("Respuesta reservacion ETN: "+respuesta);	
			//log.info("ServiciosEtnImp:solicitarReservacion().peticionReservacion_WSResponse "+respuesta);
			//id|100.0|0|Exito|
			if(respuesta!=null){
				elementos = respuesta.split("\\|");
				
				try{
					
					ViajeETNDto viajeEtn = new ViajeETNDto();
					BoletoETNDto boleto = null;
					String asientoTmp = null;
					
					if(elementos[2].equals("0")){ //Transaccion sin error
						
						log.info("paso la transaccion");
						contAd = 0; 
						contNi = 0; 
						contIn = 0; 
						contEst = 0; 
						contMa = 0;
						
						reservacion.setIdReservacion(elementos[0]);
						reservacion.setImporteTotal(valida.getFloat((elementos[1])));
						reservacion.setNumError(valida.getInt(elementos[2]));
						reservacion.setDescError(elementos[3]);
						//vvlira 20130814 evitar error en la reservación
						if(!validaReservacionExito(datosPeticionReservacion, reservacion.getImporteTotal()).equalsIgnoreCase("EXITO")){
							log.info("Error en la reservacion de boletos");
							reservacion.setIdReservacion(elementos[0]);
							reservacion.setImporteTotal(Float.parseFloat(elementos[1]));
							reservacion.setNumError(404);
							reservacion.setDescError("Error al reservar");
						}else{
							log.info("asientos: "+asientosStr);
																			
							listaAsientos = asientosStr.split("\\|");
							
							log.info("nombres: "+datosPeticionReservacion.getNombres());
							
							listaNombres  = nombPipes.split("\\,");
	
							log.info("Tamaño lista nombres: "+listaNombres.length);
							log.info("Tamaño lista asientos: "+listaAsientos.length);
							
							viajeEtn.setIdViaje(datosPeticionReservacion.getIdViaje());
							viajeEtn.setIdReservacion(valida.getLong(reservacion.getIdReservacion()));
							viajeEtn.setCorridaETN(datosPeticionReservacion.getCorrida()+"");
							viajeEtn.setIdOrigen(datosPeticionReservacion.getIdOrigen());
							viajeEtn.setIdDestino(datosPeticionReservacion.getIdDestino());
							viajeEtn.setOrigenETN(datosPeticionReservacion.getOrigen());
							viajeEtn.setDestinoETN(datosPeticionReservacion.getDestino());
							viajeEtn.setFechaETN(datosPeticionReservacion.getFecha());
							viajeEtn.setHoraETN(datosPeticionReservacion.getHora());
							viajeEtn.setDiaViaje(new ObtenDiaFecha().getDiaFecha(datosPeticionReservacion.getFecha()));
							viajeEtn.setTipoViaje(bitacora.getTipoViaje());
							viajeEtn.setTipoCorrida(datosPeticionReservacion.getTipoCorrida());
							viajeEtn.setImporteTotal(reservacion.getImporteTotal());
							
							//Por cada asiento se hace una iteración para obtener los datos del boleto y guardarlos en la DB						
							for(int aux = 0; aux<listaAsientos.length; aux++){
								
								log.info(" Contador Asiento: "+aux+" de: "+listaAsientos.length);
								boleto = new BoletoETNDto();
								
								asientoTmp = listaAsientos[aux].trim();
					    		if(asientoTmp.length()==2){
					    			log.info("LLega asiento defectuoso, arreglo el problema");
					    			log.info("Original : "+ asientoTmp);
					    			log.info("Arreglado : 0"+asientoTmp);
					    			listaAsientos[aux]="0"+asientoTmp;
					    		}
								
								log.info("reserva: "+reservacion.getIdReservacion());
								log.info("nombre:  "+listaNombres[aux]);
								log.info("asiento: "+listaAsientos[aux].trim());
								
								boleto.setIdViaje(datosPeticionReservacion.getIdViaje());
								boleto.setIdReservacion(Long.parseLong(reservacion.getIdReservacion()));
								boleto.setNombre(listaNombres[aux]);
								boleto.setAsiento(listaAsientos[aux].trim());
								
								//Para obtener los tipos de asientos
								if(datosPeticionReservacion.getNumAdulto() > 0 && contAd < datosPeticionReservacion.getNumAdulto()){
									log.info("Adulto "+contAd);
									log.info("# adultos "+datosPeticionReservacion.getNumAdulto());
									boleto.setTipoAsiento("AD");
									boleto.setImporteAsiento(datosPeticionReservacion.getTarifaAdulto());
									contAd++;
									listaBoletos.add(boleto);
									continue;
								}
								if(datosPeticionReservacion.getNumNino() > 0 && contNi < datosPeticionReservacion.getNumNino()){
									log.info("Niño "+contNi);
									log.info("# niños "+datosPeticionReservacion.getNumNino());
									boleto.setTipoAsiento("NI");
									boleto.setImporteAsiento(datosPeticionReservacion.getTarifaNino());
									contNi++;
									listaBoletos.add(boleto);
									continue;
								}
								if(datosPeticionReservacion.getNumInsen() > 0 && contIn < datosPeticionReservacion.getNumInsen()){
									log.info("Insen "+contIn);
									log.info("# insen "+datosPeticionReservacion.getNumInsen());
									boleto.setTipoAsiento("IN");
									boleto.setImporteAsiento(datosPeticionReservacion.getTarifaInsen());
									contIn++;
									listaBoletos.add(boleto);
									continue;
								}
								if(datosPeticionReservacion.getNumEstudiante() > 0 && contEst < datosPeticionReservacion.getNumEstudiante()){
									log.info("Estudiante "+contEst);
									log.info("# estudiante "+datosPeticionReservacion.getNumEstudiante());
									boleto.setTipoAsiento("ES");
									boleto.setImporteAsiento(datosPeticionReservacion.getTarifaEstudiante());
									contEst++;
									listaBoletos.add(boleto);
									continue;
								}
								if(datosPeticionReservacion.getNumMaestro() > 0 && contMa < datosPeticionReservacion.getNumMaestro()){
									log.info("Maestro "+contMa);
									log.info("# maestro "+datosPeticionReservacion.getNumMaestro());
									boleto.setTipoAsiento("MA");
									boleto.setImporteAsiento(datosPeticionReservacion.getTarifaMaestro());
									contMa++;
									listaBoletos.add(boleto);
									continue;
								}
								
							}
							
							viajeEtn.setListaBoletos(listaBoletos);
							
							// Ingresar a bitacora si la reservaci�n fue exitosa
							bitacora.setEstatus(constantesEtn.ETN_INIT_STATUS+"");						
							bitacora.setFolioETN(reservacion.getIdReservacion());						
							bitacora.setIdViaje(datosPeticionReservacion.getIdViaje()+"");
							
							log.info("antes de ingresar a la bitacora");
							
							bitacoraResp = bitacoraETN.ingresaBitacora(reservacion,bitacora,viajeEtn); 
	
							log.info("después de ingresar");
							
							reservacion.setIdBitacora(bitacoraResp.getId()+"");
							
							if(bitacoraResp.getId() == 0){ // Si no se ingres� a la bit�cora se hace la liberaci�n de la reservaci�n
								
								log.info("no se guardo");
								LibReservaPeticionDto libResPetDto = new LibReservaPeticionDto();
								libResPetDto.setFolioReservacion(reservacion.getIdReservacion());
								libResPetDto.setEmpresaVende(datosPeticionReservacion.getEmpresaVende());
								libResPetDto.setEmpresaViaja(datosPeticionReservacion.getEmpresaViaje());
								
								if (ConstantesEtn.AMBIENTE_ACTUAL.equals(ConstantesEtn.AMBIENTE_PROD)) {
									solicitaLiberaReserva(libResPetDto);
								}
								// Se regresa la reservaci�n con el error transmisi�n
								reservacion.setIdReservacion("0");
								reservacion.setImporteTotal(0);
								reservacion.setNumError(constantesEtn.ETN_ERROR_CODE_TRANS);
								reservacion.setDescError(constantesEtn.ETN_ERROR_MESS_TRANS);
							}
						}
					} else { //Asientos ya ocupados
						log.info("Asientos ocupados");
						reservacion.setIdReservacion(elementos[0]);
						reservacion.setImporteTotal(Float.parseFloat(elementos[1]));
						if (elementos.length == 5) {
							reservacion.setNumError(Integer.parseInt(elementos[3]));
							reservacion.setDescError(elementos[4]);
						} else {
							reservacion.setNumError(Integer.parseInt(elementos[2]));
							reservacion.setDescError(elementos[3]);
						}
						
					}
					
				} catch (NumberFormatException ex){ // Si la cadena tiene un formato incorrecto se genera un objeto de error 
					
					reservacion.setNumError(constantesEtn.ETN_ERROR_CODE);
					reservacion.setDescError(constantesEtn.ETN_ERROR_MESS);			
					//log.info("ServiciosEtnImpl:solicitarReservacion(). "+constantesEtn.ETN_MESSAGE_ERROR);
				}
			}else{
				log.info("Error en WS solicitarReservacion respuesta = "+respuesta);
			}
		}catch (RemoteException e) {	
			log.info("remote exception "+e);
			//log.info("ServiciosEtnImpl:solicitarReservacion() "+e.getStackTrace());	
			reservacion.setNumError(constantesEtn.ETN_ERROR_CODE);
			reservacion.setDescError(constantesEtn.ETN_ERROR_MESS);	
		}
				
		return reservacion;
	}

	/* (non-Javadoc)
	 * @see mx.com.addcel.business.ServiciosEtn#solicitarVentaBoleto(mx.com.addcel.dto.VentaBoletoPeticionDto)
	 * Actualizaci�n: ELopez
	 * M�todo que interpreta la salida de la consulta al WS peticionVendeBoleto, el cu�l arroja un String separados por "," 
	 * y por pipes "|", d�nde la primera parte son los datos de los asientos y la segunda es el c�digo y detalle del error. 
	 *  La respuesta es procesada y almacenada en el objeto VentaBoletoDto.
	 */
	@Override
	public VentaBoletoDto solicitarVentaBoleto(VentaBoletoPeticionDto datosPeticionVenta, String autorizaBanco) {
		
		boolean respVta = false;
		int operacion = 0;
		String respuesta = null;
		String operStr= "";
		String formasPagoTemp="";
		String monto="";
		String montoDec="";
		String[] elementos;
		String[] operaciones;
		String[] operacionesReales;
		/*UnsignedByte b = new UnsignedByte(); Comentados porque sirven para las pruebas
		UnsignedInt  i = new UnsignedInt();
		UnsignedLong l = new UnsignedLong();*/
		
		List<ViajeETNDto> listaViajes = new ArrayList<ViajeETNDto>();
		List<ViajeETNDto> listaViajesProcesados = new ArrayList<ViajeETNDto>();
		VentaBoletoDto ventaBoleto = new VentaBoletoDto();		 
		CancelacionPeticionDto datosCancelacion = new CancelacionPeticionDto();
		GeneraBitacora bitacoraETN = new GeneraBitacora();
		ViajeETNDto regEtn = null;
		long idBitacora=0;
		try{
			
			log.info("Entrando a peticionVendeBoleto "); 
//			respuesta = etn.peticionVendeBoleto("CELA","ACAP","01012012","050000",b,b,b,b,b,i,"0102","PEDRO PEREZ","TAP","ETN",b,"0","P","M","CS",b,l,i,i,
//					"SI","NA",l,"NA","01012012","TB","NA",l);
			
			ConsultaViajesETN viajes = bitacoraETN.consultaViajesETN(datosPeticionVenta.getIdViaje());
			
			if(viajes != null){
				
				listaViajes = viajes.getListaViajes();
				Iterator<?> iter = listaViajes.iterator();
				
				int oo =1;
				while (iter.hasNext()){
					log.info("PROCESANDO EN ETN VIAJE " + oo + " DE " + listaViajes.size());
					
					regEtn = (ViajeETNDto) iter.next();
					regEtn.setIdViaje(datosPeticionVenta.getIdViaje());
					
					log.info("Actualizando el idBitacora: "+regEtn.getIdBitacora());
					log.info("Actualizando el viaje: "+datosPeticionVenta.getIdViaje());
					log.info("Id Viaje "+datosPeticionVenta.getIdViaje());
					
					bitacoraResp.setNo_autorizacion(autorizaBanco);
					
					if(datosPeticionVenta.getSuccess().equals("true")){
						log.info("origen: "+regEtn.getIdOrigen());
						log.info("corrida: "+valida.getDouble(regEtn.getCorridaETN()));
						log.info("fecha: "+datosPeticionVenta.getFecha());
						log.info("ide Res: "+valida.getDouble(String.valueOf(regEtn.getIdReservacion())));
						log.info("empre vende: "+datosPeticionVenta.getEmpresaVende());
						log.info("empre viaje: "+datosPeticionVenta.getEmpresaViaje());
						log.info("suc ext: "+ datosPeticionVenta.getSucursalExterna());
						log.info("ofic ext: "+datosPeticionVenta.getOficinaExterna());
						log.info("fecha contable: "+datosPeticionVenta.getFechaContable());
						log.info("pago externa: "+datosPeticionVenta.getFormasPagoExternas());
						log.info("pago temp: "+datosPeticionVenta.getEbFormasPagoTemp());
						log.info("num sesion: "+datosPeticionVenta.getNumSesion());
						log.info("monto total: "+datosPeticionVenta.getMonto());
						log.info("iva "+datosPeticionVenta.getIva());
						
						monto = datosPeticionVenta.getMonto();
						
						if(monto != null && monto.length() >= 2){
							montoDec = monto.substring(monto.length()-2, monto.length());
							monto = monto.substring(0, monto.length()-2);
							monto = monto + "." + montoDec;
						}
						
						log.info("monto con punto "+monto);
						
						formasPagoTemp = 
								"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"+
								"<FormasPago xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"+
								"<Record>"+
								"<VTpat_nMontoMN>"+monto+"</VTpat_nMontoMN>"+
								"<VTpat_lPagoAutomatico>False</VTpat_lPagoAutomatico>"+
								"<VTpat_aClaveFormaPago>TB</VTpat_aClaveFormaPago>"+
								"<VTpat_aSubFormaPago>MA</VTpat_aSubFormaPago>"+
								"<VTpat_nNumeroTarjetaBancaria>"+datosPeticionVenta.getTarjeta()+"</VTpat_nNumeroTarjetaBancaria>"+
								"<VTpat_aClaveTipoTarjetaBancaria>"+regEtn.getTipoTarjeta()+"</VTpat_aClaveTipoTarjetaBancaria>"+
								"<VTpat_aNombreTarjetaHabiente>"+regEtn.getNombre()+"</VTpat_aNombreTarjetaHabiente>"+
								"<VTpat_nCodigoSeguridad>111</VTpat_nCodigoSeguridad>"+
								"<VTpat_nVoucher>"+autorizaBanco+"</VTpat_nVoucher>"+
								"<VTpat_nNumeroAutorizacion>"+autorizaBanco+"</VTpat_nNumeroAutorizacion>"+
								"<VTpat_aMetodoAutTransaccion>M</VTpat_aMetodoAutTransaccion>"+
								"</Record>"+
								"<Record> "+
								"<VTpat_nMontoMN>"+monto+"</VTpat_nMontoMN>"+
								"<VTpat_lPagoAutomatico>True</VTpat_lPagoAutomatico>"+
								"<VTpat_aClaveFormaPago>TB</VTpat_aClaveFormaPago>"+
								"<VTpat_aSubFormaPago>MA</VTpat_aSubFormaPago>"+
								"<VTpat_nNumeroTarjetaBancaria>"+datosPeticionVenta.getTarjeta()+"</VTpat_nNumeroTarjetaBancaria>"+
								"<VTpat_aClaveTipoTarjetaBancaria>"+regEtn.getTipoTarjeta()+"</VTpat_aClaveTipoTarjetaBancaria>"+
								"<VTpat_aNombreTarjetaHabiente>"+regEtn.getNombre()+"</VTpat_aNombreTarjetaHabiente>"+
								"<VTpat_nCodigoSeguridad>111</VTpat_nCodigoSeguridad>"+
								"<VTpat_nVoucher>"+autorizaBanco+"</VTpat_nVoucher>"+
								"<VTpat_nNumeroAutorizacion>"+autorizaBanco+"</VTpat_nNumeroAutorizacion>"+
								"</Record>"+
								"</FormasPago>";
						
						log.info("formasPagoTemp: "+formasPagoTemp);
						
						idBitacora=regEtn.getIdBitacora();
						if (ConstantesEtn.AMBIENTE_ACTUAL.equals(ConstantesEtn.AMBIENTE_PROD)) {
							respuesta = etn.peticionConfirmacion(regEtn.getIdOrigen(), valida.getDouble(regEtn.getCorridaETN()), regEtn.getFechaETN(), 
									valida.getDouble(String.valueOf(regEtn.getIdReservacion())), datosPeticionVenta.getEmpresaVende(), datosPeticionVenta.getEmpresaViaje(), datosPeticionVenta.getSucursalExterna(), 
									datosPeticionVenta.getOficinaExterna(), datosPeticionVenta.getFechaContable(), datosPeticionVenta.getFormasPagoExternas(), formasPagoTemp, 
									datosPeticionVenta.getNumSesion());
			            } else if (ConstantesEtn.AMBIENTE_ACTUAL.equals(ConstantesEtn.AMBIENTE_QA)) {
			            	//respuesta = "000000001524|1534.25|0|Sin Error";
			        		//respuesta = "0|0|5|Asientos Solicitados Ya Ocupados";
			            	int aux = 0;
			            	respuesta = "";
			            	for(; aux <= regEtn.getListaBoletos().size(); aux++){
			            		respuesta += "00000000" + (1500 + aux) + ",";
			            	}
			            	respuesta = respuesta.substring(0, respuesta.length() -1);
			            	respuesta += "|0|" + aux + "|Sin Error"; 
			        		//rrespuesta = "000000000000|5|0|Asientos Solicitados Ya Ocupados";
//			            	respuesta = datosPeticionVenta.getIdViaje() + "|" + monto + "|0|Sin Error";
			            	//regEtn.setIdReservacion(datosPeticionVenta.getIdViaje());
			            }
											
						log.info("respuesta "+respuesta);
						
						//log.info("ServiciosEtnImp:solicitarVentaBoleto().peticionVendeBoleto_WSResponse "+respuesta);
						if(respuesta != null){
							elementos = respuesta.split("\\|");
							
							try{
								if(elementos.length==4){
									
									operaciones = elementos[0].split(",");
									
									log.info("tamaño de las operaciones "+operaciones.length);
									
									if(operaciones.length == 1){
										operacionesReales = operaciones;
									}else{
										operacionesReales = new String[operaciones.length-1]; //Se hace esto porque el WS siempre arroja una operación mas
										for(int i=0; i<operaciones.length-1; i++){
											operacionesReales[i] = operaciones[i];
											log.info("operacion valida numero ["+i+"] valor["+operacionesReales[i]+"]");
										}
									}
									
									log.info("operaciones reales "+operacionesReales.length);
									log.info("nit: "+Integer.parseInt(elementos[2]));
									
									listaViajesProcesados.add(regEtn);
									
									ventaBoleto.setOperaciones(operacionesReales);
									ventaBoleto.setCodigoError(Integer.parseInt(elementos[1]));
									ventaBoleto.setNit(Integer.parseInt(elementos[2]));
									ventaBoleto.setDescError(elementos[3]);
									ventaBoleto.setIdTransaccion(bitacoraResp.getId());
									ventaBoleto.setAutBanco(autorizaBanco);
									if(ventaBoleto.getCodigoError() == 0) {
										//Si la compra no tuvo error en el WS
										//Obtener las operaciones
										for(int i=0; i < operacionesReales.length; i++){
											operacion = valida.getInt(operacionesReales[i]);
											operStr = operStr + operacion + "|";
										}
										
										// Se actualiza la bit�cora con la info final
										
										log.info("operaciones string "+operStr);
										bitacoraResp.setConcepto(operStr); 
										bitacoraResp.setStatus(constantesEtn.ETN_OK_STATUS);
										bitacoraResp.setId(regEtn.getIdBitacora());
										bitacoraResp.setTarjeta_compra(datosPeticionVenta.getTarjeta());
										regEtn.setIva(datosPeticionVenta.getIva());
										
										respVta = bitacoraETN.actualizaBitacora(regEtn, ventaBoleto, bitacoraResp, autorizaBanco, datosPeticionVenta.getIdViaje());
										
										if(!respVta){ // Si ocurri� un error al momento de actualizar la bit�cora se cancela la venta
																		
											datosCancelacion.setOrigen(regEtn.getIdOrigen());
											datosCancelacion.setCorrida(regEtn.getCorridaETN());
											datosCancelacion.setFechaSalida(datosPeticionVenta.getFecha());
											datosCancelacion.setFolioVenta((String.valueOf(regEtn.getIdReservacion())));
											datosCancelacion.setEmpresaVende(datosPeticionVenta.getEmpresaVende());
											datosCancelacion.setEmpresaViaja(datosPeticionVenta.getEmpresaViaje());							
											
											if (ConstantesEtn.AMBIENTE_ACTUAL.equals(ConstantesEtn.AMBIENTE_PROD)) {
												cancelarVenta(datosCancelacion);
											}
																		
											ventaBoleto.setOperaciones(operacionesReales);
											ventaBoleto.setNit(constantesEtn.ETN_ROOT_CODE);
											ventaBoleto.setCodigoError(constantesEtn.ETN_ERROR_CODE_TRANS);
											ventaBoleto.setDescError(constantesEtn.ETN_ERROR_MESS_TRANS);
											
											return ventaBoleto;
										}
									}else{
										//Se hizo la operación pero la reservación tiene error
										log.info("Se realizó la operación pero la reservación no se efectuó");
										bitacoraResp.setId(regEtn.getIdBitacora());
										bitacoraResp.setCodigo_error(ventaBoleto.getCodigoError());
										bitacoraResp.setDestino(ventaBoleto.getDescError());										
										bitacoraResp.setStatus(constantesEtn.ETN_DENAY_ERROR_CODE);	 //Si ETN falla, se actualiza la bitácora con estatus 2
										
										log.info("Descripcion "+ventaBoleto.getDescError());
										respVta = bitacoraETN.actualizaBitacora(ventaBoleto, bitacoraResp, autorizaBanco, datosPeticionVenta.getIdViaje());
										return ventaBoleto;
									}
								}else{
									//Si ocurrió un error de la cadena del WS
									ventaBoleto.setCodigoError(constantesEtn.ETN_ERROR_CODE);
									ventaBoleto.setDescError(constantesEtn.ETN_ERROR_MESS);
									bitacoraResp.setId(regEtn.getIdBitacora());
									bitacoraResp.setStatus(constantesEtn.ETN_DENAY_ERROR_CODE);	 //Si ETN falla, se actualiza la bitácora con estatus 2
									respVta = bitacoraETN.actualizaBitacora(ventaBoleto, bitacoraResp, autorizaBanco, datosPeticionVenta.getIdViaje());
									return ventaBoleto;
									//log.info("ServiciosEtnImpl:solicitarVentaBoleto(). "+constantesEtn.ETN_MESSAGE_ERROR);
								}
								
							}catch (NumberFormatException ex) {
								log.error("Ocurrio un error: {}", ex);
								ventaBoleto.setCodigoError(constantesEtn.ETN_ERROR_CODE);
								ventaBoleto.setDescError(constantesEtn.ETN_ERROR_MESS);
								bitacoraResp.setId(regEtn.getIdBitacora());
								bitacoraResp.setStatus(constantesEtn.ETN_DENAY_ERROR_CODE);	 //Si ETN falla, se actualiza la bitácora con estatus 2
								respVta = bitacoraETN.actualizaBitacora(ventaBoleto, bitacoraResp, autorizaBanco, datosPeticionVenta.getIdViaje());
								return ventaBoleto;
								//log.info("ServiciosEtnImpl:solicitarVentaBoleto(). "+constantesEtn.ETN_MESSAGE_ERROR);
							}
						}else{ //Si no se obtuvieron datos
						
							log.info("Error en WS solicitarVentaBoleto respuesta = "+respuesta);
							ventaBoleto.setCodigoError(constantesEtn.ETN_ERROR_CODE);
							ventaBoleto.setDescError(constantesEtn.ETN_ERROR_MESS);
							bitacoraResp.setId(regEtn.getIdBitacora());
							bitacoraResp.setStatus(constantesEtn.ETN_DENAY_ERROR_CODE);	 //Si ETN falla, se actualiza la bitácora con estatus 2
							respVta = bitacoraETN.actualizaBitacora(ventaBoleto, bitacoraResp, autorizaBanco, datosPeticionVenta.getIdViaje());
							return ventaBoleto;
						}
						
					}else{ //Sólo actualizo la bitacora en caso de error de autorizacion
						
						ventaBoleto.setCodigoError(constantesEtn.ETN_ERROR_CODE);
						ventaBoleto.setDescError(datosPeticionVenta.getMensaje());	 //Es el mess de error que envía 3DSecure			
						bitacoraResp.setStatus(constantesEtn.ETN_INIT_STATUS);      //Si 3DSEC falla, se actualiza la bitácora con estatus 0
						bitacoraResp.setId(regEtn.getIdBitacora());
						
						respVta = bitacoraETN.actualizaBitacora(ventaBoleto, bitacoraResp, autorizaBanco, datosPeticionVenta.getIdViaje());
						
						LibReservaPeticionDto libResPetDto = new LibReservaPeticionDto();
						
						libResPetDto.setFolioReservacion(regEtn.getIdReservacion()+"");
						libResPetDto.setEmpresaVende(datosPeticionVenta.getEmpresaVende());
						libResPetDto.setEmpresaViaja(datosPeticionVenta.getEmpresaViaje());
						
						if (ConstantesEtn.AMBIENTE_ACTUAL.equals(ConstantesEtn.AMBIENTE_PROD)) {
							solicitaLiberaReserva(libResPetDto);
						}
					}
					oo++;
					
				}// del while
				
			} else {
				
				log.info("No se obtuvieron los datos del viaje");
				ventaBoleto.setCodigoError(constantesEtn.ETN_ERROR_DATA );
				ventaBoleto.setDescError(constantesEtn.ETN_ERROR_MESS_DATA);
				return ventaBoleto;
				
			}
			
		}catch (RemoteException e) {		
			//log.info("ServiciosEtnImpl:solicitarVentaBoleto() "+e.getStackTrace());
			log.info("Error de comunicación con los WS", e);
			ventaBoleto.setCodigoError(constantesEtn.ETN_ERROR_CODE_TRANS);
			ventaBoleto.setDescError(constantesEtn.ETN_ERROR_MESS_TRANS);
			if(idBitacora!=0){
				bitacoraResp.setId(idBitacora);
				bitacoraResp.setStatus(constantesEtn.ETN_DENAY_ERROR_CODE);	 //Si ETN falla, se actualiza la bitácora con estatus 2
				respVta = bitacoraETN.actualizaBitacora(ventaBoleto, bitacoraResp, autorizaBanco, datosPeticionVenta.getIdViaje());
			}
			return ventaBoleto;
		}catch (Exception e) {		
			//log.info("ServiciosEtnImpl:solicitarVentaBoleto() "+e.getStackTrace());
			log.info("Error en el procesamiento de la compra", e);
			ventaBoleto.setCodigoError(constantesEtn.ETN_ERROR_CODE_TRANS);
			ventaBoleto.setDescError(constantesEtn.ETN_ERROR_MESS_TRANS);
			if(idBitacora!=0){
				bitacoraResp.setId(idBitacora);
				bitacoraResp.setStatus(constantesEtn.ETN_DENAY_ERROR_CODE);	 //Si ETN falla, se actualiza la bitácora con estatus 2
				respVta = bitacoraETN.actualizaBitacora(ventaBoleto, bitacoraResp, autorizaBanco, datosPeticionVenta.getIdViaje());
			}
			return ventaBoleto;
		}

		return ventaBoleto;
	}
	
	@Override
	public List<TipoServicioDto> obtenerTipoServicios(TipoServicioPeticionDto tipoServDto){
		String respuesta="";
		TipoServicioDto tipoServ;
		String[] elementos,subCadena;
		boolean flag = true;
		List<TipoServicioDto> servicios = new ArrayList<TipoServicioDto>();
		
		try{
			
			log.info("Consultando tipo de servicios...");			
			//respuesta = "PR,PRIMERA,01|PP,SERVICIO PLUS,02|Sin Error";	
			//respuesta = etn.getClasesServicio("TAP", "", "ETN");
			
			respuesta = etn.getClasesServicio(tipoServDto.getEmpresaSolicita(), "", tipoServDto.getEmpresaViaja());
//			log.info("respuesta "+respuesta);		
			
			//log.info("ServiciosEtnImp:obtenerDestinos().destinos_WSResponse "+respuesta);
			if(respuesta != null){
				elementos = respuesta.split("\\|");	
				
					for(int i = 0; i < elementos.length; i++){
						
						if(elementos[i].indexOf(",") >0){
							
							subCadena = elementos[i].split(",");
													
							if(subCadena.length > 0 ){								
								tipoServ = new TipoServicioDto();								
								tipoServ.setCveServicio(subCadena[0]);
								tipoServ.setDetServicio(subCadena[1]);
								tipoServ.setIdServicio(subCadena[2]);												
								servicios.add(tipoServ);															
								flag = false;								
							}
						}else{
							if(elementos[i].indexOf("Error") >= 0 && flag){								
								tipoServ = new TipoServicioDto();
								tipoServ.setCveServicio("ER");
								tipoServ.setIdServicio("00");
								tipoServ.setDetServicio(elementos[i]);
								servicios.add(tipoServ);
							}
						}
					}
				}
			
		}catch (RemoteException e) {		
			log.error("RemoteException : ",e);
			//log.info("ServiciosEtnImpl:obtenerTipoServicios() "+e.getStackTrace());
		}
		
		return servicios;
	}
	
	@Override
	public Boolean solicitaLiberaReserva(LibReservaPeticionDto libResPetDto){
		
		String respuesta = "";
		Boolean exito = false; 
		
		try{
			
			log.info("Liberando Reservacion...");
			//UnsignedLong uLdato = new UnsignedLong(5169);
			//respuesta = etn.peticionLiberaReservacion(uLdato,"TAP","ETN");
			
			respuesta = etn.peticionLiberaReservacion(libResPetDto.getFolioReservacion(), libResPetDto.getEmpresaVende(), libResPetDto.getEmpresaViaja());
			log.info("Respuesta peticionLiberaReservacion: "+respuesta);
			
			if(respuesta != null){
				if(respuesta.trim().length() == 0){
					log.info("Se libero la reservacion "+respuesta);
					exito = true;
				}else{
					log.info("Error "+respuesta);
				}
			}
			
		}catch (RemoteException e) {
			log.error("RemoteException : ",e);
			//log.info("ServiciosEtnImpl:solicitaLiberaReserva() "+e.getStackTrace());
		}
		
		return exito;
		
	}
	
	@Override
	public List<ItinerarioDto> obtenerItinerario(ItinerarioPeticionDto datosPeticionItinerario) {
		
		ManejadorXml admonXml = new ManejadorXml();
		
		//String respuesta = "<?xml version=\"1.0\" encoding=\"UTF8\"?><corridas xsi:noNamespaceSchemaLocation=\"corridas.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchemainstance\"> <Record><Destino_final> </Destino_final><Tipo_itinerario> </Tipo_itinerario> <Hora_Corrida> </Hora_Corrida> <Tipo_Corrida></Tipo_Corrida> <Empresa_Corrida></Empresa_Corrida><Servicio></Servicio><Tarifa></Tarifa><Disp_Estudiantes></Disp_Estudiantes><Disp_Insen></Disp_Insen><Disp_Maestros></Disp_Maestros><Disp_General></Disp_General><Corrida>123222</Corrida><Sala>abababa</Sala><Anden>3333</Anden><Tarifa_Adulto>50.0</Tarifa_Adulto><Tarifa_Nino></Tarifa_Nino><Tarifa_Insen></Tarifa_Insen><Tarifa_Estudiante></Tarifa_Estudiante><Tarifa_Maestro></Tarifa_Maestro><Clave_Destino></Clave_Destino><Tipo_Lectura></Tipo_Lectura><Clave_Ctl></Clave_Ctl><Secuencia></Secuencia><Fecha_desfaza></Fecha_desfaza><Ubicacion></Ubicacion><Fecha_Inicial></Fecha_Inicial><Cupo_Autobus></Cupo_Autobus><Error_Numero></Error_Numero><Error_Cadena></Error_Cadena><Disp_Ninios></Disp_Ninios><Disp_Promo></Disp_Promo><Tarifa_Promo></Tarifa_Promo><Fecha_Llegada></Fecha_Llegada><Moneda></Moneda><Disp_Progpaisano></Disp_Progpaisano><Tarifa_Progpaisano></Tarifa_Progpaisano><Oficina_Frontera></Oficina_Frontera><Porcentaje_Tramo_Frontera></Porcentaje_Tramo_Frontera></Record></corridas>";
		String respuesta="";
		List<ItinerarioDto> itinerarios = new ArrayList <ItinerarioDto>();
						
		try{
			
			log.info("Consultando itinerario...");
			//respuesta = etn.consultaItinerario("CAMI","18012013",555,"TAP","ETN");
			
			respuesta = etn.consultaItinerario(datosPeticionItinerario.getOrigen(),datosPeticionItinerario.getFecha(),
					datosPeticionItinerario.getCorrida(),datosPeticionItinerario.getEmpresaSolicita(),datosPeticionItinerario.getEmpresaViaje());
			
//			log.info("RESPUESTA CORRIDAS " +respuesta);
			//log.info("ServiciosEtnImp:obtenerCorridas().consultaCorridas_WSResponse "+respuesta);
			if (respuesta != null){
			
				Document documentoXml = admonXml.crearDocument(respuesta); 
				NodeList nodosTag = documentoXml.getElementsByTagName("Record");
				
				for(int aux=0; aux < nodosTag.getLength(); aux++){
					
					Node propNode = nodosTag.item(aux);
					
					if(propNode.getNodeType() == Node.ELEMENT_NODE){
						
						Element element = (Element) propNode;
						ItinerarioDto itinerario = new ItinerarioDto();
						
						itinerario.setOficina(admonXml.getValue("Oficina",element));
						itinerario.setFechaSalida(admonXml.getValue("Fecha_Salida",element));
						itinerario.setHoraSalida(admonXml.getValue("Hora_Salida",element));
						itinerario.setErrorNumero(valida.getInt(admonXml.getValue("Error_Numero",element)));
						itinerario.setErrorCadena(admonXml.getValue("Error_Cadena",element));						
						itinerarios.add(itinerario);					
					}					
				}
			}else{
				log.info("Error en WS itinerarios respuesta = "+respuesta);
			}
			
		}catch (RemoteException e) {			
			//log.info("ServiciosEtnImp:obtenerCorridas(): "+ e.getStackTrace());
		}
		
		return itinerarios;
	}
	
	@Override
	public CancelacionDto cancelarVenta(CancelacionPeticionDto datosCancelacion){
		
		String respuesta;
		String elementos[];
		String folios[];		
		List<Integer> foliosInt = new ArrayList<Integer>();
		CancelacionDto cancelaDto = new CancelacionDto();
				
		try{
			
			log.info("Cancelar venta ");
			
			respuesta = etn.peticionCancelacionF(datosCancelacion.getOrigen(),datosCancelacion.getCorrida(),datosCancelacion.getFechaSalida(),datosCancelacion.getFolioVenta(),
					datosCancelacion.getEmpresaVende(),datosCancelacion.getEmpresaViaja(),datosCancelacion.getSucExterna(),datosCancelacion.getOficinaExterna(),
					datosCancelacion.getFechaContable(),datosCancelacion.getNumSesion());
			
			//respuesta = "000059056908|0|";
					
			log.info("Respuesta peticionCancelacionF: "+respuesta); 
			
			if(respuesta != null){
				elementos = respuesta.split("\\|");	
				if(elementos.length == 3){ 
					folios = elementos[0].split(",");
					for(int aux =0; aux <folios.length; aux++){
						if(folios[aux].trim().length() > 0){							
							foliosInt.add(Integer.parseInt(folios[aux]));
						}						
					}
					
					cancelaDto.setFolioCancelado(foliosInt);
					cancelaDto.setCodError(Integer.parseInt(elementos[1]));
					cancelaDto.setDetError(elementos[2]);
				}else{ //Formato inv�lido de la cadena
					cancelaDto.setFolioCancelado(foliosInt);
					cancelaDto.setCodError(constantesEtn.ETN_ERROR_CODE);
					cancelaDto.setDetError(constantesEtn.ETN_ERROR_MESS);
				}
				
			}
			
		}catch (RemoteException e) {		
			//log.info("ServiciosEtnImpl:solicitaLiberaReserva() "+e.getStackTrace());
		}		
		return cancelaDto;		
	}
	
	public BoletosUsuario obtenerBoleto(String modulo, String idBusqueda ){
		
		String [] elementos;		
		String [] detTicket;
		String [] operaciones=null;
		String detalle, ticket;	
		
		
		BoletosUsuario boletosUsuario = new BoletosUsuario();
		List<BoletoMaestroDto> listaGeneral = new ArrayList<BoletoMaestroDto>();
		List<Bitacora> listaBitacora = new ArrayList<Bitacora>();
		
		//listaBitacora = new ConsumeServBitacora().consultaBitacoraBoleto(modulo, idBusqueda);
						
		Iterator<?> iter = listaBitacora.iterator();
		
		//"Ori: " + datosPeticionReservacion.getOrigen() + " ,Des: " + datosPeticionReservacion.getDestino() + " ,Tipo Viaje: "+bitacora.getTipoViaje() +" ,Fecha: " + datosPeticionReservacion.getFecha() + " ,Hora:"+datosPeticionReservacion.getHora()+
		//" ,Corrida: " + datosPeticionReservacion.getCorrida() + ",tipoCorrida:" + datosPeticionReservacion.getTipoCorrida() +" ,Asientos: " + asientosStr + " ,Tipo: "+ tipoAsientos + " ,Importe: " + reservacion.getImporteTotal() +
		//" ,Unitarios: "+datosPeticionReservacion.getTarifaAdulto()+"|"+datosPeticionReservacion.getTarifaNino()+"|"+datosPeticionReservacion.getTarifaInsen()+"|"+datosPeticionReservacion.getTarifaEstudiante()+"|"+datosPeticionReservacion.getTarifaMaestro()+" ,Nombre:"+datosPeticionReservacion.getNombres()
		
		
			
		String [] elemAsientos=null;
		String [] elemTipoAsientos=null;
		String [] impUnit= null;
		String origen="",destino="",tipoViaje="",fecha="",diaStr="";
		String hora="",corrida="",tipoCorrida="",asientos="";					
		String tipoAsientos="",importe="",unitarios="",nombre="";
		Date dFecha = null;
		int  dia;
		
		while (iter.hasNext()){
			BoletoMaestroDto boletoMto = new BoletoMaestroDto();
			List<ViajeETNDto> listaBoletos = new ArrayList<ViajeETNDto>();
			
			String preOpers;
			operaciones = null;
			Bitacora bita = (Bitacora) iter.next(); 
			detalle = bita.getDestino();
			elementos = detalle.split(",");
			
			ticket = bita.getTicket();
			detTicket = ticket.split(",");
			
			boletoMto.setNit(bita.getKey());
			
			if(detTicket.length > 0){
				String opers = detTicket[2];
				String [] opersAux = opers.split(":"); 
				
				if(opersAux.length > 0){
					preOpers = opersAux[1];
					operaciones = preOpers.split("|");
				}
			}

			
			if(elementos.length > 0){
				
				origen = elementos[0];
				destino = elementos[1];
				tipoViaje = elementos[2];
				fecha  = elementos[3];
				hora   = elementos[4];
				corrida = elementos[5];
				tipoCorrida = elementos[6];
				asientos = elementos[7];					
				tipoAsientos = elementos[8];
				importe = elementos[9];
				unitarios = elementos[10];
				nombre = elementos[11];
				
				
				
				fecha = diaStr +" "+ fecha;
				elemAsientos = asientos.split("|");
				elemTipoAsientos = tipoAsientos.split("|");
				impUnit = unitarios.split("|");					
			}
			
			if(operaciones != null){
				for(int i=0; i<operaciones.length; i++){
					ViajeETNDto boleto = new ViajeETNDto();
					listaBoletos.add(boleto);
				}
				
				boletoMto.setListaBoletos(listaBoletos);
			}
			
			listaGeneral.add(boletoMto);
		}
		
		boletosUsuario.setListaBoletos(listaGeneral);
		return boletosUsuario;
		
	}
	
	public String validaReservacionExito(ReservacionPeticionDto datosPeticionReservacion, float totalReservacion){
		String resultado="ERROR";
		double total;
		double diferencia;
		try{
			
			total=(datosPeticionReservacion.getTarifaAdulto() * datosPeticionReservacion.getNumAdulto()) +
					(datosPeticionReservacion.getTarifaEstudiante() * datosPeticionReservacion.getNumEstudiante()) +
					(datosPeticionReservacion.getTarifaNino() * datosPeticionReservacion.getNumNino()) +
					(datosPeticionReservacion.getTarifaInsen() * datosPeticionReservacion.getNumInsen()) +
					(datosPeticionReservacion.getTarifaMaestro() * datosPeticionReservacion.getNumMaestro());
			diferencia=total-totalReservacion;
			
			if(Math.abs(diferencia)< 5){
				resultado="EXITO";
			}else{
				log.error("Diferencia: " + total +" - " + totalReservacion);
				log.error("*+*+*+*+*+*+* ERROR RESERVACION ETN : Revisar la reservacion, hay una diferencia de $ "+diferencia+" *+*+*+*+*+*+*");
			}
			
		}catch(Exception e){
			log.error("Error al realizar la validacion");
		}
		return resultado;
	}
	
}

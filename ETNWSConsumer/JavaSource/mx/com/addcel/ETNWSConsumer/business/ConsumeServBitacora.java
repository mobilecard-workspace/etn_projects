package mx.com.addcel.ETNWSConsumer.business;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import mx.com.addcel.ETNWSConsumer.dto.Bitacora;
import mx.com.addcel.ETNWSConsumer.dto.BitacoraViajeDto;
import mx.com.addcel.ETNWSConsumer.dto.ConsultaViajesETN;
import mx.com.addcel.ETNWSConsumer.dto.RespuestaCadena;
import mx.com.addcel.ETNWSConsumer.dto.ViajeMaestroDto;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ConsumeServBitacora {
	private Logger log=Logger.getLogger(ConsumeServBitacora.class);
	
	public Bitacora ingresarBitacora2(ViajeMaestroDto viajeDto) {
        Bitacora bitacoraResp = new Bitacora();
        Gson gSon = new GsonBuilder().disableHtmlEscaping().create();
        String gSonViaje = gSon.toJson(viajeDto);
        String respuesta = null;
        try {
        	respuesta = peticionHttpsUrlParams("https://localhost:8443/AddCelBridge/ClienteBitacoraAgrega", "json="+URLEncoder.encode(gSonViaje, "UTF-8"));
            log.info("respuesta "+respuesta);
            
            RespuestaCadena resp = gSon.fromJson(respuesta, RespuestaCadena.class);
			bitacoraResp.setId(Long.parseLong(resp.getRegistrado()));
			
			log.info("resp: "+resp);
			log.info("bitacoraReso: "+bitacoraResp);
			log.info("respuesta: "+resp.getRegistrado());
			log.info("bitacoraid: "+bitacoraResp.getId());
        } catch (IOException ioE) {
            respuesta = "{\"error\":\"1\",\"numError\":\"A1001\"}";
            log.error("IOException : "+ioE);       
        }
       
        return bitacoraResp;
		
	}
	
	public boolean actualizaBitacora(BitacoraViajeDto viajeBit) {
        Gson gSon = new GsonBuilder().disableHtmlEscaping().create();
        String gSonViaje = gSon.toJson(viajeBit);
        String respuesta = null;
        Boolean resp= false;
        
        try {
        	respuesta = peticionHttpsUrlParams("https://localhost:8443/AddCelBridge/ClienteBitacoraActualiza", "json="+URLEncoder.encode(gSonViaje, "UTF-8"));
            log.info("respuesta: "+respuesta);
            resp = gSon.fromJson(respuesta, Boolean.class);
			log.info("resp "+resp);
        } catch (IOException ioE) {            
        	log.error("IOException : "+ioE); 
        }
        return resp;
	}
	
	public boolean actualizaBitacora3(BitacoraViajeDto viajeBit) {
		String gSonViaje = null;
		String respuesta="";
		boolean resp= false;
        Gson gSon = new GsonBuilder().disableHtmlEscaping().create();
		try {
			gSonViaje = gSon.toJson(viajeBit);
			respuesta = peticionHttpsUrlParams("https://localhost:8443/AddCelBridge/ClienteBitacoraActualiza", "json="+URLEncoder.encode(gSonViaje, "UTF-8"));
							
			resp = gSon.fromJson(respuesta, boolean.class);
			
			log.info("En actualizaBitacora: "+respuesta);
			//log.info("En actualizaBitacora "+resp);
			
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException : "+e); 
			return resp;
		}

		return resp;
	}
	
	/**
	 * Consulta los boletos disponibles para el usuario
	 * @param tipoCOnsulta. Si se consulta por idUsuario o por IdBitacora "U" o "B"
	 * @param parametro. Es el parámetro a consultar
	 * @return
	 */
	
	public ConsultaViajesETN consultaViajes2(long idViaje) {
        ConsultaViajesETN viajes = new ConsultaViajesETN();
        Gson gSon = new GsonBuilder().disableHtmlEscaping().create();
        String gSonViaje = gSon.toJson(idViaje);
        String respuesta = null;
        try {
            respuesta = peticionHttpsUrlParams("https://localhost:8443/AddCelBridge/ClienteBitacoraConsulta", "json="+URLEncoder.encode(gSonViaje, "UTF-8"));
            log.info("respuesta "+respuesta);
            viajes = gSon.fromJson(respuesta, ConsultaViajesETN.class);
			log.info("viajes resp: "+viajes);
        } catch (IOException ioE) {            
        	log.error("IOException : "+ioE); 
            return viajes;
        }
        return viajes;		
	}
	
	public ConsultaViajesETN consultaViajes(long idViaje){
		ConsultaViajesETN viajes = new ConsultaViajesETN();
		Gson gSon = new GsonBuilder().disableHtmlEscaping().create();
		String respuesta = null;
		try {
			respuesta = peticionHttpsUrlParams("https://localhost:8443/AddCelBridge/ClienteBitacoraConsulta", "idViaje="+URLEncoder.encode(idViaje + "", "UTF-8"));
			log.info("respuesta: "+respuesta);
			viajes = gSon.fromJson(respuesta, ConsultaViajesETN.class);
			//log.info("viajes "+viajes);
		} catch (UnsupportedEncodingException e) {
			log.error("IOException : "+e); 
			return viajes;
		}
				
		return viajes;
	}
	
	public String peticionHttpsUrlParams(String urlServicio, String parametros) {
		InputStream is = null;
        OutputStream os = null;
        InputStreamReader isr = null;
        BufferedWriter writer = null;
        BufferedReader br = null;
        HttpURLConnection conn = null;
		URL url = null;
		String inputLine = null;
        StringBuffer sbf = new StringBuffer();
        
		HostnameVerifier hv = new HostnameVerifier() {
			public boolean verify(String urlHostName, SSLSession session) {
				log.debug("Warning: URL Host: " + urlHostName + " vs. " + session.getPeerHost());
				return true;
			}
		};	        
	    
        try {
        	trustAllHttpsCertificates();
    	    HttpsURLConnection.setDefaultHostnameVerifier(hv);
            url = new URL(urlServicio);
            conn = (HttpURLConnection) url.openConnection();
            if (conn != null) {
                conn.setConnectTimeout(10000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                if (parametros != null) {
                    conn.setDoOutput(true);
                    os = conn.getOutputStream();
                    writer = new BufferedWriter(new OutputStreamWriter(os));
                    writer.write(parametros);
                    writer.flush();
                    writer.close();
                    os.flush();
                    os.close();
                    conn.connect();
                }
                is = conn.getInputStream();
                isr = new InputStreamReader(is);
                br = new BufferedReader(isr);
                while ((inputLine = br.readLine()) != null) {
                    sbf.append(inputLine);
                }
//                log.debug("Respuesta notificacion: {}", sbf);
            }
        } catch (GeneralSecurityException e) {
        	log.info("Error Al generar certificados...");
        	e.printStackTrace();
        }catch (MalformedURLException e) {
        	log.info("error al conectar con la URL: "+urlServicio);
        	e.printStackTrace();
        }catch (ProtocolException e) {
        	log.info("Error al enviar datos.. ");
			e.printStackTrace();
		} catch (IOException e) {
			log.info("Error al algo... ");
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
            if (conn != null) {
                try {
                    conn.disconnect();
                } catch (Exception e) {
                 log.error("Exception : ", e);
                }
            }
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                 log.error("Exception : ", e);
                }
            }

            if (isr != null) {
                try {
                    isr.close();
                } catch (Exception e) {
                 log.error("Exception : ", e);
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                 log.error("Exception : ", e);
                }
            }
        }
//        System.out.println(response.toString());
        return sbf.toString(); 
	}
	
	public static class miTM implements javax.net.ssl.TrustManager,
			javax.net.ssl.X509TrustManager {
		public java.security.cert.X509Certificate[] getAcceptedIssuers() {
			return null;
		}

		public boolean isServerTrusted(
				java.security.cert.X509Certificate[] certs) {
			return true;
		}

		public boolean isClientTrusted(
				java.security.cert.X509Certificate[] certs) {
			return true;
		}

		public void checkServerTrusted(
				java.security.cert.X509Certificate[] certs, String authType)
				throws java.security.cert.CertificateException {
			return;
		}

		public void checkClientTrusted(
				java.security.cert.X509Certificate[] certs, String authType)
				throws java.security.cert.CertificateException {
			return;
		}
	}

	private static void trustAllHttpsCertificates() throws Exception {
		// Create a trust manager that does not validate certificate chains:
		javax.net.ssl.TrustManager[] trustAllCerts = new javax.net.ssl.TrustManager[1];
		javax.net.ssl.TrustManager tm = new miTM();
		trustAllCerts[0] = tm;
		javax.net.ssl.SSLContext sc = javax.net.ssl.SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, null);
		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(
		sc.getSocketFactory());

	}


}

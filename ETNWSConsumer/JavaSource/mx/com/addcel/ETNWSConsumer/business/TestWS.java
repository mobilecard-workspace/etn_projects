/*
 * ---------------------------------------------------------------
 * Copyright (c) 2012 Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Prolongación paseo de la reforma 61 oficina 6A2 sexto piso, colonia paseo de las lomas
 * C.P. 01330, Delegación Alvaro Obregón, México D.F.
 * Algunos Derechos Reservados
 *
 * Este software contiene informaci&oacute;n totalmente confidencial propiedad de
 * Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Queda totalmente prohibido su uso o divulgacion 
 * en forma parcial o total y solamente podr&aacute; ser utilizada de acuerdo a los 
 * t&eacute;rminos y estatutos que determine la propia empresa.
 * --------------------------------------------------------------
 */
package mx.com.addcel.ETNWSConsumer.business;

import java.rmi.RemoteException;


import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


/**
 * @author Lic. Elena L&ocute;pez L&ocute;pez
 *
 */
public class TestWS {
	
	//ServiciosEtnImpl etn = new ServiciosEtnImpl();
	//private static Logger log=Logger.getLogger(TestWS.class);
	public static void main(String [] args){
		
		String nombres   = "SDFRTGHTYUJKLOKOPLSMDGRTYSDFTENFGRT,PEDRO PEREZ,SDFRTGHTYUJKLOKOPLSMDGRTYSDFTENFGRT*IRUTYM";
	   
	    
	    /*if (nombres != null) {
	    	String [] listaNombres = nombres.split("\\,");
	    	log.info("lista nombres en servlet "+listaNombres.length);
		    nombres = "";		    
		    for (String nombre: listaNombres) {
		    	if(nombre.length() > 35){
		    		nombres += nombre.substring(0, 35);
		    	}else if(nombre.length() == 35){
		    		nombres += nombre;
		    	}else if(nombre.length() < 35){
		    		nombres += StringUtils.rightPad(nombre, 35, " ");
		    	}
		    }
	    } 
		*/
		/*String[] operaciones = {"1","2","3"};
		
		String[] real = new String[operaciones.length-1];
		
		log.info("operaciones "+operaciones.length);
		log.info("real "+real.length);
		
		for(int i=0; i<operaciones.length-1; i++){
			real[i] = operaciones[i];			
		}
		
		for(int i=0; i<real.length; i++){
			log.info("valor real "+real[i]);
		}*/
		
		/*DateTime fechaJt = new DateTime();	
		
		DateTimeFormatter hfmt = DateTimeFormat.forPattern("HHmmss");
		DateTimeFormatter diafmt = DateTimeFormat.forPattern("dd");
		DateTimeFormatter mesfmt = DateTimeFormat.forPattern("MM");
		DateTimeFormatter aniofmt = DateTimeFormat.forPattern("yyyy");
		
		String horaInicio, horaFin;		
		String horaSistema = fechaJt.toString(hfmt);
		String diaSistema = fechaJt.toString(diafmt);
		String mesSistema = fechaJt.toString(mesfmt);
		String anioSistema = fechaJt.toString(aniofmt);
		
		String fecha = "27062013";
		
		String dia = fecha.substring(0,2);
		String mes = fecha.substring(2,4);
		String anio = fecha.substring(4,8);
		
		log.info("hora "+horaSistema);
		log.info("dia "+diaSistema); 
		log.info("mes "+mesSistema);
		log.info("dia c "+dia);
		log.info("mes c "+mes);
		log.info("anio c "+anio);
		
		int diaSis = Integer.parseInt(diaSistema);
		int mesSis = Integer.parseInt(mesSistema);
		int anioSist = Integer.parseInt(anioSistema);
		
		int diaConsulta = Integer.parseInt(dia);
		int mesConsulta = Integer.parseInt(mes);
		int anioConsulta = Integer.parseInt(anio);
		
		if( (anioConsulta > anioSist) || (mesConsulta > mesSis) || (diaConsulta > diaSis)){
			horaInicio = "000000";
		}else {			
			horaInicio = horaSistema;
		}
		
		horaFin = "240000";
		log.info("hora inicio "+horaInicio);*/
		
		String importe="1000.00";
		
		int imp = Integer.parseInt(importe);
		
		System.out.println("importe "+imp);
	   
	}
	
	
	/*public static void main(String [] args) throws RemoteException{
		
		ConsumeServBitacora serv = new ConsumeServBitacora();
		BitacoraPeticionDto bitacora = new BitacoraPeticionDto();
		ReservacionPeticionDto datosReserva = new ReservacionPeticionDto();
		ServiciosEtnImpl etn = new ServiciosEtnImpl();
		//CitecPortType  etn   = new CitecPortTypeProxy();
		
		String asientoStr = "010|011";
	    bitacora.setIdUsuario("pruebaGDF");
	    bitacora.setImei("123654789");
	    bitacora.setModelo("patito");
	    bitacora.setSoftware("Hola");	    
	    bitacora.setTelefono("5540262389");
	    bitacora.setTarjeta("25698532"); //******** verificar la tarjeta
	    bitacora.setTipoProducto("2");
	    bitacora.setProducto("12");
	    bitacora.setConcepto("hola");
	    bitacora.setTipoTelefono("123");
	    bitacora.setTipoViaje("S");
	    bitacora.setWkey("1236547");
	    bitacora.setProveedor("12");
	    bitacora.setAutorizaBanco("0");
	    bitacora.setDestinoEtn("hola");
		
	    log.info("test");
		
		UnsignedInt uIcorrida = new UnsignedInt();
    	Long corridaLong = 7392L;
    	uIcorrida.setValue(corridaLong);
		
    	//String respuesta = etn.destinos("GDLJ","TAP","ETN");	
		//String respuesta = etn.consultaDiagrama("ETN",uIcorrida,"11062013","ETN");
    	
    	CorridaPeticionDto datosPeticionCorrida = new CorridaPeticionDto();
    	
    	etn.obtenerCorridas( datosPeticionCorrida);
    	
    	
		
		//log.info("respuesta "+respuesta);
		
		//etn.solicitarReservacion(datosReserva, bitacora, asientoStr);
		
		//ConsultaViajesETN consulta = serv.consultaViajes(35);
		
		//log.info("tamaño de la lista de viajes "+consulta.getListaViajes().size());
		
		//Bitacora bita = serv.ingresarBitacora2(new ViajeMaestroDto());
		
		//int autorizaBanco = 12365;
		//VentaBoletoPeticionDto peticion = new VentaBoletoPeticionDto();
		
    	//peticion.setCorrida(new UnsignedInt(request.getParameter("corrida").toString()));
    	/*peticion.setSuccess("true");
    	peticion.setIdViaje(57);
    	peticion.setMensaje("VIAJE AUTORIZADO");
    	peticion.setEbFormasPagoTemp("TB");
    	peticion.setEmpresaVende("TAP");
    	peticion.setEmpresaViaje("ETN");
    	peticion.setFecha("07062013");
    	peticion.setFechaContable("07062013");
    	//peticion.setFolioReservacion(Double.parseDouble(request.getParameter("folioReservacion").toString()));
    	peticion.setFormasPagoExternas("");
    	peticion.setNumSesion(new UnsignedLong(0));
    	peticion.setOficinaExterna("");
    	//peticion.setOrigen(request.getParameter("origen").toString());
    	peticion.setSucursalExterna(new UnsignedLong(0));
    	peticion.setTipoCliente("I");
    	peticion.setTipoTerminal("I"); */
    	
    	//VentaBoletoDto boleto = etn.solicitarVentaBoleto(peticion,autorizaBanco);
    	
    	/*CorridaPeticionDto datos = new CorridaPeticionDto();
    	
    	double x = 2; // Estos datos comentados son para pruebas
		double y = 0;		
    	UnsignedByte u = new UnsignedByte();
    	Long tipoLong = 0L; 
    	UnsignedByte uBtipoServ = null;
    	CitecPortType  et   = new CitecPortTypeProxy();
    
    	String respuesta =et.consultaCorridas("CAMI","GDLJ","11062013","090000",new UnsignedByte(),"ETN",x,y,y,y,y,u,y,"144500");
		
    	log.info("respuesta "+respuesta);*/
    	
    	/*log.info("autorizacion:  "+boleto.getAutBanco());
    	log.info("codigo: "+boleto.getCodigoError());
    	log.info("idtrans: "+boleto.getIdTransaccion());
    	log.info("operaciones: "+boleto.getOperaciones());*/
    	
    	/*datosPeticionReservacion.setNombres("ELOPEZ|PEDRO");
		datosPeticionReservacion.setIdViaje(123);
		datosPeticionReservacion.setCorrida("7392");
		datosPeticionReservacion.setIdOrigen("CAMI");
		datosPeticionReservacion.setIdDestino("GDLJ");
		datosPeticionReservacion.setOrigen("CAMINOS");
		datosPeticionReservacion.setDestino("GUADALAJARA");
		datosPeticionReservacion.setFecha("06/06/2013");
		datosPeticionReservacion.setHora("090000");												
		datosPeticionReservacion.setTipoCorrida("LU");
		datosPeticionReservacion.setAsientos("3233");
		datosPeticionReservacion.setNumAdulto("2");
		datosPeticionReservacion.setNumNino("0");
		datosPeticionReservacion.setNumInsen("0");
		datosPeticionReservacion.setNumEstudiante("0");
		datosPeticionReservacion.setNumMaestro("0");
		
		
	}*/
	
	public void testReservacion(){
		
		
	}

}

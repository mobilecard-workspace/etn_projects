/**
 * 
 */
package mx.com.addcel.ETNWSConsumer.business;

import java.util.List;

import mx.com.addcel.ETNWSConsumer.dto.BitacoraPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.BoletosUsuario;
import mx.com.addcel.ETNWSConsumer.dto.CancelacionDto;
import mx.com.addcel.ETNWSConsumer.dto.CancelacionPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.CorridaDto;
import mx.com.addcel.ETNWSConsumer.dto.CorridaPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.DestinoDto;
import mx.com.addcel.ETNWSConsumer.dto.DestinoPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.DiagramaDto;
import mx.com.addcel.ETNWSConsumer.dto.DiagramaPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.ItinerarioDto;
import mx.com.addcel.ETNWSConsumer.dto.ItinerarioPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.LibReservaPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.OcupacionDto;
import mx.com.addcel.ETNWSConsumer.dto.OcupacionPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.OficinaDto;
import mx.com.addcel.ETNWSConsumer.dto.ReservacionDto;
import mx.com.addcel.ETNWSConsumer.dto.ReservacionPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.TarifaPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.TarifasDto;
import mx.com.addcel.ETNWSConsumer.dto.TipoServicioDto;
import mx.com.addcel.ETNWSConsumer.dto.TipoServicioPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.VentaBoletoDto;
import mx.com.addcel.ETNWSConsumer.dto.VentaBoletoPeticionDto;

/**
 * @author Ing. Efr&eacute;n Reyes Torres
 * Interfaz que define la firma de los m&eacute;todos de consumo de los servicios de ETN
 */
public interface ServiciosEtn {

	/**
	 * M&eacute;todo que obtiene las oficinas de venta de boletos de ETN
	 * @return La lista de oficinas
	 */
	List<OficinaDto> obtenerOficinas();
	
	/**
	 * 
	 * @param datosPeticionDestino
	 * @return
	 */
	List<DestinoDto> obtenerDestinos(DestinoPeticionDto datosPeticionDestino);
	
	/**
	 * 
	 * @param datosPeticionCorrida
	 * @return
	 */
	List<CorridaDto> obtenerCorridas(CorridaPeticionDto datosPeticionCorrida);
	

	/**
	 * 
	 * @param datosPeticionServicio
	 * @return
	 */	
	List<TipoServicioDto> obtenerTipoServicios(TipoServicioPeticionDto datosPeticionServicio);  
	
	/**
	 * 
	 * @param datosPeticionTarifa
	 * @return
	 */
	TarifasDto obtenerTarifas(TarifaPeticionDto datosPeticionTarifa);
	
	/**
	 * 
	 * @param datosPeticionOcupacion
	 * @return
	 */
	OcupacionDto consultarOcupacion(OcupacionPeticionDto datosPeticionOcupacion);
	
	/**
	 * 
	 * @param datosPeticionDiagrama
	 * @return
	 */
	DiagramaDto consultarDiagramaAutobus(DiagramaPeticionDto datosPeticionDiagrama);
	
	/**
	 * 
	 * @param datosPeticionReservacion
	 * @param BitacoraPeticionDto
	 * @return
	 */
	ReservacionDto solicitarReservacion(ReservacionPeticionDto datosPeticionReservacion, 
			BitacoraPeticionDto bitacora, String asientos, String nombres);
	
	/**
	 * 
	 * @param datosPeticionVenta
	 * @return
	 */
	VentaBoletoDto solicitarVentaBoleto(VentaBoletoPeticionDto datosPeticionVenta, String autorizaBanco);
	
	/**
	 * 
	 * @param libResPetDto
	 * @return
	 */	
	Boolean  solicitaLiberaReserva(LibReservaPeticionDto libResPetDto);
	
	/**
	 * 
	 * @param datosPeticionItinerario
	 * @return
	 */	
	List<ItinerarioDto> obtenerItinerario(ItinerarioPeticionDto datosPeticionItinerario);
	
	/**
	 * 
	 * @param datosCancelacion
	 * @return
	 */
	CancelacionDto cancelarVenta(CancelacionPeticionDto datosCancelacion);
	
	/**
	 * 
	 * @param modulo. Módulo a consultar "V" en venta o "C" si es consulta de boletos
	 * @param idBusqueda. Patrón de búsqueda, si es número de autorización bancaria o si es id usuario
	 * @return
	 */
	BoletosUsuario obtenerBoleto(String modulo, String idBusqueda );
	
}

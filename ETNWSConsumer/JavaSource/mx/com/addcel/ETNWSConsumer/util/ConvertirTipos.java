/*
 * ---------------------------------------------------------------
 * Copyright (c) 2012 Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Prolongaci�n paseo de la reforma 61 oficina 6A2 sexto piso, colonia paseo de las lomas
 * C.P. 01330, Delegaci�n Alvaro Obreg�n, M�xico D.F.
 * Algunos Derechos Reservados
 *
 * Este software contiene informaci&oacute;n totalmente confidencial propiedad de
 * Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Queda totalmente prohibido su uso o divulgacion 
 * en forma parcial o total y solamente podr&aacute; ser utilizada de acuerdo a los 
 * t&eacute;rminos y estatutos que determine la propia empresa.
 * --------------------------------------------------------------
 */
package mx.com.addcel.ETNWSConsumer.util;

/**
 * @author Lic. Elena L&ocute;pez L&ocute;pez
 *
 */
public class ConvertirTipos {
	
	public int getInt(String parametro){		
		int entero;		
		try{
			entero =  Integer.parseInt(parametro);
		}catch(NumberFormatException ex){		
			entero = 0;
		}		
		return entero;
	}
	public Long getLong(String parametro){		
		Long lng;		
		try{
			lng =  Long.parseLong(parametro);
		}catch(NumberFormatException ex){		
			lng = (long) 0.0;
		}		
		return lng;
	}
	
	public float getFloat(String parametro){
		float decimal;
		try{
			decimal = Float.parseFloat(parametro); 
		}catch(NumberFormatException ex){
			decimal = 0;
		}
		return decimal;
	}
	
	public Double getDouble(String parametro){		
		Double db;		
		try{
			db = Double.parseDouble(parametro);			 
		}catch(NumberFormatException ex){
			db = 0.0;
		}
		return db;		
	}
	
	public Double getDoubled(String parametro){		
		double db;		
		try{
			db = Double.parseDouble(parametro);			 
		}catch(NumberFormatException ex){
			db = 0.0;
		}
		return db;		
	}
	
	public String getString(String parametro){
		String str;
		if(parametro == null){
			str = "0";
		}else{
			str = parametro;
		}
		return str;
	}
}

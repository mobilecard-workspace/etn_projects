package mx.com.addcel.ETNWSConsumer.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import mx.com.addcel.ETNWSConsumer.dao.TransaccionesDAO;

public class ObtenDiaFecha {
	private Logger log=Logger.getLogger(ObtenDiaFecha.class);
	GregorianCalendar calendar = new GregorianCalendar();
	SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy");
		
	public String getDiaFecha(String fecha){
		
		Date dFecha   = null;
		String fechaFormato = getDDMMAAAAtoDD_MM_AAAA(fecha); 
		String diaStr = "";
		int dia = 0;
		
		try {
			
			dFecha = formatoDelTexto.parse(fechaFormato);
			calendar.setTime(dFecha);
			dia =  calendar.get(Calendar.DAY_OF_WEEK);
			
			switch(dia){
			case 1:
				diaStr = "Domingo";
				break;
			case 2:
				diaStr = "Lunes";
				break;
			case 3:
				diaStr = "Martes";
				break;
			case 4:
				diaStr = "Miercoles";
				break;
			case 5:
				diaStr = "Jueves";
				break;
			case 6:
				diaStr = "Viernes";
				break;
			case 7:
				diaStr = "Sabado";
				break;
			}
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			log.error("ParseException : ",e);
			
		}
		
		return diaStr;
	}
	
	public String getDDMMAAAAtoDD_MM_AAAA(String date){
	       

        String day= date.substring(0, 2);
        String month = date.substring(2, 4);
        String year= date.substring(4, 8);
       
        return day + "/" + month + "/" + year;
    }
	
}

/**
 * 
 */
package mx.com.addcel.ETNWSConsumer.util;

import java.io.StringReader;
import org.apache.log4j.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import mx.com.addcel.ETNWSConsumer.dao.TransaccionesDAO;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * @author Ing. Efr&eacute;n Reyes Torres
 * Clase que provee de m&eacute;todos
 * para la manipulaci&oacute;n de documentos XML
 */
public class ManejadorXml {
	private Logger log=Logger.getLogger(ManejadorXml.class);
	/**
	 * M&eacute;todo que crea un documento xml a partir de una cadena de texto
	 * con formato de xml
	 * 
	 * @param cadenaXML
	 *            cadena del xml
	 * @return El documento xml
	 */
	public Document crearDocument(String cadenaXML) {
		Document document = null;
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			document = builder.parse(new InputSource(
					new StringReader(cadenaXML)));
			document.getDocumentElement().normalize();
		} catch (Exception ex) {
			log.fatal("Exception : ", ex);
		}
		return document;
	}
	
	/**
	 * M&eacute;todo para obtener el valor de un nodo buscandolo por su tag
	 * @param tag El tag a buscar
	 * @param element El elemento que contiene el nodo
	 * @return el valor del nodo
	 */
	public String getValue(String tag, Element element) {
		String valor;
		try {
			NodeList nodes = element.getElementsByTagName(tag).item(0)
					.getChildNodes();
			Node node = (Node) nodes.item(0);
			valor = node.getNodeValue();
		} catch (NullPointerException npe) {
			valor = "";
		}
		
		return valor;
	}
}

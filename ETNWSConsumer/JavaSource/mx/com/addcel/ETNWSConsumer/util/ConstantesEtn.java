/*
 * ---------------------------------------------------------------
 * Copyright (c) 2012 Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Prolongaci�n paseo de la reforma 61 oficina 6A2 sexto piso, colonia paseo de las lomas
 * C.P. 01330, Delegaci�n Alvaro Obreg�n, M�xico D.F.
 * Algunos Derechos Reservados
 *
 * Este software contiene informaci&oacute;n totalmente confidencial propiedad de
 * Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Queda totalmente prohibido su uso o divulgacion 
 * en forma parcial o total y solamente podr&aacute; ser utilizada de acuerdo a los 
 * t&eacute;rminos y estatutos que determine la propia empresa.
 * --------------------------------------------------------------
 */
package mx.com.addcel.ETNWSConsumer.util;

/**
 * @author Lic. Elena L&ocute;pez L&ocute;pez
 *
 */
public class ConstantesEtn {
	
	public final static String AMBIENTE_QA="QA";
    public final static String AMBIENTE_PROD="PROD";
    public final static String AMBIENTE_ACTUAL="PROD";
    public static Integer NUMERO_AUTORIZACION=((int)(Math.random()*100000 - 9999) + 9999);
	
	// Errores en la cadena de salida de una transacci�n
	
	public final int     ETN_ERROR_CODE = -1;
	public final String  ETN_ERROR_MESS = "Formato de cadena de salida incorrecta.";
	
	// Indica una corrida inexistente de autobuses, elimina la salida .|.|Corrida No Existe
	
	public final int    ETN_ROOT_CODE = 0;
	public final String ETN_ROOT_MESS = "Corrida de autobuses inexistente";
	
	// Indican que una salida es correcta cuando la cadena no tiene codigo ni mensaje de exito
	
	public final int     ETN_OK_CODE    = 1111;
	public final String  ETN_OK_MESS = "Cadena correcta.";
	
	// Indican si ocurri� un error al momento de ingresar la transacci�n en la bit�cora
	
	public final int     ETN_ERROR_CODE_TRANS = -2;
	public final String  ETN_ERROR_MESS_TRANS = "Sin comunicaci&ocute;n con AddcelBridge, intente m&acute;s tarde.";
	
	//Indica si ocurrió un error en la transacción
	public final int     ETN_DENAY_ERROR_CODE = 2;
	
	//Indica si la transacción fue exitosa
	public final int     ETN_OK_STATUS = 1;
	public final int     ETN_INIT_STATUS = 0;
	
	// Indican si ocurri� un error al momento de ingresar la transacci�n en la bit�cora
	
	public final int     ETN_ERROR_DATA = -3;
	public final String  ETN_ERROR_MESS_DATA = "No se obtuvo informaci&ocute;n del viaje.";
	
	//clave de la empresa que solicita el viaje
	public final String  CVE_EMPRESA = "MOBC";
	public final String  CVE_EMPRESA_VIAJA = "TLU";
}

/*
 * ---------------------------------------------------------------
 * Copyright (c) 2012 Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Prolongación paseo de la reforma 61 oficina 6A2 sexto piso, colonia paseo de las lomas
 * C.P. 01330, Delegación Alvaro Obregón, México D.F.
 * Algunos Derechos Reservados
 *
 * Este software contiene informaci&oacute;n totalmente confidencial propiedad de
 * Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Queda totalmente prohibido su uso o divulgacion 
 * en forma parcial o total y solamente podr&aacute; ser utilizada de acuerdo a los 
 * t&eacute;rminos y estatutos que determine la propia empresa.
 * --------------------------------------------------------------
 */
package mx.com.addcel.ETNWSConsumer.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.stream.JsonReader;

/**
 * @author Lic. Elena L&ocute;pez L&ocute;pez Clase que recibe un InputStream
 *         que se espera sea un String Json para obtener los elementos que
 *         contenga y formar objetos o arreglos
 */
public class LectorJson {

	public List<String> readJsonStream(InputStream in) throws IOException {
		JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
		List<String> elementosJson = new ArrayList<String>();
		try {
			reader.beginArray();
			while (reader.hasNext()) {
				elementosJson.add(readMessage(reader));
			}
			reader.endArray();
		} finally {
			reader.close();
		}
		return elementosJson;
	}

	public String readMessage(JsonReader reader) throws IOException {
		String text = null;
		reader.beginObject();

		while (reader.hasNext()) {
			String name = reader.nextName();
			if (name.equals("id")) {
				text = reader.nextString();
			} else {
				reader.skipValue();
			}
		}
		reader.endObject();
		return text;
	}
}

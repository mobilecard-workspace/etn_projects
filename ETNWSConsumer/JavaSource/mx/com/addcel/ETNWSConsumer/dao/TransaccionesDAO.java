package mx.com.addcel.ETNWSConsumer.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import mx.com.addcel.ETNWSConsumer.dto.Bitacora;
import mx.com.addcel.ETNWSConsumer.dto.BitacoraViajeDto;
import mx.com.addcel.ETNWSConsumer.dto.BoletoETNDto;
import mx.com.addcel.ETNWSConsumer.dto.ConsultaViajesETN;
import mx.com.addcel.ETNWSConsumer.dto.ViajeETNDto;



public class TransaccionesDAO extends DAO{
	private Logger log=Logger.getLogger(TransaccionesDAO.class);
	public ConsultaViajesETN consultaViajes2(long idViaje){
		//Este método se creo para una prueba  no es productivo
		
		ViajeETNDto viaje = null;
	    List<ViajeETNDto> listaViajes = new ArrayList<ViajeETNDto>();
	    ConsultaViajesETN consulta = new ConsultaViajesETN();
	    
        try {
              
              PreparedStatement statement;
              ResultSet resultSet = null;
              
              String query =           	
            		  "SELECT t_bitacora.id_bitacora, ViajesETN.idReservacion, ViajesETN.idOrigen, ViajesETN.fechaETN, ViajesETN.corridaETN, "+
            		  " BoletosETN.idBoleto, BoletosETN.asiento "+
            		  " FROM  t_bitacora, ViajesETN, BoletosETN "+
            		  " WHERE t_bitacora.pase   = ViajesETN.idviaje "+
            		  " AND   t_bitacora.bit_ticket = ViajesETN.idReservacion "+
            		  " AND   ViajesETN.idViaje = BoletosETN.idViaje "+
            		  " AND   ViajesETN.idReservacion = BoletosETN.idReservacion "+
            		  " AND   ViajesETN.idViaje = "+idViaje+
            		  " ORDER BY ViajesETN.idReservacion,BoletosETN.idBoleto ";
            		  
              statement = (PreparedStatement) connect().prepareStatement(query);              
              resultSet = statement.executeQuery();
              
              log.info("QRY BITACORA ETN DATA "+query);
              long idRes = 0;
              long idBoleto = 0;
              
              while(resultSet.next()){
                   int aux=1;
                   log.info("Despues de buscar en bitacora ETN DATA registro: "+aux);
                  
                   long reservacion = resultSet.getLong(2);
                   
                   if(idRes != reservacion){
                	   viaje = new ViajeETNDto();
                	   BoletoETNDto boleto = new BoletoETNDto();
                	   
                	   viaje.setIdViaje(idViaje);
                       viaje.setIdBitacora(resultSet.getInt(1));
                       viaje.setIdReservacion(resultSet.getInt(2));
                       viaje.setIdOrigen(resultSet.getString(3));
                       viaje.setFechaETN(resultSet.getString(4));
                       viaje.setCorridaETN(resultSet.getString(5));
                       
                       long bol = resultSet.getInt(6);
                       
                       if(idBoleto != bol){
                    	   
	                       boleto.setIdViaje(idViaje);
	                       boleto.setIdReservacion(viaje.getIdReservacion());
	                       boleto.setIdBoleto(resultSet.getInt(6));
	                       boleto.setAsiento(resultSet.getString(7));
	                       
	                       log.info("asiento "+boleto.getAsiento());
	                       log.info("idBoleto "+boleto.getIdBoleto());
                       }
                       
                       idRes = reservacion;
                   }
                  
                   List<BoletoETNDto> listaBoletos = new ArrayList<BoletoETNDto>();
                  //listaBoletos.add(boleto);
                  
                  aux++;
              }
              //viaje.setListaBoletos(listaBoletos);
              listaViajes.add(viaje);
              log.info("Tamano de la lista de viajes "+listaViajes.size());
              
        } catch (SQLException ex) {        	 
	         log.info("ERROR UPD BITACORA: " + ex.toString());   
	         consulta.setListaViajes(listaViajes);
	         return consulta;
        } catch (Exception ex) {        	 
	         log.info("ERROR UPD BITACORA: " + ex.toString());   
	         consulta.setListaViajes(listaViajes);
	         return consulta;
        } finally {
	        try {
				disconect();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				log.error("SQLException : ",e);
			}            
        } 
        consulta.setListaViajes(listaViajes);
        return consulta;
	}
	
	public ConsultaViajesETN consultaViajes(long idViaje) { 
	        
	    ViajeETNDto viaje = null;
	    List<ViajeETNDto> listaViajes = new ArrayList<ViajeETNDto>();
	    ConsultaViajesETN consulta = new ConsultaViajesETN();
	    
	    
        try {
              
              PreparedStatement statement;
              ResultSet resultSet = null;
              
              String query = "SELECT t_bitacora.id_bitacora, ViajesETN.idReservacion, ViajesETN.idOrigen, ViajesETN.fechaETN, ViajesETN.corridaETN "+
                             "FROM  t_bitacora, ViajesETN  " +
                             "WHERE t_bitacora.pase   = ViajesETN.idviaje "+
                             "AND   t_bitacora.bit_ticket = ViajesETN.idReservacion "+
                             "AND  ViajesETN.idViaje = "+idViaje+ " ORDER BY ViajesETN.idReservacion ";
              
              statement = (PreparedStatement) connect().prepareStatement(query);              
              resultSet = statement.executeQuery();
              
              log.info("QRY BITACORA ETN DATA "+query);
              
              while(resultSet.next()){
                   int aux=1;
                   log.info("Despues de buscar en bitacora ETN DATA "+aux);
                   
                  viaje = new ViajeETNDto();
                  List<BoletoETNDto> listaBoletos = new ArrayList<BoletoETNDto>();
                  
                  viaje.setIdViaje(idViaje);
                  viaje.setIdBitacora(resultSet.getInt(1));
                  viaje.setIdReservacion(resultSet.getInt(2));
                  viaje.setIdOrigen(resultSet.getString(3));
                  viaje.setFechaETN(resultSet.getString(4));
                  viaje.setCorridaETN(resultSet.getString(5));
                  
                  String query2 =   "SELECT BoletosETN.idBoleto, BoletosETN.asiento "+
                                    " FROM  ViajesETN, BoletosETN "+
                                    " WHERE ViajesETN.idViaje = BoletosETN.idViaje  "+
                                    " AND ViajesETN.idReservacion = BoletosETN.idReservacion "+
                                    " AND ViajesETN.idViaje = "+idViaje+
                                    " AND ViajesETN.idReservacion = "+viaje.getIdReservacion()+
                                    " ORDER BY BoletosETN.idBoleto  ";
                  
                  PreparedStatement stmt;
                  ResultSet rst = null;
                  
                  stmt = (PreparedStatement) connect().prepareStatement(query2);              
                  rst  = stmt.executeQuery();
                  
                  while(rst.next()){
                      int aux2=1;
                      
                      log.info("Buscando boletos en bitacora ETN DATA "+aux2);
                      log.info("BOLETOS ETN DATA "+query2);
                      
                      BoletoETNDto boleto = new BoletoETNDto();
                      
                      boleto.setIdViaje(idViaje);
                      boleto.setIdReservacion(viaje.getIdReservacion());
                      boleto.setIdBoleto(rst.getInt(1));
                      boleto.setAsiento(rst.getString(2));
                      
                      listaBoletos.add(boleto);
                      aux2++;
                  }
                  
                  aux++;
                  viaje.setListaBoletos(listaBoletos);
                  listaViajes.add(viaje);
              }
              
              log.info("Tamano de la lista de viajes "+listaViajes.size());
              
        } catch (SQLException ex) {        	 
	         log.info("ERROR UPD BITACORA: " + ex.toString());   
	         consulta.setListaViajes(listaViajes);
	         return consulta;
        } catch (Exception ex) {        	 
	         log.info("ERROR UPD BITACORA: " + ex.toString());   
	         consulta.setListaViajes(listaViajes);
	         return consulta;
        } finally {
	        try {
				disconect();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				log.error("SQLException : ",e);
			}            
        }  
        
        consulta.setListaViajes(listaViajes);
        return consulta;
    }
	 
	public boolean actualizaBitacora(BitacoraViajeDto viaje){ 
	        
        int status;
        boolean rows = true;
        String qry="";
	        
         try {
	          log.info("UPD BITACORA");
	          long date =  new java.util.Date().getTime();
	           
	          ResultSet resultSet = null;
	          
	          Bitacora bitacora = viaje.getBitacora();
	          
	          if(bitacora.getCodigo_error() == 0){
                  
               ViajeETNDto viajeEtn = viaje.getViaje();
               status = 1; //Estatus de la bitacora que indica el termino de la transaccion exitosa
               qry = " UPDATE t_bitacora "+
                      " SET bit_status = ?, bit_codigo_error = ?, destino = ?, bit_no_autorizacion = ? "+
                      " WHERE id_bitacora = ? "+
                      " AND pase = ? "+
                      " AND TRIM(bit_ticket) = ? ";
                
               log.info("UPD BITACORA query1 "+qry);
               log.info("UPD BITACORA status "+status);
               log.info("UPD BITACORA codigo "+bitacora.getCodigo_error());
               log.info("UPD BITACORA destino "+bitacora.getDestino());
               log.info("UPD BITACORA autoriza "+bitacora.getNo_autorizacion());
               log.info("UPD BITACORA id "+bitacora.getId());
               log.info("UPD BITACORA idViaje "+viajeEtn.getIdViaje());
               log.info("UPD BITACORA idRser "+viajeEtn.getIdReservacion());
                    
               PreparedStatement statement = (PreparedStatement) connect().prepareStatement (qry);
                      
                statement.setLong(1, status);
                statement.setLong(2, bitacora.getCodigo_error());
                statement.setString(3, bitacora.getDestino());                            
                statement.setString(4, bitacora.getNo_autorizacion());
                statement.setLong(5, bitacora.getId());
                statement.setLong(6, viajeEtn.getIdViaje());
                statement.setString(7, viajeEtn.getIdReservacion()+"");

                statement.execute(); 
                    
                qry = "UPDATE ViajesETN SET nit = ? WHERE idViaje = ? AND idReservacion = ? ";
                    
                log.info("UPD BITACORA nit "+viajeEtn.getNit());
                
                PreparedStatement st = (PreparedStatement) connect().prepareStatement (qry);
                
                st.setLong(1, viajeEtn.getNit());
                st.setLong(2, viajeEtn.getIdViaje());
                st.setLong(3, viajeEtn.getIdReservacion());
                
                st.execute();

                String operacionStr = bitacora.getConcepto();

                String [] operaciones = operacionStr.split("\\|");

                List<BoletoETNDto> listaBoletos = viajeEtn.getListaBoletos();

                if(operaciones != null){ 
                	log.info("Buscando las operaciones");
                	log.info("Tamanio de los boletos "+listaBoletos.size()+" tamanio de las operaciones "+operaciones.length);
                    
                	if(listaBoletos.size() == operaciones.length){                  
                      
                		log.info("si los boletos son igual a las operaciones");
                        
	                    Iterator<?> iter = listaBoletos.iterator();
	                    int aux=0;

	                    while (iter.hasNext()){
	                    	
	                    	log.info("actualizando operaciones de los boletos ETN");
	                    	BoletoETNDto boleto = (BoletoETNDto) iter.next();
	
	                    	PreparedStatement stmt = (PreparedStatement) connect().prepareStatement (" UPDATE BoletosETN " +
	                                " SET operacion = ? WHERE idBoleto = ? AND idViaje = ? AND idReservacion = ? ");
	                      
	                    	log.info("UPD BITACORA operacion: "+Integer.parseInt(operaciones[aux]));
	                    	log.info("UPD BITACORA idBoleto: "+boleto.getIdBoleto());
	                    	log.info("UPD BITACORA idViaje: "+viajeEtn.getIdViaje());
	                    	log.info("UPD BITACORA idRser: "+viajeEtn.getIdReservacion());
                        
		                    stmt.setInt(1, Integer.parseInt(operaciones[aux]));
		                    stmt.setLong(2, boleto.getIdBoleto());
		                    stmt.setLong(3, viajeEtn.getIdViaje());                            
		                    stmt.setLong(4, viajeEtn.getIdReservacion());
                      
		                    log.info("UPD BITACORA id "+bitacora.getId());
		                    log.info("UPD BITACORA idViaje "+viajeEtn.getIdViaje());
		                    log.info("UPD BITACORA idRser "+viajeEtn.getIdReservacion());

		                    stmt.execute();  
		                    aux++;
	                    }
	                    
                  }else{
                	  
                	  log.info("las operaciones no coinciden con los boletos");
                      
                       qry = " UPDATE t_bitacora "+
                      " SET bit_status = ?, bit_codigo_error = ?, destino = ?, bit_no_autorizacion = ? "+
                      " WHERE id_bitacora = ? ";
                      
                        PreparedStatement stmt = (PreparedStatement) connect().prepareStatement (qry);
                  
                        stmt.setLong(1, 0);
                        stmt.setLong(2, 88);
                        stmt.setString(3, "Las operaciones no coinciden con los boletos");                            
                        stmt.setString(4, bitacora.getNo_autorizacion());
                        stmt.setLong(5, bitacora.getId());
                        
                        stmt.execute();  
                        rows = false;
                  }
                	
                }else{
                    rows=false;
                }
                  
              }else{
            	  
            	  log.info("Acutalizando en bitacora porque hubo error en la compra ETN");
                 
	              status = 2; // Estatus de la bitacora que indica el termino de la transaccion con error ya sea del banco o del WS
	              qry = " UPDATE t_bitacora "+
	                      " SET bit_status = ?, bit_codigo_error = ?, destino = ?, bit_no_autorizacion = ?  WHERE id_bitacora = ? ";
                
	              log.info("UPD BITACORA query "+qry);
	              log.info("UPD BITACORA codigo "+bitacora.getCodigo_error());
	              log.info("UPD BITACORA destino "+bitacora.getDestino());
	              log.info("UPD BITACORA autoriza "+bitacora.getNo_autorizacion());
	              log.info("UPD BITACORA id "+bitacora.getId());
	                
	              PreparedStatement statement = (PreparedStatement) connect().prepareStatement (qry);
	
	              statement.setLong(1, bitacora.getStatus());
	              statement.setLong(2, bitacora.getCodigo_error());
	              statement.setString(3, bitacora.getDestino());                            
	              statement.setString(4, bitacora.getNo_autorizacion());
	              statement.setLong(5, bitacora.getId());
	                
	              statement.execute();  
              }
              
        } catch (SQLException ex) {
            log.info("Excepcion QSL en actualizacion "+ex);
             return rows=false;
        } catch(Exception ex){
             log.info("ERROR UPD BITACORA VENTA ETN : " + ex.toString());
             return rows=false;
        } finally {
            try {
				disconect();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				log.error("SQLException : ",e);
			}
        }
         
        return rows;
    }

}

/*
 * ---------------------------------------------------------------
 * Copyright (c) 2012 Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Prolongación paseo de la reforma 61 oficina 6A2 sexto piso, colonia paseo de las lomas
 * C.P. 01330, Delegación Alvaro Obregón, México D.F.
 * Algunos Derechos Reservados
 *
 * Este software contiene informaci&oacute;n totalmente confidencial propiedad de
 * Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Queda totalmente prohibido su uso o divulgacion 
 * en forma parcial o total y solamente podr&aacute; ser utilizada de acuerdo a los 
 * t&eacute;rminos y estatutos que determine la propia empresa.
 * --------------------------------------------------------------
 */
package mx.com.addcel.ETNWSConsumer.dao;

import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import mx.com.addcel.ETNWSConsumer.business.TestWS;

import com.mysql.jdbc.Connection;

/**
 * @author Lic. Elena L&ocute;pez L&ocute;pez
 *
 */
public class DAO {
	private static Logger log=Logger.getLogger(DAO.class);
	private Connection  connection  = null;
    private String userName = "mobilcard";//root-mobilcard
    private String password = "Mobil1696Card";// 54V173?Fo0nt-mobilcard
    private String url = "jdbc:mysql://DataBaseMC/mobilecard";//addcel-mobilcard
    //private String url = "jdbc:mysql://50.57.192.212:3306/mobilecard";//addcel-mobilcard
    
    public static void main(String [] args){
    	
    	try {
			log.info(new DAO().connect().toString());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			log.error("ClassNotFoundException : ",e);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error("SQLException : ",e);
		}
    }

    public  Connection connect () throws ClassNotFoundException, SQLException{
        Class.forName("com.mysql.jdbc.Driver");
        connection = (Connection) DriverManager.getConnection(url, userName, password);
        log.info("conexión "+connection);
        return connection;
    }

    public void disconect() throws SQLException{
        connection.close();
    }

}

package mx.com.addcel.ETNWSConsumer.dto;

import java.util.ArrayList;
import java.util.List;

public class BoletoMaestroDto {

	String nit;
	List<ViajeETNDto> listaBoletos = new ArrayList<ViajeETNDto> ();
	
	
	public String getNit() {
		return nit;
	}
	public void setNit(String nit) {
		this.nit = nit;
	}
	public List<ViajeETNDto> getListaBoletos() {
		return listaBoletos;
	}
	public void setListaBoletos(List<ViajeETNDto> listaBoletos) {
		this.listaBoletos = listaBoletos;
	}
	
	
}

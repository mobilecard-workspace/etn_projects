package mx.com.addcel.ETNWSConsumer.dto;

import java.util.ArrayList;
import java.util.List;

public class CancelacionDto {

	
	List<Integer> folioCancelado = new ArrayList<Integer>();
	int    codError;
	String detError;
	
	public List<Integer> getFolioCancelado() {
		return folioCancelado;
	}
	public void setFolioCancelado(List<Integer> folioCancelado) {
		this.folioCancelado = folioCancelado;
	}
	public int getCodError() {
		return codError;
	}
	public void setCodError(int codError) {
		this.codError = codError;
	}
	public String getDetError() {
		return detError;
	}
	public void setDetError(String detError) {
		this.detError = detError;
	}
	

}

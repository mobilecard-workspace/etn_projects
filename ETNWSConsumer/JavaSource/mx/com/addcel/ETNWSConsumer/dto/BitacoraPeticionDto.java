package mx.com.addcel.ETNWSConsumer.dto;

public class BitacoraPeticionDto{

	private String idUsuario;
	
	private String telefono;
	
	private String tipoProducto;
	
	private String imei;
	
	private String tarjeta;
	
	private String tipoTelefono;
	
	private String software;
	
	private String modelo;
	
	private String wkey;
	
	private String folioETN;
	
	private String autorizaBanco;
	
	private String codError;
	
	private String detError;
	
	private String tipoViaje;
	
	private String monto;
	
	private String proveedor;
	
	private String producto;
	
	private String estatus;
	
	private String concepto;
	
	private String idBitacora;
	
	private String detalle;
	
	private String idViaje;
	
	private String idOrigen;
	
	private String idDestino;
	
	private String origenEtn;
	
	private String destinoEtn;
	
	private String fechaEtn;
	
	private String horaEnt;

	private String tipoCorrida;
	
	private String corrida;
	
	public BitacoraPeticionDto(){}
	
	public String getIdViaje() {
		return idViaje;
	}

	public void setIdViaje(String idViaje) {
		this.idViaje = idViaje;
	}

	public String getIdOrigen() {
		return idOrigen;
	}

	public void setIdOrigen(String idOrigen) {
		this.idOrigen = idOrigen;
	}

	public String getIdDestino() {
		return idDestino;
	}

	public void setIdDestino(String idDestino) {
		this.idDestino = idDestino;
	}

	public String getOrigenEtn() {
		return origenEtn;
	}

	public void setOrigenEtn(String origenEtn) {
		this.origenEtn = origenEtn;
	}

	public String getDestinoEtn() {
		return destinoEtn;
	}

	public void setDestinoEtn(String destinoEtn) {
		this.destinoEtn = destinoEtn;
	}

	public String getFechaEtn() {
		return fechaEtn;
	}

	public void setFechaEtn(String fechaEtn) {
		this.fechaEtn = fechaEtn;
	}

	public String getHoraEnt() {
		return horaEnt;
	}

	public void setHoraEnt(String horaEnt) {
		this.horaEnt = horaEnt;
	}

	public String getTipoCorrida() {
		return tipoCorrida;
	}

	public void setTipoCorrida(String tipoCorrida) {
		this.tipoCorrida = tipoCorrida;
	}

	public String getCorrida() {
		return corrida;
	}

	public void setCorrida(String corrida) {
		this.corrida = corrida;
	}

	public String getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(String idBitacora) {
		this.idBitacora = idBitacora;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getFolioETN() {
		return folioETN;
	}

	public void setFolioETN(String folioETN) {
		this.folioETN = folioETN;
	}

	public String getAutorizaBanco() {
		return autorizaBanco;
	}

	public void setAutorizaBanco(String autorizaBanco) {
		this.autorizaBanco = autorizaBanco;
	}

	public String getCodError() {
		return codError;
	}

	public void setCodError(String codError) {
		this.codError = codError;
	}

	public String getDetError() {
		return detError;
	}

	public void setDetError(String detError) {
		this.detError = detError;
	}

	public String getTipoViaje() {
		return tipoViaje;
	}

	public void setTipoViaje(String tipoViaje) {
		this.tipoViaje = tipoViaje;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTipoProducto() {
		return tipoProducto;
	}

	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getTipoTelefono() {
		return tipoTelefono;
	}

	public void setTipoTelefono(String tipoTelefono) {
		this.tipoTelefono = tipoTelefono;
	}

	public String getSoftware() {
		return software;
	}

	public void setSoftware(String software) {
		this.software = software;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getWkey() {
		return wkey;
	}

	public void setWkey(String wkey) {
		this.wkey = wkey;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	
}

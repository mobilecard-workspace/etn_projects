/*
 * ---------------------------------------------------------------
 * Copyright (c) 2012 Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Prolongación paseo de la reforma 61 oficina 6A2 sexto piso, colonia paseo de las lomas
 * C.P. 01330, Delegación Alvaro Obregón, México D.F.
 * Algunos Derechos Reservados
 *
 * Este software contiene informaci&oacute;n totalmente confidencial propiedad de
 * Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Queda totalmente prohibido su uso o divulgacion 
 * en forma parcial o total y solamente podr&aacute; ser utilizada de acuerdo a los 
 * t&eacute;rminos y estatutos que determine la propia empresa.
 * --------------------------------------------------------------
 */
package mx.com.addcel.ETNWSConsumer.dto;

import mx.com.addcel.ETNWSConsumer.util.ConvertirTipos;

import org.apache.axis.types.UnsignedByte;

/**
 * @author Ing. Efr&eacute;n Reyes Torres
 *
 */
public class CorridaPeticionDto {
	private ConvertirTipos cv = new ConvertirTipos();
	
	/**
     * Origen del viaje
     */
    private String origen;
    /**
     * Destino del viaje
     */
    private String destino;
    /**
     * Fecha salida de viaje. Cadena de caracteres con el formato DDMMAAAA
     */
    private String fecha;
    /**
     * Hora de Salida del Viaje Deseado. Cadena de caracteres con el formato HHMMSS
     */
    private String hora;    
    /**
     * Tipo de Servicio del Viaje Deseado. El catalogo se obtiene con la operación getTiposServicio (unsignedByte – 2)
     */
    private UnsignedByte tipoServicio;
    /**
     * Empresa que solicita la venta
     */
    private String empresa;
    /**
     * Cantidad de adultos solicitados
     */
    private double numAdulto;
    /**
     * Cantidad de niños solicitados
     */
    private double numNino;
    /**
     * Cantidad de insen Solicitados
     */
    private double numInsen;
    /**
     * Cantidad de estudiantes solicitados
     */
    private double numEstudiante;
    /**
     * Cantidad de Maestros solicitados
     */
    private double numMaestro;
    /**
     * Para saber si regresa las corridas de todas las empresas, o sólo de una empresa.
     * 0 – Regresa únicamente las corridas de la empresa principal.
     * 1 – Regresa las corridas de todas las empresas asociadas.
     */
    private UnsignedByte multiempresa;
    /**
     * Cantidad de ProgrPaisano solicitados
     */
    private double numPaisano;
    /**
     * Hora Final para la consulta de corridas Formato HHMMSS
     * Para consultar corridas de la mañana enviar Hora=’050000’ HoraFin=’120000’
     * Para consultar corridas de la tarde enviar Hora=’120000’ HoraFin=’190000’
     */
    private String horaFin;
	
    /**
	 * @return the origen
	 */
	public String getOrigen() {
		return origen;
	}
	/**
	 * @param origen the origen to set
	 */
	public void setOrigen(String origen) {
		this.origen = origen;
	}
	/**
	 * @return the destino
	 */
	public String getDestino() {
		return destino;
	}
	/**
	 * @param destino the destino to set
	 */
	public void setDestino(String destino) {
		this.destino = destino;
	}
	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}
	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	/**
	 * @return the hora
	 */
	public String getHora() {
		return hora;
	}
	/**
	 * @param hora the hora to set
	 */
	public void setHora(String hora) {
		this.hora = hora;
	}
	/**
	 * @return the tipoServicio
	 */
	public UnsignedByte getTipoServicio() {
		return tipoServicio;
	}
	/**
	 * @param tipoServicio the tipoServicio to set
	 */
	public void setTipoServicio(UnsignedByte tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	/**
	 * @return the empresa
	 */
	public String getEmpresa() {
		return empresa;
	}
	/**
	 * @param empresa the empresa to set
	 */
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	/**
	 * @return the numAdulto
	 */
	public double getNumAdulto() {
		return numAdulto;
	}
	/**
	 * @param numAdulto the numAdulto to set
	 */
	public void setNumAdulto(String numAdulto) {
		this.numAdulto = cv.getDoubled(numAdulto);
	}
	/**
	 * @return the numNino
	 */
	public double getNumNino() {
		return numNino;
	}
	/**
	 * @param numNino the numNino to set
	 */
	public void setNumNino(String numNino) {
		this.numNino = cv.getDoubled(numNino);
	}
	/**
	 * @return the numInsen
	 */
	public double getNumInsen() {
		return numInsen;
	}
	/**
	 * @param numInsen the numInsen to set
	 */
	public void setNumInsen(String numInsen) {
		this.numInsen = cv.getDoubled(numInsen);
	}
	/**
	 * @return the numEstudiante
	 */
	public double getNumEstudiante() {
		return numEstudiante;
	}
	/**
	 * @param numEstudiante the numEstudiante to set
	 */
	public void setNumEstudiante(String numEstudiante) {
		this.numEstudiante = cv.getDoubled(numEstudiante);
	}
	/**
	 * @return the numMaestro
	 */
	public double getNumMaestro() {
		return numMaestro;
	}
	/**
	 * @param numMaestro the numMaestro to set
	 */
	public void setNumMaestro(String numMaestro) {
		this.numMaestro = cv.getDoubled(numMaestro);
	}
	/**
	 * @return the multiempresa
	 */
	public UnsignedByte getMultiempresa() {
		return multiempresa;
	}
	/**
	 * @param multiempresa the multiempresa to set
	 */
	public void setMultiempresa(UnsignedByte multiempresa) {
		this.multiempresa = multiempresa;
	}
	/**
	 * @return the numPaisano
	 */
	public double getNumPaisano() {
		return numPaisano;
	}
	/**
	 * @param numPaisano the numPaisano to set
	 */
	public void setNumPaisano(String numPaisano) {
		this.numPaisano = cv.getDoubled(numPaisano);
	}
	/**
	 * @return the horaFin
	 */
	public String getHoraFin() {
		return horaFin;
	}
	/**
	 * @param horaFin the horaFin to set
	 */
	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}
    
    
}

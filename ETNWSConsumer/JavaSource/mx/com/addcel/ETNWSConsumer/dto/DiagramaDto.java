/*
 * ---------------------------------------------------------------
 * Copyright (c) 2012 Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Prolongaci�n paseo de la reforma 61 oficina 6A2 sexto piso, colonia paseo de las lomas
 * C.P. 01330, Delegaci�n Alvaro Obreg�n, M�xico D.F.
 * Algunos Derechos Reservados
 *
 * Este software contiene informaci&oacute;n totalmente confidencial propiedad de
 * Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Queda totalmente prohibido su uso o divulgacion 
 * en forma parcial o total y solamente podr&aacute; ser utilizada de acuerdo a los 
 * t&eacute;rminos y estatutos que determine la propia empresa.
 * --------------------------------------------------------------
 */
package mx.com.addcel.ETNWSConsumer.dto;

/**
 * @author Ing. Efr&eacute;n Reyes Torres
 *
 */
public class DiagramaDto {

	private String[] diagramaAutobus;
	
	private String numFilas;
	
	private String numFilasArriba;
	
	private String descripcion;
	
	private int numError;
	
	private String descError;
	
	private int numAsientos;

	public int getNumAsientos() {
		return numAsientos;
	}

	public void setNumAsientos(int numAsientos) {
		this.numAsientos = numAsientos;
	}

	public String[] getDiagramaAutobus() {
		return diagramaAutobus;
	}

	public void setDiagramaAutobus(String[] diagramaAutobus) {
		this.diagramaAutobus = diagramaAutobus;
	}

	public String getNumFilas() {
		return numFilas;
	}

	public void setNumFilas(String numFilas) {
		this.numFilas = numFilas;
	}
	
	public String getNumFilasArriba() {
		return numFilasArriba;
	}

	public void setNumFilasArriba(String numFilasArriba) {
		this.numFilasArriba = numFilasArriba;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	/**
	 * @return the numError
	 */
	public int getNumError() {
		return numError;
	}

	/**
	 * @param numError the numError to set
	 */
	public void setNumError(int numError) {
		this.numError = numError;
	}

	/**
	 * @return the descError
	 */
	public String getDescError() {
		return descError;
	}

	/**
	 * @param descError the descError to set
	 */
	public void setDescError(String descError) {
		this.descError = descError;
	}
}

/*
 * ---------------------------------------------------------------
 * Copyright (c) 2012 Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Prolongaci�n paseo de la reforma 61 oficina 6A2 sexto piso, colonia paseo de las lomas
 * C.P. 01330, Delegaci�n Alvaro Obreg�n, M�xico D.F.
 * Algunos Derechos Reservados
 *
 * Este software contiene informaci&oacute;n totalmente confidencial propiedad de
 * Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Queda totalmente prohibido su uso o divulgacion 
 * en forma parcial o total y solamente podr&aacute; ser utilizada de acuerdo a los 
 * t&eacute;rminos y estatutos que determine la propia empresa.
 * --------------------------------------------------------------
 */
package mx.com.addcel.ETNWSConsumer.dto;

import org.apache.axis.types.UnsignedInt;
import org.apache.axis.types.UnsignedLong;

/**
 * @author Ing. Efr&eacute;n Reyes Torres
 *
 */
public class VentaBoletoPeticionDto {

	/**
	 * Origen del viaje
	 */
	private String origen;
	
	/**
	 * Fecha salida de viaje. Cadena de caracteres con el formato DDMMAAAA
	 */
	private String fecha;
	
	/**
	 * Numero de corrida seleccionada para viajar
	 */
	private UnsignedInt corrida;
	
	/**
	 * Empresa que solicita la venta
	 */
	private String empresaVende;
	
	/**
	 * Empresa donde Viaja
	 */
	private String empresaViaje;
	
	/**
	 * P = Propia I = Indistinta
	 */
	private String tipoTerminal;
	
	/**
	 * M = Mostrador T = Tel&eacute;fono A = Agencia
	 */
	private String tipoCliente;
	
	/**
	 *  Folio de la Reservación que se desea confirmar.
	 */
	private Double folioReservacion;
		
	/**
	 * Numero de Terminal
	 */
	private UnsignedLong sucursalExterna;
	
	/**
	 * Clave de la Oficina donde se realiza la venta
	 */
	private String oficinaExterna;
	
	/**
	 * Fecha Contable en donde se realiza la venta. DDMMAAAA
	 */
	private String fechaContable;
	
	/**
	 * Forma en que se pago la venta
	 * TB = Tarjeta bancaria
	 * EF = Pago en efectivo
	 */
	private String formasPagoExternas;
	
	/**
	 * Detalle de la forma en que se pago la venta (XML)
	 */
	private String ebFormasPagoTemp;
	
	/**
	 * Sesi&oacute;n de la terminal
	 */
	private UnsignedLong numSesion;
	
	private String success;
	
	private long idViaje;
	
	private String mensaje;

	private String tarjeta;
	
	private String monto;
	
	private String iva;
	
	
	public String getIva() {
		return iva;
	}

	public void setIva(String iva) {
		this.iva = iva;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public long getIdViaje() {
		return idViaje;
	}

	public void setIdViaje(long idViaje) {
		this.idViaje = idViaje;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public UnsignedInt getCorrida() {
		return corrida;
	}

	public void setCorrida(UnsignedInt corrida) {
		this.corrida = corrida;
	}

	public String getEmpresaVende() {
		return empresaVende;
	}

	public void setEmpresaVende(String empresaVende) {
		this.empresaVende = empresaVende;
	}

	public String getEmpresaViaje() {
		return empresaViaje;
	}

	public void setEmpresaViaje(String empresaViaje) {
		this.empresaViaje = empresaViaje;
	}

	public String getTipoTerminal() {
		return tipoTerminal;
	}

	public void setTipoTerminal(String tipoTerminal) {
		this.tipoTerminal = tipoTerminal;
	}

	public String getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	public Double getFolioReservacion() {
		return folioReservacion;
	}

	public void setFolioReservacion(Double folioReservacion) {
		this.folioReservacion = folioReservacion;
	}

	public UnsignedLong getSucursalExterna() {
		return sucursalExterna;
	}

	public void setSucursalExterna(UnsignedLong sucursalExterna) {
		this.sucursalExterna = sucursalExterna;
	}

	public String getOficinaExterna() {
		return oficinaExterna;
	}

	public void setOficinaExterna(String oficinaExterna) {
		this.oficinaExterna = oficinaExterna;
	}

	public String getFechaContable() {
		return fechaContable;
	}

	public void setFechaContable(String fechaContable) {
		this.fechaContable = fechaContable;
	}

	public String getFormasPagoExternas() {
		return formasPagoExternas;
	}

	public void setFormasPagoExternas(String formasPagoExternas) {
		this.formasPagoExternas = formasPagoExternas;
	}

	public String getEbFormasPagoTemp() {
		return ebFormasPagoTemp;
	}

	public void setEbFormasPagoTemp(String ebFormasPagoTemp) {
		this.ebFormasPagoTemp = ebFormasPagoTemp;
	}

	public UnsignedLong getNumSesion() {
		return numSesion;
	}

	public void setNumSesion(UnsignedLong numSesion) {
		this.numSesion = numSesion;
	}

}

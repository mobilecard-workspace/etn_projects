package mx.com.addcel.ETNWSConsumer.dto;

import org.apache.axis.types.UnsignedLong;

import mx.com.addcel.ETNWSConsumer.util.ConvertirTipos;

public class CancelacionPeticionDto {
	
	private String origen;
	private double corrida;
	private String fechaSalida;
	private String folioVenta;
	private String empresaVende;
	private String empresaViaja;
	private UnsignedLong sucExterna;
	private String oficinaExterna="";
	private String fechaContable;
	private UnsignedLong numSesion;
	private ConvertirTipos cv = new ConvertirTipos();
	
	public String getOrigen() {
		return origen;
	}
	public void setOrigen(String origen) {
		this.origen = origen;
	}
	public double getCorrida() {
		return corrida;
	}
	public void setCorrida(String corrida) { 
		this.corrida = cv.getDouble(corrida);
	}
	public String getFechaSalida() {
		return fechaSalida;
	}
	public void setFechaSalida(String fechaSalida) {
		this.fechaSalida = fechaSalida;
	}
	public String getFolioVenta() {
		return folioVenta;
	}
	public void setFolioVenta(String folioVenta) {
		this.folioVenta = folioVenta;
	}
	public String getEmpresaVende() {
		return empresaVende;
	}
	public void setEmpresaVende(String empresaVende) {
		this.empresaVende = empresaVende;
	}
	public String getEmpresaViaja() {
		return empresaViaja;
	}
	public void setEmpresaViaja(String empresaViaja) {
		this.empresaViaja = empresaViaja;
	}
	public UnsignedLong getSucExterna() {
		return sucExterna;
	}
	public void setSucExterna(String sucExterna) {
		this.sucExterna = new UnsignedLong(0);		
	}
	
	public String getOficinaExterna() {
		return oficinaExterna;
	}
	public void setOficinaExterna(String oficinaExterna) {
		this.oficinaExterna = oficinaExterna;
	}
	public String getFechaContable() {
		return fechaContable;
	}
	public void setFechaContable(String fechaContable) {
		this.fechaContable = fechaContable;
	}
	public UnsignedLong getNumSesion() {
		return numSesion;
	}
	public void setNumSesion(String numSesion) {
		this.numSesion = new UnsignedLong(0);
	}
	
	

}

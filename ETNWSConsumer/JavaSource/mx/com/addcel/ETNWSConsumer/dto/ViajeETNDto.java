/*
 * ---------------------------------------------------------------
 * Copyright (c) 2012 Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Prolongación paseo de la reforma 61 oficina 6A2 sexto piso, colonia paseo de las lomas
 * C.P. 01330, Delegación Alvaro Obregón, México D.F.
 * Algunos Derechos Reservados
 *
 * Este software contiene informaci&oacute;n totalmente confidencial propiedad de
 * Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Queda totalmente prohibido su uso o divulgacion 
 * en forma parcial o total y solamente podr&aacute; ser utilizada de acuerdo a los 
 * t&eacute;rminos y estatutos que determine la propia empresa.
 * --------------------------------------------------------------
 */
package mx.com.addcel.ETNWSConsumer.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lic. Elena L&ocute;pez L&ocute;pez
 *
 */
public class ViajeETNDto {
	
	private long idViaje;
	
	private long idReservacion;
	
	private long nit;
	
	private String idOrigen;
	
	private String idDestino;	
	
	private String corridaETN;
	
	private String destinoETN;
	
	private String origenETN;
	
	private String fechaETN;
	
	private String horaETN;
	
	private String diaViaje;
	
	private String tipoViaje;
	
	private String tipoCorrida;

	private double importeTotal;
	
	private List<BoletoETNDto> listaBoletos;
	
	private long idBitacora;
	 
	private String nombre;
    
    private String tipoTarjeta;
    
    private String iva;
 
	 
	public String getIva() {
		return iva;
	}


	public void setIva(String iva) {
		this.iva = iva;
	}


	public ViajeETNDto(){}
	
	
    public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getTipoTarjeta() {
		return tipoTarjeta;
	}


	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}


	public long getIdBitacora() {
        return idBitacora;
    }

    public void setIdBitacora(long idBitacora) {
        this.idBitacora = idBitacora;
    }
	
	public List<BoletoETNDto> getListaBoletos() {
		return listaBoletos;
	}

	public void setListaBoletos(List<BoletoETNDto> listaBoletos) {
		this.listaBoletos = listaBoletos;
	}

	public double getImporteTotal() {
		return importeTotal;
	}

	public void setImporteTotal(double importeTotal) {
		this.importeTotal = importeTotal;
	}

	public long getIdViaje() {
		return idViaje;
	}

	public void setIdViaje(long idViaje) {
		this.idViaje = idViaje;
	}

	public long getIdReservacion() {
		return idReservacion;
	}

	public void setIdReservacion(long idReservacion) {
		this.idReservacion = idReservacion;
	}

	public long getNit() {
		return nit;
	}

	public void setNit(long nit) {
		this.nit = nit;
	}

	public String getIdOrigen() {
		return idOrigen;
	}

	public void setIdOrigen(String idOrigen) {
		this.idOrigen = idOrigen;
	}

	public String getIdDestino() {
		return idDestino;
	}

	public void setIdDestino(String idDestino) {
		this.idDestino = idDestino;
	}

	public String getCorridaETN() {
		return corridaETN;
	}

	public void setCorridaETN(String corridaETN) {
		this.corridaETN = corridaETN;
	}

	public String getDestinoETN() {
		return destinoETN;
	}

	public void setDestinoETN(String destinoETN) {
		this.destinoETN = destinoETN;
	}

	public String getOrigenETN() {
		return origenETN;
	}

	public void setOrigenETN(String origenETN) {
		this.origenETN = origenETN;
	}

	public String getFechaETN() {
		return fechaETN;
	}

	public void setFechaETN(String fechaETN) {
		this.fechaETN = fechaETN;
	}

	public String getHoraETN() {
		return horaETN;
	}

	public void setHoraETN(String horaETN) {
		this.horaETN = horaETN;
	}

	public String getDiaViaje() {
		return diaViaje;
	}

	public void setDiaViaje(String diaViaje) {
		this.diaViaje = diaViaje;
	}

	public String getTipoViaje() {
		return tipoViaje;
	}

	public void setTipoViaje(String tipoViaje) {
		this.tipoViaje = tipoViaje;
	}

	public String getTipoCorrida() {
		return tipoCorrida;
	}

	public void setTipoCorrida(String tipoCorrida) {
		this.tipoCorrida = tipoCorrida;
	}
	
}

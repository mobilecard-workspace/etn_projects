/*
 * ---------------------------------------------------------------
 * Copyright (c) 2012 Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Prolongaci�n paseo de la reforma 61 oficina 6A2 sexto piso, colonia paseo de las lomas
 * C.P. 01330, Delegaci�n Alvaro Obreg�n, M�xico D.F.
 * Algunos Derechos Reservados
 *
 * Este software contiene informaci&oacute;n totalmente confidencial propiedad de
 * Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Queda totalmente prohibido su uso o divulgacion 
 * en forma parcial o total y solamente podr&aacute; ser utilizada de acuerdo a los 
 * t&eacute;rminos y estatutos que determine la propia empresa.
 * --------------------------------------------------------------
 */
package mx.com.addcel.ETNWSConsumer.dto;

/**
 * @author Ing. Efr&eacute;n Reyes Torres
 *
 */
public class ReservacionPeticionDto {
	
	private String idPago;

	/**
	 * Origen del viaje
	 */
	private String origen;
	
	/**
	 * Destino del viaje
	 */
	private String destino;
	
	/**
	 * Fecha salida de viaje. Cadena de caracteres con el formato DDMMAAAA
	 */
	private String fecha;
	
	/**
	 * Hora de salida de viaje. Cadena con el Formato HHMMSS
	 */
	private String hora;
	
	/**
	 * Cantidad de adultos solicitados
	 */
	private Double numAdulto;
	
	/**
	 * Cantidad de ni�os solicitados
	 */
	private Double numNino;
	
	/**
	 * Cantidad de insen Solicitados
	 */
	private Double numInsen;
	
	/**
	 * Cantidad de estudiantes solicitados
	 */
	private Double numEstudiante;
	
	/**
	 * Cantidad de maestros solicitados
	 */
	private Double numMaestro;
	
	/**
	 * Numero de corrida seleccionada para viajar
	 */
	private Double corrida;
	
	/**
	 * 
	 */
	private String tipoCorrida;
	
	/**
	 * Cadena de Asientos Seleccionados.
	 * Ejemplo:
	 * 020417
	 * Significa que desean el asiento 2, 4 y 17.
	 * La asignaci�n de los asientos se basa en la cantidad deasientos solicitados por cada tipo de pasajero, es decir, si setiene una cadena del tipo 0102040708, y en los par�metros deNoAdulto tenemos 3 y NoNino tenemos 1 y NoInsen 1, losasientos se asignaran de la siguiente manera, 01, 02,04, ser�nlos de adulto, 07 de ni�o y 08 de INSEN.
	 */
	private String asientos;
	
	/**
	 * Cadena de Nombres de Pasajeros.
	 * 35 Caracteres para cada nombre de pasajero.
	 */
	private String nombres;
	
	/**
	 * En desuso, enviar un CERO
	 */
	private double folioReservacion;
	
	/**
	 * Empresa que solicita la venta
	 */
	private String empresaVende;
	
	/**
	 * Empresa donde Viaja
	 */
	private String empresaViaje;
	
	/**
	 * P = Propia I = Indistinta
	 */
	private String tipoTerminal;
	
	/**
	 * M = Mostrador T = Tel�fono A = Agencia
	 */
	private String tipoCliente;
	
	/**
	 * CS = Confirmado de sistema
	 * CE = Confirmado electr�nico
	 * NS = No confirmado de sistema
	 * NE = No confirmado electr�nico
	 * R = Reservaci�n
	 */
	private String tipoOperacion;
	
	/**
	 * Tarifa unitaria del boleto por adulto
	 */
	private double tarifaAdulto;
	
	/**
	 * Tarifa unitaria del boleto por nino
	 */
	private double tarifaNino;
	
	/**
	 * Tarifa unitaria del boleto por INSEN
	 */
	private double tarifaInsen;
	
	/**
	 * Tarifa unitaria del boleto por estudiante
	 */
	private double tarifaEstudiante;
	
	/**
	 * Tarifa unitaria del boleto por maestro
	 */
	private double tarifaMaestro;
	
	public String getIdOrigen() {
		return idOrigen;
	}

	public void setIdOrigen(String idOrigen) {
		this.idOrigen = idOrigen;
	}

	public String getIdDestino() {
		return idDestino;
	}

	public void setIdDestino(String idDestino) {
		this.idDestino = idDestino;
	}

	/**
	 * Número de identificación de la transacción completa
	 */
	private long idViaje;
	
	private String idOrigen;
	
	private String idDestino;
	

	public long getIdViaje() {
		return idViaje;
	}

	public void setIdViaje(long idViaje) {
		this.idViaje = idViaje;
	}

	public String getTipoCorrida() {
		return tipoCorrida;
	}

	public void setTipoCorrida(String tipoCorrida) {
		this.tipoCorrida = tipoCorrida;
	}

	public double getTarifaAdulto() {
		return tarifaAdulto;
	}

	public void setTarifaAdulto(double tarifaAdulto) {
		this.tarifaAdulto = tarifaAdulto;
	}

	public double getTarifaNino() {
		return tarifaNino;
	}

	public void setTarifaNino(double tarifaNino) {
		this.tarifaNino = tarifaNino;
	}

	public double getTarifaInsen() {
		return tarifaInsen;
	}

	public void setTarifaInsen(double tarifaInsen) {
		this.tarifaInsen = tarifaInsen;
	}

	public double getTarifaEstudiante() {
		return tarifaEstudiante;
	}

	public void setTarifaEstudiante(double tarifaEstudiante) {
		this.tarifaEstudiante = tarifaEstudiante;
	}

	public double getTarifaMaestro() {
		return tarifaMaestro;
	}

	public void setTarifaMaestro(double tarifaMaestro) {
		this.tarifaMaestro = tarifaMaestro;
	}

	/**
	 * @return the origen
	 */
	public String getOrigen() {
		return origen;
	}

	/**
	 * @param origen the origen to set
	 */
	public void setOrigen(String origen) {
		this.origen = origen;
	}

	/**
	 * @return the destino
	 */
	public String getDestino() {
		return destino;
	}

	/**
	 * @param destino the destino to set
	 */
	public void setDestino(String destino) {
		this.destino = destino;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the hora
	 */
	public String getHora() {
		return hora;
	}

	/**
	 * @param hora the hora to set
	 */
	public void setHora(String hora) {
		this.hora = hora;
	}

	/**
	 * @return the numAdulto
	 */
	public Double getNumAdulto() {
		return numAdulto;
	}

	/**
	 * @param numAdulto the numAdulto to set
	 */
	public void setNumAdulto(String numAdulto) {
		this.numAdulto = validaString(numAdulto);
	}

	/**
	 * @return the numNino
	 */
	public Double getNumNino() {
		return numNino;
	}

	/**
	 * @param numNino the numNino to set
	 */
	public void setNumNino(String numNino) {
		this.numNino = validaString(numNino);
	}

	/**
	 * @return the numInsen
	 */
	public Double getNumInsen() {
		return numInsen;
	}

	/**
	 * @param numInsen the numInsen to set
	 */
	public void setNumInsen(String numInsen) {
		this.numInsen = validaString(numInsen);
	}

	/**
	 * @return the numEstudiante
	 */
	public Double getNumEstudiante() {
		return numEstudiante;
	}

	/**
	 * @param numEstudiante the numEstudiante to set
	 */
	public void setNumEstudiante(String numEstudiante) {
		this.numEstudiante = validaString(numEstudiante);
	}

	/**
	 * @return the numMaestro
	 */
	public Double getNumMaestro() {
		return numMaestro;
	}

	/**
	 * @param numMaestro the numMaestro to set
	 */
	public void setNumMaestro(String numMaestro) {
		this.numMaestro = validaString(numMaestro);
	}

	/**
	 * @return the corrida
	 */
	public Double getCorrida() {
		return corrida;
	}

	/**
	 * @param corrida the corrida to set
	 */
	public void setCorrida(String corrida) {
		this.corrida = validaString(corrida);
	}

	/**
	 * @return the asientos
	 */
	public String getAsientos() {
		return asientos;
	}

	/**
	 * @param asientos the asientos to set
	 */
	public void setAsientos(String asientos) {
		this.asientos = asientos;
	}

	/**
	 * @return the nombres
	 */
	public String getNombres() {
		return nombres;
	}

	/**
	 * @param nombres the nombres to set
	 */
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	/**
	 * @return the folioReservacion
	 */
	public Double getFolioReservacion() {
		return folioReservacion;
	}

	/**
	 * @param folioReservacion the folioReservacion to set
	 */
	public void setFolioReservacion(double folioReservacion) {
		this.folioReservacion = 0;
	}

	/**
	 * @return the empresaVende
	 */
	public String getEmpresaVende() {
		return empresaVende;
	}

	/**
	 * @param empresaVende the empresaVende to set
	 */
	public void setEmpresaVende(String empresaVende) {
		this.empresaVende = empresaVende;
	}

	/**
	 * @return the empresaViaje
	 */
	public String getEmpresaViaje() {
		return empresaViaje;
	}

	/**
	 * @param empresaViaje the empresaViaje to set
	 */
	public void setEmpresaViaje(String empresaViaje) {
		this.empresaViaje = empresaViaje;
	}

	/**
	 * @return the tipoTerminal
	 */
	public String getTipoTerminal() {
		return tipoTerminal;
	}

	/**
	 * @param tipoTerminal the tipoTerminal to set
	 */
	public void setTipoTerminal(String tipoTerminal) {
		this.tipoTerminal = tipoTerminal;
	}

	/**
	 * @return the tipoCliente
	 */
	public String getTipoCliente() {
		return tipoCliente;
	}

	/**
	 * @param tipoCliente the tipoCliente to set
	 */
	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	/**
	 * @return the tipoOperacion
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}

	/**
	 * @param tipoOperacion the tipoOperacion to set
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	
	private Double validaString(String dato){		
		Double num = 0.0;		
		if(dato!=null){
			num = Double.parseDouble(dato);
		}
		return num;
	}

	public String getIdPago() {
		return idPago;
	}

	public void setIdPago(String idPago) {
		this.idPago = idPago;
	}
}

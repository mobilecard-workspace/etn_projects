/*
 * ---------------------------------------------------------------
 * Copyright (c) 2012 Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Prolongación paseo de la reforma 61 oficina 6A2 sexto piso, colonia paseo de las lomas
 * C.P. 01330, Delegación Alvaro Obregón, México D.F.
 * Algunos Derechos Reservados
 *
 * Este software contiene informaci&oacute;n totalmente confidencial propiedad de
 * Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Queda totalmente prohibido su uso o divulgacion 
 * en forma parcial o total y solamente podr&aacute; ser utilizada de acuerdo a los 
 * t&eacute;rminos y estatutos que determine la propia empresa.
 * --------------------------------------------------------------
 */
package mx.com.addcel.ETNWSConsumer.dto;

/**
 * @author Ing. Efr&eacute;n Reyes Torres
 *
 */
public class TerminalPeticionDto {
	
	private String claveEmpresa;
    
    /**
     * Tipo de terminal a listar (N normal, B boletera)
     */
    private char tipoTerminal;
    
    private String claveUsuario;

	/**
	 * @return the claveEmpresa
	 */
	public String getClaveEmpresa() {
		return claveEmpresa;
	}

	/**
	 * @param claveEmpresa the claveEmpresa to set
	 */
	public void setClaveEmpresa(String claveEmpresa) {
		this.claveEmpresa = claveEmpresa;
	}

	/**
	 * @return the tipoTerminal
	 */
	public char getTipoTerminal() {
		return tipoTerminal;
	}

	/**
	 * @param tipoTerminal the tipoTerminal to set
	 */
	public void setTipoTerminal(char tipoTerminal) {
		this.tipoTerminal = tipoTerminal;
	}

	/**
	 * @return the claveUsuario
	 */
	public String getClaveUsuario() {
		return claveUsuario;
	}

	/**
	 * @param claveUsuario the claveUsuario to set
	 */
	public void setClaveUsuario(String claveUsuario) {
		this.claveUsuario = claveUsuario;
	}
}

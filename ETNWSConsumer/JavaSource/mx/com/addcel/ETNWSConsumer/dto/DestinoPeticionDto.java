/*
 * ---------------------------------------------------------------
 * Copyright (c) 2012 Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Prolongación paseo de la reforma 61 oficina 6A2 sexto piso, colonia paseo de las lomas
 * C.P. 01330, Delegación Alvaro Obregón, México D.F.
 * Algunos Derechos Reservados
 *
 * Este software contiene informaci&oacute;n totalmente confidencial propiedad de
 * Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Queda totalmente prohibido su uso o divulgacion 
 * en forma parcial o total y solamente podr&aacute; ser utilizada de acuerdo a los 
 * t&eacute;rminos y estatutos que determine la propia empresa.
 * --------------------------------------------------------------
 */
package mx.com.addcel.ETNWSConsumer.dto;

/**
 * @author Ing. Efr&eacute;n Reyes Torres
 *
 */
public class DestinoPeticionDto {
	/**
     * Clave de Oficina Origen
     */
    String oficinaOrigen;
    /**
     * Empresa que solicita la venta
     */
    String empresaSolicita;
    /**
     * Empresa por la que viaja
     */
    String empresaViaja;
	/**
	 * @return the oficinaOrigen
	 */
	public String getOficinaOrigen() {
		return oficinaOrigen;
	}
	/**
	 * @param oficinaOrigen the oficinaOrigen to set
	 */
	public void setOficinaOrigen(String oficinaOrigen) {
		this.oficinaOrigen = oficinaOrigen;
	}
	/**
	 * @return the empresaSolicita
	 */
	public String getEmpresaSolicita() {
		return empresaSolicita;
	}
	/**
	 * @param empresaSolicita the empresaSolicita to set
	 */
	public void setEmpresaSolicita(String empresaSolicita) {
		this.empresaSolicita = empresaSolicita;
	}
	/**
	 * @return the empresaViaja
	 */
	public String getEmpresaViaja() {
		return empresaViaja;
	}
	/**
	 * @param empresaViaja the empresaViaja to set
	 */
	public void setEmpresaViaja(String empresaViaja) {
		this.empresaViaja = empresaViaja;
	}
    
}

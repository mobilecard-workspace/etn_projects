package mx.com.addcel.ETNWSConsumer.dto;


public class BitacoraViajeDto {

	ViajeETNDto viaje;
	Bitacora bitacora;
	
	public ViajeETNDto getViaje() {
		return viaje;
	}
	public void setViaje(ViajeETNDto viaje) {
		this.viaje = viaje;
	}
	public Bitacora getBitacora() {
		return bitacora;
	}
	public void setBitacora(Bitacora bitacora) {
		this.bitacora = bitacora;
	}
	
}

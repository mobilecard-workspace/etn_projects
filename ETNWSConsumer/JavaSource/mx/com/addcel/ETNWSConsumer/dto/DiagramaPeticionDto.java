/*
 * ---------------------------------------------------------------
 * Copyright (c) 2012 Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Prolongaci�n paseo de la reforma 61 oficina 6A2 sexto piso, colonia paseo de las lomas
 * C.P. 01330, Delegaci�n Alvaro Obreg�n, M�xico D.F.
 * Algunos Derechos Reservados
 *
 * Este software contiene informaci&oacute;n totalmente confidencial propiedad de
 * Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Queda totalmente prohibido su uso o divulgacion 
 * en forma parcial o total y solamente podr&aacute; ser utilizada de acuerdo a los 
 * t&eacute;rminos y estatutos que determine la propia empresa.
 * --------------------------------------------------------------
 */
package mx.com.addcel.ETNWSConsumer.dto;

import mx.com.addcel.ETNWSConsumer.util.ConvertirTipos;

import org.apache.axis.types.UnsignedInt;

/**
 * @author Ing. Efr&eacute;n Reyes Torres
 *
 */
public class DiagramaPeticionDto {
	private ConvertirTipos ct = new ConvertirTipos();

	/**
	 * Empresa que solicita la venta
	 */
	private String empresaVende;
	
	/**
	 * Numero de corrida seleccionada para viajar
	 */
	private UnsignedInt corrida;
	
	/**
	 * Fecha salida de viaje. Cadena de caracteres con el formato DDMMAAAA
	 */
	private String fecha;
	
	/**
	 * Empresa donde Viaja
	 */
	private String empresaViaje;

	public String getEmpresaVende() {
		return empresaVende;
	}

	public void setEmpresaVende(String empresaVende) {
		this.empresaVende = empresaVende;
	}

	public UnsignedInt getCorrida() {
		return corrida;
	}

	public void setCorrida(String corrida) {		
		UnsignedInt uIcorrida = new UnsignedInt();
    	Long corridaLong = ct.getLong(corrida); 
    	uIcorrida.setValue(corridaLong);
		this.corrida = uIcorrida;		
	}
		
	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getEmpresaViaje() {
		return empresaViaje;
	}

	public void setEmpresaViaje(String empresaViaje) {
		this.empresaViaje = empresaViaje;
	}
}

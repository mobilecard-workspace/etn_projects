/*
 * ---------------------------------------------------------------
 * Copyright (c) 2012 Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Prolongación paseo de la reforma 61 oficina 6A2 sexto piso, colonia paseo de las lomas
 * C.P. 01330, Delegación Alvaro Obregón, México D.F.
 * Algunos Derechos Reservados
 *
 * Este software contiene informaci&oacute;n totalmente confidencial propiedad de
 * Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Queda totalmente prohibido su uso o divulgacion 
 * en forma parcial o total y solamente podr&aacute; ser utilizada de acuerdo a los 
 * t&eacute;rminos y estatutos que determine la propia empresa.
 * --------------------------------------------------------------
 */
package mx.com.addcel.ETNWSConsumer.dto;

import mx.com.addcel.ETNWSConsumer.util.ConvertirTipos;


/**
 * @author Lic. Elena L&ocute;pez L&ocute;pez
 *
 */
public class ItinerarioPeticionDto {
	
	private ConvertirTipos cv = new ConvertirTipos();
	/*
	 * Origen del viaje
	 */
	private String origen;
	
	/*
	 * Fecha salida de viaje. Cadena de caracteres con el formato DDMMAAAA
	 */
	private String fecha;
	
	/*
	 * Numero de corrida seleccionada para viajar
	 */
	private Double corrida;
	
	/*
	 * Empresa que solicita la venta
	 */
	private String empresaSolicita;
	
	/*
	 * Empresa donde Viaja
	 */
	private String empresaViaje;

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public Double getCorrida() {
		return corrida;
	}

	public void setCorrida(String corrida) {;
		this.corrida = cv.getDouble(corrida);
	}

	public String getEmpresaSolicita() {
		return empresaSolicita;
	}

	public void setEmpresaSolicita(String empresaSolicita) {
		this.empresaSolicita = empresaSolicita;
	}

	public String getEmpresaViaje() {
		return empresaViaje;
	}

	public void setEmpresaViaje(String empresaViaje) {
		this.empresaViaje = empresaViaje;
	}
	
	
	
}

package mx.com.addcel.ETNWSConsumer.dto;

import org.apache.axis.types.UnsignedInt;
import org.apache.axis.types.UnsignedLong;

public class LibReservaPeticionDto {
	
	/**
	 * Folio de la reservacion a liberar
	 */
	private UnsignedLong  folioReservacion ;
	
	/*
	 * Empresa que realiza la venta
	 * */	
	private String empresaVende;
	
	/**
	 * Empresa por la que se viaja
	 */	
	private String empresaViaja;
	
	public UnsignedLong getFolioReservacion() {
		return folioReservacion;
	}

	public void setFolioReservacion(String folioReservacion) {
		this.folioReservacion = validaString(folioReservacion);
	}

	public String getEmpresaVende() {
		return empresaVende;
	}

	public void setEmpresaVende(String empresaVende) {
		this.empresaVende = empresaVende;
	}

	public String getEmpresaViaja() {
		return empresaViaja;
	}

	public void setEmpresaViaja(String empresaViaja) {
		this.empresaViaja = empresaViaja;
	}

	private UnsignedLong validaString(String dato){		
		
		UnsignedLong uLdato;
		
		if(dato!=null){
			uLdato = new UnsignedLong(dato);
		}else{
			uLdato = new UnsignedLong("");
		}
				
		return uLdato; 
	}
}

/*
 * ---------------------------------------------------------------
 * Copyright (c) 2012 Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Prolongación paseo de la reforma 61 oficina 6A2 sexto piso, colonia paseo de las lomas
 * C.P. 01330, Delegación Alvaro Obregón, México D.F.
 * Algunos Derechos Reservados
 *
 * Este software contiene informaci&oacute;n totalmente confidencial propiedad de
 * Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Queda totalmente prohibido su uso o divulgacion 
 * en forma parcial o total y solamente podr&aacute; ser utilizada de acuerdo a los 
 * t&eacute;rminos y estatutos que determine la propia empresa.
 * --------------------------------------------------------------
 */
package mx.com.addcel.ETNWSConsumer.dto;

import org.apache.axis.types.UnsignedByte;

/**
 * @author Ing. Efr&eacute;n Reyes Torres
 *
 */
public class TarifaPeticionDto {
	/**
     * Empresa que solicita la venta
     */
    private String empresaVende;
    /**
     * Origen del viaje
     */
    private String origen;
    /**
     * Destino del viaje
     */
    private String destino;
    /**
     * Tipo de Servicio del Viaje Deseado. El catalogo se obtiene con la operación getTiposServicio (unsignedByte – 2)
     */
    private UnsignedByte tipoServicio;
    /**
     * Empresa donde Viaja
     */
    private String empresaViaje;
	/**
	 * @return the empresaVende
	 */
	public String getEmpresaVende() {
		return empresaVende;
	}
	/**
	 * @param empresaVende the empresaVende to set
	 */
	public void setEmpresaVende(String empresaVende) {
		this.empresaVende = empresaVende;
	}
	/**
	 * @return the origen
	 */
	public String getOrigen() {
		return origen;
	}
	/**
	 * @param origen the origen to set
	 */
	public void setOrigen(String origen) {
		this.origen = origen;
	}
	/**
	 * @return the destino
	 */
	public String getDestino() {
		return destino;
	}
	/**
	 * @param destino the destino to set
	 */
	public void setDestino(String destino) {
		this.destino = destino;
	}
	/**
	 * @return the tipoServicio
	 */
	public UnsignedByte getTipoServicio() {
		return tipoServicio;
	}
	/**
	 * @param tipoServicio the tipoServicio to set
	 */
	public void setTipoServicio(UnsignedByte tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	/**
	 * @return the empresaViaje
	 */
	public String getEmpresaViaje() {
		return empresaViaje;
	}
	/**
	 * @param empresaViaje the empresaViaje to set
	 */
	public void setEmpresaViaje(String empresaViaje) {
		this.empresaViaje = empresaViaje;
	}
}

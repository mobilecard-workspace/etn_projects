/*
 * com.addcel.quartz.model.AbstractVO.java	
 *
 * Copyright by AddCel,
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of AddCel 
 *
 * --------------------------------------------------------------------------------------------------------------------------------------------------------
 *								MODIFICATION LOG
 *
 * SE					DATE		RFS#		PURPOSE
 * --------------------------------------------------------------------------------------------------------------------------------------------------------
 * Victor Lira			27/06/2013	NA			Initial Version
 * --------------------------------------------------------------------------------------------------------------------------------------------------------
 */
package mx.com.addcel.ETNWSConsumer.dto;


import org.apache.commons.lang.builder.StandardToStringStyle;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.log4j.Logger;


public class Functions {
	private static final StandardToStringStyle ss=new StandardToStringStyle();
	private static Logger log=Logger.getLogger(Functions.class);

	/**
	 * Returns the methods that does not need a parameter And the value that
	 * those methods return It is useful in debugging. For examplo
	 * System.out.println("user = " + userVO);
	 * 
	 * @return
	 */
	public static String toString(Object o) {
		String resultado = null;
		try {
			ss.setFieldSeparator("\n");
			ss.setNullText("null");
			ss.setUseClassName(true);
			ss.setFieldNameValueSeparator(": ");
			ss.setArrayContentDetail(true);
			ss.setArraySeparator(",");
			ss.setUseIdentityHashCode(false);
			ss.setContentEnd("\n ]");
			ss.setContentStart("[\n");
			ss.setDefaultFullDetail(true);
			resultado=ToStringBuilder.reflectionToString(o,ss,true);
		} catch (Exception e) {
			log.error("Error en el toString : ",e);
		}
		return resultado;
	}
}

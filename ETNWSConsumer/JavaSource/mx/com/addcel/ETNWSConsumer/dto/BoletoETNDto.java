/*
 * ---------------------------------------------------------------
 * Copyright (c) 2012 Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Prolongación paseo de la reforma 61 oficina 6A2 sexto piso, colonia paseo de las lomas
 * C.P. 01330, Delegación Alvaro Obregón, México D.F.
 * Algunos Derechos Reservados
 *
 * Este software contiene informaci&oacute;n totalmente confidencial propiedad de
 * Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Queda totalmente prohibido su uso o divulgacion 
 * en forma parcial o total y solamente podr&aacute; ser utilizada de acuerdo a los 
 * t&eacute;rminos y estatutos que determine la propia empresa.
 * --------------------------------------------------------------
 */
package mx.com.addcel.ETNWSConsumer.dto;

/**
 * @author Lic. Elena L&ocute;pez L&ocute;pez
 *
 */
public class BoletoETNDto {
	
	private long idBoleto;
	
	private long idViaje;
	
	private long idReservacion;
	
	private long operacion;
	
	private String tipoAsiento;
	
	private double importeAsiento;
	
	private String asiento;
	
	private String nombre;

	public BoletoETNDto(){}

	
	
	public long getIdBoleto() {
		return idBoleto;
	}


	public void setIdBoleto(long idBoleto) {
		this.idBoleto = idBoleto;
	}



	public long getIdViaje() {
		return idViaje;
	}

	public void setIdViaje(long idViaje) {
		this.idViaje = idViaje;
	}

	public long getIdReservacion() {
		return idReservacion;
	}

	public void setIdReservacion(long idReservacion) {
		this.idReservacion = idReservacion;
	}

	public long getOperacion() {
		return operacion;
	}

	public void setOperacion(long operacion) {
		this.operacion = operacion;
	}

	public String getTipoAsiento() {
		return tipoAsiento;
	}

	public void setTipoAsiento(String tipoAsiento) {
		this.tipoAsiento = tipoAsiento;
	}

	public double getImporteAsiento() {
		return importeAsiento;
	}

	public void setImporteAsiento(double importeAsiento) {
		this.importeAsiento = importeAsiento;
	}

	public String getAsiento() {
		return asiento;
	}

	public void setAsiento(String asiento) {
		this.asiento = asiento;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}

package mx.com.addcel.ETNWSConsumer.dto;

public class TipoServicioDto {

	private String cveServicio = "";
	private String detServicio = "";
	private String idServicio    = "";
	
	public String getCveServicio() {
		return cveServicio;
	}
	public void setCveServicio(String cveServicio) {
		this.cveServicio = cveServicio;
	}
	public String getDetServicio() {
		return detServicio;
	}
	public void setDetServicio(String detServicio) {
		this.detServicio = detServicio;
	}
	public String getIdServicio() {
		return idServicio;
	}
	public void setIdServicio(String idServicio) {
		this.idServicio = idServicio;
	}
	
}

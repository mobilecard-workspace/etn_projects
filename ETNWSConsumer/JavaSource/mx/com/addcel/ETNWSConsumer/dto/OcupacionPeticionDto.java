/*
 * ---------------------------------------------------------------
 * Copyright (c) 2012 Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Prolongación paseo de la reforma 61 oficina 6A2 sexto piso, colonia paseo de las lomas
 * C.P. 01330, Delegación Alvaro Obregón, México D.F.
 * Algunos Derechos Reservados
 *
 * Este software contiene informaci&oacute;n totalmente confidencial propiedad de
 * Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Queda totalmente prohibido su uso o divulgacion 
 * en forma parcial o total y solamente podr&aacute; ser utilizada de acuerdo a los 
 * t&eacute;rminos y estatutos que determine la propia empresa.
 * --------------------------------------------------------------
 */
package mx.com.addcel.ETNWSConsumer.dto;

import mx.com.addcel.ETNWSConsumer.util.ConvertirTipos;

/**
 * @author Ing. Efr&eacute;n Reyes Torres
 *
 */
public class OcupacionPeticionDto {

	private ConvertirTipos ct = new ConvertirTipos();
	/**
	 * Origen del viaje
	 */
	String origen;
	
	/**
	 * Destino del viaje
	 */
	String destino;
	
	/**
	 * Fecha salida de viaje. Cadena de caracteres con el formato DDMMAAAA
	 */
	String fecha;
	
	/**
	 * Numero de corrida seleccionada para viajar
	 */
	Double corrida;
	
	/**
	 * Corrida seleccionada para viajar
	 */
	String strCorrida;
	
	/**
	 * Empresa que solicita la venta
	 */
	String empresaVende;
	
	/**
	 * Empresa donde Viaja
	 */
	String empresaViaje;

	/**
	 * @return the origen
	 */
	public String getOrigen() {
		return origen;
	}

	/**
	 * @param origen the origen to set
	 */
	public void setOrigen(String origen) {
		this.origen = origen;
	}

	/**
	 * @return the destino
	 */
	public String getDestino() {
		return destino;
	}

	/**
	 * @param destino the destino to set
	 */
	public void setDestino(String destino) {
		this.destino = destino;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the corrida
	 */
	public Double getCorrida() {
		return corrida;
	}

	/**
	 * @param corrida the corrida to set
	 */
	public void setCorrida(String corrida) {
		this.corrida = ct.getDouble(corrida);		
	}
	
	/**
	 * @param strCorrida the strCorrida to set
	 */
	public void setStrCorrida(String strCorrida) {
		this.strCorrida = ct.getString(strCorrida);		
	}
	
	/**
	 * @return the strCorrida
	 */
	public String getStrCorrida() {
		return strCorrida;
	}
	
	/**
	 * @return the empresaVende
	 */
	public String getEmpresaVende() {
		return empresaVende;
	}

	/**
	 * @param empresaVende the empresaVende to set
	 */
	public void setEmpresaVende(String empresaVende) {
		this.empresaVende = empresaVende;
	}

	/**
	 * @return the empresaViaje
	 */
	public String getEmpresaViaje() {
		return empresaViaje;
	}

	/**
	 * @param empresaViaje the empresaViaje to set
	 */
	public void setEmpresaViaje(String empresaViaje) {
		this.empresaViaje = empresaViaje;
	}
}

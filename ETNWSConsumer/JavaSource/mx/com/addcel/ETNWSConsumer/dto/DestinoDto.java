/*
 * ---------------------------------------------------------------
 * Copyright (c) 2012 Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Prolongación paseo de la reforma 61 oficina 6A2 sexto piso, colonia paseo de las lomas
 * C.P. 01330, Delegación Alvaro Obregón, México D.F.
 * Algunos Derechos Reservados
 *
 * Este software contiene informaci&oacute;n totalmente confidencial propiedad de
 * Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Queda totalmente prohibido su uso o divulgacion 
 * en forma parcial o total y solamente podr&aacute; ser utilizada de acuerdo a los 
 * t&eacute;rminos y estatutos que determine la propia empresa.
 * --------------------------------------------------------------
 */
package mx.com.addcel.ETNWSConsumer.dto;

/**
 * @author Ing. Efr&eacute;n Reyes Torres
 *
 */
public class DestinoDto {
	String claveDestino;
    String descripcionDestino;
	/**
	 * @return the claveDestino
	 */
	public String getClaveDestino() {
		return claveDestino;
	}
	/**
	 * @param claveDestino the claveDestino to set
	 */
	public void setClaveDestino(String claveDestino) {
		this.claveDestino = claveDestino;
	}
	/**
	 * @return the descripcionDestino
	 */
	public String getDescripcionDestino() {
		return descripcionDestino;
	}
	/**
	 * @param descripcionDestino the descripcionDestino to set
	 */
	public void setDescripcionDestino(String descripcionDestino) {
		this.descripcionDestino = descripcionDestino;
	}
}

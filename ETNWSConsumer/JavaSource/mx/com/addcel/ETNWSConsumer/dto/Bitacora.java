package mx.com.addcel.ETNWSConsumer.dto;

public class Bitacora {
	
	private long id;
    private long usuario;
    private long proveedor;
    private long producto;
    private String fecha;
    private String concepto;
    private float cargo;
    private String no_autorizacion;
    private int codigo_error;
    private int car_id;
    private int status;
    private String ticket;
    private String imei;
    private String destino;
    private String tarjeta_compra;
    private String tipo;
    private String software;
    private String modelo;
    private String key;
    private long pase;
    
    
	public long getPase() {
		return pase;
	}
	public void setPase(long pase) {
		this.pase = pase;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getUsuario() {
		return usuario;
	}
	public void setUsuario(long usuario) {
		this.usuario = usuario;
	}
	public long getProveedor() {
		return proveedor;
	}
	public void setProveedor(long proveedor) {
		this.proveedor = proveedor;
	}
	public long getProducto() {
		return producto;
	}
	public void setProducto(long producto) {
		this.producto = producto;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public float getCargo() {
		return cargo;
	}
	public void setCargo(float cargo) {
		this.cargo = cargo;
	}
	public String getNo_autorizacion() {
		return no_autorizacion;
	}
	public void setNo_autorizacion(String no_autorizacion) {
		this.no_autorizacion = no_autorizacion;
	}
	public int getCodigo_error() {
		return codigo_error;
	}
	public void setCodigo_error(int codigo_error) {
		this.codigo_error = codigo_error;
	}
	public int getCar_id() {
		return car_id;
	}
	public void setCar_id(int car_id) {
		this.car_id = car_id;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
	public String getTarjeta_compra() {
		return tarjeta_compra;
	}
	public void setTarjeta_compra(String tarjeta_compra) {
		this.tarjeta_compra = tarjeta_compra;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getSoftware() {
		return software;
	}
	public void setSoftware(String software) {
		this.software = software;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
    
    
}

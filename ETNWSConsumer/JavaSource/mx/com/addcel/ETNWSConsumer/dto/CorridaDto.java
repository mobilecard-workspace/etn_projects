/*
 * ---------------------------------------------------------------
 * Copyright (c) 2012 Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Prolongación paseo de la reforma 61 oficina 6A2 sexto piso, colonia paseo de las lomas
 * C.P. 01330, Delegación Alvaro Obregón, México D.F.
 * Algunos Derechos Reservados
 *
 * Este software contiene informaci&oacute;n totalmente confidencial propiedad de
 * Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Queda totalmente prohibido su uso o divulgacion 
 * en forma parcial o total y solamente podr&aacute; ser utilizada de acuerdo a los 
 * t&eacute;rminos y estatutos que determine la propia empresa.
 * --------------------------------------------------------------
 */
package mx.com.addcel.ETNWSConsumer.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Ing. Efr&eacute;n Reyes Torres
 *
 */
//@XmlRootElement(name = "Record")
public class CorridaDto {

	private String destino;
    private String tipoItinerario;
    private String horaCorrida;
    private String tipoCorrida;
    private String empresaCorrida;
    private String servicio;
    private float tarifa;
    private int dispEstudiante;
    private int dispInsen;
    private int dispMaestro;
    private int dispGeneral;
    private String corrida;
    private String sala;
    private String anden;
    private float tarifaAdulto;
    private float tarifaNino;
    private float tarifaInsen;
    private float tarifaEstudiante;
    private float tarifaMaestro;
    private String cveDestino;
    private String tipoLectura;
    private String cveCtl;
    private String secuencia;
    private String fechaDesfaza;
    private String Ubicacion;
    private String fechaInicial;
    private int cupoAutobus;
    private int errorNumero;
    private String errorCadena;
    private int dispNinos;
    private int dispPromo;
    private float tarifaPromo;
    private String fechaLlegada;
    private String moneda;
    private int dispPaisano;
    private float tarifaProgPaisano;
    private String oficinaFrontera;
    private float porcentajeTramoFrontera;
    private String detServicio;
    private int iva;
	
    public int getIva() {
		return iva;
	}
	public void setIva(int iva) {
		this.iva = iva;
	}
	//@XmlElement(name = "Destino_final")
    public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
	
	//@XmlElement(name = "Tipo_itinerario")
	public String getTipoItinerario() {
		return tipoItinerario;
	}
	public void setTipoItinerario(String tipoItinerario) {
		this.tipoItinerario = tipoItinerario;
	}
	
	//@XmlElement(name = "Hora_Corrida")
	public String getHoraCorrida() {
		return horaCorrida;
	}
	public void setHoraCorrida(String horaCorrida) {
		this.horaCorrida = horaCorrida;
	}
	
	//@XmlElement(name = "Tipo_Corrida")
	public String getTipoCorrida() {
		return tipoCorrida;
	}
	public void setTipoCorrida(String tipoCorrida) {
		this.tipoCorrida = tipoCorrida;
	}
	
	//@XmlElement(name = "Empresa_Corrida")
	public String getEmpresaCorrida() {
		return empresaCorrida;
	}
	public void setEmpresaCorrida(String empresaCorrida) {
		this.empresaCorrida = empresaCorrida;
	}
	
	//@XmlElement(name = "Servicio")
	public String getServicio() {
		return servicio;
	}
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
	
	//@XmlElement(name = "Tarifa")
	public float getTarifa() {
		return tarifa;
	}
	public void setTarifa(float tarifa) {
		this.tarifa = tarifa;
	}
	
	//@XmlElement(name = "Disp_Estudiantes")
	public int getDispEstudiante() {
		return dispEstudiante;
	}
	public void setDispEstudiante(int dispEstudiante) {
		this.dispEstudiante = dispEstudiante;
	}
	
	//@XmlElement(name = "Disp_Insen")
	public int getDispInsen() {
		return dispInsen;
	}
	public void setDispInsen(int dispInsen) {
		this.dispInsen = dispInsen;
	}
	
	//@XmlElement(name = "Disp_Maestros")
	public int getDispMaestro() {
		return dispMaestro;
	}
	public void setDispMaestro(int dispMaestro) {
		this.dispMaestro = dispMaestro;
	}
	
	//@XmlElement(name = "Disp_General")
	public int getDispGeneral() {
		return dispGeneral;
	}
	public void setDispGeneral(int dispGeneral) {
		this.dispGeneral = dispGeneral;
	}
	
	//@XmlElement(name = "Corrida")
	public String getCorrida() {
		return corrida;
	}
	public void setCorrida(String corrida) {
		this.corrida = corrida;
	}
	
	//@XmlElement(name = "Sala")
	public String getSala() {
		return sala;
	}
	public void setSala(String sala) {
		this.sala = sala;
	}
	
	//@XmlElement(name = "Anden")
	public String getAnden() {
		return anden;
	}
	public void setAnden(String anden) {
		this.anden = anden;
	}
	
	//@XmlElement(name = "Tarifa_Adulto")
	public float getTarifaAdulto() {
		return tarifaAdulto;
	}
	public void setTarifaAdulto(float tarifaAdulto) {
		this.tarifaAdulto = tarifaAdulto;
	}
	
	//@XmlElement(name = "Tarifa_Nino")
	public float getTarifaNino() {
		return tarifaNino;
	}
	public void setTarifaNino(float tarifaNino) {
		this.tarifaNino = tarifaNino;
	}
	
	//@XmlElement(name = "Tarifa_Insen")
	public float getTarifaInsen() {
		return tarifaInsen;
	}
	public void setTarifaInsen(float tarifaInsen) {
		this.tarifaInsen = tarifaInsen;
	}
	
	//@XmlElement(name = "Tarifa_Estudiante")
	public float getTarifaEstudiante() {
		return tarifaEstudiante;
	}
	public void setTarifaEstudiante(float tarifaEstudiante) {
		this.tarifaEstudiante = tarifaEstudiante;
	}
	
	//@XmlElement(name = "Tarifa_Maestro")
	public float getTarifaMaestro() {
		return tarifaMaestro;
	}
	public void setTarifaMaestro(float tarifaMaestro) {
		this.tarifaMaestro = tarifaMaestro;
	}
	
	//@XmlElement(name = "Clave_Destino")
	public String getCveDestino() {
		return cveDestino;
	}
	public void setCveDestino(String cveDestino) {
		this.cveDestino = cveDestino;
	}
	
	//@XmlElement(name = "Tipo_Lectura")
	public String getTipoLectura() {
		return tipoLectura;
	}
	public void setTipoLectura(String tipoLectura) {
		this.tipoLectura = tipoLectura;
	}
	
	//@XmlElement(name = "Clave_Ctl")
	public String getCveCtl() {
		return cveCtl;
	}
	public void setCveCtl(String cveCtl) {
		this.cveCtl = cveCtl;
	}
	
	//@XmlElement(name = "Secuencia")
	public String getSecuencia() {
		return secuencia;
	}
	public void setSecuencia(String secuencia) {
		this.secuencia = secuencia;
	}
	
	//@XmlElement(name = "Fecha_desfaza")
	public String getFechaDesfaza() {
		return fechaDesfaza;
	}
	public void setFechaDesfaza(String fechaDesfaza) {
		this.fechaDesfaza = fechaDesfaza;
	}
	
	//@XmlElement(name = "Ubicacion")
	public String getUbicacion() {
		return Ubicacion;
	}
	public void setUbicacion(String ubicacion) {
		Ubicacion = ubicacion;
	}
	
	//@XmlElement(name = "Fecha_Inicial")
	public String getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	
	//@XmlElement(name = "Cupo_Autobus")
	public int getCupoAutobus() {
		return cupoAutobus;
	}
	public void setCupoAutobus(int cupoAutobus) {
		this.cupoAutobus = cupoAutobus;
	}
	
	//@XmlElement(name = "Error_Numero")
	public int getErrorNumero() {
		return errorNumero;
	}
	public void setErrorNumero(int errorNumero) {
		this.errorNumero = errorNumero;
	}
	
	//@XmlElement(name = "Error_Cadena")
	public String getErrorCadena() {
		return errorCadena;
	}
	public void setErrorCadena(String errorCadena) {
		this.errorCadena = errorCadena;
	}
	
	//@XmlElement(name = "Disp_Ninios")
	public int getDispNinos() {
		return dispNinos;
	}
	public void setDispNinos(int dispNinos) {
		this.dispNinos = dispNinos;
	}
	
	//@XmlElement(name = "Disp_Promo")
	public int getDispPromo() {
		return dispPromo;
	}
	public void setDispPromo(int dispPromo) {
		this.dispPromo = dispPromo;
	}
	
	//@XmlElement(name = "Tarifa_Promo")
	public float getTarifaPromo() {
		return tarifaPromo;
	}
	public void setTarifaPromo(float tarifaPromo) {
		this.tarifaPromo = tarifaPromo;
	}
	
	//@XmlElement(name = "Fecha_Llegada")
	public String getFechaLlegada() {
		return fechaLlegada;
	}
	public void setFechaLlegada(String fechaLlegada) {
		this.fechaLlegada = fechaLlegada;
	}
	
	//@XmlElement(name = "Moneda")
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	
	//@XmlElement(name = "Disp_Progpaisano")
	public int getDispPaisano() {
		return dispPaisano;
	}
	public void setDispPaisano(int dispPaisano) {
		this.dispPaisano = dispPaisano;
	}
	
	//@XmlElement(name = "Oficina_Frontera")
	public String getOficinaFrontera() {
		return oficinaFrontera;
	}
	public void setOficinaFrontera(String oficinaFrontera) {
		this.oficinaFrontera = oficinaFrontera;
	}
	
	//@XmlElement(name = "Porcentaje_Tramo_Frontera")
	public float getPorcentajeTramoFrontera() {
		return porcentajeTramoFrontera;
	}
	public void setPorcentajeTramoFrontera(float porcentajeTramoFrontera) {
		this.porcentajeTramoFrontera = porcentajeTramoFrontera;
	}
	/**
	 * @return the tarifaProgPaisano
	 */
	//@XmlElement(name = "Tarifa_Progpaisano")
	public float getTarifaProgPaisano() {
		return tarifaProgPaisano;
	}
	/**
	 * @param tarifaProgPaisano the tarifaProgPaisano to set
	 */
	public void setTarifaProgPaisano(float tarifaProgPaisano) {
		this.tarifaProgPaisano = tarifaProgPaisano;
	}
	public String getDetServicio() {
		return detServicio;
	}
	public void setDetServicio(String detServicio) {
		this.detServicio = detServicio;
	}
	
	
}

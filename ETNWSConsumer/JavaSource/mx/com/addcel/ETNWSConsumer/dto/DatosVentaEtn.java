/*
 * ---------------------------------------------------------------
 * Copyright (c) 2012 Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Prolongación paseo de la reforma 61 oficina 6A2 sexto piso, colonia paseo de las lomas
 * C.P. 01330, Delegación Alvaro Obregón, México D.F.
 * Algunos Derechos Reservados
 *
 * Este software contiene informaci&oacute;n totalmente confidencial propiedad de
 * Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Queda totalmente prohibido su uso o divulgacion 
 * en forma parcial o total y solamente podr&aacute; ser utilizada de acuerdo a los 
 * t&eacute;rminos y estatutos que determine la propia empresa.
 * --------------------------------------------------------------
 */
package mx.com.addcel.ETNWSConsumer.dto;

/**
 * @author Lic. Elena L&ocute;pez L&ocute;pez
 *
 */
public class DatosVentaEtn {
	
	private String corrida;
	
	private Double reservacionEtn;
	
	private String idOrigen;

	public String getCorrida() {
		return corrida;
	}

	public void setCorrida(String corrida) {
		this.corrida = corrida;
	}

	public Double getReservacionEtn() {
		return reservacionEtn;
	}

	public void setReservacionEtn(double reservacionEtn) {
		this.reservacionEtn = reservacionEtn;
	}

	public String getIdOrigen() {
		return idOrigen;
	}

	public void setIdOrigen(String idOrigen) {
		this.idOrigen = idOrigen;
	}
	
	
	

}

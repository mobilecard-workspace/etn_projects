package mx.com.addcel.ETNWSConsumer.dto;

import java.util.ArrayList;
import java.util.List;

public class TarifasDto {
	
	private List<Float> tarifas = new ArrayList<Float>();
	private String codError;
	private String detError;
	
	public void setTarifas(List<Float> tarifas){
		this.tarifas = tarifas;				
	}
	
	public void setCodError(String codError){
		
		this.codError = codError;		
	}
	
	public void setDetError(String detError){
		this.detError = detError;
	}

	public List<Float> getTarifas(){
		return tarifas;
	}

	public String getCodError(){
		return codError;
	}
	
	public String getDetError(){
		return detError;
	}
}

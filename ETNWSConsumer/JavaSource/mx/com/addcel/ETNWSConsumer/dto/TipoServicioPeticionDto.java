package mx.com.addcel.ETNWSConsumer.dto;

public class TipoServicioPeticionDto {
	
	private String empresaViaja    = "";
	private String empresaSolicita = "";
	private String claseServicio   = "";
	
	public String getEmpresaViaja() {
		return empresaViaja;
	}
	public void setEmpresaViaja(String empresaViaja) {
		this.empresaViaja = empresaViaja;
	}
	public String getEmpresaSolicita() {
		return empresaSolicita;
	}
	public void setEmpresaSolicita(String empresaSolicita) {
		this.empresaSolicita = empresaSolicita;
	}
	public String getClaseServicio() {
		return claseServicio;
	}
	public void setClaseServicio(String claseServicio) {
		this.claseServicio = claseServicio;
	}

}

/*
 * ---------------------------------------------------------------
 * Copyright (c) 2012 Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Prolongaci�n paseo de la reforma 61 oficina 6A2 sexto piso, colonia paseo de las lomas
 * C.P. 01330, Delegaci�n Alvaro Obreg�n, M�xico D.F.
 * Algunos Derechos Reservados
 *
 * Este software contiene informaci&oacute;n totalmente confidencial propiedad de
 * Radiocomunicaciones y soluciones celulares S.A de C.V.
 * Queda totalmente prohibido su uso o divulgacion 
 * en forma parcial o total y solamente podr&aacute; ser utilizada de acuerdo a los 
 * t&eacute;rminos y estatutos que determine la propia empresa.
 * --------------------------------------------------------------
 */
package mx.com.addcel.ETNWSConsumer.dto;

/**
 * @author Ing. Efr&eacute;n Reyes Torres
 *
 */
public class ReservacionDto {

	private String idReservacion;
	
	private float importeTotal;
	
	private int numError;
	
	private String descError;
	
	private String idBitacora;

	
	public String getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(String idBitacora) {
		this.idBitacora = idBitacora;
	}

	/**
	 * @return the idReservacion
	 */
	public String getIdReservacion() {
		return idReservacion;
	}

	/**
	 * @param idReservacion the idReservacion to set
	 */
	public void setIdReservacion(String idReservacion) {
		this.idReservacion = idReservacion;
	}

	/**
	 * @return the importeTotal
	 */
	public float getImporteTotal() {
		return importeTotal;
	}

	/**
	 * @param importeTotal the importeTotal to set
	 */
	public void setImporteTotal(float importeTotal) {
		this.importeTotal = importeTotal;
	}

	/**
	 * @return the numError
	 */
	public int getNumError() {
		return numError;
	}

	/**
	 * @param numError the numError to set
	 */
	public void setNumError(int numError) {
		this.numError = numError;
	}

	/**
	 * @return the descError
	 */
	public String getDescError() {
		return descError;
	}

	/**
	 * @param descError the descError to set
	 */
	public void setDescError(String descError) {
		this.descError = descError;
	}
}

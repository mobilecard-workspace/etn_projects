package mx.com.addcel.ETNWSConsumer.view;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.addcel.ETNWSConsumer.business.ServiciosEtn;
import mx.com.addcel.ETNWSConsumer.business.ServiciosEtnImpl;
import mx.com.addcel.ETNWSConsumer.dto.ItinerarioDto;
import mx.com.addcel.ETNWSConsumer.dto.ItinerarioPeticionDto;
import mx.com.addcel.ETNWSConsumer.util.ConstantesEtn;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Servlet implementation class ConsultarItinerario
 */
//@WebServlet("/ConsultarItinerario")
public class ConsultarItinerario extends HttpServlet {
	private Logger log=Logger.getLogger(ConsultarItinerario.class);
	private static final long serialVersionUID = 1L;
	private ServiciosEtn etn = new ServiciosEtnImpl();
    private ItinerarioPeticionDto peticion = new ItinerarioPeticionDto();  
    private ConstantesEtn cte = new ConstantesEtn();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConsultarItinerario() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
	    PrintWriter out = response.getWriter();    
	    Enumeration<String> en = request.getParameterNames();
    	String element = null;
    	log.info("********************ConsultarItinerario ***********************");
    	while(en.hasMoreElements()){
    		element=(String)en.nextElement();
    		log.info(element+ " : " +request.getParameter(element));
    	}
    	log.info("********************ConsultarItinerario fin***********************");	
	       	
    	peticion.setCorrida(request.getParameter("corrida"));
    	peticion.setEmpresaSolicita(cte.CVE_EMPRESA);
    	peticion.setEmpresaViaje(cte.CVE_EMPRESA_VIAJA);
    	peticion.setFecha(request.getParameter("fecha").toString());
    	peticion.setOrigen(request.getParameter("origen").toString());
    	
    	
    	List<ItinerarioDto> itinerarios = etn.obtenerItinerario(peticion);
	    Gson gSon = new Gson();
	    String gSonItinerarios = gSon.toJson(itinerarios);
	    JsonParser parser = new JsonParser();
	    JsonArray array = parser.parse(gSonItinerarios).getAsJsonArray();	    
	    JsonObject jO = new JsonObject();
	    jO.add("listaItinerarios", array);
	    out.write(jO.toString());
	    
	    
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}

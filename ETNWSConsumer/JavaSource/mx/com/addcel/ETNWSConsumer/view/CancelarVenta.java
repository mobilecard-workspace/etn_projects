package mx.com.addcel.ETNWSConsumer.view;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mx.com.addcel.ETNWSConsumer.business.ServiciosEtn;
import mx.com.addcel.ETNWSConsumer.business.ServiciosEtnImpl;
import mx.com.addcel.ETNWSConsumer.dto.CancelacionDto;
import mx.com.addcel.ETNWSConsumer.dto.CancelacionPeticionDto;

/**
 * Servlet implementation class CancelarVenta
 */
//@WebServlet("/CancelarVenta")
public class CancelarVenta extends HttpServlet {
	private Logger log=Logger.getLogger(CancelarVenta.class);
	private static final long serialVersionUID = 1L;
	private ServiciosEtn etn = new ServiciosEtnImpl();	
	private CancelacionPeticionDto cancelaDto = new CancelacionPeticionDto();
	DateTime fechaJt = new DateTime();	
	DateTimeFormatter dfmt = DateTimeFormat.forPattern("ddMMyyyy");
	String fechaSistema = fechaJt.toString(dfmt);
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CancelarVenta() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	response.setContentType("application/json");
	    PrintWriter out = response.getWriter();
	    Enumeration en=null;
    	en=request.getParameterNames();
    	String element;
    	log.info("********************CancelarVenta ***********************");
    	while(en.hasMoreElements()){
    		element=(String)en.nextElement();
    		log.info(element+ " : " +request.getParameter(element));
    	}
    	log.info("********************CancelarVenta fin***********************");	
	    cancelaDto.setOrigen(request.getParameter("origen"));
	    cancelaDto.setCorrida(request.getParameter("corrida"));
	    cancelaDto.setFechaSalida(request.getParameter("fechaSalida"));
	    cancelaDto.setFolioVenta(request.getParameter("folioVenta"));
	    cancelaDto.setFechaContable(fechaSistema);
	    cancelaDto.setEmpresaVende("TAP");
	    cancelaDto.setEmpresaViaja("ETN");
	    
	    CancelacionDto cancela = etn.cancelarVenta(cancelaDto);
	    Gson gson = new GsonBuilder().disableHtmlEscaping().create();
	    out.println(gson.toJson(cancela));
	    
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

}

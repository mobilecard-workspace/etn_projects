package mx.com.addcel.ETNWSConsumer.view;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import mx.com.addcel.ETNWSConsumer.business.ServiciosEtn;
import mx.com.addcel.ETNWSConsumer.business.ServiciosEtnImpl;
import mx.com.addcel.ETNWSConsumer.dto.BoletosUsuario;


import com.google.gson.Gson;

/**
 * Servlet implementation class ConsultarBoleto
 */

//@WebServlet("/ConsultarBoleto")
public class ConsultarBoleto extends HttpServlet {
	private Logger log=Logger.getLogger(ConsultarBoleto.class);
	private static final long serialVersionUID = 1L;
    
	private ServiciosEtn etn = new ServiciosEtnImpl();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConsultarBoleto() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    /**
     * 
     */    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	
    	response.setContentType("application/json");
    	response.setCharacterEncoding("UTF-8");
    	
	    PrintWriter out = response.getWriter();
	    log.info("En el servlet");
	    
	    Enumeration en=null;
    	en=request.getParameterNames();
    	String element;
    	log.info("********************ConsultarBoleto ***********************");
    	while(en.hasMoreElements()){
    		element=(String)en.nextElement();
    		log.info(element+ " : " +request.getParameter(element));
    	}
    	log.info("********************ConsultarBoleto fin***********************");	
	    String modulo     = request.getParameter("modulo");
	    String idBusqueda = request.getParameter("idBusqueda");
	    
	    String respuesta = "[{\"nit\":12356,\"listaBoletos\":[{\"origen\":\"TAM\",\"destino\":\"ACA\",\"asiento\":\"01\",\"tipoAsiento\":\"AD\",\"importe\":22,\"tipoViaje\":\"L\"," +
				"\"operacion\":\"1235\",\"fecha\":\"Viernes 31/05/2013\",\"hora\":\"09:30\",\"afiliacion\":\"235698\",\"autBanco\":5689, \"nombre\":\"PEDRO PEREZ\"},{\"origen\":\"ACA\",\"destino\":\"TAM\",\"asiento\":\"01\",\"tipoAsiento\":\"AD\",\"importe\":22,\"tipoViaje\":\"L\"," +
				"\"operacion\":\"789\",\"fecha\":\"Viernes 07/06/2013\",\"hora\":\"11:30\",\"afiliacion\":\"235698\",\"autBanco\":5689, \"nombre\":\"PEDRO PEREZ\"}] }]";
	    
	    BoletosUsuario boletos = etn.obtenerBoleto(modulo, idBusqueda);	
	    log.info("aqui "+respuesta);
	    out.println(new Gson().toJson(boletos)); 
	    
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

}

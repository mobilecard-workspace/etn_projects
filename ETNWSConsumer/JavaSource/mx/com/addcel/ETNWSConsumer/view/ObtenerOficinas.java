package mx.com.addcel.ETNWSConsumer.view;

import java.io.IOException;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import mx.com.addcel.ETNWSConsumer.business.ServiciosEtn;
import mx.com.addcel.ETNWSConsumer.business.ServiciosEtnImpl;
import mx.com.addcel.ETNWSConsumer.dto.CancelacionDto;
import mx.com.addcel.ETNWSConsumer.dto.CancelacionPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.CorridaPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.DestinoPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.DiagramaPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.ItinerarioPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.LibReservaPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.OcupacionPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.ReservacionPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.TarifaPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.VentaBoletoPeticionDto;

/**
 * Servlet implementation class ObtenerOficinas
 */
//@WebServlet("/ObtenerOficinas")
public class ObtenerOficinas extends HttpServlet {
	private Logger log=Logger.getLogger(ObtenerOficinas.class);
	private static final long serialVersionUID = 1L;
	private ServiciosEtn etn = new ServiciosEtnImpl();
	//private DestinoPeticionDto  datosPeticionDestino = new DestinoPeticionDto(); 
	//private TarifaPeticionDto datosPeticionTarifa = new TarifaPeticionDto();
	//private OcupacionPeticionDto datosPeticionOcupacion = new OcupacionPeticionDto(); 
	//private DiagramaPeticionDto datosPeticionDiagrama = new DiagramaPeticionDto();
	//private ReservacionPeticionDto datosPeticionReservacion = new ReservacionPeticionDto(); 
	//private VentaBoletoPeticionDto datosPeticionVenta = new VentaBoletoPeticionDto(); 
	//private CorridaPeticionDto datosPeticionCorrida = new CorridaPeticionDto ();
	//private LibReservaPeticionDto datosPeticionLibera = new LibReservaPeticionDto();
	//private ItinerarioPeticionDto datosPeticionItinerario = new ItinerarioPeticionDto();
	private CancelacionPeticionDto datosCancelacion = new CancelacionPeticionDto(); 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ObtenerOficinas() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	
    private String obtenerOficinas() {
    	String respuesta = "";
    	
    	//etn.obtenerDestinos(datosPeticionDestino);    	
    	//etn.obtenerOficinas();     	
    	//etn.obtenerCorridas(datosPeticionCorrida);
    	//etn.obtenerTarifas(datosPeticionTarifa);
    	//etn.consultarOcupacion(datosPeticionOcupacion);
    	//etn.consultarDiagramaAutobus(datosPeticionDiagrama);
    	//etn.solicitarReservacion(datosPeticionReservacion);
    	//etn.solicitarVentaBoleto(datosPeticionVenta);
    	//etn.obtenerTipoServicios("TAP","ETN");
    	//etn.solicitaLiberaReserva(datosPeticionLibera);
    	//etn.obtenerItinerario(datosPeticionItinerario);
    	etn.cancelarVenta(datosCancelacion);
    	log.info("termine ");
    	return respuesta;
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		obtenerOficinas();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		obtenerOficinas();
	}

}

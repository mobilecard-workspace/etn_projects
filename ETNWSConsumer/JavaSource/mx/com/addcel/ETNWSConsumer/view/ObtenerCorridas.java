package mx.com.addcel.ETNWSConsumer.view;

import java.io.IOException;
import java.io.Writer;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.addcel.ETNWSConsumer.business.ServiciosEtn;
import mx.com.addcel.ETNWSConsumer.business.ServiciosEtnImpl;
import mx.com.addcel.ETNWSConsumer.dto.CorridaDto;
import mx.com.addcel.ETNWSConsumer.dto.CorridaPeticionDto;
import mx.com.addcel.ETNWSConsumer.util.ConstantesEtn;

import org.apache.axis.types.UnsignedByte;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Servlet implementation class ObtenerCorridas
 */
//@WebServlet("/ObtenerCorridas")
public class ObtenerCorridas extends HttpServlet {
	private Logger log=Logger.getLogger(ObtenerCorridas.class);
	private static final long serialVersionUID = 1L;
	private ServiciosEtn etn = new ServiciosEtnImpl();   
	CorridaPeticionDto corridaPeticion = new CorridaPeticionDto();
	DateTime fechaJt = new DateTime();
	
	DateTimeFormatter hfmt = DateTimeFormat.forPattern("HHmmss");
	DateTimeFormatter diafmt = DateTimeFormat.forPattern("dd");
	DateTimeFormatter mesfmt = DateTimeFormat.forPattern("MM");
	DateTimeFormatter aniofmt = DateTimeFormat.forPattern("yyyy");
	
	String horaInicio;		
	String horaSistema = fechaJt.toString(hfmt);
	String diaSistema = fechaJt.toString(diafmt);
	String mesSistema = fechaJt.toString(mesfmt);
	String anioSistema = fechaJt.toString(aniofmt);
	
	
	private ConstantesEtn cte = new ConstantesEtn();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ObtenerCorridas() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
	    
    	UnsignedByte uBMulti = new UnsignedByte();
    	Long multiEmpLong = 0L; //Solo corrida de la clave de empresa principal
    	uBMulti.setValue(multiEmpLong);
    	
    	response.setContentType("application/json");
    	response.setCharacterEncoding("UTF-8");
    	Writer out = response.getWriter();
    	
    	String fecha = request.getParameter("fechaIni");
    	
    	String dia = fecha.substring(0,2);
		String mes = fecha.substring(2,4);
		String anio = fecha.substring(4,8);
    	
    	int diaSis = Integer.parseInt(diaSistema);
		int mesSis = Integer.parseInt(mesSistema);
		int anioSist = Integer.parseInt(anioSistema);
		
		int diaConsulta = Integer.parseInt(dia);
		int mesConsulta = Integer.parseInt(mes);
		int anioConsulta = Integer.parseInt(anio);
		Enumeration<String> en = request.getParameterNames();
    	String element = null;
    	log.info("********************ObtenerCorridas ***********************");
    	while(en.hasMoreElements()){
    		element=(String)en.nextElement();
    		log.info(element+ " : " +request.getParameter(element));
    	}
    	log.info("********************ObtenerCorridas fin***********************");	
	      
		
		corridaPeticion.setOrigen(request.getParameter("origen"));
    	corridaPeticion.setDestino(request.getParameter("destino"));
    	corridaPeticion.setFecha(fecha);    	
    	corridaPeticion.setEmpresa(cte.CVE_EMPRESA);
    	corridaPeticion.setNumAdulto(request.getParameter("numAdulto"));
    	corridaPeticion.setNumNino(request.getParameter("numNino"));
    	corridaPeticion.setNumInsen(request.getParameter("numInsen"));
    	corridaPeticion.setNumEstudiante(request.getParameter("numEst"));
    	corridaPeticion.setNumMaestro(request.getParameter("numMto"));
    	corridaPeticion.setMultiempresa(uBMulti);
    	corridaPeticion.setNumPaisano(request.getParameter("numPaisano"));
		
		if( (anioConsulta > anioSist) || (mesConsulta > mesSis) || (diaConsulta > diaSis)){
			horaInicio = "000000";
		}else {			
			horaInicio = horaSistema;
		}
		
    	corridaPeticion.setHora(horaInicio);
    	corridaPeticion.setHoraFin("240000");
		 
	    List<CorridaDto> corridas = etn.obtenerCorridas(corridaPeticion);
	    Gson gSon = new Gson();
	    String gSonCorrida = gSon.toJson(corridas);
	    JsonParser parser = new JsonParser();
	    JsonArray array = parser.parse(gSonCorrida).getAsJsonArray();	    
	    JsonObject jO = new JsonObject();
	    jO.add("listaCorridas", array);
	    out.write(jO.toString());
    	
    }
    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

}

package mx.com.addcel.ETNWSConsumer.view;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.addcel.ETNWSConsumer.business.ServiciosEtn;
import mx.com.addcel.ETNWSConsumer.business.ServiciosEtnImpl;
import mx.com.addcel.ETNWSConsumer.dto.BitacoraPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.ReservacionDto;
import mx.com.addcel.ETNWSConsumer.dto.ReservacionPeticionDto;
import mx.com.addcel.ETNWSConsumer.util.ConstantesEtn;
import mx.com.addcel.ETNWSConsumer.util.StringUtils;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Servlet implementation class ConsultarReservacion
 */
//@WebServlet("/ConsultarReservacion")
public class ConsultarReservacion extends HttpServlet {
	private Logger log=Logger.getLogger(ConsultarReservacion.class);
	private static final long serialVersionUID = 1L;
	private ServiciosEtn etn = new ServiciosEtnImpl();
	private ReservacionPeticionDto datosReserva = new ReservacionPeticionDto();
	private BitacoraPeticionDto bitacora = new BitacoraPeticionDto(); 
	private ConstantesEtn cte = new ConstantesEtn();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConsultarReservacion() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	
    	response.setContentType("application/json");
    	response.setCharacterEncoding("UTF-8");
	    PrintWriter out = response.getWriter();
	    log.info("En el servlet");
	    
	    String asientoStr="";
//	    Date dFecha=null;
	    String nombres   = request.getParameter("nombres");
	    String nombPipe  = request.getParameter("nombres");
	    Enumeration<String> en = request.getParameterNames();
    	String element = null;
    	log.info("********************ConsultarReservacion ***********************");
    	while(en.hasMoreElements()){
    		element=(String)en.nextElement();
    		log.info(element+ " : " +request.getParameter(element));
    	}
    	log.info("********************ConsultarReservacion fin***********************");	
	      
	    if (nombres != null) {
	    	String [] listaNombres = nombres.split("\\,");
	    	log.info("Tamaño lista nombres en servlet: "+listaNombres.length);
		    nombres = "";		    
		    for (String nombre: listaNombres) {
		    	if(nombre.length() > 35){
		    		nombres += nombre.substring(0, 35);
		    	}else if(nombre.length() == 35){
		    		nombres += nombre;
		    	}else if(nombre.length() < 35){
		    		nombres += StringUtils.rightPad(nombre, 35, " ");
		    	}
		    }
	    } else {
	    	nombres = "Default                           ";
	    }
	    
//	    String nomb="";
	    
	    if(nombPipe != null){
	    	String [] listaNombres = nombPipe.split("\\,");
	    	log.info("Tamaño lista nombres con comas en servlet: "+listaNombres.length);
	    	nombPipe = "";		    
		    for (String nombre: listaNombres) {
		    	if(nombre.length() > 35){
		    		nombPipe += nombre.substring(0, 35) + ",";
		    	}else if(nombre.length() == 35){
		    		nombPipe += nombre + ",";
		    	}else if(nombre.length() < 35){
		    		nombPipe += StringUtils.rightPad(nombre, 35, " ") + ",";
		    	}
		    }
	    } else {
	    	nombPipe = "Default                           ,";
	    }
	    
	    log.info("nombres: "+nombres);
	    log.info("nombres comas: "+nombPipe);
	    
	    String asientos = request.getParameter("asientos");
//	    String asientosCorregidos="";
	    
	    if(asientos != null){
	    	String [] listaAsientos = asientos.split("\\|");
	    	log.info("Tamaño lista asientos en servlet: "+listaAsientos.length);
	    	for (int i = 0; i<listaAsientos.length; i++) {
	    		String asientoTmp=listaAsientos[i].trim();
	    		if(asientoTmp.length()==2){
	    			log.info("LLega asiento defectuoso, arreglo el problema");
	    			log.info("Original : "+ asientoTmp);
	    			log.info("Arreglado : "+ "0"+asientoTmp);
	    			asientoStr = asientoStr +"0"+asientoTmp;  
	    		}else{
	    			asientoStr = asientoStr + asientoTmp;  
	    		}
		    }
	    }
	    
	    log.info("asiento Pipe: "+asientos);
	    log.info("asiento limpio: "+asientoStr);
	    
	    log.info("nombres limpio: "+nombres);
	    log.info("nomComas: "+nombPipe);
	    
	    if(request.getParameter("idPago") != null){
	    	datosReserva.setIdPago(request.getParameter("idPago"));
	    }
	    
	    bitacora.setIdUsuario(request.getParameter("idUsuario"));
	    bitacora.setImei(request.getParameter("imei"));
	    bitacora.setModelo(request.getParameter("modelo"));
	    bitacora.setSoftware(request.getParameter("software"));	    
	    bitacora.setTelefono(request.getParameter("telefono"));
	    bitacora.setTarjeta(request.getParameter("tarjeta")); //******** verificar la tarjeta
	    bitacora.setTipoProducto("Reservacion de Boletos de Autobus " + request.getParameter("tipoProducto"));
	    bitacora.setProducto("12");
	    bitacora.setTipoTelefono(request.getParameter("tipoTelefono"));
	    bitacora.setTipoViaje(request.getParameter("tipoViaje"));
	    bitacora.setWkey(request.getParameter("wkey"));
	    bitacora.setProveedor("12");
	    bitacora.setAutorizaBanco("0");	    
	    
	    datosReserva.setOrigen(request.getParameter("origen"));
	    datosReserva.setDestino(request.getParameter("destino"));
	    datosReserva.setIdDestino(request.getParameter("idDestino"));
	    datosReserva.setIdOrigen(request.getParameter("idOrigen"));
	    datosReserva.setFecha(request.getParameter("fecha"));
	    datosReserva.setHora(request.getParameter("hora"));
	    datosReserva.setNumAdulto(request.getParameter("numAdulto"));
	    datosReserva.setNumNino(request.getParameter("numNino"));
	    datosReserva.setNumInsen(request.getParameter("numInsen"));
	    datosReserva.setNumEstudiante(request.getParameter("numEstudiante"));
	    datosReserva.setNumMaestro(request.getParameter("numMaestro"));
	    datosReserva.setCorrida(request.getParameter("corrida"));
	    datosReserva.setTipoCorrida(request.getParameter("tipoCorrida"));
	    datosReserva.setTarifaAdulto(Double.parseDouble(request.getParameter("tarifaAdulto")));
	    datosReserva.setTarifaNino(Double.parseDouble(request.getParameter("tarifaNino")));
	    datosReserva.setTarifaInsen(Double.parseDouble(request.getParameter("tarifaInsen")));
	    datosReserva.setTarifaEstudiante(Double.parseDouble(request.getParameter("tarifaEstudiante")));
	    datosReserva.setTarifaMaestro(Double.parseDouble(request.getParameter("tarifaMaestro")));
	    datosReserva.setIdViaje(Long.parseLong(request.getParameter("idViaje")));
	    datosReserva.setAsientos(asientoStr);
	    datosReserva.setNombres(nombres);	    
	    datosReserva.setEmpresaVende(cte.CVE_EMPRESA);
	    datosReserva.setEmpresaViaje(cte.CVE_EMPRESA_VIAJA);
	    datosReserva.setTipoTerminal("");
	    datosReserva.setTipoCliente("");
	    datosReserva.setTipoOperacion("");
	   	    
	    log.info("antes de entrar solicitarReservacion");
	    ReservacionDto reserva = etn.solicitarReservacion(datosReserva,bitacora,asientos,nombPipe);
	    Gson gson = new GsonBuilder().disableHtmlEscaping().create();
	    
	    out.println(gson.toJson(reserva)); 
	    
    }
    
    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

}

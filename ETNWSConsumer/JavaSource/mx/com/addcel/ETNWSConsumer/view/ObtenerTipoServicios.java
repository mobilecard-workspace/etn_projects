package mx.com.addcel.ETNWSConsumer.view;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import mx.com.addcel.ETNWSConsumer.business.ServiciosEtn;
import mx.com.addcel.ETNWSConsumer.business.ServiciosEtnImpl;
import mx.com.addcel.ETNWSConsumer.dto.TipoServicioDto;
import mx.com.addcel.ETNWSConsumer.dto.TipoServicioPeticionDto;
import mx.com.addcel.ETNWSConsumer.util.ConstantesEtn;

/**
 * Servlet implementation class ObtenerTipoServicios
 */
//@WebServlet("/ObtenerTipoServicios")
public class ObtenerTipoServicios extends HttpServlet {
	private Logger log=Logger.getLogger(ObtenerTipoServicios.class);
	private static final long serialVersionUID = 1L;
	private ServiciosEtn etn = new ServiciosEtnImpl();
	private TipoServicioPeticionDto datosServ = new TipoServicioPeticionDto();
	private ConstantesEtn cte = new ConstantesEtn();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ObtenerTipoServicios() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	
    	response.setContentType("application/json");
    	response.setCharacterEncoding("UTF-8");
	    PrintWriter out = response.getWriter();
	    Enumeration en=null;
    	en=request.getParameterNames();
    	String element;
    	log.info("********************ObtenerTipoServicios ***********************");
    	while(en.hasMoreElements()){
    		element=(String)en.nextElement();
    		log.info(element+ " : " +request.getParameter(element));
    	}
    	log.info("********************ObtenerTipoServicios fin***********************");	
	    datosServ.setEmpresaSolicita(cte.CVE_EMPRESA);
	    datosServ.setEmpresaViaja(cte.CVE_EMPRESA_VIAJA);
	    
	    List<TipoServicioDto> tiposServ = etn.obtenerTipoServicios(datosServ);
	    Gson gSon = new Gson();
	    String gSonServicios = gSon.toJson(tiposServ);
	    JsonParser parser = new JsonParser();
	    JsonArray array = parser.parse(gSonServicios).getAsJsonArray();	    
	    JsonObject jO = new JsonObject();
	    jO.add("listaServicios", array);
	    out.write(jO.toString());
	  
    }
    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

}

package mx.com.addcel.ETNWSConsumer.view;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.addcel.ETNWSConsumer.business.ServiciosEtn;
import mx.com.addcel.ETNWSConsumer.business.ServiciosEtnImpl;
import mx.com.addcel.ETNWSConsumer.dto.VentaBoletoDto;
import mx.com.addcel.ETNWSConsumer.dto.VentaBoletoPeticionDto;
import mx.com.addcel.ETNWSConsumer.util.ConstantesEtn;
import mx.com.addcel.ETNWSConsumer.util.ConvertirTipos;

import org.apache.axis.types.UnsignedLong;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Servlet implementation class ConfirmacionVenta
 */
//@WebServlet("/ConfirmacionVenta")
public class ConfirmacionVenta extends HttpServlet {
	private Logger log=Logger.getLogger(ConfirmacionVenta.class);
	private static final long serialVersionUID = 1L;
	//private ServiciosEtn etn = new ServiciosEtnImpl();
	private ConstantesEtn cte = new ConstantesEtn();
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ConfirmacionVenta() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void venta(HttpServletRequest request, HttpServletResponse response) {
    	response.setContentType("application/json");
    	VentaBoletoPeticionDto peticion = new VentaBoletoPeticionDto();
    	VentaBoletoDto respuesta = new VentaBoletoDto();
    	Gson gson = new GsonBuilder().disableHtmlEscaping().create();    	
    	ServiciosEtn etn = new ServiciosEtnImpl();
    	DateTime fechaJt = new DateTime();
    	DateTimeFormatter dfmt = DateTimeFormat.forPattern("ddMMyyyy");
    	String fecha = fechaJt.toString(dfmt);
    	ConvertirTipos tipos = new ConvertirTipos();
    	String autorizaBanco = null;
//    	long idViaje = 0;
    	Enumeration<String> en = request.getParameterNames();
    	String element;
    	log.info("********************ConfirmacionVenta ***********************");
    	while(en.hasMoreElements()){
    		element=(String)en.nextElement();
    		log.info(element+ " : " +request.getParameter(element));
    	}
    	log.info("********************ConfirmacionVenta fin***********************");
    	
	    try {
	    	
	    	autorizaBanco = request.getParameter("autorizacion");
	    	//peticion.setCorrida(new UnsignedInt(request.getParameter("corrida").toString()));
	    	peticion.setSuccess(request.getParameter("success"));
	    	peticion.setIdViaje(tipos.getLong(request.getParameter("idViaje")));
	    	peticion.setMensaje(request.getParameter("mensaje"));
	    	peticion.setTarjeta(request.getParameter("tarjeta"));
	    	peticion.setMonto(request.getParameter("monto"));
	    	peticion.setEbFormasPagoTemp("0");
	    	peticion.setEmpresaVende(cte.CVE_EMPRESA);
	    	peticion.setEmpresaViaje(cte.CVE_EMPRESA_VIAJA);
	    	peticion.setFecha(fecha);
	    	peticion.setFechaContable(fecha);
	    	//peticion.setFolioReservacion(Double.parseDouble(request.getParameter("folioReservacion").toString()));
	    	peticion.setFormasPagoExternas("TB");
	    	peticion.setNumSesion(new UnsignedLong(0));
	    	peticion.setOficinaExterna("0");
	    	//peticion.setOrigen(request.getParameter("origen").toString());
	    	peticion.setSucursalExterna(new UnsignedLong(0));
	    	peticion.setTipoCliente("W");
	    	peticion.setTipoTerminal("I"); 
	    	peticion.setIva(request.getParameter("iva"));
	    	respuesta = etn.solicitarVentaBoleto(peticion,autorizaBanco);
	    	PrintWriter out = response.getWriter();
	    	out.print(gson.toJson(respuesta));
		} catch (IOException e) {
			log.error("IOException : ",e);
		} 
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		venta(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		venta(request, response);
	}

}

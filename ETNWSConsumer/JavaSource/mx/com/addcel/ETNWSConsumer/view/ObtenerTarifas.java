package mx.com.addcel.ETNWSConsumer.view;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.axis.types.UnsignedByte;
import org.apache.log4j.Logger;

import com.google.gson.Gson;

import mx.com.addcel.ETNWSConsumer.business.ServiciosEtn;
import mx.com.addcel.ETNWSConsumer.business.ServiciosEtnImpl;
import mx.com.addcel.ETNWSConsumer.dto.TarifaPeticionDto;
import mx.com.addcel.ETNWSConsumer.dto.TarifasDto;
import mx.com.addcel.ETNWSConsumer.util.ConstantesEtn;

/**
 * Servlet implementation class ObtenerTarifas
 */
//@WebServlet("/ObtenerTarifas")
public class ObtenerTarifas extends HttpServlet {
	private Logger log=Logger.getLogger(ObtenerTarifas.class);
	private static final long serialVersionUID = 1L;
	private ServiciosEtn etn = new ServiciosEtnImpl();
	private TarifaPeticionDto datosObtenTarifas = new TarifaPeticionDto(); 
	private ConstantesEtn cte = new ConstantesEtn();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ObtenerTarifas() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	
    	response.setContentType("application/json");
    	response.setCharacterEncoding("UTF-8");
	    PrintWriter out = response.getWriter();
	    Enumeration en=null;
    	en=request.getParameterNames();
    	String element;
    	log.info("********************ObtenerTarifas ***********************");
    	while(en.hasMoreElements()){
    		element=(String)en.nextElement();
    		log.info(element+ " : " +request.getParameter(element));
    	}
    	log.info("********************ObtenerTarifas fin***********************");	
	    UnsignedByte uBtipoServ = new UnsignedByte();
    	Long tipoServLong = 2L; //Tipo servicio clave de primera
    	uBtipoServ.setValue(tipoServLong);
	    
	    datosObtenTarifas.setEmpresaVende(cte.CVE_EMPRESA);
	    datosObtenTarifas.setOrigen(request.getParameter("origen"));
	    datosObtenTarifas.setDestino(request.getParameter("destino"));
	    datosObtenTarifas.setTipoServicio(uBtipoServ);
	    datosObtenTarifas.setEmpresaViaje(cte.CVE_EMPRESA_VIAJA);
	    	    
	    TarifasDto tarifas = etn.obtenerTarifas(datosObtenTarifas);
	    out.println(new Gson().toJson(tarifas));
    }
    
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

}

package mx.com.addcel.ETNWSConsumer.view;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import mx.com.addcel.ETNWSConsumer.business.ServiciosEtn;
import mx.com.addcel.ETNWSConsumer.business.ServiciosEtnImpl;
import mx.com.addcel.ETNWSConsumer.dto.LibReservaPeticionDto;
import mx.com.addcel.ETNWSConsumer.util.ConstantesEtn;

/**
 * Servlet implementation class ObtenerLibReservacion
 */
//@WebServlet("/ObtenerLibReservacion")
public class ObtenerLibReservacion extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Logger log=Logger.getLogger(ObtenerLibReservacion.class);
	private ServiciosEtn etn = new ServiciosEtnImpl();
	private LibReservaPeticionDto libResPetDto = new LibReservaPeticionDto(); 
	private ConstantesEtn cte = new ConstantesEtn();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ObtenerLibReservacion() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	
    	response.setContentType("application/json");
	    PrintWriter out = response.getWriter();
	    Enumeration en=null;
    	en=request.getParameterNames();
    	String element;
    	log.info("********************ObtenerLibReservacion ***********************");
    	while(en.hasMoreElements()){
    		element=(String)en.nextElement();
    		log.info(element+ " : " +request.getParameter(element));
    	}
    	log.info("********************ObtenerLibReservacion fin***********************");	
	    libResPetDto.setFolioReservacion(request.getParameter("folio"));
	    libResPetDto.setEmpresaVende(cte.CVE_EMPRESA);
	    libResPetDto.setEmpresaViaja(cte.CVE_EMPRESA_VIAJA);
	    
	    Boolean libera = etn.solicitaLiberaReserva(libResPetDto);
	    out.println(new Gson().toJson(libera));
	    
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

}

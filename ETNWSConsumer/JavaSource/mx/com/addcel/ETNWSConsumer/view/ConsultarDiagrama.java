package mx.com.addcel.ETNWSConsumer.view;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import mx.com.addcel.ETNWSConsumer.business.ServiciosEtn;
import mx.com.addcel.ETNWSConsumer.business.ServiciosEtnImpl;
import mx.com.addcel.ETNWSConsumer.dto.DiagramaDto;
import mx.com.addcel.ETNWSConsumer.dto.DiagramaPeticionDto;
import mx.com.addcel.ETNWSConsumer.util.ConstantesEtn;

/**
 * Servlet implementation class ConsultarDiagrama
 */
//@WebServlet("/ConsultarDiagrama")
public class ConsultarDiagrama extends HttpServlet {
	private Logger log=Logger.getLogger(ConsultarDiagrama.class);
	private static final long serialVersionUID = 1L;
	private ServiciosEtn etn = new ServiciosEtnImpl();	
	private DiagramaPeticionDto datosDiagrama = new DiagramaPeticionDto();
	private ConstantesEtn cte = new ConstantesEtn();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConsultarDiagrama() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	
    	response.setContentType("application/json");
	    PrintWriter out = response.getWriter();
	    Enumeration en=null;
    	en=request.getParameterNames();
    	String element;
    	log.info("********************ConsultarDiagrama ***********************");
    	while(en.hasMoreElements()){
    		element=(String)en.nextElement();
    		log.info(element+ " : " +request.getParameter(element));
    	}
    	log.info("********************ConsultarDiagrama fin***********************");	
	   
	    	    
	    datosDiagrama.setEmpresaVende(cte.CVE_EMPRESA);
	    datosDiagrama.setCorrida(request.getParameter("corrida"));
	    datosDiagrama.setFecha(request.getParameter("fecha"));
	    datosDiagrama.setEmpresaViaje(cte.CVE_EMPRESA_VIAJA);
	    
	    DiagramaDto diagrama = etn.consultarDiagramaAutobus(datosDiagrama);
	    out.println(new Gson().toJson(diagrama));
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

}

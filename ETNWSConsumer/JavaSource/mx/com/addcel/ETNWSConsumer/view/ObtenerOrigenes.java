package mx.com.addcel.ETNWSConsumer.view;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.addcel.ETNWSConsumer.business.ServiciosEtn;
import mx.com.addcel.ETNWSConsumer.business.ServiciosEtnImpl;
import mx.com.addcel.ETNWSConsumer.dto.OficinaDto;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


/**
 * Servlet implementation class ObtenerOrigenes
 */
//@WebServlet("/ObtenerOrigenes")
public class ObtenerOrigenes extends HttpServlet {
	private Logger log=Logger.getLogger(ObtenerOrigenes.class);
	private static final long serialVersionUID = 1L;
	private ServiciosEtn etn = new ServiciosEtnImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ObtenerOrigenes() {
        super();
        // TODO Auto-generated constructor stub
    }

		
	 protected void processRequest(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
		 
		 // De momento no se obtiene nada
		//String empSolicitaVta = request.getParameter("empSolicitaVta");
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
	    PrintWriter out = response.getWriter();
	    Enumeration<String> en = request.getParameterNames();
    	String element = null;
//    	log.info("********************ObtenerOrigenes ***********************");
    	while(en.hasMoreElements()){
    		element=(String)en.nextElement();
    		log.info(element+ " : " +request.getParameter(element));
    	}
//    	log.info("********************ObtenerOrigenes fin***********************");	
	    List<OficinaDto> oficinas = etn.obtenerOficinas();
	    Gson gSon = new Gson();
	    String gSonOficina = gSon.toJson(oficinas);
	    JsonParser parser  = new JsonParser();
	    JsonArray array    = parser.parse(gSonOficina).getAsJsonArray();	    
	    JsonObject jO = new JsonObject();
	    jO.add("lista", array);
	    out.write(jO.toString());
	     
	 }
    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
		
	}

}

package mx.com.addcel.ETNWSConsumer.view;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.addcel.ETNWSConsumer.business.ServiciosEtn;
import mx.com.addcel.ETNWSConsumer.business.ServiciosEtnImpl;
import mx.com.addcel.ETNWSConsumer.dto.DestinoDto;
import mx.com.addcel.ETNWSConsumer.dto.DestinoPeticionDto;
import mx.com.addcel.ETNWSConsumer.util.ConstantesEtn;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Servlet implementation class ObtenerDestinos
 */
//@WebServlet("/ObtenerDestinos")
public class ObtenerDestinos extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Logger log=Logger.getLogger(ObtenerDestinos.class);
	private DestinoPeticionDto datosOrigen = new DestinoPeticionDto();
	private ServiciosEtn etn = new ServiciosEtnImpl();   
	private ConstantesEtn cte = new ConstantesEtn();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ObtenerDestinos() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
	 

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
	    PrintWriter out = response.getWriter();
	    Enumeration<String> en = request.getParameterNames();
    	String element = null;
//    	log.info("********************ObtenerDestinos ***********************");
    	while(en.hasMoreElements()){
    		element=(String)en.nextElement();
    		log.info(element+ " : " +request.getParameter(element));
    	}
//    	log.info("********************ObtenerDestinos fin***********************");	
		datosOrigen.setOficinaOrigen(request.getParameter("origen"));
		datosOrigen.setEmpresaSolicita(cte.CVE_EMPRESA);
		datosOrigen.setEmpresaViaja(cte.CVE_EMPRESA_VIAJA);
		 
	    List<DestinoDto> destinos = etn.obtenerDestinos(datosOrigen);
	    Gson gSon = new Gson();
	    String gSonDestinos = gSon.toJson(destinos);
	    JsonParser parser = new JsonParser();
	    JsonArray array = parser.parse(gSonDestinos).getAsJsonArray();	    
	    JsonObject jO = new JsonObject();
	    jO.add("listaDestinos", array);
	    out.write(jO.toString()); 
	    	     
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

}

package mx.com.addcel.ETNWSConsumer.view;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import mx.com.addcel.ETNWSConsumer.business.ServiciosEtn;
import mx.com.addcel.ETNWSConsumer.business.ServiciosEtnImpl;
import mx.com.addcel.ETNWSConsumer.dto.OcupacionDto;
import mx.com.addcel.ETNWSConsumer.dto.OcupacionPeticionDto;
import mx.com.addcel.ETNWSConsumer.util.ConstantesEtn;

/**
 * Servlet implementation class ConsultarOcupacion
 */
//@WebServlet("/ConsultarOcupacion")
public class ConsultarOcupacion extends HttpServlet {
	private Logger log=Logger.getLogger(ConsultarOcupacion.class);
	private static final long serialVersionUID = 1L;
	private ServiciosEtn etn = new ServiciosEtnImpl();
	private OcupacionPeticionDto datosEnt = new OcupacionPeticionDto();
	private ConstantesEtn cte = new ConstantesEtn();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConsultarOcupacion() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	
    	response.setContentType("application/json");
	    PrintWriter out = response.getWriter();
	    Enumeration en=null;
    	en=request.getParameterNames();
    	String element;
    	log.info("********************ConsultarOcupacion ***********************");
    	while(en.hasMoreElements()){
    		element=(String)en.nextElement();
    		log.info(element+ " : " +request.getParameter(element));
    	}
    	log.info("********************ConsultarOcupacion fin***********************");	
	      
		datosEnt.setOrigen(request.getParameter("origen"));
	    datosEnt.setDestino(request.getParameter("destino"));
	    datosEnt.setFecha(request.getParameter("fecha"));
	    datosEnt.setCorrida(request.getParameter("corrida"));
	    datosEnt.setStrCorrida(request.getParameter("corrida"));
	    datosEnt.setEmpresaVende(cte.CVE_EMPRESA);
	    datosEnt.setEmpresaViaje(cte.CVE_EMPRESA_VIAJA);
	    
	    OcupacionDto ocupados = etn.consultarOcupacion(datosEnt);
	    out.println(new Gson().toJson(ocupados));
	}
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

}

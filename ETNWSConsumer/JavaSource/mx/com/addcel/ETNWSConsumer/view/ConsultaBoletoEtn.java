package mx.com.addcel.ETNWSConsumer.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Enumeration;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;


/**
 * Servlet implementation class ConsultaBoletoEtn
 */
public class ConsultaBoletoEtn extends HttpServlet {
	private Logger log=Logger.getLogger(ConsultaBoletoEtn.class);
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConsultaBoletoEtn() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String modulo = request.getParameter("modulo");
        String idBusqueda = request.getParameter("idBusqueda");
        response.setContentType("text/json;charset=UTF-8");
        
        PrintWriter out = response.getWriter();
        Enumeration<String> en = request.getParameterNames();
    	String element = null;
    	log.info("********************ConsultaBoletoEtn ***********************");
    	while(en.hasMoreElements()){
    		element=(String)en.nextElement();
    		log.info(element+ " : " +request.getParameter(element));
    	}
    	log.info("********************ConsultaBoletoEtn fin***********************");	
	    
        try {
        	
            /* TODO output your page here. You may use following sample code. */
        	String cadenaJson=conexionURLRemota(modulo, idBusqueda);
            out.println(cadenaJson);            
        } finally {            
            out.close();
        }
    }
    
    private String conexionURLRemota(String modulo,String idBusqueda){
        String response="";        
        try {
            //Cambiar url
        
            URL url=new URL("http://localhost:8080/AddCelBridge/ConsultaServlet");
            HttpURLConnection con=(HttpURLConnection)url.openConnection(); 
            //Se indica que se escribira en la conexión
            con.setDoOutput(true);           
            con.setRequestMethod("POST");
            OutputStreamWriter wr=new OutputStreamWriter(con.getOutputStream());
            String parametros="modulo="+URLEncoder.encode(modulo, "UTF-8")+"&idBusqueda="+URLEncoder.encode(idBusqueda, "UTF-8");           
            wr.write(parametros);
            wr.close();
            //.............................
            

            BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuilder sb=new StringBuilder();
            while((response=br.readLine()) !=null){               
                sb.append(response);
            }
            response=sb.toString();
            
        } catch (MalformedURLException ex) {
        	log.error("MalformedURLException : ",ex);
        } catch (IOException ex) {
        	log.error("IOException : ", ex);
        }
        
         return response;
        
    }
    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

}

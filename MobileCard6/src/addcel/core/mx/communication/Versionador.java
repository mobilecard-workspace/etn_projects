package addcel.core.mx.communication;


import java.util.Vector;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import net.rim.device.api.system.Application;

import mx.com.ib.app.MainClass;
import mx.com.ib.connection.Communications;
import mx.com.ib.connection.HttpListener;
import mx.com.ib.utils.Utils;
import mx.com.ib.views.WelcomeScreen;

public class Versionador extends Thread implements HttpListener {

	
	private WelcomeScreen welcomeScreen;
	private String url = MainClass.URL_VERSIONADOR;
	
	
	public Versionador(WelcomeScreen welcomeScreen){
		
		this.welcomeScreen = welcomeScreen;
	}
	
	
	public void run(){
		connect();
	}
	
	public void connect(){
		Application.getApplication();
		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.start();
		}
		
		if(MainClass.idealConnection != null){
			if(this.url != null){
				try{
					Communications.sendHttpRequest(this,this.url);
				}catch(Throwable t){
					//getMessageError();
				}
			}
		}
		else{
			Utils.checkConnectionType();
			this.connect();
		}
	}
	
	public void receiveHttpResponse(int appCode, byte[] response) {

		System.out.println(new String(response));
		
		String json = new String(response);
		
		JSONObject jsonObject;
		try {
			
			jsonObject = new JSONObject(json);
			welcomeScreen.addFields(jsonObject);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		
		
		
	}

	public void handleHttpError(int errorCode, String error) {
		// TODO Auto-generated method stub
		
	}

	public void receiveEstatus(String msg) {
		// TODO Auto-generated method stub
		
	}

	public void receiveHeaders(Vector _headers) {
		// TODO Auto-generated method stub
		
	}

	public boolean isDestroyed() {
		// TODO Auto-generated method stub
		return false;
	}

}

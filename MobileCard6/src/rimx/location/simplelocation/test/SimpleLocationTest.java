package rimx.location.simplelocation.test;

import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.MainScreen;

public class SimpleLocationTest extends UiApplication{
	MainScreen testScreen;
	
	public SimpleLocationTest(){
		testScreen = new TestScreen();
		pushScreen(testScreen);
	}
	
	public static void main(String[] args){
		SimpleLocationTest app = new SimpleLocationTest();		
		app.enterEventDispatcher();
	}
}

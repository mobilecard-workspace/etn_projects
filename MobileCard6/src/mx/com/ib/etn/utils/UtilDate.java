package mx.com.ib.etn.utils;

import java.util.Calendar;
import java.util.Date;

public class UtilDate {

	
	public static String getDD_MM_AAAAtoDDMMAAAA(String date){
		
		/*
				DD/MM/AAAA a DDMMAAAA 
		*/
		
		String day = date.substring(0, 2);
		String month = date.substring(3, 5);
		String year = date.substring(6, 10);
		
		
		return day + month + year;
	}
	

	public static String getAAAAMMDDtoDD_MM_AAAA(String date){
		
		/*
				AAAAMMDD a DD/MM/AAAA 
		*/
		String year = date.substring(0, 4);
		String month = date.substring(4, 6);
		String day = date.substring(6, 8);
		
		return day + "/" + month + "/" + year;
	}
	
	
	
	
	public static String getDateToDDMMAAAA(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		int iYear = calendar.get(Calendar.YEAR);
		int iMonth = calendar.get(Calendar.MONTH);
		int iDay = calendar.get(Calendar.DAY_OF_MONTH);

		String sYear = Integer.toString(iYear);
		String sMonth = Integer.toString(iMonth + 1);
		String sDay = Integer.toString(iDay);

		sMonth = (sMonth.length() == 1) ? "0" + sMonth : sMonth;
		sDay = (sDay.length() == 1) ? "0" + sDay : sDay;
		
		return sDay + sMonth + sYear;
	}
	
	
	public static boolean after(Date depart, Date arrive){

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(depart);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		Date a = calendar.getTime();
		
		calendar.setTime(arrive);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Date b = calendar.getTime();
		
		long la = a.getTime();
		long lb = b.getTime();
		
		boolean value = (la <= lb); 
		
		return value;
		
/*
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(depart);

		int departYear = calendar.get(Calendar.YEAR);
		int departMonth = calendar.get(Calendar.MONTH);
		int departDay = calendar.get(Calendar.DAY_OF_MONTH);

		
		calendar.setTime(arrive);

		int arriveYear = calendar.get(Calendar.YEAR);
		int arriveMonth = calendar.get(Calendar.MONTH);
		int arriveDay = calendar.get(Calendar.DAY_OF_MONTH);
		
		boolean value = false;
		
		
		
		if (departYear > arriveYear){
			return true;
		}
			
		
		}
*/
	}
	
}

package mx.com.ib.etn.utils;

import net.rim.device.api.ui.Color;

public class UtilColor {

	
	public final static int BACKGROUND = 0x414042;
	
	public final static int TITLE_STRING_BLACK = Color.WHITE;
	public final static int TITLE_STRING = Color.WHITE;
	public final static int TITLE_BACKGROUND = 0xF16430; 

	public final static int BUTTON_FOCUS = 0x5a9eff;
	public final static int BUTTON_FOCUS_STRING = Color.WHITE;
	public final static int BUTTON_SELECTED = 0xF16430;
	public final static int BUTTON_UNSELECTED = 0x848284;

	public final static int ELEMENT_BACKGROUND = 0x414042;
	public final static int ELEMENT_FOCUS = 0x313431;
	public final static int ELEMENT_STRING_TITLE = 0xF16430;
	public final static int ELEMENT_STRING_DATA = Color.WHITE;

/*
	public static int BG_GRAY = 0x414042;
	public static int BG_ORANGE = 0xF16430;
	public static int COLOR_LINK = 0x38D4FF;
*/

	public static int toRGB(int r, int g, int b){
		return (r<<16)+(g<<8)+b;		 
	}

	public static int getBlack(){
		return toRGB(30, 30, 30);
	}

	public static int getDarkGray(){
		return toRGB(120, 120, 120);
	}

	public static int getLightGray(){
		return toRGB(200, 200, 200);
	}

	public static int getLightOrange(){
		return toRGB(255, 184, 101);
	}

	public static int getDarkOrange(){
		return toRGB(255, 159, 45);
	}
}

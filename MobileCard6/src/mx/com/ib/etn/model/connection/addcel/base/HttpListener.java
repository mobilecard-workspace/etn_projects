/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.com.ib.etn.model.connection.addcel.base;

import java.util.Vector;

/**
 *
 * @author Administrador
 */

public interface HttpListener {
    
    public void receiveHttpResponse(int appCode, byte[] response);
    public void handleHttpError(int errorCode,String error);   
    public void receiveEstatus(String msg);
    public void receiveHeaders(Vector _headers);
    public boolean isDestroyed();
}

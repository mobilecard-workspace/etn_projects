package mx.com.ib.etn.model.connection;

public class Url {

	
	
	public static String URL_LOGIN = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_userLogin.jsp";
	public static String URL_PURCHASE = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_purchase.jsp";
	public static String URL_GET_BANKS = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getBanks.jsp";
	public static String URL_TERMS = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getConditions.jsp";
	public static String URL_USER_UPDATE = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_userUpdate.jsp";
	public static String URL_USER_INSERT = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_userInsert.jsp";
	public static String URL_GET_USERDATA = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getUserData.jsp";
	public static String URL_GET_PRODUCTS = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getProducts.jsp";
	public static String URL_GET_CARDTYPES = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getCardType.jsp";
	public static String URL_GET_PROVIDERS = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getProviders.jsp";
	public static String URL_GET_CATEGORIES = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getCategoris.jsp";
	public static String URL_GET_PROMOTIONS = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getPromotions.jsp";
	public static String URL_GET_CONSULTS = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getUserPurchases.jsp";
	public static String URL_UPDATE_PASS = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_userPasswordUpdate.jsp";
	public static String URL_UPDATE_PASS_MAIL = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_userPasswordUpdateMail.jsp";
	public static String URL_PURCHASE_IAVE = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_purchase_iave.jsp";
	public static String URL_GET_PASSWORD = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_RecoveryUP.jsp";
	public static String URL_SET_INVITA = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_setInvita.jsp";
	public static String URL_GET_COMISION = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getComision.jsp";
	public static String URL_GET_TAGS = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getTipoRecargaTag.jsp";
	public static String URL_SET_TAG = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_setTag.jsp";
	public static String URL_GET_TAGS_USER = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getTags.jsp";
	public static String URL_REMOVE_TAGS_USER = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_removeTag.jsp";
	public static String URL_PURCHASE_OHL = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_purchase_ohl.jsp";
	public static String URL_PURCHASE_VIAPASS = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_purchase_viapass.jsp";
	public static String URL_SET_SMS = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_setSMS.jsp";
	public static String URL_PURCHASE_PASE = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_purchase_pase.jsp";
	public static String URL_GET_ESTADOS = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getEstados.jsp";
	public static String URL_GET_ESTADO_CIVIL = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getEstadoCivil.jsp";
	public static String URL_GET_PARENTESCO = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getParentesco.jsp";
	public static String URL_PURCHASE_VITA_IND = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_purchase_vitamedica.jsp";
	public static String URL_INTERJET_GET_MONTO = "http://www.addcelapp.com:12345/getPNR2.aspx?pnr=";
	public static String URL_INTERJET_PURCHASE = "http://www.addcelapp.com:12345/getInterjetTransaction.aspx";
}

package mx.com.ib.etn.model.data;

import java.util.Date;
import java.util.Vector;

import mx.com.ib.etn.beans.dto.Passenger;
import mx.com.ib.etn.beans.dto.Router;
import mx.com.ib.etn.beans.dto.Scheduled;
import mx.com.ib.etn.view.seats.GraphicButtonSeat;

public class DataKeeper {

	private DataTravel dataTravel = null;

	private Scheduled scheduledDepart = null; 
	private Scheduled scheduledArrive = null;
	private Router routerDepart = null; 
	private Router routerArrive = null;

	private GraphicButtonSeat seatsDepart[][];
	private GraphicButtonSeat seatsArrive[][];
	
	private Vector passengersDepart;
	private Vector passengersArrive;
	
	private StringBuffer sSeatDepart = null;
	private StringBuffer namesDepart = null;

	private StringBuffer sSeatArrive = null;
	private StringBuffer namesArrive = null;
	
	private static DataKeeper instance = null;

	private DataKeeper() {

		dataTravel = new DataTravel();
	}

	public synchronized static DataKeeper getInstance() {
		if (instance == null) {
			synchronized (DataKeeper.class) {
				DataKeeper inst = instance;
				if (inst == null) {
					synchronized (DataKeeper.class) {
						instance = new DataKeeper();
					}
				}
			}
		}
		return instance;
	}

	public void setDataTravel(Date dateDepart, Date dateArrive, String depart,
			String arrive, int iAdults, int iChildren, int iElderlies,
			int iStudents, int iProfessors, boolean isBasicFlight) {

		dataTravel.setDateDepart(dateDepart);
		dataTravel.setDateArrive(dateArrive);
		dataTravel.setDepart(depart);
		dataTravel.setArrive(arrive);
		dataTravel.setiAdults(iAdults);
		dataTravel.setiChildren(iChildren);
		dataTravel.setiElderlies(iElderlies);
		dataTravel.setiStudents(iStudents);
		dataTravel.setiProfessors(iProfessors);
		dataTravel.setBasicFlight(isBasicFlight);
	}
	
	
	public DataTravel getDataTravel(){
		
		return dataTravel;
	}
	
	public Scheduled getScheduledDepart() {
		return scheduledDepart;
	}

	public void setScheduledDepart(Scheduled scheduledDepart) {
		this.scheduledDepart = scheduledDepart;
	}

	public Scheduled getScheduledArrive() {
		return scheduledArrive;
	}

	public void setScheduledArrive(Scheduled scheduledArrive) {
		this.scheduledArrive = scheduledArrive;
	}



	public String getStringSeatsDepart(){
		return sSeatDepart.toString();
	}
	
	public String getStringNamesDepart(){
		return namesDepart.toString();
	}
	
	public void createSeatsNamesDepart(){
		
		sSeatDepart = new StringBuffer();
		
		namesDepart = new StringBuffer();
		
		int count = 0;
		
		int i = seatsDepart.length;
		int j = seatsDepart[0].length;

		for (int a = 0; a < i; a++) {
			for (int b = 0; b < j; b++) {
				
				Passenger passenger2 = seatsDepart[a][b].getPassenger();
				
				if (passenger2 != null){
					
					String seat = seatsDepart[a][b].getSeat();
					
					if (seat.indexOf("T")>=0){
						
						seat = seat.substring(0, 2);
					}
					
					sSeatDepart.append(seat);
					
					if (count == 0){
						namesDepart.append(passenger2.getName());
						count++;
					} else {
						namesDepart.append(",").append(passenger2.getName());
					}
				}
			}
		}
	}

	
	public String getStringSeatsArrive(){
		return sSeatDepart.toString();
	}
	
	public String getStringNamesArrive(){
		return namesDepart.toString();
	}
	
	public void createSeatsNamesArrive(){
		
		sSeatArrive = new StringBuffer();
		
		namesArrive = new StringBuffer();
		
		int count = 0;
		
		int i = seatsArrive.length;
		int j = seatsArrive[0].length;

		for (int a = 0; a < i; a++) {
			for (int b = 0; b < j; b++) {
				
				Passenger passenger2 = seatsArrive[a][b].getPassenger();
				
				if (passenger2 != null){
					
					String seat = seatsArrive[a][b].getSeat();
					
					if (seat.indexOf("T")>=0){
						
						seat = seat.substring(0, 2);
					}
					sSeatArrive.append(seat);
					if (count == 0){
						namesArrive.append(passenger2.getName());
						count++;
					} else {
						namesArrive.append(",").append(passenger2.getName());
					}
				}
			}
		}
	}
	
	
	
	public Router getRouterArrive() {
		return routerArrive;
	}

	public void setRouterArrive(Router routerArrive) {
		this.routerArrive = routerArrive;
	}

	public Router getRouterDepart() {
		return routerDepart;
	}

	public void setRouterDepart(Router routerDepart) {
		this.routerDepart = routerDepart;
	}

	public Vector getPassengersArrive() {
		return passengersArrive;
	}

	public void setPassengersArrive(Vector passengersArrive) {
		this.passengersArrive = passengersArrive;
	}

	public Vector getPassengersDepart() {
		return passengersDepart;
	}

	public void setPassengersDepart(Vector passengersDepart) {
		this.passengersDepart = passengersDepart;
	}
	
	public GraphicButtonSeat[][] getSeatsDepart() {
		return seatsDepart;
	}

	public void setSeatsDepart(GraphicButtonSeat[][] seatsDepart) {
		this.seatsDepart = seatsDepart;
	}

	public GraphicButtonSeat[][] getSeatsArrive() {
		return seatsArrive;
	}

	public void setSeatsArrive(GraphicButtonSeat[][] seatsArrive) {
		this.seatsArrive = seatsArrive;
	}
}

package mx.com.ib.etn.view.seats;

import java.util.Date;

import mx.com.ib.etn.beans.dto.Passenger;
import mx.com.ib.etn.model.connection.base.ThreadHTTP;
import mx.com.ib.etn.model.creatorbeans.fromjs.JSBusDiagram;
import mx.com.ib.etn.model.data.DataKeeper;
import mx.com.ib.etn.model.data.DataTravel;
import mx.com.ib.etn.utils.UtilDate;
import mx.com.ib.etn.utils.UtilIcon;
import mx.com.ib.etn.view.controls.CustomMainScreen;
import mx.com.ib.etn.view.controls.TitleOrangeRichTextField;
import mx.com.ib.etn.view.controls.Viewable;
import mx.com.ib.etn.view.controls.WhiteLabelField;
import net.rim.blackberry.api.browser.URLEncodedPostData;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Screen;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.GridFieldManager;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

import org.json.me.JSONException;
import org.json.me.JSONObject;

//public class ViewSeats extends CustomMainScreen implements FieldChangeListener, Viewable {
public class ViewSeats extends CustomMainScreen implements Viewable {

	private String flight;
	private String dateDepart;
	private JSBusDiagram jsBusDiagram;
	private EditField passengerFiled;

	private Passenger passenger;
	private GraphicButtonSeat seats[][];

	private DataKeeper dataKeeper = null;
	private DataTravel dataTravel = null;

	private boolean isDepartFlight;

	private boolean action = false;
	private boolean name = false;
	private CustomGridFieldSeats gridFieldSeats;
	private Passenger lastPassenger;

	
	public ViewSeats(boolean isDepartFlight, String flight, String dateDepart, Passenger passenger) {

		//super();
		this.flight = flight;
		this.dateDepart = dateDepart;
		this.passenger = passenger;
		this.isDepartFlight = isDepartFlight;
		this.lastPassenger = clonePassenger(passenger);
		
		BitmapField bitmapField = UtilIcon.getTitle();
		add(bitmapField);

		TitleOrangeRichTextField orangeRichTextField = new TitleOrangeRichTextField(
				"Paso 5 - Selecci�n de asiento y nombre de pasajero");
		add(orangeRichTextField);
		add(new LabelField(""));

		WhiteLabelField labelPassenger = new WhiteLabelField("Pasajero: ");
		passengerFiled = new EditField("", passenger.getName());
		Listener listener = new Listener(passengerFiled, passenger);
		passengerFiled.setChangeListener(listener);

		Background background = BackgroundFactory
				.createLinearGradientBackground(Color.WHITE, Color.WHITE,
						Color.WHITESMOKE, Color.WHITESMOKE);
		passengerFiled.setBackground(background);

		HorizontalFieldManager fieldManager = new HorizontalFieldManager();
		fieldManager.add(labelPassenger);
		fieldManager.add(passengerFiled);
		add(fieldManager);

		add(new LabelField(""));
		// add(passengerFiled);

		dataKeeper = DataKeeper.getInstance();

		if (isDepartFlight) {
			seats = dataKeeper.getSeatsDepart();
		} else {
			seats = dataKeeper.getSeatsArrive();
		}

		if (seats == null) {

			URLEncodedPostData encodedPostData = new URLEncodedPostData(
					URLEncodedPostData.DEFAULT_CHARSET, false);

			encodedPostData.append("fecha", dateDepart);
			encodedPostData.append("corrida", flight);

			ThreadHTTP threadHTTP = new ThreadHTTP();
			threadHTTP.setHttpConfigurator(Viewable.SEE_DIAGRAM, this,
					encodedPostData);
			threadHTTP.start();
		} else {
			
			updateManager(seats);
		}
	}

	
	public void sendMessage(String message) {

		Dialog.alert(message);
	}
	
	public void setData(int request, JSONObject jsObject) {

		if (request == Viewable.SEE_DIAGRAM) {

			try {
				jsBusDiagram = new JSBusDiagram();
				seats = jsBusDiagram.createDiagram(jsObject);

			} catch (JSONException jsonE) {

				jsonE.printStackTrace();
				Dialog.alert("Error al interpretar la informacion");
			}

			dataTravel = dataKeeper.getDataTravel();

			Date date = dataTravel.getDateDepart();

			String sDate = UtilDate.getDateToDDMMAAAA(date);

			URLEncodedPostData encodedPostData = new URLEncodedPostData(
					URLEncodedPostData.DEFAULT_CHARSET, false);

			encodedPostData.append("origen", dataTravel.getDepart());
			encodedPostData.append("destino", dataTravel.getArrive());

			encodedPostData.append("fecha", sDate);
			encodedPostData.append("corrida", flight);

			ThreadHTTP threadHTTP = new ThreadHTTP();
			threadHTTP.setHttpConfigurator(Viewable.SEE_OCCUPANCY, this,
					encodedPostData);
			threadHTTP.start();

		} else if (request == Viewable.SEE_OCCUPANCY) {

			try {

				seats = jsBusDiagram.setOccupancy(jsObject, seats);

				if (isDepartFlight) {
					dataKeeper.setSeatsDepart(seats);
				} else {
					dataKeeper.setSeatsArrive(seats);
				}
				updateManager(seats);

			} catch (JSONException jsonE) {

				jsonE.printStackTrace();
			}
		}
	}


	
	public boolean onSavePrompt(){
		
		System.out.println(passenger.getName());
		return super.onSavePrompt();
	}
	
	public void close() {

		
		/*
		final int SAVE = 0;
		final int QUIT = 1;
		int DEFAULT = SAVE;
		int answer = Dialog.ask("Opciones", new String[] {"Guardar Informaci�n", "Salir" }, new int[] { SAVE, QUIT }, DEFAULT);

		if (answer == QUIT) {
			gridFieldSeats.clearSeats();
			gridFieldSeats.reallocateSeat(passenger, lastPassenger);
			gridFieldSeats.deleteAll();
			passenger = lastPassenger;
		} else {
			gridFieldSeats.clearSeats();
			gridFieldSeats.deleteAll();
		}
		
				String seat = passenger.getSeat();
		
		if ((seat == null)||(!passenger.isNamed())){
			
			
		}

		*/
		
		
		Dialog.inform("Guardar Informaci�n");
		
		gridFieldSeats.clearSeats();
		gridFieldSeats.deleteAll();
		
		super.close();
	}
	
	
	private Passenger clonePassenger(Passenger passenger){
		
		Passenger passenger2 = new Passenger();
		
		
		passenger2.setKind(passenger.getKind());
		passenger2.setCharge(passenger.getCharge());
		passenger2.setName(passenger.getName());
		passenger2.setSeat(passenger.getSeat());
		passenger2.setNamed(passenger.isNamed());
		
		return passenger2;
	}
	
	private void updateManager(GraphicButtonSeat seats[][]) {

		int i = seats.length;
		int j = seats[0].length;

		if (gridFieldSeats == null){
			
			gridFieldSeats = new CustomGridFieldSeats(i, j, GridFieldManager.FIELD_HCENTER, passenger, seats);
			add(gridFieldSeats);
		} else {
			
			delete(gridFieldSeats);
			gridFieldSeats = new CustomGridFieldSeats(i, j, GridFieldManager.FIELD_HCENTER, passenger, seats);
			add(gridFieldSeats); 
		}
	}


}

class Listener implements FieldChangeListener{

	private EditField passengerFiled;
	private Passenger passenger;
	
	Listener(EditField passengerFiled, Passenger passenger){
		
		this.passengerFiled = passengerFiled;
		this.passenger = passenger;
	}
	
	public void fieldChanged(Field field, int context) {

		if (field == (EditField)passengerFiled){
			
			String name = passengerFiled.getText();
			
			if ((name != null)&&(name.length() > 0)){
				passenger.setName(name);
				passenger.setNamed(true);
			} else {
				passenger.setName(null);
				passenger.setNamed(false);
			}
		}
	}
}


/*
private void updateManager(GraphicButtonSeat seats[][]) {

	int i = seats.length;
	int j = seats[0].length;

	int widthSeat = 0;
	int heightSeat = 0;
	int columns = j;

	int widthDisplay = Display.getWidth();
	int heightDisplay = Display.getHeight();

	fieldManager = new GridFieldManager(i, j,
			GridFieldManager.FIELD_HCENTER);

	if ((i > 0) && (j > 0)) {

		widthSeat = seats[0][0].getPreferredWidth();
		heightSeat = seats[0][0].getPreferredHeight();
	}

	final int extend = (widthDisplay - (widthSeat * columns)) / 2;

	for (int a = 0; a < i; a++) {
		for (int b = 0; b < j; b++) {
			seats[a][b].setChangeListener(this);
			fieldManager.add(seats[a][b]);
		}
	}

	fieldManager.setColumnPadding(0);
	fieldManager.setRowPadding(0);

	HorizontalFieldManager managerHorizontal = new HorizontalFieldManager();
	LabelField labelField = new LabelField("") {

		public int getPreferredWidth() {
			return extend;
		}
	};

	managerHorizontal.add(labelField);
	managerHorizontal.add(fieldManager);

	add(managerHorizontal);

	int a = fieldManager.getWidth();
	int b = fieldManager.getHeight();
}
*/




/*
public void fieldChanged(Field arg0, int arg1) {

	if (arg0 == (EditField)passengerFiled){
		name = true;
	}
	
	
	if (arg0 instanceof GraphicButtonSeat) {

		action = true;
		
		GraphicButtonSeat seat = (GraphicButtonSeat) arg0;

		seat.setChangeListener(null);

		if (!seat.isOccupied()) {
			Passenger passenger2 = seat.getPassenger();

			if (passenger2 == null) {

				int i = seats.length;
				int j = seats[0].length;

				for (int a = 0; a < i; a++) {
					for (int b = 0; b < j; b++) {

						Passenger passenger3 = seats[a][b].getPassenger();

						if (passenger3 != null) {

							if (passenger3.equals(passenger)) {
								seats[a][b].setPassenger(null);
								seats[a][b].isPermitted(false);
							}
						}
					}
				}
				seat.setPassenger(passenger);
				seat.isPermitted(true);
			}
		}
		seat.setChangeListener(this);
	}
}
*/








/*
 * 
 * Buen d�a David:
 * 
 * Ya resolvimos las dudas con el tema de los asientos, la simbolog�a es la
 * siguiente:
 * 
 * c -> cafeter�a H -> ba�o hombres M -> ba�o mujeres P -> escalera T ->
 * television U -> ba�o unisex
 * 
 * para saber cual es la distribuci�n tendr�as que ver el siguiente arreglo
 * 
 * 
 * [ "01  ","00  ","02  ","03T ", "04  ","00  ","05  ","06  ",
 * "07  ","00  ","08  ","09  ", "10T ","00  ","11  ","12  ",
 * "13  ","00  ","14  ","15  ", "16  ","00  ","17  ","18T ",
 * "19  ","00  ","20  ","21  ", "22  ","00  ","23  ","24  ",
 * "00  ","00  ","00  ","00  ", "00  ","00  ","00  ","00  ",
 * "00  ","00  ","00  ","00  ", "00  ","00  ","00  ","00  ",
 * "00M ","00C ","00C ","00C ", "00H ","00" ], "numFilas":"9", "numError":1111,
 * "descError":"Cadena correcta.", "numAsientos":24
 * 
 * 
 * 
 * {"asientos":[ "0","0","0","0","0","0", "0","0","0","1","0","0",
 * "0","0","0","0","0","0", "0","0","0","0","0","0" ], "codigoError":"00",
 * "descripcionError":"Sin Error."}
 * 
 * 
 * 
 * 01 ,00 ,02 ,03T ,04 ,00 ,05 ,06 ,07 ,00 ,08 ,09 ,10T ,00 ,11 ,12 ,13 ,00 ,14
 * ,15 ,16 ,00 ,17 ,18T ,19 ,00 ,20 ,21 ,22 ,00 ,23 ,24 ,00 ,00 ,00 ,00 ,00 ,00
 * ,00 ,00 ,00 ,00 ,00 ,00 ,00 ,00 ,00 ,00 ,00M ,00C ,00C ,00C ,00H ,00|9|
 * 
 * el n�mero 9 que aparece al final entre pipes indica que son 9 filas
 * 
 * y el arreglo de enteros indica la distribuci�n de los asientos, por ejemplo:
 * 
 * 01 , 00 , 02 , 03T 04 , 00 , 05 , 06 07 , 00 , 08 , 09 10T, 00 , 11 , 12 13 ,
 * 00 , 14 , 15 16 , 00 , 17 , 18T 19 , 00 , 20 , 21 22 , 00 , 23 , 24 00 , 00 ,
 * 00 , 00 00 , 00 , 00 , 00 00 , 00 , 00 , 00 00 , 00 , 00 , 00 0M ,00C ,00C
 * ,00C ,00H 00
 * 
 * Si vemos el diagrama de la p�gina de ETN concuerda con lo que nos regresa la
 * consulta al servicio En la imagen apenas se percibe, pero si te das cuenta en
 * los asientos que tienen la letra "T" pinta una "Televisi�n", Charly ya
 * solicit� las im�genes para cada cosa que aparece en el diagrama.
 * 
 * Por otra parte la ocupaci�n de los asientos te la enviaremos en un arreglo de
 * enteros donde la longitud corresponder� al n�mero de asientos del autob�s,
 * actualmente estamos regresando 101 posiciones pero se est� trabajando en
 * adaptarlo a como te estoy especificando.
 */
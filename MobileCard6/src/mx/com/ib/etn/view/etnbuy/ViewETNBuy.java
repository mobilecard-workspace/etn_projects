package mx.com.ib.etn.view.etnbuy;

import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import mx.com.ib.beans.GeneralBean;
import mx.com.ib.etn.beans.dto.Router;
import mx.com.ib.etn.model.connection.base.ThreadHTTP;
import mx.com.ib.etn.model.creatorbeans.fromjs.JSRouterArrive;
import mx.com.ib.etn.model.creatorbeans.fromjs.JSRouterDepart;
import mx.com.ib.etn.model.data.DataKeeper;
import mx.com.ib.etn.utils.UtilColor;
import mx.com.ib.etn.utils.UtilDate;
import mx.com.ib.etn.utils.UtilIcon;
import mx.com.ib.etn.view.animated.SplashScreen;
import mx.com.ib.etn.view.controls.CustomMainScreen;
import mx.com.ib.etn.view.controls.CustomSelectedButtonField;
import mx.com.ib.etn.view.controls.TitleOrangeRichTextField;
import mx.com.ib.etn.view.controls.Viewable;
import mx.com.ib.etn.view.controls.WhiteLabelField;
import mx.com.ib.etn.view.etnbuy.route.CustomButtonFieldManager;
import mx.com.ib.etn.view.scheduled.ViewScheduledDepart;
import net.rim.blackberry.api.browser.URLEncodedPostData;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.DateField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NumericChoiceField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import org.json.me.JSONException;
import org.json.me.JSONObject;

public class ViewETNBuy extends CustomMainScreen implements
		FieldChangeListener, Viewable {

	private CustomSelectedButtonField basicFlight = null;
	private CustomSelectedButtonField roundFlight = null;

	private ObjectChoiceField choiceFieldDepart = null;
	private ObjectChoiceField choiceFieldArrive = null;

	private DateField dateFieldDepart = null;
	private DateField dateFieldArrive = null;

	private NumericChoiceField choiceFieldAdult = null;
	private NumericChoiceField choiceFieldChild = null;
	private NumericChoiceField choiceFieldElderly = null;
	private NumericChoiceField choiceFieldStudent = null;
	private NumericChoiceField choiceFieldProfessor = null;

	private Bitmap encodedImage = null;
	private ButtonField ok = null;

	// private GeneralBean generalBean = null;

	private boolean isBasicFlight = false;
	private boolean isFirst = true;

	private String depart = null;
	private String arrive = null;

	private long dateDepart = 0;
	private long dateArrive = 0;

	private int iDepart = 0;
	private int iArrive = 0;

	private int iAdults = 0;
	private int iChildren = 0;
	private int iElderlies = 0;
	private int iStudents = 0;
	private int iProfessors = 0;

	private Vector vectorDepart = null;
	private String arrayDepart[] = null;

	private Vector vectorArrive = null;
	private String arrayArrive[] = null;

	
	SplashScreen splashScreen = null;
	
	
	private CustomButtonFieldManager customFieldManager = null;

	// public ViewETNBuy(GeneralBean generalBean) {
	public ViewETNBuy(GeneralBean generalBeans) {

		super();
		splashScreen = SplashScreen.getInstance();
		// this.generalBean = generalBean;

		//splashScreen.init()
		
		configurationFlightView(isBasicFlight);
		loadDepartData();
		

	}


	private void loadDepartData() {

		splashScreen.start();

		URLEncodedPostData encodedPostData = new URLEncodedPostData(
				URLEncodedPostData.DEFAULT_CHARSET, false);

		ThreadHTTP threadHTTP = new ThreadHTTP();
		threadHTTP.setHttpConfigurator(Viewable.DEPART, this, encodedPostData);
		threadHTTP.start();
	}


	private void loadArriveData() {
		
		splashScreen.start();
		
		choiceFieldArrive.setChoices(null);

		int index = choiceFieldDepart.getSelectedIndex();

		if (index >= 0) {
			Router router = (Router) vectorDepart.elementAt(index);

			URLEncodedPostData encodedPostData = new URLEncodedPostData(
					URLEncodedPostData.DEFAULT_CHARSET, false);
			encodedPostData.append("origen", router.getKey());

			ThreadHTTP threadHTTP = new ThreadHTTP();
			threadHTTP.setHttpConfigurator(Viewable.ARRIVE, this,
					encodedPostData);
			threadHTTP.start();
		} else {
			Dialog.alert("No existe informaci�n en Origen");
		}
	}

	
	public void sendMessage(String message) {

		Dialog.alert(message);
	}
	
	
	public void setData(int request, JSONObject jsObject) {

		splashScreen.remove();
		
		switch (request) {
		case Viewable.DEPART: {
			JSRouterDepart jsRouter = new JSRouterDepart();
			try {
				vectorDepart = jsRouter.getVectorRouters(jsObject,
						Viewable.DEPART);
				arrayDepart = jsRouter.getArrayRouters(jsObject,
						Viewable.DEPART);
				choiceFieldDepart.setChoices(arrayDepart);

				choiceFieldArrive.setChoices(null);
			} catch (JSONException e) {
				e.printStackTrace();
				Dialog.alert("Error al interpretar los datos");
			}
		}
			break;

		case Viewable.ARRIVE: {
			JSRouterArrive jsRouter = new JSRouterArrive();
			try {
				vectorArrive = jsRouter.getVectorRouters(jsObject,
						Viewable.ARRIVE);
				arrayArrive = jsRouter.getArrayRouters(jsObject,
						Viewable.ARRIVE);
				choiceFieldArrive.setChoices(arrayArrive);
			} catch (JSONException e) {
				e.printStackTrace();
				Dialog.alert("Error al interpretar los datos");
			}
		}
			break;

		default:
			break;
		}
	}

	
	public void configurationFlightView(boolean isBasicFlight) {

		deleteAll();

		BitmapField bitmapField = UtilIcon.getTitle();
		add(bitmapField);

		TitleOrangeRichTextField orangeRichTextField01 = new TitleOrangeRichTextField(
				"Paso 1 - Datos de viaje");

		add(orangeRichTextField01);
		add(new LabelField(""));

		CustomSelectedButtonField basicFlight = new CustomSelectedButtonField(
				"Viaje Sencillo", Field.FIELD_VCENTER);
		CustomSelectedButtonField roundFlight = new CustomSelectedButtonField(
				"Viaje Redondo", Field.FIELD_VCENTER);

		basicFlight.setSelected(this.isBasicFlight);
		roundFlight.setSelected(!this.isBasicFlight);

		customFieldManager = new CustomButtonFieldManager(basicFlight,
				roundFlight);
		basicFlight.setChangeListener(this);
		roundFlight.setChangeListener(this);

		customFieldManager.add(roundFlight);
		customFieldManager.add(basicFlight);

		add(customFieldManager);

		String choices[] = null;

		choiceFieldDepart = new ObjectChoiceField("", arrayDepart, iDepart);
		choiceFieldDepart.setChangeListener(this);
		WhiteLabelField textFieldDepart = new WhiteLabelField("Origen");

		HorizontalFieldManager fieldManager1 = new HorizontalFieldManager();
		fieldManager1.add(textFieldDepart);
		fieldManager1.add(choiceFieldDepart);
		add(fieldManager1);

		choiceFieldArrive = new ObjectChoiceField("", choices, iArrive);
		choiceFieldArrive.setChangeListener(this);
		WhiteLabelField textFieldArrive = new WhiteLabelField("Destino");
		HorizontalFieldManager fieldManager2 = new HorizontalFieldManager();
		fieldManager2.add(textFieldArrive);
		fieldManager2.add(choiceFieldArrive);
		add(fieldManager2);

		dateFieldDepart = new DateField("", System.currentTimeMillis(),
				DateField.DATE) {
			protected void paint(Graphics graphics) {
				graphics.setColor(UtilColor.TITLE_STRING_BLACK);
				super.paint(graphics);
			}
		};

		WhiteLabelField labelDateDepart = new WhiteLabelField("Fecha de salida");
		HorizontalFieldManager fieldManager3 = new HorizontalFieldManager();
		fieldManager3.add(labelDateDepart);
		fieldManager3.add(dateFieldDepart);
		add(fieldManager3);

		dateFieldArrive = new DateField("", System.currentTimeMillis(),
				DateField.DATE) {
			protected void paint(Graphics graphics) {
				graphics.setColor(UtilColor.TITLE_STRING_BLACK);
				super.paint(graphics);
			}
		};
		WhiteLabelField labelDateArrive = new WhiteLabelField(
				"Fecha de Regreso");
		HorizontalFieldManager fieldManager4 = new HorizontalFieldManager();
		fieldManager4.add(labelDateArrive);
		fieldManager4.add(dateFieldArrive);
		if (!isBasicFlight) {
			add(fieldManager4);
		}

		if (dateDepart > 0) {
			dateFieldDepart.setDate(dateDepart);
		}

		if (dateArrive > 0) {
			dateFieldArrive.setDate(dateArrive);
		}

		TitleOrangeRichTextField orangeRichTextField02 = new TitleOrangeRichTextField(
				"Paso 2 - A�adir pasajeros");
		add(orangeRichTextField02);

		choiceFieldAdult = addChoiceField("Adulto", iAdults);
		choiceFieldChild = addChoiceField("Menor", iChildren);
		choiceFieldElderly = addChoiceField("Senectud", iElderlies);
		choiceFieldStudent = addChoiceField("Estudiante", iStudents);
		choiceFieldProfessor = addChoiceField("Profesor", iProfessors);

		ok = new ButtonField("Elegir Horario");
		ok.setChangeListener(this);

		add(ok);
	}

	private NumericChoiceField addChoiceField(String title, int number) {
		int iStartAt = 0;
		int iEndAt = 31;
		int iIncrement = 1;

		WhiteLabelField labelField = new WhiteLabelField(title);
		NumericChoiceField choiceField = new NumericChoiceField("", iStartAt,
				iEndAt, iIncrement, number);

		HorizontalFieldManager fieldManager = new HorizontalFieldManager();
		fieldManager.add(labelField);
		fieldManager.add(choiceField);
		add(fieldManager);

		return choiceField;
	}

	private void keepData() {

		dateDepart = dateFieldDepart.getDate();
		dateArrive = dateFieldArrive.getDate();

		iDepart = choiceFieldDepart.getSelectedIndex();
		iArrive = choiceFieldArrive.getSelectedIndex();

		DataKeeper dataKeeper = DataKeeper.getInstance();

		if (iDepart >= 0) {
			Router router = (Router) vectorDepart.elementAt(iDepart);
			dataKeeper.setRouterDepart(router);
			depart = router.getKey();
		} else {
			depart = null;
			dataKeeper.setRouterDepart(null);
		}

		if (iArrive >= 0) {
			Router router = (Router) vectorArrive.elementAt(iArrive);
			dataKeeper.setRouterArrive(router);
			arrive = router.getKey();
		} else {
			arrive = null;
			dataKeeper.setRouterArrive(null);
		}

		iAdults = choiceFieldAdult.getSelectedIndex();
		iChildren = choiceFieldChild.getSelectedIndex();
		iElderlies = choiceFieldElderly.getSelectedIndex();
		iStudents = choiceFieldStudent.getSelectedIndex();
		iProfessors = choiceFieldProfessor.getSelectedIndex();
	}

	private String getStringForChoice(int index, ObjectChoiceField choiceField) {
		String choice = (String) choiceField.getChoice(index);
		return choice;
	}

	public void fieldChanged(Field arg0, int arg1) {

		if (arg0 == (ObjectChoiceField) choiceFieldDepart) {

			if (isFirst) {
				isFirst = false;
				loadArriveData();
			} else {

				if (ObjectChoiceField.CONTEXT_CHANGE_OPTION == arg1) {

					loadArriveData();
				}
			}

		} else if (arg0 == (ButtonField) ok) {

			keepData();

			Calendar calendar = Calendar.getInstance();

			long dateDepart01l = dateFieldDepart.getDate();
			long dateArrive01l = dateFieldArrive.getDate();

			Date dateDepart01 = new Date(dateDepart01l);
			Date dateArrive02 = new Date(dateArrive01l);

			calendar.setTime(dateDepart01);
			Date a = calendar.getTime();

			calendar.setTime(dateArrive02);
			Date b = calendar.getTime();

			boolean value = false;

			if (isBasicFlight) {
				value = true;
			} else {
				value = UtilDate.after(a, b);
			}

			// boolean value = UtilDate.after(a, b);

			if (value) {

				if (verifyNumPassengers()) {
					DataKeeper dataKeeper = DataKeeper.getInstance();
					dataKeeper.setDataTravel(new Date(dateDepart), new Date(
							dateArrive), depart, arrive, iAdults, iChildren,
							iElderlies, iStudents, iProfessors, isBasicFlight);

					UiApplication.getUiApplication().pushScreen(
							new ViewScheduledDepart(true, isBasicFlight));
				} else {

					Dialog.alert("Selecciona al menos un pasajero.");
				}

			} else {

				Dialog.alert("La Fecha de regreso es anterior a la fecha de salida");
			}

		} else if (arg0 == (CustomSelectedButtonField) customFieldManager
				.getBasicFlight()) {
			roundFlight = customFieldManager.getRoundFlight();
			roundFlight.setSelected(false);
			isBasicFlight = true;
			keepData();
			configurationFlightView(isBasicFlight);
			loadArriveData();
		} else if (arg0 == (CustomSelectedButtonField) customFieldManager
				.getRoundFlight()) {
			basicFlight = customFieldManager.getBasicFlight();
			basicFlight.setSelected(false);
			isBasicFlight = false;
			keepData();
			configurationFlightView(isBasicFlight);
			loadArriveData();
		}
	}

	private boolean verifyNumPassengers() {

		boolean value = false;

		if ((iAdults > 0) || (iChildren > 0) || (iElderlies > 0)
				|| (iStudents > 0) || (iProfessors > 0)

		) {
			value = true;
		} else {
			value = false;
		}

		return value;
	}
}

/*
 * encodedImage = Bitmap.getBitmapResource(MainClass.HD_COMPRAS); encodedImage =
 * Bitmap.getBitmapResource("falla");
 * 
 * BitmapField bitmapField = new BitmapField(encodedImage);
 * 
 * Background background =
 * BackgroundFactory.createBitmapBackground(encodedImage);
 * setBackground(background);
 * 
 * //setBanner(new BannerBitmap(MainClass.HD_COMPRAS)); //add(bitmapField);
 */

/*
 * int iStartAt = 0; int iEndAt = 31; int iIncrement = 1;
 * 
 * choiceFieldAdult = new NumericChoiceField("Adulto", iStartAt, iEndAt,
 * iIncrement, iAdults); choiceFieldChild = new NumericChoiceField("Menor",
 * iStartAt, iEndAt, iIncrement, iChildren); choiceFieldElderly = new
 * NumericChoiceField("Senectud", iStartAt, iEndAt, iIncrement, iElderlies);
 * choiceFieldStudent = new NumericChoiceField("Estudiante", iStartAt, iEndAt,
 * iIncrement, iStudents); choiceFieldProfessor = new
 * NumericChoiceField("Profesor", iStartAt, iEndAt, iIncrement, iProfessors);
 */

/*
 * add(choiceFieldAdult); add(choiceFieldChild); add(choiceFieldElderly);
 * add(choiceFieldStudent); add(choiceFieldProfessor);
 */

/*
 * backgroundDate =
 * BackgroundFactory.createLinearGradientBackground(Color.WHITESMOKE,
 * Color.WHITESMOKE, Color.GRAY, Color.GRAY); Background backgroundDate2 =
 * BackgroundFactory.createLinearGradientBackground(Color.GRAY, Color.GRAY,
 * Color.WHITESMOKE, Color.WHITESMOKE);
 * //dateFieldDepart.setBackground(Field.VISUAL_STATE_NORMAL, backgroundDate);
 * //dateFieldDepart.setBackground(Field.VISUAL_STATE_ACTIVE, backgroundDate2);
 * //dateFieldDepart.setBackground(Field.VISUAL_STATE_DISABLED, backgroundDate);
 * //dateFieldDepart.setBackground(Field.VISUAL_STATE_DISABLED_FOCUS,
 * backgroundDate);
 */

/*
 * RichTextField textField = new RichTextField(RichTextField.NON_FOCUSABLE) {
 * protected void paint(Graphics graphics) {
 * graphics.setBackgroundColor(0xF16430); graphics.clear();
 * graphics.setColor(Color.WHITE); graphics.drawText("Paso 1 - Datos de viaje",
 * 0, 0, DrawStyle.HCENTER, Display.getWidth());
 * 
 * super.paint(graphics); } };
 */

package mx.com.ib.etn.view.roundflight;

import mx.com.ib.etn.beans.dto.Router;
import mx.com.ib.etn.beans.dto.Scheduled;
import mx.com.ib.etn.utils.UtilColor;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;

public class RoundFlightRichTextField extends RichTextField {

	final public static int DEPART = 0;
	final public static int ARRIVE = 1;
	final public static int DATE_DEPART = 2;
	final public static int DATE_ARRIVE = 3;

	final private String depart = "Origen: ";
	final private String arrive = "Destino: ";
	final private String dateDepart = "Partida: ";
	final private String dateArrive = "Llegada: ";

	private String title;
	private String data;
	private int width; 
	private int margin;
	
	public RoundFlightRichTextField(Router routerDepart, Router routerArrive, Scheduled scheduled, int type) {

		super("", RichTextField.NON_FOCUSABLE | RichTextField.USE_ALL_HEIGHT);
		
		width = Display.getWidth();
		margin = width/4;

		switch (type) {

		case RoundFlightRichTextField.DEPART:
			title = depart;
			data = routerDepart.getDescription();
			break;
		case RoundFlightRichTextField.ARRIVE:
			title = arrive;
			data = routerArrive.getDescription();
			break;
		case RoundFlightRichTextField.DATE_DEPART:
			title = dateDepart;
			data = scheduled.getDateDepart() + " " + scheduled.getHourDepart(); 
			break;
		case RoundFlightRichTextField.DATE_ARRIVE:
			title = dateArrive;
			data = scheduled.getDateArrive() + " " + scheduled.getHourArrive();
			break;

		}
	}


	public void paint(Graphics graphics) {

		graphics.setColor(UtilColor.ELEMENT_STRING_TITLE);
		graphics.drawText(title, 0, 0, DrawStyle.RIGHT, margin);
		graphics.setColor(UtilColor.ELEMENT_STRING_DATA);
		graphics.drawText(data, margin, 0, DrawStyle.LEFT, width);

		super.paint(graphics);
	}
}


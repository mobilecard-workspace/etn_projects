package mx.com.ib.etn.view.scheduled;

import mx.com.ib.etn.beans.dto.Scheduled;
import mx.com.ib.etn.model.data.DataKeeper;
import mx.com.ib.etn.model.data.DataTravel;
import mx.com.ib.etn.utils.UtilColor;
import mx.com.ib.etn.view.roundflight.ViewRoundFligth;
import mx.com.ib.etn.view.vroute.ViewRoute;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class CustomGridFieldScheduledArrive extends VerticalFieldManager {

	private Scheduled scheduled;
	private boolean isDepartFlight;
	private boolean isBasicFlight;
	
	public CustomGridFieldScheduledArrive(boolean isDepartFlight, boolean isBasicFlight, Scheduled scheduled) {
		super();
		this.scheduled = scheduled;
		this.isDepartFlight = isDepartFlight;
		this.isBasicFlight = isBasicFlight;
		
		add(new ScheduledRichTextField(scheduled, ScheduledRichTextField.DATE_DEPART));
		add(new ScheduledRichTextField(scheduled, ScheduledRichTextField.DATE_ARRIVE));
		add(new ScheduledRichTextField(scheduled, ScheduledRichTextField.DATE_LINE));
		add(new ScheduledRichTextField(scheduled, ScheduledRichTextField.DATE_CHARGE));
	}


	public boolean setData() {
		return true;
	}


	protected void paint(Graphics graphics){

		graphics.clear();
		
		if (isFocus()){
			graphics.setColor(UtilColor.ELEMENT_FOCUS);
			graphics.setGlobalAlpha(200);
			graphics.fillRoundRect(0, 0, getPreferredWidth(), getPreferredHeight(), 1, 1);
			graphics.setGlobalAlpha(255);
		} else {
			graphics.setColor(UtilColor.ELEMENT_BACKGROUND);
			graphics.fillRoundRect(0, 0, getPreferredWidth(), getPreferredHeight(), 1, 1);
		}
		
		super.paint(graphics);
	}	
	
	
	
	public boolean isFocusable() {
		return true;
	}

	public int getPreferredWidth() {
		return Display.getWidth();
	}

	protected boolean navigationClick(int status, int time) {
		fieldChangeNotify(0);
		showView();
		return super.navigationClick(status, time);
	}

	protected boolean keyChar(char character, int status, int time) {
		if (character == Keypad.KEY_ENTER) {
			fieldChangeNotify(0);
			showView();
			return true;
		}
		
/*
		else if (character == Keypad.KEY_ESCAPE){
			Dialog.alert("Salir");
		}
*/
		return super.keyChar(character, status, time);
	}

	protected boolean touchEvent(TouchEvent message) {

		if (message.getEvent() == TouchEvent.CLICK){
			fieldChangeNotify(0);
			showView();
			return true;
		}

		return super.touchEvent(message);
	}
	
	protected void onFocus(int direction) {
		super.onFocus(direction);
		invalidate();
	}

	protected void onUnfocus() {
		super.onUnfocus();
		invalidate();
	}


	private void showView(){

		DataKeeper dataKeeper = DataKeeper.getInstance();
		DataTravel dataTravel = dataKeeper.getDataTravel();
		String depart = dataTravel.getArrive();
		Scheduled scheduled = getScheduled();

		final int SEAT = 0;
		final int ROUTE = 1;
		int DEFAULT = SEAT;
		int answer = Dialog.ask("Opciones", new String[] {"Asignar Asientos", "Revisar itinerario."}, new int[] {SEAT,ROUTE}, DEFAULT);
		
		switch (answer) {
		case SEAT:
			dataKeeper.setScheduledArrive(scheduled);
			UiApplication.getUiApplication().pushScreen(new ViewRoundFligth(isBasicFlight));
			break;
		case ROUTE:
			UiApplication.getUiApplication().pushScreen(new ViewRoute(scheduled, depart));
			break;
		}
	}

	public Scheduled getScheduled(){

		ScheduledRichTextField richTextField = (ScheduledRichTextField)getField(0);
		Scheduled scheduled = richTextField.getScheduled();
		return scheduled;
	}
}

package mx.com.ib.etn.view.controls;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.LabelField;

public class CustomLabelField extends LabelField{

	private String text;
	private int position;
	private int widthPercent;
	private int color;
	
	
	/*
		position:
			DrawStyle.LEFT
			DrawStyle.RIGHT
			DrawStyle.HCENTER
	
		width:
			Display.getWidth():

		color:
			UtilColor.BACKGROUND
			UtilColor.BACKGROUND_FOCUS
	
			UtilColor.TITLE_STRING_BLACK
			UtilColor.TITLE_STRING
			UtilColor.TITLE_BACKGROUND 

			UtilColor.BUTTON_FOCUS
			UtilColor.BUTTON_FOCUS_STRING
			UtilColor.BUTTON_SELECTED
			UtilColor.BUTTON_UNSELECTED

			UtilColor.ELEMENT_BACKGROUND
			UtilColor.ELEMENT_FOCUS
			UtilColor.ELEMENT_STRING_TITLE
			UtilColor.ELEMENT_STRING_DATA
	*/


	public CustomLabelField(String text, int position, int widthPercent, int color){
		super(text, LabelField.NON_FOCUSABLE | position);
		this.text = text;
		this.position = position;
		this.widthPercent = Display.getWidth() * widthPercent;
		this.color = color;
	}
	
	
	public void paint(Graphics graphics) {

		graphics.setColor(color);
		graphics.drawText(text, 0, 0, position, widthPercent);
		super.paint(graphics);
	}
}
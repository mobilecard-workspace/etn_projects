package mx.com.ib.etn.view.vroute;

import mx.com.ib.etn.beans.dto.VRouter;
import mx.com.ib.etn.utils.UtilColor;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;

public class ArriveRichTextField extends RichTextField{

	private VRouter arrive;
	private String dateArrive;
	private int width = 0;
	private int center = 0;
	public ArriveRichTextField(VRouter arrive) {

		super("", RichTextField.NON_FOCUSABLE | RichTextField.USE_ALL_HEIGHT);
		this.arrive = arrive;
		dateArrive = arrive.getOffice();
		width = Display.getWidth();
		//center = width/(9*2);
		
	}


	public void paint(Graphics graphics) {

		graphics.setColor(UtilColor.ELEMENT_STRING_TITLE);
		graphics.drawText("Llegada: ", 0, 0, DrawStyle.RIGHT, width / 3);
		graphics.setColor(UtilColor.ELEMENT_STRING_DATA);
		graphics.drawText(dateArrive, width / 3, 0, DrawStyle.LEFT, width);

		super.paint(graphics);
	}
}

package mx.com.ib.etn.view.vroute;

import mx.com.ib.etn.beans.dto.VRouter;
import mx.com.ib.etn.utils.UtilColor;
import mx.com.ib.etn.utils.UtilDate;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;

public class TimeDepartRichTextField extends RichTextField{
	
	private VRouter depart;
	private String dateDepart;
	private int width = 0;
	
	public TimeDepartRichTextField(VRouter depart) {

		super("", RichTextField.NON_FOCUSABLE | RichTextField.USE_ALL_HEIGHT);
		this.depart = depart;

		String date = UtilDate.getAAAAMMDDtoDD_MM_AAAA(depart.getDateDepart());

		dateDepart = date + " " + depart.getHourDepart();
		width = Display.getWidth();
		
	}


	public void paint(Graphics graphics) {

		graphics.setColor(UtilColor.ELEMENT_STRING_TITLE);
		graphics.drawText("Dia y hora: ", 0, 0, DrawStyle.RIGHT, width / 3);
		graphics.setColor(UtilColor.ELEMENT_STRING_DATA);
		graphics.drawText(dateDepart, width / 3, 0, DrawStyle.LEFT, width);

		super.paint(graphics);
	}
}

package mx.com.ib.views;

import java.util.Calendar;
import java.util.Date;

import mx.com.ib.app.MainClass;
import mx.com.ib.beans.BeneficiarioBean;
import mx.com.ib.controls.BorderBasicEditField;
import mx.com.ib.controls.CustomVerticalFieldManager;
import mx.com.ib.controls.GenericImageButtonField;
import net.rim.device.api.i18n.SimpleDateFormat;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.CheckboxField;
import net.rim.device.api.ui.component.DateField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;


public class addBeneficiarioScreen extends PopupScreen implements FieldChangeListener{
	
	
	public LabelField TipoTag = null;
	
	
	public HorizontalFieldManager hfmTitle = null;
	public HorizontalFieldManager hfmBtn = null;
	public HorizontalFieldManager hfmnick = null;
	
	public HorizontalFieldManager tagHFM = null;
	public HorizontalFieldManager dvHFM = null;
	
	public CustomVerticalFieldManager customVerticalFieldManager = null;
	
	public int widthManager = 0;
	public boolean isLogin = false;
	public LabelField name,birthDate1 = null;
	public LabelField lastNames = null;
	public LabelField lastNamesM = null;
	public BorderBasicEditField basicName = null;
	public BorderBasicEditField basicLastNames = null;
	public BorderBasicEditField basicLastNamesM = null;
	public GenericImageButtonField btnChange = null;
	private VitamedicaIndividualScreen actualizate=null;
	public SimpleDateFormat dateFormat = null;
	public DateField dateField = null;	
	public Calendar calendar = null;
	public Calendar calendarbirthday = null;
	public String sex[],edocivil[],par[] = null;
	public String sex2[] = null;
	public ObjectChoiceField choicesSex  = null;
	public ObjectChoiceField choicesPare  = null;
	public ObjectChoiceField choicesEDOCivil  = null;
	public int pos=0;
	BeneficiarioBean bene=null;
	CheckboxField t=null;

	public addBeneficiarioScreen(BeneficiarioBean bene,int numBene,CheckboxField c){
	
		super(new VerticalFieldManager(VERTICAL_SCROLL));
		this.isLogin=isLogin;
		this.bene=bene;
		this.pos=numBene;
		this.t=c;
		initVariables();
		
		addFields();
		
		if(bene!=null){
			setFields();
			System.out.println("beneficiario Vac�o");
		}
	}
	
	private void setFields() {
		basicName.setText(bene.getNombre());
		basicLastNames.setText(bene.getApPat());
		basicLastNamesM.setText(bene.getApMat());
		//setFecha
	
		String numYear = bene.getFecNac().substring(0, MainClass.userBean.getBirthday().indexOf("-"));
		calendar.set(Calendar.YEAR, Integer.parseInt(numYear));
		String newYear = bene.getFecNac().substring(bene.getFecNac().indexOf("-")+1, bene.getFecNac().length()); 
		String month = "";		
		month = newYear.substring(0, newYear.indexOf("-"));
		calendar.set(Calendar.MONTH, Integer.parseInt(month)-1);	
		newYear = newYear.substring(newYear.indexOf("-")+1, newYear.length());
		calendar.set(Calendar.DATE, Integer.parseInt(newYear));
		dateField.setDate(calendar.getTime());
		
		choicesSex.setSelectedIndex(Integer.parseInt(bene.getId_gen()));
		choicesPare.setSelectedIndex(Integer.parseInt(bene.getId_par()));
		choicesEDOCivil.setSelectedIndex(Integer.parseInt(bene.getId_edoc()));
	
	// TODO Auto-generated method stub
	
}
	public void initVariables(){
	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_DOWN);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	     TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	     transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
	     transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_UP);
	     transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    	 engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
		hfmTitle = new HorizontalFieldManager(Field.FIELD_HCENTER);
		hfmnick = new HorizontalFieldManager(Field.FIELD_LEFT);
		hfmBtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		
		widthManager = Display.getWidth() - (Display.getWidth()/3);
		customVerticalFieldManager = new CustomVerticalFieldManager(Display.getWidth()-(Display.getWidth()/3),Display.getHeight()-30, HorizontalFieldManager.FIELD_LEFT | VERTICAL_SCROLL);
		TipoTag = new LabelField("Datos Familiar", Field.NON_FOCUSABLE);
		name = new LabelField("Nombre(s): ", Field.NON_FOCUSABLE);	
		lastNames = new LabelField("Apellido Paterno: ", Field.NON_FOCUSABLE);
		lastNamesM = new LabelField("Apellido Materno: ", Field.NON_FOCUSABLE);
		basicName = new BorderBasicEditField("","", 50, widthManager, Field.FOCUSABLE);
		birthDate1 = new LabelField("Fecha de Nacimiento: ", Field.NON_FOCUSABLE);
		basicLastNames = new BorderBasicEditField("","", 50, widthManager, Field.FOCUSABLE);
		basicLastNamesM = new BorderBasicEditField("","", 50, widthManager, Field.FOCUSABLE);
		calendar = Calendar.getInstance();
		
		calendarbirthday = Calendar.getInstance();
		
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateField = new DateField("", Long.MIN_VALUE, dateFormat, Field.FIELD_LEFT){
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				super.paint(graphics);
				invalidate();
			}
			
			protected void drawFocus(Graphics graphics, boolean on) {
				// TODO Auto-generated method stub
				super.drawFocus(graphics, on);
				invalidate();
			}
		};
		dateField.setDate(calendar.getTime());
		sex = new String[]{"Seleccione","Femenino","Masculino"};
		sex2 = new String[]{"","F","M"};
		choicesSex = new ObjectChoiceField("Sexo: ", sex){
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				super.paint(graphics);
				invalidate();
			}
			
			protected void drawFocus(Graphics graphics, boolean on) {
				// TODO Auto-generated method stub
				super.drawFocus(graphics, on);
				invalidate();
			}
			protected void fieldChangeNotify(int context)
            {
                   if(context==2){
                	   choicesSex.setFocus();
                   }
            }
		};
	//	edocivil = new String[]{"Seleccione","Soltero(a)","Casado(a)","Divorciado(a)","Viudo(a)","Union Libre"};
		
		edocivil = new String[MainClass.EdoCivilBeans.length];
		
		for(int i = 0; i < MainClass.EdoCivilBeans.length; i++){
			edocivil[i] = MainClass.EdoCivilBeans[i].getDescription();
		}
		
		choicesEDOCivil = new ObjectChoiceField("Estado Civil: ", edocivil){
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				super.paint(graphics);
				invalidate();
			}
			
			protected void drawFocus(Graphics graphics, boolean on) {
				// TODO Auto-generated method stub
				super.drawFocus(graphics, on);
				invalidate();
			}
			protected void fieldChangeNotify(int context)
            {
                   if(context==2){
                	   choicesEDOCivil.setFocus();
                   }
            }
		};
	//	par = new String[]{"Seleccione","Soltero(a)","Casado(a)","Divorciado(a)","Viudo(a)","Union Libre"};
		
		
		par = new String[MainClass.parenBeans.length];
		
		for(int i = 0; i < MainClass.parenBeans.length; i++){
			par[i] = MainClass.parenBeans[i].getDescription();
		}
		choicesPare = new ObjectChoiceField("Parentesco: ", par){
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				super.paint(graphics);
				invalidate();
			}
			
			protected void drawFocus(Graphics graphics, boolean on) {
				// TODO Auto-generated method stub
				super.drawFocus(graphics, on);
				invalidate();
			}
			protected void fieldChangeNotify(int context)
            {
                   if(context==2){
                	   choicesPare.setFocus();
                   }
            }
		};
		btnChange =  new GenericImageButtonField(MainClass.BTN_OK_OVER,MainClass.BTN_OK,"ok"){
			
			protected boolean navigationClick(int status, int time) {
			
				//UiApplication.getUiApplication().popScreen(this);	
			
				Date da = new Date(dateField.getDate());
				
				Calendar ca = Calendar.getInstance();
				Calendar cal = Calendar.getInstance();
				ca.setTime(da);		
				
				int valAge = cal.get(Calendar.YEAR) - ca.get(Calendar.YEAR);
				
				if(basicName.getText().trim().equals("")){
		        	
		            Dialog.alert("Ingrese su Nombre.");
		            basicName.setFocus();
		            
		        }else if(basicLastNames.getText().trim().equals("")){
		        	
		            Dialog.alert("Ingrese su apellido paterno.");
		            basicLastNames.setFocus();
		            
		        }
		        else if(valAge > 100){
		        	
		        	Dialog.alert( "Fecha de Nacimiento invalida. El a�o no puede ser anterior a " + (cal.get(Calendar.YEAR)-100) + ".");
		        	
		        }else if(choicesSex.getSelectedIndex()==0){
		        	
		            Dialog.alert("Elija su sexo.");
		            choicesSex.setFocus();
		            
		        }/*else if(choicesEDOCivil.getSelectedIndex()==0){
		        	
		            Dialog.alert("Elija su estado civil.");
		            choicesEDOCivil.setFocus();
		            
		        }*//*else if(choicesPare.getSelectedIndex()==0){
		        	
		            Dialog.alert("Elija el parentesco.");
		            choicesPare.setFocus();
		            
		        }*/else{
		        	Date d = new Date(dateField.getDate());
		    		
		    		
		    		calendar.setTime(da);		
		    		int month = calendar.get(calendar.MONTH)+1;
		    		int day = calendar.get(calendar.DATE);
		    		String strMonth = "";
		    		String strDate = "";
		    		
		    		if(month < 10){
		    			strMonth = "0" + month;
		    		}else{
		    			strMonth = "" + month;
		    		}
		    		
		    		if(day < 10){
		    			strDate = "0" + day;
		    		}else{
		    			strDate = "" + day;
		    		}
		        	
		        	MainClass.Beneficiarios[pos]=new BeneficiarioBean(basicName.getText().trim().toUpperCase(),basicLastNames.getText().toUpperCase(),
		        			basicLastNamesM.getText().toUpperCase(), calendar.get(calendar.YEAR)+"-"+strMonth+"-"+strDate,
		        			""+sex2[choicesSex.getSelectedIndex()],""+MainClass.EdoCivilBeans[choicesEDOCivil.getSelectedIndex()].getClave(),""+MainClass.parenBeans[choicesPare.getSelectedIndex()].getClave());
		        	//BeneficiariosScreen.doit.select(true);
		     if(pos==0){
		    	 BeneficiariosScreen.Uno.setChecked(true);
		    	BeneficiariosScreen.Uno.select(true);
		     }
		     if(pos==1){
		    	 BeneficiariosScreen.Dos.setChecked(true);
		    		BeneficiariosScreen.Dos.select(true);
		     }
		     if(pos==2){
		    	 BeneficiariosScreen.Tres.setChecked(true);
		    	 BeneficiariosScreen.Tres.select(true);
		     }
		     if(pos==3){
		    	 BeneficiariosScreen.Cuatro.setChecked(true);
		    	 BeneficiariosScreen.Cuatro.select(true);
		     }
		        	close();
		        	
		        }
		    		
		        
				
				return true;
			}
			
		};
	}
	
	public void addFields(){
		add(new NullField());
		hfmTitle.add(TipoTag);
		
		add(hfmTitle);
		customVerticalFieldManager.add(new LabelField("",Field.NON_FOCUSABLE));
		if(Display.getWidth()==320||Display.getWidth()==240){
			name.setFont(MainClass.appFont3);
			basicName.setFont(MainClass.appFont3);
			lastNames.setFont(MainClass.appFont3);
			basicLastNames.setFont(MainClass.appFont3);
			lastNamesM.setFont(MainClass.appFont3);
			basicLastNamesM.setFont(MainClass.appFont3);		
			birthDate1.setFont(MainClass.appFont3);
			dateField.setFont(MainClass.appFont3);
			choicesSex.setFont(MainClass.appFont3);
			choicesEDOCivil.setFont(MainClass.appFont3);
			choicesPare.setFont(MainClass.appFont3);
			
		}
		customVerticalFieldManager.add(name);		
		customVerticalFieldManager.add(basicName);		
		customVerticalFieldManager.add(lastNames);		
		customVerticalFieldManager.add(basicLastNames);	
		customVerticalFieldManager.add(lastNamesM);		
		customVerticalFieldManager.add(basicLastNamesM);
		customVerticalFieldManager.add(new LabelField("",Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(birthDate1);
		customVerticalFieldManager.add(dateField);
		customVerticalFieldManager.add(new LabelField("",Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(choicesSex);
		customVerticalFieldManager.add(new LabelField("",Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(choicesEDOCivil);
		customVerticalFieldManager.add(new LabelField("",Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(choicesPare);
		customVerticalFieldManager.add(new LabelField("",Field.NON_FOCUSABLE));	
		hfmBtn.add(btnChange);
		add(customVerticalFieldManager);
		add(hfmBtn);
		
	}

	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		
	}
	
	public boolean keyChar(char key, int status, int time) {
	    //intercept the ESC key
	    boolean retval = false;
	    switch (key) {

	        case Characters.ESCAPE:
	            close();
	            break;
	        default:
	            retval = super.keyChar(key,status,time);
	    }
	    return retval;
	}
	
	public void close() {
		// TODO Auto-generated method stub
		UiApplication.getUiApplication().popScreen(this);
	}
	
}
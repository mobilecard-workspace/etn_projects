package mx.com.ib.views;

import crypto.Crypto;
import mx.com.ib.app.MainClass;
import mx.com.ib.controls.BgManager;
import mx.com.ib.controls.BorderBasicEditField;
import mx.com.ib.controls.CustomVerticalFieldManager;
import mx.com.ib.controls.GenericImageButtonField;
import mx.com.ib.controls.HorizontalFieldManagerS;
import mx.com.ib.threads.PasswordThread;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;


public class RecPassScreen extends MainScreen{
	
	public BgManager bgManager = null;
	
//	public LabelField title = null;
	public LabelField user = null,helpUser=null;
	public LabelField note=null;
	
	public LabelField recUser=null;
	
	public BorderBasicEditField basicUser = null;
	
	public HorizontalFieldManager hfmMain = null;
	public HorizontalFieldManager hfmTitle = null;
	public HorizontalFieldManager hfmbtn = null;

	public HorizontalFieldManager hfmUser = null;
	
	public HorizontalFieldManagerS hfmBtns = null;
	
	public VerticalFieldManager vfmFooter = null;
	
	public CustomVerticalFieldManager customVerticalFieldManager = null;
	public CustomVerticalFieldManager customVerticalFieldManager2 = null;
	
	public GenericImageButtonField btnOk = null;	
	
//	public String json = "";
	
	public RecPassScreen(){
	
		initVariables();
		
		addFields();
		
	}
	
	public void initVariables(){
		 TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
		    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
		    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
		    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
		    UiEngineInstance engine = Ui.getUiEngineInstance();
		    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
		     TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
		     transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 2000);
		     transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
		     transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
		    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
		    	 engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
	    		
		bgManager = new BgManager(MainClass.BG_GRAY);

		user = new LabelField("Digite su Usuario: ");
		user.setFont(MainClass.appFont);
		
		hfmMain = new HorizontalFieldManager(VerticalFieldManager.FIELD_HCENTER|HorizontalFieldManager.FIELD_HCENTER|Field.FIELD_HCENTER|VERTICAL_SCROLL){
			public int getPreferredWidth() {
				// TODO Auto-generated method stub
				return Display.getWidth();
			}
			
			protected void sublayout(int maxWidth, int maxHeight) {
				// TODO Auto-generated method stub
				super.sublayout(getPreferredWidth(), maxHeight);
			}
		};
		hfmBtns = new HorizontalFieldManagerS(HorizontalFieldManager.FIELD_HCENTER| HorizontalFieldManager.USE_ALL_WIDTH,Bitmap.getBitmapResource(MainClass.BTN_REGISTER).getWidth());
		hfmTitle = new HorizontalFieldManager(Field.FIELD_HCENTER);
		hfmbtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		hfmUser = new HorizontalFieldManager(Field.FIELD_LEFT);
		
		
		vfmFooter = new VerticalFieldManager(VERTICAL_SCROLL){
			public int getPreferredWidth() {
				// TODO Auto-generated method stub
				return Display.getWidth();
			}
			
			protected void sublayout(int maxWidth, int maxHeight) {
				// TODO Auto-generated method stub
				super.sublayout(getPreferredWidth(), maxHeight);
			}
		};
		
		customVerticalFieldManager = new CustomVerticalFieldManager(Display.getWidth(),Display.getHeight(), HorizontalFieldManager.FIELD_LEFT| VERTICAL_SCROLL);
		//customVerticalFieldManager2 = new CustomVerticalFieldManager(Display.getWidth(),145, HorizontalFieldManager.FIELD_LEFT| VERTICAL_SCROLL);
		
		basicUser = new BorderBasicEditField("","", 16, Display.getWidth(), Field.FOCUSABLE);
		
		note =new LabelField("Para recuperar su contrase�a es necesario ingresar el usuario con el cual se registr�. Un correo ser� enviado a la direcci�n que ingres� en el registro con su contrase�a.");
		btnOk =  new GenericImageButtonField(MainClass.BTN_OK_OVER,MainClass.BTN_OK,"ok"){
			
			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub				
				if(!basicUser.getText().equals("")){
					
					
				MainClass.splashScreen.start();
				String cadena=Crypto.aesEncrypt(MainClass.key, "{\"cadena\":\""+basicUser.getText().trim()+"\"}");
				
				new PasswordThread(cadena).start();
				
				
				
				
			
				}else{
					
					Dialog.alert("Escribe tu nombre de Usuario");
				}
				return true;
			}
			
		};		
		
		recUser =new LabelField("Olvid� mi nombre de Usuario",Field.FOCUSABLE){
			
			public void paint(Graphics g){
				
				
				g.setColor(MainClass.COLOR_LINK);
				super.paint(g);
			}
			protected boolean navigationClick(int status, int time) {
				
				UiApplication.getUiApplication().pushScreen(new RecUserScreen());
				return true;
			}
			protected void onFocus(int direction) {  
		        super.onFocus(direction);
		        invalidate(); 
		     //   
		    }   
		 
			 protected void onUnfocus() {  
		           super.onUnfocus();
		           invalidate();  
		    } 
			
			
		};
		recUser.setFont(Font.getDefault().derive(Font.UNDERLINED));
		
		
		helpUser =new LabelField("?",Field.FOCUSABLE){
			
			public void paint(Graphics g){
				
				
				g.setColor(MainClass.BG_ORANGE);
				super.paint(g);
			}
			protected boolean navigationClick(int status, int time) {
				Dialog.inform("El nombre de usuario es de m�ximo 16 caracteres");
				//UiApplication.getUiApplication().pushScreen(new RecUserScreen());
				return true;
			}
			protected void onFocus(int direction) {  
		        super.onFocus(direction);
		        invalidate(); 
		     //   
		    }   
		 
			 protected void onUnfocus() {  
		           super.onUnfocus();
		           invalidate();  
		    } 
			
			
		};
		
	}
	
	public void addFields(){		
		
		add(new NullField());
		BitmapField bField = new BitmapField(Bitmap.getBitmapResource(MainClass.BG_LOGIN));
		
		customVerticalFieldManager.add(bField);
		customVerticalFieldManager.add(new LabelField(""));
	
		hfmUser.add(user);
		hfmUser.add(helpUser);
		customVerticalFieldManager.add(hfmUser);
		customVerticalFieldManager.add(basicUser);
		
		customVerticalFieldManager.add(new LabelField(""));		
		
		hfmBtns.add(btnOk);
		
		customVerticalFieldManager.add(hfmBtns);
		customVerticalFieldManager.add(new LabelField("",Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(note);
		recUser.setPadding(20, 0, 0, 0);
		hfmbtn.add(recUser);
		customVerticalFieldManager.add(hfmbtn);
		customVerticalFieldManager.add(new LabelField("",Field.NON_FOCUSABLE));
		
		hfmMain.add(customVerticalFieldManager);
		
		vfmFooter.add(hfmMain);
		
		bgManager.add(vfmFooter);
				
		add(bgManager);		
		
	}
	
	protected boolean onSavePrompt() {
		return true;
	}
	
	public void cleanFields(){
		
		basicUser.setText("");
		
	}
	

	
	protected boolean keyDown(int keycode, int time)
	   {
	        if (Keypad.key(keycode) == Characters.ESCAPE) 
	        {
	        	close();
	        	return true;
	        }
	        return false;
	   }
	
}

package mx.com.ib.views;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import mx.com.ib.app.MainClass;
import mx.com.ib.etn.utils.UtilIcon;
import mx.com.ib.etn.view.controls.CustomSelectedButtonField;
import mx.com.ib.threads.implement.ObtenerLineas;
import mx.com.ib.threads.implement.InsertaLinea;
import mx.com.ib.views.components.ColorRichTextField;
import mx.com.ib.views.components.Viewable;
import net.rim.blackberry.api.browser.URLEncodedPostData;
import net.rim.device.api.browser.field2.BrowserField;
import net.rim.device.api.browser.field2.BrowserFieldConfig;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.RichTextField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

//?user=pruebagdf&ref=3904823849872349873~MBN6901&monto=300&mod=edomex
//http://www.addcelapp.com:12345/getId.aspx?user=pruebagdf&ref=3904823849872349873~MBN6901&monto=300&mod=edomex

public class CobroPlacaEdoMExico extends MainScreen implements
		FieldChangeListener, Viewable {

	private VerticalFieldManager bgManagerGray = null;

	private CustomSelectedButtonField buttonField = null;
	private CustomSelectedButtonField venta = null;

	private EditField placa;
	private EditField celular;
	private EditField captura;
	private EditField monto;

	private String login;
	private String sPlaca;
	private String sMontos;
	private String sLinea;

	private String tempPlaca;
	private String tempCelular;

	
	public CobroPlacaEdoMExico() {

		initVariables();
		login = MainClass.userBean.getLogin();
	}


	public void initVariables() {
		TransitionContext transition = new TransitionContext(
				TransitionContext.TRANSITION_SLIDE);
		transition.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
		transition.setIntAttribute(TransitionContext.ATTR_DIRECTION,
				TransitionContext.DIRECTION_LEFT);
		transition.setIntAttribute(TransitionContext.ATTR_STYLE,
				TransitionContext.STYLE_PUSH);
		UiEngineInstance engine = Ui.getUiEngineInstance();
		engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH,
				transition);
		TransitionContext transition2 = new TransitionContext(
				TransitionContext.TRANSITION_SLIDE);
		transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 2000);
		transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION,
				TransitionContext.DIRECTION_RIGHT);
		transition2.setIntAttribute(TransitionContext.ATTR_STYLE,
				TransitionContext.STYLE_PUSH);
		engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP,
				transition2);
		engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);

		Background backgroundMainScreen = BackgroundFactory
				.createSolidBackground(0x414042);

		VerticalFieldManager fieldManager = new VerticalFieldManager();

		fieldManager.setBackground(backgroundMainScreen);

		BitmapField bitmapField = UtilIcon.getTitleEDO();

		fieldManager.add(bitmapField);

		ColorRichTextField textField01 = new ColorRichTextField(
				"Descripci�n del producto:", Color.WHITE);
		ColorRichTextField textField02 = new ColorRichTextField(
				"Pago servicios Edo. de M�xico", Color.WHITE);

		fieldManager.add(textField01);
		fieldManager.add(textField02);
		fieldManager.add(new LabelField(""));

		ColorRichTextField textField03 = new ColorRichTextField(
				"N�mero de placa:", Color.WHITE);
		fieldManager.add(textField03);

		placa = new EditField("", "") {

			public void paint(Graphics graphics) {
				graphics.setColor(Color.BLACK);
				super.paint(graphics);
			}

		};
		Background whitebackground = BackgroundFactory
				.createSolidBackground(Color.WHITE);
		placa.setBackground(whitebackground);
		fieldManager.add(placa);

		ColorRichTextField textField04 = new ColorRichTextField(
				"N�mero de tel�fono celular:", Color.WHITE);
		fieldManager.add(textField04);

		celular = new EditField("", "") {

			public void paint(Graphics graphics) {
				graphics.setColor(Color.BLACK);
				super.paint(graphics);
			}
		};
		celular.setBackground(whitebackground);
		fieldManager.add(celular);

		buttonField = new CustomSelectedButtonField("Validar",
				ButtonField.CONSUME_CLICK);
		buttonField.setChangeListener(this);
		fieldManager.add(new LabelField(""));
		fieldManager.add(buttonField);
		fieldManager.add(new LabelField(""));

		Background background01 = BackgroundFactory
				.createSolidBackground(0xAAAAAA);
		bgManagerGray = new VerticalFieldManager(
				VerticalFieldManager.USE_ALL_WIDTH);
		bgManagerGray.setBackground(background01);

		Background background02 = BackgroundFactory
				.createSolidBackground(0xCCCCCC);
		RichTextField textField05 = new RichTextField("L�nea de captura:",
				RichTextField.NON_FOCUSABLE);
		captura = new EditField(EditField.NON_FOCUSABLE) {

			public void paint(Graphics graphics) {
				graphics.setColor(Color.BLACK);
				super.paint(graphics);
			}
		};
		captura.setBackground(background02);

		bgManagerGray.add(textField05);
		bgManagerGray.add(captura);
		bgManagerGray.add(new LabelField(""));

		RichTextField textField06 = new RichTextField("Monto:",
				RichTextField.NON_FOCUSABLE);
		monto = new EditField(EditField.NON_FOCUSABLE) {

			public void paint(Graphics graphics) {
				graphics.setColor(Color.BLACK);
				super.paint(graphics);
			}
		};
		monto.setBackground(background02);

		bgManagerGray.add(textField06);
		bgManagerGray.add(monto);
		bgManagerGray.add(new LabelField(""));

		fieldManager.add(bgManagerGray);

		venta = new CustomSelectedButtonField("Aceptar",
				ButtonField.CONSUME_CLICK);
		venta.setChangeListener(this);
		fieldManager.add(new LabelField(""));
		fieldManager.add(venta);
		add(fieldManager);

	}

	public void fieldChanged(Field field, int context) {

		if (field == buttonField) {

			if (checkFields()) {

				String json = createJson();
				
				captura.setText("");
				monto.setText("");
				
				ObtenerLineas obtenerLineas = new ObtenerLineas(json, this);
				obtenerLineas.run();
				
			} else {
				Dialog.alert("Faltan llenar campos.");
			}

		} else if (field == venta) {

			String post = createPost();
			InsertaLinea insertaLinea = new InsertaLinea(post, this);
			insertaLinea.run();
		}
	}


	private boolean checkFields() {

		tempPlaca = placa.getText();
		tempCelular = celular.getText();

		if ((tempPlaca != null) && (tempPlaca.length() > 0)
				&& (tempCelular != null) && (tempCelular.length() > 0)) {

			return true;
		} else {
			return false;
		}
	}

	private String createJson() {

		StringBuffer json = new StringBuffer();

		String backSlash = "\"";
		String coma = ",";

		json.append("{");
		json.append("telefono:").append(backSlash).append(tempCelular)
				.append(backSlash).append(",");
		json.append("placa:").append(backSlash).append(tempPlaca).append(backSlash).append(",");
		json.append("login:").append(backSlash).append(login).append(backSlash);
		json.append("}");

		return json.toString();
	}

	

	private String createPost(){
		
		StringBuffer post = new StringBuffer();

		String equal = "=";
		String and = "&";
		
		post.append("?user=").append(login).append(and);
		//post.append("?user=").append("pruebagdf").append(and);
		post.append("ref=").append(sLinea).append("~").append(sPlaca).append(and);
		post.append("monto=").append(sMontos).append(and);
		post.append("mod=edomex");
		
		return post.toString();
	}

	
	public void setData(int request, final JSONObject jsObject) {

		final JSONObject jsObject5 = jsObject;

		UiApplication.getUiApplication().invokeLater(new Runnable() {
			public void run() {

				String temMessage = null;


				
				try {

					if(jsObject5.has("insercion")){
						
						String referencia = jsObject5.getString("insercion");
						
						
						URLEncodedPostData encodedPostData = new URLEncodedPostData(
								URLEncodedPostData.DEFAULT_CHARSET, false);
						
						
						String temp = String.valueOf(sMontos);
						
						int index = sMontos.indexOf("$");
						
						if (index >= 0){
							
							sMontos = sMontos.substring(1);
						}
						
						sMontos = Double.valueOf(sMontos).toString();
						
						encodedPostData.append("user", login);
						encodedPostData.append("referencia", referencia);
						encodedPostData.append("monto", sMontos);
						encodedPostData.append("mod", "edomex");
						
						
						BrowserFieldConfig config = new BrowserFieldConfig();
				        config.setProperty(BrowserFieldConfig.ENABLE_COOKIES,Boolean.TRUE);
				        config.setProperty(BrowserFieldConfig.JAVASCRIPT_ENABLED, Boolean.TRUE);
				        config.setProperty(BrowserFieldConfig.NAVIGATION_MODE,BrowserFieldConfig.NAVIGATION_MODE_POINTER);

				        BrowserField browserField = new BrowserField(config); 
				        //browserField.addListener(new MyBrowserFieldListener(this));
					    //browserField.requestContent(url);
					    
				        String URL = "http://mobilecard.mx:8080/EdoMexWeb/envio.jsp" + "?" + encodedPostData.toString();
				        
				        browserField.requestContent(URL);
						
						MainScreen mainScreen = new MainScreen();
						
						mainScreen.add(browserField);
						
						UiApplication.getUiApplication().pushScreen(mainScreen);
						
						
						
						//Dialog.alert(temMessage);

					} else {
						
						int resultado = jsObject5.getInt("resultado");

						if (resultado == 9) {
							
							/*
							temMessage = jsObject5.toString();
							char[] tempChar = temMessage.toCharArray();

							int length = tempChar.length;
							StringBuffer stringBuffer = new StringBuffer();
							for (int index = 0; index < length; index++) {

								if (!((tempChar[index] == '\\') || (tempChar[index] == '"'))) {
									stringBuffer.append(tempChar[index]);
								}
							}

							temMessage = stringBuffer.toString();

							JSONObject jsonObject2 = new JSONObject(temMessage);

							JSONObject jsonObject3 = jsonObject2
									.getJSONObject("mensaje");

							sPlaca = jsonObject3.getString("placa");
							sMontos = jsonObject3.getString("monto");
							sLinea = jsonObject3.getString("linea");

							captura.setText(sLinea);
							monto.setText(sMontos);
							 */
							
/*
							temMessage = jsObject5.toString();
							char[] tempChar = temMessage.toCharArray();

							int length = tempChar.length;
							StringBuffer stringBuffer = new StringBuffer();
							for (int index = 0; index < length; index++) {

								if (!((tempChar[index] == '\\') || (tempChar[index] == '"'))) {
									stringBuffer.append(tempChar[index]);
								}
							}

							temMessage = stringBuffer.toString();

							JSONObject jsonObject2 = new JSONObject(temMessage);
*/
							JSONObject jsonObject3 = jsObject5
									.getJSONObject("mensaje");

							sPlaca = jsonObject3.getString("placa");
							sMontos = jsonObject3.getString("monto");
							sLinea = jsonObject3.getString("linea");

							captura.setText(sLinea);
							monto.setText(sMontos);
							
							
						} else if (resultado > 0 ){
							
							String mensaje = jsObject5.getString("mensaje");
							
							Dialog.alert(mensaje);
							
							//{"resultado":3,"mensaje":"NO EXISTE LA PLACA (GVFU4)"}
							
						} else {
							String referencia = jsObject5.getString("mensaje");
							
							/*
								URL -> "http://mobilecard.mx:8080/EdoMexWeb/envio.jsp";
								PARAMS ->
	        						params.put("user", Usuario.getLogin(EdomexPlacaActivity.this)); -> Nombre de Usuario MobileCard
	        						params.put("referencia", this.idTransaccion); -> Id que regresa el WS
	        						params.put("monto", Double.toString(this.monto)); -> Monto a pagar
	        						params.put("mod", "edomex")
							*/
							
							

						}
					}

				} catch (JSONException e) {
					e.printStackTrace();
					Dialog.alert("Error al interpretar la respuesta.");
				}
			}
		});
	}


	public void sendMessage(String message) {
		// TODO Auto-generated method stub
		final String messages = message;

		UiApplication.getUiApplication().invokeLater(new Runnable() {
			public void run() {
				Dialog.alert(messages);
			}
		});
	}
}

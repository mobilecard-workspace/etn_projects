package mx.com.ib.views;

import mx.com.ib.app.MainClass;
import mx.com.ib.controls.BannerBitmap;
import mx.com.ib.controls.BgManager;
import mx.com.ib.controls.CustomVerticalFieldManager;
import mx.com.ib.controls.LabelImage;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;
import net.rim.device.api.system.KeypadListener;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.RichTextField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

import mx.com.ib.threads.threadPromoImages;

public class PromotionsScreen extends MainScreen{
	
	public BgManager bgManager = null;
	public VerticalFieldManager vfmMAin = null;
	public CustomVerticalFieldManager customVerticalFieldManager = null;
	
	public LabelImage buttonLeft = new LabelImage(MainClass.BTN_IZQ, Field.NON_FOCUSABLE | Field.FIELD_HCENTER);
	public LabelImage buttonRight = new LabelImage(MainClass.BTN_DER, Field.NON_FOCUSABLE| Field.FIELD_HCENTER);
	public LabelField counterText = null;
//	public VerticalFieldManager banner = null;
	public HorizontalFieldManager buttonManager = null;
	public HorizontalFieldManager bitmapManager = null;
	public BitmapField bitmapField = null;
	public int galleryPos = 0;
	public int galleryLength = MainClass.promotionLabelField.length;
	public int widthImage = 0;
	public RichTextField text = null;
//	public String texto = "123456786543456ujhygfrrgnhjfdfbgnh  fdvfrre reifugc erfyerg fu3fg eihgdfc erifg rfhc rfuhr cfreuifhc efruihcr efiurhe cfreifhc eruifhcreuifhcvfbvfj vberifbgrfrcjfhbr vhjfbr hvb rfiuhf crifbjre feribvfceribr ivbfrvbhjefr vbhriyvr4v fevbhf vefrivb4fghrivcb erhbcfr ivfgr4 rfghreifvbefjpbhcfr crf";
	
	//public BitmapField headerBitmapField = null;
	//public BitmapField footerBitmapField = null;
	
	public PromotionsScreen(){
	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	     TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	     transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	     transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	     transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    	 engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
		bgManager = new BgManager(0xFFFFFF);
		
		vfmMAin = new VerticalFieldManager(VERTICAL_SCROLL|Field.FIELD_HCENTER);
		customVerticalFieldManager = new CustomVerticalFieldManager(Display.getWidth()-20,Display.getHeight()-30, HorizontalFieldManager.FIELD_HCENTER|VERTICAL_SCROLL){
			public void paint(Graphics graphics) {
				// Draw the background image and then call paint.
			
				 //graphics.drawBitmap(0, 0, Display.getWidth()-20, Display.getHeight()+60, Utils.resizeBitmap(Bitmap.getBitmapResource("bgPromos.png"), Display.getWidth()-20, Display.getHeight()+60), 0, 0);
				super.paint(graphics);
			}
		};
		
		setBanner(new BannerBitmap(0xFFFFFF, MainClass.HD_PROMOS));
		
		counterText = new LabelField(" "+(galleryPos+1) + " de " + " " +galleryLength, Field.NON_FOCUSABLE);
		buttonManager = new HorizontalFieldManager(Field.FIELD_HCENTER)
		{

			protected void paintBackground(Graphics graphics) {
				graphics.setColor(Color.BLACK);
				graphics.fillRect(0, 0, Display.getWidth(), Display.getHeight());
				graphics.setColor(Color.WHITE);
				super.paint(graphics);
			}
		};
		
		bitmapManager= new HorizontalFieldManager(Field.FIELD_HCENTER);
		
		buttonManager.add(buttonLeft);
		buttonManager.add(counterText);
		buttonManager.add(buttonRight);
		
		
		
		widthImage = Display.getHeight()-20-counterText.getHeight();
		
		bitmapField = new BitmapField(MainClass.promotionLabelField[galleryPos].getImage(), Field.FOCUSABLE){
/*			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub
				MainClass.splashScreen.start();
				new ProductsThread().start();
				return true;
			}
			*/
			
			protected void layout(int width, int height) {
				// TODO Auto-generated method stub
				super.layout(Display.getWidth()-20, height);
			}
			
			public void setBitmap(Bitmap bitmap) {
							// TODO Auto-generated method stub
							super.setBitmap(bitmap);
							invalidate();
						}
		};		
		
		text = new RichTextField(MainClass.promotionLabelField[galleryPos].getText()){
			protected void layout(int width, int height) {
				// TODO Auto-generated method stub
				super.layout(Display.getWidth()-20, height);
			}
			
			protected void paint(Graphics g){
				g.setColor(0x000000);
				super.paint(g);
			}
		};
		
		//customVerticalFieldManager.add(headerBitmapField);
		customVerticalFieldManager.add(buttonManager);
		
//		bitmapManager.add(bitmapField);
		bitmapManager.add(MainClass.promotionLabelField[galleryPos]);
		
		customVerticalFieldManager.add(bitmapManager);
		
		customVerticalFieldManager.add(text);
		
		customVerticalFieldManager.add(new LabelField(""));

		//customVerticalFieldManager.add(footerBitmapField);
		
		vfmMAin.add(customVerticalFieldManager);
		
		bgManager.add(vfmMAin);
		
		add(bgManager);
		
		new threadPromoImages(MainClass.promotionLabelField, 0 , MainClass.promotionLabelField.length).start();
		setStatus(new BannerBitmap(0xFFFFFF));
		
	}
	
	protected boolean navigationMovement(int dx, int dy, int status, int time)
	{
		if(status == KeypadListener.STATUS_FOUR_WAY)
		{
			if(dx>0)
			{
				if (galleryPos+1 < galleryLength) {
					galleryPos++;
					counterText.setText(" "+(galleryPos+1) + " de " + " " +galleryLength);
//					bitmapField.setBitmap((MainClass.promotionLabelField[galleryPos].getImage()));
					bitmapManager.deleteAll();
					bitmapManager.add(MainClass.promotionLabelField[galleryPos]);
//					text.setText(texto);
					text.setText(MainClass.promotionLabelField[galleryPos].getText());
				}
				
			}
			if(dx<0)
			{
				if (galleryPos > 0) {
					galleryPos--;
					counterText.setText(" "+(galleryPos+1) + " de " + " " +galleryLength);
//					bitmapField.setBitmap(MainClass.promotionLabelField[galleryPos].getImage());
					bitmapManager.deleteAll();
					bitmapManager.add(MainClass.promotionLabelField[galleryPos]);
					
					text.setText(MainClass.promotionLabelField[galleryPos].getText());
				}
				
			}
		}
		return false;
	}
	
	protected boolean keyDown(int keycode, int time)
	   {
		if (Keypad.key(keycode) == Characters.ESCAPE) 
	        {
	        	close();
	        	return true;
	        }
	        return false;
	   }
	
	public void close() {
		// TODO Auto-generated method stub
		UiApplication.getUiApplication().popScreen(this);
	}	

}


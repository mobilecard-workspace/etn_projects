package mx.com.ib.views.components;

import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;

public class ColorRichTextField extends RichTextField {

	int color;
	
	public ColorRichTextField(String text, int color){
		super(text, RichTextField.NON_FOCUSABLE);
		this.color = color;
	}

	public void paint(Graphics graphics) {
	    graphics.setColor(color);
	    super.paint(graphics);
	}
	
}

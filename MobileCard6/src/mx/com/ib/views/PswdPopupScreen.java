package mx.com.ib.views;



import crypto.Crypto;
import mx.com.ib.app.MainClass;
import mx.com.ib.controls.BorderPasswordField;
import mx.com.ib.controls.GenericImageButtonField;
import mx.com.ib.threads.PswdThread;
import mx.com.ib.utils.Utils;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class PswdPopupScreen extends PopupScreen implements FieldChangeListener{
	
	public LabelField actualPswd = null;
	public LabelField pswd = null;
	public LabelField confirmPswd = null;
	public LabelField title = null;
	
	public BorderPasswordField basicActualPassword = null;
	public BorderPasswordField basicPassword = null;
	public BorderPasswordField basicPasswordConfirm = null;
	
	public HorizontalFieldManager hfmTitle = null;
	public HorizontalFieldManager hfmBtn = null;
	
	public int widthManager = 0;
	
	public GenericImageButtonField btnChange = null;
	
	public PswdPopupScreen(String text, long style){
		
		super(new VerticalFieldManager(VERTICAL_SCROLL));
		
		initVariables();
		
		addFields();
		
	}
	
	public void initVariables(){
	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	     TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	     transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 2000);
	     transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	     transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    	 engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
		hfmTitle = new HorizontalFieldManager(Field.FIELD_HCENTER);
		hfmBtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		
		widthManager = Display.getWidth() - 100;
		
		actualPswd = new LabelField("Contraseña Actual: ", Field.NON_FOCUSABLE);
		pswd = new LabelField("Nuevo Contraseña: ", Field.NON_FOCUSABLE);
		confirmPswd = new LabelField("Confirme Contraseña: ", Field.NON_FOCUSABLE);
		title = new LabelField("Cambio de Contraseña", Field.NON_FOCUSABLE);
		
		basicActualPassword = new BorderPasswordField("","", 12, this.getWidth(), Field.FOCUSABLE);
		basicPassword = new BorderPasswordField("","", 12, this.getWidth(), Field.FOCUSABLE);
		basicPasswordConfirm = new BorderPasswordField("","", 12, this.getWidth(), Field.FOCUSABLE);
		
		btnChange =  new GenericImageButtonField(MainClass.BTN_OK_OVER, MainClass.BTN_OK,"continuar"){
			
			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub
				
				checkPaswd();
				return true;
			}
			
		};
		
	}
	
	public void addFields(){
		
		hfmTitle.add(title);
		hfmBtn.add(btnChange);
		add(hfmTitle);
		add(new LabelField(""));
		add(actualPswd);
		add(basicActualPassword);
		add(new LabelField(""));
		add(pswd);
		add(basicPassword);
		add(new LabelField(""));
		add(confirmPswd);
		add(basicPasswordConfirm);
		add(new LabelField(""));
		add(hfmBtn);
		add(new LabelField(""));
		
	}

	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		
	}
	
	public boolean keyChar(char key, int status, int time) {
	    //intercept the ESC key
	    boolean retval = false;
	    switch (key) {

	        case Characters.ESCAPE:
	            close();
	            break;
	        default:
	            retval = super.keyChar(key,status,time);
	    }
	    return retval;
	}
	
	public void close() {
		// TODO Auto-generated method stub
		UiApplication.getUiApplication().popScreen(this);
	}
	
	public void checkPaswd(){
		
		if(!basicActualPassword.getText().trim().equals("")){
			
			if(basicActualPassword.getText().length() >= 3){

				
				if(!basicPassword.getText().trim().equals("")){
				
					if(basicPassword.getText().length() >= 8){
						
						if(basicPassword.getText().trim().equals(basicPasswordConfirm.getText().trim())){
							
							MainClass.splashScreen.start();	
							String pas=Crypto.aesEncrypt(Utils.parsePass(MainClass.p),updatePswd());
							String pass=Utils.mergeStr(pas, MainClass.p);
							new PswdThread("json="+pass).run();
							
						}else{
							
							UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("El password debe de coincidir con la confirmación.", Field.NON_FOCUSABLE));
							
						}
						
					}else{
						
						UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Su nuevo password debe de contener al menos 8 caracteres.", Field.NON_FOCUSABLE));
						
					}
					
				}else{
					
					UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Debe de ingresar su nuevo password.", Field.NON_FOCUSABLE));
					
				}						
				
			}else{
				
				UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Su password debe de contener al menos 8 caracteres.", Field.NON_FOCUSABLE));
				
			}
			
		}else{
			
			UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Debe de ingresar el password actual.", Field.NON_FOCUSABLE));
			
		}
		
//		if(!basicPassword.getText().trim().equals("")&&!basicPasswordConfirm.getText().trim().equals("")){
//			
//			if(basicPassword.getText().trim().equals(basicPasswordConfirm.getText().trim())){
//				
//				MainClass.splashScreen.start();				
//				new PswdThread("json="+Crypto.aesEncrypt(MainClass.key,updatePswd())).run();
////				UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("json="+updatePswd(), Field.NON_FOCUSABLE));
//				
//			}else{
//				
//				UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("El password debe de coincidir con la confirmacion.", Field.NON_FOCUSABLE));
//				
//			}
//			
//		}else{
//			
//			UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Ingrese password y confirmacion.", Field.NON_FOCUSABLE));
//			
//		}
		
	}
	
	public String updatePswd(){		
		
		StringBuffer jsonPswd = new StringBuffer("{\"login\":\"");
		
		
		
		jsonPswd.append(MainClass.userBean.getLogin());
		
		jsonPswd.append("\",\"password\":\""+basicActualPassword.getText().trim()+"\",\"newPassword\":\"");		
		
		jsonPswd.append(basicPassword.getText().trim()+"\"}");
		
		MainClass.userPswd = basicPassword.getText().trim(); 
		
		
		
		return jsonPswd.toString();
		
	}

}

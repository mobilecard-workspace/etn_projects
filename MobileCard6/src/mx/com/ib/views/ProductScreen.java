package mx.com.ib.views;

import mx.com.ib.app.MainClass;
import mx.com.ib.beans.GeneralBean;
import mx.com.ib.controls.BannerBitmap;
import mx.com.ib.controls.BgManager;
import mx.com.ib.controls.CustomVerticalFieldManager;
import mx.com.ib.controls.ProductLabelField;
import mx.com.ib.views.confirm.ImageScreen;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class ProductScreen extends MainScreen{
	
public BgManager bgManager = null;
	
	public LabelField title,vita = null;
	public LabelField products = null;
	
	public HorizontalFieldManager hfmMain = null;
	public HorizontalFieldManager hfmTitle,hfmTitle2 = null;
	
	public CustomVerticalFieldManager vfmFooter = null;
	
	public CustomVerticalFieldManager customVerticalFieldManager = null;
	
	public GeneralBean[] bankBeans = null;

	public ProductScreen(GeneralBean[] bankBeans){
		
		this.bankBeans = bankBeans;
		
		initVariables();
		
		addFields();
		
		products.setText(MainClass.textTitleInternal);
		
	}

	public void initVariables(){
	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 200);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	     TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	     transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	     transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	     transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    	 engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
	    	 
	    	 
	    	 bgManager = new BgManager(MainClass.BG_GRAY);
		
		title = new LabelField("Cantidad a pagar"){
			
			public void paint(Graphics g){
				
				
				g.setColor(MainClass.BG_ORANGE);
				super.paint(g);
			}
		};
		if(MainClass.IDPROVEEDOR.endsWith("9")){
			title = new LabelField("Cantidad a pagar"){
				public void paint(Graphics g){
					
					
					g.setColor(MainClass.BG_ORANGE);
					super.paint(g);
				}
			};	
			
		}
		
		products = new LabelField("");
		
		hfmMain = new HorizontalFieldManager(VerticalFieldManager.FIELD_HCENTER|VERTICAL_SCROLL);
		hfmTitle = new HorizontalFieldManager(Field.FIELD_RIGHT);
		hfmTitle2 = new HorizontalFieldManager(Field.FIELD_HCENTER);
		
//		vfmFooter = new VerticalFieldManager(VERTICAL_SCROLL);
//		
//		customVerticalFieldManager = new CustomVerticalFieldManager(Display.getWidth()-20,Display.getHeight()-Bitmap.getBitmapResource("footerTexto.png").getHeight(), HorizontalFieldManager.FIELD_LEFT);
//		
vfmFooter = new CustomVerticalFieldManager(Display.getWidth(),Display.getHeight()-30, HorizontalFieldManager.FIELD_LEFT | VERTICAL_SCROLL);
		
		customVerticalFieldManager = new CustomVerticalFieldManager(Display.getWidth()-20, HorizontalFieldManager.FIELD_HCENTER| VERTICAL_SCROLL);
		vita =new LabelField("�Qu� es Membres�a Vitam�dica?",Field.FOCUSABLE){
			
			public void paint(Graphics g){
				
				
				g.setColor(MainClass.COLOR_LINK);
				super.paint(g);
			}
			protected boolean navigationClick(int status, int time) {
				
				MainClass.splashScreen.start();
				UiApplication.getUiApplication().pushScreen(new ImageScreen());
				return true;
			}
			protected void onFocus(int direction) {  
		        super.onFocus(direction);
		        invalidate(); 
		     //   
		    }   
		 
			 protected void onUnfocus() {  
		           super.onUnfocus();
		           invalidate();  
		    } 
			 protected boolean touchEvent (TouchEvent message) {
				   switch(message.getEvent()) {
				      case TouchEvent.CLICK:
				    		MainClass.splashScreen.start();
				    		UiApplication.getUiApplication().pushScreen(new ImageScreen()); 

				         return true;
				   }
				   return false; 
				}
			
			
		};

	}
	
	public void addFields(){
		
		setBanner(new BannerBitmap(MainClass.HD_TIENDA));
		
		add(new NullField());
		customVerticalFieldManager.add(products);
		hfmTitle.add(title);
		customVerticalFieldManager.add(hfmTitle);
		customVerticalFieldManager.add(new LabelField("",Field.NON_FOCUSABLE));
		if(MainClass.IDPROVEEDOR.endsWith("9")){
			customVerticalFieldManager.add(vita);
			customVerticalFieldManager.add(new LabelField("",Field.NON_FOCUSABLE));
		}
		
		hfmMain.add(customVerticalFieldManager);
		
		vfmFooter.add(hfmMain);
		
			for(int i = 0; i < this.bankBeans.length; i++){
				
				ProductLabelField productLabelField = new ProductLabelField(this.bankBeans[i], true);
				productLabelField.setPreferredWidth(Display.getWidth());
				productLabelField.setPreferredHeight(productLabelField.getFont().getHeight()+10);
				vfmFooter.add(productLabelField);
			}
		
		vfmFooter.add(new LabelField(""));
		bgManager.add(vfmFooter);
		add(bgManager);
		setStatus(new BannerBitmap());
	}

	
}

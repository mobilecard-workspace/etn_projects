package mx.com.ib.views;



import crypto.Crypto;

import mx.com.ib.app.MainClass;
import mx.com.ib.beans.TipoRecargaTag;
import mx.com.ib.controls.BorderBasicEditField;
import mx.com.ib.controls.GenericImageButtonField;
import mx.com.ib.threads.GetTagsThread;
import mx.com.ib.threads.SetTagThread;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.ChoiceField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.text.TextFilter;

public class TagsPopupScreen extends PopupScreen implements FieldChangeListener{
	public static final int CLAVE_IAVE = 1;
	public static final int CLAVE_OHL = 2;
	public static final int CLAVE_VIAPASS = 3;
	public static final int CLAVE_PASE = 4;
	public static final int CLAVE_OTRO = 0;
	
	public int CLAVE_NOW = 0;
	
	public LabelField TipoTag = null;
	public LabelField nick = null,pin=null,clave=null;
	public LabelField img2 = null;
	public LabelField img3 = null;	
	public LabelField imgnick = null;	
	
	
	public BorderBasicEditField basicLabel = null;	
	public BorderBasicEditField basicClave = null;
	public BorderBasicEditField basicPin = null;	
	
	public HorizontalFieldManager hfmTitle = null;
	public HorizontalFieldManager hfmBtn = null;
	public HorizontalFieldManager hfmnick = null;
	public HorizontalFieldManager hfmCards = null;
	
	public HorizontalFieldManager tagHFM = null;
	public HorizontalFieldManager dvHFM = null;
	
	public String tagNames[] = null;
	public ObjectChoiceField choicesTagNames  = null;
	
	public int widthManager = 0;
	public boolean isLogin = false;
	
	public GenericImageButtonField btnChange = null;
	private NewAccountScreen actualizate=null;
	
	public TagsPopupScreen(String text, long style,boolean isLogin,NewAccountScreen actualizate){
		
		super(new VerticalFieldManager(VERTICAL_SCROLL));
		this.isLogin=isLogin;
		this.actualizate=actualizate;
		initVariables();
		
		addFields();
		
	}
public TagsPopupScreen(String text, long style,boolean isLogin){
		
		super(new VerticalFieldManager(VERTICAL_SCROLL));
		this.isLogin=isLogin;
		
		initVariables();
		
		addFields();
		
	}
	
	public void initVariables(){
	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	     TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	     transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 2000);
	     transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	     transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    	 engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
		hfmTitle = new HorizontalFieldManager(Field.FIELD_HCENTER);
		hfmnick = new HorizontalFieldManager(Field.FIELD_LEFT);
		hfmBtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		
		widthManager = Display.getWidth() - 100;
		
		TipoTag = new LabelField("Agrega un TAG: ", Field.NON_FOCUSABLE);
		nick = new LabelField("Etiqueta: ", Field.NON_FOCUSABLE);
		pin = new LabelField("D�gito verificador: ");
		
		
		tagHFM    = new HorizontalFieldManager(Field.FIELD_LEFT);
		dvHFM     = new HorizontalFieldManager(Field.FIELD_LEFT);
		basicClave = new BorderBasicEditField("","", 14, Display.getWidth()-20, Field.FOCUSABLE|EditField.FILTER_NUMERIC);
		basicPin = new BorderBasicEditField("","", 1, Display.getWidth()-20, Field.FOCUSABLE|EditField.FILTER_NUMERIC);
		
		basicLabel = new BorderBasicEditField("","", 12, Display.getWidth(), Field.FOCUSABLE);		
		
		btnChange =  new GenericImageButtonField(MainClass.BTN_SAVE_OVER, MainClass.BTN_SAVE,"continuar"){
			
			protected boolean navigationClick(int status, int time) {
				
				if(basicPin.getText().trim().equals("")){
					if(CLAVE_NOW == CLAVE_IAVE)
						Dialog.alert("Debe de ingresar su D�gito Verificador.");
					if(CLAVE_NOW == CLAVE_PASE)
						Dialog.alert("Debe de ingresar su D�gito Verificador.");	
					if(CLAVE_NOW == CLAVE_OHL)
						Dialog.alert("Debe de ingresar su confirmaci�n de TAG.");
					if(CLAVE_NOW == CLAVE_VIAPASS)
						Dialog.alert("Debe de ingresar su N�mero de cliente.");
				}
				else if(basicClave.getText().trim().equals("")&&CLAVE_NOW != CLAVE_VIAPASS){			
					if(CLAVE_NOW == CLAVE_IAVE)Dialog.alert("Debe de ingresar su clave de tarjeta.");
					if(CLAVE_NOW == CLAVE_PASE)Dialog.alert("Debe de ingresar su clave de tarjeta.");
					if(CLAVE_NOW == CLAVE_OHL)Dialog.alert("Debe de ingresar su clave de tarjeta.");
				}else if(basicLabel.getText().trim().equals("")){			
					Dialog.alert("Debe de ingresar una nombre al TAG.");			
				} else if(CLAVE_NOW == CLAVE_OHL && !basicPin.getText().equals(basicClave.getText())){
					Dialog.alert("Los n�meros de TAG no coinciden.");
				}else{
					if(CLAVE_NOW== CLAVE_OHL){
						basicPin.setText("1");
					}
							
					AgregaTag();					
				}				
				return true;
			}
			
		};
		
		tagNames = new String[MainClass.TagsBeans.length];
		
		for(int i = 0; i < MainClass.TagsBeans.length; i++){
			tagNames[i] = MainClass.TagsBeans[i].getNombre();
		}
		
		choicesTagNames = new ObjectChoiceField("TIPO:", tagNames){
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				super.paint(graphics);
				invalidate();
			}
			
			protected void drawFocus(Graphics graphics, boolean on) {
				// TODO Auto-generated method stub
				super.drawFocus(graphics, on);
				invalidate();
			}
		};
		
		choicesTagNames.setChangeListener(new FieldChangeListener(){

			public void fieldChanged(Field field, int context) {
				// TODO Auto-generated method stub
				System.out.println("index: "+choicesTagNames.getSelectedIndex());
					if(context == ChoiceField.CONTEXT_CHANGE_OPTION){
						TipoRecargaTag tag = MainClass.TagsBeans[choicesTagNames.getSelectedIndex()];
						int clave = Integer.parseInt(tag.getClave());
						if(clave == 1){
							setIaveFields();
						}
						
						if(clave == 2){
							setOhlFields();
						}
						if(clave == 3){
							setVIAPASSFields();
						}
						if(clave == 4){
							setPASEFields();
						}
					}
			}
			
		});
		
		clave = new LabelField("N�mero de TAG:");
		
		
		img2 =new LabelField("�qu� es esto?",Field.FOCUSABLE){
			
			public void paint(Graphics g){
				
				
				g.setColor(Color.ORANGE);
				super.paint(g);
			}
			protected boolean navigationClick(int status, int time) {
				
				// TODO Auto-generated method stub
				if(CLAVE_NOW == CLAVE_IAVE){
					UiApplication.getUiApplication().pushScreen(new ImagePopUpune("TAG", Field.FOCUSABLE));
				}
				
				if(CLAVE_NOW == CLAVE_OHL){
					UiApplication.getUiApplication().pushScreen(new ImagePopUpune("OHL", Field.FOCUSABLE));
				}
				if(CLAVE_NOW == CLAVE_VIAPASS){
					UiApplication.getUiApplication().pushScreen(new ImagePopUpune("VIAPASS", Field.FOCUSABLE));
				}
				if(CLAVE_NOW == CLAVE_PASE){
					UiApplication.getUiApplication().pushScreen(new ImagePopUpune("PASE", Field.FOCUSABLE));
				}
				return true;
			}
			protected void onFocus(int direction) {  
		        super.onFocus(direction);
		        invalidate(); 
		     //   
		    }   
		 
			 protected void onUnfocus() {  
		           super.onUnfocus();
		           invalidate();  
		    } 
			
			
		};
		
		
		img2.setFont(Font.getDefault().derive(Font.UNDERLINED));

		img3 =new LabelField("�qu� es esto?",Field.FOCUSABLE){
			
			public void paint(Graphics g){
				
				
				g.setColor(Color.ORANGE);
				super.paint(g);
			}
			protected boolean navigationClick(int status, int time) {
				
				// TODO Auto-generated method stub
				if(CLAVE_NOW == CLAVE_IAVE){
					UiApplication.getUiApplication().pushScreen(new ImagePopUpune("Digito_Verificador", Field.FOCUSABLE));
				}
				
				if(CLAVE_NOW == CLAVE_OHL){
					
				}
				if(CLAVE_NOW == CLAVE_VIAPASS){
					UiApplication.getUiApplication().pushScreen(new ImagePopUpune("VIAPASS", Field.FOCUSABLE));
				}
				if(CLAVE_NOW == CLAVE_PASE){
					UiApplication.getUiApplication().pushScreen(new ImagePopUpune("Digito_Verificador_pase", Field.FOCUSABLE));
				}
				
				return true;
			}
			protected void onFocus(int direction) {  
		        super.onFocus(direction);
		        invalidate(); 
		     //   
		    }   
		 
			 protected void onUnfocus() {  
		           super.onUnfocus();
		           invalidate();  
		    } 
			
			
		};
		img3.setFont(Font.getDefault().derive(Font.UNDERLINED));
		imgnick =new LabelField("�qu� es esto?",Field.FOCUSABLE){
			
			public void paint(Graphics g){
				
				
				g.setColor(Color.ORANGE);
				super.paint(g);
			}
			protected boolean navigationClick(int status, int time) {
				
				Dialog.inform("Colocar un nombre para identificar este TAG ej. Mi Tag, Tag Papa, Tag Esposa, etc.");
				
				return true;
			}
			protected void onFocus(int direction) {  
		        super.onFocus(direction);
		        invalidate(); 
		     //   
		    }   
		 
			 protected void onUnfocus() {  
		           super.onUnfocus();
		           invalidate();  
		    } 
			
			
		};
		imgnick.setFont(Font.getDefault().derive(Font.UNDERLINED));
		
	}
	
	public void addFields(){
		add(new NullField());
		hfmTitle.add(TipoTag);
		hfmBtn.add(btnChange);
		add(hfmTitle);
		add(new LabelField(""));
		add(choicesTagNames);
		add(new LabelField(""));
		hfmnick.add(nick);
		hfmnick.add(imgnick);
		add(hfmnick);
		add(basicLabel);
		add(new LabelField(""));	
		tagHFM.add(clave);
		tagHFM.add(img2);
		add(tagHFM);
		add(basicClave);
		add(new LabelField("", Field.NON_FOCUSABLE));		
		dvHFM.add(pin);
		dvHFM.add(img3);
		add(dvHFM);
		add(basicPin);
		add(new LabelField("", Field.NON_FOCUSABLE));
		
		
		add(hfmBtn);
		add(new LabelField(""));
		
		TipoRecargaTag tag = MainClass.TagsBeans[0];
		int clave = Integer.parseInt(tag.getClave());
		if(clave == 1){
			setIaveFields();
		}
		
		if(clave == 2){
			setOhlFields();
		}
		if(clave == 3){
			setVIAPASSFields();
		}
		if(clave == 4){
			setPASEFields();
		}
		
	}

	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		
	}
	
	public boolean keyChar(char key, int status, int time) {
	    //intercept the ESC key
	    boolean retval = false;
	    switch (key) {

	        case Characters.ESCAPE:
	            close();
	            break;
	        default:
	            retval = super.keyChar(key,status,time);
	    }
	    return retval;
	}
	
	public void close() {
		// TODO Auto-generated method stub
		UiApplication.getUiApplication().popScreen(this);
	}
	private void AgregaTag(){
		if(isLogin){
			String TAGJson="";
			TAGJson="{\"etiqueta\":\""+basicLabel.getText().trim()+"\",\"numero\":\""+basicClave.getText().trim()+"\",\"dv\":\""+basicPin.getText().trim()+"\",\"tipotag\":\""+MainClass.TagsBeans[choicesTagNames.getSelectedIndex()].getClave()+"\",\"usuario\":\""+MainClass.userBean.getIdUser()+"\"}";
			
			String json=Crypto.aesEncrypt(MainClass.key,TAGJson);
			new SetTagThread(json,actualizate).start();
		}else{
			String TAG="";	
			TAG=basicLabel.getText().trim()+"|"+basicClave.getText().trim()+"|"+basicPin.getText().trim()+"|"+MainClass.TagsBeans[choicesTagNames.getSelectedIndex()].getClave();
			String TAGJson="";
			
			MainClass.TAG_REGISTER="\"etiqueta\":\""+basicLabel.getText().trim()+"\",\"numero\":\""+basicClave.getText().trim()+"\",\"dv\":\""+basicPin.getText().trim()+"\",\"idtiporecargatag\":\""+MainClass.TagsBeans[choicesTagNames.getSelectedIndex()].getClave();
			UiApplication.getUiApplication().popScreen(this);						
		}
				
	}
	
	
	public void setIaveFields(){
		CLAVE_NOW = CLAVE_IAVE;
		pin.setText("D�gito verificador:");
		basicPin.setText("");
		basicClave.setText("");
		basicPin.setMaxSize(1);
		basicClave.setFilter(TextFilter.get(TextFilter.NUMERIC));
		
		img3.setText("�qu� es esto?");
	}
	
	
	public void setPASEFields(){
		CLAVE_NOW = CLAVE_PASE;
		pin.setText("D�gito verificador:");
		basicPin.setText("");
		basicClave.setText("");
		basicPin.setMaxSize(1);
		basicClave.setFilter(TextFilter.get(TextFilter.NUMERIC));
		
		img3.setText("�qu� es esto?");
	}
	
	public void setOhlFields(){
		CLAVE_NOW = CLAVE_OHL;
		pin.setText("Confirmar TAG:");
		basicClave.setFilter(TextFilter.get(TextFilter.NUMERIC));
		basicPin.setText("");
		basicClave.setText("");
		basicPin.setMaxSize(14);
		
		img3.setText("");
	}
	public void setVIAPASSFields(){
		CLAVE_NOW = CLAVE_VIAPASS;
		pin.setText("N�mero de Cliente:");
		basicPin.setText("");
		basicClave.setText("");
		basicPin.setMaxSize(8);
		basicClave.setMaxSize(12);
		basicClave.setFilter(TextFilter.get(TextFilter.NUMERIC));
		
		img3.setText("�qu� es esto?");
	}
}
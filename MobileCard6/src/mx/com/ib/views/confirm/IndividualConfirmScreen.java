package mx.com.ib.views.confirm;


import crypto.Crypto;
import mx.com.ib.app.MainClass;
import mx.com.ib.controls.BorderBasicEditField;
import mx.com.ib.controls.GenericImageButtonField;

import mx.com.ib.threads.ComisionThread;
import mx.com.ib.utils.Utils;
import mx.com.ib.views.BeneficiariosScreen;
import mx.com.ib.views.ConsultScreen;
import mx.com.ib.views.VitamedicaBuyScreen;
import mx.com.ib.views.VitamedicaIndividualScreen;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;

import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;

import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;


public class IndividualConfirmScreen extends PopupScreen implements FieldChangeListener{
	
	
	public LabelField TipoTag = null;
	
	
	public HorizontalFieldManager hfmTitle = null;
	public HorizontalFieldManager hfmBtn = null;
	public HorizontalFieldManager hfmnick = null;
	
	public HorizontalFieldManager tagHFM = null;
	public HorizontalFieldManager dvHFM = null;
	
	
	
	public int widthManager = 0;
	public boolean isLogin = false;
	
	public GenericImageButtonField btnChange = null;
	private VitamedicaIndividualScreen actualizate=null;
	
	public IndividualConfirmScreen(String text, long style,boolean isLogin,VitamedicaIndividualScreen actualizate){
		
		super(new VerticalFieldManager(VERTICAL_SCROLL));
		this.isLogin=isLogin;
		this.actualizate=actualizate;
		initVariables();
		
		addFields();
		
	}
public IndividualConfirmScreen(String text, long style,boolean isLogin){
		
		super(new VerticalFieldManager(VERTICAL_SCROLL));
		this.isLogin=isLogin;
		
		initVariables();
		
		addFields();
		
	}
	
	public void initVariables(){
	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_DOWN);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	     TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	     transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	     transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_UP);
	     transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    	 engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
		hfmTitle = new HorizontalFieldManager(Field.FIELD_HCENTER);
		hfmnick = new HorizontalFieldManager(Field.FIELD_LEFT);
		hfmBtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		
		widthManager = Display.getWidth() - 30;
		
		TipoTag = new LabelField("�Confirmas tus datos?", Field.NON_FOCUSABLE);
		
		btnChange =  new GenericImageButtonField(MainClass.BTN_OK_OVER,MainClass.BTN_OK,"ok"){
			
			protected boolean navigationClick(int status, int time) {
				int x = Dialog.ask(Dialog.D_OK_CANCEL, "Por favor verifique que sus datos sean correctos, si es as� de clic en OK, de lo contrario de clic en cancelar para empezar otra vez");
				if(x==Dialog.CANCEL)UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
				if(x==Dialog.OK){
					System.out.println("entra al OK");
					
					if(MainClass.tipoVita==1){
					/*UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
					MainClass.splashScreen.start();
					
					String json2 = Crypto.aesEncrypt(MainClass.key,MainClass.IDPROVEEDOR);
					String newUrl = Utils.replaceURL(json2, "+", "%2B");		
					new ComisionThread(newUrl,6,null).start();*/
						UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
						UiApplication.getUiApplication().pushScreen(new VitamedicaBuyScreen(null));
					}
					if(MainClass.tipoVita==2){
						UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
						UiApplication.getUiApplication().pushScreen(new BeneficiariosScreen());
					}
				}
				//UiApplication.getUiApplication().popScreen(this);	
				return true;
			}
			
		};
	}
	
	public void addFields(){
		add(new NullField());
		hfmTitle.add(TipoTag);
		
		add(hfmTitle);
		add(new LabelField(""));
		hfmBtn.add(btnChange);
		String mem="individual";
		add(new LabelField("Tipo de Membres�a: "));
		if(MainClass.tipoVita==1){
			add(new LabelField(mem){
				public void paint(Graphics g){
					
					
					g.setColor(Color.ORANGE);
					super.paint(g);
				}
			});	
			
		}
		if(MainClass.tipoVita==2){
			add(new LabelField("Familiar"){
				public void paint(Graphics g){
					
					
					g.setColor(Color.ORANGE);
					super.paint(g);
				}
			});	
			
		}
		
		add(new LabelField("Nombre Completo:"));
		String nomcompleto=MainClass.invididualBean.getNombre()+" "+MainClass.invididualBean.getApellidopaterno()+ " "+MainClass.invididualBean.getApellidomaterno();
		add(new LabelField(nomcompleto,Field.FOCUSABLE){
			public void paint(Graphics g){
				
				
				g.setColor(Color.ORANGE);
				super.paint(g);
			}
		});
		String fechana=MainClass.invididualBean.getFecNac();
		add(new LabelField("Fecha de nacimiento: "));
		add(new LabelField(fechana,Field.FOCUSABLE){
			public void paint(Graphics g){
				
				
				g.setColor(Color.ORANGE);
				super.paint(g);
			}
		});
		
		String sexo=MainClass.invididualBean.getSexo();
		add(new LabelField("G�nero: "));
		
		if(sexo.equals("F")){sexo="Femenino";}
		if(sexo.equals("M")){sexo="Masculino";}
		add(new LabelField(sexo,Field.FOCUSABLE){
			public void paint(Graphics g){
				
				
				g.setColor(Color.ORANGE);
				super.paint(g);
			}
		});
		String edoCivil=MainClass.invididualBean.getEdocivil();
		int x=edoCivil.indexOf("-");
		String e=edoCivil.substring(x+1, edoCivil.length());
		add(new LabelField("Estado civil: "));
		
		add(new LabelField(e,Field.FOCUSABLE){
			public void paint(Graphics g){
				
				
				g.setColor(Color.ORANGE);
				super.paint(g);
			}
		});
		String telCasa=MainClass.invididualBean.getTelCasa();
		add(new LabelField("Tel�fono de casa: "));
		add(new LabelField(telCasa,Field.FOCUSABLE){
			public void paint(Graphics g){
				
				
				g.setColor(Color.ORANGE);
				super.paint(g);
			}
		});
	/*	String telofi=MainClass.invididualBean.getTelOfna();
		add(new LabelField("Tel�fono de oficina: "));
		add(new LabelField(telofi,Field.FOCUSABLE){
			public void paint(Graphics g){
				
				
				g.setColor(Color.ORANGE);
				super.paint(g);
			}
		});*/
		String cel=MainClass.invididualBean.getTelCel();
		add(new LabelField("Tel�fono celular: "));
		add(new LabelField(cel,Field.FOCUSABLE){
			public void paint(Graphics g){
				
				
				g.setColor(Color.ORANGE);
				super.paint(g);
			}
		});
		String mail=MainClass.invididualBean.getMail();
		add(new LabelField("Correo electr�nico: "));
		add(new LabelField(mail,Field.FOCUSABLE){
			public void paint(Graphics g){
				
				
				g.setColor(Color.ORANGE);
				super.paint(g);
			}
		});
		String numfamiliares=MainClass.invididualBean.getNumPer();
		add(new LabelField("N�mero de Beneficiarios: "));
		add(new LabelField(numfamiliares,Field.FOCUSABLE){
			public void paint(Graphics g){
				
				
				g.setColor(Color.ORANGE);
				super.paint(g);
			}
		});
		add(new LabelField("Direcci�n "));
		String calle=MainClass.invididualBean.getCalle();
		add(new LabelField("Calle: "));
		add(new LabelField(calle,Field.FOCUSABLE){
			public void paint(Graphics g){
				
				
				g.setColor(Color.ORANGE);
				super.paint(g);
			}
		});
		String numext=MainClass.invididualBean.getNumExt();
		add(new LabelField("N�mero exterior: "));
		add(new LabelField(numext,Field.FOCUSABLE){
			public void paint(Graphics g){
				
				
				g.setColor(Color.ORANGE);
				super.paint(g);
			}
		});
		String numint=MainClass.invididualBean.getNumInt();
		add(new LabelField("N�mero interior: "));
		add(new LabelField(numint,Field.FOCUSABLE){
			public void paint(Graphics g){
				
				
				g.setColor(Color.ORANGE);
				super.paint(g);
			}
		});
		String col=MainClass.invididualBean.getCol();
		add(new LabelField("Colonia: "));
		add(new LabelField(col,Field.FOCUSABLE){
			public void paint(Graphics g){
				
				
				g.setColor(Color.ORANGE);
				super.paint(g);
			}
		});
		String cp=MainClass.invididualBean.getCP();
		add(new LabelField("C�digo postal: "));
		add(new LabelField(cp,Field.FOCUSABLE){
			public void paint(Graphics g){
				
				
				g.setColor(Color.ORANGE);
				super.paint(g);
			}
		});
		add(hfmBtn);
		
	}

	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		
	}
	
	public boolean keyChar(char key, int status, int time) {
	    //intercept the ESC key
	    boolean retval = false;
	    switch (key) {

	        case Characters.ESCAPE:
	            close();
	            break;
	        default:
	            retval = super.keyChar(key,status,time);
	    }
	    return retval;
	}
	
	public void close() {
		// TODO Auto-generated method stub
		UiApplication.getUiApplication().popScreen(this);
	}
	
}
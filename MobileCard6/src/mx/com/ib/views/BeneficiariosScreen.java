package mx.com.ib.views;

import crypto.Crypto;
import mx.com.ib.app.MainClass;
import mx.com.ib.beans.BeneficiarioBean;
import mx.com.ib.beans.GeneralBean;
import mx.com.ib.beans.ProductBean;
import mx.com.ib.controls.BgManager;
import mx.com.ib.controls.CustomVerticalFieldManager;
import mx.com.ib.controls.BannerBitmap;
import mx.com.ib.controls.GenericImageButtonField;

import mx.com.ib.controls.ProductLabelField;
import mx.com.ib.threads.CatProductsThread;
import mx.com.ib.threads.ComisionThread;
import mx.com.ib.threads.CreditInfoThread;
import mx.com.ib.threads.ThreadBuyNowImages;
import mx.com.ib.utils.Utils;
import mx.com.ib.views.confirm.IndividualConfirmScreen;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.CheckboxField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.FlowFieldManager;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;


public class BeneficiariosScreen extends MainScreen{
	
	public BgManager bgManager = null;
	
	public LabelField products,	 Message  = null;
	
	
	public CustomVerticalFieldManager vfmFooter = null;
	public GenericImageButtonField btnChange = null;
	
	public ProductBean[] productBean = null;
	public GeneralBean[] bankBeans = null;
	
	public ProductLabelField[] productLabelFields = null;
	public FlowFieldManager grid;
	public HorizontalFieldManager hfmPass=null;
	public HorizontalFieldManager hfmBtn = null;
	public static CheckboxField doit=null;
	
	public  static CheckboxField Uno,Dos,Tres,Cuatro=null;
	public BeneficiariosScreen(){
	

		initVariables();
		
		addFields();
		
		
	
	}
	
	
	
	
	
	public void initVariables(){
	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	    
	    TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	    transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
	    hfmPass = new HorizontalFieldManager(Field.FIELD_VCENTER);
		bgManager = new BgManager(MainClass.BG_GRAY);
		products = new LabelField("Agrega tus Beneficiarios", Field.USE_ALL_WIDTH | LabelField.HCENTER);
		
		grid = new FlowFieldManager( Field.USE_ALL_WIDTH);		
		vfmFooter = new CustomVerticalFieldManager(Display.getWidth(),
				Display.getHeight(), 
				HorizontalFieldManager.FIELD_HCENTER | VERTICAL_SCROLL);
		
		 Message = new LabelField(""+MainClass.invididualBean.getNombre()+" "+MainClass.invididualBean.getApellidopaterno()+" debe agregar a "+MainClass.invididualBean.getNumPer()+" persona(s)",Field.FIELD_HCENTER){			
			public void paint(Graphics graphics){				
				int g = graphics.getColor();
				graphics.setColor(MainClass.BG_ORANGE);
				super.paint(graphics);
			}
			protected void onFocus(int direction) {  
		        super.onFocus(direction);
		        invalidate(); 
		     //   
		    }   
		 
			 protected void onUnfocus() {  
		           super.onUnfocus();
		           invalidate();  
		    } 
		};
		Message.setFont(MainClass.appFont1);
		hfmBtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		
	btnChange =  new GenericImageButtonField(MainClass.BTN_OK_OVER,MainClass.BTN_OK,"ok"){
			
			protected boolean navigationClick(int status, int time) {
				boolean a=true;
				for( int x=0; x<Integer.parseInt(MainClass.invididualBean.getNumPer());x++){
					if(MainClass.Beneficiarios[x]==null){
						a=false;
					}
				}
				
				if(a){
					
				/*MainClass.splashScreen.start();
				
				String json2 = Crypto.aesEncrypt(MainClass.key,MainClass.IDPROVEEDOR);
				String newUrl = Utils.replaceURL(json2, "+", "%2B");		
				new ComisionThread(newUrl,6,null).start();*/
					UiApplication.getUiApplication().pushScreen(new VitamedicaBuyScreen(null));
				}else{
					Dialog.alert("Uno o m�s Beneficiarios no est� completo");
				}
				return true;
			}
			
		};
		
	}
	
	
public void addFields(){
		setBanner(new BannerBitmap(MainClass.HD_TIENDA));
		
		vfmFooter.add(new LabelField(""));
		vfmFooter.add(products);
		vfmFooter.add(new LabelField(""));
		vfmFooter.add(Message);
		vfmFooter.add(new LabelField(""));
		MainClass.Beneficiarios= new BeneficiarioBean[Integer.parseInt(MainClass.invididualBean.getNumPer())];
		for( int x=0; x<Integer.parseInt(MainClass.invididualBean.getNumPer());x++){
			 hfmPass = new HorizontalFieldManager(Field.FIELD_VCENTER);
			 
			 if(x==0){
				 Uno= new CheckboxField("",false,Field.NON_FOCUSABLE);
				 Uno.setLabel("");
				 Uno.setEditable(false);
				 Uno.select(false); 
				 
			 }
			 if(x==1){
				 Dos= new CheckboxField("",false,Field.NON_FOCUSABLE);
				 Dos.setLabel("");
				 Dos.setEditable(false);
				 Dos.select(false); 
				 
			 }
			 if(x==2){
				 Tres= new CheckboxField("",false,Field.NON_FOCUSABLE);
				 Tres.setLabel("");
				 Tres.setEditable(false);
				 Tres.select(false); 
				 
			 }
			 if(x==3){
				 Cuatro= new CheckboxField("",false,Field.NON_FOCUSABLE);
				 Cuatro.setLabel("");
				 Cuatro.setEditable(false);
				 Cuatro.select(false); 
				 
			 }
			
			 
		
			 final int persona=x;
			
			hfmPass.add(new ButtonField(" Agregar Beneficiario "+(x+1),Field.FIELD_HCENTER){
				protected boolean navigationClick(int status, int time) {
					// TODO Auto-generated method stub
					
					   UiApplication.getUiApplication().pushScreen(new addBeneficiarioScreen(MainClass.Beneficiarios[persona],persona,doit));
					
					return true;
				}
				
				
				protected boolean touchEvent (TouchEvent message) {
					   switch(message.getEvent()) {
					      case TouchEvent.CLICK:
					    	  UiApplication.getUiApplication().pushScreen(new addBeneficiarioScreen(MainClass.Beneficiarios[persona],persona,doit));
					         return true;
					   }
					   return false; 
					}
				public void paint(Graphics g){
					
					
					g.setColor(MainClass.BG_ORANGE);
					super.paint(g);
				}
				
			});
			
			
			 hfmPass.add(new LabelField("  "));
			 
			 if(x==0){				 
				 hfmPass.add(Uno);
			 }
			 if(x==1){				 
				 hfmPass.add(Dos);
			 }
			 if(x==2){				 
				 hfmPass.add(Tres);
			 }
			 if(x==3){				 
				 hfmPass.add(Cuatro);
			 }
			
			vfmFooter.add(hfmPass);
		
		}
		vfmFooter.add(new LabelField(""));
		hfmBtn.add(btnChange);
		vfmFooter.add(hfmBtn);
		bgManager.add(vfmFooter);
	//	setStatus(new BannerBitmap());
		add(bgManager);
		
	}

}

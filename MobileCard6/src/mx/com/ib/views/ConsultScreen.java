package mx.com.ib.views;

import crypto.Crypto;

import mx.com.ib.app.MainClass;

import mx.com.ib.controls.BannerBitmap;
import mx.com.ib.controls.BgManager;
import mx.com.ib.controls.CustomVerticalFieldManager;
import mx.com.ib.controls.GenericImageButtonField;
import mx.com.ib.controls.HorizontalFieldManagerS;
import mx.com.ib.controls.HyperlinkButtonField;
import mx.com.ib.threads.ConsultThread;
import mx.com.ib.utils.Utils;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class ConsultScreen extends MainScreen{
	
	public BgManager bgManager = null;
	
	public HorizontalFieldManager hfmMain = null;
	public HorizontalFieldManager hfmTitle = null;
	public HorizontalFieldManager hfmLinks = null;
	
	public int widthManager = 0;
	
	public VerticalFieldManager vfmFooter = null;
	public VerticalFieldManager vfmInternalConsults = null;
	
	public CustomVerticalFieldManager customVerticalFieldManager = null;
	
	public HorizontalFieldManagerS hfmBtns = null;
	
	public GenericImageButtonField btnOk = null;
	public BannerBitmap baner;
	
	GenericImageButtonField todayLink = null;
	GenericImageButtonField thisWeekLink = null;
	GenericImageButtonField thisMonthLink = null;
	GenericImageButtonField lastMonthLink = null;
	
	public ConsultScreen(){
		
		initVariables();
		
		addFields();
		
	}
	
	public void initVariables(){
		
	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	     TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	     transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	     transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	     transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    	 engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
	    	//	bgManager = new BgManager(0x414042);

	    		bgManager = new BgManager(MainClass.BG_GRAY);

		widthManager = Display.getWidth() - 20;
		
		hfmTitle = new HorizontalFieldManager(Field.FIELD_RIGHT);
		hfmLinks = new HorizontalFieldManager(Field.FIELD_HCENTER|HORIZONTAL_SCROLL);
		hfmMain = new HorizontalFieldManager(VerticalFieldManager.FIELD_HCENTER|VERTICAL_SCROLL);
		
		hfmBtns = new HorizontalFieldManagerS(HorizontalFieldManager.FIELD_HCENTER| HorizontalFieldManager.USE_ALL_WIDTH,Bitmap.getBitmapResource(MainClass.BTN_CONTINUE_OVER).getWidth());
		
		customVerticalFieldManager = new CustomVerticalFieldManager(Display.getWidth(),Display.getHeight(), HorizontalFieldManager.FIELD_LEFT| VERTICAL_SCROLL);
		
		vfmFooter = new VerticalFieldManager(VERTICAL_SCROLL);
		vfmInternalConsults = new VerticalFieldManager(VERTICAL_SCROLL);
		
		btnOk =  new GenericImageButtonField(MainClass.BTN_OK_OVER,MainClass.BTN_OK,"ok"){
			
			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub				
				
				close();
				return true;
			}
			
		};
		
		todayLink = new GenericImageButtonField(MainClass.BTN_HOY, MainClass.BTN_HOY_OVER,  "hoy" ){
			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub
				
				MainClass.splashScreen.start();
				vfmInternalConsults.deleteAll();
				new ConsultThread(generateJSON("1"), vfmInternalConsults).run();
				return true;
			}
		};
		
		thisWeekLink = new GenericImageButtonField(MainClass.BTN_SEMANA,MainClass.BTN_SEMANA_OVER, "semana" ){
			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub
				
				MainClass.splashScreen.start();
				vfmInternalConsults.deleteAll();
				new ConsultThread(generateJSON("2"), vfmInternalConsults).run();
				return true;
			}
		};
		
		thisMonthLink = new GenericImageButtonField(MainClass.BTN_ESTE_MES, MainClass.BTN_ESTE_MES_OVER, "este_mes" ){
			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub
				
				MainClass.splashScreen.start();
				vfmInternalConsults.deleteAll();
				new ConsultThread(generateJSON("3"), vfmInternalConsults).run();
				return true;
			}
		};
		
		lastMonthLink = new GenericImageButtonField(MainClass.BTN_MES, MainClass.BTN_MES_OVER, "mes_anterior" ){
			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub
				
				MainClass.splashScreen.start();
				vfmInternalConsults.deleteAll();
				new ConsultThread(generateJSON("4"), vfmInternalConsults).run();
				return true;
			}
		};
		
	}
	
	public void addFields(){
		
		//hfmTitle.add(new BitmapField(Bitmap.getBitmapResource(MainClass.IMG_HEADER_PURCHASES)));
		//hfmTitle.add(new BitmapField(Bitmap.getBitmapResource(MainClass.IMG_HEADER_LOGO)));
	//	BitmapField bField = new BitmapField(Bitmap.getBitmapResource(MainClass.BG_CONSULTA));
		baner= new BannerBitmap(MainClass.BG_CONSULTA);
		setBanner(baner);
	//	setBanner(bField);
		//customVerticalFieldManager.add(bField);
		//HorizontalFieldManager hman = new HorizontalFieldManager(Field.FIELD_HCENTER);
		//hman.add(new BitmapField(Bitmap.getBitmapResource(MainClass.BTN_BUY_OVER), Field.FIELD_HCENTER));
		hfmBtns.add(btnOk);
		
	//	customVerticalFieldManager.add(hman);
		
		
		/*LabelField date = new LabelField("Fecha: ", Field.NON_FOCUSABLE);
		date.setFont(MainClass.alphaSansFamily.getFont(Font.PLAIN, 5, Ui.UNITS_pt));
		hfmLinks.add(date);*/
		hfmLinks.add(todayLink);
		hfmLinks.add(thisWeekLink);
		hfmLinks.add(thisMonthLink);
		hfmLinks.add(lastMonthLink);
		
		customVerticalFieldManager.add(hfmLinks);
		
//		customVerticalFieldManager.add(new ConsultLabelField(new ConsultBean("Fecha y Hora", "Concepto", "Cargo"), Color.WHITE, Color.BLUE, MainClass.alphaSansFamily.getFont(Font.PLAIN, 7, Ui.UNITS_pt), Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(vfmInternalConsults);
		
		LabelField note = new  LabelField("Nota: El estado de cuenta corresponde unicamente a las compras realizadas a trav�s de Mobile Card.");
		
		note.setFont(MainClass.alphaSansFamily.getFont(Font.PLAIN, 5, Ui.UNITS_pt));
		
		customVerticalFieldManager.add(new LabelField(""));
		
		customVerticalFieldManager.add(note);
		
		customVerticalFieldManager.add(new LabelField(""));
		
		customVerticalFieldManager.add(hfmBtns);
		
		customVerticalFieldManager.add(new LabelField(""));
		
		hfmMain.add(customVerticalFieldManager);
		
		vfmFooter.add(hfmMain);
		
//		vfmFooter.add(new BitmapField(Bitmap.getBitmapResource("footer.PNG")));
		//vfmFooter.add(new FooterLabelField());
		
		bgManager.add(vfmFooter);
		setStatus(new BannerBitmap());
		add(bgManager);
		
		vfmInternalConsults.deleteAll();
		new ConsultThread(generateJSON("1"), vfmInternalConsults).run();
		
	}
	
	public String generateJSON(String id){
		
		String json2 = Crypto.aesEncrypt(Utils.parsePass(MainClass.p),"{\"login\":\""+MainClass.userBean.getLogin()+"\",\"password\":"+"\""+MainClass.p+"\",\"periodo\":"+id+"}");
		String json=Utils.mergeStr(json2, MainClass.p);
		json = "json="+json;
		
		return json;
	}

	class MyHyperlinkButtonField extends HyperlinkButtonField
	{
	    MyHyperlinkButtonField( String label ) {
	        super( label, 0x0000FF, 0xFFFFFF, 0x0000FF, 0, 0 );
	        setPadding( 8, 5, 8, 5 );
	        setFont(MainClass.alphaSansFamily.getFont(Font.PLAIN, 5, Ui.UNITS_pt));
	    }
	}

	public void close() {
		// TODO Auto-generated method stub
		UiApplication.getUiApplication().popScreen(this);
	}
	
}
package mx.com.ib.views;

import java.io.UnsupportedEncodingException;

import mx.com.ib.app.MainClass;
import mx.com.ib.beans.UserBean;
import mx.com.ib.controls.BgManager;
import mx.com.ib.controls.BorderBasicEditField;
import mx.com.ib.controls.BorderPasswordField;
import mx.com.ib.controls.CustomVerticalFieldManager;
import mx.com.ib.controls.GenericImageButtonField;
import mx.com.ib.controls.HorizontalFieldManagerS;
import mx.com.ib.threads.CreditInfoThread;
import mx.com.ib.threads.LoginThread;
import mx.com.ib.utils.Utils;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import crypto.Crypto;

public class LoginScreen extends MainScreen {

	public BgManager bgManager = null;

	// public LabelField title = null;
	public LabelField user = null, helpUser = null, helpPass = null,
			smslabel = null;
	public LabelField password = null;

	public LabelField recPass = null;

	public BorderBasicEditField basicUser = null;
	public BorderPasswordField basicPassword = null;

	public HorizontalFieldManager hfmMain = null;
	public HorizontalFieldManager hfmTitle, hfmsms = null;
	public HorizontalFieldManager pass = null;
	public HorizontalFieldManager userhfm = null;

	public HorizontalFieldManagerS hfmBtns = null;

	public VerticalFieldManager vfmFooter = null;

	public CustomVerticalFieldManager customVerticalFieldManager = null;
	public CustomVerticalFieldManager customVerticalFieldManager2 = null;

	public GenericImageButtonField btnRegister = null;
	public GenericImageButtonField btnOk = null, btnsms = null;

	// public String json = "";

	public LoginScreen() {

		initVariables();
		addFields();
	}

	public void initVariables() {

		TransitionContext transition = new TransitionContext(
				TransitionContext.TRANSITION_SLIDE);
		transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
		transition.setIntAttribute(TransitionContext.ATTR_DIRECTION,
				TransitionContext.DIRECTION_LEFT);
		transition.setIntAttribute(TransitionContext.ATTR_STYLE,
				TransitionContext.STYLE_PUSH);

		UiEngineInstance engine = Ui.getUiEngineInstance();
		engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH,
				transition);

		TransitionContext transition2 = new TransitionContext(
				TransitionContext.TRANSITION_SLIDE);
		transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
		transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION,
				TransitionContext.DIRECTION_RIGHT);
		transition2.setIntAttribute(TransitionContext.ATTR_STYLE,
				TransitionContext.STYLE_PUSH);

		engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP,
				transition2);
		engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);

		bgManager = new BgManager(MainClass.BG_GRAY);

		user = new LabelField("Usuario: ");
		user.setFont(MainClass.appFont);
		password = new LabelField("Contrase�a: ", Field.NON_FOCUSABLE);
		password.setFont(MainClass.appFont);

		hfmMain = new HorizontalFieldManager(VerticalFieldManager.FIELD_HCENTER
				| HorizontalFieldManager.FIELD_HCENTER | Field.FIELD_HCENTER
				| VERTICAL_SCROLL) {

			public int getPreferredWidth() {
				// TODO Auto-generated method stub
				return Display.getWidth();
			}

			protected void sublayout(int maxWidth, int maxHeight) {
				// TODO Auto-generated method stub
				super.sublayout(getPreferredWidth(), maxHeight);
			}
		};
		hfmBtns = new HorizontalFieldManagerS(
				HorizontalFieldManager.FIELD_HCENTER
						| HorizontalFieldManager.USE_ALL_WIDTH, Bitmap
						.getBitmapResource(MainClass.BTN_REGISTER).getWidth());
		hfmTitle = new HorizontalFieldManager(Field.FIELD_HCENTER);
		hfmsms = new HorizontalFieldManager(Field.FIELD_HCENTER);
		pass = new HorizontalFieldManager(Field.FIELD_LEFT);
		userhfm = new HorizontalFieldManager(Field.FIELD_LEFT);

		vfmFooter = new VerticalFieldManager(VERTICAL_SCROLL) {
			public int getPreferredWidth() {
				// TODO Auto-generated method stub
				return Display.getWidth();
			}

			protected void sublayout(int maxWidth, int maxHeight) {
				// TODO Auto-generated method stub
				super.sublayout(getPreferredWidth(), maxHeight);
			}
		};

		customVerticalFieldManager = new CustomVerticalFieldManager(
				Display.getWidth(), Display.getHeight(),
				HorizontalFieldManager.FIELD_LEFT | VERTICAL_SCROLL);
		// customVerticalFieldManager2 = new
		// CustomVerticalFieldManager(Display.getWidth(),145,
		// HorizontalFieldManager.FIELD_LEFT| VERTICAL_SCROLL);

		basicUser = new BorderBasicEditField("", "", 16, Display.getWidth(),
				Field.FOCUSABLE);
		basicPassword = new BorderPasswordField("", "", 12, Display.getWidth(),
				Field.FOCUSABLE);

		btnRegister = new GenericImageButtonField(MainClass.BTN_REGISTER_OVER,
				MainClass.BTN_REGISTER, "register") {

			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub

				MainClass.splashScreen.start();
				MainClass.userBean = null;

				// new CreditInfoThread("bancos").start();
				// new CreditInfoThread("tarjetas").start();
				new CreditInfoThread("proveedores").start();
				// UiApplication.getUiApplication().pushScreen(new
				// DatePickerDialog());
				return true;
			}

		};

		btnOk = new GenericImageButtonField(MainClass.BTN_OK_OVER,
				MainClass.BTN_OK, "ok") {

			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub

				try {
					login();
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return true;
			}

		};
		btnsms = new GenericImageButtonField(MainClass.BTN_SMS_OVER,
				MainClass.BTN_SMS, "ok") {

			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub
				UiApplication.getUiApplication().pushScreen(
						new SmsScreen("", Field.NON_FOCUSABLE));
				return true;
			}

		};

		recPass = new LabelField("Recuperar Contrase�a", Field.FOCUSABLE) {

			public void paint(Graphics g) {

				g.setColor(MainClass.COLOR_LINK);
				super.paint(g);
			}

			protected boolean navigationClick(int status, int time) {

				UiApplication.getUiApplication()
						.pushScreen(new RecPassScreen());
				return true;
			}

			protected void onFocus(int direction) {
				super.onFocus(direction);
				invalidate();
				//
			}

			protected void onUnfocus() {
				super.onUnfocus();
				invalidate();
			}

		};
		recPass.setFont(Font.getDefault().derive(Font.UNDERLINED));

		helpUser = new LabelField("?", Field.FOCUSABLE) {

			public void paint(Graphics g) {

				g.setColor(MainClass.BG_ORANGE);
				super.paint(g);
			}

			protected boolean navigationClick(int status, int time) {
				Dialog.inform("El nombre de usuario es de m�ximo 16 caracteres");
				// UiApplication.getUiApplication().pushScreen(new
				// RecUserScreen());
				return true;
			}

			protected void onFocus(int direction) {
				super.onFocus(direction);
				invalidate();
				//
			}

			protected void onUnfocus() {
				super.onUnfocus();
				invalidate();
			}

		};
		helpPass = new LabelField("? ", Field.FOCUSABLE) {

			public void paint(Graphics g) {

				g.setColor(MainClass.BG_ORANGE);
				super.paint(g);
			}

			protected boolean navigationClick(int status, int time) {
				Dialog.inform("La contrase�a debe tener m�nimo 8 caracteres y m�ximo 12");
				// UiApplication.getUiApplication().pushScreen(new
				// RecUserScreen());
				return true;
			}

			protected void onFocus(int direction) {
				super.onFocus(direction);
				invalidate();
				//
			}

			protected void onUnfocus() {
				super.onUnfocus();
				invalidate();
			}

		};
		smslabel = new LabelField(
				"Si usted acaba de registrarse y no ha recibido su SMS, de click al siguiente bot�n:") {

			public void paint(Graphics g) {

				g.setColor(MainClass.BG_ORANGE);
				super.paint(g);
			}

			protected void onFocus(int direction) {
				super.onFocus(direction);
				invalidate();
				//
			}

			protected void onUnfocus() {
				super.onUnfocus();
				invalidate();
			}

		};

	}

	public void addFields() {

		add(new NullField());
		BitmapField bField = new BitmapField(
				Bitmap.getBitmapResource(MainClass.BG_LOGIN));

		customVerticalFieldManager.add(bField);
		customVerticalFieldManager.add(new LabelField(""));

		userhfm.add(user);
		userhfm.add(helpUser);
		customVerticalFieldManager.add(userhfm);
		customVerticalFieldManager.add(basicUser);

		customVerticalFieldManager.add(new LabelField(""));

		pass.add(password);
		pass.add(helpPass);
		pass.add(recPass);
		customVerticalFieldManager.add(pass);
		customVerticalFieldManager.add(basicPassword);

		customVerticalFieldManager.add(new LabelField(""));

		hfmBtns.add(btnRegister);
		hfmBtns.add(btnOk);

		customVerticalFieldManager.add(hfmBtns);

		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		customVerticalFieldManager.add(smslabel);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));
		hfmsms.add(btnsms);
		customVerticalFieldManager.add(hfmsms);
		customVerticalFieldManager.add(new LabelField("", Field.NON_FOCUSABLE));

		hfmMain.add(customVerticalFieldManager);

		vfmFooter.add(hfmMain);

		bgManager.add(vfmFooter);

		add(bgManager);
	}

	protected boolean onSavePrompt() {
		return true;
	}

	public void login() throws UnsupportedEncodingException {

		if (basicUser.getText().trim().equals("")) {
			Dialog.inform("Debe ingresar el usuario.");
		} else if (basicPassword.getText().trim().equals("")) {
			Dialog.inform("Debe ingresar su password.");
		} else if (basicPassword.getText().trim().length() < 8) {
			Dialog.alert("Tu contrase�a debe ser de al menos 8 caract�res.");
		} else {

			MainClass.p = basicPassword.getText().trim();

			String jsonComplete = "{\"login\":\"" + basicUser.getText().trim()
					+ "\",\"tipo\":\"" + MainClass.TIPO + "\",\"imei\":\""
					+ Utils.getImei() + "\",\"modelo\":\"" + MainClass.MODELO
					+ "\",\"software\":\"" + MainClass.SOFTWARE
					+ "\",\"key\":\"" + Utils.getImei() + "\",\"password\":"
					+ "\"" + basicPassword.getText().trim()
					+ "\",\"passwordS\":" + "\""
					+ Utils.sha1(basicPassword.getText().trim()) + "\"}";
			String parser = Utils.parsePass(basicPassword.getText().trim());
			
			System.out.println(jsonComplete);
			System.out.println(parser);
			
			MainClass.userJSON = Crypto.aesEncrypt(parser, jsonComplete);
			
			System.out.println(MainClass.userJSON);
			
			MainClass.userBean = new UserBean(basicUser.getText().trim(),
					basicPassword.getText().trim(), "", "", "", "", "", "", "",
					"", "", "", "");
			String n2 = Utils.mergeStr(MainClass.userJSON, basicPassword
					.getText().trim());
			
			System.out.println(n2);
			new LoginThread(this, n2).start();
			
			/*
			System.out.println("{\"login\":\"" + basicUser.getText().trim()
					+ "\",\"tipo\":\"" + MainClass.TIPO + "\",\"imei\":\""
					+ Utils.getImei() + "\",\"modelo\":\"" + MainClass.MODELO
					+ "\",\"software\":\"" + MainClass.SOFTWARE
					+ "\",\"key\":\"" + Utils.getImei() + "\",\"password\":"
					+ "\"" + basicPassword.getText().trim()
					+ "\",\"passwordS\":" + "\""
					+ Utils.sha1(basicPassword.getText().trim()) + "\"}");
			 */

		}

	}

	public void cleanFields() {

		basicUser.setText("");
		basicPassword.setText("");

	}

	public void close() {

		MainClass.userBean = null;
		MainClass.userJSON = "";
		System.exit(0);
	}

	protected boolean keyDown(int keycode, int time) {
		if (Keypad.key(keycode) == Characters.ESCAPE) {
			close();
			return true;
		}
		return false;
	}

}

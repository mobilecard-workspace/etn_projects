package mx.com.ib.views;

import crypto.Crypto;
import mx.com.ib.app.MainClass;
import mx.com.ib.controls.BgManager;
import mx.com.ib.controls.CustomVerticalFieldManager;
import mx.com.ib.controls.GenericImageButtonField;
import mx.com.ib.threads.InfoUserThread;
import mx.com.ib.threads.ProductsThread;
import mx.com.ib.threads.PromoThread;
import mx.com.ib.utils.Utils;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class MenuScreen extends MainScreen {

	// public LabelField title = null;

	public HorizontalFieldManager hfmMain = null;
	public HorizontalFieldManager hfmTitle = null;

	public HorizontalFieldManager hfmBtns1 = null;
	public HorizontalFieldManager hfmBtns2 = null;
	public HorizontalFieldManager hfmBtns3 = null;
	public HorizontalFieldManager hfmBtns4 = null;
	public VerticalFieldManager hfmBtns5 = null;
	public HorizontalFieldManager hfmBtns6 = null;

	public CustomVerticalFieldManager customVerticalFieldManager = null;
	public CustomVerticalFieldManager customVerticalFieldManager2 = null;

	public GenericImageButtonField btnBuy = null;
	public GenericImageButtonField btnConsult = null;
	public GenericImageButtonField btnAccount = null;
	public GenericImageButtonField btnPromotions = null;
	public GenericImageButtonField btnExit = null;

	public BgManager bgManager = null;
	public Bitmap bField = null;
	public Bitmap bField2 = null;

	public VerticalFieldManager vfmFooter = null;

	public MenuScreen() {

		initVariables();
		initButtons();
		addFields();

	}

	public void initVariables() {
		TransitionContext transition = new TransitionContext(
				TransitionContext.TRANSITION_SLIDE);
		transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
		transition.setIntAttribute(TransitionContext.ATTR_DIRECTION,
				TransitionContext.DIRECTION_LEFT);
		transition.setIntAttribute(TransitionContext.ATTR_STYLE,
				TransitionContext.STYLE_PUSH);

		UiEngineInstance engine = Ui.getUiEngineInstance();
		engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH,
				transition);

		TransitionContext transition2 = new TransitionContext(
				TransitionContext.TRANSITION_SLIDE);
		transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
		transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION,
				TransitionContext.DIRECTION_RIGHT);
		transition2.setIntAttribute(TransitionContext.ATTR_STYLE,
				TransitionContext.STYLE_PUSH);

		engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP,
				transition2);
		engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
		bgManager = new BgManager(MainClass.BG_MENU);
		// title = new LabelField("		MENU", Field.FIELD_HCENTER){
		// protected void layout(int width, int height) {
		// // TODO Auto-generated method stub
		// super.layout(80, height);
		// }
		// }
		// ;
		// hfmMain.add(new NullField(Field.FOCUSABLE));
		hfmMain = new HorizontalFieldManager(Field.FIELD_HCENTER
				| VERTICAL_SCROLL | Field.USE_ALL_WIDTH);
		hfmTitle = new HorizontalFieldManager(Field.FIELD_RIGHT);
		vfmFooter = new VerticalFieldManager(VERTICAL_SCROLL);

		customVerticalFieldManager = new CustomVerticalFieldManager(
				Display.getWidth() / 2, Display.getHeight(),
				HorizontalFieldManager.USE_ALL_WIDTH | Field.FIELD_VCENTER);
		customVerticalFieldManager2 = new CustomVerticalFieldManager(
				(Display.getWidth() / 2) - 20, Display.getHeight(),
				HorizontalFieldManager.USE_ALL_WIDTH | Field.FIELD_VCENTER);
		hfmBtns1 = new HorizontalFieldManager(Field.FIELD_RIGHT);
		hfmBtns2 = new HorizontalFieldManager(Field.FIELD_RIGHT);
		hfmBtns3 = new HorizontalFieldManager(Field.FIELD_RIGHT);
		hfmBtns4 = new HorizontalFieldManager(Field.FIELD_RIGHT);
		hfmBtns6 = new HorizontalFieldManager(Field.FIELD_HCENTER);

		bField = Bitmap.getBitmapResource(MainClass.BG_MENU_MC);
		bField2 = Bitmap.getBitmapResource(MainClass.BTN_INVITA);
		hfmBtns5 = new VerticalFieldManager(Field.FIELD_HCENTER);

	}

	public void addFields() {

		add(new NullField());
		hfmBtns1.add(btnBuy);
		hfmBtns4.add(btnConsult);
		hfmBtns3.add(btnAccount);
		hfmBtns2.add(btnPromotions);
		BitmapField bit = new BitmapField(bField, Field.FOCUSABLE) {
			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub

				UiApplication.getUiApplication().pushScreen(new AboutScreen());

				return true;
			}

			protected void onFocus(int direction) {
				super.onFocus(direction);
				invalidate();
				//
			}

			protected void onUnfocus() {
				super.onUnfocus();
				invalidate();
			}
		};

		BitmapField bit2 = new BitmapField(bField2, Field.FOCUSABLE) {
			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub

				UiApplication.getUiApplication().pushScreen(
						new InvitationScreen());

				return true;
			}

			protected void onFocus(int direction) {
				super.onFocus(direction);

				invalidate();
				//
			}

			protected void onUnfocus() {

				super.onUnfocus();
				invalidate();
			}
		};
		hfmBtns5.add(bit);

		hfmBtns6.add(bit2);

		customVerticalFieldManager.add(hfmBtns1);
		customVerticalFieldManager.add(hfmBtns2);

		bit.setMargin(Display.getHeight() / 2 - bField.getHeight() / 2, 0, 0, 0);

		// bField.setMargin(top, right, bottom, left)
		customVerticalFieldManager2.add(hfmBtns5);
		customVerticalFieldManager2.add(hfmBtns6);
		customVerticalFieldManager.add(hfmBtns4);
		customVerticalFieldManager.add(hfmBtns3);

		int suma = Bitmap.getBitmapResource(MainClass.BTN_BUY).getHeight()
				+ Bitmap.getBitmapResource(MainClass.BTN_ACCOUNT).getHeight()
				+ Bitmap.getBitmapResource(MainClass.BTN_PROMOTIONS)
						.getHeight()
				+ Bitmap.getBitmapResource(MainClass.BTN_CONSULT).getHeight();
		btnBuy.setMargin(Display.getHeight() / 2 - suma / 2, 0, 0, 0);
		// System.out.println("Display.getHeight()/2: "+Display.getHeight()/2);
		// System.out.println("suma)/2: "+suma/2);
		hfmMain.add(customVerticalFieldManager2);
		hfmMain.add(customVerticalFieldManager);

		bgManager.add(hfmMain);

		add(bgManager);

	}

	public void initButtons() {
		btnBuy = new GenericImageButtonField(MainClass.BTN_CONSULT_OVER,
				MainClass.BTN_CONSULT, "comprar") {

			protected boolean navigationClick(int status, int time) {

				MainClass.splashScreen.start();
				new ProductsThread().start();
				return true;
			}

		};
		btnConsult = new GenericImageButtonField(MainClass.BTN_BUY_OVER,
				MainClass.BTN_BUY, "consulta") {

			protected boolean navigationClick(int status, int time) {

				UiApplication.getUiApplication()
						.pushScreen(new ConsultScreen());
				return true;
			}

		};

		btnAccount = new GenericImageButtonField(MainClass.BTN_ACCOUNT_OVER,
				MainClass.BTN_ACCOUNT, "cuenta") {

			protected boolean navigationClick(int status, int time) {

				MainClass.splashScreen.start();
				MainClass.userJSON = Crypto.aesEncrypt(
						Utils.parsePass(MainClass.p), "{\"login\":\""
								+ MainClass.userBean.getLogin()
								+ "\",\"password\":" + "\"" + MainClass.p
								+ "\"}");
				String n2 = Utils.mergeStr(MainClass.userJSON, MainClass.p);
				new InfoUserThread(n2).start();
				return true;
			}

		};
		btnPromotions = new GenericImageButtonField(
				MainClass.BTN_PROMOTIONS_OVER, MainClass.BTN_PROMOTIONS,
				"promocion") {

			protected boolean navigationClick(int status, int time) {

				new PromoThread().start();
				return true;
			}

		};
	}

	public void addFields2() {

		customVerticalFieldManager.add(new LabelField(""));

		hfmBtns1.add(btnBuy);
		hfmBtns2.add(btnConsult);
		hfmBtns3.add(btnAccount);
		hfmBtns4.add(btnPromotions);
		customVerticalFieldManager.add(hfmBtns1);
		customVerticalFieldManager.add(hfmBtns4);
		customVerticalFieldManager.add(hfmBtns2);
		customVerticalFieldManager.add(hfmBtns3);
		hfmMain.add(customVerticalFieldManager);

		bgManager.add(hfmMain);

		add(bgManager);
	}

	protected boolean onSavePrompt() {
		return true;
	}

	public void close() {

		MainClass.userBean = null;
		MainClass.userJSON = "";
		UiApplication.getUiApplication().pushScreen(new LoginScreen());
	}

	protected boolean keyDown(int keycode, int time) {

		if (Keypad.key(keycode) == Characters.ESCAPE) {
			// close();
			System.exit(0);
			return true;
		}
		return false;
	}

}

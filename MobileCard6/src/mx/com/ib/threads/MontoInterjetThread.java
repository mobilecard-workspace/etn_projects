
package mx.com.ib.threads;

import java.util.Vector;

import mx.com.ib.app.MainClass;
import mx.com.ib.connection.Communications;
import mx.com.ib.connection.HttpListener;
import mx.com.ib.controls.BorderBasicEditField;
import mx.com.ib.etn.view.animated.SplashScreen;
import mx.com.ib.utils.Utils;
import mx.com.ib.views.MessagePopupScreen;
import net.rim.device.api.system.Application;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;

import org.json.me.JSONObject;


public class MontoInterjetThread extends Thread implements HttpListener {

	public String url = null;
	public String reservacion = null;
	public BorderBasicEditField monto= null;
	
	public MontoInterjetThread(String reservacion, BorderBasicEditField monto){
		this.reservacion = reservacion;
		this.monto = monto;
		this.url = MainClass.URL_INTERJET_GET_MONTO + "?pnr=" + this.reservacion;
	}
	
	public void run(){
		connect();
	}
/*
	public void receiveHttpResponse(int appCode, byte[] response) {

		String mensajeMonto = new String(response);

		JSONObject jsonObject = null;

		try{

			jsonObject = new JSONObject(mensajeMonto);
			
		} catch(Exception e){
			
			jsonObject = new JSONObject();
			
			e.printStackTrace();
			getMessageError(null);
		}
		
		try{

			String monto =  jsonObject.optString("monto", "");
			String status =  jsonObject.optString("status", "");
			String pnr =  jsonObject.optString("pnr", "");
			
			
			synchronized (Application.getEventLock()) {
				MainClass.splashScreen.remove();
			}			
			
			//SplashScreen splashScreen = SplashScreen.getInstance();
			//splashScreen.remove();
			
			String error = null;
			
			if (monto != null && monto != "") {
				if (status.equals("Hold")) {
					reservacion = pnr;
					this.monto.setText(monto);
				} else if (status == "") {
					//this.monto.setText("El n�mero de reservaci�n no existe.");
					error = "El n�mero de reservaci�n no existe.";
				} else {
					//this.monto.setText("El periodo de pago para esta reservaci�n ha expirado.");
					error = "El periodo de pago para esta reservaci�n ha expirado.";
				}
			} else {
				this.monto.setText("El n�mero de reservaci�n no es v�lido.");
				error = "El n�mero de reservaci�n no es v�lido.";
			}
			
			if (error != null){
				
				getMessageError(error);
			}
			
		} catch(Exception e){
			e.printStackTrace();
			getMessageError(null);
		}
	}
*/
	
	
	public void receiveHttpResponse(int appCode, byte[] response) {

		String mensajeMonto = null;
		String monto = null;
		
		try{
			mensajeMonto = new String(response,0,response.length,"UTF-8");
			monto = MainClass.jsParser.getMontoInterjet(mensajeMonto);

			
			if(monto.equals("")){
				synchronized (Application.getEventLock()) {
					MainClass.splashScreen.remove();
					this.monto.setText("N�mero de reservaci�n inv�lido.");
				}
				
			} else if(monto.equals("null")){
				synchronized (Application.getEventLock()) {
					MainClass.splashScreen.remove();
					this.monto.setText("N�mero de reservaci�n inv�lido.");
				}
				
			} else if(monto != null && !monto.equals("")){
				synchronized (Application.getEventLock()) {
					MainClass.splashScreen.remove();
					this.monto.setText(monto);
				}
			} else {
				synchronized (Application.getEventLock()) {
					MainClass.splashScreen.remove();
					Dialog.alert("No se puede obtener monto. Intente de nuevo.");
				}
			}
		} catch(Exception e){
			e.printStackTrace();
			getMessageError("Error al momento de recibir la rspuesta.");
		}
	}
	public void connect(){
		if(MainClass.idealConnection != null){
			if(this.url != null){
				try{
					Communications.sendHttpRequest(this, this.url);
				}
				catch(Throwable t){
					
				}
			} else {
				Utils.checkConnectionType();
				this.connect();
			}
		}
	}

	public void handleHttpError(int errorCode, String error) {
	}

	public void receiveEstatus(String msg) {
	}

	public void receiveHeaders(Vector _headers) {
	}

	public boolean isDestroyed() {
		return false;
	}
	
	public void getMessageError(String error){
		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.remove();
			if (error == null){
				error = "Informaci�n no disponible. Favor de intentar m�s tarde";
			}
			
			UiApplication.getUiApplication().pushScreen(new MessagePopupScreen(error, Field.NON_FOCUSABLE));
		}
	}

}

package mx.com.ib.threads;

import java.io.UnsupportedEncodingException;
import java.util.Vector;

import crypto.Crypto;

import net.rim.device.api.system.Application;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;

import mx.com.ib.app.MainClass;
import mx.com.ib.beans.PurchaseBean;
import mx.com.ib.connection.Communications;
import mx.com.ib.connection.HttpListener;
import mx.com.ib.utils.Utils;
import mx.com.ib.views.FolioScreen;
import mx.com.ib.views.MessagePopupScreen;

public class PurchaseVitaIndThread extends Thread implements HttpListener{
	
	public String url = MainClass.URL_PURCHASE_VITA_IND;
	public String post = "";
	public String pass = "";
	
	public PurchaseVitaIndThread(String json,String pass){		
		this.post = "json="+json;
		this.pass=pass;
	}
	
	public void run() {
		// TODO Auto-generated method stub
		connect();
	}
	
	public void connect(){
		
		Application.getApplication();
		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.start();
		}
		
		if(MainClass.idealConnection != null){			
			if(this.url != null){				
				try{				    	   
					Communications.sendHttpPost(this,this.url,this.post);
				}catch(Throwable t){

				}
			}
		}else{		
			//System.out.println("ELSE: ");
			Utils.checkConnectionType();
			this.connect();
		}		
	}

	public void handleHttpError(int errorCode, String error) {
		// TODO Auto-generated method stub
		getMessageError();
	}

	public boolean isDestroyed() {
		// TODO Auto-generated method stub
		return false;
	}

	public void receiveEstatus(String msg) {
		// TODO Auto-generated method stub		
	}

	public void receiveHeaders(Vector _headers) {
		// TODO Auto-generated method stub		
	}

	public void receiveHttpResponse(int appCode, byte[] response) {
		// TODO Auto-generated method stub		
		
		String sTemp = null;
		StringBuffer sb = new StringBuffer();
		
		try {
			
			sb.append(new String(response,0,response.length,"UTF-8"));
			sTemp = new String (sb.toString());
			//System.out.println("STEMP: "+sTemp);
			sTemp = Crypto.aesDecrypt(Utils.parsePass(pass), sTemp);
			//System.out.println("STEMP: "+sTemp);
			
			PurchaseBean purchaseBean = MainClass.jsParser.getResultPurchase(sTemp);
			
			UiApplication.getUiApplication();
			
			synchronized (Application.getEventLock()) {				
				MainClass.splashScreen.remove();
				UiApplication.getUiApplication().pushScreen(new FolioScreen(purchaseBean));
				// UiApplication.getUiApplication().pushScreen(new MessagePopupScreen(sTemp, Field.NON_FOCUSABLE));				
			}
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			getMessageError();
		}		
	}
	
	public void getMessageError(){

		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.remove();
			UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Informaci�n no disponible. Favor de intentar m�s tarde", Field.NON_FOCUSABLE));
		}
		
	}

}
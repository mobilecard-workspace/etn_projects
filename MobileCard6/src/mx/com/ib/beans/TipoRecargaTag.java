package mx.com.ib.beans;

public class TipoRecargaTag {
    private String clave;
    private String nombre;
 /**
    * @return the clave
    */
   public String getClave() {
       return clave;
   }
   
   public TipoRecargaTag(String clave,String nombre){
	   this.clave=clave;
	   this.nombre=nombre;
	   
   }

   /**
    * @param clave the clave to set
    */
   public void setClave(String clave) {
       this.clave = clave;
   }

   /**
    * @return the nombre
    */
   public String getNombre() {
       return nombre;
   }

   /**
    * @param nombre the nombre to set
    */
   public void setNombre(String nombre) {
       this.nombre = nombre;
   }
    
    
    
}


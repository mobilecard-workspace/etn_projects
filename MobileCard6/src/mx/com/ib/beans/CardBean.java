package mx.com.ib.beans;

public class CardBean {
	
	private String numTarjeta;
	private String domTarjeta;
	private String cpTarjeta;
	private String vencimientoTarjeta;
	
	public String getNumTarjeta() {
		return numTarjeta;
	}
	public void setNumTarjeta(String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}
	public String getDomTarjeta() {
		return domTarjeta;
	}
	public void setDomTarjeta(String domTarjeta) {
		this.domTarjeta = domTarjeta;
	}
	public String getCpTarjeta() {
		return cpTarjeta;
	}
	public void setCpTarjeta(String cpTarjeta) {
		this.cpTarjeta = cpTarjeta;
	}
	public String getVencimientoTarjeta() {
		return vencimientoTarjeta;
	}
	public void setVencimientoTarjeta(String vencimientoTarjeta) {
		this.vencimientoTarjeta = vencimientoTarjeta;
	}
	
	public String toString() {
		return "CardBean [numTarjeta=" + numTarjeta + "]";
	}
}

package mx.com.ib.beans;

public class ConsultBean {

	private String user = "";
	private String provider = "";
	private String product = "";
	private String date = "";
	private String concept = "";
	private String amount = "";
	private String numAuthorization = "";
	private String codeError = "";
	private String carId = "";
	private String status = "";
	
	public ConsultBean(String text1, String text2, String text3) {
		this.date = text1;
		this.concept = text2;
		this.amount = text3;
	}
	
	public ConsultBean() {
		super();
	}
	public ConsultBean(String user, String provider, String product,
			String date, String concept, String amount,
			String numAuthorization, String codeError, String carId,
			String status) {
		super();
		this.user = user;
		this.provider = provider;
		this.product = product;
		this.date = date;
		this.concept = concept;
		this.amount = amount;
		this.numAuthorization = numAuthorization;
		this.codeError = codeError;
		this.carId = carId;
		this.status = status;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getConcept() {
		return concept;
	}
	public void setConcept(String concept) {
		this.concept = concept;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getNumAuthorization() {
		return numAuthorization;
	}
	public void setNumAuthorization(String numAuthorization) {
		this.numAuthorization = numAuthorization;
	}
	public String getCodeError() {
		return codeError;
	}
	public void setCodeError(String codeError) {
		this.codeError = codeError;
	}
	public String getCarId() {
		return carId;
	}
	public void setCarId(String carId) {
		this.carId = carId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
}

package mx.com.ib.app;

import mx.com.ib.beans.BeneficiarioBean;
import mx.com.ib.beans.CardBean;
import mx.com.ib.beans.GeneralBean;
import mx.com.ib.beans.IndividualBean;
import mx.com.ib.beans.TipoRecargaTag;
import mx.com.ib.beans.UserBean;
import mx.com.ib.beans.UsuarioTag;
import mx.com.ib.connection.Communications;
import mx.com.ib.controls.PromotionLabelField;
import mx.com.ib.parser.JSONParser;
import mx.com.ib.splash.SplashScreen;
import mx.com.ib.views.WelcomeScreen;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.FontFamily;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;

public class MainClass extends UiApplication {

	public static MainClass mainClass = null;

	// URLS's
/*
	public static String URL_LOGIN = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_userLogin.jsp";
	public static String URL_PURCHASE = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_purchase.jsp";
	public static String URL_GET_BANKS = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getBanks.jsp";
	public static String URL_TERMS = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getConditions.jsp";
	public static String URL_USER_UPDATE = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_userUpdate.jsp";
	public static String URL_USER_INSERT = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_userInsert.jsp";
	public static String URL_GET_USERDATA = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getUserData.jsp";
	public static String URL_GET_PRODUCTS = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getProducts.jsp";
	public static String URL_GET_CARDTYPES = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getCardType.jsp";
	public static String URL_GET_PROVIDERS = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getProviders.jsp";
	public static String URL_GET_CATEGORIES = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getCategoris.jsp";
	public static String URL_GET_PROMOTIONS = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getPromotions.jsp";
	public static String URL_GET_CONSULTS = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getUserPurchases.jsp";
	public static String URL_UPDATE_PASS = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_userPasswordUpdate.jsp";
	public static String URL_UPDATE_PASS_MAIL = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_userPasswordUpdateMail.jsp";
	public static String URL_PURCHASE_IAVE = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_purchase_iave.jsp";
	public static String URL_GET_PASSWORD = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_RecoveryUP.jsp";
	public static String URL_SET_INVITA = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_setInvita.jsp";
	public static String URL_GET_COMISION = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getComision.jsp";
	public static String URL_GET_TAGS = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getTipoRecargaTag.jsp";
	public static String URL_SET_TAG = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_setTag.jsp";
	public static String URL_GET_TAGS_USER = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getTags.jsp";
	public static String URL_REMOVE_TAGS_USER = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_removeTag.jsp";
	public static String URL_PURCHASE_OHL = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_purchase_ohl.jsp";
	public static String URL_PURCHASE_VIAPASS = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_purchase_viapass.jsp";
	public static String URL_SET_SMS = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_setSMS.jsp";
	public static String URL_PURCHASE_PASE = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_purchase_pase.jsp";
	public static String URL_GET_ESTADOS = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getEstados.jsp";
	public static String URL_GET_ESTADO_CIVIL = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getEstadoCivil.jsp";
	public static String URL_GET_PARENTESCO = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_getParentesco.jsp";
	public static String URL_PURCHASE_VITA_IND = "http://201.161.23.42:8080/AddCelBridge/Servicios/adc_purchase_vitamedica.jsp";

	public static String URL_INTERJET_GET_MONTO = "http://www.addcelapp.com:12345/getPNR3.aspx?pnr=";
	public static String URL_INTERJET_PURCHASE = "http://www.addcelapp.com:12345/getInterjetTransaction.aspx";
*/

	
	
	  public static String URL_VERSIONADOR =
	  "http://www.mobilecard.mx:8080/Vitamedica/getVersion?idApp=2&idPlataforma=3";
	  public static String URL_LOGIN =
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_userLogin.jsp";
	  public static String URL_PURCHASE =
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_purchase.jsp";
	  public static String URL_GET_BANKS =
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_getBanks.jsp";
	  public static String URL_TERMS =
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_getConditions.jsp";
	  public static String URL_USER_UPDATE =
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_userUpdate.jsp";
	  public static String URL_USER_INSERT =
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_userInsert.jsp";
	  public static String URL_GET_USERDATA =
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_getUserData.jsp";
	  public static String URL_GET_PRODUCTS =
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_getProducts.jsp";
	  public static String URL_GET_CARDTYPES =
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_getCardType.jsp";
	  public static String URL_GET_PROVIDERS =
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_getProviders.jsp";
	  public static String URL_GET_CATEGORIES =
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_getCategoris.jsp";
	  public static String URL_GET_PROMOTIONS =
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_getPromotions.jsp";
	  public static String URL_GET_CONSULTS =
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_getUserPurchases.jsp"
	  ; public static String URL_UPDATE_PASS =
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_userPasswordUpdate.jsp"
	  ; public static String URL_UPDATE_PASS_MAIL =
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_userPasswordUpdateMail.jsp"
	  ; public static String URL_PURCHASE_IAVE =
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_purchase_iave.jsp";
	  public static String URL_GET_PASSWORD=
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_RecoveryUP.jsp";
	  public static String URL_SET_INVITA=
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_setInvita.jsp";
	  public static String URL_GET_COMISION=
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_getComision.jsp";
	  public static String URL_GET_TAGS=
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_getTipoRecargaTag.jsp"
	  ; public static String URL_SET_TAG=
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_setTag.jsp"; public
	  static String URL_GET_TAGS_USER=
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_getTags.jsp";
	  public static String URL_REMOVE_TAGS_USER=
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_removeTag.jsp";
	  public static String URL_PURCHASE_OHL =
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_purchase_ohl.jsp";
	  public static String URL_PURCHASE_VIAPASS =
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_purchase_viapass.jsp"
	  ; public static String URL_SET_SMS=
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_setSMS.jsp"; public
	  static String URL_PURCHASE_PASE =
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_purchase_pase.jsp";
	  public static String URL_GET_ESTADOS=
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_getEstados.jsp";
	  public static String URL_GET_ESTADO_CIVIL=
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_getEstadoCivil.jsp"
	  ; public static String URL_GET_PARENTESCO=
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_getParentesco.jsp";
	  public static String URL_PURCHASE_VITA_IND=
	  "http://mobilecard.mx:8080/AddCelBridge/Servicios/adc_purchase_vitamedica.jsp"; 

/*	  
	  public static String
	  URL_INTERJET_GET_MONTO="http://www.addcelapp.com:12345/getPNR2.aspx?pnr="; 
	  public static String URL_INTERJET_PURCHASE =
	  "http://www.addcelapp.com:12345/getInterjetTransaction.aspx";
*/

	  public static String
	  URL_INTERJET_GET_MONTO="http://www.mobilecard.mx:8080/InterjetWeb/getPNR3"; 
	  public static String URL_INTERJET_PURCHASE =
	  "http://www.mobilecard.mx:8080/InterjetWeb/getInterjetTransaction";
	  
	  
	  
	  public static String URL_GOB_ESTADO_LINEA =
	  "http://mobilecard.mx:8595/GobEstado/Consumer";
	  
	  public static String URL_GOB_ESTADO_INSERTA =
	  "http://www.addcelapp.com:12345/getId.aspx";
	  
	public static JSONParser jsParser = new JSONParser();
	public static String BTN_REGISTER = "btn_registrarse";
	public static String BTN_REGISTER_OVER = "btn_registrarse_ov";
	public static String BTN_OK = "btn_ok";
	public static String BTN_OK_OVER = "btn_ok_ov";
	public static String BTN_DESCARGAR = "btn_descargar";
	public static String BTN_DESCARGAR_OVER = "btn_descargar_ov";
	public static String BTN_IGNORAR = "btn_ignorar";
	public static String BTN_IGNORAR_OVER = "btn_ignorar_ov";

	public static String BTN_CONTINUE = "btn_cont";
	public static String BTN_CONTINUE_OVER = "btn_cont_ov";

	public static String BTN_SMS = "btn_sms";
	public static String BTN_SMS_OVER = "btn_sms_ov";

	public static String BTN_PSWD = "btn_contra";
	public static String BTN_PSWD_OVER = "btn_contra_ov";
	public static String BTN_SAVE = "btn_guardar";
	public static String BTN_SAVE_OVER = "btn_guardar_ov";
	public static String BTN_TAGS = "btn_tags";
	public static String BTN_TAGS_OVER = "btn_tags_ov";
	public static String BTN_TAGS2 = "btn_ctags";
	public static String BTN_TAGS2_OVER = "btn_ctags_ov";
	public static String BTN_COMPRAR = "btn_comprar";
	public static String BTN_COMPRAR_OVER = "btn_comprar_ov";
	
	/*
	 * public static String BTN_BUY = "btn_compras"; public static String
	 * BTN_BUY_OVER = "btn_compras_ov"; public static String BTN_CONSULT =
	 * "btn_tienda"; public static String BTN_CONSULT_OVER = "btn_tienda_ov";
	 * public static String BTN_ACCOUNT = "btn_cuenta"; public static String
	 * BTN_ACCOUNT_OVER = "btn_cuenta_ov"; public static String BTN_PROMOTIONS =
	 * "btn_promo"; public static String BTN_PROMOTIONS_OVER = "btn_promo_ov";
	 */
	
	public static String BTN_BUY = "btn_compras_ov";
	public static String BTN_BUY_OVER = "btn_compras";
	public static String BTN_CONSULT = "btn_tienda_ov";
	public static String BTN_CONSULT_OVER = "btn_tienda";
	public static String BTN_ACCOUNT = "btn_cuenta_ov";
	public static String BTN_ACCOUNT_OVER = "btn_cuenta";
	public static String BTN_PROMOTIONS = "btn_promo_ov";
	public static String BTN_PROMOTIONS_OVER = "btn_promo";

	/*
	 * public static String BTN_BUY2 = "btn_compras2"; public static String
	 * BTN_BUY_OVER2 = "btn_compras2_ov"; public static String BTN_CONSULT2 =
	 * "btn_tienda2"; public static String BTN_CONSULT_OVER2 = "btn_tienda2_ov";
	 * public static String BTN_ACCOUNT2 = "btn_cuenta2"; public static String
	 * BTN_ACCOUNT_OVER2 = "btn_cuenta2_ov"; public static String
	 * BTN_PROMOTIONS2 = "btn_promo2"; public static String BTN_PROMOTIONS_OVER2
	 * = "btn_promo2_ov";
	 */

	public static int BG_GRAY = 0x414042;
	public static int BG_ORANGE = 0xF16430;
	public static int COLOR_LINK = 0x38D4FF;
	public static String RECOVERY_PASS = "-2";

	public static String BTN_SEMANA = "btn_semana";
	public static String BTN_SEMANA_OVER = "btn_semana_ov";
	public static String BTN_HOY = "btn_hoy";
	public static String BTN_HOY_OVER = "btn_hoy_ov";
	public static String BTN_ESTE_MES = "btn_este_mes";
	public static String BTN_ESTE_MES_OVER = "btn_este_mes_ov";
	public static String BTN_MES = "btn_mes";
	public static String BTN_MES_OVER = "btn_mes_ov";
	public static String BTN_BACK = "btn_back";
	public static String BTN_BACK_OVER = "btn_back_ov";

	public static String BTN_IZQ = "btn_izq_off";
	public static String BTN_DER = "btn_der_off";
	public static String BTN_INVITA = "invita";
	public static String BTN_INVITA_OVER = "invita";
	public static String BTN_INVITAR = "btn_invita";
	public static String BTN_INVITAR_OVER = "btn_invita_ov";

	// Imagenes

	public static String IMG_REGISTRATE = "registrate";
	public static String IMG_ACTUALIZATE = "actualizate";

	// Loader

	public static String GIF_LOADER = "preloader_b.gif";
	public static String COMISION_IAVE = "0.0";
	public static String COMISION_OHL = "0.0";
	public static String COMISION_VIAPASS = "0.0";
	public static String COMISION_IAVE_VIP = "0.0";
	public static String COMISION_TAE_TELCEL = "0.0";
	public static String COMISION_VITAMEDICA = "0.0";
	public static String COMISION_ETN = "0.0";
	// Headers

	public static String HD_TIENDA = "header_tienda.png";
	public static String HD_TIEMPO_AIRE = "header_tiempoaire.png";
	public static String HD_REGISTRATE = "header_registrate.png";
	public static String HD_ACTUALIZATE = "header_actualizate.png";
	public static String HD_COMPRAS = "header_compras.png";
	public static String HD_CONFIRMACION = "header_confirmacion.png";
	public static String HD_PROMOS = "header_promos.png";
	public static String HD_INVITA = "invita.png";

	// Fondos
	public static String BG_SPLASH = "splash.png";
	public static String BG_VITA = "infovm.png";
	public static String BG_LOGIN = "login.png";
	public static String BG_CVV2 = "cvv2.png";
	public static String BG_IAVETAG = "tagiave.png";
	public static String BG_IAVETAG2 = "iave2.png";
	public static String BG_OHLTAG = "tag_ohl.png";
	public static String BG_VIAPASS = "tagviapass1.png";
	public static String BG_VIAPASS1 = "tagviapass2.png";
	public static String BG_PASE = "tagpase.png";
	public static String BG_PASE2 = "tagpase2.png";
	public static String BG_IAVEDV = "dviave.png";
	public static String BG_MENU = "menu.png";
	public static String BG_MENU_MC = "menu_mc.png";
	public static String BG_INVITA = "invita.png";
	public static String BG_PROMO = "promo.png";
	public static String BG_COMPRA = "fondoCompra.png";
	public static String BG_CONSULTA = "compras.png";
	public static String BG_CUENTA = "cuenta.png";
	public static String BG_CONFIRMACION = "confirmacion.png";
	public static String FOOTER = "footer.png";
	// Font

	public static FontFamily alphaSansFamily = null;
	public static Font appFont = null;
	public static FontFamily alphaSansFamily1 = null;
	public static Font appFont1 = null;
	public static Font appFont2 = null;
	public static Font appFont3 = null;
	public static String idealConnection = null;
	public static String key = "1234567890ABCDEF0123456789ABCDEF";
	public static String TAG_REGISTER = "\"etiqueta\":\"" + ""
			+ "\",\"numero\":\"" + "0" + "\",\"dv\":\"" + "0"
			+ "\",\"idtiporecargatag\":\"" + "1";

	public static SplashScreen splashScreen = null;

	public static Bitmap bitmap = null;

	public static String userJSON = "";
	public static String userPswd = "";
	public static String p = "";
	public static String p2 = "";
	public static String passTEMP = "xCL8m39sJ1";

	public static String textTitle = "";
	public static String textTitleInternal = "";
	public static String IDPROVEEDOR = "";
	public static String IDPRODUCTO = "";
	public static String textTitleConfirm = "";
	public static String X = "0.0";
	public static String Y = "0.0";
	public static String TIPO = "BlackBerry";
	public static String SOFTWARE = "5.0";
	public static String MODELO = "0";

	public static UserBean userBean = null;
	public static IndividualBean invididualBean = null;
	public static int edo = 0;
	public static int sex = 0;
	public static int type = 0;
	public static int bank = 0;
	public static int day = 0;
	public static int month = 0;
	public static int year = 0;
	public static int monthCredit = 0;
	public static int yearCredit = 0;

	public static int tipoVita = 0;

	public static String IdUser = "";
	public static String msg = "";
	public static GeneralBean[] EdoBeans = null;
	public static GeneralBean[] bankBeans = null;
	public static GeneralBean[] creditBeans = null;
	public static GeneralBean[] parenBeans = null;
	public static GeneralBean[] providersBeans = null;
	public static GeneralBean[] EdoCivilBeans = null;
	public static TipoRecargaTag[] TagsBeans = null;
	public static UsuarioTag[] TagsBeansbyUser = null;
	public static BeneficiarioBean[] Beneficiarios = null;

	// BEANS DE TARJETAS AGREGADOS EL 6 DE DICIEMBRE DE 2012

	public static CardBean visaBean = null;
	public static CardBean amexBean = null;

	public static PromotionLabelField[] promotionLabelField = null;

	public static void main(String args[]) {

		Communications.start();
		mainClass = new MainClass();
		mainClass.enterEventDispatcher();

	}

	public MainClass() {

		MainClass.splashScreen = new SplashScreen();

		try {

			alphaSansFamily = FontFamily.forName("BBAlpha Serif");
			appFont = alphaSansFamily.getFont(Font.PLAIN, 9, Ui.UNITS_pt);
			alphaSansFamily1 = FontFamily.forName("BBAlpha Serif");
			appFont1 = alphaSansFamily.getFont(Font.PLAIN, 5, Ui.UNITS_pt);
			appFont3 = alphaSansFamily.getFont(Font.PLAIN, 6, Ui.UNITS_pt);
			appFont2 = alphaSansFamily.getFont(Font.PLAIN, 7, Ui.UNITS_pt);

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		initVariables();
		pushScreen(new WelcomeScreen());
	}

	public void initVariables() {
		int wid = Display.getWidth();
		int hei = Display.getHeight();

		System.out.println("Display " + wid + "x" + hei);
		int screen = 0;

		int sum = Math.abs(240 - wid) + Math.abs(260 - hei);

		int temp = Math.abs(320 - wid) + Math.abs(240 - hei);
		if (temp < sum) {
			sum = temp;
			screen = 1;
		}

		temp = Math.abs(360 - wid) + Math.abs(480 - hei);
		if (temp < sum) {
			sum = temp;
			screen = 2;
		}

		temp = Math.abs(480 - wid) + Math.abs(320 - hei);
		if (temp < sum) {
			sum = temp;
			screen = 3;
		}

		temp = Math.abs(480 - wid) + Math.abs(360 - hei);
		if (temp < sum) {
			sum = temp;
			screen = 4;
		}

		// IMG_HEADER_LOGO = "logo-nuevo.png";

		String carpeta = "";
		String img = ".png";
		switch (screen) {
		case 0:
			System.out.println("Se usa 240x260");
			carpeta += "240x260_";
			img = "_sm" + img;
			break;
		case 1:
			System.out.println("Se usa 320x240");
			carpeta += "320x240_";
			img = "_sm" + img;
			break;
		case 2:
			System.out.println("Se usa 360x480");
			carpeta += "360x480_";
			break;
		case 3:
			System.out.println("Se usa 480x320");
			carpeta += "480x320_";
			break;
		case 4:
			System.out.println("Se usa 480x360");
			carpeta += "480x360_";
			break;
		}

		BG_SPLASH = carpeta + BG_SPLASH;
		BG_LOGIN = carpeta + BG_LOGIN;
		BG_MENU = carpeta + BG_MENU;
		BG_MENU_MC = carpeta + BG_MENU_MC;
		BG_PROMO = carpeta + BG_PROMO;
		BG_COMPRA = carpeta + BG_COMPRA;
		BG_CONSULTA = carpeta + BG_CONSULTA;
		BG_CUENTA = carpeta + BG_CUENTA;
		BG_CONFIRMACION = carpeta + BG_CONFIRMACION;
		HD_TIENDA = carpeta + HD_TIENDA;
		HD_TIEMPO_AIRE = carpeta + HD_TIEMPO_AIRE;
		HD_REGISTRATE = carpeta + HD_REGISTRATE;
		HD_ACTUALIZATE = carpeta + HD_ACTUALIZATE;
		HD_COMPRAS = carpeta + HD_COMPRAS;
		HD_CONFIRMACION = carpeta + HD_CONFIRMACION;
		HD_PROMOS = carpeta + HD_PROMOS;
		HD_INVITA = carpeta + HD_INVITA;

		FOOTER = carpeta + FOOTER;

		BTN_REGISTER = BTN_REGISTER + img;
		BTN_REGISTER_OVER = BTN_REGISTER_OVER + img;
		BTN_OK = BTN_OK + img;
		BTN_OK_OVER = BTN_OK_OVER + img;

		BTN_INVITAR = BTN_INVITAR + img;
		BTN_INVITAR_OVER = BTN_INVITAR_OVER + img;

		BTN_DESCARGAR = BTN_DESCARGAR + img;
		BTN_DESCARGAR_OVER = BTN_DESCARGAR_OVER + img;
		BTN_IGNORAR = BTN_IGNORAR + img;
		BTN_IGNORAR_OVER = BTN_IGNORAR_OVER + img;

		BTN_CONTINUE = BTN_CONTINUE + img;
		BTN_CONTINUE_OVER = BTN_CONTINUE_OVER + img;

		BTN_PSWD = BTN_PSWD + img;
		BTN_PSWD_OVER = BTN_PSWD_OVER + img;

		BTN_SAVE = BTN_SAVE + img;
		BTN_SAVE_OVER = BTN_SAVE_OVER + img;

		BTN_TAGS = BTN_TAGS + img;
		BTN_TAGS_OVER = BTN_TAGS_OVER + img;

		BTN_TAGS2 = BTN_TAGS2 + img;
		BTN_TAGS2_OVER = BTN_TAGS2_OVER + img;

		BTN_BACK = BTN_BACK + img;
		BTN_BACK_OVER = BTN_BACK_OVER + img;

		BTN_COMPRAR = BTN_COMPRAR + ".png";
		BTN_COMPRAR_OVER = BTN_COMPRAR_OVER + ".png";

		BTN_BUY = BTN_BUY + img;
		BTN_SMS_OVER = BTN_SMS_OVER + img;
		BTN_SMS = BTN_SMS + img;

		BTN_BUY_OVER = BTN_BUY_OVER + img;
		BTN_CONSULT = BTN_CONSULT + img;
		BTN_CONSULT_OVER = BTN_CONSULT_OVER + img;
		BTN_ACCOUNT = BTN_ACCOUNT + img;
		BTN_ACCOUNT_OVER = BTN_ACCOUNT_OVER + img;
		BTN_PROMOTIONS = BTN_PROMOTIONS + img;
		BTN_PROMOTIONS_OVER = BTN_PROMOTIONS_OVER + img;
		BTN_INVITA_OVER = BTN_INVITA_OVER + ".png";
		BTN_INVITA = BTN_INVITA + img;
		System.out.println("el boton que usea es+++++++++++++++" + BTN_INVITA);

		/*
		 * BTN_BUY2 = BTN_BUY2+img; BTN_BUY_OVER2 = BTN_BUY_OVER2+img;
		 * BTN_CONSULT2 = BTN_CONSULT2+img; BTN_CONSULT_OVER2 =
		 * BTN_CONSULT_OVER2+img; BTN_ACCOUNT2 = BTN_ACCOUNT2+img;
		 * BTN_ACCOUNT_OVER2 = BTN_ACCOUNT_OVER2+img; BTN_PROMOTIONS2 =
		 * BTN_PROMOTIONS2+img; BTN_PROMOTIONS_OVER2 = BTN_PROMOTIONS_OVER2+img;
		 */

		BTN_SEMANA = BTN_SEMANA + ".png";
		BTN_SEMANA_OVER = BTN_SEMANA_OVER + ".png";
		BTN_HOY = BTN_HOY + ".png";
		BTN_HOY_OVER = BTN_HOY_OVER + ".png";
		BTN_ESTE_MES = BTN_ESTE_MES + ".png";
		BTN_ESTE_MES_OVER = BTN_ESTE_MES_OVER + ".png";
		BTN_MES = BTN_MES + ".png";
		BTN_MES_OVER = BTN_MES_OVER + ".png";

		BTN_IZQ = BTN_IZQ + ".png";
		BTN_DER = BTN_DER + ".png";

		IMG_REGISTRATE = IMG_REGISTRATE + ".png";
		IMG_ACTUALIZATE = IMG_ACTUALIZATE + ".png";
	}

}

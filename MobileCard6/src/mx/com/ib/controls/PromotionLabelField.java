package mx.com.ib.controls;

import mx.com.ib.app.MainClass;
import mx.com.ib.beans.ProductBean;
import mx.com.ib.threads.ProductsThread;
import mx.com.ib.utils.Utils;
import mx.com.ib.views.BuyScreen;
import mx.com.ib.views.ConsultScreen;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.system.EncodedImage;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.LabelField;

public class PromotionLabelField extends LabelField{
	
	public ProductBean productBean = null;
	public Bitmap image = null;
	public int myWidht = Display.getWidth() - 20;

	public PromotionLabelField(ProductBean promos){
		
		super(promos.getDescription(), Field.FOCUSABLE);
		
		this.productBean = promos;
		this.productBean = promos;
		int x = Display.getWidth()-10;
		int y = 0;
		int xImage = 400;
		int yImage = 200;
		
		y =  (x * yImage)/xImage;
		
		image = Utils.resizeBitmap(Bitmap.getBitmapResource("dummy_loader.jpg"), x, y); 
		
		
	}
	
	protected void paint(Graphics graphics) {
		// TODO Auto-generated method stub
		graphics.drawBitmap((myWidht - this.image.getWidth())/2, 0, this.image.getWidth(), this.image.getHeight(), this.image, 0, 0);
		invalidate();

	}
	
	protected void drawFocus(Graphics graphics, boolean on) {
		// TODO Auto-generated method stub
		graphics.drawBitmap((myWidht - this.image.getWidth())/2, 0, this.image.getWidth(), this.image.getHeight(), this.image, 0, 0);
		invalidate();

	}
	
	protected boolean navigationClick(int status, int time) {
		// TODO Auto-generated method stub
		MainClass.splashScreen.start();
		new ProductsThread().start();
		return true;
	}

	protected void layout(int width, int height) {
		// TODO Auto-generated method stub
		this.setExtent(myWidht, this.image.getHeight());
	}
	
	public ProductBean getProductBean() {
		return productBean;
	}

	public void setProductBean(ProductBean productBean) {
		this.productBean = productBean;
	}

	public Bitmap getImage() {
		return image;
	}

	public void setImage(Bitmap image) {
		
		int x = Display.getWidth()-10;
		int y = 0;
		int xImage = image.getWidth();
		int yImage = image.getHeight();
		
		y =  (x * yImage)/xImage;
		
		this.image = Utils.resizeBitmap(image, x, y);
		
		 MainClass.mainClass.repaint();
		
	}

}

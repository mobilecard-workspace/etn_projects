package mx.com.ib.controls;

import mx.com.ib.app.MainClass;
import mx.com.ib.utils.Utils;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.FontFamily;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.LabelField;

public class LabelRegion extends LabelField
{
	
	public String comp = "";
	public String region = "";
	public String _text = null;
	
	public int color = 0;
	public int width = 0;
	public int _width;
	public int _height;
	
	public String[] arrayWords = null;	
	
	public FontFamily alphaSansFamily  = null;
	
	public Font appFont ;
	
	public LabelRegion(String text, int w, long style, int _width, int color)
	{
		
		super(text, style);		
		
		if(text.length()>60){
			try {			
				alphaSansFamily = FontFamily.forName("BBAlpha Serif");
				if(Display.getWidth()<320){
					appFont = alphaSansFamily.getFont(Font.PLAIN, 6, Ui.UNITS_pt);
				}else if(Display.getWidth()==320){
					appFont = alphaSansFamily.getFont(Font.PLAIN, 8, Ui.UNITS_pt);
				}else{
					appFont = alphaSansFamily.getFont(Font.PLAIN, 5, Ui.UNITS_pt);
				}				
			} catch (ClassNotFoundException e) {
			}
			setFont(appFont);
		}else{
			setFont(MainClass.appFont);
		}
		

		this._text = text;
		this.comp = text;		
	
		this.width = w;
		this._width = _width;
		this.color = color;
		
	}
	
	protected void paint( Graphics g)
	{		
//		for(int i=0; i<this._width; i= i+imageOff.getWidth())
//		{
//			g.drawBitmap(i, 0, imageOff.getWidth(), imageOff.getHeight(), imageOff, 0, 0);
//		}
		g.setColor(this.color);
		
			if(Display.getWidth()>320){
				if(getFont().getAdvance(this.comp)<=(Display.getWidth()-10)){
					Utils.paintTextMiddle(g, this.comp, (getWidth()-(getFont().getAdvance(this.comp)))/2, 10, 40);
				}else{
					Utils.paintTextMiddle(g, this.comp, 5, 0, 40);
				}
			}else if(Display.getWidth()<320){
				
				if(getFont().getAdvance(this.comp) <= (Display.getWidth()-10)){
					
					Utils.paintTextMiddle(g, this.comp, (getWidth()-(getFont().getAdvance(this.comp)))/2, 10, 20);
					
				}else{
					
					Utils.paintTextMiddle(g, this.comp, 5, 2, 20);
					
				}
				
			}else{
				if(getFont().getAdvance(this.comp)<=(Display.getWidth()-10)){
					Utils.paintTextMiddle(g, this.comp, (getWidth()-(getFont().getAdvance(this.comp)))/2, 10, 25);
				}else{
					Utils.paintTextMiddle(g, this.comp, 5, 0, 25);
				}
			}
		
			
	}
	
	protected void drawFocus(Graphics g, boolean on)
	{		

		g.setColor(Color.WHITE);

			if(Display.getWidth()>320){
				if(getFont().getAdvance(this.comp)<=(Display.getWidth()-10)){
					Utils.paintTextMiddle(g, this.comp, (getWidth()-(getFont().getAdvance(this.comp)))/2, 10, 40);
				}else{
					Utils.paintTextMiddle(g, this.comp, 5, 0, 40);
				}
			}else if(Display.getWidth()<320){
				
				if(getFont().getAdvance(this.comp) <= (Display.getWidth()-10)){
					
					Utils.paintTextMiddle(g, this.comp, (getWidth()-(getFont().getAdvance(this.comp)))/2, 10, 20);
					
				}else{
					
					Utils.paintTextMiddle(g, this.comp, 5, 2, 20);
					
				}
				
			}else{
				if(getFont().getAdvance(this.comp)<=(Display.getWidth()-10)){
					Utils.paintTextMiddle(g, this.comp, (getWidth()-(getFont().getAdvance(this.comp)))/2, 10, 25);
				}else{
					Utils.paintTextMiddle(g, this.comp, 5, 0, 25);
				}
			}

	}
	
	protected boolean navigationClick(int status, int time) 
	{

//		if(this._idRegion==null){
//			
//		}else{
//			if(this._idRegion.equals("NonFocus")){
//				
//				UiApplication.getUiApplication().pushScreen(new RegionScreen());
//				
//			}else{
//				
//				MainClass.persistentRegion.setContents(this._idRegion);
//		        MainClass.persistentRegion.commit();
//		        MainClass.idScreen = MainClass.idBuyNowScreen;
//		        MainClass.feedBuyNow = null;
//		        MainClass.feedCategories = null;
//		        MainClass.feedSubCategories = null;
//		        MainClass.feedUpcoming = null;
//		        MainClass.feedStates = null;
//		        MainClass.feedCenters = null;
//		        MainClass.splashScreen.start();		        
//				MainClass.trackGral = MainClass.trackMaker + this.idState;
//				Utils.tracking(MainClass.trackGral+MainClass.trackPageBuyNow);
////				new BannerThread(MainClass.urlBanner).start();
//		        new BuyNowThread(MainClass.linkGralHotTix+MainClass.persistentRegion.getContents()+".json").start();
//			}     
//		}
        return true;        
	}
	
	protected void onUnfocus() {
		// TODO Auto-generated method stub
		super.onUnfocus();
	}
	
	protected void layout(int width, int height) 
	{

		this.setExtent(getPreferredWidth(), getPreferredHeight());		
		
	}
	
	public int getPreferredWidth() {
		// TODO Auto-generated method stub
		return Display.getWidth();
	}
	
	public int getPreferredHeight() {
		// TODO Auto-generated method stub
		return 20;
	}

	
}

package mx.com.ib.controls;

import net.rim.device.api.ui.component.Dialog;

public class GenericImageButtonField extends ImageButtonField {

	public net.rim.device.api.ui.Screen nextScreen;
	public String id = null;
	public String text = null;

	public GenericImageButtonField(String img, String img2, String id) {

		super(img, img, img2);
		this.id = id;
	}

	protected boolean navigationClick(int status, int time) {

		return true;
	}

	public boolean showQuestion() {
		String[] opciones = { "Si", "No" };
		int[] valores = { 1, 0 };
		int regreso;
		boolean sigue = false;
		regreso = Dialog
				.ask("Esta acci�n requiere hacer una conexion a los servidores de la Red Social, esto puede tomar varios minutos.�Desea continuar?",
						opciones, valores, 0);

		switch (regreso) {

		case 0:
			break;
		case 1:
			sigue = true;
			break;

		}
		return sigue;
	}

}

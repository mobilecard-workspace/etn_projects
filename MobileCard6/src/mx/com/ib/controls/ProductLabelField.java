package mx.com.ib.controls;

import mx.com.ib.app.MainClass;
import mx.com.ib.beans.GeneralBean;
import mx.com.ib.beans.ProductBean;
import mx.com.ib.etn.view.etnbuy.ViewETNBuy;
import mx.com.ib.threads.CatProductsThread;
import mx.com.ib.threads.ComisionThread;
import mx.com.ib.threads.ProviderThread;
import mx.com.ib.threads.VitamedicaThread;
import mx.com.ib.utils.Utils;
import mx.com.ib.views.CobroPlacaEdoMExico;
import mx.com.ib.views.ConfirmProductScreen;
import mx.com.ib.views.InterjetScreen;
import mx.com.ib.views.PlacasScreen;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import crypto.Crypto;

public class ProductLabelField extends LabelField {

	public String title = "";
	public Bitmap bitmap;
	public ProductBean productBeans = null;
	public GeneralBean generalBean = null;
	public boolean flag = false;
	private int pfWidth, pfHeight, padding;

	public ProductLabelField(String title) {

		super("", Field.FOCUSABLE);

		setConfig(title);
	}

	public ProductLabelField(ProductBean productBeans) {

		super("", Field.FOCUSABLE);

		this.productBeans = productBeans;

		setConfig(productBeans.getDescription());

	}

	public ProductLabelField(GeneralBean productBeans, boolean flag) {

		super("", Field.FOCUSABLE);

		this.flag = flag;
		String title = productBeans.getDescription();

		if (!productBeans.getName().equals("no")) {
			if (MainClass.IDPROVEEDOR.equals("10")) {

				title = productBeans.getName();
			} else if (MainClass.IDPROVEEDOR.equals("13")) {

				title = productBeans.getName();
			} else {

				title = productBeans.getDescription() + " "
						+ productBeans.getName();
			}

		}

		this.generalBean = productBeans;
		setConfig(title);
	}

	public ProductLabelField(GeneralBean productBeans) {

		super("", Field.FOCUSABLE);
		this.generalBean = productBeans;
		setConfig(productBeans.getDescription());
	}

	private void setConfig(String title) {

		this.title = title;

		pfWidth = Display.getWidth() / (14 / 4);
		pfHeight = Display.getWidth() / (14 / 4);

		setBitmap(Bitmap.getBitmapResource("tiempoaire.png"));
	}

	protected void paint(Graphics graphics) {

		if ((this.productBeans != null) || (this.productBeans == null && !flag)) {

			graphics.drawBitmap(
					getPreferredWidth() / 2 - bitmap.getWidth() / 2,
					getPreferredHeight() / 2 - bitmap.getHeight() / 2,
					this.bitmap.getWidth(), this.bitmap.getHeight(),
					this.bitmap, 0, 0);

			Font font = getFont();
			int width = font.getBounds(this.title);
			if (width > getPreferredWidth()) {
				int height = font.getHeight();
				int niuh = (getPreferredWidth() - 5) * height / width;
				Font niuF = font.derive(Font.PLAIN, niuh);
				graphics.setFont(niuF);
				if (Display.getWidth() == 480) {
					graphics.drawText(
							this.title,
							(getPreferredWidth() - niuF.getBounds(this.title)) / 2,
							bitmap.getWidth() + 45);

				} else {
					graphics.drawText(
							this.title,
							(getPreferredWidth() - niuF.getAdvance(this.title)) / 2,
							(getPreferredHeight() - niuF.getHeight()));

				}

			} else {

				if (Display.getWidth() == 480) {
					graphics.drawText(
							this.title,
							(getPreferredWidth() - getFont().getBounds(
									this.title)) / 2, bitmap.getWidth() + 45);

				} else {
					graphics.drawText(
							this.title,
							(getPreferredWidth() - getFont().getAdvance(
									this.title)) / 2,
							(getPreferredHeight() - getFont().getHeight()));

				}
			}

		} else {

			if (MainClass.IDPROVEEDOR.equals("13")) {

				graphics.drawText(this.title, 18,
						(getPreferredHeight() - getFont().getHeight()) / 2);
			} else {
				graphics.drawText("$ " + this.title, 18,
						(getPreferredHeight() - getFont().getHeight()) / 2);
			}
		}

		invalidate();
		super.paint(graphics);
	}

	protected void drawFocus(Graphics graphics, boolean on) {

		if ((this.productBeans != null) || (this.productBeans == null && !flag)) {

			graphics.drawBitmap(
					getPreferredWidth() / 2 - bitmap.getWidth() / 2,
					getPreferredHeight() / 2 - bitmap.getHeight() / 2,
					this.bitmap.getWidth(), this.bitmap.getHeight(),
					this.bitmap, 0, 0);
			int x = this.bitmap.getWidth() + 15;
			graphics.drawText(this.title, x, (getPreferredHeight() - getFont()
					.getHeight()));

		} else {

			graphics.drawText(this.title, 5, (getPreferredHeight() - getFont()
					.getHeight()) / 2);

		}

		//invalidate();
		super.drawFocus(graphics, on);
	}

	protected void layout(int width, int height) {

		this.setExtent(getPreferredWidth(), getPreferredHeight());
	}

	public int getPreferredHeight() {

		return pfHeight;
	}

	public void setPreferredHeight(int pfh) {
		pfHeight = pfh;
	}

	public int getPreferredWidth() {
		return pfWidth;
	}

	public void setPreferredWidth(int pfw) {
		pfWidth = pfw;
	}

	protected boolean navigationClick(int status, int time) {

		if ((this.generalBean != null)
				&& (this.generalBean.getClave().equals("12"))) {

			UiApplication.getUiApplication().pushScreen(
					new ViewETNBuy(generalBean));

		} else {

			System.out.println("Accion");
			if (this.productBeans != null) {

				MainClass.textTitle = this.productBeans.getDescription();
				MainClass.splashScreen.start();
				new ProviderThread(this.productBeans.getClave()).start();

				/*
				 * if ("6".equals(this.productBeans.getClave())) {
				 * 
				 * MainClass.textTitleConfirm =
				 * this.productBeans.getDescription(); Dialog.alert(
				 * "Lo sentimos, este producto no es compatible con su versi�n de Sistema Operativo "
				 * ); } else { MainClass.textTitle =
				 * this.productBeans.getDescription();
				 * MainClass.splashScreen.start(); new
				 * ProviderThread(this.productBeans.getClave()).start(); }
				 */

			} else if (this.productBeans == null && !flag) {

				System.out.println("accion2");

				MainClass.bitmap = this.bitmap;
				MainClass.textTitleInternal = MainClass.textTitle + " \n"
						+ this.generalBean.getDescription();

				MainClass.splashScreen.start();

				if (this.generalBean.getamex().equals("1")) {
					if (this.generalBean.getClave().equals("11")) {
						MainClass.splashScreen.remove();
						UiApplication.getUiApplication().pushScreen(
								new InterjetScreen());
					} else {
						new CatProductsThread(this.generalBean.getClaveWS())
								.start();

					}
				} else if (this.generalBean.getamex().equals("0")) {
					MainClass.splashScreen.remove();
					if (this.generalBean.getvisa().equals("3")) {
						Dialog.alert("Lo sentimos, este producto no est� disponible en este momento para tarjetas AMEX ");
					}
					if (this.generalBean.getvisa().equals("2")) {
						Dialog.alert("Lo sentimos, este producto no est� disponible en este momento para tarjetas MasterCard ");
					}
					if (this.generalBean.getvisa().equals("1")) {
						Dialog.alert("Lo sentimos, este producto no est� disponible en este momento para tarjetas Visa ");
					}
				}

				MainClass.IDPROVEEDOR = this.generalBean.getClave();

			} else {

				System.out.println("accion3");

				MainClass.textTitleConfirm = MainClass.textTitleInternal
						+ "  \nMonto: $" + this.generalBean.getDescription();

				System.out.println("id proveeeeedooooooor "
						+ MainClass.IDPROVEEDOR);
				if (MainClass.IDPROVEEDOR.equals("5")) {
					MainClass.splashScreen.start();
					String json = Crypto.aesEncrypt(MainClass.key,
							MainClass.IDPROVEEDOR);
					new ComisionThread(json, 1, this.generalBean).start();
					System.out.println("SOY IAVE");

				} else if (MainClass.IDPROVEEDOR.equals("6")) {
					MainClass.splashScreen.start();
					String json2 = Crypto.aesEncrypt(MainClass.key,
							MainClass.IDPROVEEDOR);
					new ComisionThread(json2, 2, this.generalBean).start();

					System.out.println("SOY ohl");
				} else if (MainClass.IDPROVEEDOR.equals("7")) {
					MainClass.splashScreen.start();
					String json3 = Crypto.aesEncrypt(MainClass.key,
							MainClass.IDPROVEEDOR);
					String newUrl = Utils.replaceURL(json3, "+", "%2B");
					new ComisionThread(newUrl, 3, this.generalBean).start();

				} else if (MainClass.IDPROVEEDOR.equals("8")) {
					MainClass.splashScreen.start();
					String json2 = Crypto.aesEncrypt(MainClass.key,
							MainClass.IDPROVEEDOR);
					String newUrl = Utils.replaceURL(json2, "+", "%2B");
					new ComisionThread(newUrl, 4, this.generalBean).start();

				} else if (MainClass.IDPROVEEDOR.equals("3")) {
					MainClass.splashScreen.start();
					String json2 = Crypto.aesEncrypt(MainClass.key,
							MainClass.IDPROVEEDOR);

					new ComisionThread(json2, 5, this.generalBean).start();

				} else if (MainClass.IDPROVEEDOR.equals("10")) {

					UiApplication.getUiApplication().pushScreen(
							new PlacasScreen());
				} else if (MainClass.IDPROVEEDOR.equals("9")
						&& this.generalBean.getClaveE().equals("16")) {

					MainClass.tipoVita = 1;
					MainClass.IDPRODUCTO = this.generalBean.getClaveE();
					MainClass.splashScreen.start();
					MainClass.userJSON = Crypto.aesEncrypt(
							Utils.parsePass(MainClass.p), "{\"login\":\""
									+ MainClass.userBean.getLogin()
									+ "\",\"password\":" + "\"" + MainClass.p
									+ "\"}");
					String n2 = Utils.mergeStr(MainClass.userJSON, MainClass.p);
					new VitamedicaThread(n2).start();

				} else if (MainClass.IDPROVEEDOR.equals("9")
						&& this.generalBean.getClaveE().equals("17")) {

					MainClass.tipoVita = 1;
					MainClass.IDPRODUCTO = this.generalBean.getClaveE();
					MainClass.splashScreen.start();
					MainClass.userJSON = Crypto.aesEncrypt(
							Utils.parsePass(MainClass.p), "{\"login\":\""
									+ MainClass.userBean.getLogin()
									+ "\",\"password\":" + "\"" + MainClass.p
									+ "\"}");
					String n2 = Utils.mergeStr(MainClass.userJSON, MainClass.p);
					new VitamedicaThread(n2).start();

				} else if (MainClass.IDPROVEEDOR.equals("9")
						&& this.generalBean.getClaveE().equals("18")) {

					MainClass.tipoVita = 2;
					MainClass.IDPRODUCTO = this.generalBean.getClaveE();
					MainClass.splashScreen.start();
					MainClass.userJSON = Crypto.aesEncrypt(
							Utils.parsePass(MainClass.p), "{\"login\":\""
									+ MainClass.userBean.getLogin()
									+ "\",\"password\":" + "\"" + MainClass.p
									+ "\"}");
					String n2 = Utils.mergeStr(MainClass.userJSON, MainClass.p);
					new VitamedicaThread(n2).start();

				} else if (MainClass.IDPROVEEDOR.equals("9")
						&& this.generalBean.getClaveE().equals("19")) {

					MainClass.tipoVita = 2;
					MainClass.IDPRODUCTO = this.generalBean.getClaveE();
					MainClass.splashScreen.start();
					MainClass.userJSON = Crypto.aesEncrypt(
							Utils.parsePass(MainClass.p), "{\"login\":\""
									+ MainClass.userBean.getLogin()
									+ "\",\"password\":" + "\"" + MainClass.p
									+ "\"}");
					String n2 = Utils.mergeStr(MainClass.userJSON, MainClass.p);
					new VitamedicaThread(n2).start();
				} else if (MainClass.IDPROVEEDOR.equals("11")) {

					System.out.println("SOY INTERJET");
					String json2 = Crypto.aesEncrypt(MainClass.key,
							MainClass.IDPROVEEDOR);
					UiApplication.getUiApplication().pushScreen(
							new InterjetScreen());

				} else if (MainClass.IDPROVEEDOR.equals("13")) {

					UiApplication.getUiApplication().pushScreen(
							new CobroPlacaEdoMExico());

				} else {

					UiApplication.getUiApplication().pushScreen(
							new ConfirmProductScreen(this.generalBean));
				}
			}
		}

		return true;
	}

	public Bitmap getBitmap() {
		return bitmap;
	}

	public void setBitmap(Bitmap bmp) {

		float nh, nw;
		int padding = 0;
		if (Display.getWidth() == 480) {
			padding = 100;
		} else if (Display.getWidth() == 360) {
			padding = 43;

		} else {

			padding = 35;
		}

		if (bmp.getWidth() > bmp.getHeight()) {
			float prop = (float) bmp.getHeight() / (float) bmp.getWidth();
			nh = (getPreferredWidth() - padding);
			nw = prop * (getPreferredWidth() - padding);
		} else {
			float prop = (float) bmp.getWidth() / (float) bmp.getHeight();
			nh = prop * (getPreferredHeight() - padding);
			nw = getPreferredHeight() - padding;
		}

		this.bitmap = bmp;
	}

	public ProductBean getProductBeans() {
		return productBeans;
	}

	public void setProductBeans(ProductBean productBeans) {
		this.productBeans = productBeans;
	}

	public GeneralBean getGeneralBean() {
		return generalBean;
	}

	public void setGeneralBean(GeneralBean generalBean) {
		this.generalBean = generalBean;
	}
}

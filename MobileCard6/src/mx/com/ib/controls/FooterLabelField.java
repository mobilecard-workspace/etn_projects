package mx.com.ib.controls;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.LabelField;

public class FooterLabelField extends LabelField{
	
	public Bitmap rigthBitmap = null;
	public Bitmap leftBitmap = null;
	public Bitmap fillBitmap = null;
	
	public FooterLabelField(){
		super("", Field.NON_FOCUSABLE);
		
		initVariables();
		
	}
	
	public void initVariables(){
		
		rigthBitmap = Bitmap.getBitmapResource("footerImagen.png");
		leftBitmap = Bitmap.getBitmapResource("footerTexto.png");
		fillBitmap = Bitmap.getBitmapResource("footerBarra.png");
		
	}
	
	protected void paint(Graphics graphics) {
		// TODO Auto-generated method stub
		for(int i=0; i<Display.getWidth(); i= i+fillBitmap.getWidth())
		{
			graphics.drawBitmap(i, 0, fillBitmap.getWidth(), fillBitmap.getHeight(), fillBitmap, 0, 0);
		}
		
		graphics.drawBitmap(0, 0, leftBitmap.getWidth(), leftBitmap.getHeight(), leftBitmap, 0, 0);
		
		graphics.drawBitmap(Display.getWidth() - rigthBitmap.getWidth(), 0, rigthBitmap.getWidth(), rigthBitmap.getHeight(), rigthBitmap, 0, 0);
		
		
	}
	
	protected void drawFocus(Graphics graphics, boolean on) {
		// TODO Auto-generated method stub
//		super.drawFocus(graphics, on);
	}
	
	protected void layout(int width, int height) {
		// TODO Auto-generated method stub
//		super.layout(Display.getWidth(), leftBitmap.getHeight());
		this.setExtent(Display.getWidth(), leftBitmap.getHeight());
	}
	

}

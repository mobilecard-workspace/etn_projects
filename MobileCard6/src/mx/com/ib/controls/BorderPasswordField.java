package mx.com.ib.controls;

import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.PasswordEditField;

public class BorderPasswordField extends PasswordEditField{
	
	public int width = 0;
	
	public BorderPasswordField(String label, String value, int maxNumChars, int width,long style){
		
		super(label, value, maxNumChars, style);
		
		this.width = width;
		
	}
	
	protected void paintBackground(Graphics graphics) {
		graphics.setColor(Color.WHITE);
		graphics.fillRoundRect(0, 0, getWidth(), getHeight(), 12, 12);
	}	
	
    public void paint(Graphics graphics) {
        // Override paint to set stipple round Field
        super.paint(graphics);
        graphics.setStipple(0xFFFFFFFF);
        graphics.setColor(0X00000000); // Black
//        graphics.setColor(Color.RED); // Black
        int startX = this.getFont().getAdvance(this.getLabel());
//        if(Display.getWidth()>320){
        	graphics.drawRect(startX,0, width - startX, this.getHeight());
//        }else{
//        	graphics.drawRect(startX,0, 150 - startX, this.getHeight());
//        }
        	graphics.setColor(Color.BLACK);
        	super.paint(graphics);
//        	graphics.drawText(text, x, y)
//        	invalidate();
    }
    
    protected void drawFocus(Graphics graphics, boolean on) {
    	// TODO Auto-generated method stub
        super.paint(graphics);
        graphics.setStipple(0xFFFFFFFF);
        graphics.setColor(0X00000000); // Black
//        graphics.setColor(Color.RED); // Black
        int startX = this.getFont().getAdvance(this.getLabel());
//        if(Display.getWidth()>320){
        	graphics.drawRect(startX,0, width - startX, this.getHeight());
//        }else{
//        	graphics.drawRect(startX,0, 150 - startX, this.getHeight());
//        }
        	graphics.setColor(Color.BLACK);
//        	invalidate();
    	super.drawFocus(graphics, on);
    }
    
    protected boolean keyDown(int keycode, int time) {
    	// TODO Auto-generated method stub
    	
    	return super.keyDown(keycode, time);
    }

}

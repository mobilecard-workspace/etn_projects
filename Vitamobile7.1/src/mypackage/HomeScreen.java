package mypackage;



import net.rim.blackberry.api.browser.Browser;
import net.rim.blackberry.api.browser.BrowserSession;
import net.rim.device.api.system.ApplicationManager;
import net.rim.device.api.system.ApplicationManagerException;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import threads.EstadosThread;
import threads.MembresiaThread;
import Controls.BgManager;
import Controls.CustomVerticalFieldManager;
import Controls.GenericImageButtonField;
import app.MainClass;

public class HomeScreen extends MainScreen{
	
	public LabelField Message = null;
	
	public VerticalFieldManager hfmMain = null;
	public HorizontalFieldManager hfmTitle = null;
	
	public HorizontalFieldManager hfmBtns1 = null;
	public HorizontalFieldManager hfmBtns2 = null;
	public HorizontalFieldManager hfmBtns3 = null;
	public HorizontalFieldManager hfmBtns4 = null;
	public VerticalFieldManager  hfmBtns5 = null;
	public HorizontalFieldManager hfmBtns6 = null;
	
	public CustomVerticalFieldManager customVerticalFieldManager = null;
	public CustomVerticalFieldManager customVerticalFieldManager2 = null;
	public GenericImageButtonField btndirectorio = null;
	public GenericImageButtonField btncard = null;
	public GenericImageButtonField btnvita = null;
	public GenericImageButtonField btnporcentaje = null;
	public GenericImageButtonField btnmobile = null;
	public GenericImageButtonField btnshare = null;
	
	public BgManager bgManager = null;
	public Bitmap bField=null;
	public Bitmap bField2=null;
	
	
	public HorizontalFieldManager hfmbtn = null;
	public VerticalFieldManager vfmFooter = null;
	
	public HomeScreen(){
		
		initVariables();
		initButtons();
		addFields();
		
		
	}
	
	public void initVariables(){
	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	    
	    TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	    transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    engine.setAcceptableDirections(Display.DIRECTION_NORTH);
		bgManager = new BgManager(MainClass.BG_MENU);

		hfmMain = new VerticalFieldManager(Field.FIELD_HCENTER|VERTICAL_SCROLL|Field.USE_ALL_WIDTH);
		hfmTitle = new HorizontalFieldManager(Field.FIELD_RIGHT);
		vfmFooter = new VerticalFieldManager(VERTICAL_SCROLL);
		
		customVerticalFieldManager = new CustomVerticalFieldManager(Display.getWidth(),Display.getHeight()/2, HorizontalFieldManager.USE_ALL_WIDTH|Field.FIELD_VCENTER);
		customVerticalFieldManager2 = new CustomVerticalFieldManager(Display.getWidth(),Display.getHeight()/3, HorizontalFieldManager.USE_ALL_WIDTH|Field.FIELD_VCENTER);
		hfmBtns1 = new HorizontalFieldManager(Field.FIELD_HCENTER){
			//define width
           
			
		};
		hfmBtns2 = new HorizontalFieldManager(Field.FIELD_HCENTER);
		hfmBtns3 = new HorizontalFieldManager(HorizontalFieldManager.FIELD_RIGHT){
			
			 public int getPreferredWidth()
	            {
	                return Display.getWidth();
	            }
	      
	          /*  protected void sublayout( int maxWidth, int maxHeight )
	            {
	                super.sublayout(getPreferredWidth(), 
	                                getPreferredHeight());
	                setExtent(getPreferredWidth(), getPreferredHeight());
	            }*/
		};
		hfmBtns4 = new  HorizontalFieldManager(Field.FIELD_RIGHT);
		hfmBtns6 = new  HorizontalFieldManager(Field.FIELD_HCENTER);
		   hfmbtn = new HorizontalFieldManager(Field.FIELD_RIGHT);
		bField =  Bitmap.getBitmapResource(MainClass.HEADER_MENU);
		bField2=Bitmap.getBitmapResource(MainClass.TEXT);
		//bField2 =  Bitmap.getBitmapResource(MainClass.BTN_INVITA);
		hfmBtns5 = new  VerticalFieldManager(Field.FIELD_HCENTER);
		
		
		Message = new LabelField("Membres�a Vitam�dica ofrece asistencia m�dica telef�nica gratuita, precios preferenciales con m�dicos privados de diferentes especialidades y descuentos exclusivos en farmacias y otros servicios m�dicos.",Field.FIELD_HCENTER){			
			public void paint(Graphics graphics){				
				int g = graphics.getColor();
				graphics.setColor(MainClass.BG_GRAY);
				super.paint(graphics);
			}
			protected void onFocus(int direction) {  
		        super.onFocus(direction);
		        invalidate(); 
		     //   
		    }   
		 
			 protected void onUnfocus() {  
		           super.onUnfocus();
		           invalidate();  
		    } 
		};
		Message.setFont(MainClass.appFont1);
		
	}
	
	public void addFields(){	
	//	setBanner(new BannerBitmap(MainClass.HEADER));
		add(new NullField());
		BitmapField b= new BitmapField(bField);
		hfmBtns3.add(b);
		hfmBtns1.add(btndirectorio);
		hfmBtns1.add(btnvita);
		hfmBtns1.add(btnporcentaje);
	
		
		//margen 
	
	/*	btndirectorio.setMargin(Display.getHeight()/6, 0, 0, 0);
		btnvita.setMargin(Display.getHeight()/6, 0, 0, 0);
		btnporcentaje.setMargin(Display.getHeight()/6, 0, 0, 0);
		
		if(Display.getHeight()==480){
			btndirectorio.setMargin(Display.getHeight()/10, 0, 0, 0);
			btnvita.setMargin(Display.getHeight()/10, 0, 0, 0);
			btnporcentaje.setMargin(Display.getHeight()/10, 0, 0, 0);
		}*/
		
		if(Display.getHeight()==480){
			btndirectorio.setMargin(Display.getHeight()/10, 0, 0, 0);
			btnvita.setMargin(Display.getHeight()/10, 0, 0, 0);
			btnporcentaje.setMargin(Display.getHeight()/10, 0, 0, 0);
		}
		hfmMain.add(hfmBtns3);
		/*BitmapField b2= new BitmapField(bField2);
		hfmBtns6.setPadding(Display.getHeight()/30, 0, 0, 0);
		hfmBtns6.add(b2);
		
		hfmMain.add(hfmBtns6);*/
		
	//	hfmMain.add(Message);
		hfmBtns1.setPadding(Display.getHeight()/30, 0, 0, 0);
		hfmMain.add(hfmBtns1);	
		
		
		//parte de abajo
		hfmBtns2.add(btnmobile);
		hfmBtns2.add(btncard);
		hfmBtns2.add(btnshare);
		hfmBtns2.setPadding(Display.getHeight()/30, 0, 0, 0);
		hfmMain.add(hfmBtns2);	

		
		
		
		
		hfmMain.add(new LabelField(""));
		hfmbtn.add(new ButtonField("Cerrar sesi�n"){
			protected boolean navigationClick(int status, int time) {
					
				
				int pregunta=Dialog.ask(Dialog.D_YES_NO, "�Cerrar sesi�n?");
	            
	            if(Dialog.YES==pregunta){
	            	UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
	            }
	            if(Dialog.NO==pregunta){
	            	
	            }
			
				return true;
				}
				
			});
		hfmMain.add(hfmbtn);
		
		bgManager.add(hfmMain);
		
		add(bgManager);
		
	}
	
	public void initButtons(){
		
		btndirectorio = new GenericImageButtonField(MainClass.DIR_OVER,MainClass.DIR,"comprar"){
			
			protected boolean navigationClick(int status, int time) {
				
				/*MapView mapview = new MapView();  
                mapview.setLatitude((int)(19.369361*100000));
                mapview.setLongitude((int )(-99.266622*100000));
                mapview.setZoom(3);
                MapsArguments mapsArgs = new MapsArguments(mapview);
                Invoke.invokeApplication(Invoke.APP_TYPE_MAPS, mapsArgs);*/
				
				if(MainClass.IDCOMRPA!=null&&!MainClass.IDCOMRPA.equals("")){
					
					  new EstadosThread("").start();
				}else{
					UiApplication.getUiApplication().pushScreen(new LoginScreen());
				}
				
				
				
 
				return true;
			}
			
		};
		btncard = new GenericImageButtonField(MainClass.CARD_OVER,MainClass.CARD,"comprar"){
			
			protected boolean navigationClick(int status, int time) {
				/*int directions = net.rim.device.api.system.Display.DIRECTION_EAST;

			net.rim.device.api.ui.Ui.getUiEngineInstance().setAcceptableDirections(directions);*/
			//	MainClass.splashScreen.start();
				/*int direction = Display.DIRECTION_EAST;
				Ui.getUiEngineInstance().setAcceptableDirections(direction);*/
				if(MainClass.IDCOMRPA!=null&&!MainClass.IDCOMRPA.equals("")){
								
					String idMember="folio="+ MainClass.IDCOMRPA;					
					new MembresiaThread(idMember).start();		
				}else{
					UiApplication.getUiApplication().pushScreen(new LoginScreen());
				}
					
				
				
				
			//	UiApplication.getUiApplication().pushScreen(new PreMemberScreen());
				
				// Use code like this before invoking Display.setCurrent() in the MIDlet constructor
				
				return true;
			}
			
		}; 
		System.out.println("*************************************************************"+MainClass.VITA_OVER+MainClass.VITA);
		btnvita = new GenericImageButtonField(MainClass.VITA_OVER,MainClass.VITA,"comprar"){
			
			protected boolean navigationClick(int status, int time) {
				UiApplication.getUiApplication().pushScreen(new queEsVitaScreen());
				return true;
			}
			
		}; 
		btnporcentaje = new GenericImageButtonField(MainClass.PORCENTAJE_OVER,MainClass.PORCENTAJE,"comprar"){
			
			protected boolean navigationClick(int status, int time) {
				UiApplication.getUiApplication().pushScreen(new LoNuevoScreen());
				return true;
			}
			
		}; 
		btnmobile = new GenericImageButtonField(MainClass.MOBILE_OVER,MainClass.MOBILE,"comprar"){
			
			protected boolean navigationClick(int status, int time) {
			
			
				
				BrowserSession browserSession; 
    			browserSession = Browser.getDefaultSession();   			
    			browserSession.displayPage("http://appworld.blackberry.com/webstore/content/129830/?countrycode=MX");	
    			System.exit(0);
				return true;
			}
			
		}; 
		btnshare = new GenericImageButtonField(MainClass.SHARE_OVER,MainClass.SHARE,"comprar"){
			
			protected boolean navigationClick(int status, int time) {
				UiApplication.getUiApplication().pushScreen(new InvitationScreen());
				return true;
			}
			
		}; 
}
	
	
	

	
	protected boolean onSavePrompt() {
		return true;
	}
	
	public void close()
    {
		
	
    }
	
	protected boolean keyDown(int keycode, int time)
	   {

	        if (Keypad.key(keycode) == Characters.ESCAPE) 
	        {
//	        	close();
	        	
	        	
	        	int pregunta=Dialog.ask(Dialog.D_YES_NO, "�Cerrar sesi�n?");
	            
	            if(Dialog.YES==pregunta){
	            	UiApplication.getUiApplication().popScreen(this);
	            }
	            if(Dialog.NO==pregunta){
	            	
	            }
	        	return true;
	        }
	        return false;
	   }

}

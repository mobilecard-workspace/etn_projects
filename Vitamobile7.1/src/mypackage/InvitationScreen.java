package mypackage;

import Controls.BannerBitmap;
import Controls.BgManager;
import Controls.BorderBasicEditField;
import Controls.GenericImageButtonField;
import addcel.core.mx.communication.WSinvoker;
import app.MainClass;





import mx.com.ib.utils.Utils;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.ActiveRichTextField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;

import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class InvitationScreen extends MainScreen{
	public BgManager bgManager = null;
	public VerticalFieldManager hfmMain = null;
	public HorizontalFieldManager hfmbtn = null;
	public LabelField MyName,nameFriend,email= null;
	public BorderBasicEditField basicMyName,basicnameFriend, basicemail=null;
	public int widthManager = 0;
	public GenericImageButtonField btnOk = null;
	public ActiveRichTextField note=null;
	public ActiveRichTextField note2=null;
	private int WAIT_TIME = 500000000;
	public InvitationScreen(){
		initVariables();
		addFields();
	}
	public void initVariables(){
	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	    
	    TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	    transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
	    widthManager = Display.getWidth();		
		//bgManager = new BgManager(MainClass.BG_CUENTA);
		bgManager = new BgManager(MainClass.BG_MENU);
	    MyName = new LabelField("Mi nombre es: ", Field.NON_FOCUSABLE){
	    	  public void paint(Graphics graphics){				
					int g = graphics.getColor();
					graphics.setColor(0x000000);
					super.paint(graphics);
				} 
	    };	
		nameFriend = new LabelField("Nombre de mi amigo: ", Field.NON_FOCUSABLE){
			  public void paint(Graphics graphics){				
					int g = graphics.getColor();
					graphics.setColor(0x000000);
					super.paint(graphics);
				} 
		};
		email = new LabelField("Tel�fono de mi amigo:", Field.NON_FOCUSABLE){
			  public void paint(Graphics graphics){				
					int g = graphics.getColor();
					graphics.setColor(0x000000);
					super.paint(graphics);
				} 
		};	
		basicMyName = new BorderBasicEditField("","", 16, widthManager, Field.FOCUSABLE);
		
		basicnameFriend = new BorderBasicEditField("","", 30, widthManager, Field.FOCUSABLE);
		basicemail = new BorderBasicEditField("","", 10, widthManager, Field.FOCUSABLE|EditField.FILTER_NUMERIC);
		note = new ActiveRichTextField("�Este servicio no tiene nig�n costo para ti!",Field.FOCUSABLE|FIELD_HCENTER){
			  public void paint(Graphics graphics){				
					int g = graphics.getColor();
					graphics.setColor(0x000000);
					super.paint(graphics);
				} 
		};
		note.select(true);
		note2 = new ActiveRichTextField("�Invita a un amigo a descargar esta aplicaci�n!",Field.FOCUSABLE|FIELD_HCENTER){
			  public void paint(Graphics graphics){				
					int g = graphics.getColor();
					graphics.setColor(0x000000);
					super.paint(graphics);
				} 
		};
		note2.select(true);
	    
	    
	    hfmMain = new VerticalFieldManager(Field.FIELD_HCENTER|VERTICAL_SCROLL|Field.USE_ALL_WIDTH);
	    hfmbtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
	    
	    
	    btnOk =  new GenericImageButtonField(MainClass.BTN_INVITAR_OVER,MainClass.BTN_INVITAR,"ok"){
			
			protected boolean navigationClick(int status, int time) {
				// TODO Auto-generated method stub	
				
			/*	if(basicMyName.equals("")){
					Dialog.alert("Ingresa tu nobmre");
				}else if(basicnameFriend.getText().equals("")){
					Dialog.alert("Ingresa nombre de tu amigo");
				}else if(basicemail.getText().equals("")){
					Dialog.alert("Ingresa su email");
				}else if(!isEmail(basicemail.getText())){
					Dialog.alert("mail inv�lido");
				}else{
					
					MakeInvitation();
				}
				*/
				if(basicnameFriend.getText().equals("")){
					Dialog.alert("Ingresa nombre de tu amigo");
				}else if(basicemail.getText().equals("")){
					Dialog.alert("Ingresa el n�mero celular");
				}else if(basicMyName.getText().equals("")){
					Dialog.alert("Ingresa tu nombre");
				}
				else if(!Utils.isNumeric(basicemail.getText().trim())){
					Dialog.alert("N�mero inv�lido");
				}else if(basicemail.getText().trim().length()<10){
					Dialog.alert("El n�mero celular debe ser de 10 d�gitos");
				}
				else{
					
					MakeInvitation();
				}
				
				return true;
			}
			
		};		
	}
	public void addFields(){
	//	setBanner(new BannerBitmap(MainClass.BG_ORANGE, MainClass.HD_INVITA));
		add(new NullField());

		setBanner(new BannerBitmap(MainClass.SUB_HEADER));
	
		hfmMain.add(MyName);
		hfmMain.add(basicMyName);
		hfmMain.add(nameFriend);
		hfmMain.add(basicnameFriend);
		hfmMain.add(email);
		hfmMain.add(basicemail);
		hfmMain.add(new LabelField(""));
		hfmMain.add(note2);
		hfmMain.add(new LabelField(""));
		hfmbtn.add(btnOk);
		hfmMain.add(hfmbtn);
		hfmMain.add(new LabelField(""));
		hfmMain.add(note);
		
		
			bgManager.add(hfmMain);
		add(bgManager);
	}
	public boolean isEmail(String correo){
		boolean escorreo=true;
		if(correo.indexOf("@")==-1){
			escorreo=false;
		}
		if(correo.indexOf(".")==-1){
			escorreo=false;
		}
		
		return escorreo;
		
	}
	public void MakeInvitation(){
		
		MainClass.splashScreen.start();
		setInvitation();
	//	setLecturaN();
		
	}
	public void close() {
		// TODO Auto-generated method stub
		UiApplication.getUiApplication().popScreen(this);
	}
	protected boolean onSavePrompt() 
	{
		return true;
	}
	private String setInvitation() {
		// TODO Auto-generated method stub
		
		
		
		//lanzarError("Verson: "+appDesc.getVersion());
		String paramsGetVersion[] = {"name","friend","telefono"};
	//	String valsGetVersion[] = {"MBB", appDesc.getVersion()};
		String valsGetVersion[] = {basicMyName.getText(),basicnameFriend.getText(),basicemail.getText()};
		WSinvoker wsGetVersion = new WSinvoker("InvitaSMS", paramsGetVersion, valsGetVersion);
		    			
		    			
		try 
		{	// Obtener Secciones
		
			Thread thGetVersion = new Thread(wsGetVersion);
			thGetVersion.start();
			
			
			int wait=0;
			while(!wsGetVersion.isFinished)
			{
				if(wait==WAIT_TIME)
				{
					wsGetVersion.isFinished = true;
					wsGetVersion.estado = false;
				}
				wait += 1;
			}
		}
		catch(Exception ex){
			System.out.println("Error: "+ex.getMessage());
		}
		finally{
			
			
			MainClass.splashScreen.remove();
			if(wsGetVersion.resultado != null && !wsGetVersion.resultado.equals("")){
				
				UiApplication.getUiApplication().pushScreen(new HomeScreen());	
				UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Gracias tu invitaci�n ha sido enviada", Field.NON_FOCUSABLE));
				
			}else{			
				UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Ocurr�o un error intente m�s tarde", Field.NON_FOCUSABLE));
			}
		
			
		}
		return wsGetVersion.resultado;
	}
	
	

	
	
}

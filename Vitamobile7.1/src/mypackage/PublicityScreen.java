package mypackage;

import Controls.BannerBitmap;
import Controls.BgManager;
import app.MainClass;
import mx.com.ib.utils.MyBrowserFieldConfig;
import net.rim.device.api.browser.field2.BrowserField;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class PublicityScreen extends MainScreen {
	public VerticalFieldManager hfmMain = null;
	public BgManager bgManager = null;
	public PublicityScreen(){
		 TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
		    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
		    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
		    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
		    
		    UiEngineInstance engine = Ui.getUiEngineInstance();
		    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
		    
		    TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
		    transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
		    transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
		    transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
		    
		    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
		    engine.setAcceptableDirections(Display.DIRECTION_NORTH);
			bgManager = new BgManager(MainClass.BG_MENU);
			
		hfmMain = new VerticalFieldManager(Field.FIELD_HCENTER|VERTICAL_SCROLL|Field.USE_ALL_WIDTH);
		MyBrowserFieldConfig _config = new MyBrowserFieldConfig();
		BrowserField _bf2 = new BrowserField(_config);

		

		_bf2.requestContent("http://mobilecard.mx:8080/Vitamedica/promo.htm");
		
		
		add(new NullField());
		setBanner(new BannerBitmap(MainClass.SUB_HEADER));
		hfmMain.add(_bf2);
		bgManager.add(hfmMain);
		add(bgManager);
		
	}
	
}

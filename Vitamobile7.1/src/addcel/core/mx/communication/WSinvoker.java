package addcel.core.mx.communication;

import org.ksoap.SoapObject;
import org.ksoap.transport.HttpTransport;

public class WSinvoker implements Runnable{

    private static final String NAMESPACE = "http://tempuri.org/";
    
    private static final String URL = "http://201.161.23.42:15000/vitamobile.asmx";
    //";deviceside=true";
    /*";apn=internet.itelcel.com;"+
    "tunnelauthusername=webgprs;tunnelauthpassword=webgrps2002";*/
    private String SOAP_ACTION = "http://tempuri.org/";


	
	
    public  String resultado;
    private String method_name;
    private String[] params;
    private String[] valores;
    public boolean estado;
    public boolean isFinished;

    public WSinvoker(String method_name, String[] params, String[] valores)
    {
        estado = false;
        isFinished = false;
        this.method_name = method_name;
        this.params = params;
        this.valores = valores;
        resultado = "";
        SOAP_ACTION += method_name;
    }

    public void run()
    {
        try
        {
            SoapObject request = new SoapObject(NAMESPACE, method_name);
            
                for (int x = 0; x < params.length; x++)
                {
                    request.addProperty(params[x], valores[x]);
                    System.out.println("Param: "+params[x]);
            	   		System.out.println("Prop: "+request.getProperty(params[x]));
                }
          
            HttpTransport JavaMEHttpTransport = new HttpTransport();
            JavaMEHttpTransport.setUrl(URL);
            JavaMEHttpTransport.setSoapAction(SOAP_ACTION);
            Object result = JavaMEHttpTransport.call(request);
            request = null;
            System.out.println("WS "+ method_name+" result:");
            System.out.println(result);
            if (result.equals("[RW][-1][/RW]") || result.equals(""))
            {
                isFinished = true;
                estado = false;
            }
            else
            {
                isFinished = true;
                estado = true;
                resultado = result.toString();
            }
        }

        catch(Exception e)
        {
           isFinished = true;
           estado = false;
           System.out.println("WSException "+e.getMessage());
        }
    }

}




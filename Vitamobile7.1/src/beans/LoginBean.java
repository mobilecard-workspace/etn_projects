package beans;

public class LoginBean {
	 String mensaje;
     int id;

     public LoginBean(String mensaje, int id)
     {
         this.mensaje = mensaje;
         this.id = id;

     }
     public LoginBean()
     {
    

     }

     public String getMensaje()
     {
         return mensaje;
     }

     public void setMensajeombre(String msg)
     {
         this.mensaje = msg;
     }

     public int getId()
     {
         return id;
     }

     public void setId(int ID)
     {
         this.id = ID;
     }
}

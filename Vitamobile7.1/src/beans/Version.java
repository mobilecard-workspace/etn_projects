package beans;

public class Version {

	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTipo() {
		return tipo;
	}
	public Version(int id, String tipo, String version, String url) {
		super();
		this.id = id;
		this.tipo = tipo;
		this.version = version;
		this.url = url;
	}
	public Version() {
		// TODO Auto-generated constructor stub
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	private String tipo, version, url;
	
}

package beans;

public class Banner {
String id;
public Banner(String id, String url) {
	super();
	this.id = id;
	this.url = url;
}
public Banner() {

}
String url;
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getUrl() {
	return url;
}
public void setUrl(String url) {
	this.url = url;
}

}

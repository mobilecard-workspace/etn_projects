//VERSION BB6

package threads;

import java.util.Vector;

import mx.com.ib.connection.Communications;
import mx.com.ib.connection.HttpListener;
import mx.com.ib.utils.Utils;
import mypackage.MemberScreen;
import mypackage.MessagePopupScreen;
import mypackage.PreMemberScreen;
import net.rim.device.api.system.Application;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;
import app.MainClass;

public class MembresiaThread extends Thread implements HttpListener{
	
	public String url = MainClass.URL_GET_MEMBER;
	public String post = "";


	
	public MembresiaThread(String json){
	
		  //vfmInternalConsults = new VerticalFieldManager(VERTICAL_SCROLL);
		this.post = json;
		
//		connect();
		
	}
	
	public void connect(){
		
		Application.getApplication();
		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.start();
		}
		
		if(MainClass.idealConnection != null)
		{
			
			if(this.url != null)
			{				
				try{				    	   
					Communications.sendHttpPost(this,this.url,this.post);
					
		       }catch(Throwable t){
		    	   
		       }
			}
		}
		else
		{		
		
			Utils.checkConnectionType();
			this.connect();
		}
		
	}
	
	public void run() {
		// TODO Auto-generated method stub
		this.setPriority(Thread.MAX_PRIORITY);
		connect();
		//super.run();
	}

	public void handleHttpError(int errorCode, String error) {
		// TODO Auto-generated method stub
		
		getMessageError(error);
		
	}

	public boolean isDestroyed() {
		// TODO Auto-generated method stub
		return false;
	}

	public void receiveEstatus(String msg) {
		// TODO Auto-generated method stub
		
	}

	public void receiveHeaders(Vector _headers) {
		// TODO Auto-generated method stub
		
	}

	public void receiveHttpResponse(int appCode, byte[] response) {
		// TODO Auto-generated method stub
		
		String sTemp = null;
		StringBuffer sb = new StringBuffer();		
		
		try {
			
			sb.append(new String(response,0,response.length,"UTF-8"));
			sTemp = new String (sb.toString());
			System.out.println(sTemp);
			UiApplication.getUiApplication();
			synchronized (Application.getEventLock()) {
				MainClass.Membresia=MainClass.jsParser.GET_MEMBER(sTemp);
				MainClass.splashScreen.remove();
				if(	MainClass.Membresia!=null){
					
						 System.gc();
						 if(MainClass.Benes.length==0||MainClass.Benes==null){
							 
							 UiApplication.getUiApplication().pushScreen(new MemberScreen());
						 }else{
							 if(MainClass.Membresia.getCode()=="null"){
								 MainClass.Membresia = MainClass.Benes[0];
								 UiApplication.getUiApplication().pushScreen(new MemberScreen());
							 }
							 else{
								 UiApplication.getUiApplication().pushScreen(new PreMemberScreen());
							 }
						 }
						 
					
						
					
					}
						
					else{
						UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Intente m�s tarde", Field.NON_FOCUSABLE));	
					}
				
			}
		}catch(Exception e){
			//System.out.println(e.toString());
			getMessageError(e.getMessage());
		}

	}
	
	public void getMessageError(String e){

		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.remove();
			UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Intente m�s tarde. ", Field.NON_FOCUSABLE));
		}
		
	}

}
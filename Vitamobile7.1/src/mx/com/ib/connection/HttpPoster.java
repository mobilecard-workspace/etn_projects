/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.com.ib.connection;

import java.io.*; 
import java.util.*;
import javax.microedition.io.*;
import javax.microedition.lcdui.Image;

import app.MainClass;


import mx.com.ib.utils.Utils;

import java.io.IOException;

/**
 *
 * @author Administrador
 */

public class HttpPoster implements Runnable  {
    private ContentConnection connection = null;    
    private volatile boolean aborting = false;
    private Vector listenerQueue = new Vector();
    private boolean post = false;
    private String stringPost = null;
    private Vector URLQueue = new Vector();
    private int intentos = 1;
    private HttpConnection conn = null;
    private int flag = 0;
    
    private String url="";
    private HttpListener listener=null;

    public HttpPoster() {
       Thread thread = new Thread(this);
       thread.start();
    } 


    public synchronized void sendHttpRequest(HttpListener listener,String URL){
        //aborting=false;
        //System.out.println("Entra a sendHttpRequest");
        if (aborting == true){
            aborting = false;
            this.run();
             //System.out.println("aborting : false");
             //System.out.println("Se reinicia el hilo");
            }
             
       
        listenerQueue.addElement(listener);
        stringPost = null;
        post = false;
        URLQueue.addElement(URL);
        notify(); // wake up sending thread
    }
    public synchronized void sendHttpRequest(HttpListener listener,String URL, String _post){
        //aborting=false;
        //System.out.println("Entra a sendHttpPost");
        if (aborting == true){
            aborting = false;
            this.run();
             //System.out.println("aborting : false");
             //System.out.println("Se reinicia el hilo");
            }
         
        stringPost = _post;
        post = true;
        listenerQueue.addElement(listener);
        
        URLQueue.addElement(URL);
        notify(); // wake up sending thread
    }
    
    


    public void run()
    {
        running :
        while (!aborting)
        {
            //System.out.println("HttpPoster Runnig");
            url="";
            synchronized (this)
            {
                while (listenerQueue.size() == 0) {
                    try {
                        wait(); // releases lock

                    } catch (InterruptedException e) {

                    }

                    if (aborting) {
                        break running;
                    }
                }

              listener = (HttpListener)
                       (listenerQueue.elementAt(0));
              url = (String)(URLQueue.elementAt(0));

               listenerQueue.removeElementAt(0);
               URLQueue.removeElementAt(0);
//                
       
            }
            /// cambio
            if(post)
            	doSend(listener,url,stringPost);
            else
                doSend(listener,url);
        }
        
        //System.out.println("Se termina el hilo de conexiones");
    }

    private void doSend(HttpListener listener,String url,String post)
    
    {
    	   int code = 0;
           DataInputStream in = null;
           OutputStream dos;
           String errorStr = null;
           boolean wasError = false;
           ByteArrayOutputStream responseBytes =  null;
           HttpConnection connection = null;
           byte data[] = null;
           byte postByte[] = null;
           Utils.checkConnectionType();
           url = url + ";" + MainClass.idealConnection +";"+ "ConnectionTimeout=30000";
           System.out.println("URL: "+url);
           System.out.println("POST: "+post);
           
           try
           {
        	 
           
               	connection = (HttpConnection) Connector.open(
    					url, 
    					Connector.READ_WRITE,
    					true);
                
                   postByte = new byte[post.length()];
                   postByte = post.getBytes();
                   connection.setRequestMethod(HttpConnection.POST);
                   connection.setRequestProperty("content-length", postByte.length + "");
                   connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                   dos = connection.openOutputStream();
                   dos.write(postByte);
                   dos.flush();
                   dos.close();
                   // poner post
                   in = connection.openDataInputStream();
                   
                   int length = (int) connection.getLength();
                   
               if (length != -1)
               {
                 data = new byte[length];

                 // Read the png into an array
                 in.readFully(data);
               }
               else  // Length not available...
               {
                   responseBytes = new ByteArrayOutputStream();
                   int ch;
                   while ((ch = in.read()) != -1)
                           responseBytes.write(ch);
                 
                   data = responseBytes.toByteArray();             
               }}catch(Exception e)
               {
               	wasError = true;
               	e.printStackTrace();
               	
               }

           finally
           {
                   if (in != null)  
                   {
                           try{
                                   in.close(); in = null; 
                           }catch (IOException e) {
                                   //System.out.println(e);
                           }
                   }
           }

           if (wasError)
           {
                   if(intentos>=1) 
                   {
                           if(!aborting)  //  <-- cristian
                           {
                                   if(listener != null && !listener.isDestroyed())
                                           listener.handleHttpError(code,errorStr);                     

                                   //System.out.println("Error de conexion");
                           }
                   } 
                   else
                   { 
                           System.gc();
                   }       
           }
           else 
           {
                   
                   try{
                   if(listener != null && !listener.isDestroyed())
                   {
                           //System.out.println("Manda imagen a receiveHttpResponse");
                           //System.out.println(listener);

                           listener.receiveHttpResponse(0,data);
                   }
                   }catch(Throwable t){
                           t.printStackTrace();
                   }

                   //System.out.println("Termina receiveHttpResponse");
           }

    }
    
	private void doSend(HttpListener listener, String url) {
		int code = 0;
		DataInputStream in = null;
		String errorStr = null;
		boolean wasError = false;
		ByteArrayOutputStream responseBytes = null;
		// ContentConnection connection = null;
		HttpConnection connection = null;
		byte data[] = null;

		Utils.checkConnectionType();
		url = url + ";" + MainClass.idealConnection + ";"
				+ "ConnectionTimeout=30000";
		//System.out.println(url);

		//System.out.println(url);
		try {
			// connection = (ContentConnection) Connector.open(url);
			connection = (HttpConnection) Connector.open(url,
					Connector.READ_WRITE, true);
			//System.out.println("URL!!!!"+url);
			connection.setRequestMethod(HttpConnection.GET);
			connection.setRequestProperty("User-Agent",
					"Profile/MIDP-1.0 Confirguration/CLDC-1.0");
			connection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");

			getConnectionInformation(connection);

			in = connection.openDataInputStream();

			int length = (int) connection.getLength();

			if (length != -1) {
				data = new byte[length];

				// Read the png into an array
				in.readFully(data);
			} else // Length not available...
			{
				responseBytes = new ByteArrayOutputStream();
				int ch;
				while ((ch = in.read()) != -1)
					responseBytes.write(ch);

				data = responseBytes.toByteArray();
			}
		} catch (Exception e) {
			wasError = true;
			e.printStackTrace();
            //System.out.println("========================");
            //System.out.println("HTTPOSTER: "+e);
            //System.out.println("========================");
		}
		/*
		 * }catch (Throwable t){ wasError = true; t.printStackTrace(); }
		 */
		finally {
			if (in != null) {
				try {
					in.close();
					in = null;
					if (connection != null)
						connection.close();
				} catch (IOException e) {
					//System.out.println(e);
		            //System.out.println("========================");
		            //System.out.println("ERROR FINALLY: "+e);
		            //System.out.println("========================");
				}
			}
		}

		if (wasError) {
			if (intentos >= 1) {
				if (!aborting) 
				{
					if (listener != null && !listener.isDestroyed())
						listener.handleHttpError(code, errorStr);

					//System.out.println("Error de conexion");
				}
			} else {
				System.gc();
			}
		} else {

			try {
				if (listener != null && !listener.isDestroyed()) {
					//System.out.println("Manda imagen a receiveHttpResponse");
					//System.out.println(listener);

					listener.receiveHttpResponse(0, data);
				}
			} catch (Throwable t) {
				t.printStackTrace();
			}

			//System.out.println("Termina receiveHttpResponse");
		}

	}
    
    void getConnectionInformation(HttpConnection hc) {

    	//System.out.println("Request Method for this connection is " + hc.getRequestMethod());
    	//System.out.println("URL in this connection is " + hc.getURL());
    	//System.out.println("Protocol for this connection is " + hc.getProtocol()); // It better be HTTP:)
    	//System.out.println("This object is connected to " + hc.getHost() + " host");
    	//System.out.println("HTTP Port in use is " + hc.getPort());
    	//System.out.println("Query parameter in this request are " + hc.getQuery());

    	}
   
   // The instance is useless after abort has been called.
    public void abort()
    {
        //System.out.println("Entra a Abort de HttpPoster.java");   
        //aborting = true;
        listenerQueue.removeAllElements();
        URLQueue.removeAllElements();
        synchronized (this)
        {  
            //stop();
            notify(); // wake up our posting thread and kill it
        }
        
    }

    public boolean isAborting()
    {
        return aborting;
    }


    public int getIntentos()
    {
        return intentos;
    }

    public void setIntentos(int value)
    {
        intentos = value;
    }
    
    public void SetFlag(int f)
    {
        flag =f;
        
     }
    public int GetFlag()
    {
        return flag;
        }

    public void closeConnection()
    {
       try {  if (conn != null)
              {  try { conn.close(); }
                 catch (Exception e) { }
              }

       } catch (Exception e) {

       }

    }
}

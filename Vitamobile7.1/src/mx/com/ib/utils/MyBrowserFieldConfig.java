package mx.com.ib.utils;

import net.rim.device.api.browser.field2.BrowserFieldConfig;
import net.rim.device.api.system.ApplicationDescriptor;
import net.rim.device.api.system.Display;

public class MyBrowserFieldConfig extends BrowserFieldConfig 
{
    public MyBrowserFieldConfig()
    {
        ApplicationDescriptor desc = ApplicationDescriptor.currentApplicationDescriptor();
	if (desc != null)
    	{
    		String name = desc.getName();
    		String version = desc.getVersion();

    		//Default = BlackBerry Browser's User Agent
		setProperty(BrowserFieldConfig.USER_AGENT, name + " " + version);	
    	}


        //The following settings can be configured:


        //Default = FALSE
	setProperty(BrowserFieldConfig.ALLOW_CS_XHR, Boolean.TRUE);

        //Default = FALSE
	setProperty(BrowserFieldConfig.ENABLE_GEARS, Boolean.TRUE);

        //Default = NAVIGATION_MODE_NONE
	setProperty(BrowserFieldConfig.NAVIGATION_MODE, BrowserFieldConfig.NAVIGATION_MODE_POINTER);

        //Default = 5000 milliseconds
	setProperty(BrowserFieldConfig.JAVASCRIPT_TIMEOUT, new Integer(1000));

        //Default = Set by rendering engine
      //  setProperty(BrowserFieldConfig.HTTP_HEADERS, "Content-Type=text/html; CustomHeader=12345");    

        //Default = TRUE
        setProperty(BrowserFieldConfig.JAVASCRIPT_ENABLED, Boolean.TRUE);    

        //Default = TRUE
        setProperty(BrowserFieldConfig.MDS_TRANSCODING_ENABLED, Boolean.FALSE);    
        //Default = TRUE:
   //     setProperty(BrowserFieldConfig.USER_SCALABLE, Boolean.FALSE);    

        //Default = None
     //   setProperty(BrowserFieldConfig.VIEWPORT_WIDTH, new Integer(Display.getWidth() / 2 ));
        
        //Default = None
       // setProperty(BrowserFieldConfig.INITIAL_SCALE, new Float(1.5));

    }
}
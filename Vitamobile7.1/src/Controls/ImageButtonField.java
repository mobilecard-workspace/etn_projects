package Controls;


import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.MainScreen;

/**
 * Custom button field that shows how to use images as button backgrounds.
 */
public class ImageButtonField extends Field {
	private String _label;
	private Font _font;

	private Bitmap _currentPicture;
	private Bitmap _onPicture;
	private Bitmap _offPicture;
	boolean no = true;
	String id = null;
	private boolean selected = false;

	/**
	 * Constructor.
	 * 
	 * @param text
	 *            - the text to be displayed on the button
	 * @param style
	 *            - combination of field style bits to specify display
	 *            attributes
	 */
	public ImageButtonField(String text, String _onImg, String _offImg,
			long focusable) {
		super(focusable);

		_label = text;

		_onPicture = Bitmap.getBitmapResource(_onImg);
		_offPicture = Bitmap.getBitmapResource(_offImg);

		_currentPicture = _offPicture;
	}

	public ImageButtonField(String text, String _onImg, String _offImg,String _id) {
		super(Field.FOCUSABLE);

		_label = text;

		_onPicture = Bitmap.getBitmapResource(_onImg);
		_offPicture = Bitmap.getBitmapResource(_offImg);

		_currentPicture = _offPicture;
		id = _id;
		
	}
	public ImageButtonField(String text, String _onImg, String _offImg) {
		super(Field.FOCUSABLE);

		_label = text;

		_onPicture = Bitmap.getBitmapResource(_onImg);
		_offPicture = Bitmap.getBitmapResource(_offImg);

		_currentPicture = _offPicture;

		
	}
	public ImageButtonField(String text, String _onImg, String _offImg, boolean _no) {
		super(Field.FOCUSABLE);

		_label = text;

		_onPicture = Bitmap.getBitmapResource(_onImg);
		_offPicture = Bitmap.getBitmapResource(_offImg);

		_currentPicture = _offPicture;
		
		no = _no;
	}

	/**
	 * Field implementation.
	 * 
	 * @see net.rim.device.api.ui.Field#getPreferredHeight()
	 */
	public int getPreferredHeight() {
		// return _labelHeight + 4;
		return this._onPicture.getHeight();
	}

	/**
	 * Field implementation.
	 * 
	 * @see net.rim.device.api.ui.Field#getPreferredWidth()
	 */
	public int getPreferredWidth() {
		// return _labelWidth + 8;
		return this._onPicture.getWidth();
	}

	/**
	 * Field implementation. Changes the picture when focus is gained.
	 * 
	 * @see net.rim.device.api.ui.Field#onFocus(int)
	 */
	protected void onFocus(int direction) {

		// super.onUnfocus();

		// invalidate();
	}

	/**
	 * Field implementation. Changes picture back when focus is lost.
	 * 
	 * @see net.rim.device.api.ui.Field#onUnfocus()
	 */
	protected void onUnfocus() {
		this.selected = false;
		_currentPicture = _offPicture;
//		MainClass._instance.repaint();
		

		// super.onUnfocus();
		// MainClass._instance.repaint();
		 invalidate();
	}

	/**
	 * Field implementation.
	 * 
	 * @see net.rim.device.api.ui.Field#drawFocus(Graphics, boolean)
	 */
	protected void drawFocus(Graphics graphics, boolean on) {
		this.selected = true;
		_currentPicture = _onPicture;
		graphics.drawBitmap(0, 0, getWidth(), getHeight(), _currentPicture, 0,
				0);
		invalidate();
		// Then draw the text
//		if (this._label.length() != 0) {
//			graphics.setColor(Color.WHITE);
//			graphics.setFont(_font);
//			graphics.drawText(_label, 4, 2, (int) (getStyle()
//					& DrawStyle.ELLIPSIS | DrawStyle.HALIGN_MASK),
//					getWidth() - 6);
//		}
		// MainClass._instance.repaint(); // Do nothing
	}

	protected void pintaFocus(Graphics graphics) {
		graphics.drawBitmap(0, 0, getWidth(), getHeight(), _currentPicture, 0,
				0);
		invalidate();
		// Then draw the text
//		if (this._label.length() != 0) {
//			graphics.setColor(Color.WHITE);
//			graphics.setFont(_font);
//			graphics.drawText(_label, 4, 2, (int) (getStyle()
//					& DrawStyle.ELLIPSIS | DrawStyle.HALIGN_MASK),
//					getWidth() - 6);
//		}
	}

	/**
	 * Field implementation.
	 * 
	 * @see net.rim.device.api.ui.Field#layout(int, int)
	 */
	protected void layout(int width, int height) {
		setExtent(Math.min(width, getPreferredWidth()), Math.min(height,
				getPreferredHeight()));
	}

	/**
	 * Field implementation.
	 * 
	 * @see net.rim.device.api.ui.Field#paint(Graphics)
	 */
	protected void paint(Graphics graphics) {
		// First draw the background colour and picture
		if (!this.selected) {
			graphics.drawBitmap(0, 0, getWidth(), getHeight(), _currentPicture,
					0, 0);

			// Then draw the text
//			if (this._label.length() != 0) {
//				graphics.setColor(Color.WHITE);
//				graphics.setFont(_font);
//				graphics.drawText(_label, 4, 2, (int) (getStyle()
//						& DrawStyle.ELLIPSIS | DrawStyle.HALIGN_MASK),
//						getWidth() - 6);
//			}
		} else {
			pintaFocus(graphics);
		}
		invalidate();
	}

	protected void paintBackground(Graphics g) {
		// //System.out.println("ImageButtonField - paintBackGrounf : entra");
		// g.setColor(Color.BLUEVIOLET);
		// g.fillRect(0,0, this.getWidth(), this.getHeight());

	}

	/**
	 * Overridden so that the Event Dispatch thread can catch this event instead
	 * of having it be caught here..
	 * 
	 * @see net.rim.device.api.ui.Field#navigationClick(int, int)
	 */
	protected boolean navigationClick(int status, int time) {
		if(no && id!=null)
		{
//		MainClass.fondoActual = "fondo_320_240.jpg";
//		MainClass.urlNoticias = "http://desarrollo.elfinanciero.com.mx/moviles/noticias.php?idsec=5&idcat="+id;
//		//System.out.println("URL: "+ MainClass.urlNoticias);
//		UiApplication.getUiApplication().pushScreen(new ScreenNoticias());
		}else
		
		if(_label.equals("minuto"))
		{
//			MainClass.splash.start();
//			UiApplication.getUiApplication().pushScreen(new ScreenMinuto());
		}
		
		fieldChangeNotify(1);
		
		return true;
	}

	public boolean isSeleccionado() {
		return selected;
	}
}
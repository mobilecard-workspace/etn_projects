package mypackage;

import threads.BusquedaThread;
import threads.LoginThread;
import threads.ProveedorThread;
import Controls.BannerBitmap;
import Controls.BgManager;
import Controls.BorderBasicEditField;
import Controls.GenericImageButtonField;
import app.MainClass;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.component.RadioButtonField;
import net.rim.device.api.ui.component.RadioButtonGroup;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.text.TextFilter;

public class LoginScreen extends MainScreen {
	public BgManager bgManager = null;
	public VerticalFieldManager hfmMain = null;
	public HorizontalFieldManager hfmbtn = null;
	public LabelField titulo,user= null;
	public BorderBasicEditField txtUser;
	public GenericImageButtonField btnOk = null;
	public RadioButtonGroup idSelectionRadio = null;
	
	public LoginScreen(){
		TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	    
	    TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	    transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    engine.setAcceptableDirections(Display.DIRECTION_NORTH);
	    bgManager = new BgManager(MainClass.BG_SPLASH);
	    System.out.println(MainClass.BG_MENU);
	    hfmMain = new VerticalFieldManager(Field.FIELD_HCENTER|VERTICAL_SCROLL);
	    hfmbtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
	    
	    
	    titulo= new LabelField("Acceso", Field.FIELD_HCENTER){
			public void paint(Graphics g){
				
				
				g.setColor(MainClass.BG_GRAY);
				super.paint(g);
			}
		};
		
		txtUser = new BorderBasicEditField("","", 16, Display.getWidth(), Field.FOCUSABLE);
		txtUser.setFilter(TextFilter.get(TextFilter.INTEGER));  
		user = new LabelField("Ingresa tu clave de acceso:");
		
		 btnOk =  new GenericImageButtonField(MainClass.BTN_INVITAR_OVER,MainClass.BTN_INVITAR,"ok");
		 
		
		setBanner(new BannerBitmap(MainClass.SUB_HEADER));
		add(new NullField());
		hfmMain.add(titulo);
		hfmMain.add(user);
		hfmMain.add(txtUser);
		
		RadioButtonField idRadio = new RadioButtonField("Id de compra");
		RadioButtonField credencialRadio = new RadioButtonField("N�mero de membres�a");
		
		idSelectionRadio = new RadioButtonGroup();
		
		idSelectionRadio.add(idRadio);
		idSelectionRadio.add(credencialRadio);
		hfmMain.add(hfmbtn);
		
		hfmMain.add(idRadio);
		hfmMain.add(credencialRadio);
		
		
		hfmbtn.add(new ButtonField("Iniciar Sesi�n"){
			protected boolean navigationClick(int status, int time) {
				if(txtUser.getText().length()!=0){
		    		  new LoginThread("vitaU="+txtUser.getText().trim() + 
					   		   "&tipoId="+idSelectionRadio.getSelectedIndex()).start();
		    		  MainClass.IDCOMRPA=txtUser.getText().trim();
				}else{
					Dialog.alert("La clave de compra no puede ir vac�a");
				}
				return true;
			}
			protected boolean touchEvent (TouchEvent message) {
				   switch(message.getEvent()) {
				      case TouchEvent.CLICK:
				    	  
				    	if(txtUser.getText().length()!=0){
				    		  new LoginThread("vitaU="+txtUser.getText().trim() + 
							   		   "&tipoId="+idSelectionRadio.getSelectedIndex()).start();
				    		  MainClass.IDCOMRPA=txtUser.getText().trim();
						}else{
							Dialog.alert("La clave de compra no puede ir vac�a");
						}
				    
				         return true;
				   }
				   return false; 
				}
		});
		
		bgManager.add(hfmMain);
		add(bgManager);
	}
	protected boolean onSavePrompt() {
		return true;
	}
}

package mypackage;

import threads.EspecialidadThread;
import threads.ProveedorThread;
import Controls.BannerBitmap;
import Controls.BgManager;
import Controls.BorderBasicEditField;
import Controls.GenericImageButtonField;
import addcel.core.mx.communication.WSinvoker;
import app.MainClass;





import mx.com.ib.utils.Utils;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.ActiveRichTextField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.component.SeparatorField;

import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class BusquedaScreen extends MainScreen{
	public BgManager bgManager = null;
	public VerticalFieldManager hfmMain,hfmbtn2 = null;
	public HorizontalFieldManager hfmbtn = null;
	public LabelField MyName,nameFriend,email,nombre,nombre2= null;
	public BorderBasicEditField basicMyName,basicnameFriend, basicemail=null;
	public int widthManager = 0;
	public GenericImageButtonField btnOk = null;
	public ActiveRichTextField note=null;
	public ActiveRichTextField note2=null;
	private int x=0;
	private int WAIT_TIME = 500000000;
	public BusquedaScreen(){
		initVariables();
		addFields();
	}
	public void initVariables(){
	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	    
	    TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	    transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
	    widthManager = Display.getWidth();		
		//bgManager = new BgManager(MainClass.BG_CUENTA);
		bgManager = new BgManager(MainClass.BG_MENU);
	    MyName = new LabelField("Resultados ", Field.NON_FOCUSABLE);	
		
	    
	    
	    hfmMain = new VerticalFieldManager(Field.FIELD_HCENTER|VERTICAL_SCROLL|Field.USE_ALL_WIDTH);
	    hfmbtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
	  
	    
	    
	    btnOk =  new GenericImageButtonField(MainClass.BTN_INVITAR_OVER,MainClass.BTN_INVITAR,"ok"){
			
			protected boolean navigationClick(int status, int time) {
			
				
				return true;
			}
			
		};		
	}
	public void addFields(){
		setBanner(new BannerBitmap(MainClass.SUB_HEADER));
		add(new NullField());
		
		hfmbtn.add(MyName);
		MyName.setFont(Font.getDefault().derive(Font.BOLD, Font.getDefault().getHeight(), Ui.UNITS_px));
		hfmMain.add(hfmbtn);
		
		hfmMain.add(new SeparatorField());
		
		for( x =0;x<MainClass.busquedas.length;x++){
			//nombre
		    hfmbtn = new HorizontalFieldManager(Field.FIELD_LEFT|VERTICAL_SCROLL|Field.USE_ALL_WIDTH|Field.FOCUSABLE){		    	
		    	public void paint(Graphics graphics){				
					int g = graphics.getColor();
					graphics.setColor(0x000000);
					super.paint(graphics);
				}
				protected void onFocus(int direction) {  
			        super.onFocus(direction);
			        invalidate(); 
			     //   
			    }   			 
				 protected void onUnfocus() {  
			           super.onUnfocus();
			           invalidate();  
			    }
		    };			
			nombre = new LabelField("Nombre: ", Field.FIELD_LEFT){
				public void paint(Graphics graphics){				
					int g = graphics.getColor();
					graphics.setColor(0x850c10);
					super.paint(graphics);
				}
				protected void onFocus(int direction) {  
			        super.onFocus(direction);
			        invalidate(); 
			     //   
			    }   
			 
				 protected void onUnfocus() {  
			           super.onUnfocus();
			           invalidate();  
			    }
	            };
	            nombre.setFont(Font.getDefault().derive(Font.BOLD, Font.getDefault().getHeight()-4, Ui.UNITS_px));
	           // hfmbtn.add(nombre);	            
	            nombre2 = new LabelField(MainClass.busquedas[x].getNombre(), Field.FIELD_LEFT|Field.FOCUSABLE){
					public void paint(Graphics graphics){				
						int g = graphics.getColor();
						graphics.setColor(0x000000);
						super.paint(graphics);
					}
					protected void onFocus(int direction) {  
				        super.onFocus(direction);
				        invalidate(); 
				     //   
				    }   
				 
					 protected void onUnfocus() {  
				           super.onUnfocus();
				           invalidate();  
				    }
		            };
		      		nombre2.setFont(Font.getDefault().derive(Font.BOLD, Font.getDefault().getHeight()-4, Ui.UNITS_px));
		            hfmbtn.add(nombre2);            
	            hfmMain.add(hfmbtn);
			   //especialidad
    	    hfmbtn = new HorizontalFieldManager(Field.FIELD_LEFT|VERTICAL_SCROLL|Field.USE_ALL_WIDTH|Field.FOCUSABLE){		    	
		    	public void paint(Graphics graphics){				
					int g = graphics.getColor();
					graphics.setColor(0x000000);
					super.paint(graphics);
				}
				protected void onFocus(int direction) {  
			        super.onFocus(direction);
			        invalidate(); 
			     //   
			    }   			 
				 protected void onUnfocus() {  
			           super.onUnfocus();
			           invalidate();  
			    }
		    };			
			nombre = new LabelField("Especialidad o Servicio: ", Field.FIELD_LEFT){
				public void paint(Graphics graphics){				
					int g = graphics.getColor();
					graphics.setColor(0x850c10);
					super.paint(graphics);
				}
				protected void onFocus(int direction) {  
			        super.onFocus(direction);
			        invalidate(); 
			     //   
			    }   
			 
				 protected void onUnfocus() {  
			           super.onUnfocus();
			           invalidate();  
			    }
	            };
	            nombre.setFont(Font.getDefault().derive(Font.BOLD, Font.getDefault().getHeight()-4, Ui.UNITS_px));
	          //  hfmbtn.add(nombre);	            
	            nombre2 = new LabelField(MainClass.busquedas[x].getEspecialidades(), Field.FIELD_LEFT){
					public void paint(Graphics graphics){				
						int g = graphics.getColor();
						graphics.setColor(0x000000);
						super.paint(graphics);
					}
					protected void onFocus(int direction) {  
				        super.onFocus(direction);
				        invalidate(); 
				     //   
				    }   
				 
					 protected void onUnfocus() {  
				           super.onUnfocus();
				           invalidate();  
				    }
		            };
		      		nombre2.setFont(Font.getDefault().derive(Font.BOLD, Font.getDefault().getHeight()-4, Ui.UNITS_px));
		            hfmbtn.add(nombre2);            
	            hfmMain.add(hfmbtn);
			
			
			
			
			
	            //direccion
	    	    hfmbtn = new HorizontalFieldManager(Field.FIELD_LEFT|VERTICAL_SCROLL|Field.USE_ALL_WIDTH|Field.FOCUSABLE){		    	
			    	public void paint(Graphics graphics){				
						int g = graphics.getColor();
						graphics.setColor(0x000000);
						super.paint(graphics);
					}
					protected void onFocus(int direction) {  
				        super.onFocus(direction);
				        invalidate(); 
				     //   
				    }   			 
					 protected void onUnfocus() {  
				           super.onUnfocus();
				           invalidate();  
				    }
			    };			
				nombre = new LabelField("Direcci�n: ", Field.FIELD_LEFT){
					public void paint(Graphics graphics){				
						int g = graphics.getColor();
						graphics.setColor(0x850c10);
						super.paint(graphics);
					}
					protected void onFocus(int direction) {  
				        super.onFocus(direction);
				        invalidate(); 
				     //   
				    }   
				 
					 protected void onUnfocus() {  
				           super.onUnfocus();
				           invalidate();  
				    }
		            };
		            nombre.setFont(Font.getDefault().derive(Font.BOLD, Font.getDefault().getHeight()-4, Ui.UNITS_px));
		     //       hfmbtn.add(nombre);	            
		            nombre2 = new LabelField(MainClass.busquedas[x].getUbicacion(), Field.FIELD_LEFT|Field.FOCUSABLE){
						public void paint(Graphics graphics){				
							int g = graphics.getColor();
							graphics.setColor(0x000000);
							super.paint(graphics);
						}
						protected void onFocus(int direction) {  
					        super.onFocus(direction);
					        invalidate(); 
					     //   
					    }   
					 
						 protected void onUnfocus() {  
					           super.onUnfocus();
					           invalidate();  
					    }
			            };
			      		nombre2.setFont(Font.getDefault().derive(Font.BOLD, Font.getDefault().getHeight()-4, Ui.UNITS_px));
			            hfmbtn.add(nombre2);            
		            hfmMain.add(hfmbtn);
		            
		            
		            
		            //precio
		    	    hfmbtn = new HorizontalFieldManager(Field.FIELD_RIGHT|VERTICAL_SCROLL|Field.USE_ALL_WIDTH|Field.FOCUSABLE){		    	
				    	public void paint(Graphics graphics){				
							int g = graphics.getColor();
							graphics.setColor(0x000000);
							super.paint(graphics);
						}
						protected void onFocus(int direction) {  
					        super.onFocus(direction);
					        invalidate(); 
					     //   
					    }   			 
						 protected void onUnfocus() {  
					           super.onUnfocus();
					           invalidate();  
					    }
				    };			
					nombre = new LabelField("Consulta o descuento: ", Field.FIELD_RIGHT){
						public void paint(Graphics graphics){				
							int g = graphics.getColor();
							graphics.setColor(0x850c10);
							super.paint(graphics);
						}
						protected void onFocus(int direction) {  
					        super.onFocus(direction);
					        invalidate(); 
					     //   
					    }   
					 
						 protected void onUnfocus() {  
					           super.onUnfocus();
					           invalidate();  
					    }
			            };
			            nombre.setFont(Font.getDefault().derive(Font.BOLD, Font.getDefault().getHeight()-4, Ui.UNITS_px));
			   //         hfmbtn.add(nombre);	            
			            nombre2 = new LabelField(MainClass.busquedas[x].getPrecio(), Field.FIELD_RIGHT){
							public void paint(Graphics graphics){				
								int g = graphics.getColor();
								graphics.setColor(0x850c10);
								super.paint(graphics);
							}
							protected void onFocus(int direction) {  
						        super.onFocus(direction);
						        invalidate(); 
						     //   
						    }   
						 
							 protected void onUnfocus() {  
						           super.onUnfocus();
						           invalidate();  
						    }
				            };
				      		nombre2.setFont(Font.getDefault().derive(Font.BOLD, Font.getDefault().getHeight()-4, Ui.UNITS_px));
				            hfmbtn.add(nombre2);    
				            hfmMain.add(hfmbtn);
				          final int ss=x;
				            //mas detalle
				        	nombre = new LabelField("Mostrar Detalles... ", Field.FIELD_HCENTER|Field.FOCUSABLE){
								public void paint(Graphics graphics){				
									int g = graphics.getColor();
									graphics.setColor(0x3333FF);
									super.paint(graphics);
								}
								protected void onFocus(int direction) {  
							        super.onFocus(direction);
							        invalidate(); 
							     //   
							    }   
							 
								 protected void onUnfocus() {  
							           super.onUnfocus();
							           invalidate();  
							    }
								 
								 protected boolean navigationClick(int status, int time) {
										// TODO Auto-generated method stub
									 
									 new ProveedorThread("id="+MainClass.busquedas[ss].getID()).start();
										return true;
									}
									
									
									protected boolean touchEvent (TouchEvent message) {
										   switch(message.getEvent()) {
										      case TouchEvent.CLICK:
										    	  
										    	  System.out.println(ss);
										    //	  System.out.println("id="+MainClass.busquedas[x].getID());
										   new ProveedorThread("id="+MainClass.busquedas[ss].getID()).start();

										         return true;
										   }
										   return false; 
										}
								 
					            };
					            nombre.setFont(Font.getDefault().derive(Font.BOLD, Font.getDefault().getHeight()-4, Ui.UNITS_px));
					            hfmbtn = new HorizontalFieldManager(Field.FIELD_HCENTER|VERTICAL_SCROLL|Field.USE_ALL_WIDTH|Field.FOCUSABLE){		    	
							    	public void paint(Graphics graphics){				
										int g = graphics.getColor();
										graphics.setColor(0x000000);
										super.paint(graphics);
									}
									protected void onFocus(int direction) {  
								        super.onFocus(direction);
								        invalidate(); 
								     //   
								    }   			 
									 protected void onUnfocus() {  
								           super.onUnfocus();
								           invalidate();  
								    }
							    };			
					            hfmbtn.add(nombre);
					        
				            
			            hfmMain.add(hfmbtn);
					
	            
	            
	            
			hfmMain.add(new SeparatorField());
		}
		
		
		
			bgManager.add(hfmMain);
		add(bgManager);
	}

	public void close() {
		// TODO Auto-generated method stub
		UiApplication.getUiApplication().popScreen(this);
	}
	protected boolean onSavePrompt() 
	{
		return true;
	}

	
	

	
	
}

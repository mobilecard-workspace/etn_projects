package mypackage;

import app.MainClass;
import Controls.BannerBitmap;
import Controls.BgManager;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class ProveedoresScreen extends MainScreen {
	public BgManager bgManager = null;
	public VerticalFieldManager hfmMain = null;
	
	public ProveedoresScreen(){
		initVariables();
		addFields();
	}

	private void addFields() {
		// TODO Auto-generated method stub
		add(new NullField());
		setBanner(new BannerBitmap(MainClass.SUB_HEADER));
		add(new NullField());
		
		 HorizontalFieldManager hfmbtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		 hfmMain.add(hfmbtn);
		 Bitmap bmp = Bitmap.getBitmapResource("prov_biomedica.jpg");
	     BitmapField bfield = new BitmapField(bmp,Field.FOCUSABLE);    
	     hfmbtn.add(bfield);
	     System.out.println(Display.getWidth()+" -- "+bmp.getWidth());
	     
	     hfmbtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		 hfmMain.add(hfmbtn);
	     bmp = Bitmap.getBitmapResource("prov_chopo.png");
	     bfield = new BitmapField(bmp,Field.FOCUSABLE);    
	     hfmbtn.add(bfield);
	     System.out.println(Display.getWidth()+" -- "+bmp.getWidth());
	     
	     hfmbtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		 hfmMain.add(hfmbtn);
	     bmp = Bitmap.getBitmapResource("prov_devlyn.png");
	     bfield = new BitmapField(bmp,Field.FOCUSABLE);    
	     hfmbtn.add(bfield);
	     System.out.println(Display.getWidth()+" -- "+bmp.getWidth());
	     
	     hfmbtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		 hfmMain.add(hfmbtn);
	     bmp = Bitmap.getBitmapResource("prov_farm_esp.png");
	     bfield = new BitmapField(bmp,Field.FOCUSABLE);    
	     hfmbtn.add(bfield);
	     System.out.println(Display.getWidth()+" -- "+bmp.getWidth());
	     
	     hfmbtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		 hfmMain.add(hfmbtn);
	     bmp = Bitmap.getBitmapResource("prov_farmatodo.png");
	     bfield = new BitmapField(bmp,Field.FOCUSABLE);    
	     hfmbtn.add(bfield);
	     System.out.println(Display.getWidth()+" -- "+bmp.getWidth());
	     
	     hfmbtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		 hfmMain.add(hfmbtn);
	     bmp = Bitmap.getBitmapResource("prov_lapi.jpg");
	     bfield = new BitmapField(bmp,Field.FOCUSABLE);    
	     hfmbtn.add(bfield);
	     System.out.println(Display.getWidth()+" -- "+bmp.getWidth());
	     
	     hfmbtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		 hfmMain.add(hfmbtn);
	     bmp = Bitmap.getBitmapResource("prov_lux.png");
	     bfield = new BitmapField(bmp,Field.FOCUSABLE);    
	     hfmbtn.add(bfield);
	     System.out.println(Display.getWidth()+" -- "+bmp.getWidth());
	     
/*	     hfmbtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		 hfmMain.add(hfmbtn);
	     bmp = Bitmap.getBitmapResource("prov_starmdica.png");
	     bfield = new BitmapField(bmp,Field.FOCUSABLE);    
	     hfmbtn.add(bfield);
	     System.out.println(Display.getWidth()+" -- "+bmp.getWidth());
	     
	     hfmbtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		 hfmMain.add(hfmbtn);
	     bmp = Bitmap.getBitmapResource("prov_vivo_hosp.png");
	     bfield = new BitmapField(bmp,Field.FOCUSABLE);
	     System.out.println(Display.getWidth()+" -- "+bmp.getWidth());
	     
	     hfmbtn.add(bfield);*/
	     
		bgManager.add(hfmMain);
		add(bgManager);
	}

	private void initVariables() {
		// TODO Auto-generated method stub
		TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	    
	    TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	    transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    
	    bgManager = new BgManager(MainClass.BG_MENU);
		   
	    
	    hfmMain = new VerticalFieldManager(Field.FIELD_HCENTER|VERTICAL_SCROLL|Field.USE_ALL_WIDTH);
	    
	}
}

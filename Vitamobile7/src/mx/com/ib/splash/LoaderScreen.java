package mx.com.ib.splash;


import app.MainClass;
import mx.com.ib.connection.Communications;
import net.rim.device.api.system.GIFEncodedImage;
import net.rim.device.api.ui.ContextMenu;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngine;
import net.rim.device.api.ui.component.Menu;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class LoaderScreen extends PopupScreen {

	boolean dosPantallas = false;

	public LoaderScreen() {
		super(new VerticalFieldManager(), Field.FOCUSABLE);

		init();

	}	

	private void init() {
/*
		AnimatedGIFField gif = new AnimatedGIFField(
			(GIFEncodedImage) (GIFEncodedImage.getEncodedImageResource(MainClass.GIF_LOADER))
		);

		add(gif);
*/
		GIFEncodedImage encodedImage = null;

		try{
			encodedImage = (GIFEncodedImage)GIFEncodedImage.getEncodedImageResource(MainClass.GIF_LOADER);
		} catch (Exception exception){
			String e = exception.toString();
			exception.printStackTrace();
		}

		AnimatedGIFField gif = new AnimatedGIFField(encodedImage);

		add(gif);
		
	}
	

	public void remove() {
		if (this.isDisplayed()) {
			UiApplication.getUiApplication().popScreen(this);
		}
	}
	

	public void start() {
		dosPantallas = false;
		synchronized (UiApplication.getEventLock()) {
			if (!this.isDisplayed()) {
				//System.out.println("start");
//				UiApplication.getUiApplication().pushGlobalScreen(this, 1,UiEngine.GLOBAL_QUEUE);
				UiApplication.getUiApplication().pushScreen(this);
				UiApplication.getUiApplication().repaint();
			}
		}

	}

	
	public void start(boolean _dosPantallas) {
		dosPantallas = _dosPantallas;
		synchronized (UiApplication.getEventLock()) {
			if (!this.isDisplayed()) {
				//System.out.println("start");
//				UiApplication.getUiApplication().pushGlobalScreen(this, 1,UiEngine.GLOBAL_QUEUE);
				UiApplication.getUiApplication().pushScreen(this);
				UiApplication.getUiApplication().repaint();
			}
		}

	}

	
	protected boolean keyDown(int in_nKeyCode, int in_nTime) {
		int nKeyPressed = Keypad.key(in_nKeyCode);

		if (nKeyPressed == Keypad.KEY_MENU) {
			this.getMenu(Menu.INSTANCE_DEFAULT).show();
			return true;
		}

		return false;
	}

	
	public boolean onClose() {
		Communications.reestart();
		if (dosPantallas) {
			UiApplication.getUiApplication().popScreen(this);
			UiApplication.getUiApplication().popScreen(
					UiApplication.getUiApplication().getActiveScreen());
		} else {
			UiApplication.getUiApplication().popScreen(this);
		}
		return true;
	}

	
	private final MenuItem cancelar = new MenuItem("Cancelar conexión", 0, 0) {

		public void run() {

			UiApplication.getUiApplication().popScreen(
					UiApplication.getUiApplication().getActiveScreen());
			
		}

	};

	
	protected void makeMenu(Menu menu, int instance) {

		Field focus = UiApplication.getUiApplication().getActiveScreen()
				.getLeafFieldWithFocus();
		if (focus != null) {
			ContextMenu contextMenu = focus.getContextMenu();
			if (!contextMenu.isEmpty()) {

//				menu.add(contextMenu);
//
//				menu.addSeparator();

			}
		}

//		menu.add(cancelar);

	}
}
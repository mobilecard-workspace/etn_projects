package Controls;


import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;

public class TitleSectionField extends RichTextField {

        int mTextColor;
        int mBgColor;

        public TitleSectionField(String text, int bgColor, int textColor, long style) {
                super(text, style);
                mTextColor = textColor;
                mBgColor = bgColor;
        }

        protected void paint(Graphics graphics) {
                graphics.clear();
                graphics.setColor(mBgColor);
                graphics.fillRect(0, 0, getWidth(), getHeight());
                graphics.setColor(mTextColor);
                super.paint(graphics);
        }

}
package threads;

import java.util.Vector;

import mx.com.ib.connection.Communications;
import mx.com.ib.connection.HttpListener;
import mx.com.ib.utils.Utils;
import mypackage.HomeScreen;
import mypackage.LoginScreen;
import mypackage.MessagePopupScreen;
import mypackage.VersionScreen;
import mypackage.preLoginScreen;
import net.rim.device.api.system.Application;
import net.rim.device.api.system.ApplicationDescriptor;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;
import app.MainClass;
import beans.Version;

public class getVersionThread extends Thread implements HttpListener{
	
	public String url = MainClass.URL_GET_VERSION;
	public String post = "";

	
	public getVersionThread(String json){
		
		this.post = json;
		
//		connect();
		
	}
	
	public void connect(){
		
		Application.getApplication();
		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.start();
		}
		
		if(MainClass.idealConnection != null)
		{
			
			if(this.url != null)
			{				
				try{				    	   
					Communications.sendHttpPost(this,this.url,this.post);
					
		       }catch(Throwable t){
		    	   
		       }
			}
		}
		else
		{		
		
			Utils.checkConnectionType();
			this.connect();
		}
		
	}
	
	public void run() {
		// TODO Auto-generated method stub
		this.setPriority(Thread.MAX_PRIORITY);
		connect();
		//super.run();
	}

	public void handleHttpError(int errorCode, String error) {
		// TODO Auto-generated method stub
		
		getMessageError(error);
		
	}

	public boolean isDestroyed() {
		// TODO Auto-generated method stub
		return false;
	}

	public void receiveEstatus(String msg) {
		// TODO Auto-generated method stub
		
	}

	public void receiveHeaders(Vector _headers) {
		// TODO Auto-generated method stub
		
	}

	public void receiveHttpResponse(int appCode, byte[] response) {
		// TODO Auto-generated method stub
		
		String sTemp = null;
		StringBuffer sb = new StringBuffer();		
		
		try {
			
			sb.append(new String(response,0,response.length,"UTF-8"));
			sTemp = new String (sb.toString());
			
			UiApplication.getUiApplication();
			synchronized (Application.getEventLock()) {
				System.out.println(sTemp);
				MainClass.splashScreen.remove();
				System.out.println("remueve");
				
				Version categorias=null;
				System.out.println("crea");
				categorias=MainClass.jsParser.GET_VERSION(sTemp);
				System.out.println("parsea");
				ApplicationDescriptor appDesc = ApplicationDescriptor.currentApplicationDescriptor();	
				if(categorias!=null){
					System.out.println("1");
					if(categorias!=null&&!categorias.equals("")){
						
						
						if(appDesc.getVersion().toString().equals(categorias.getVersion())){
							/*System.out.println("3");
							String categories="idPlataforma=3";
							System.out.println("4");
							new getCategoriesThread(categories).start();			
							System.out.println("5");*/
							
							UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
							UiApplication.getUiApplication().pushScreen(new preLoginScreen());
							/*String banner="idPlataforma=3";
							System.out.println("4");
							new BannerThread(banner).start();			
							System.out.println("5");*/
							
						}else{
						
							if(categorias.getTipo().equals("2")){
								 UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
									UiApplication.getUiApplication().pushScreen(new VersionScreen(categorias));
								/*
								System.out.println("else2");
								
				    			int pregunta=Dialog.ask(Dialog.D_YES_NO, "Existe una nueva versi�n de Mundo ejecutivo �Desea Descargar?");
				                
				                if(Dialog.YES==pregunta){
				                	BrowserSession browserSession; 
				        			browserSession = Browser.getDefaultSession();   			
				        			browserSession.displayPage(categorias.getUrl());	
				        			System.exit(0);
				                }
				                if(Dialog.NO==pregunta){
				                	String categories="idPlataforma=3";
				        			new getCategoriesThread(categories).start();		
				                }
							*/}
							if(categorias.getTipo().equals("1")){
								 UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
									UiApplication.getUiApplication().pushScreen(new VersionScreen(categorias));
								/*
								System.out.println("tipo1 llego ac�");
							
				    			//Dialog.inform("Existe una nueva versi�n de Mundo ejecutivo");   
								UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("hay una nueva versi�n de Mundo ejecutivo", Field.NON_FOCUSABLE));
				    			System.out.println("tipo1 llego ac�2");
				                	BrowserSession browserSession; 
				        			browserSession = Browser.getDefaultSession();   			
				        			browserSession.displayPage(categorias.getUrl());
				        			System.exit(0);
				                
							
							*/}	
						}
						System.out.println("****************************** "+categorias.getVersion());
						System.out.println("****************************** "+categorias.getTipo());
						System.out.println("****************************** "+categorias.getUrl());
						   
						}else{
							System.exit(0);
						}
					//UiApplication.getUiApplication().pushScreen(new SplashScreen(categorias));
								
				}
					
				else{
					UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Intente m�s tarde", Field.NON_FOCUSABLE));	
				}
				
				
			}
		}catch(Exception e){
			//System.out.println(e.toString());
			getMessageError(e.getMessage());
		}

	}
	
	public void getMessageError(String e){

		synchronized (Application.getEventLock()) {
			MainClass.splashScreen.remove();
			UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Intente m�s tarde. ", Field.NON_FOCUSABLE));
		}
		
	}

}

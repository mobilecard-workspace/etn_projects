package mx.com.ib.etn.view.controls;

import mx.com.ib.etn.utils.UtilColor;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;

public class TitleOrangeRichTextField extends RichTextField{

	private String title;
	private int width = 0;
	
	
	public TitleOrangeRichTextField(String title){
		super(RichTextField.NON_FOCUSABLE);
		this.title = title;
		this.width = Display.getWidth();
	}
	
	
	protected void paint(Graphics graphics) {
		graphics.setBackgroundColor(UtilColor.TITLE_BACKGROUND);
		graphics.clear();
		graphics.setColor(UtilColor.TITLE_STRING);
		graphics.drawText(title, 0, 0,
				DrawStyle.HCENTER, width);

		super.paint(graphics);
	}
}

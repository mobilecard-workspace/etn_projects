package mx.com.ib.etn.view.vroute;

import mx.com.ib.etn.beans.dto.VRouter;
import mx.com.ib.etn.utils.UtilColor;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;

public class DepartRichTextField extends RichTextField{

	private VRouter depart;
	private String office;
	private int width = 0;

	public DepartRichTextField(VRouter depart) {

		super("", RichTextField.NON_FOCUSABLE | RichTextField.USE_ALL_HEIGHT);
		this.depart = depart;
		office = depart.getOffice();
		width = Display.getWidth();
	}


	public void paint(Graphics graphics) {

		graphics.setColor(UtilColor.ELEMENT_STRING_TITLE);
		graphics.drawText("Salida: ", 0, 0, DrawStyle.RIGHT, width / 3);
		graphics.setColor(UtilColor.ELEMENT_STRING_DATA);
		graphics.drawText(office, width / 3, 0, DrawStyle.LEFT, width);

		super.paint(graphics);
	}
}

package mx.com.ib.etn.view.scheduled;

import mx.com.ib.etn.beans.beans.UserBean;
import mx.com.ib.etn.beans.dto.Scheduled;
import mx.com.ib.etn.model.data.DataKeeper;
import mx.com.ib.etn.model.data.DataTravel;
import mx.com.ib.etn.utils.UtilColor;
import mx.com.ib.etn.view.controls.Viewable;
import mx.com.ib.etn.view.passengers.ViewPassengers;
import mx.com.ib.etn.view.popup.LoginPopUp;
import mx.com.ib.etn.view.vroute.ViewRoute;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.Screen;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.VerticalFieldManager;

import org.json.me.JSONObject;


public class CustomGridFieldScheduledDepart extends VerticalFieldManager implements Viewable {

	private Scheduled scheduled;
	private boolean isDepartFlight;
	private boolean isBasicFlight;
	
	public CustomGridFieldScheduledDepart(boolean isDepartFlight, boolean isBasicFlight, Scheduled scheduled) {
		super();
		this.scheduled = scheduled;
		this.isDepartFlight = isDepartFlight;
		this.isBasicFlight = isBasicFlight;
		
		add(new ScheduledRichTextField(scheduled, ScheduledRichTextField.DATE_DEPART));
		add(new ScheduledRichTextField(scheduled, ScheduledRichTextField.DATE_ARRIVE));
		add(new ScheduledRichTextField(scheduled, ScheduledRichTextField.DATE_LINE));
		add(new ScheduledRichTextField(scheduled, ScheduledRichTextField.DATE_SERVICE));
		add(new ScheduledRichTextField(scheduled, ScheduledRichTextField.DATE_CHARGE));
		add(new NullField());
	}


	public boolean setData() {
		return true;
	}


	protected void paint(Graphics graphics){

		graphics.clear();
		
		if (isFocus()){
			graphics.setColor(UtilColor.ELEMENT_FOCUS);
			graphics.setGlobalAlpha(200);
			graphics.fillRoundRect(0, 0, getPreferredWidth(), getPreferredHeight(), 1, 1);
			graphics.setGlobalAlpha(255);
		} else {
			graphics.setColor(UtilColor.ELEMENT_BACKGROUND);
			graphics.fillRoundRect(0, 0, getPreferredWidth(), getPreferredHeight(), 1, 1);
		}
		
		super.paint(graphics);
	}	
	
	
	
	public boolean isFocusable() {
		return true;
	}

	public int getPreferredWidth() {
		return Display.getWidth();
	}

	protected boolean navigationClick(int status, int time) {
		fieldChangeNotify(0);
		showView();
		return super.navigationClick(status, time);
	}

	protected boolean keyChar(char character, int status, int time) {
		if (character == Keypad.KEY_ENTER) {
			fieldChangeNotify(0);
			showView();
			return true;
		}
		
/*
		else if (character == Keypad.KEY_ESCAPE){
			Dialog.alert("Salir");
		}
*/
		return super.keyChar(character, status, time);
	}

	protected boolean touchEvent(TouchEvent message) {

		if (message.getEvent() == TouchEvent.CLICK){
			fieldChangeNotify(0);
			showView();
			return true;
		}

		return super.touchEvent(message);
	}
	
	protected void onFocus(int direction) {
		super.onFocus(direction);
		invalidate();
	}

	protected void onUnfocus() {
		super.onUnfocus();
		invalidate();
	}


	private void showView(){

		if (isBasicFlight){

			if (UserBean.nameLogin == null){
				UiApplication.getUiApplication().pushScreen(new LoginPopUp(this));
			} else {
				showDialog();
			}
		} else {
			showDialog();
		}
	}


	private void showDialog() {

		DataKeeper dataKeeper = DataKeeper.getInstance();
		DataTravel dataTravel = dataKeeper.getDataTravel();

		String depart = dataTravel.getIdDepart();
		Scheduled scheduled = getScheduled();
		
		
		final int SEAT = 0;
		final int ROUTE = 1;
		final int BASIC = 2;
		int answer = 0;

		if (isBasicFlight) {
			int DEFAULT = BASIC;
			answer = Dialog.ask("Opciones", new String[] { "Asignar Asientos", "Revisar itinerario." }, new int[] { BASIC, ROUTE}, DEFAULT);
		} else {
			int DEFAULT = SEAT;
			answer = Dialog.ask("Opciones", new String[] {"Horario de regreso", "Revisar itinerario." }, new int[] {SEAT, ROUTE}, DEFAULT);
		}

		Screen screen = null;
		switch (answer) {
		case SEAT:
			dataKeeper.setScheduledDepart(scheduled);
			screen = UiApplication.getUiApplication().getActiveScreen();
			UiApplication.getUiApplication().popScreen(screen);
			UiApplication.getUiApplication().pushScreen(new ViewScheduledArrive(false, isBasicFlight));
			break;
		case ROUTE:
			UiApplication.getUiApplication().pushScreen(new ViewRoute(scheduled, depart));
			break;
		case BASIC:
			dataKeeper.setScheduledDepart(scheduled);
			screen = UiApplication.getUiApplication().getActiveScreen();
			UiApplication.getUiApplication().popScreen(screen);
			UiApplication.getUiApplication().pushScreen(new ViewPassengers(true, isBasicFlight));
			break;
		}
/*
		final int SEAT = 0;
		final int ROUTE = 1;
		final int BASIC = 2;
		final int CLOSE = 3;
		int answer = 0;

		if (isBasicFlight) {
			int DEFAULT = BASIC;
			answer = Dialog.ask("Opciones", new String[] { "Asignar Asientos", "Revisar itinerario", "Cancelar" }, new int[] { BASIC, ROUTE, CLOSE}, DEFAULT);
		} else {
			int DEFAULT = SEAT;
			answer = Dialog.ask("Opciones", new String[] {"Horario de regreso", "Revisar itinerario", "Cancelar" }, new int[] {SEAT, ROUTE, CLOSE}, DEFAULT);
		}

		switch (answer) {
		case SEAT:
			dataKeeper.setScheduledDepart(scheduled);
			UiApplication.getUiApplication().pushScreen(new ViewScheduledArrive(false, isBasicFlight));
			break;
		case ROUTE:
			UiApplication.getUiApplication().pushScreen(new ViewRoute(scheduled, depart));
			break;
		case BASIC:
			dataKeeper.setScheduledDepart(scheduled);
			UiApplication.getUiApplication().pushScreen(new ViewPassengers(true, isBasicFlight));
			break;
			
		case CLOSE:
			UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
			break;
		}
*/
	}
	
	public Scheduled getScheduled(){
		
		ScheduledRichTextField richTextField = (ScheduledRichTextField)getField(0);
		Scheduled scheduled = richTextField.getScheduled();
		return scheduled;
	}


	public void setData(int request, JSONObject jsObject) {
		// TODO Auto-generated method stub
		
	}


	public void sendMessage(String message) {
		// TODO Auto-generated method stub
		
	}
}
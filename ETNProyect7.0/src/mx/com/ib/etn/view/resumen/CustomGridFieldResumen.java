package mx.com.ib.etn.view.resumen;

import net.rim.device.api.ui.component.NullField;
import mx.com.ib.etn.beans.beans.Resumen;

public class CustomGridFieldResumen extends CustomGridField{

	public CustomGridFieldResumen(Resumen resumen){
		
		super();
		
		CustomRichTextField rResumen = new CustomRichTextField("Total: ", resumen.getImporteTotal());
		
		add(rResumen);
		add(new NullField());
	}
	
}

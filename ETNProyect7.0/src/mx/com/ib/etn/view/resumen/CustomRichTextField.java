package mx.com.ib.etn.view.resumen;

import mx.com.ib.etn.utils.UtilColor;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;

public class CustomRichTextField extends RichTextField {

	private String title;
	private String data;
	private int size = 0;
	private int allSize = 0;
	
	public CustomRichTextField(String title, String data) {

		super("", RichTextField.NON_FOCUSABLE | RichTextField.USE_ALL_HEIGHT);
		
		this.title = title;
		this.data = data;
		
		size = (Display.getWidth() / 5) * 2;
		allSize = Display.getWidth();
		
	}


	public void paint(Graphics graphics) {

		graphics.setColor(UtilColor.ELEMENT_STRING_TITLE);
		graphics.drawText(title, 0, 0, DrawStyle.RIGHT, size);
		graphics.setColor(UtilColor.ELEMENT_STRING_DATA);
		graphics.drawText(data, size, 0, DrawStyle.LEFT, allSize);

		super.paint(graphics);
	}
}
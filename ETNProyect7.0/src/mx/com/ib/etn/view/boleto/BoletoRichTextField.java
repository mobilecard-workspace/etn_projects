package mx.com.ib.etn.view.boleto;

import mx.com.ib.etn.beans.beans.Boleto;
import mx.com.ib.etn.utils.UtilColor;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;

public class BoletoRichTextField extends RichTextField {


	final public static int TITLE_ORIGEN = 0;
	final public static int TITLE_DESTINO = 1;
	final public static int TITLE_SALIDA = 2;
	
	final private String titleOrigen = "Origen: ";
	final private String titleDestino = "Destino: ";
	final private String titleSalida = "Salida: ";

	private String title = null;
	private String info = null;
	
	private Boleto boleto;

	public Boleto getBoleto(){
		return boleto;
	}
	
	public BoletoRichTextField(Boleto boleto, int type) {

		super("", RichTextField.NON_FOCUSABLE | RichTextField.USE_ALL_HEIGHT);
		
		this.boleto = boleto;
		
		switch (type) {
		case BoletoRichTextField.TITLE_ORIGEN:
			title = titleOrigen;
			info = boleto.getOrigen();
			break;
		case BoletoRichTextField.TITLE_DESTINO:
			title = titleDestino;
			info = boleto.getDestino();
			break;
		case BoletoRichTextField.TITLE_SALIDA:
			title = titleSalida;
			info = boleto.getFecha() + " " + boleto.getHora();
			break;
		}
	}


	public void paint(Graphics graphics) {

		graphics.setColor(UtilColor.ELEMENT_STRING_TITLE);
		graphics.drawText(title, 0, 0, DrawStyle.RIGHT,
				Display.getWidth() / 4);
		graphics.setColor(UtilColor.ELEMENT_STRING_DATA);
		graphics.drawText(info, Display.getWidth() / 4, 0, DrawStyle.LEFT,
				Display.getWidth());

		super.paint(graphics);
	}
}


package mx.com.ib.etn.view.boleto;

import mx.com.ib.etn.beans.beans.Boleto;
import mx.com.ib.etn.utils.UtilColor;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;

public class BoletoDataRichTextField extends RichTextField {


	final public static int TITLE_ORIGEN = 0;
	final public static int TITLE_DESTINO = 1;
	final public static int TITLE_SALIDA = 2;
	
	final private String titleOrigen = "Origen: ";
	final private String titleDestino = "Destino: ";
	final private String titleSalida = "Salida: ";

	private String title = null;
	private String info = null;
	
	private Boleto boleto;
	
	private int width = 0;
	private int width5 = 0;
	private int width4 = 0;
	
	public Boleto getBoleto(){
		return boleto;
	}
	
	public BoletoDataRichTextField(String title, String info) {

		super("", RichTextField.NON_FOCUSABLE | RichTextField.USE_ALL_HEIGHT);

		this.title = title + ":";
		this.info = info;
		width = Display.getWidth();
		width4 = width/20;
		width5 = 6 * (width/20);
		
		Font font01 = Font.getDefault();
		Font font02 = font01.derive(font01.getStyle(), 20);
		setFont(font02);
	}


	public void paint(Graphics graphics) {

		graphics.setColor(UtilColor.ELEMENT_STRING_TITLE);
		graphics.drawText(title, 0, 0, DrawStyle.RIGHT, width5);
		graphics.setColor(UtilColor.ELEMENT_STRING_DATA);
		graphics.drawText(info, width5 + width4/2, 0, DrawStyle.LEFT, width);

		super.paint(graphics);
	}
}


/*
	public BoletoDataRichTextField(Boleto boleto, int type) {

		super("", RichTextField.NON_FOCUSABLE | RichTextField.USE_ALL_HEIGHT);
		
		//LabelField labelField = new LabelField("Hello World");
		Font font01 = Font.getDefault();
		
		Font font02 = font01.derive(font01.getStyle(), 23);
		setFont(font02);

		this.boleto = boleto;
		
		switch (type) {
		case BoletoRichTextField.TITLE_ORIGEN:
			title = titleOrigen;
			info = boleto.getOrigen();
			break;
		case BoletoRichTextField.TITLE_DESTINO:
			title = titleDestino;
			info = boleto.getDestino();
			break;
		case BoletoRichTextField.TITLE_SALIDA:
			title = titleSalida;
			info = boleto.getFecha() + " " + boleto.getHora();
			break;
		}
	}
*/
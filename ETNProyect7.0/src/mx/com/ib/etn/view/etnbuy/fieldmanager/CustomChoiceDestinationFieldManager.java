package mx.com.ib.etn.view.etnbuy.fieldmanager;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class CustomChoiceDestinationFieldManager extends VerticalFieldManager{

	private int width; 
	private int height;
	
	private ObjectChoiceField choiceField;
	
	public CustomChoiceDestinationFieldManager(ObjectChoiceField choiceField){
		
		super();
		this.choiceField = choiceField;
		width = choiceField.getPreferredWidth();
		height = choiceField.getPreferredHeight();
	}
	
	
	public void updatePreferredSize(){
		width = choiceField.getPreferredWidth();
		height = choiceField.getPreferredHeight();
	}
	
	public int getPreferredWidth() {
		return Display.getWidth()/3;
	}
	
	/*
	public int getPreferredHeight() {
		return height;
	}
	*/
}

package mx.com.ib.etn.view.etnbuy.fieldmanager;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class CustomStringDestinationFieldManager extends VerticalFieldManager{

	private ObjectChoiceField choiceField;
	private int width;
	private int height;
	
	public CustomStringDestinationFieldManager(ObjectChoiceField choiceField){
		
		super();
		this.choiceField = choiceField;
		int width = Display.getWidth();
		this.width = width - choiceField.getPreferredWidth();
		height = choiceField.getPreferredHeight();
	}
	
	
	public void updatePreferredSize(){
		int width = Display.getWidth();
		this.width = width - choiceField.getPreferredWidth();
		height = choiceField.getPreferredHeight();
		System.out.println("this " + this.width + " width " + width);
	}
	
	
	public int getPreferredWidth() {
		return Display.getWidth()/3;
	}
/*
	public int getPreferredHeight() {
		return height;
	}
	*/
}

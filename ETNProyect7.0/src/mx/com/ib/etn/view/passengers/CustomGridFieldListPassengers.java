package mx.com.ib.etn.view.passengers;

import mx.com.ib.etn.beans.dto.Passenger;
import mx.com.ib.etn.beans.dto.Scheduled;
import mx.com.ib.etn.model.data.DataKeeper;
import mx.com.ib.etn.utils.UtilColor;
import mx.com.ib.etn.view.seats.SeatsController;
import mx.com.ib.etn.view.seats.ViewSeats;
import mx.com.ib.etn.view.seats.ViewSeats2;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.VerticalFieldManager;

/*

	Presentara la informacion de los pasajeros:
	Nombre
	Tipo de pasajero
	Costo del viaje

*/

public class CustomGridFieldListPassengers extends VerticalFieldManager {

	private Passenger passenger;
	private NameRichTextField title;
	private PassengerKindRichTextField passengerKindRichTextField;
	private ChargeRichTextField chargeRichTextField;
	private SeatRichTextField seatRichTextField;
	private boolean isDepartFlight;
	private boolean isBasicFlight;
	private boolean isUsed = false;


	public CustomGridFieldListPassengers(boolean isDepartFlight, boolean isBasicFlight, Passenger passenger) {
	//public CustomGridFieldListPassengers() {
		super();

		this.isDepartFlight = isDepartFlight;
		this.isBasicFlight = isBasicFlight;
		this.passenger = passenger;

		title = new NameRichTextField(passenger);
		passengerKindRichTextField = new PassengerKindRichTextField(passenger);
		chargeRichTextField = new ChargeRichTextField(passenger);
		seatRichTextField = new SeatRichTextField(passenger);
		add(title);
		add(passengerKindRichTextField);
		add(chargeRichTextField);
		add(seatRichTextField);
		add(new NullField());
	}
	
	public boolean setData() {
		return true;
	}

	public void setName(String name){
		
		title.setLabel(name);
		passenger.setName(name);
	}
	
	public Passenger getPassenger(){
		return passenger;
	}
	
	protected void paint(Graphics graphics){

		graphics.clear();
		
		if (isFocus()){
			graphics.setColor(UtilColor.ELEMENT_FOCUS);
			graphics.setGlobalAlpha(200);
			graphics.fillRoundRect(0, 0, getPreferredWidth(), getPreferredHeight(), 1, 1);
			graphics.setGlobalAlpha(255);
		} else {
			graphics.setColor(UtilColor.ELEMENT_BACKGROUND);
			graphics.fillRoundRect(0, 0, getPreferredWidth(), getPreferredHeight(), 1, 1);
		}
		
		super.paint(graphics);
	}
	
	public boolean isFocusable() {
		return true;
	}

	public int getPreferredWidth() {
		return Display.getWidth();
	}

	protected boolean navigationClick(int status, int time) {
		fieldChangeNotify(0);
		showViewSeats();
		return super.navigationClick(status, time);
	}

	protected boolean keyChar(char character, int status, int time) {
		if (character == Keypad.KEY_ENTER) {
			fieldChangeNotify(0);
			showViewSeats();
			return true;
		}
		return super.keyChar(character, status, time);
	}

	
	protected boolean touchEvent(TouchEvent message) {

		if (message.getEvent() == TouchEvent.CLICK){
			fieldChangeNotify(0);
			showViewSeats();
			return true;
		}

		return super.touchEvent(message);
	}
	
	
	protected void onFocus(int direction) {
		super.onFocus(direction);
		invalidate();
	}

	protected void onUnfocus() {
		super.onUnfocus();
		invalidate();
	}
	
	
	public boolean isUsed() {
		return isUsed;
	}

	public void setUsed(boolean isUsed) {
		this.isUsed = isUsed;
	}
	
	
	private void showViewSeats(){

		isUsed = true;

		DataKeeper dataKeeper = null;
		Scheduled scheduled = null;
		String flight = null;
		String dateDepart = null;

		dataKeeper = DataKeeper.getInstance();

		if (isDepartFlight){
			scheduled = dataKeeper.getScheduledDepart();
			flight = scheduled.getFlight();
			dateDepart = scheduled.getDateDepartToString();
		} else {
			scheduled = dataKeeper.getScheduledDepart();
			flight = scheduled.getFlight();
			dateDepart = scheduled.getDateDepartToString();
		}

		UiApplication.getUiApplication().pushScreen(new ViewSeats2(isDepartFlight, flight, dateDepart, passenger));
	}
}
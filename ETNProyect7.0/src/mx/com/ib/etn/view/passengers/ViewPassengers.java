package mx.com.ib.etn.view.passengers;

import java.util.Vector;

import mx.com.ib.etn.beans.beans.Resumen;
import mx.com.ib.etn.beans.dto.Passenger;
import mx.com.ib.etn.beans.dto.Scheduled;
import mx.com.ib.etn.model.connection.base.ThreadHTTP;
import mx.com.ib.etn.model.connection.base.UrlEncode;
import mx.com.ib.etn.model.data.DataKeeper;
import mx.com.ib.etn.model.data.DataTravel;
import mx.com.ib.etn.model.data.Payer;
import mx.com.ib.etn.view.animated.SplashScreen;
import mx.com.ib.etn.view.controls.CustomMainScreen;
import mx.com.ib.etn.view.controls.Viewable;
import mx.com.ib.etn.view.resumen.ViewResumenCompra;
import net.rim.blackberry.api.browser.URLEncodedPostData;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Screen;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;

import org.json.me.JSONException;
import org.json.me.JSONObject;


/*
	Reune la informacion de los pasajeros, menos el nombre
	y crea un gridfielmanager que los mostrara.
*/

public class ViewPassengers extends CustomMainScreen implements FieldChangeListener,
Viewable {

	
	private DataKeeper dataKeeper;
	private DataTravel dataTravel;
	private Scheduled scheduled; 
			
	private Vector passengers;
	private ButtonField accept;
	private ButtonField acceptBasic;
	private boolean isDepartFlight;
	private boolean isBasicFlight;
	private SplashScreen splashScreen;

	private String importeTotal1;
	private String numError1;
	private String idReservacion1;
	
	
	private double importe1 = 0;
	private double monto = 0;

	
	public ViewPassengers(boolean isDepartFlight, boolean isBasicFlight){
	//public ViewPassengers(){
		super(true, "Selección de pasajeros y asientos");

		this.isDepartFlight = isDepartFlight;
		this.isBasicFlight = isBasicFlight;

		dataKeeper = DataKeeper.getInstance();
		dataTravel = dataKeeper.getDataTravel();
		
		if (isDepartFlight){
			scheduled = dataKeeper.getScheduledDepart();
			passengers = dataKeeper.getPassengersDepart();
			if (passengers == null){
				passengers = new Vector();
				setPassengers();
				dataKeeper.setPassengersDepart(passengers);
			}
		} else {
			scheduled = dataKeeper.getScheduledArrive();
			passengers = dataKeeper.getPassengersArrive();
			if (passengers == null){
				passengers = new Vector();
				setPassengers();
				dataKeeper.setPassengersArrive(passengers);
			}
		}

		addPassengers(passengers);

		
		if (isBasicFlight){
			acceptBasic = new ButtonField("Hacer pago", ButtonField.CONSUME_CLICK);
			acceptBasic.setChangeListener(this);
			add(acceptBasic);
		} else {
			accept = new ButtonField("Continuar", ButtonField.CONSUME_CLICK);
			accept.setChangeListener(this);
			add(accept);
		}
	}

	
	public void setPassengers(){
		int iAdults = dataTravel.getiAdults();
		int iChildren = dataTravel.getiChildren();
		int iElderlies = dataTravel.getiElderlies();
		int iStudents = dataTravel.getiStudents();
		int iProfessors = dataTravel.getiProfessors();

		addPassenger(iAdults, Passenger.KIND_ADULT, scheduled.getChargeAdults());
		addPassenger(iChildren, Passenger.KIND_CHILDREN, scheduled.getChargeChildren());
		addPassenger(iElderlies, Passenger.KIND_ELDERL, scheduled.getChargeElderlies());
		addPassenger(iStudents, Passenger.KIND_STUDENT, scheduled.getChargeStudents());
		addPassenger(iProfessors, Passenger.KIND_PROFESSOR, scheduled.getChargeProfessors());
	}
	
	
	
	private void addPassenger(int numPassenger, String kindPassenger, String charger){

		if (numPassenger > 0){
			for(int i = 0; i < numPassenger; i++){
				Passenger passenger = new Passenger();
				passenger.setKind(kindPassenger);
				passenger.setCharge(charger);
				passengers.addElement(passenger);
			}
		}
	}
	

	private void addPassengers(Vector passengers){
		
		int size = passengers.size();
		
		for(int index = 0; index < size; index++){
			
			Passenger passenger = (Passenger)passengers.elementAt(index);
			
			CustomGridFieldListPassengers fieldListPassengers = null; 
			fieldListPassengers = new CustomGridFieldListPassengers(isDepartFlight, isBasicFlight, passenger);
			add(fieldListPassengers);
		}
	}

	
	public void fieldChanged(Field field, int context) {
		
		if (checkUsed()){
			if (field == (ButtonField)accept){
				UiApplication.getUiApplication().popScreen(this);
			} else if (field == (ButtonField)acceptBasic){

				splashScreen = SplashScreen.getInstance();
				splashScreen.start();

				URLEncodedPostData idSecuencia = new URLEncodedPostData("", "");
				UrlEncode idUrlEncode = new UrlEncode();
				idUrlEncode.setRequest(Viewable.ID_SECUENCIA);
				idUrlEncode.setEncodedPostData(idSecuencia);

				Payer payer = new Payer();
				URLEncodedPostData irEncoded = payer.toPay(isBasicFlight, isBasicFlight, this);

				UrlEncode irUrlEncode = new UrlEncode();

				irUrlEncode.setRequest(Viewable.CREATE);
				irUrlEncode.setEncodedPostData(irEncoded);

				Vector vector = new Vector();
				vector.addElement(idUrlEncode);
				vector.addElement(irUrlEncode);

				ThreadHTTP threadHTTP = new ThreadHTTP();
				threadHTTP.setHttpConfigurator(this, vector);
				threadHTTP.start();
			}
			
		} else {
			
			Dialog.alert("Aun faltan pasajeros por asignar");
		}
	}

	
	private boolean checkUsed(){
		
		boolean value = true;
		
		int fields = getFieldCount();
		
		for (int index = 0; index < fields; index++){
			
			Field field = getField(index);
			
			if (field instanceof CustomGridFieldListPassengers){
				
				CustomGridFieldListPassengers passengerView = (CustomGridFieldListPassengers)field;
				
				Passenger passenger = passengerView.getPassenger();

				if (
						(!passenger.isNamed()) &&
						((passenger.getSeat() == null) ||
						(passenger.getSeat().length() == 0))
					){
					value = false;
				}
			}
		}
		
		return value;
	}

	
	public void sendMessage(String message) {

		Dialog.alert(message);
	}
	
	
	public void setData(int request, JSONObject jsObject) {

		splashScreen.remove();

		String referencia = null;

		try {
			Resumen resumenOrigen = new Resumen();


			JSONObject jsReferencia = jsObject.getJSONObject("0");
			referencia = jsReferencia.getString("secuencia");

			JSONObject jsViaje1 = jsObject.getJSONObject("1");
			String descError = jsViaje1.getString("descError");
			int numError = jsViaje1.getInt("numError");

			if (numError == 0) {
				importeTotal1 = jsViaje1.getString("importeTotal");
				numError1 = jsViaje1.getString("numError");
				idReservacion1 = jsViaje1.getString("idReservacion");

				if (true) {
					if (importeTotal1 != null) {

						importe1 = Double.valueOf(importeTotal1).doubleValue();
					}

					monto = importe1;

					resumenOrigen.setIdReservacion(idReservacion1);
					resumenOrigen.setImporte(importeTotal1);
					resumenOrigen.setImporteTotal(String.valueOf(monto));

					//Screen screen = UiApplication.getUiApplication().getActiveScreen();
					UiApplication.getUiApplication().popScreen(this);
					
					UiApplication.getUiApplication().pushScreen(new ViewResumenCompra(resumenOrigen, null, referencia));
				}
			} else {
				Dialog.alert(descError);
			}
		} catch (JSONException e) {
			Dialog.alert("Error al interpretar la información.");
			e.printStackTrace();
		}
	}


	public boolean onClose() {

		deleteAll();
		dataKeeper.setPassengersDepart(null);
		dataKeeper.setPassengersArrive(null);
		dataKeeper.setSeatsDepart(null);
		dataKeeper.setSeatsArrive(null);
		return super.onClose();
	}
}



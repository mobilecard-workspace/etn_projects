package mx.com.ib.etn.view.seats;

import mx.com.ib.etn.beans.dto.Passenger;
import mx.com.ib.etn.utils.UtilColor;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.component.ButtonField;

public class GraphicButtonSeat extends Field {

	final public static int SEAT_NOT_OCCUPIED = 1;
	final public static int SEAT_NOT_OCCUPIED_TV = 2;
	
	final public static int SEAT_AISLE  = 3;
	
	final public static int SEAT_OCCUPIED = 11;
	final public static int SEAT_OCCUPIED_TV = 12;

	final public static int SEAT_WC_MAN = 13;
	final public static int SEAT_WC_WOMAN = 14;
	final public static int SEAT_COFFEE_SHOP = 15;

	final public static int SEAT_WC_UNISEX = 16;
	final public static int SEAT_STAIR = 17;
	
	final public static int SEAT_SECOND_FLOOR = 18;
	
	
	final public static int KIND_ADULT = 6;
	final public static int KIND_CHILD = 7;
	final public static int KIND_ELDERLY = 8;
	final public static int KIND_STUDENT = 9;
	final public static int KIND_PROFESOR = 10;

	public int other = 0;	

	public Bitmap seatStatusBitMap;
	public Bitmap customerStatusBitMap;
	
	public int preferredHeight;
	public int preferredWidth;
	
	public int spaceWidth;
	public int spaceHeight;
	
	
	public int unFocusHeight;
	public int unFocusWidth;
	
	public int focusHeight;
	public int focusWidth;

	public int statusSeat = 0;
	public boolean isUsedByCustomer = false;

	public boolean isOccupied = false;
	public String seat;
	public Passenger passenger;
	

	public boolean isUsedByCustomer() {
		return isUsedByCustomer;
	}


	public void setUsedByCustomer(boolean isUsedByCustomer) {
		this.isUsedByCustomer = isUsedByCustomer;
	}
	
	public void setUsedByCustomerInvalidate(boolean isUsedByCustomer) {
		this.isUsedByCustomer = isUsedByCustomer;
		invalidate();
	}
	
	
	public boolean isSamePassenger(Passenger passenger){
		
		return passenger.equals(passenger);
	}
	

	
	public GraphicButtonSeat(int statusSeat, int other){
		super(ButtonField.CONSUME_CLICK);
		this.statusSeat = statusSeat;
		this.other = other;
	}
	
	public boolean isOccupied() {
		return isOccupied;
	}

	public void setOccupied(boolean isOccupied) {
		this.isOccupied = isOccupied;
	}

	public boolean isSeatAisle(){
		return GraphicButtonSeat.SEAT_AISLE == statusSeat;
	}

	public void setBitmap(){

		if ((isOccupied)&&(statusSeat != GraphicButtonSeat.SEAT_AISLE)){

			statusSeat += 10;
		}
		
		switch (statusSeat) {
		
		case GraphicButtonSeat.SEAT_OCCUPIED:
			seatStatusBitMap = Bitmap.getBitmapResource("asiento_ocupado.png");
			break;
		case GraphicButtonSeat.SEAT_NOT_OCCUPIED:
			seatStatusBitMap = Bitmap.getBitmapResource("asiento_desocupado.png");
			break;
		case GraphicButtonSeat.SEAT_OCCUPIED_TV:
			seatStatusBitMap = Bitmap.getBitmapResource("asiento_ocupado_tele.png");
			break;
		case GraphicButtonSeat.SEAT_NOT_OCCUPIED_TV:
			seatStatusBitMap = Bitmap.getBitmapResource("asiento_desocupado_tele.png");
			break;
		case GraphicButtonSeat.SEAT_AISLE:

			switch (other) {
				case GraphicButtonSeat.SEAT_WC_MAN:
					seatStatusBitMap = Bitmap.getBitmapResource("asiento_ocupado_wc_hombre.png");
					break;
				case GraphicButtonSeat.SEAT_WC_WOMAN:
					seatStatusBitMap = Bitmap.getBitmapResource("asiento_ocupado_wc_mujer.png");
					break;
				case GraphicButtonSeat.SEAT_COFFEE_SHOP:
					seatStatusBitMap = Bitmap.getBitmapResource("asiento_ocupado_cafeteria.png");
					break;
				case GraphicButtonSeat.SEAT_WC_UNISEX:
					seatStatusBitMap = Bitmap.getBitmapResource("asiento_ocupado_wc_unisex.png");
					break;
				case GraphicButtonSeat.SEAT_STAIR:
					seatStatusBitMap = Bitmap.getBitmapResource("asiento_escalera.png");
					break;
				case GraphicButtonSeat.SEAT_SECOND_FLOOR:
					seatStatusBitMap = Bitmap.getBitmapResource("asiento_nuevo_piso.png");
					break;
				default:seatStatusBitMap = Bitmap.getBitmapResource("asiento_pasillo.png");
			}
			
			break;
		}
		
		if (seatStatusBitMap == null){
			
			seatStatusBitMap = Bitmap.getBitmapResource("asiento_pasillo.png");
		}
		
		if (statusSeat == GraphicButtonSeat.SEAT_NOT_OCCUPIED_TV){
			
			customerStatusBitMap = Bitmap.getBitmapResource("asiento_ocupado_tele_cliente.png");
		} else {
			customerStatusBitMap = Bitmap.getBitmapResource("asiento_ocupado_cliente.png");
		}
		
		unFocusHeight = seatStatusBitMap.getHeight();
		unFocusWidth = seatStatusBitMap.getWidth();

		focusHeight = customerStatusBitMap.getHeight();
		focusWidth = customerStatusBitMap.getWidth();

		preferredHeight = Math.max(focusHeight, unFocusHeight);
		preferredWidth = Math.max(focusWidth, unFocusWidth);
		
		
		Font  font = Font.getDefault();
		
		spaceWidth = font.getAdvance(seat);
		spaceHeight = font.getHeight();

		preferredHeight = preferredHeight + spaceHeight;
		//preferredWidth = preferredWidth + spaceWidth;
		
	}
	
	public int getPreferredHeight(){
		return preferredHeight;
	}

	public int getPreferredWidth(){
		return preferredWidth;
	}

	protected void layout(int width, int height){
		setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	protected void paint(Graphics graphics){

		graphics.clear();
		if (isUsedByCustomer){
			graphics.drawBitmap(0, spaceHeight, focusWidth, focusHeight, customerStatusBitMap, 0, 0);
		} else {
			graphics.drawBitmap(0, spaceHeight, unFocusWidth, unFocusHeight, seatStatusBitMap, 0, 0);
		}

		
		if (statusSeat != GraphicButtonSeat.SEAT_AISLE){
			graphics.setColor(UtilColor.TITLE_BACKGROUND);
			graphics.drawText(seat, 0, 0, DrawStyle.HCENTER, preferredWidth);
		}


		if (isFocus()){
			graphics.setColor(Color.WHITE);
			graphics.setGlobalAlpha(110);
			graphics.fillRoundRect(0, 0, preferredWidth, preferredHeight, 1, 1);
			graphics.setGlobalAlpha(255);
		}
	}
	
	public boolean isFocusable(){

		boolean value = !(
							(isOccupied) || 
							(statusSeat == GraphicButtonSeat.SEAT_AISLE) 
						);
		
		return value;
	}
	
	protected void onFocus(int direction){
		super.onFocus(direction);
		invalidate();
	}
	
	protected void onUnfocus(){
		super.onUnfocus();
		invalidate();
	}
	
	protected boolean navigationClick(int status, int time) {
		fieldChangeNotify(0);
		return true;
	}
	
	protected boolean keyChar(char character, int status, int time) {
		if (character == Keypad.KEY_ENTER) {
			fieldChangeNotify(0);
			return true;
		}
		return super.keyChar(character, status, time);
	}
	
	
	public void isPermitted(boolean isUsed){
		
		if ((!isOccupied) && (!isSeatAisle())){
			isUsedByCustomer = isUsed;
		}
		
		invalidate();
	}
	
	
	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		
		if (seat != null){
			
			if (seat.indexOf("T")>=0){

				int index = seat.indexOf("T");
				seat = seat.substring(0, index);
			}
		}
		
		this.seat = seat;
	}

	public Passenger getPassenger() {
		return passenger;
	}

	public void setPassenger(Passenger passenger) {
		this.passenger = passenger;
	}
}

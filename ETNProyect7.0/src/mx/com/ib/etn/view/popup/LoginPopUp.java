package mx.com.ib.etn.view.popup;

import mx.com.ib.etn.model.connection.addcel.InfoThread.Login;
import mx.com.ib.etn.view.controls.Viewable;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.PasswordEditField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class LoginPopUp extends PopupScreen implements FieldChangeListener{

	private LabelField lNombre;
	private EditField eUsuario;
	
	private LabelField lContrasena;
	private PasswordEditField eContrasena;

	private ButtonField aceptar;
	private ButtonField cancelar;

	private Viewable viewable;
	
	
	public LoginPopUp(Viewable viewable){
	//public LoginPopUp(){
		super(new VerticalFieldManager());
	
		this.viewable = viewable;
		
		lNombre = new LabelField("Nombre: ");
		eUsuario = new EditField("", "");
		
		lContrasena = new LabelField("Contraseņa: ");
		eContrasena = new PasswordEditField("", "");

		aceptar = new ButtonField("Aceptar");
		cancelar = new ButtonField("Cancelar");
		
		HorizontalFieldManager hManager = new HorizontalFieldManager();
		hManager.add(aceptar);
		hManager.add(cancelar);
		
		aceptar.setChangeListener(this);
		cancelar.setChangeListener(this);
		
		add(lNombre);
		add(eUsuario);
		add(lContrasena);
		add(eContrasena);

		add(hManager);
	}

	
    protected boolean keyChar(char c, int status, int time){

    	if (c == Characters.ESCAPE) {
             close();
    	}

        return super.keyChar(c, status, time);
   }


	public void fieldChanged(Field field, int context) {
		
		if (field == aceptar){
			getLogin();
		} else if (field == cancelar){
			close();
		}
	}

	
	private void getLogin(){
		
		String contrasena = eContrasena.getText();
		String usuario = eUsuario.getText();
		
		if ( checkString(contrasena)  && checkString(usuario)){
			Login loginThread = new Login(null, usuario, contrasena);
			loginThread.run();
			close();
		} else {
			Dialog.alert("Verificar usuario y contraseņa");
		}
	}
	
	
	private boolean checkString(String string){
		
		boolean value = false;
		
		if ((string != null)&&(string.length() >= 8) ){
			value = true;
		} 
		
		return value;
	}
}
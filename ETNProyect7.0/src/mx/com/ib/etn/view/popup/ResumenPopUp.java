package mx.com.ib.etn.view.popup;

import mx.com.ib.etn.beans.beans.Resumen;
import mx.com.ib.etn.beans.beans.UserBean;
import mx.com.ib.etn.view.controls.Viewable;
import mx.com.ib.etn.view.pagoweb.VPagoWeb;
import net.rim.blackberry.api.browser.URLEncodedPostData;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Screen;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class ResumenPopUp extends PopupScreen implements FieldChangeListener{
	
	private LabelField lReservacion;
	private LabelField lMonto1;
	private LabelField lMonto2;
	private LabelField lMonto;
	
	private LabelField eReservacion;
	private LabelField eMonto1;
	private LabelField eMonto2;
	private LabelField eMonto;
	
	private ButtonField aceptar;
	private ButtonField cancelar;

	private String referencia;
	
	private Resumen resumenOrigen;
	private Resumen resumenDestino;

	private Viewable viewable;

	public ResumenPopUp(Viewable viewable, Resumen resumenOrigen, Resumen resumenDestino, String referencia){

		super(new VerticalFieldManager());

		this.referencia = referencia;

		this.viewable = viewable;

		this.resumenOrigen = resumenOrigen;
		this.resumenDestino = resumenDestino;
		
		if (resumenDestino != null){
			viajeRedondo();
		} else {
			viajeSencillo();
		}

	}

	private void viajeRedondo(){
		
		lReservacion = new LabelField("Reservacion: ");
		lMonto1 = new LabelField("Monto origen: ");
		lMonto2 = new LabelField("Monto destino: ");
		lMonto = new LabelField("Monto Total: ");
		
		eReservacion = new LabelField(resumenOrigen.getIdReservacion());
		eMonto1 = new LabelField(resumenOrigen.getImporte());
		eMonto2 = new LabelField(resumenDestino.getImporte());
		eMonto = new LabelField(resumenOrigen.getImporteTotal());
		
		add(lReservacion);
		add(eReservacion);
		add(lMonto1);
		add(eMonto1);

		add(lMonto2);
		add(eMonto2);
		add(lMonto);
		add(eMonto);

		aceptar = new ButtonField("Aceptar");
		cancelar = new ButtonField("Cancelar");

		aceptar.setChangeListener(this);
		cancelar.setChangeListener(this);
		
		HorizontalFieldManager hManager = new HorizontalFieldManager();
		hManager.add(aceptar);
		hManager.add(cancelar);
		
		add(hManager);		
	}
	
	
	private void viajeSencillo(){

		lReservacion = new LabelField("Reservacion: ");
		lMonto1 = new LabelField("Monto origen: ");
		lMonto = new LabelField("Monto Total: ");

		eReservacion = new LabelField(resumenOrigen.getIdReservacion());
		eMonto1 = new LabelField(resumenOrigen.getImporte());
		eMonto = new LabelField(resumenOrigen.getImporteTotal());
		
		add(lReservacion);
		add(eReservacion);
		add(lMonto1);
		add(eMonto1);

		add(lMonto);
		add(eMonto);

		aceptar = new ButtonField("Aceptar");
		cancelar = new ButtonField("Cancelar");

		aceptar.setChangeListener(this);
		cancelar.setChangeListener(this);
		
		HorizontalFieldManager hManager = new HorizontalFieldManager();
		hManager.add(aceptar);
		hManager.add(cancelar);
		
		add(hManager);
		
	}
	
    protected boolean keyChar(char c, int status, int time){

    	if (c == Characters.ESCAPE) {
             close();
    	}

        return super.keyChar(c, status, time);
   }


	public void fieldChanged(Field field, int context) {
		
		if (field == aceptar){
			URLEncodedPostData encodedPostData = new URLEncodedPostData(URLEncodedPostData.DEFAULT_CHARSET, false);

			encodedPostData.append("user", UserBean.idLogin);  
			encodedPostData.append("referencia", referencia);
			encodedPostData.append("monto", resumenOrigen.getImporteTotal());
			//encodedPostData.append("monto", "3");

			String post = encodedPostData.toString();
			
			VPagoWeb pagoWeb = new VPagoWeb("Pago 3D-Secure ETN");
			pagoWeb.execute(post);
			
			Screen screen = UiApplication.getUiApplication().getActiveScreen();
			UiApplication.getUiApplication().popScreen(screen);
			
		    UiApplication.getUiApplication().pushScreen(pagoWeb);
		    
			close();
		    
		} else if (field == cancelar){
			close();
		}
	}
}
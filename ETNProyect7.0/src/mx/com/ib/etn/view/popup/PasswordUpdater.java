package mx.com.ib.etn.view.popup;

import java.io.UnsupportedEncodingException;

import org.json.me.JSONObject;

import mx.com.ib.etn.model.connection.Url;
import mx.com.ib.etn.model.connection.addcel.implementation.UpdateUserPassThread;
import mx.com.ib.etn.utils.UtilSecurity;
import mx.com.ib.etn.utils.Utils;
import mx.com.ib.etn.view.controls.Viewable;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.PasswordEditField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class PasswordUpdater extends PopupScreen implements FieldChangeListener, Viewable{

	private LabelField lNombre;
	private EditField eUsuario;
	
	private LabelField lContrasena1;
	private PasswordEditField eContrasena1;

	private LabelField lContrasena2;
	private PasswordEditField eContrasena2;	

	private LabelField lContrasena3;
	private PasswordEditField eContrasena3;	
	
	private ButtonField aceptar;
	private ButtonField cancelar;

	public String url = Url.URL_UPDATE_PASS_MAIL;
	public String post = "";
	
	public PasswordUpdater(Viewable viewable){

		super(new VerticalFieldManager());
	
		
		lNombre = new LabelField("Nombre: ");
		eUsuario = new EditField("", "");
		
		lContrasena1 = new LabelField("Contraseņa actual: ");
		eContrasena1 = new PasswordEditField("", "");

		lContrasena2 = new LabelField("Nueva Contraseņa: ");
		eContrasena2 = new PasswordEditField("", "");
		
		lContrasena3 = new LabelField("Repite nueva Contraseņa: ");
		eContrasena3 = new PasswordEditField("", "");		
		
		aceptar = new ButtonField("Aceptar");
		cancelar = new ButtonField("Cancelar");
		
		HorizontalFieldManager hManager = new HorizontalFieldManager();
		hManager.add(aceptar);
		hManager.add(cancelar);
		
		aceptar.setChangeListener(this);
		cancelar.setChangeListener(this);
		
		add(lNombre);
		add(eUsuario);
		add(lContrasena1);
		add(eContrasena1);

		add(lContrasena2);
		add(eContrasena2);
		
		add(lContrasena3);
		add(eContrasena3);
		
		add(hManager);
		
		System.out.println("public PasswordUpdater(Viewable viewable){ END");
	}

	
    protected boolean keyChar(char c, int status, int time){

    	if (c == Characters.ESCAPE) {
             close();
    	}

        return super.keyChar(c, status, time);
   }


	public void fieldChanged(Field field, int context) {
		
		if (field == aceptar){
			getLogin();
		} else if (field == cancelar){
			close();
		}
	}

	
	private void getLogin(){

		String sContrasena1 = eContrasena1.getText(); 
		String sContrasena2 = eContrasena2.getText();
		String sContrasena3 = eContrasena3.getText();
		String sUsuario = eUsuario.getText();

		if ( checkString(eContrasena1.getText()) && 
			 checkString(eContrasena2.getText()) && 
			 checkString(eContrasena3.getText()) && 
			 checkString(eUsuario.getText()) && 
			 sContrasena2.equals(sContrasena3)
			){
			
			String json = securityPasswordJson(sUsuario, sContrasena1, sContrasena3);
			
			UpdateUserPassThread updateUserPassThread = new UpdateUserPassThread(null, Url.URL_UPDATE_PASS_MAIL, json, sContrasena3);
			updateUserPassThread.run();
			close();
		} else {
			Dialog.alert("Verificar usuario y contraseņas");
		}
	}
	
	
	private boolean checkString(String string){
		
		boolean value = false;
		
		if ((string != null)&&(string.length() >= 8) ){
			value = true;
		}
		
		return value;
	}
	

	
	public String securityPasswordJson(String login, String password, String newPassword){
		String pas = "";
		try {
			pas = UtilSecurity.aesEncrypt(UtilSecurity.parsePass(newPassword), updatePswd(login, password, newPassword));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		System.out.println(pas);
		
		String pass = Utils.mergeStr(pas, newPassword);
		System.out.println(pass);

		return "json=" + pass;
	}
	
	
	public String updatePswd(String login, String password, String newPassword) throws UnsupportedEncodingException {

		StringBuffer jsonPswd = new StringBuffer("{\"login\":\"");
		jsonPswd.append(login);
		jsonPswd.append("\",\"passwordS\":\""+ UtilSecurity.sha1(password));
		jsonPswd.append("\",\"password\":\"" + newPassword
				+ "\",\"newPassword\":\"");
		jsonPswd.append(newPassword + "\"}");
		
		System.out.println(jsonPswd.toString());
		
		return jsonPswd.toString();
	}


	public void setData(int request, JSONObject jsObject) {
		
		Dialog.alert(jsObject.toString());
		
	}


	public void sendMessage(String message) {
		Dialog.alert(message);
	}
}
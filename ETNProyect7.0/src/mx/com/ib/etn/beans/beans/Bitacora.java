package mx.com.ib.etn.beans.beans;

public class Bitacora {

	//URLEncodedPostData encodedPostData = new URLEncodedPostData(URLEncodedPostData.DEFAULT_CHARSET, false);
	//encodedPostData.append("user", login);
	
	private String idUsuario;
	private String monto;
	private String proveedor;
	private String producto;
	private String autorizacion;
	private String folioETN;
	private String codError;
	private String detError;
	private String estatus;
	private String detalle;
	
	private String imei;
	private String modelo;
	private String software;
	private String tipoProducto;
	private String wkey;

	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getProveedor() {
		return proveedor;
	}
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getAutorizacion() {
		return autorizacion;
	}
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}
	public String getFolioETN() {
		return folioETN;
	}
	public void setFolioETN(String folioETN) {
		this.folioETN = folioETN;
	}
	public String getCodError() {
		return codError;
	}
	public void setCodError(String codError) {
		this.codError = codError;
	}
	public String getDetError() {
		return detError;
	}
	public void setDetError(String detError) {
		this.detError = detError;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getSoftware() {
		return software;
	}
	public void setSoftware(String software) {
		this.software = software;
	}
	public String getTipoProducto() {
		return tipoProducto;
	}
	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}
	public String getWkey() {
		return wkey;
	}
	public void setWkey(String wkey) {
		this.wkey = wkey;
	}
}

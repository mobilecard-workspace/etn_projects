package mx.com.ib.etn.model.data;

import java.util.Date;

public class DataTravel {
	
	private Date dateDepart = null;
	private Date dateArrive = null;

	private String depart = null;
	private String arrive = null;

	private String idDepart = null;
	private String idArrive = null;

	private int iAdults = 0;
	private int iChildren = 0;
	private int iElderlies = 0;
	private int iStudents = 0;
	private int iProfessors = 0;

	private boolean isBasicFlight = false;
	
	
	
	public String getIdDepart() {
		return idDepart;
	}
	public void setIdDepart(String idDepart) {
		this.idDepart = idDepart;
	}
	public String getIdArrive() {
		return idArrive;
	}
	public void setIdArrive(String idArrive) {
		this.idArrive = idArrive;
	}
	
	
	public boolean isBasicFlight() {
		return isBasicFlight;
	}
	public void setBasicFlight(boolean isBasicFlight) {
		this.isBasicFlight = isBasicFlight;
	}
	public Date getDateDepart() {
		return dateDepart;
	}
	public void setDateDepart(Date dateDepart) {
		this.dateDepart = dateDepart;
	}
	public Date getDateArrive() {
		return dateArrive;
	}
	public void setDateArrive(Date dateArrive) {
		this.dateArrive = dateArrive;
	}

	public String getDepart() {
		return depart;
	}

	public String getArrive() {
		return arrive;
	}

	public void setArrive(String arrive) {
		this.arrive = arrive;
	}
	public void setDepart(String depart) {
		this.depart = depart;
	}

	public int getiAdults() {
		return iAdults;
	}
	public void setiAdults(int iAdults) {
		this.iAdults = iAdults;
	}
	public int getiChildren() {
		return iChildren;
	}
	public void setiChildren(int iChildren) {
		this.iChildren = iChildren;
	}
	public int getiElderlies() {
		return iElderlies;
	}
	public void setiElderlies(int iElderlies) {
		this.iElderlies = iElderlies;
	}
	public int getiStudents() {
		return iStudents;
	}
	public void setiStudents(int iStudents) {
		this.iStudents = iStudents;
	}
	public int getiProfessors() {
		return iProfessors;
	}
	public void setiProfessors(int iProfessors) {
		this.iProfessors = iProfessors;
	}
	
/*
	public String getDateDepartToString(){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dateDepart);

		int iYear = calendar.get(Calendar.YEAR);
		int iMonth = calendar.get(Calendar.MONTH);
		int iDay = calendar.get(Calendar.DAY_OF_MONTH);

		String sYear = Integer.toString(iYear);
		String sMonth = Integer.toString(iMonth + 1);
		String sDay = Integer.toString(iDay);

		sMonth = (sMonth.length() == 1) ? "0" + sMonth : sMonth;
		sDay = (sDay.length() == 1) ? "0" + sDay : sDay;
		
		return sDay + sMonth + sYear;
	}
*/
	
	
}

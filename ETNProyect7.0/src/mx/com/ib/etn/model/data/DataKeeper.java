package mx.com.ib.etn.model.data;

import java.util.Date;
import java.util.Vector;

import mx.com.ib.etn.beans.dto.Passenger;
import mx.com.ib.etn.beans.dto.Router;
import mx.com.ib.etn.beans.dto.Scheduled;
import mx.com.ib.etn.view.seats.GraphicButtonSeat;

public class DataKeeper {

	private DataTravel dataTravel = null;

	private Scheduled scheduledDepart = null; 
	private Scheduled scheduledArrive = null;
	private Router routerDepart = null; 
	private Router routerArrive = null;

	private GraphicButtonSeat seatsDepart[][];
	private GraphicButtonSeat seatsArrive[][];

	private GraphicButtonSeat seatsDepart1[][];
	private GraphicButtonSeat seatsDepart2[][];
	
	private GraphicButtonSeat seatsArrive1[][];
	private GraphicButtonSeat seatsArrive2[][];
	
	private Vector passengersDepart;
	private Vector passengersArrive;
	
	private StringBuffer sSeatDepart = null;
	private StringBuffer namesDepart = null;

	private StringBuffer sSeatArrive = null;
	private StringBuffer namesArrive = null;
	
	private static DataKeeper instance = null;

	private boolean isDoubleFloor = false;
	private boolean isFirstFloor = false;
	
	private DataKeeper() {

		dataTravel = new DataTravel();
	}

	public synchronized static DataKeeper getInstance() {
		if (instance == null) {
			synchronized (DataKeeper.class) {
				DataKeeper inst = instance;
				if (inst == null) {
					synchronized (DataKeeper.class) {
						instance = new DataKeeper();
					}
				}
			}
		}
		return instance;
	}

	public void setDataTravel(Date dateDepart, Date dateArrive, String depart,
			String arrive, String idDepart,
			String idArrive, int iAdults, int iChildren, int iElderlies,
			int iStudents, int iProfessors, boolean isBasicFlight) {

		dataTravel.setDateDepart(dateDepart);
		dataTravel.setDateArrive(dateArrive);
		dataTravel.setDepart(depart);
		dataTravel.setArrive(arrive);
		
		
		dataTravel.setIdDepart(idDepart);
		dataTravel.setIdArrive(idArrive);
		
		
		dataTravel.setiAdults(iAdults);
		dataTravel.setiChildren(iChildren);
		dataTravel.setiElderlies(iElderlies);
		dataTravel.setiStudents(iStudents);
		dataTravel.setiProfessors(iProfessors);
		dataTravel.setBasicFlight(isBasicFlight);
	}

	
	public DataTravel getDataTravel(){
		
		return dataTravel;
	}
	
	public Scheduled getScheduledDepart() {
		return scheduledDepart;
	}

	public void setScheduledDepart(Scheduled scheduledDepart) {
		this.scheduledDepart = scheduledDepart;
	}

	public Scheduled getScheduledArrive() {
		return scheduledArrive;
	}

	public void setScheduledArrive(Scheduled scheduledArrive) {
		this.scheduledArrive = scheduledArrive;
	}



	public String getStringSeatsDepart(){
		return sSeatDepart.toString();
	}
	
	public String getStringNamesDepart(){
		return namesDepart.toString();
	}
	
	
	
	public void createSeatsNamesDepart(){
		
		if (isDoubleFloor){
			createSeatsNamesDepartDoubleFlor();
		} else {
			createSeatsNamesDepartSimpleFloor();
		}
	}
	
	

	public void createSeatsNamesDepartSimpleFloor(){
		
		sSeatDepart = new StringBuffer();
		
		namesDepart = new StringBuffer();
		
		int count = 0;
		
		int i = seatsDepart.length;
		int j = seatsDepart[0].length;

		for (int a = 0; a < i; a++) {
			for (int b = 0; b < j; b++) {
				
				Passenger passenger2 = seatsDepart[a][b].getPassenger();
				
				if (passenger2 != null){
					
					String seat = seatsDepart[a][b].getSeat();
					
					if (seat.indexOf("T")>=0){

						int index = seat.indexOf("T");
						seat = seat.substring(0, index);
					}
					
					if (count == 0){
						namesDepart.append(passenger2.getName());
						sSeatDepart.append(seat);
						count++;
					} else {
						namesDepart.append(",").append(passenger2.getName());
						sSeatDepart.append("|").append(seat);
					}
				}
			}
		}
	}
	
	
	
	public void createSeatsNamesDepartDoubleFlor(){

		sSeatDepart = new StringBuffer();
		namesDepart = new StringBuffer();
			
		int count = 0;
		
		int i = seatsDepart1.length;
		int j = seatsDepart1[0].length;

		for (int a = 0; a < i; a++) {
			for (int b = 0; b < j; b++) {
				
				Passenger passenger2 = seatsDepart1[a][b].getPassenger();
				
				if (passenger2 != null){
					
					String seat = seatsDepart1[a][b].getSeat();
					
					if (seat.indexOf("T")>=0){

						int index = seat.indexOf("T");
						seat = seat.substring(0, index);
					}
					
					//sSeatDepart.append(seat);
					
					if (count == 0){
						namesDepart.append(passenger2.getName());
						sSeatDepart.append(seat);
						count++;
					} else {
						namesDepart.append(",").append(passenger2.getName());
						sSeatDepart.append("|").append(seat);
					}
				}
			}
		}
		
		
		count = 0;
		
		i = seatsDepart2.length;
		j = seatsDepart2[0].length;

		for (int a = 0; a < i; a++) {
			for (int b = 0; b < j; b++) {
				
				Passenger passenger2 = seatsDepart2[a][b].getPassenger();
				
				if (passenger2 != null){
					
					String seat = seatsDepart2[a][b].getSeat();
					
					if (seat.indexOf("T")>=0){

						int index = seat.indexOf("T");
						seat = seat.substring(0, index);
					}

					if (count == 0){
						namesDepart.append(passenger2.getName());
						sSeatDepart.append(seat);
						count++;
					} else {
						namesDepart.append(",").append(passenger2.getName());
						sSeatDepart.append("|").append(seat);
					}
				}
			}
		}
	}
	



	public String getStringSeatsArrive(){
		return sSeatDepart.toString();
	}
	
	public String getStringNamesArrive(){
		return namesDepart.toString();
	}

	public void createSeatsNamesArrive(){

		if (isDoubleFloor){
			createSeatsNamesArriveDoubleFloor();
		} else {
			createSeatsNamesArriveSimpleFloor();
		}

	}
	
	
	public void createSeatsNamesArriveSimpleFloor(){
		
		sSeatArrive = new StringBuffer();
		namesArrive = new StringBuffer();
		
		int count = 0;
		
		int i = seatsArrive.length;
		int j = seatsArrive[0].length;

		for (int a = 0; a < i; a++) {
			for (int b = 0; b < j; b++) {
				
				Passenger passenger2 = seatsArrive[a][b].getPassenger();
				
				if (passenger2 != null){
					
					String seat = seatsArrive[a][b].getSeat();
					
					if (seat.indexOf("T")>=0){
						
						int index = seat.indexOf("T");
						seat = seat.substring(0, index);
					}
					//sSeatArrive.append(seat);
					if (count == 0){
						namesArrive.append(passenger2.getName());
						sSeatArrive.append(seat);
						count++;
					} else {
						namesArrive.append(",").append(passenger2.getName());
						sSeatArrive.append("|").append(seat);
					}
				}
			}
		}
	}
	
	
	
	public void createSeatsNamesArriveDoubleFloor(){
		
		sSeatArrive = new StringBuffer();
		namesArrive = new StringBuffer();
		
		int count = 0;
		
		int i = seatsArrive1.length;
		int j = seatsArrive1[0].length;

		for (int a = 0; a < i; a++) {
			for (int b = 0; b < j; b++) {
				
				Passenger passenger2 = seatsArrive1[a][b].getPassenger();
				
				if (passenger2 != null){
					
					String seat = seatsArrive1[a][b].getSeat();
					
					if (seat.indexOf("T")>=0){
						
						int index = seat.indexOf("T");
						seat = seat.substring(0, index);
					}
					//sSeatArrive.append(seat);
					if (count == 0){
						namesArrive.append(passenger2.getName());
						sSeatArrive.append(seat);
						count++;
					} else {
						namesArrive.append(",").append(passenger2.getName());
						sSeatArrive.append("|").append(seat);
					}
				}
			}
		}
	
	
	
		count = 0;
		
		i = seatsArrive2.length;
		j = seatsArrive2[0].length;

		for (int a = 0; a < i; a++) {
			for (int b = 0; b < j; b++) {
				
				Passenger passenger2 = seatsArrive2[a][b].getPassenger();
				
				if (passenger2 != null){
					
					String seat = seatsArrive2[a][b].getSeat();
					
					if (seat.indexOf("T")>=0){
						
						int index = seat.indexOf("T");
						seat = seat.substring(0, index);
					}
					if (count == 0){
						namesArrive.append(passenger2.getName());
						sSeatArrive.append(seat);
						count++;
					} else {
						namesArrive.append(",").append(passenger2.getName());
						sSeatArrive.append("|").append(seat);
					}
				}
			}
		}
	}
	
	
	
	
	

	
	
	
	public Router getRouterArrive() {
		return routerArrive;
	}

	public void setRouterArrive(Router routerArrive) {
		this.routerArrive = routerArrive;
	}

	public Router getRouterDepart() {
		return routerDepart;
	}

	public void setRouterDepart(Router routerDepart) {
		this.routerDepart = routerDepart;
	}

	public Vector getPassengersArrive() {
		return passengersArrive;
	}

	public void setPassengersArrive(Vector passengersArrive) {
		this.passengersArrive = passengersArrive;
		/*
		if (passengersDepart != null){
			
			int aSize = passengersArrive.size();
			int dSize = passengersDepart.size();
			
			if (aSize == dSize){
				
				for(int index = 0; index < aSize; index++ ){
					
					Passenger aPassenger = (Passenger)passengersArrive.elementAt(index);
					Passenger dPassenger = (Passenger)passengersDepart.elementAt(index);
					
				}
			}
		}
		*/
	}

	public Vector getPassengersDepart() {
		return passengersDepart;
	}

	public void setPassengersDepart(Vector passengersDepart) {
		this.passengersDepart = passengersDepart;
	}

	public GraphicButtonSeat[][] getSeatsDepart() {
		return seatsDepart;
	}

	public void setSeatsDepart(GraphicButtonSeat[][] seatsDepart) {
		this.seatsDepart = seatsDepart;
	}

	public GraphicButtonSeat[][] getSeatsArrive() {
		return seatsArrive;
	}

	public void setSeatsArrive(GraphicButtonSeat[][] seatsArrive) {
		this.seatsArrive = seatsArrive;
	}

	public boolean isDoubleFloor() {
		return isDoubleFloor;
	}

	public void setDoubleFloor(boolean isDoubleFloor) {
		this.isDoubleFloor = isDoubleFloor;
	}
	
	public GraphicButtonSeat[][] getSeatsDepart1() {
		return seatsDepart1;
	}

	public void setSeatsDepart1(GraphicButtonSeat[][] seatsDepart1) {
		this.seatsDepart1 = seatsDepart1;
	}

	public GraphicButtonSeat[][] getSeatsArrive1() {
		return seatsArrive1;
	}

	public void setSeatsArrive1(GraphicButtonSeat[][] seatsArrive1) {
		this.seatsArrive1 = seatsArrive1;
	}

	public GraphicButtonSeat[][] getSeatsDepart2() {
		return seatsDepart2;
	}

	public void setSeatsDepart2(GraphicButtonSeat[][] seatsDepart2) {
		this.seatsDepart2 = seatsDepart2;
	}

	public GraphicButtonSeat[][] getSeatsArrive2() {
		return seatsArrive2;
	}

	public void setSeatsArrive2(GraphicButtonSeat[][] seatsArrive2) {
		this.seatsArrive2 = seatsArrive2;
	}

	public boolean isFirstFloor() {
		return isFirstFloor;
	}

	public void setFirstFloor(boolean isFirstFloor) {
		this.isFirstFloor = isFirstFloor;
	}
}

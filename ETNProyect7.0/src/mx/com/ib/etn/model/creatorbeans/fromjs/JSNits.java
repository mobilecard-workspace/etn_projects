package mx.com.ib.etn.model.creatorbeans.fromjs;

import java.util.Vector;

import mx.com.ib.etn.beans.beans.Boleto;
import mx.com.ib.etn.beans.beans.Nit;
import mx.com.ib.etn.beans.beans.ListaNits;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

public class JSNits {

	public ListaNits getVector(JSONObject jsObject) throws JSONException{
		
		JSONArray jsonNits = jsObject.getJSONArray("BoletosUsuario");

		Vector nits = new Vector();

		int length = jsonNits.length();

		ListaNits listaNits = new ListaNits();
		
		for (int iNits = 0; iNits < length; iNits++) {

			JSONObject jsonNit = jsonNits.getJSONObject(iNits);
			JSONArray jsonBoletos = jsonNit.getJSONArray("listaBoletos");
			int lengthBoletos = jsonBoletos.length();

			Nit nit = new Nit();
			nit.setNit(jsonNit.getString("nit"));

			
			for (int iBoletos = 0; iBoletos < length; iBoletos++) {

				JSONObject jsBoleto = jsonBoletos.getJSONObject(iBoletos);

				Boleto boleto = new Boleto();

				boleto.setOrigen(jsBoleto.getString("origen"));
				boleto.setDestino(jsBoleto.getString("destino"));
				boleto.setAsiento(jsBoleto.getString("asiento"));
				boleto.setTipoAsiento(jsBoleto.getString("tipoAsiento"));
				boleto.setImporte(jsBoleto.getString("importe"));
				boleto.setTipoViaje(jsBoleto.getString("tipoViaje"));
				boleto.setOperacion(jsBoleto.getString("operacion"));
				boleto.setFecha(jsBoleto.getString("fecha"));
				boleto.setHora(jsBoleto.getString("hora"));
				boleto.setAfiliacion(jsBoleto.getString("afiliacion"));
				boleto.setAutBanco(jsBoleto.getString("autBanco"));
				boleto.setNombre(jsBoleto.getString("nombre"));
				
				nit.addElement(boleto);
			}

			listaNits.addElement(nit);
		}

        return listaNits;
	}
	
/*
	public Vector getUnitVector(JSONObject jsObject) throws JSONException{
		
		JSONArray jsonNits = jsObject.getJSONArray("BoletosUsuario");

		Vector boletos = new Vector();

		int length = jsonNits.length();

		for (int iNits = 0; iNits < length; iNits++) {

			JSONObject jsonNit = jsonNits.getJSONObject(iNits);
			JSONArray jsonBoletos = jsonNit.getJSONArray("listaBoletos");
			int lengthBoletos = jsonBoletos.length();

			String nit = jsonNit.getString("nit");

			
			for (int iBoletos = 0; iBoletos < length; iBoletos++) {

				JSONObject jsBoleto = jsonBoletos.getJSONObject(iBoletos);

				Boleto boleto = new Boleto();

				boleto.setNit(nit);
				boleto.setOrigen(jsBoleto.getString("origen"));
				boleto.setDestino(jsBoleto.getString("destino"));
				boleto.setAsiento(jsBoleto.getString("asiento"));
				boleto.setTipoAsiento(jsBoleto.getString("tipoAsiento"));
				boleto.setImporte(jsBoleto.getString("importe"));
				boleto.setTipoViaje(jsBoleto.getString("tipoViaje"));
				boleto.setOperacion(jsBoleto.getString("operacion"));
				boleto.setFecha(jsBoleto.getString("fecha"));
				boleto.setHora(jsBoleto.getString("hora"));
				boleto.setAfiliacion(jsBoleto.getString("afiliacion"));
				boleto.setAutBanco(jsBoleto.getString("autBanco"));
				boleto.setNombre(jsBoleto.getString("nombre"));
				
				boletos.addElement(boleto);
			}
		}

        return boletos;
	}
*/
	
	public Vector getUnitVector(JSONObject jsObject) throws JSONException {

		JSONArray jsonNits = jsObject.getJSONArray("listaBoletos");

		Vector boletos = new Vector();

		int length = jsonNits.length();

		for (int iNits = 0; iNits < length; iNits++) {

			JSONObject jsBoleto = jsonNits.getJSONObject(iNits);

			Boleto boleto = new Boleto();

			
			boleto.setNit(jsBoleto.optString("nit", ""));
			boleto.setOrigen(jsBoleto.optString("origen", ""));
			boleto.setDestino(jsBoleto.optString("destino", ""));
			boleto.setAsiento(jsBoleto.optString("asiento", ""));
			boleto.setTipoAsiento(jsBoleto.optString("tipoAsiento", ""));
			boleto.setImporte(jsBoleto.optString("importe", ""));
			boleto.setTipoViaje(jsBoleto.optString("tipoViaje", ""));
			boleto.setOperacion(jsBoleto.optString("operacion", ""));
			boleto.setFecha(jsBoleto.optString("fecha", ""));
			boleto.setHora(jsBoleto.optString("hora", ""));
			boleto.setAfiliacion(jsBoleto.optString("afiliacion", ""));
			boleto.setAutBanco(jsBoleto.optString("autBanco", ""));
			boleto.setNombre(jsBoleto.optString("nombre", ""));

			boletos.addElement(boleto);
		}

		return boletos;
	}	
	
	public String verificarComponente(JSONObject object, String value) throws JSONException{
		
		String valor = "";
		
		if (object.has(value)){
			
			valor = object.getString(value);
		}
		
		return valor;
	}
	
}


/*				
Destino: TEQUISQUIAPAN
Salida: VIERNES 24/05/2013 09:30
Asiento: 4
Tipo Servicio: DE LUJO
Pasajero: PRUEBA
Tipo Pasajero: INSEN
Monto: $7.50
Forma de Pago: TB
Afiliacion: 3742228
*/			

package mx.com.ib.etn.model.creatorbeans.fromjs;

import java.util.Vector;

import mx.com.ib.etn.beans.beans.Boleto;
import mx.com.ib.etn.beans.dto.Scheduled;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

public class JSBoleto {

	
	public Vector getVector(JSONObject jsObject) throws JSONException{
		
		JSONArray jsonArray = jsObject.getJSONArray("listaBoletos");
		Boleto boleto = null;
		Vector boletos = new Vector();

		int length = jsonArray.length();

		for (int i = 0; i < length; i++) {

			JSONObject childJSONObject = jsonArray.getJSONObject(i);
			boleto = new Boleto();

			boleto.setOrigen(childJSONObject.getString("origen"));
			boleto.setDestino(childJSONObject.getString("destino"));
			boleto.setAsiento(childJSONObject.getString("asiento"));
			boleto.setTipoAsiento(childJSONObject.getString("tipoAsiento"));
			boleto.setImporte(childJSONObject.getString("importe"));
			boleto.setTipoViaje(childJSONObject.getString("tipoViaje"));
			boleto.setOperacion(childJSONObject.getString("operacion"));
			boleto.setFecha(childJSONObject.getString("fecha"));
			boleto.setHora(childJSONObject.getString("hora"));
			boleto.setAfiliacion(childJSONObject.getString("afiliacion"));
			boleto.setAutBanco(childJSONObject.getString("autBanco"));
			boleto.setNombre(childJSONObject.getString("nombre"));
/*				
			Destino: TEQUISQUIAPAN
			Salida: VIERNES 24/05/2013 09:30
			Asiento: 4
			Tipo Servicio: DE LUJO
			Pasajero: PRUEBA
			Tipo Pasajero: INSEN
			Monto: $7.50
			Forma de Pago: TB
			Afiliacion: 3742228
*/			
			boletos.addElement(boleto);

		}

        return boletos;
	}
	
	
}

package mx.com.ib.etn.model.creatorbeans.fromjs;

import java.util.Vector;

import mx.com.ib.etn.beans.dto.Router;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

public class JSRouterDepart {

	public Vector getVectorRouters(JSONObject jsObject, int select) throws JSONException{
		
		JSONArray jsonArray = jsObject.getJSONArray("lista");
		
		Router router = null;
		Vector routers = new Vector();
		
		int length = jsonArray.length();
		
		String sRouters[] = new String[length];
		
        for (int i = 0; i < length; i++) {
             JSONObject childJSONObject = jsonArray.getJSONObject(i);

             router = new Router();
             router.setKey(childJSONObject.getString("clave"));
             String description = childJSONObject.getString("descripcion"); 
             router.setDescription(description);
             sRouters[i] = description;
             routers.addElement(router);
        }

        return routers;
	}
	
	public String[] getArrayRouters(JSONObject jsObject, int select) throws JSONException{
		
		JSONArray jsonArray = jsObject.getJSONArray("lista");
		Router router = null;
		int length = jsonArray.length();
		String routers[] = new String[length];
		
        for (int i = 0; i < length; i++) {
             JSONObject childJSONObject = jsonArray.getJSONObject(i);

             String description = childJSONObject.getString("descripcion"); 
             routers[i] = description;
        }

        return routers;
	}
}

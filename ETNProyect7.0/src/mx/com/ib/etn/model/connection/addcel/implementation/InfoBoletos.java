package mx.com.ib.etn.model.connection.addcel.implementation;

import mx.com.ib.etn.view.controls.Viewable;

import org.json.me.JSONException;
import org.json.me.JSONObject;

public class InfoBoletos extends HttpListener {

	public InfoBoletos(String post, Viewable viewable, String url) {
		super(post, url, viewable);
	}

	
	public InfoBoletos(Viewable viewable, String url, String post) {
		super(viewable, url, post);
	}	

	
	public void receiveHttpResponse(int appCode, byte[] response) {

		JSONObject jsObject = null;
		String json = new String(response);
		System.out.println(json);
		try {
			
			jsObject = new JSONObject(json);
			sendData(jsObject);
		} catch (JSONException e) {
			e.printStackTrace();
			//sendMessageError("Error en lectura de JSON");
			sendMessageError(e.toString());
		}
	}
}


package mx.com.ib.etn.model.connection.addcel.implementation;

import mx.com.ib.etn.utils.UtilSecurity;
import mx.com.ib.etn.view.controls.Viewable;
import mx.com.ib.etn.view.popup.MessagePopupScreen;
import net.rim.device.api.system.Application;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;

import org.json.me.JSONException;
import org.json.me.JSONObject;



public class Terminos extends HttpListener {

	public Terminos(String post, String url, Viewable viewable) {
		super(viewable, url, post);
	}

	public void receiveHttpResponse(int appCode, byte[] response) {

		String sTemp = null;
		StringBuffer sb = new StringBuffer();

		sTemp = new String(response);

		String key = "1234567890ABCDEF0123456789ABCDEF";

		sTemp = UtilSecurity.aesDecrypt(key, sTemp);

		System.out.println(sTemp);

		UiApplication.getUiApplication();
		synchronized (Application.getEventLock()) {

			String acentos = remplazacadena(getTerms(sTemp));
			UiApplication.getUiApplication().pushScreen(
					new MessagePopupScreen(acentos, Field.FOCUSABLE));
		}
	}

	public String getTerms(String json) {

		String msj = "";

		try {

			JSONObject jsObject = new JSONObject(json);

			msj = jsObject.getString("Descripcion");

		} catch (JSONException e) {
			e.printStackTrace();
			msj = "Informaci�n no disponible.";
			return msj;
		}

		return msj;
	}

	public String reemplaza(String src, String orig, String nuevo) {
		if (src == null) {
			return null;
		}
		int tmp = 0;
		String srcnew = "";
		while (tmp >= 0) {
			tmp = src.indexOf(orig);
			if (tmp >= 0) {
				if (tmp > 0) {
					srcnew += src.substring(0, tmp);
				}
				srcnew += nuevo;
				src = src.substring(tmp + orig.length());
			}
		}
		srcnew += src;
		return srcnew;
	}

	public String remplazacadena(String conAcentos) {
		String caracteres[] = { "�?", "á", "É", "é", "�?", "í", "Ó", "ó",
				"Ú", "ú", "Ñ", "ñ" };
		String letras[] = { "�", "�", "�", "�", "�", "�", "�", "�", "�", "�",
				"�", "�" };
		for (int counter = 0; counter < caracteres.length; counter++) {
			conAcentos = reemplaza(conAcentos, caracteres[counter],
					letras[counter]);
		}
		return conAcentos;
	}

}
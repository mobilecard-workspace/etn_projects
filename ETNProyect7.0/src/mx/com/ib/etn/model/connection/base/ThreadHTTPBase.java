package mx.com.ib.etn.model.connection.base;

import java.io.IOException;
import java.util.Vector;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import mx.com.ib.etn.view.controls.Viewable;
import net.rim.blackberry.api.browser.URLEncodedPostData;
import net.rim.device.api.ui.UiApplication;

public class ThreadHTTPBase extends Thread {

	private HttpConfigurator httpConfigurator = null;
	private int request = 0;
	private Viewable viewable = null;
	private URLEncodedPostData encodedPostData = null;
	Vector urlEncoders;

	public ThreadHTTPBase(int request, Viewable viewable,
			URLEncodedPostData encodedPostData) {

		this.request = request;
		this.viewable = viewable;
		this.encodedPostData = encodedPostData;
	}

	public ThreadHTTPBase(Viewable viewable, Vector urlEncoders) {

		this.viewable = viewable;
		this.urlEncoders = urlEncoders;
	}

	public void run() {

		if (request > 0) {
			runSingle();
		} else {
			runArray();
		}
	}

	private void sendError(final String error) {

		UiApplication.getUiApplication().invokeLater(new Runnable() {
			public void run() {
				// controller.receiveAction(actionError);
				viewable.sendMessage(error);
			}
		});
	}

	public void runSingle() {

		SingletonHTTP singleton = SingletonHTTP.getInstance();
		singleton.setConfiguration(request, viewable, encodedPostData);

		try {

			singleton.execute();

		} catch (net.rim.device.api.xml.parsers.ParserConfigurationException e) {
			e.printStackTrace();
			sendError("Aviso: El analizador de xml tuvo un error.");
		} catch (IOException e) {
			e.printStackTrace();
			sendError("Aviso: Problemas con conexi�n al servidor");
		}
	}
	
	
	public void runArray(){

		int size = urlEncoders.size();

		final JSONObject jsObjects = new JSONObject();

		for (int index = 0; index < size; index++) {

			UrlEncode urlEncode = (UrlEncode) urlEncoders.elementAt(index);

			SingletonHTTP singleton = SingletonHTTP.getInstance();
			singleton.setConfiguration(urlEncode.getRequest(), viewable, urlEncode.getEncodedPostData());

			try {

				JSONObject jsObject = singleton.execute(urlEncode);
				jsObjects.put(String.valueOf(index), jsObject);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				sendError("Aviso: El analizador de json tuvo un error.");
			} catch (net.rim.device.api.xml.parsers.ParserConfigurationException e) {
				e.printStackTrace();
				sendError("Aviso: El analizador de xml tuvo un error.");
			} catch (IOException e) {
				e.printStackTrace();
				sendError("Aviso: Problemas con conexi�n al servidor");
			}
		}

		UiApplication.getUiApplication().invokeLater(new Runnable() {
			public void run() {
				// controller.receiveAction(actionError);
				viewable.setData(0, jsObjects);
			}
		});
	}
}
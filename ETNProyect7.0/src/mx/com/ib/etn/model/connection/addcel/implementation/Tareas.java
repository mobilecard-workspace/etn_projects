package mx.com.ib.etn.model.connection.addcel.implementation;

import mx.com.ib.etn.beans.beans.Bitacora;
import mx.com.ib.etn.beans.beans.UserBean;
import net.rim.blackberry.api.browser.URLEncodedPostData;


public class Tareas {

	
	public String toCreateBitacora(Bitacora bitacora){
		
		URLEncodedPostData encodedPostData = new URLEncodedPostData(URLEncodedPostData.DEFAULT_CHARSET, false);
		encodedPostData.append("idUsuario", bitacora.getIdUsuario());
		encodedPostData.append("monto", bitacora.getMonto());
		encodedPostData.append("proveedor", bitacora.getProveedor());
		encodedPostData.append("producto", bitacora.getProducto());
		encodedPostData.append("autorizacion", bitacora.getAutorizacion());
		encodedPostData.append("folioETN", bitacora.getFolioETN());
		encodedPostData.append("codError",bitacora.getCodError()); 
		encodedPostData.append("detError", bitacora.getDetError());
		encodedPostData.append("estatus", bitacora.getEstatus());
		encodedPostData.append("detalle", bitacora.getDetalle());

		//Par�metros de �quipo
		encodedPostData.append("imei", bitacora.getImei());
		encodedPostData.append("modelo", bitacora.getModelo());
		encodedPostData.append("software", bitacora.getSoftware());
		encodedPostData.append("tipoProducto", bitacora.getTipoProducto());
		encodedPostData.append("wkey", bitacora.getWkey());

		String post = encodedPostData.toString();
		
		return post;
	}
	
	
	public String toCreatePagoWeb(String user, long referencia, double monto){
		
		URLEncodedPostData encodedPostData = new URLEncodedPostData(URLEncodedPostData.DEFAULT_CHARSET, false);

		encodedPostData.append("user", UserBean.nameLogin);  
		encodedPostData.append("referencia", Long.toString(referencia));
		encodedPostData.append("monto", Double.toString(monto));

		String post = encodedPostData.toString();
		
		return post;
	}
	
	
	public String toCreatePagoWeb(String user, long referencia, String monto){
		
		URLEncodedPostData encodedPostData = new URLEncodedPostData(URLEncodedPostData.DEFAULT_CHARSET, false);

		encodedPostData.append("user", UserBean.nameLogin);  
		encodedPostData.append("referencia", Long.toString(referencia));
		encodedPostData.append("monto", monto);

		String post = encodedPostData.toString();
		
		return post;
	}
	
}

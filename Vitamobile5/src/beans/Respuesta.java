package beans;

public class Respuesta {
String id,mensaje;

public Respuesta() {
	super();
	// TODO Auto-generated constructor stub
}

public Respuesta(String id, String mensaje) {
	super();
	this.id = id;
	this.mensaje = mensaje;
}

public String getId() {
	return id;
}

public void setId(String id) {
	this.id = id;
}

public String getMensaje() {
	return mensaje;
}

public void setMensaje(String mensaje) {
	this.mensaje = mensaje;
}
}

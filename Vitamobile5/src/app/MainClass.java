package app;

import beans.Busqueda;
import beans.Especialidad;
import beans.Estado;
import beans.LoginBean;
import beans.Municipio;
import beans.Version;
import beans.member;
import threads.getVersionThread;
import mx.com.ib.connection.Communications;
import mx.com.ib.parser.JSONParser;
import mx.com.ib.splash.LoaderScreen;
import mypackage.SplashScreen;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.FontFamily;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;


public class MainClass extends UiApplication{
	
	public static MainClass mainClass = null;
	public final static int  bgTitleSectionField = 0xFFFFFF;
    public final static int  fgTitleSectionField = 0x000000;
//	URLS's
/*
	public static String URL_GET_CATEGORIES_MOBILE = "http://205.251.132.134:9090/getCategories.aspx";
	public static String URL_GET_NEWS_BY_SECTION = "http://205.251.132.134:9090/getNewsBySection.aspx";
	public static String URL_GET_RECENT = "http://205.251.132.134:9090/getRecent.aspx";
	public static String URL_GET_NEWS_DETAIL= "http://205.251.132.134:9090/getNewsDetailJava.aspx";
	public static String URL_GET_PUBLICITY= "http://205.251.132.134:9090/getPublicity.aspx";
	public static String URL_GET_BANNER_SPLASH= "http://205.251.132.134:9090/getBannerSplash.aspx";
	public static String URL_GET_SEARCH= "http://205.251.132.134:9090/getSearch.aspx";
	public static String URL_GET_COMMENTS= "http://205.251.132.134:9090/getComments.aspx";
	public static String URL_SET_COMMENT= "http://205.251.132.134:9090/setComment.aspx";
	
	public static String URL_GET_VERSION = "http://201.161.23.42:15000/getVersion.aspx";
	public static String URL_GET_MEMBER= "http://201.161.23.42:15000/membresia.aspx";
	public static String URL_GET_ESTADOS= "http://201.161.23.42:15000/getEstados.aspx";
	public static String URL_GET_MUN= "http://201.161.23.42:15000/getMunicipio.aspx";
	public static String URL_GET_PROVEEDOR= "http://201.161.23.42:15000/getEspecialidad.aspx";
	public static String URL_BUSQUEDA= "http://201.161.23.42:15000/Busqueda.aspx";
	public static String URL_CONSULTA= "http://201.161.23.42:15000/getConsulta.aspx";
	public static String URL_LOGIN= "http://201.161.23.42:15000/getLogin.aspx";
*/

	public static String URL_GET_VERSION = "http://mobilecard.mx:8080/Vitamedica/getVersion";
	public static String URL_GET_MEMBER= "http://mobilecard.mx:8080/Vitamedica/membresia";
	public static String URL_GET_ESTADOS= "http://mobilecard.mx:8080/Vitamedica/getEstados";
	public static String URL_GET_MUN= "http://mobilecard.mx:8080/Vitamedica/getMunicipio";
	public static String URL_GET_PROVEEDOR= "http://mobilecard.mx:8080/Vitamedica/getEspecialidad";
	public static String URL_BUSQUEDA= "http://mobilecard.mx:8080/Vitamedica/Busqueda";
	public static String URL_CONSULTA= "http://mobilecard.mx:8080/Vitamedica/getConsulta";
	public static String URL_LOGIN= "http://mobilecard.mx:8080/Vitamedica/getLogin";
	
    public static String PROMOS = "menu_promos.png";
    public static String PROMOS_OVER = "menu_promosblur.png";
    public static String PROV = "menu_proveedores.png";
    public static String PROV_OVER = "menu_proveedoresblur.png";
    public static String COM_USUARIOS = "menu_comusuarios.png";
    public static String COM_USUARIOS_OVER = "menu_comusuariosblur.png";

	
	public static member Membresia=null;
	public static LoginBean USR=null;
	public static String IDCOMRPA=null;
	public static member[] Benes=null;
	public static Estado[]edos=null;
	public static Municipio[]muni=null;
	public static Especialidad[]Especialidades=null;
	public static Busqueda[]busquedas=null;
	public static String DetalleProveedor=null;
	
	
	public static JSONParser jsParser = new JSONParser();	
	public static String GIF_LOADER = "preloader_b.gif";
//botones	
	public static String FACEBOOK="f.png";
	public static String TWITTER="t.png";
	
	//botones menu
	public static String CARD="card";
	public static String CARD_OVER="cardblur";
	public static String VITA="vita";
	public static String VITA_OVER="vitablur";
	public static String PORCENTAJE="porcentaje";
	public static String PORCENTAJE_OVER="porcentajeblur";
	public static String MOBILE="mobile";
	public static String MOBILE_OVER="mobileblur";
	public static String DIR="directorio";
	public static String DIR_OVER="directorioblur";	
	public static String SHARE="share";
	public static String SHARE_OVER="shareblur";	
	
	//botones que es
	public static String FUNCIONA="funciona";
	public static String FUNCIONA_OVER="funcionablur";
	public static String ATENCION="atencion";
	public static String ATENCION_OVER="atencionblur";
	public static String PREGUNTAS="preguntas";
	public static String PREGUNTAS_OVER="preguntasBlur";
	public static String BENEFICIOS="beneficios";
	public static String BENEFICIOS_OVER="beneficiosblur";
	
	public static String PAGINA="Pagina";
	public static String PAGINA_OVER="Paginablur";
	
	
	//invita
	public static String BTN_INVITAR = "btn_invita";
	public static String BTN_INVITAR_OVER = "btn_invita_ov";
	
//	colors	
	public static String BLACK="0x000000";	
	public static int BG_GRAY=0x414042;
			
// Fondos	
	public static String BG_SPLASH = "splash.png";
	public static String HEADER_MENU = "header_menu.png";
	public static String TEXT = "texto.png";
	public static String HEADER	 = "header.png";
	public static String SUB_HEADER	 = "subheader.png";
	public static Bitmap temp = Bitmap.getBitmapResource("tiempoaire.png");
	public static String BG_MENU= "menu.png";
	public static String FOOTER = "footer.png";
//	Font		
	public static FontFamily alphaSansFamily = null;
	public static Font appFont = null;
	public static FontFamily alphaSansFamily1 = null;
	public static Font appFont1 = null;
	public static Font appFont2 = null;
	public static String idealConnection = null;	
	public static LoaderScreen splashScreen = null;
	public static Bitmap bitmap = null;	
	public static String IdUser="";	
	public static String msg="";
	public static int zoom=14;
	public static String MODO_MAPA="staticmap";
	//public static String MODO_ESTANDAR="roadmap";
	//public static String MODO_ESTANDAR="staticmap";
	public static String[]SECCIONES={"","ACTUALES","ECOMOM�A Y NEGOCIOS","MUNDO PYME","MUJER EJECUTIVA","LIFESTYLE","RESPONSABILIDAD SOCIAL","POL�TICA Y SOCIEDAD","SALUD","MUNDO EXPRESS","","","","",""};
	
	
	public static void main(String args[]){
		
		
		Communications.start();
		mainClass = new MainClass();
		mainClass.enterEventDispatcher();
		
	}
	
	public MainClass(){		
        
        MainClass.splashScreen = new LoaderScreen();
		
		try {
			
			alphaSansFamily = FontFamily.forName("BBAlpha Serif");
			appFont = alphaSansFamily.getFont(Font.PLAIN, 11, Ui.UNITS_pt);
			alphaSansFamily1 = FontFamily.forName("BBAlpha Serif");
			appFont1 = alphaSansFamily.getFont(Font.PLAIN, 5, Ui.UNITS_pt);
			appFont2 = alphaSansFamily.getFont(Font.PLAIN, 6, Ui.UNITS_pt);
			
		} catch (ClassNotFoundException e) {
	}
		 
		initVariables();
		Version ver=null;
		pushScreen(new SplashScreen(ver));
		
		
		
		
		

	}
	
	public void initVariables(){
		int wid = Display.getWidth();
		int hei = Display.getHeight();
		
		System.out.println("Display "+wid+"x"+hei);
		int screen = 0;
		
		int sum = Math.abs(240 - wid) + Math.abs(260 - hei);
				
		int temp = Math.abs(320 - wid) + Math.abs(240 - hei);
		if(temp < sum){
			sum = temp;
			screen = 1;
		}
		
		temp = Math.abs(360 - wid) + Math.abs(480 - hei);
		if(temp < sum){
			sum = temp;
			screen = 2;
		}
		
		temp = Math.abs(480 - wid) + Math.abs(320 - hei);
		if(temp < sum){
			sum = temp;
			screen = 3;
		}
		
		temp = Math.abs(480 - wid) + Math.abs(360 - hei);
		if(temp < sum){
			sum = temp;
			screen = 4;
		}
		temp = Math.abs(640 - wid) + Math.abs(480 - hei);
		if(temp < sum){
			sum = temp;
			screen = 5;
		}
		
		//IMG_HEADER_LOGO = "logo-nuevo.png";
		
		String carpeta = "";
		String img = ".png";
		switch(screen){
			case 0:
				System.out.println("Se usa 240x260");
				carpeta += "240x260_";
				img = "_sn"+img;
				break;
			case 1:
				System.out.println("Se usa 320x240");
				carpeta += "320x240_";
				img = "_sn"+img;
				break;
			case 2:
				System.out.println("Se usa 360x480");
				carpeta += "360x480_";
				break;
			case 3:
				System.out.println("Se usa 480x320");
				carpeta += "480x320_";
				break;
			case 4:
				System.out.println("Se usa 480x360");
				carpeta += "480x360_";
				break;
			case 5:
				System.out.println("Se usa 640x480");
				carpeta += "640x480_";
				img = "b"+img;
				break;
		}
		
		BG_SPLASH = carpeta+BG_SPLASH;
		TEXT=carpeta+TEXT;
		HEADER_MENU=carpeta+HEADER_MENU;
		FOOTER=carpeta+FOOTER;
		HEADER = carpeta+HEADER;
		SUB_HEADER=carpeta+	SUB_HEADER;
		BG_MENU=carpeta+BG_MENU;
		DIR=DIR+img;
		DIR_OVER=DIR_OVER+img;
		CARD=CARD+img;
		CARD_OVER=CARD_OVER+img;
		VITA=VITA+img;
		VITA_OVER=VITA_OVER+img;
		PORCENTAJE=PORCENTAJE+img;
		PORCENTAJE_OVER=PORCENTAJE_OVER+img;
		MOBILE=MOBILE+img;
		MOBILE_OVER=MOBILE_OVER+img;
		SHARE=SHARE+img;
		SHARE_OVER=SHARE_OVER+img;
		BTN_INVITAR=BTN_INVITAR+img;
		BTN_INVITAR_OVER=BTN_INVITAR_OVER+img;
		

		FUNCIONA=FUNCIONA+img;
		FUNCIONA_OVER=FUNCIONA_OVER+img;
		ATENCION=ATENCION+img;
		ATENCION_OVER=ATENCION_OVER+img;
		PREGUNTAS=PREGUNTAS+img;
		PREGUNTAS_OVER=PREGUNTAS_OVER+img;
		BENEFICIOS=BENEFICIOS+img;
		BENEFICIOS_OVER=BENEFICIOS_OVER+img;
		PAGINA=PAGINA+img;
		PAGINA_OVER=PAGINA_OVER+img;
		
	
		
	}

}

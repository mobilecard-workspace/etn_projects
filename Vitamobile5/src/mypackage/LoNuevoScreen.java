package mypackage;

import app.MainClass;
import Controls.BannerBitmap;
import Controls.BgManager;
import Controls.GenericImageButtonField;
import mypackage.DirectorioScreen.logRespuesta;
import net.rim.device.api.system.Application;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class LoNuevoScreen extends MainScreen {
	public BgManager bgManager = null;
	public VerticalFieldManager hfmMain = null;
	public LabelField titulo,user= null;
	public GenericImageButtonField btnPromos, btnProv, btnAtencion;
	
	public LoNuevoScreen(){
		initVariables();
		addFields();
	}

	private void addFields() {
		// TODO Auto-generated method stub
		add(new NullField());
		setBanner(new BannerBitmap(MainClass.SUB_HEADER));
	
		
		HorizontalFieldManager hfmbtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		hfmMain.add(new LabelField());
		hfmMain.add(hfmbtn);
		hfmbtn.add(btnProv);
		
		hfmbtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		hfmMain.add(hfmbtn);
		hfmbtn.add(btnPromos);
		
		hfmbtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
		hfmMain.add(hfmbtn);
		hfmbtn.add(btnAtencion);
		
		bgManager.add(hfmMain);
		add(bgManager);
		
	}

	private void initVariables() {
		// TODO Auto-generated method stub
		TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	    
	    TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	    transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    
	    bgManager = new BgManager(MainClass.BG_MENU);
		   
	    
	    hfmMain = new VerticalFieldManager(Field.FIELD_HCENTER|VERTICAL_SCROLL|Field.USE_ALL_WIDTH);
	    
	    btnPromos = new GenericImageButtonField(MainClass.PROMOS_OVER, MainClass.PROMOS, "Promociones"){
	    	protected boolean navigationClick(int status, int time){
	    		UiApplication.getUiApplication().pushScreen(new PublicityScreen());
	    		return true;
	    	}
	    	
	    	
	    };
	    
	    btnProv = new GenericImageButtonField(MainClass.PROV_OVER, MainClass.PROV, "Proveedores"){
	    	protected boolean navigationClick(int status, int time){
	    		UiApplication.getUiApplication().pushScreen(new ProveedoresScreen());
	    		return true;
	    	}
	    };
	    
	    btnAtencion = new GenericImageButtonField(MainClass.COM_USUARIOS_OVER, 
	    		MainClass.COM_USUARIOS, "Atencion a usuarios"){
	    	protected boolean navigationClick(int status, int time) {
	    		synchronized (Application.getEventLock()) {
	    			UiApplication.getUiApplication().pushScreen(
		    				new MessagePopupScreen("Pensando en tu econom�a y bienestar pr�ximamente una nueva Cadena de Farmacias se unir� a la familia Membres�a Vitam�dica. Espera noticias.", 
		    						Field.NON_FOCUSABLE)
		    		);
	    		}
	    		
	    		return true;
	    	}
	    };
	}
	
}

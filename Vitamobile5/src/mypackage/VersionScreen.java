package mypackage;

import threads.threadBuyNowImages;
import app.MainClass;
import beans.CategoriesBean;
import beans.Version;
import Controls.BannerBitmap;
import Controls.BgManager;
import Controls.CustomVerticalFieldManager;
import Controls.ProductLabelField;
import net.rim.blackberry.api.browser.Browser;
import net.rim.blackberry.api.browser.BrowserSession;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.FlowFieldManager;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;


public class VersionScreen extends MainScreen{
	
	public BgManager bgManager = null;	
	public LabelField products = null;	
	public CustomVerticalFieldManager vfmFooter = null;	
	public Version categoria = null;
	public ProductLabelField[] productLabelFields = null;
	public FlowFieldManager grid;
	
	public VersionScreen(){
	

		initVariables();
		
		addFields();
		
		
	
	}
	
	public VersionScreen(Version productBeans){
		
		this.categoria = productBeans;
		

		
		initVariables();
	
		addFields();
		
		
	}
	
	/*public BuyScreen(CategoriesBean[] productBeans){
		
		this.bankBeans = productBeans;
		
		initVariables();
		
		addFields();
		
		products.setText(MainClass.textTitle);		
		
	}*/
	
	public void initVariables(){
	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	    
	    TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	    transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
	    
		bgManager = new BgManager(MainClass.BG_MENU);
		products = new LabelField("Cat�logo de Productos", Field.USE_ALL_WIDTH | LabelField.HCENTER);
		
		grid = new FlowFieldManager( Field.USE_ALL_WIDTH);		
		vfmFooter = new CustomVerticalFieldManager(Display.getWidth(),
				Display.getHeight(), 
				HorizontalFieldManager.FIELD_LEFT | VERTICAL_SCROLL);
		/*HorizontalFieldManager hfm = new HorizontalFieldManager(); 
		setBanner(hfm);*/
		
	}
	
	
public void addFields(){
		setBanner(new BannerBitmap(MainClass.SUB_HEADER));
		add(new NullField());
		
	//	vfmFooter.add(products);
		vfmFooter.add(new LabelField(""));
		vfmFooter.add(new LabelField("Existe una nueva versi�n de Vitamobile"));
		vfmFooter.add(new LabelField(""));
		if(categoria.getTipo().equals("2")){
		vfmFooter.add(new ButtonField("Ignorar"){
			protected boolean navigationClick(int status, int time) {
				
	            

				UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
				UiApplication.getUiApplication().pushScreen(new preLoginScreen());
			return true;
			}
			});
		}
		vfmFooter.add(new ButtonField("Descargar"){
			protected boolean navigationClick(int status, int time) {
				BrowserSession browserSession; 
    			browserSession = Browser.getDefaultSession();   			
    			browserSession.displayPage(categoria.getUrl());	
    			System.exit(0);
            
            		
            
			return true;
			}
			
		});
		
		bgManager.add(vfmFooter);
		//setStatus(new BannerBitmap());
		add(bgManager);
		
	}
public boolean onSavePrompt(){
	return true;
}

}
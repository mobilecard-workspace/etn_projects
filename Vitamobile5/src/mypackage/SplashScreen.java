package mypackage;

import java.util.Timer;
import java.util.TimerTask;

import beans.Version;


import threads.getVersionThread;


import net.rim.blackberry.api.browser.Browser;
import net.rim.blackberry.api.browser.BrowserSession;
import net.rim.device.api.system.ApplicationDescriptor;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.KeyListener;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class SplashScreen extends MainScreen 
{   	
	VerticalFieldManager _fieldManagerMiddle;
	
	BitmapField _bitmap;
	Bitmap _DAFLogo;
	Version version;
	private Timer timer = new Timer();      
	
	public SplashScreen(Version ver)
	{     
		super();      
		this.version=ver;
		_fieldManagerMiddle = new VerticalFieldManager(Manager.VERTICAL_SCROLL | Field.USE_ALL_WIDTH | Field.FIELD_HCENTER);
		BitmapField bField = new BitmapField(Bitmap.getBitmapResource(app.MainClass.BG_SPLASH));
	//	_DAFLogo = Bitmap.getBitmapResource("daf-logo.gif");
		//_bitmap = new BitmapField(_DAFLogo);
		//_bitmap.setSpace(Graphics.getScreenWidth()/2 - _DAFLogo.getWidth()/2, Graphics.getScreenHeight()/2 - _DAFLogo.getHeight()/2);
		
		_fieldManagerMiddle.add(bField);
		
		add(_fieldManagerMiddle);
		
		SplashScreenListener listener = new SplashScreenListener(this);      
		
		this.addKeyListener((KeyListener) listener);      
		timer.schedule(new CountDown(), 1000);      
		//UiApplication.getUiApplication().pushScreen(this); 
	}   
	
	public void dismiss() 
	
	{
		
		timer.cancel();
		String version="idApp=1&idPlataforma=3";
		new getVersionThread(version).start();
	
	}   
	
	private class CountDown extends TimerTask 
	{      
		public void run() 
		{         
			DismissThread dThread = new DismissThread();         
			UiApplication.getUiApplication().invokeLater(dThread);
		}   
	}   
	
	private class DismissThread implements Runnable 
	{      
		public void run() 
		{         
			dismiss();      
		}   
	}   
	
	protected boolean navigationClick(int status, int time) 
	{     
		dismiss();      
		return true;   
	}   
	
	protected boolean navigationUnclick(int status, int time) 
	{      
		return false;   
	}   
	
	protected boolean navigationMovement(int dx, int dy, int status, int time) 
	{      
		return false;   
	}   
	
	public static class SplashScreenListener implements KeyListener 
	{      
		private SplashScreen screen;    
		
		public boolean keyChar(char key, int status, int time) 
		{         
			//intercept the ESC and MENU key - exit the splash screen         
			boolean retval = false;
			
			switch (key) 
			{            
				case Characters.CONTROL_MENU:    System.exit(0);        
				case Characters.ESCAPE:            
					//screen.dismiss();  
					 System.exit(0);
					retval = true;            
					break;         
			}         	
			return retval;      
		}     

		public boolean keyDown(int keycode, int time) 
		{         
			return false;      
		}      
		
		public boolean keyRepeat(int keycode, int time) 
		{         
			return false;      
		}      
		
		public boolean keyStatus(int keycode, int time) 
		{         
			return false;      
		}      
		
		public boolean keyUp(int keycode, int time) 
		{         
			return false;      
		}      
		
		public SplashScreenListener(SplashScreen splash) 
		{         
			screen = splash;      
		}   
	}
}
package mypackage;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;

public class TitleField extends Field {
        
        private Bitmap bmp;
        private int bgColor = 0x007934;
        private String title;
        
        public TitleField(Bitmap bmp, long style, int bgColor){
                super(style);
                this.bmp = bmp;
                this.bgColor = bgColor;
        }
        
        public TitleField(Bitmap bmp, long style){
                super(style);
                this.bmp = bmp;
        }
        
        public TitleField(Bitmap bmp, String title, long style){
                super(style);
                this.bmp = bmp;
                this.title = title;
        }
    
        
        public void paint(Graphics g) {
                int color = g.getColor();
        g.setBackgroundColor(bgColor);
        g.setColor(bgColor);
        g.clear();
        //g.fillRect(g.getClippingRect().x, g.getClippingRect().y, g.getClippingRect().width, g.getClippingRect().height);
        if(title != null) {
            int titleWidth = g.getFont().getAdvance(title);
            int paddingLeft = (g.getClippingRect().width - titleWidth - bmp.getWidth())/2 - 6;
            g.drawBitmap(paddingLeft, 0, bmp.getWidth(), bmp.getHeight(), bmp, 0, 0);
            //int paddingTop = (bmp.getHeight() - g.getFont().getHeight())/2;
            g.setColor(0xFFFFFF);
            g.drawText(title, paddingLeft + bmp.getWidth() + 6, 0);
        } else {
            g.drawBitmap((g.getClippingRect().width-bmp.getWidth())/2, 0, bmp.getWidth(), bmp.getHeight(), bmp, 0, 0);
        }
        //super.paint(g);
        g.setColor(color);
    }
        
        public int getPreferredHeight() {
                if(bmp != null) {
                        return bmp.getHeight();
                } else {
                        return super.getPreferredHeight();
                }
        }
        protected void layout(int width, int height) {
                setExtent(width, getPreferredHeight());
                
        }
        
        

}
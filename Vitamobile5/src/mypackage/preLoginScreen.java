package mypackage;

import net.rim.blackberry.api.browser.Browser;
import net.rim.blackberry.api.browser.BrowserSession;
import net.rim.device.api.system.Application;
import net.rim.device.api.system.ApplicationDescriptor;
import net.rim.device.api.system.ApplicationManager;
import net.rim.device.api.system.ApplicationManagerException;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.CodeModuleManager;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import Controls.BgManager;
import Controls.CustomVerticalFieldManager;
import Controls.GenericImageButtonField;
import app.MainClass;

public class preLoginScreen extends MainScreen{
	
	public LabelField Message,siya = null;
	
	public VerticalFieldManager hfmMain = null;
	public HorizontalFieldManager hfmTitle = null;
	
	public HorizontalFieldManager hfmBtns1 = null;
	public HorizontalFieldManager hfmBtns2 = null;
	public HorizontalFieldManager hfmBtns3 = null;
	public HorizontalFieldManager hfmBtns4 = null;
	public VerticalFieldManager  hfmBtns5 = null;
	public HorizontalFieldManager hfmBtns6 = null;
	
	public CustomVerticalFieldManager customVerticalFieldManager = null;
	public CustomVerticalFieldManager customVerticalFieldManager2 = null;
	public GenericImageButtonField btnBeneficios = null;
	public GenericImageButtonField btnFunciona = null;
	public GenericImageButtonField btnPreguntas = null;
	public GenericImageButtonField btnAtencion = null;

	
	public BgManager bgManager = null;
	public Bitmap bField=null;
	public Bitmap bField2=null;
	
	
	public HorizontalFieldManager hfmbtn = null;
	public VerticalFieldManager vfmFooter = null;
	
	public preLoginScreen(){
		
		initVariables();
		initButtons();
		addFields();
		
		
	}
	
	public void initVariables(){
	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	    
	    TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	    transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    engine.setAcceptableDirections(Display.DIRECTION_NORTH);
		bgManager = new BgManager(MainClass.BG_MENU);

		hfmMain = new VerticalFieldManager(Field.FIELD_HCENTER|VERTICAL_SCROLL|Field.USE_ALL_WIDTH);
		hfmTitle = new HorizontalFieldManager(Field.FIELD_RIGHT);
		vfmFooter = new VerticalFieldManager(VERTICAL_SCROLL);
		
		customVerticalFieldManager = new CustomVerticalFieldManager(Display.getWidth(),Display.getHeight()/2, HorizontalFieldManager.USE_ALL_WIDTH|Field.FIELD_VCENTER);
		customVerticalFieldManager2 = new CustomVerticalFieldManager(Display.getWidth(),Display.getHeight()/3, HorizontalFieldManager.USE_ALL_WIDTH|Field.FIELD_VCENTER);
		hfmBtns1 = new HorizontalFieldManager(Field.FIELD_HCENTER){
			//define width
           
			
		};
		hfmBtns2 = new HorizontalFieldManager(Field.FIELD_HCENTER);
		hfmBtns3 = new HorizontalFieldManager(HorizontalFieldManager.FIELD_RIGHT){
			
			 public int getPreferredWidth()
	            {
	                return Display.getWidth();
	            }
	      
	          /*  protected void sublayout( int maxWidth, int maxHeight )
	            {
	                super.sublayout(getPreferredWidth(), 
	                                getPreferredHeight());
	                setExtent(getPreferredWidth(), getPreferredHeight());
	            }*/
		};
		hfmBtns4 = new  HorizontalFieldManager(Field.FIELD_RIGHT);
		hfmBtns6 = new  HorizontalFieldManager(Field.FIELD_HCENTER);
		   hfmbtn = new HorizontalFieldManager(Field.FIELD_RIGHT);
		bField =  Bitmap.getBitmapResource(MainClass.HEADER_MENU);
		bField2=Bitmap.getBitmapResource(MainClass.TEXT);
		//bField2 =  Bitmap.getBitmapResource(MainClass.BTN_INVITA);
		hfmBtns5 = new  VerticalFieldManager(Field.FIELD_HCENTER);
		
		siya=new LabelField("Si ya eres Usuario",Field.FIELD_RIGHT){
			public void paint(Graphics graphics){				
				int g = graphics.getColor();
				graphics.setColor(0x3333FF);
				super.paint(graphics);
			}
			protected void onFocus(int direction) {  
		        super.onFocus(direction);
		        invalidate(); 
		     //   
		    }   
		 
			 protected void onUnfocus() {  
		           super.onUnfocus();
		           invalidate();  
		    }
			
			
		};
		siya.setFont(MainClass.appFont1);
		
		Message = new LabelField("Membres�a Vitam�dica ofrece asistencia m�dica telef�nica gratuita, precios preferenciales con m�dicos privados de diferentes especialidades y descuentos exclusivos en farmacias y otros servicios m�dicos.",Field.FIELD_HCENTER){			
			public void paint(Graphics graphics){				
				int g = graphics.getColor();
				graphics.setColor(MainClass.BG_GRAY);
				super.paint(graphics);
			}
			protected void onFocus(int direction) {  
		        super.onFocus(direction);
		        invalidate(); 
		     //   
		    }   
		 
			 protected void onUnfocus() {  
		           super.onUnfocus();
		           invalidate();  
		    } 
		};
		Message.setFont(MainClass.appFont1);
		
	}
	
	public void addFields(){	
	//	setBanner(new BannerBitmap(MainClass.HEADER));
		add(new NullField());
	
		BitmapField b= new BitmapField(bField);
		hfmBtns3.add(b);
		hfmBtns1.add(btnBeneficios);
		hfmBtns1.add(btnFunciona);
		
	
		
		//margen 
	
	/*	btndirectorio.setMargin(Display.getHeight()/6, 0, 0, 0);
		btnvita.setMargin(Display.getHeight()/6, 0, 0, 0);
		btnporcentaje.setMargin(Display.getHeight()/6, 0, 0, 0);
		
		if(Display.getHeight()==480){
			btndirectorio.setMargin(Display.getHeight()/10, 0, 0, 0);
			btnvita.setMargin(Display.getHeight()/10, 0, 0, 0);
			btnporcentaje.setMargin(Display.getHeight()/10, 0, 0, 0);
		}*/
		
	
		hfmMain.add(hfmBtns3);
		
		
		hfmMain.add(hfmBtns6);
		
	//	hfmMain.add(Message);
	
		hfmMain.add(hfmBtns1);	
		
		
		//parte de abajo
		hfmBtns2.add(btnPreguntas);
		hfmBtns2.add(btnAtencion);
	
		hfmBtns2.setPadding(Display.getHeight()/30, 0, 0, 0);
		hfmMain.add(hfmBtns2);	
		hfmMain.add(new LabelField());
		
		hfmMain.add(siya);
		hfmbtn.add(new ButtonField("Iniciar Sesi�n"){
			protected boolean navigationClick(int status, int time) {
					
				UiApplication.getUiApplication().pushScreen(new LoginScreen());
				return true;
				}
				
			});
		hfmMain.add(hfmbtn);
		
		
		
		
		bgManager.add(hfmMain);
		
		add(bgManager);
		
	}
	
	public void initButtons(){
		
		btnBeneficios = new GenericImageButtonField(MainClass.VITA_OVER,MainClass.VITA,"comprar"){
			
			protected boolean navigationClick(int status, int time) {
				
				synchronized (Application.getEventLock()) {
	    			UiApplication.getUiApplication().pushScreen(
		    				new MessagePopupScreen("Membres�a Vitam�dica ofrece asistencia m�dica telef�nica gratuita, precios preferenciales con m�dicos privados de diferentes especialidades y descuentos exclusivos en farmacias y otros servicios m�dicos.", 
		    						Field.NON_FOCUSABLE)
		    		);
	    		}
				
				
 
				return true;
			}
			
		};
		btnPreguntas = new GenericImageButtonField(MainClass.PAGINA_OVER,MainClass.PAGINA,"comprar"){
			
			protected boolean navigationClick(int status, int time) {
				
				BrowserSession browserSession; 
    			browserSession = Browser.getDefaultSession();   			
    			browserSession.displayPage("https://www.membresiavitamedica.com.mx/");	
    			System.exit(0);
			
				
				return true;
			}
			
		}; 
		
		btnAtencion = new GenericImageButtonField(MainClass.MOBILE_OVER,MainClass.MOBILE,"comprar"){
			
			protected boolean navigationClick(int status, int time) {
				BrowserSession browserSession; 
    			browserSession = Browser.getDefaultSession();   			
    			browserSession.displayPage("http://appworld.blackberry.com/webstore/content/129830/?countrycode=MX");	
    			System.exit(0);
				return true;
			}
			
		}; 
		btnFunciona = new GenericImageButtonField(MainClass.BENEFICIOS_OVER,MainClass.BENEFICIOS,"comprar"){
			
			protected boolean navigationClick(int status, int time) {
				UiApplication.getUiApplication().pushScreen(new VitaScreen("http://mobilecard.mx:8080/Vitamedica/beneficiosi.htm"));
				return true;
			}
			
		}; 
		}
	
	
	

	
	protected boolean onSavePrompt() {
		return true;
	}
	
	public void close()
    {
	System.exit(0);
	
    }
	
	protected boolean keyDown(int keycode, int time)
	   {

	        if (Keypad.key(keycode) == Characters.ESCAPE) 
	        {
	        	close();
	     
	        	return true;
	        }
	        return false;
	   }

}
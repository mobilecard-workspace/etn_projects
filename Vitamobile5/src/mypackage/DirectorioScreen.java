package mypackage;

import mx.com.ib.connection.Respuesta;
import mx.com.ib.utils.Utils;
import net.rim.device.api.system.Application;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.ActiveRichTextField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import threads.BusquedaThread;
import threads.EspecialidadThread;
import threads.MunicipiosThread;

import Controls.BannerBitmap;
import Controls.BgManager;
import Controls.BorderBasicEditField;
import Controls.GenericImageButtonField;
import Controls.ProductLabelField;
import app.MainClass;
import beans.Especialidad;
import beans.Estado;
import beans.Municipio;

public class DirectorioScreen extends MainScreen{
	public BgManager bgManager = null;
	public VerticalFieldManager hfmMain = null;
	public HorizontalFieldManager hfmbtn = null;
	public HorizontalFieldManager hfmtitle = null;
	public LabelField titulo,nameFriend,email= null;
	public BorderBasicEditField basicMyName,basicnameFriend, basicemail=null;
	public int widthManager = 0;
	public GenericImageButtonField btnOk = null;
	public ActiveRichTextField note=null;
	public ActiveRichTextField note2=null;
	private int WAIT_TIME = 500000000;
	Estado[] estado=null;
	Municipio[] mun=null;
	Especialidad[] esp=null;
	String idEstado="";
	String idMunicipio="";
	String idEspecialidad="";
	public ObjectChoiceField choicesEDO,choicesMun,choicesEs=null;
	String[]arraymun= new String[1];
	String[]arrayes= new String[1];
	String[]arrayinit= new String[1];
	public DirectorioScreen(){
		initVariables();
		addFields();
	}
	public void initVariables(){
	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	    
	    TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	    transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
	    widthManager = Display.getWidth();		
		//bgManager = new BgManager(MainClass.BG_CUENTA);
		bgManager = new BgManager(MainClass.BG_MENU);
	   
		logRespuesta res=new logRespuesta();
	    
	    hfmMain = new VerticalFieldManager(Field.FIELD_HCENTER|VERTICAL_SCROLL|Field.USE_ALL_WIDTH);
	    hfmbtn = new HorizontalFieldManager(Field.FIELD_HCENTER);
	    hfmtitle = new HorizontalFieldManager(Field.FIELD_HCENTER);
	    
	    
	    titulo= new LabelField("Filtre la informaci�n para realizar su b�squeda"){
			public void paint(Graphics g){
				
				
				g.setColor(0x000000);
				super.paint(g);
			}
		};
		estado=MainClass.edos;
		String[]array= new String[estado.length+1];
		array[0]="seleccione";
		arrayinit[0]="seleccione";
		for(int x=1;x<array.length;x++){
			array[x]=estado[x-1].getNombre();
		}
		
		
			
		
		choicesEDO  = new ObjectChoiceField("Estado: ", array){
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				super.paint(graphics);
				graphics.setColor(MainClass.BG_GRAY);
				super.paint(graphics);
				invalidate();
			}
			
			protected void drawFocus(Graphics graphics, boolean on) {
				// TODO Auto-generated method stub
				super.drawFocus(graphics, on);
				invalidate();
			}
			protected void fieldChangeNotify(int context)
            {
                   if(context==2&&choicesEDO.getSelectedIndex()!=0){
                	
                	  /* String val = choicesEDO.getChoice(choicesEDO.getSelectedIndex()).toString();
                	   choicesEDO.getChoice(choicesEDO.getSelectedIndex());
	                    System.out.println("Selected: " + val);
	                    int index = val.indexOf("-");
	                    String des = val.substring(0, index);
	                    System.out.println("destinatariosend: " + des);
	                	
	                	idEstado=des;
	                    new MunicipiosThread("id="+des,res).start();*/
                	   
                	int xxx= choicesEDO.getSelectedIndex();
                	String des=MainClass.edos[xxx-1].getId();
                	logRespuesta res=new logRespuesta();
                	idEstado=des;
                	 new MunicipiosThread("id="+des,res).start();
                   }
            }
			

		};
		
		//mun
		
	
		arraymun[0]="seleccione";
	
		
		
		choicesMun = new ObjectChoiceField("Ciudad/Delegaci�n:", arraymun){
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				super.paint(graphics);
				graphics.setColor(MainClass.BG_GRAY);
				super.paint(graphics);
				invalidate();
			}
			
			protected void drawFocus(Graphics graphics, boolean on) {
				// TODO Auto-generated method stub
				super.drawFocus(graphics, on);
				invalidate();
			}
			protected void fieldChangeNotify(int context)
            {
                   if(context==2&&choicesMun.getSelectedIndex()!=0){
                	   choicesEs.setChoices(arrayes);
                	   //idEstado=8&idMunicipio=2
                	   String val = choicesMun.getChoice(choicesMun.getSelectedIndex()).toString();
	                  /*  System.out.println("Selected: " + val);
	                    int index = val.indexOf("-");
	                    String des = val.substring(0, index);
	                    System.out.println("destinatariosend: " + des);
	                */
                		logRespuesta res=new logRespuesta();
	                	idMunicipio=val;
	                //	Dialog.alert(idMunicipio);
	                    new EspecialidadThread("idMunicipio="+Utils.QuitaEspacios(idMunicipio)+"&idEstado="+idEstado,res).start();
                	 /*  String val = choicesEDO.getChoice(choicesEDO.getSelectedIndex()).toString();
	                    System.out.println("Selected: " + val);
	                    int index = val.indexOf("-");
	                    String des = val.substring(0, index);
	                    System.out.println("destinatariosend: " + des);*/
	             
	       
                   }
            }
			

		};
		
		arrayes[0]="seleccione";
		
		
		choicesEs = new ObjectChoiceField("Especialidad: ", arrayes){
			protected void paint(Graphics graphics) {
				// TODO Auto-generated method stub
				super.paint(graphics);
				graphics.setColor(MainClass.BG_GRAY);
				super.paint(graphics);
				invalidate();
			}
			
			protected void drawFocus(Graphics graphics, boolean on) {
				// TODO Auto-generated method stub
				super.drawFocus(graphics, on);
				invalidate();
			}
			protected void fieldChangeNotify(int context)
            {
                   if(context==2&&choicesEs.getSelectedIndex()!=0){
                	  
                	  
	                    idEspecialidad= choicesEs.getChoice(choicesEs.getSelectedIndex()).toString();
	                    idEspecialidad=Utils.QuitaEspacios(idEspecialidad);
	                   // choicesEs.getChoice(choicesEs.getSelectedIndex()).toString();
	       
                   }
            }
			

		};
	    
	    
	    btnOk =  new GenericImageButtonField(MainClass.BTN_INVITAR_OVER,MainClass.BTN_INVITAR,"ok"){
			
		
			
		};		
	}
	public void addFields(){
	//	setBanner(new BannerBitmap(MainClass.BG_ORANGE, MainClass.HD_INVITA));
		add(new NullField());
		setBanner(new BannerBitmap(MainClass.SUB_HEADER));
		add(new NullField());
		hfmtitle.add(titulo);
		hfmMain.add(hfmtitle);
		hfmMain.add(choicesEDO);
		hfmMain.add(choicesMun);
		hfmMain.add(choicesEs);
		hfmMain.add(new LabelField(""));
		hfmbtn.add(new ButtonField("Buscar"){
			protected boolean navigationClick(int status, int time) {
					
			/*	String idEstado="";
				String idMunicipio="";
				String idEspecialidad="";*/
				if(choicesEDO.getSelectedIndex()==0){
					Dialog.alert("No has seleccionado un Estado");
					
				}else if(choicesMun.getSelectedIndex()==0){
					Dialog.alert("No has seleccionado Ciudad/Delegaci�n");	
				}else if(choicesEs.getSelectedIndex()==0){
					Dialog.alert("No has seleccionado una Especialidad");	
				}
				else{
					
					System.out.println("idMunicipio="+idMunicipio+"&idEstado="+idEstado+"&idEspecialidad="+idEspecialidad);
					  new BusquedaThread("idMunicipio="+idMunicipio+"&idEstado="+idEstado+"&idEspecialidad="+idEspecialidad).start();
				}
				
	            
				return true;
				}
				
			});
		hfmMain.add(hfmbtn);
		
		
			bgManager.add(hfmMain);
		add(bgManager);
	}
	public boolean isEmail(String correo){
		boolean escorreo=true;
		if(correo.indexOf("@")==-1){
			escorreo=false;
		}
		if(correo.indexOf(".")==-1){
			escorreo=false;
		}
		
		return escorreo;
		
	}
	public void MakeInvitation(){
		
		MainClass.splashScreen.start();
	
	//	setLecturaN();
		
	}
	public void close() {
		// TODO Auto-generated method stub
		UiApplication.getUiApplication().popScreen(this);
	}
	protected boolean onSavePrompt() 
	{
		return true;
	}

	
	
class logRespuesta implements Respuesta{

		

		public String Respuestas(String response) {
			// TODO Auto-generated method stub
			   
			UiApplication.getUiApplication();
			synchronized (Application.getEventLock()) {
				Municipio[] categorias=null;
				MainClass.splashScreen.remove();
				categorias=MainClass.jsParser.GET_MUN(response);
				MainClass.muni=categorias;
				
				if(categorias!=null){
				if(categorias.length>0){
					mun=MainClass.muni;
					arraymun= new String[mun.length+1];
					arraymun[0]="seleccione";
					for(int x=0;x<mun.length;x++){
					//	arraymun[x]=mun[x-1].getId()+"-"+mun[x-1].getNombre();
						arraymun[x+1]=Utils.remplazacadena(mun[x].getNombre());
						System.out.println(mun[x].getNombre());
						
					}
				
					choicesMun.setChoices(arraymun);
					logRespuesta res=new logRespuesta();
					if(choicesMun.getSelectedIndex()!=0){
						 new EspecialidadThread("idEstado="+idEstado,res).start();	
					}
			
					//UiApplication.getUiApplication().pushScreen(new DirectorioScreen());
				
					
				}else{
					choicesMun.setChoices(arrayinit);
					choicesEs.setChoices(arrayinit);	
				}
				}
					
				else{
					UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Intente m�s tarde", Field.NON_FOCUSABLE));	
				}
				
				
			}
			
			return null;
		}

		public String RespuestasE(String response) {
			  
			UiApplication.getUiApplication();
			synchronized (Application.getEventLock()) {
				Especialidad[] categorias=null;
				MainClass.splashScreen.remove();
				categorias=MainClass.jsParser.GET_ESP(response);
				MainClass.Especialidades=categorias;
				
				if(categorias!=null){
				if(categorias.length>0){
					esp=MainClass.Especialidades;
					arrayes= new String[esp.length+1];
					arrayes[0]="seleccione";
					for(int x=0;x<esp.length;x++){
						arrayes[x+1]=Utils.remplazacadena(esp[x].getNombre());
					}
					choicesEs.setChoices(arrayes);
					//UiApplication.getUiApplication().pushScreen(new DirectorioScreen());
				
					
				}else{
					choicesEs.setChoices(arrayinit);	
				}
				}
					
				else{
					UiApplication.getUiApplication().pushScreen(new MessagePopupScreen("Intente m�s tarde", Field.NON_FOCUSABLE));	
				}
				
				
			}
			return null;
		}
		
	}
	
	
}

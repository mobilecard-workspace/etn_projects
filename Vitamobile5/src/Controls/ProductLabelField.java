package Controls;



import beans.CategoriesBean;
import mx.com.ib.utils.Utils;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.LabelField;


public class ProductLabelField extends LabelField{
	
	public String title = "";
	public String id = "";
	public Bitmap bitmap;
	public CategoriesBean productBeans = null;
	public CategoriesBean generalBean = null;
	public boolean flag = false;
	private int pfWidth, pfHeight, padding;
	  public static boolean t=true;
	public ProductLabelField(String title){
		
		super("", Field.FOCUSABLE);
		
		this.title = title;
		
		pfWidth = Display.getWidth()/(14/4);
		pfHeight = Display.getWidth()/(14/4);
		
		setBitmap(Bitmap.getBitmapResource("tiempoaire.png"));
	}
	
	public ProductLabelField(CategoriesBean productBeans,String nuevo){
		
		super("",Field.FOCUSABLE);
		
		this.title = productBeans.getText();
	
	
		
		this.productBeans = productBeans;
		
		pfWidth = Display.getWidth()/(14/4);
		pfHeight = Display.getWidth()/(14/4);
		
		setBitmap(Bitmap.getBitmapResource("tiempoaire.png"));
		
	}
	
	public ProductLabelField(CategoriesBean productBeans, boolean flag){
		
		super("",Field.FOCUSABLE);
		
		
		this.flag = flag;
		this.title = productBeans.getText();
		
		
		this.generalBean = productBeans;
		
		pfWidth = Display.getWidth()/(14/4);
		pfHeight = Display.getWidth()/(14/4);
		
		setBitmap(Bitmap.getBitmapResource("tiempoaire.png"));
	}
	
	public ProductLabelField(CategoriesBean productBeans){
		
		super(remplazacadena(productBeans.getText()),Field.FOCUSABLE);
	//	if(!productBeans.getClave().equals("7")){
		this.title = productBeans.getText();
		this.id=productBeans.getId();
		//title=remplazacadena(title);
		
		System.out.println(title);
		this.generalBean = productBeans;
		pfWidth = Display.getWidth()/(14/4);
		pfHeight = Display.getWidth()/(14/4);		
		setBitmap(Bitmap.getBitmapResource("tiempoaire.png"));
		}
	//}
	
	protected void paint(Graphics graphics) {
		// TODO Auto-generated method stub
		if((this.productBeans!=null)||(this.productBeans == null && !flag)){
			
			graphics.drawBitmap((getPreferredWidth()/2-bitmap.getWidth()/2)-2, 
					(getPreferredHeight()/2-bitmap.getHeight()/2)-2, 
					this.bitmap.getWidth(), this.bitmap.getHeight(), this.bitmap, 0, 0);
			int x = this.bitmap.getWidth()+5;
			
			Font font = getFont();
			
	        int width = font.getBounds(this.title); 
	        if(width > getPreferredWidth()){
	            int height = font.getHeight();
	            int niuh = (getPreferredWidth()-5)*height/width;
	       //     int niuh = (getPreferredWidth()-10)*height/width;
	            Font niuF = font.derive(Font.PLAIN, niuh);
	            graphics.setFont(niuF);
	           if(Display.getWidth()==480){
	        	   graphics.drawText(this.title, 
							(getPreferredWidth()-niuF.getBounds(this.title))/2, 
							bitmap.getWidth()+20);
	        	   
	           }else{
	        	   graphics.drawText(this.title, 
							(getPreferredWidth()-niuF.getAdvance(this.title))/2, 
							(getPreferredHeight()-niuF.getHeight()));
	        	   
	           }
	            
	        }else{
	        	//Font niuF = font.derive(Font.PLAIN, font.getHeight()-5);
	        	//graphics.setFont(niuF);
	        	 if(Display.getWidth()==480){
	        			graphics.drawText(this.title, 
	    						(getPreferredWidth()-getFont().getBounds(this.title))/2, 
	    						bitmap.getWidth()+24);
		        	   
		           }else{
		        	   graphics.drawText(this.title, 
								(getPreferredWidth()-getFont().getAdvance(this.title))/2, 
								(getPreferredHeight()-getFont().getHeight()));
		        	   
		           }
	        //g.drawText(text, (getContentRect().width-getFont().getBounds(text))/2, 2*padding+icon.getHeight());

			
	        }
			
		}else{
			
			graphics.drawText("$ "+this.title, 18, (getPreferredHeight()-getFont().getHeight())/2);
			
		}

		invalidate();
		super.paint(graphics);
	}
	
	protected void drawFocus(Graphics graphics, boolean on) {
		// TODO Auto-generated method stub
		if((this.productBeans!=null)||(this.productBeans == null && !flag)){
			
			graphics.drawBitmap(getPreferredWidth()/2-bitmap.getWidth()/2, 
					getPreferredHeight()/2-bitmap.getHeight()/2, 
					this.bitmap.getWidth(), this.bitmap.getHeight(), this.bitmap, 0, 0);
			int x = this.bitmap.getWidth()+15;
			graphics.drawText(this.title, x, (getPreferredHeight()-getFont().getHeight()));
			
		}else{
			
			graphics.drawText(this.title, 5, (getPreferredHeight()-getFont().getHeight())/2);
			
		}
		
		invalidate();
		super.drawFocus(graphics, on);
	}
	
	protected void layout(int width, int height) {
		// TODO Auto-generated method stub
		this.setExtent(getPreferredWidth(), getPreferredHeight());
	}
	
	public int getPreferredHeight() {
		// TODO Auto-generated method stub
		//return this.bitmap.getHeight()+10;
		return pfHeight;
	}
	
	public void setPreferredHeight(int pfh) {
		// TODO Auto-generated method stub
		//return this.bitmap.getHeight()+10;
		pfHeight = pfh;
	}
	
	public int getPreferredWidth() {
		// TODO Auto-generated method stub
		return pfWidth;
	}
	
	public void setPreferredWidth(int pfw){
		pfWidth = pfw;
	}
	
	protected boolean navigationClick(int status, int time) {
		
		t=true;
		String cadena="idSeccion="+id+"&plataforma=3";
		
		/*if(id.equals("1")){
			String cadena2="idSeccion=";
			new getRecentThread(cadena2+id).start();
			
		}else{
			String cadena="idSeccion=";
			new getNewsBySectionThread(cadena+id).start();
		}*/
		
		
		// TODO Auto-generated method stub
		/*
		if(this.productBeans != null){
			
			MainClass.textTitle = this.productBeans.getDescription();
			MainClass.splashScreen.start();
			new ProviderThread(this.productBeans.getClave()).start();
			System.out.println("IF1111");
			
		}else if(this.productBeans == null && !flag){

		
			
			MainClass.bitmap = this.bitmap;
			MainClass.textTitleInternal = MainClass.textTitle + " - " + this.generalBean.getDescription();  
			
			
			MainClass.splashScreen.start();
			new CatProductsThread(this.generalBean.getClaveWS()).start();
			System.out.println(this.generalBean.getDescription());
			System.out.println(this.generalBean.getClave());
			MainClass.IDPROVEEDOR=this.generalBean.getClave();
			System.out.println(this.generalBean.getClaveWS());
			System.out.println(this.generalBean.getPath());
			
		}else{		
			
			
			
			MainClass.textTitleConfirm = MainClass.textTitleInternal + " " + this.generalBean.getDescription();
			
			System.out.println("id proveeeeedooooooor "+MainClass.IDPROVEEDOR);
			if(/*MainClass.textTitleInternal.indexOf("IAVE") != -1||*//*MainClass.IDPROVEEDOR.equals("5")){
				MainClass.splashScreen.start();
				String json = Crypto.aesEncrypt(MainClass.key,MainClass.IDPROVEEDOR);
				new ComisionThread(json,1,this.generalBean).start();
				System.out.println("SOY IAVE");
			
			}else if(/*MainClass.textTitleInternal.indexOf("OHL") != -1||*//*MainClass.IDPROVEEDOR.equals("6")){
				MainClass.splashScreen.start();
				String json2 = Crypto.aesEncrypt(MainClass.key,MainClass.IDPROVEEDOR);
				new ComisionThread(json2,2,this.generalBean).start();
				
				System.out.println("SOY ohl");
			}else if(/*MainClass.textTitleInternal.indexOf("VIAPASS") != -1||*//*MainClass.IDPROVEEDOR.equals("7")){
				MainClass.splashScreen.start();
				String json3 = Crypto.aesEncrypt(MainClass.key,MainClass.IDPROVEEDOR);
				//hacer un substring
				String newUrl = Utils.replaceURL(json3, "+", "%2B");				
				new ComisionThread(newUrl,3,this.generalBean).start();
				
				
			}else if(/*MainClass.textTitleInternal.indexOf("OHL") != -1||*//*MainClass.IDPROVEEDOR.equals("8")){
				MainClass.splashScreen.start();
				String json2 = Crypto.aesEncrypt(MainClass.key,MainClass.IDPROVEEDOR);
				String newUrl = Utils.replaceURL(json2, "+", "%2B");		
				new ComisionThread(newUrl,4,this.generalBean).start();
				
			
			}
			
			else{
				
				UiApplication.getUiApplication().pushScreen(new ConfirmProductScreen(this.generalBean));
				
			}
			System.out.println("IF333");
		}
		System.out.println("PRoDUCT LABEL IN");
		System.out.println(" ");
		System.out.println(" ");
		System.out.println(" ");
		System.out.println("PRoDUCT LABEL OUT");
		*/
		return true;
	}
	
	public Bitmap getBitmap() {		
		
		return bitmap;
		
	}

	public void setBitmap(Bitmap bmp) {		
		
		//this.bitmap = Utils.resizeBitmap(bitmap, Bitmap.getBitmapResource("tiempoaire.png").getWidth(), Bitmap.getBitmapResource("tiempoaire.png").getHeight());
		
		float nh, nw;
		int padding=0;
		if(Display.getWidth()==480){
			 padding = 52;
		}else if(Display.getWidth()==360){
			 padding = 43;
			
		}else{
			
			
			 padding = 35;
		}
		
		
			if(bmp.getWidth() > bmp.getHeight()){
				float prop = (float)bmp.getHeight()/(float)bmp.getWidth();
				nh = (getPreferredWidth() - padding);
				nw = prop * (getPreferredWidth() - padding);
			}else{
				 float prop = (float)bmp.getWidth()/(float)bmp.getHeight();
				 nh = prop * (getPreferredHeight() - padding);
				 nw = getPreferredHeight()-padding;
			}
		System.out.println((int)nh+"img "+(int) nw);
		this.bitmap = Utils.resizeBitmap(bmp, (int)nh, (int) nw);
				
	}
	
	public CategoriesBean getProductBeans() {
		return productBeans;
	}

	public void setProductBeans(CategoriesBean productBeans) {
		this.productBeans = productBeans;
	}

	public CategoriesBean getGeneralBean() {
		return generalBean;
	}

	public void setGeneralBean(CategoriesBean generalBean) {
		this.generalBean = generalBean;
	}
	
	   public static String reemplaza(String src, String orig, String nuevo) {
	        if (src == null) {
	            return null;
	        }
	        int tmp = 0;
	        String srcnew = "";
	        while (tmp >= 0) {
	            tmp = src.indexOf(orig);
	            if (tmp >= 0) {
	                if (tmp > 0) {
	                    srcnew += src.substring(0, tmp);
	                }
	                srcnew += nuevo;
	                src = src.substring(tmp + orig.length());
	            }
	        }
	        srcnew += src;
	        return srcnew;
	    }

	    public static String remplazacadena(String conAcentos) {
	        String caracteres[] = {" "};
	        String letras[] = {"\n"};
	        for (int counter = 0; counter < caracteres.length; counter++) {
	            conAcentos = reemplaza(conAcentos, caracteres[counter], letras[counter]);
	        }
	        return conAcentos;
	    }

}

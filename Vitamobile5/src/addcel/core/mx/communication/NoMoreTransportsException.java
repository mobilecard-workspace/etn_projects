package addcel.core.mx.communication;

public class NoMoreTransportsException extends Exception {
    public NoMoreTransportsException(String s) {
        super(s);
    }

    public NoMoreTransportsException() {
    }
}
